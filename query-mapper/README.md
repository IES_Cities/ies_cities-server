 Query mapper library
======================

This module contains the funcionality to perform SQL queries on SPARQL and SQL datasources by translating the query
to the corresponding format, if needed.

Mapping description
-------------------

The mapping process is based on the usage of schema files that describe how to process the query for each datasource.

### RDF Datasource
For RDF datasources, which can be accesed using SPARQL, the mapping file has the following format.
There are more examples in *IES_Cities-server/query-mapper/src/test/resources/schema/* directory. In addition,
there is an utility to extract this schema from an SPARQL endpoint, see below.

	{
		"mapping": "sparql",	
		"sparqlEndpoint": "http://domain/sparql",
		"graph": "",
		"tables": [
			{	"name": "table1",
				"rdfClass": "http://domain/table1#Class1",
				"primaryKey": [ 
					{ 	"name": "id"	}
				],
				"columns": [
					{	"name": "colNameA",
						"rdfProperty": "http://domain/table1#PropertyA",
						"type": "STRING"
					},
					{	"name": "colNameB",
						"rdfProperty": "http://domain/table1#PropertyB",
						"type": "STRING"
					},
					{	"name": "colNameC",
						"rdfProperty": "http://domain/table1#PropertyC",
						"type": "STRING"
					}
				]
			},
	
			{	"name": "table2",
				"rdfClass": "http://domain/table1#Class2",
				"primaryKey": [ 
					{ 	"name": "id",
						"references": "table1"
					}
				],
				"columns": [
					{	"name": "colName1",
						"rdfProperty": "http://domain/table2#Property1",
						"type": "STRING"
					},
					{	"name": "colName2",
						"rdfProperty": "http://domain/table2#Property2",
						"type": "STRING"
					},
					{	"name": "colName3",
						"rdfProperty": "http://domain/table2#Property3",
						"type": "INTEGER"
					}
				]
			},
	
			{	"name": "table3",
				"rdfClass": "http://domain/table1#Class3",
				"primaryKey": [ 
					{ 	"name": "id"	}
				],
				"columns": [
					{	"name": "colName1",
						"rdfProperty": "http://domain/table3#Property1",
						"type": "STRING"
					},
					{	"name": "colName2",
						"rdfProperty": "http://domain/table3#Property2",
						"type": "STRING"
					},
					{	"name": "colName3",
						"rdfProperty": "http://domain/table3#Property3",
						"type": "STRING"
					}
				]
			}
		]
	}
	
Every schema mapping contains the URL of the SPARQL endpoint and the name of the RDF graph
that it is being translated to a relational view. The graph name can be left empty, meaning
that the default graph for that endpoint will be used. The mapping also contains a list of 
\emph{Table} descriptions elements, one for each relational table that will be created in
the resulting view. Each \emph{Table} description contains the following elements: 

* **Name:** This is the name that SQL queries will use when referring to the table and, 
therefore, it must be unique inside a mapping. It can also have the special value
*manytomany* which is used to specify a N to M relationship mapping table.
* **RDF Class:** The URI of the RDF class that is mapped to this table. All
instances from the RDF model belonging to the class will appear as rows of the table.
* **Columns:** A list of column descriptions which contains information about how
class properties are mapped to table attributes.
* **Primary Keys:** A list which defines the name of the primary keys of the table.

Each *Column* description contains the following elements needed by SQLify to map the
RDF properties into attributes:

* **Name:** The name that SQL queries will use when referring to the column. It
must be unique inside each table description.
* **RDF Property:** The URI of the property that this column is mapping from the
RDF model.
* **Type:** The type of the mapped column, which can be one of the following
basic types: *BOOLEAN, CHAR, BYTE, FLOAT, DOUBLE, SHORT, INT, LONG, STRING*, when mapping
* **RDF Data Object** properties. It can also have the special value *objectproperty* to
indicate that the mapped RDF property is an *Object Property*.


### Relational Database mapping
This mapping is used to redirect SQL queries to an external database. 

	{
		"mapping": "database",
		"dbType": "PostgreSQL",
		"host": "thehost",
		"database": "somedatabase",
		"user": "someuser",
		"pass": "somepass"
	}
	
* **dbType:** SQLite | PostgreSQL | MySQL
* **host:** The IP/name of the machine where the database is located on
* **database:** The name of the database. For SQLite database it is the path to the db file.
* **user:** User with select privileges on the database. Not required for SQLite databases.
* **pass:** Password for the user. Not required for SQLite databases.



Build
-----

		mvn install  


Command line utility
--------------------

The library contains a command line utility to test the posibilities of the translation process.

1. **List available graphs in SPARQL endpoint**

		java -jar target/query-mapper-0.1-full.jar --list-graphs <sparqlEndpoint>

2. **Get schema from SPARQL endpoint** 

	*--graph* is optional. If not specified the endpoint default graph will be used.
	
	*--output* is optional. The output will be written to the console if no output file is specified.

		java -jar target/query-mapper-0.1-full.jar --get-schema <sparqlEndpoint> --graph <graphURI> --output <outputFile>
		
3. **Obtain schema in SQL format** 		

	*--output* is optional. The output will be written to the console if no output file is specified.
	
		java -jar target/query-mapper-0.1-full.jar --export-schema <schemaFile> --output <outputFile>

4. **Start SQLify client using specified schema**

	Add optional parameter *-v* to show SPARQL queries 

		java -jar target/query-mapper-0.1-full.jar --connect <schemaFile>
		

Example
-------

1. **Start Fuseki server with sample RDF data**

	**Note:** Fuseki server blocks current terminal. **Note:** Fuseki server blocks current terminal. Open another terminal to continue.

		java -jar fuseki/fuseki-server.jar --update --file=fuseki/data/foaf.rdf /ds

2. **Get schema automatically from RDF**

		java -jar target/query-mapper-0.1-full.jar --get-schema http://localhost:3030/ds/query --output foaf_schema.json

3. **Start SQLify client using downloaded schema**

		java -jar target/query-mapper-0.1-full.jar --connect foaf_schema.json
        
4. **Run SQL queries**

		sqlify> select * from foaf_Person;
        
5. **Export schema to SQL format (Optional)** 

		java -jar target/query-mapper-0.1-full.jar --export-schema foaf_schema.json --output foaf_schema.sql

		
Commands
--------

+ **Show tables**

	Print the list of available tables for the current schema.
		
		sqlify> show tables;
		
+ **Change output format**

	Change the query output format. Supported types: **json_v1**, **json_v2**, **json_ld** (only with compatible endpoints).
	
		sqlify> output <type>
	
+ **Insert data from JSON file**

	Only for relational backed datasets.
	
		sqlify> insert json <path>
		
+ **Insert data from JSON-LD file**

	Only for relational backed datasets.
	
		sqlify> insert json_ld <path>
		
	
