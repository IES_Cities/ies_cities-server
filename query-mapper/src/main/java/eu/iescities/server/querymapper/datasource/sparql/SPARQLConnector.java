/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.sparql;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector.Table.Column;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class SPARQLConnector extends DataSourceConnector {

	public static class Table {

		public static class Key {

			@Expose
			private String name;
			
			@Expose
			private LiteralType type;
			
			@Expose
			private String references;

			public Key() {

			}

			public Key(String name) {
				this.name = name;
			}

			public Key(String name, LiteralType type) {
				this.name = name;
				this.type = type;
			}

			public Key(String name, String references) {
				this.name = name;
				this.references = references;
			}

			public void setName(String name) {
				this.name = name;
			}

			public void setType(LiteralType type) {
				this.type = type;
			}

			public void setReferences(String references) {
				this.references = references;
			}

			public String getName() {
				return name;
			}

			public String getReferences() {
				return references;
			}

			public LiteralType getType() {
				if (type == null) {
					return LiteralType.STRING;
				}

				return type;
			}

			@Override
			public boolean equals(Object o) {
				if (!(o instanceof Key)) {
					return false;
				}
				
				final Key key = (Key) o;
				return this.name.equals(key.name)
					&& this.type == key.type
					&& ((this.references == null && key.references == null) || this.references.equals(key.references));
			}
		}

		public static class Column {

			@Expose
			private String name;
			
			@Expose
			private String rdfProperty;
			
			@Expose
			private String type;

			public Column() {

			}

			public Column(String name, String rdfProperty, String type) {
				this.name = name;
				this.rdfProperty = rdfProperty;
				this.type = type;
			}

			public String getRdfProperty() {
				return rdfProperty;
			}

			public void setRdfProperty(String rdfProperty) {
				this.rdfProperty = rdfProperty;
			}

			public void setName(String name) {
				this.name = name;
			}

			public void setType(String type) {
				this.type = type;
			}

			public String getName() {
				return name;
			}

			public String getRDFProperty() {
				return rdfProperty;
			}

			public String getType() {
				return type;
			}

			@Override
			public boolean equals(Object o) {
				if (!(o instanceof Column)) {
					return false;
				}
				
				final Column column = (Column) o;
				return this.name.equals(column.name)
					&& this.rdfProperty.equals(column.rdfProperty)
					&& this.type.equals(column.type);
			}
		}

		@Expose
		private String name;
		
		@Expose
		private String rdfClass;
		
		@Expose
		private List<Key> primaryKey = new ArrayList<Key>();

		@Expose
		private List<Column> columns = new ArrayList<Column>();

		public Table() {

		}

		public Table(String name, String rdfClass) {
			this.name = name;
			this.rdfClass = rdfClass;
		}

		public String getRdfClass() {
			return rdfClass;
		}

		public void setRdfClass(String rdfClass) {
			this.rdfClass = rdfClass;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setPrimaryKey(List<Key> primaryKey) {
			this.primaryKey = primaryKey;
		}

		public void setColumns(List<Column> columns) {
			this.columns = columns;
		}

		public void addKey(Key key) {
			primaryKey.add(key);
		}

		public String getName() {
			return name;
		}

		public String getRDFClass() {
			return rdfClass;
		}

		public List<Key> getPrimaryKey() {
			return primaryKey;
		}

		public List<Column> getColumns() {
			return columns;
		}

		public void addColumn(Column column) {		
			if (!containsColumn(column.getName())) {
				columns.add(column);
			}
		}

		public Column getColumn(String name) {
			for (Column column : columns)
				if (column.getName().toLowerCase().equals(name.toLowerCase()))
					return column;

			return null;
		}

		public boolean containsColumn(String name) {
			return getColumn(name) != null;
		}

		public boolean containsKey(String keyName) {
			for (Key key : primaryKey)
				if (key.getName().equals(keyName))
					return true;
			return false;
		}

		public boolean isManyToMany() {
			return getRDFClass().equals(SchemaBuilder.MANY_TO_MANY);
		}

		public void merge(Table table) {
			for (Column column : table.getColumns()) {
				if (!this.containsColumn(column.getName())) {
					this.addColumn(column);
				}
			}
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Table)) {
				return false;
			}
			
			final Table table = (Table) o;
			
			return this.name.equals(table.name)
				&& this.rdfClass.equals(table.rdfClass)
				&& this.primaryKey.equals(table.primaryKey)
				&& this.columns.equals(table.columns);
		}
	}

	@Expose
	private String queryEndpoint;
	
	@Expose
	private String updateEndpoint;
	
	@Expose
	private String graph;
	
	@Expose
	private List<Table> tables;

	public SPARQLConnector() {
		super(ConnectorType.sparql);
	}

	public SPARQLConnector(String sparqlEndpoint, String graph) {
		super(ConnectorType.sparql);

		this.queryEndpoint = sparqlEndpoint;
		this.graph = graph;
		this.tables = new ArrayList<Table>();
	}

	public void setQueryEndpoint(String queryEndpoint) {
		this.queryEndpoint = queryEndpoint;
	}

	public void setUpdateEndpoint(String updateEndpoint) {
		this.updateEndpoint = updateEndpoint;
	}

	public void setGraph(String graph) {
		this.graph = graph;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	public List<Table> getTables() {
		return tables;
	}

	public String getQueryEndpoint() {
		return queryEndpoint;
	}

	public String getUpdateEndpoint() {
		return updateEndpoint;
	}

	public String getGraph() {
		return graph;
	}

	public List<Table> getSchemaTables() {
		return Collections.unmodifiableList(tables);
	}

	public void addTable(Table table) {
		tables.add(table);
	}

	public Table getTable(String name) {
		for (Table table : tables)
			if (table.getName().equals(name))
				return table;

		return null;
	}

	public boolean containsTable(String name) {
		return getTable(name) != null;
	}

	public static SPARQLConnector load(String jsonSchema) {
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss")
				.create();
		return gson.fromJson(jsonSchema, SPARQLConnector.class);
	}

	public static SPARQLConnector load(InputStream is) throws IOException {
		InputStreamReader reader = new InputStreamReader(is);
		try {
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss")
					.create();
			return gson.fromJson(reader, SPARQLConnector.class);
		} catch (JsonSyntaxException jse) {
			throw new IOException(jse);
		}
	}

	public void merge(SPARQLConnector schema) {
		for (SPARQLConnector.Table mergedTable : schema.getSchemaTables()) {
			final Table table = getTable(mergedTable.getName());
			if (table != null) {
				table.merge(mergedTable);
			} else
				this.addTable(mergedTable);
		}
	}

	public List<String> getTableNames() throws DataSourceManagementException {
		final List<String> names = new ArrayList<String>();
		for (Table table : tables) {
			names.add(table.getName());
		}

		return names;
	}

	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		final DataSourceInfo dataSourceInfo = new DataSourceInfo(
				ConnectorType.sparql);

		for (Table table : tables) {
			final TableInfo tableInfo = new TableInfo(table.getName());
			for (Column column : table.getColumns()) {
				final ColumnInfo columnInfo = new ColumnInfo(column.getName(),
						column.getType(), false);
				tableInfo.addColumnInfo(columnInfo);
			}
			dataSourceInfo.addTable(tableInfo);
		}

		return dataSourceInfo;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SPARQLConnector)) {
			return false;
		}
		
		final SPARQLConnector sparqlConnector = (SPARQLConnector) o;
		return this.queryEndpoint.equals(sparqlConnector.queryEndpoint)
			&& this.updateEndpoint.equals(sparqlConnector.updateEndpoint)
			&& this.graph.equals(sparqlConnector.graph)
			&& this.tables.equals(sparqlConnector.tables);
	}
}