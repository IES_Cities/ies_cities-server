package eu.iescities.server.querymapper.datasource.database.connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;

public class SQLiteConnection extends DBConnection {
		
	public SQLiteConnection(String database) {
		super(DBType.SQLite, "", "", "", database);
	}

	@Override
	public String getDriver() {
		return "org.sqlite.JDBC";
	}

	@Override
	public int getLastInsertedId() throws DataSourceSecurityManagerException {
		try {			
			final ResultSet results = stmt.executeQuery("SELECT LAST_INSERT_ROWID()");
			if (results.next()) {	
				return results.getInt(1);
			} else {
				throw new DataSourceSecurityManagerException("Could not obtain last inserted id for security management"); 
			}
		} catch (Exception e) {
			throw new DataSourceSecurityManagerException(e.getMessage());
		}
	}

	@Override
	public DataSourceInfo getDBInfo() throws DataSourceManagementException {
		final DataSourceInfo datasourceInfo = new DataSourceInfo(ConnectorType.database);
		
		try {
			final List<String> tables = getTables();
			
			for (String tableName : tables) {
				final ResultSet tableResult = stmt.executeQuery(String.format("PRAGMA table_info(\"%s\")", tableName));
				
				final TableInfo tableInfo = new TableInfo(tableName);
				while (tableResult.next()) {
					final String columnName = tableResult.getString("name");
					final String type = tableResult.getString("type");
					final boolean primaryKey = tableResult.getBoolean("pk");
					tableInfo.addColumnInfo(new ColumnInfo(columnName, type, primaryKey));
				}
				
				datasourceInfo.addTable(tableInfo);
			}
			
			return datasourceInfo;
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		}
	}

	@Override
	public List<String> getTables() throws DataSourceManagementException {
		final List<String> tables = new ArrayList<String>();
		try {
			final ResultSet result = stmt.executeQuery("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;");
			while (result.next()) {
				tables.add(result.getString(1));
			}
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		}  
		
		return tables;
	}

	@Override
	public String getConnectionURL() {
		return String.format("jdbc:sqlite:%s", database);
	}
}
