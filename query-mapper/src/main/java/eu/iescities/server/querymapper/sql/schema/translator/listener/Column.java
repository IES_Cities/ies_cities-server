/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.listener;

import eu.iescities.server.querymapper.sql.schema.translator.util.VariableBinder;

class Column {

	private final String name;
	private String alias;
	private boolean primaryKey;

	private String table;

	public Column(String name, String table, String alias) {
		this.name = name;
		this.table = table;
		this.alias = alias;
		this.primaryKey = false;
	}

	public Column(String name, String table, String alias, boolean primaryKey) {
		this.name = name;
		this.table = table;
		this.alias = alias;
		this.primaryKey = primaryKey;
	}

	public String getName() {
		return name;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey() {
		primaryKey = true;
	}

	public String getSPARQLField(boolean qualify) {
		String fieldStr = "(str(?%s) as ?%s) ";
		if (getAlias().isEmpty()) {
			return String.format(fieldStr, 
				isPrimaryKey()? VariableBinder.getInstance().bind(getTable()) : VariableBinder.getInstance().bind(getName()), 
				qualify? getTable() + "_" + getName(): getName());
		}
		else
			return String.format(fieldStr, 
				isPrimaryKey()? VariableBinder.getInstance().bind(getTable()) : VariableBinder.getInstance().bind(getName()), 
				getAlias());
	}
}