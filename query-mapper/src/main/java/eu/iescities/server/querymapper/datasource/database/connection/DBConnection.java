package eu.iescities.server.querymapper.datasource.database.connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;

public abstract class DBConnection {
	
	protected final String user;
	protected final String pass;
	
	protected final String host;
	protected final String database;
	
	private final DBType dbType;
	
	private Connection conn = null;
	protected Statement stmt = null;
	
	public DBConnection(DBType dbType, String user, String pass, String host, String database) {
		this.dbType = dbType;
		this.user = user;
		this.pass = pass;
		this.host = host;
		this.database = database;
	}

	public DBType getDBType() {
		return dbType;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPass() {
		return pass;
	}
	
	public String getDatabase() {
		return database;
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public Statement getStatement() {
		return stmt;
	}
	
	public void beginTransaction() throws DataSourceManagementException {
		try {
			open();
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not open database connection with transactional mode", e);
		}
	}
	
	public void commit() throws DataSourceManagementException {
		try {
			conn.commit();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not commit changes to database",  e);
		} finally {
			close();
		}
	}
	
	public void rollback() throws DataSourceManagementException {
		try {
			conn.rollback();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not rollback changes in database",  e);
		} finally {
			close();
		}
	}
	
	public void open() throws DataSourceManagementException {
		try {
			final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionURL(), getDriver());
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
		} catch (ClassNotFoundException e) {
			throw new DataSourceManagementException("Could not open connection to database. Driver could not be loaded", e);
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not create statement for database", e);
		}
	}
	
	public void close() throws DataSourceManagementException {
		try {
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
			
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not close database connection", e);
		}
	}
	
	public abstract String getConnectionURL();
	
	public abstract String getDriver();
	
	public abstract int getLastInsertedId() throws DataSourceSecurityManagerException;
	
	public abstract DataSourceInfo getDBInfo() throws DataSourceManagementException;
	
	public abstract List<String> getTables() throws DataSourceManagementException;

	public void closeAll() throws DataSourceManagementException {
		try {
			PoolManager.getInstance().closeDataSource(getConnectionURL());
		} catch (IOException e) {
			throw new DataSourceManagementException("Could not close datasource", e);
		}
	}
}
