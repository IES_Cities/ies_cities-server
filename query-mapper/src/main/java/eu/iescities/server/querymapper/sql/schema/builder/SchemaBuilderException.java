/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

@SuppressWarnings("serial")
public class SchemaBuilderException extends Exception {

	private String query;

	public SchemaBuilderException(String query) {
		this.query = query;
	}

	public SchemaBuilderException(String message, String query) {
		super(message);
		this.query = query;
	}

	public SchemaBuilderException(String message, Throwable cause, String query) {
		super(message, cause);
		this.query = query;
	}

	public SchemaBuilderException(Throwable cause, String query) {
		super(cause);
		this.query = query;
	}

	public String getQuery() {
		return query;
	}
}