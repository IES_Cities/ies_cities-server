/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;

public class DataSourceInfo {

	public static class TableInfo {
		
		private String name;
		private List<ColumnInfo> columns = new ArrayList<DataSourceInfo.ColumnInfo>();
		
		public TableInfo() {
			
		}
		
		public TableInfo(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public void addColumnInfo(ColumnInfo columnInfo) {
			columns.add(columnInfo);
		}
		
		public List<ColumnInfo> getColumns() {
			return Collections.unmodifiableList(columns);
		}

		public void setName(String name) {
			this.name = name;
		}
		
		public void setColumns(List<ColumnInfo> columns) {
			this.columns = columns;
		}

		public ColumnInfo getPrimaryKey() {
			for (ColumnInfo column : columns) {
				if (column.isKey()) {
					return column;
				}
			}
			return null;
		}
		
		public ColumnInfo getColumn(String name) { 
			for (ColumnInfo cInfo : columns) {
				if (cInfo.getName().equals(name)) {
					return cInfo;
				}
			}
			
			return null;
		}
	}
	
	public static class ColumnInfo {
		
		private String name;
		private String type;
		private boolean isKey;
		
		public ColumnInfo() {
			
		}
		
		public ColumnInfo(String name, String type, boolean key) {
			this.name = name;
			this.type = type;
			this.isKey = key;
		}
		
		public String getName() {
			return name;
		}
		
		public String getType() {
			return type;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setType(String type) {
			this.type = type;
		}
		
		public boolean isKey() {
			return isKey;
		}
		
		public void setKey(boolean isKey) {
			this.isKey = isKey;
		}
	}

	private ConnectorType type = ConnectorType.not_available;
	private List<TableInfo> tables = new ArrayList<DataSourceInfo.TableInfo>();
	
	public DataSourceInfo() {
		
	}
	
	public DataSourceInfo(ConnectorType type) {
		this.type = type;
	}
	

	public ConnectorType getType() {
		return type;
	}

	public void addTable(TableInfo tableInfo) {
		tables.add(tableInfo);
	}
	
	public List<TableInfo> getTables() {
		return tables;
	}

	public void setType(ConnectorType type) {
		this.type = type;
	}
	
	public void setTables(List<TableInfo> tables) {
		this.tables = tables;
	}

	public TableInfo getTable(String tableName) {
		for (TableInfo tableInfo : tables) {
			if (tableInfo.getName().toLowerCase().equals(tableName.toLowerCase())) {
				return tableInfo;
			}
		}
		return null;
	}
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(String.format("Type: %s\n", type.toString()));
		for (TableInfo tableInfo : tables) {
			strBuilder.append(String.format("Table: %s\n", tableInfo.getName()));
			for (ColumnInfo columnInfo : tableInfo.getColumns()) {
				strBuilder.append(String.format("Column: %s\n", columnInfo.getName()));
				strBuilder.append(String.format("Type: %s\n", columnInfo.getType()));
				strBuilder.append(String.format("Key: %s\n", columnInfo.isKey()));
			}
		}
		
		return strBuilder.toString();
	}
}
