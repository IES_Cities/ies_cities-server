/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.IntervalSet;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.parser.SQLSelectLexer;
import eu.iescities.server.querymapper.sql.parser.SQLSelectParser;
import eu.iescities.server.querymapper.sql.schema.translator.listener.SPARQLTranslatorListener;
import eu.iescities.server.querymapper.sql.schema.translator.listener.TranslatedQuery;
import eu.iescities.server.querymapper.sql.schema.translator.util.VariableBinder;

public class SPARQLTranslator {

	private final SPARQLConnector schema;

	public SPARQLTranslator(SPARQLConnector schema) {
		this.schema = schema;
	}

	public SPARQLConnector getSchemaConnector() {
		return schema;
	}

	public TranslatedQuery translate(String sqlQuery) throws TranslationException {
		if (schema == null)
			throw new TranslationException("Schema not loaded");

		VariableBinder.getInstance().reset();

		try {
			ANTLRInputStream input = new ANTLRInputStream(new ByteArrayInputStream(sqlQuery.getBytes()));

			SQLSelectLexer lexer = new SQLSelectLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			SQLSelectParser parser = new SQLSelectParser(tokens);

			parser.removeErrorListeners();
			parser.setErrorHandler(new BailErrorStrategy());

			ParseTree tree = parser.statement();
			ParseTreeWalker walker = new ParseTreeWalker();

			SPARQLTranslatorListener translatorListener = new SPARQLTranslatorListener(schema);
			walker.walk(translatorListener, tree);

			return translatorListener.getSPARQLQuery();
		} catch (IOException ioe) {
			throw new TranslationException(ioe);
		} catch (ParseCancellationException pce) {
			if (pce.getCause() instanceof RecognitionException) {
				RecognitionException inputMismatchException = (RecognitionException)pce.getCause();
				Token token = inputMismatchException.getOffendingToken();
				IntervalSet expecting = inputMismatchException.getExpectedTokens();
				throw new TranslationException("line " + token.getLine() + ":" + token.getCharPositionInLine() +
					" Expecting '" + expecting.toString(inputMismatchException.getRecognizer().getTokenNames()) + "'" +
					" but '" + token.getText() + "' was found instead");
			}

			throw new TranslationException(pce);
		}
	}
}