/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json.schema;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.query.DBDataSourceQuerier;
import eu.iescities.server.querymapper.datasource.database.update.DBDataSourceUpdater;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.JSONDumper;
import eu.iescities.server.querymapper.datasource.json.JSONDumper.StorageType;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.SecurityFactory;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class JSONSchemaDataSource implements SQLQueriable, SQLUpdateable, JSONUpdateable {
	
	class WrappedJsonConnector extends JSONConnector {
		
		private final String json;
		
		public WrappedJsonConnector(String json, String root, String key) {
			this.json = json;
			setRoot(root);
			setKey(key);
		}

		@Override
		public Reader getReader() throws MalformedURLException, IOException {
			return new StringReader(json);
		}
	}
	
	private final DataSourceSecurityManager manager;
	private JSONDumper jsonDumper;
	private final DBDataSourceQuerier querier;
	private final DBDataSourceUpdater updater;
	private Permissions permissions;
	
	public JSONSchemaDataSource(JSONSchemaConnector connector, String dataSourceID) throws DatabaseCreationException, DataSourceManagementException {
		this.manager = SecurityFactory.getInstance().getSecurityManager();
		this.jsonDumper = createDatabase(connector, dataSourceID);
		this.querier = new DBDataSourceQuerier(jsonDumper.getDumpDBConnector(), manager);
		this.updater = new DBDataSourceUpdater(jsonDumper.getDumpDBConnector(), manager);
		this.permissions = connector.getPermissions();
	}
	
	private JSONDumper createDatabase(JSONSchemaConnector connector, String dataSourceID) throws DatabaseCreationException, DataSourceManagementException {
		final JSONDumper currentJSONDumper = new JSONDumper(connector, manager, dataSourceID, StorageType.USER);
		if (!currentJSONDumper.existsUserDB()) {
			JSONDumper jsonDumper = null;
			final JsonObject rootObj = connector.getSchema();
			final JsonArray tables = rootObj.get("tables").getAsJsonArray();
			for (int i = 0; i < tables.size(); i++) {
				if (tables.get(i).isJsonObject()) {
					final JsonObject table = tables.get(i).getAsJsonObject();
					final String key = table.get("key").getAsString();
					final String name = table.get("name").getAsString();
					final String tableJSON = table.toString();
							 
					jsonDumper = new JSONDumper(new WrappedJsonConnector(tableJSON, name, key), manager, dataSourceID, StorageType.USER, true);
					jsonDumper.reloadDatabase();
				}
			}
			
			jsonDumper.setAppend(false);
			
			return jsonDumper;
		} else {
			return currentJSONDumper; 
		}
	}

	@Override
	public int executeSQLUpdate(String sqlUpdate, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeSQLUpdate(sqlUpdate, permissions, userid);
	}

	@Override
	public ResultSet getTables() throws DataSourceManagementException {
		return querier.getTables();
	}
	
	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		final DataSourceInfo info = querier.getInfo();
		info.setType(ConnectorType.json_schema);
		return info;
	}

	@Override
	public ResultSet executeSQLSelect(String sqlQuery, String userid, DataOrigin dataOrigin) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException {
		return querier.executeSQLSelect(sqlQuery, permissions, userid);
	}

	@Override
	public long getExecutionTime() {
		return querier.getExecutionTime();
	}

	@Override
	public int executeJSONUpdate(String json, String userid) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeJSONUpdate(json, userid, permissions);
	}

	@Override
	public int executeSQLTransactionUpdate(String text, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeSQLTransactionUpdate(text, permissions, userid);
	}

	@Override
	public Permissions getPermissions() {
		return permissions;
	}

	@Override
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public String createQueryAll() throws DataSourceManagementException {
		return querier.createQueryAll();
	}
	
	@Override
	public ResultSet queryAll(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		final String query = createQueryAll();
		return executeSQLSelect(query, userid, dataOrigin);
	}
	
	@Override
	public JsonObject queryAllJson(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		return querier.queryAllCompact(permissions, userid);
	}
}
