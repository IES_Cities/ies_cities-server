package eu.iescities.server.querymapper.datasource.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class URLReader extends Reader {
	
	private final InputStreamReader reader;

	public URLReader(String url, String charset) throws MalformedURLException, IOException {
		if (url.startsWith("file://")) {
			final URL obj = new URL(url);
		    final URLConnection conn = obj.openConnection();
		    
			reader = new InputStreamReader(conn.getInputStream(), Charset.forName(charset));
		} else if (url.startsWith("http://") || url.startsWith("https://")) {
			reader = checkRedirect(url, charset);
		} else {
			final InputStream is = this.getClass().getResourceAsStream(url);
			if (is == null) {
				throw new IOException("Could not open URL " + url);
			} else {
				reader = new InputStreamReader(is, Charset.forName(charset));
			}
		}
	}
	
	private InputStreamReader checkRedirect(String url, String charset) throws IOException {
		final URL obj = new URL(url);
	    HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	    
	    boolean redirect = false;
	    
	    final int status = conn.getResponseCode();
	    if (status != HttpURLConnection.HTTP_OK) {
	    	if (status == HttpURLConnection.HTTP_MOVED_TEMP
	    		|| status == HttpURLConnection.HTTP_MOVED_PERM
	    			|| status == HttpURLConnection.HTTP_SEE_OTHER)
	    	redirect = true;
	    }
	    
	    if (redirect) {
	    	final String newUrl = conn.getHeaderField("Location");	    	
	    	conn.disconnect();
	    	conn = (HttpURLConnection) new URL(newUrl).openConnection();
	    }
	    
    	return new InputStreamReader(conn.getInputStream(), Charset.forName(charset));
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		return reader.read(cbuf, off, len);
	}

	@Override
	public void close() throws IOException {
		reader.close();		
	}
}
