/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;

class Table {

	private final String name;
	private final String alias;
	private final SPARQLConnector.Table tableSchema;

	private final List<Column> columns = new ArrayList<Column>();

	public Table(String name, String alias, SPARQLConnector.Table tableSchema) {
		this.name = name;
		this.alias = alias;
		this.tableSchema = tableSchema;
	}

	public String getName() {
		return name;
	}

	public void addColumn(Column column) {
		columns.add(column);
	}

	public boolean containsColumn(String name) {
		for (Column column : columns)
			if (column.getName().equals(name))
				return true;

		return false;
	}

	public List<Column> getColumns() {
		return Collections.unmodifiableList(columns);
	}

	public String getAlias() {
		return alias;
	}

	public SPARQLConnector.Table getTableSchema() {
		return tableSchema;
	}
}