/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;

public class TablePermission {
	
	@Expose
	private String table;
	
	@Expose
	private AccessType access;
	
	@Expose
	private List<String> users;
	
	public TablePermission() {
		this.users = new ArrayList<String>();
	}

	public TablePermission(String table, AccessType access) {
		this.table = table;
		this.access = access;
		this.users = new ArrayList<String>();
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public AccessType getAccess() {
		return access;
	}

	public void setAccess(AccessType access) {
		this.access = access;
	}
	
	public List<String> getUsers() {
		return users;
	}
	
	public void addUser(String user) {
		users.add(user);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof TablePermission)) {
			return false;
		}
		
		final TablePermission tablePermission = (TablePermission) o;
		return this.table.equals(tablePermission.table)
				&& this.access == tablePermission.access
				&& this.users.equals(tablePermission.users);
	}
}
