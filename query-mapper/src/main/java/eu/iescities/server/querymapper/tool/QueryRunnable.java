package eu.iescities.server.querymapper.tool;

import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.tool.SQLifyTool.OUTPUT_FORMATS;

public class QueryRunnable implements Runnable {

	private final SQLQueriable queriable;
	private final OUTPUT_FORMATS outputFormat;
	private String sqlQuery;
	private int time;
	
	public QueryRunnable(SQLQueriable queriable, OUTPUT_FORMATS outputFormat, String sqlQuery, int time) {
		this.queriable = queriable;
		this.outputFormat = outputFormat;
		this.sqlQuery = sqlQuery; 
		this.time = time;
	}
	
	@Override
	public void run() {
		boolean stop = false;
		while(!stop) {
			SQLifyTool.processSQLQuery(queriable, outputFormat, sqlQuery);
			try {
				Thread.sleep(time * 1000);
			} catch (InterruptedException e) {
				stop = true;
			}
		}
	}
}
