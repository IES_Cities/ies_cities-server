package eu.iescities.server.querymapper.datasource.json;

import java.util.HashMap;
import java.util.Map;

public class KeyGenerator {
	
	private Map<String, Integer> keys;

	public KeyGenerator() {
		keys = new HashMap<String, Integer>();
	}
	
	public int next(String table) {
		if (!keys.containsKey(table)) {
			keys.put(table, new Integer(1));
		} else {
			keys.put(table, keys.get(table) + 1);
		}		
		
		return keys.get(table); 
	}
}
