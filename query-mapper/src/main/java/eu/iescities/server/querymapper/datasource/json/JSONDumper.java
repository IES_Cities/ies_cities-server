/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.exporter.SPARQLSchemaExporter;

public class JSONDumper {
	
	final static Logger logger = Logger.getLogger(JSONDumper.class);

	public static final String DUMPS_DIR = Config.getInstance().getDataDir() + File.separatorChar + "dumps";
	public static final String USER_DIR = Config.getInstance().getDataDir() + File.separatorChar + "user";
	
	public enum StorageType { DUMP, USER };
	
	private DBConnector dumpDBConnector;
	private DBConnector userDBConnector;
	private DBConnector tempDBConnector;
	
	protected final DataSourceSecurityManager manager;
	
	protected final String dataSourceID;
	protected final JSONConnector connector;
	
	private final StorageType storage;
	private boolean append = false;
	
	protected JSONDumper(JSONConnector connector, DataSourceSecurityManager manager, String dataSourceID) {
		this.connector = connector;
		this.manager = manager;
		this.dumpDBConnector = null;
		this.userDBConnector = null;
		this.dataSourceID = dataSourceID;
		this.storage = StorageType.DUMP;
	}
	
	public JSONDumper(JSONConnector connector, DataSourceSecurityManager manager, String dataSourceID, StorageType storage) throws DatabaseCreationException, DataSourceManagementException {
		this.connector = connector;
		this.dataSourceID = dataSourceID;
		this.manager = manager;
		this.storage = storage;
		
		createConnectors();
	}
	
	public JSONDumper(JSONConnector connector, DataSourceSecurityManager manager, String dataSourceID, StorageType storage, boolean append) throws DatabaseCreationException, DataSourceManagementException {
		this.connector = connector;
		this.dataSourceID = dataSourceID;
		this.manager = manager;
		this.storage = storage;
		this.append = append;
		
		createConnectors();
	}
	
	protected void createConnectors() throws DataSourceManagementException {
		switch (storage) {
			case DUMP:		this.dumpDBConnector = new DBConnector(DBType.SQLite, "", getDumpDBPath(), "", "");
							this.tempDBConnector = new DBConnector(DBType.SQLite, "", getTempDumpDBPath(), "", "");
							this.userDBConnector = new DBConnector(DBType.SQLite, "", getDBUserPath(), "", "");
							break;
							
			case USER:		this.dumpDBConnector = new DBConnector(DBType.SQLite, "", getDBUserPath(), "", "");
							break;
		}
	}
	
	public DBConnector getDumpDBConnector() {
		return dumpDBConnector;
	}
	
	public DBConnector getUserDBConnector() {
		return userDBConnector;
	}
	
	public String getDumpDBPath() {
		return DUMPS_DIR + File.separatorChar + dataSourceID + ".db";
	}
	
	public String getDBUserPath() {
		return USER_DIR + File.separatorChar + dataSourceID + ".db";
	}
	
	private String getTempDumpDBPath() {
		return getDumpDBPath() + ".temp";
	}

	public void reloadDatabase() throws DatabaseCreationException, DataSourceManagementException {
		checkDirs();
		
		updateDatabase();
	}
	
	private void checkDirs() {		
		Config.getInstance().createDataDir();
		
		final File dumpsDir = new File(DUMPS_DIR);
		if (!dumpsDir.exists()) {
			dumpsDir.mkdir();
		}
		
		final File userDir = new File(USER_DIR);
		if (!userDir.exists()) {
			userDir.mkdir();
		}
	}  
	
	private String getSQLDeleteData(DBConnector connector) throws DataSourceManagementException {
		StringBuilder strBuilder = new StringBuilder();
		
		for (String table : connector.getTables()) {
			strBuilder.append("DELETE FROM " + StringQuoter.quote(table) + ";");
		}
		
		return strBuilder.toString();
	}
	
	private void copyDumpDB() throws IOException, DataSourceManagementException {
		logger.debug("Copying dump DB " + getDumpDBConnector().getDatabase());
		
		DatabaseLock.getInstance().adquireWriteLock(getDumpDBPath());
		
		try {
			dumpDBConnector.closeAll();
			tempDBConnector.closeAll();
			
			final Path source = FileSystems.getDefault().getPath(getDumpDBPath());
			final Path dest = FileSystems.getDefault().getPath(getTempDumpDBPath());
			if (Files.exists(source)) {
				Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
			}
		} finally {
			DatabaseLock.getInstance().releaseWriteLock(getDumpDBPath());
		}
		
		logger.debug("Dump DB copied " + getDumpDBConnector().getDatabase());
	}
	
	private void replaceDumpDB() throws IOException, DataSourceManagementException {
		logger.debug("Replacing dump DB " + getDumpDBConnector().getDatabase());
		
		try {
			DatabaseLock.getInstance().adquireWriteLock(getDumpDBPath());
			
			tempDBConnector.closeAll();
			dumpDBConnector.closeAll();
			
			final Path dest = FileSystems.getDefault().getPath(getDumpDBPath());
			final Path source = FileSystems.getDefault().getPath(getTempDumpDBPath());
			Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(source);
		} finally {
			DatabaseLock.getInstance().releaseWriteLock(getDumpDBPath());
		}
		
		logger.debug("Dump DB replaced " + getDumpDBConnector().getDatabase());
	}
	
	protected void updateDatabase() throws DatabaseCreationException, DataSourceManagementException {
		switch (storage) {
			case DUMP:	updateDatabaseDump();
						break;
						
			case USER:	updateDatabaseUser();
						break;
		}
	}
	
	private void updateDatabaseDump() throws DatabaseCreationException {
		logger.debug("Updating dump DB " + getDumpDBConnector().getDatabase());
		
		try {
			copyDumpDB();
			
			updateTempDB();
			
			replaceDumpDB();
		} catch (Exception e) {
			throw new DatabaseCreationException("Cannot create database dump for JSON. " + e.getMessage(), e);
		} 
		
		logger.debug("Dump DB updated " + getDumpDBConnector().getDatabase());
	}

	private void updateTempDB() throws DataSourceManagementException, InvalidRootException, MalformedURLException, IOException, SQLException {
		logger.debug("Updating temp DB");
		
		tempDBConnector.beginTransaction();
		
		final JSONSchemaLoader loader = new JSONSchemaLoader(connector);			
		final SPARQLConnector schemaConnector = loader.loadSchema();
		final SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schemaConnector);
		
		createDumpDB(tempDBConnector, exporter, false);			
		createUserDB(exporter);
		deleteData(tempDBConnector);
		updateDumpData(tempDBConnector);
		
		tempDBConnector.commit();
		
		logger.debug("Temp DB updated");
	}
	
	private void updateDatabaseUser() throws DatabaseCreationException {		
		try {			
			dumpDBConnector.beginTransaction();
			
			final JSONSchemaLoader loader = new JSONSchemaLoader(connector);			
			final SPARQLConnector schemaConnector = loader.loadSchema();
			final SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schemaConnector);
			
			createDumpDB(dumpDBConnector, exporter, true);
			deleteData(dumpDBConnector);
			
			dumpDBConnector.commit();
			
		} catch (Exception e) {
			throw new DatabaseCreationException("Cannot create database dump for JSON. " + e.getMessage(), e);
		} 
	}
	
	private void deleteData(DBConnector dbConnector) throws DataSourceManagementException, SQLException {
		final String sqlDeleteData = getSQLDeleteData(dbConnector);
		dbConnector.executeUpdate(sqlDeleteData);						
	}

	private void updateDumpData(DBConnector dbConnector) throws DataSourceManagementException, SQLException, InvalidRootException, MalformedURLException, IOException {
		final JSONDataDumper dataDumper = new JSONDataDumper(connector, dbConnector);
		dataDumper.dumpData();
	}

	private void createUserDB(SPARQLSchemaExporter exporter) throws SQLException, DataSourceManagementException {
		userDBConnector.beginTransaction();
		if (userDBConnector.getTables().isEmpty()) {    	
	    	final String sqlCreateTables = exporter.getSQLCreateTables(true).trim();
			userDBConnector.executeUpdate(sqlCreateTables);
		}
		userDBConnector.commit();
	}

	private void createDumpDB(DBConnector dbConnector, SPARQLSchemaExporter exporter, boolean addUserRow) throws DataSourceManagementException, SQLException {		
		if (dbConnector.getTables().isEmpty() || append) {	    	
			final String sqlCreateTables = exporter.getSQLCreateTables(addUserRow).trim();
			dbConnector.executeUpdate(sqlCreateTables);
		}
	}

	public void setAppend(boolean append) {
		this.append = append;
	}
	
	public static void deleteData(String datasourceID) throws IOException {
		JSONDumper.deleteDB(DUMPS_DIR + File.separatorChar + datasourceID + ".db");
		JSONDumper.deleteDB(USER_DIR + File.separatorChar + datasourceID + ".db");
	}
	
	private static void deleteDB(String dbPath) throws IOException {
		DatabaseLock.getInstance().adquireWriteLock(dbPath);
		final File userFile = new File(dbPath);
		if (userFile.exists()) {
			PoolManager.getInstance().closeDataSource("jdbc:sqlite:" + dbPath);
			userFile.delete();
		}
		DatabaseLock.getInstance().releaseWriteLock(dbPath);
	}

	public boolean existsUserDB() {
		final File userDB = new File(getDBUserPath());
		return userDB.exists();
	}
}
