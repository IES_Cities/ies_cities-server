/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.query;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.csv.CSVConnector;
import eu.iescities.server.querymapper.datasource.csv.CSVDataSource;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.DBDataSource;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.JSONDataSource;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaDataSource;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;

public class QueriableFactory {

	public static SQLQueriable createQueriable(DataSourceConnector connector, String dataSourceID, boolean forceLoad) throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException {
		switch (connector.getType()) {
			case sparql:	return new SPARQLDataSource((SPARQLConnector) connector);
			
			case json:		return new JSONDataSource((JSONConnector) connector, dataSourceID, forceLoad);
			
			case database:	return new DBDataSource((DBConnector) connector, forceLoad);
			
			case csv:		return new CSVDataSource((CSVConnector) connector, dataSourceID, forceLoad);
			
			case json_schema:	return new JSONSchemaDataSource((JSONSchemaConnector) connector, dataSourceID);
			
			default:		throw new QueriableFactoryException("Unsupported connector type");
		}
	}
}
