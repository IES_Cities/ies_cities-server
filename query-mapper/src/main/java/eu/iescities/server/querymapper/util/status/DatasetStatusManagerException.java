package eu.iescities.server.querymapper.util.status;

@SuppressWarnings("serial")
public class DatasetStatusManagerException extends Exception {

	public DatasetStatusManagerException(String message) {
		super(message);
	}
}
