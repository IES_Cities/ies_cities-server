/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.update;

import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.query.SQLValidator;
import eu.iescities.server.querymapper.datasource.database.update.TransactionParser.SQLStatement;
import eu.iescities.server.querymapper.datasource.json.DatabaseLock;
import eu.iescities.server.querymapper.datasource.json.JSONUpdateProcessor;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.update.JSONTranslator;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class DBDataSourceUpdater {

	private final DBConnector connector;
	private final DataSourceSecurityManager manager;
	
	public DBDataSourceUpdater(DBConnector connector, DataSourceSecurityManager manager) {
		this.connector = connector;
		this.manager = manager;
	}
	
	public int executeSQLUpdate(String sqlUpdate, Permissions permissions, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final SQLValidator sqlValidator = new SQLValidator();
		
		if (!sqlValidator.isValid(sqlUpdate)) {
			throw new DataSourceManagementException("Could not execute SQL update. Invalid statement.");
		}
		
		try (final DBConnector open = connector.open()) {
			DatabaseLock.getInstance().adquireWriteLock(connector.getConnectionURL());
			switch (sqlValidator.getStmtType(sqlUpdate)) {
				case INSERT:	final int insertedRows = open.executeInsert(sqlUpdate, permissions, manager, userid);
								return insertedRows;
					
				case DELETE:	final int deletedRows = open.executeDelete(sqlUpdate, permissions, manager, userid);
								return deletedRows;
					
				case UPDATE:	final int updatedRows = open.executeUpdate(sqlUpdate, permissions, manager, userid);
								return updatedRows;
					
				default:		throw new DataSourceManagementException("Invalid update statement. Only INSERT, DELETE or UPDATE are supported");
			}
		} finally {
			DatabaseLock.getInstance().releaseWriteLock(connector.getConnectionURL());
		} 
	}
	
	public int executeJSONUpdate(String json, String userid, Permissions permissions) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException {		
		final JSONTranslator translator = new JSONTranslator();
		final UpdateBlock updateBlock = translator.translateJSON(json);
		
		DatabaseLock.getInstance().adquireWriteLock(connector.getConnectionURL());
		
		try {
			final JSONUpdateProcessor processor = new JSONUpdateProcessor(connector, manager, false);
			return processor.processUpdateBlock(updateBlock, userid, permissions);
		} finally {
			DatabaseLock.getInstance().releaseWriteLock(connector.getConnectionURL());
		}
	}

	public int executeSQLTransactionUpdate(String text, Permissions permissions, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final TransactionParser parser = new TransactionParser();
		
		final List<SQLStatement> stmts = parser.parse(text);
		
		if (stmts.isEmpty()) {
			throw new DataSourceManagementException("Could not parse transaction statements.");
		}
		
		try {
			connector.beginTransaction();
			
			DatabaseLock.getInstance().adquireWriteLock(connector.getConnectionURL());
			
			int modifiedRows = 0;			
			for (SQLStatement stmt : stmts) {
				switch (stmt.getType()) {
					case INSERT:
						final int insertedRows = connector.executeInsert(stmt.getSql(), permissions, manager, userid);
						modifiedRows += insertedRows;
						break;
						
					case DELETE:
						final int deletedRows = connector.executeDelete(stmt.getSql(), permissions, manager, userid);
						modifiedRows += deletedRows;
						break;
						
					case UPDATE:		
						final int updatedRows = connector.executeUpdate(stmt.getSql(), permissions, manager, userid);
						modifiedRows += updatedRows;
						break;
						
					default: 
						throw new DataSourceManagementException("Invalid update statement. Only INSERT, DELETE or UPDATE are supported");
				}
			}
			
			connector.commit();
			
			return modifiedRows;
		} catch (DataSourceManagementException e) {
			connector.rollback();
			throw new DataSourceManagementException("Could not execute transaction statements: rolling back. " + e.getMessage());
		} finally {
			DatabaseLock.getInstance().releaseWriteLock(connector.getConnectionURL());
		}
	}
}
