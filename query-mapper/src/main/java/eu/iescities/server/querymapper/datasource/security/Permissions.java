/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class Permissions {

	public enum AccessType { ALL, AUTH, OWNER, NONE, USER, DEFAULT };   
	
	public enum ActionType { SELECT, DELETE, INSERT, UPDATE };
	
	@Expose
	private List<TablePermission> select;
	
	@Expose
	private List<TablePermission> insert;
	
	@Expose
	private List<TablePermission> delete;
	
	@Expose
	private List<TablePermission> update;
	
	public Permissions() {
		select = new ArrayList<TablePermission>();
		delete = new ArrayList<TablePermission>();
		insert = new ArrayList<TablePermission>();
		update = new ArrayList<TablePermission>();
	}
	
	public List<TablePermission> getSelect() {
		return select;
	}

	public void setSelect(List<TablePermission> select) {
		this.select = select;
	}

	public List<TablePermission> getInsert() {
		return insert;
	}

	public void setInsert(List<TablePermission> insert) {
		this.insert = insert;
	}

	public List<TablePermission> getDelete() {
		return delete;
	}

	public void setDelete(List<TablePermission> delete) {
		this.delete = delete;
	}

	public List<TablePermission> getUpdate() {
		return update;
	}

	public void setUpdate(List<TablePermission> update) {
		this.update = update;
	}

	public List<TablePermission> getSelectPermissions() {
		return select;
	}
	
	public List<TablePermission> getDeletePermissions() {
		return delete;
	}
	
	public List<TablePermission> getInsertPermissions() {
		return insert;
	}
	
	public List<TablePermission> getUpdatePermissions() {
		return update;
	}

	public void addSelectPermission(TablePermission tablePermission) {
		select.add(tablePermission);		
	}
	
	public void addDeletePermission(TablePermission tablePermission) {
		delete.add(tablePermission);
	}
	
	public void addInsertPermission(TablePermission tablePermission) {
		insert.add(tablePermission);
	}
	
	public void addUpdatePermission(TablePermission tablePermission) {
		update.add(tablePermission);
	}
	
	private AccessType findPermission(List<TablePermission> actionPermissions, String table) {
		for (TablePermission tablePermission : actionPermissions) {
			if (tablePermission.getTable().equals(table)) {
				return tablePermission.getAccess();
			}
		}
		
		return AccessType.DEFAULT;
	}
	
	private List<String> findUserList(List<TablePermission> actionPermissions, String table) {
		for (TablePermission tablePermission : actionPermissions) {
			if (tablePermission.getTable().equals(table)) {
				return tablePermission.getUsers();
			}
		}
		
		return Collections.emptyList();
	}
	
	public AccessType getPermission(ActionType action, String table) {
		AccessType access = AccessType.NONE;
		
		final DefaultPermissions defaultPermissions = SecurityFactory.getInstance().getDefaultPermissions();
		switch (action) {
			case SELECT:	access = findPermission(select, table);
							if (access == AccessType.DEFAULT) {
								return defaultPermissions.getSelectDefaultPermission();
							}
							break; 
			
			case DELETE:	access = findPermission(delete, table);
							if (access == AccessType.DEFAULT) {
								return defaultPermissions.getDeleteDefaultPermission();
							}
							break;
			
			case INSERT:	access = findPermission(insert, table);
							if (access == AccessType.DEFAULT) {
								return defaultPermissions.getInsertDefaultPermission();
							}
							break;
							
			case UPDATE:	access = findPermission(update, table);
							if (access == AccessType.DEFAULT) {
								return defaultPermissions.getUpdateDefaultPermission();
							}
							break;
		}
		
		return access;
	}
	
	public List<String> getUserList(ActionType action, String table) {
		switch (action) {
			case SELECT:	return findUserList(select, table);
			
			case DELETE:	return findUserList(delete, table);
			
			case INSERT:	return findUserList(insert, table);	
			
			case UPDATE:	return findUserList(update, table);
		}
		
		return Collections.emptyList();
	}
	
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.setDateFormat("dd/MM/yyyy HH:mm:ss").setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}

	public boolean isEmpty() {
		return select.isEmpty() && delete.isEmpty() && insert.isEmpty() && update.isEmpty();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Permissions)) {
			return false;
		}
		
		final Permissions permissions = (Permissions) o;
		return this.select.equals(permissions.select)
				&& this.insert.equals(permissions.insert)
				&& this.delete.equals(permissions.delete)
				&& this.update.equals(permissions.update);
	}
}
 