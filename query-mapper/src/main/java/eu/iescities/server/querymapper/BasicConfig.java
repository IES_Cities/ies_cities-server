package eu.iescities.server.querymapper;

import java.util.Properties;

public abstract class BasicConfig {
	
	public BasicConfig() {
		
	}
	
	protected String getProperty(Properties p, String propertyName, String current) {
		String value = p.getProperty(propertyName);
		if (value == null || value.isEmpty()) {
			value = current;
		}
		System.out.println(String.format("%s=%s", propertyName, value));
		return value;
	}
	
	protected int getProperty(Properties p, String propertyName, int current) {
		String value = p.getProperty(propertyName);
		
		int intValue;
		if (value == null || value.isEmpty()) {
			intValue = current;
		} else {
			intValue = Integer.parseInt(value);
		}
		
		System.out.println(String.format("%s=%d", propertyName, intValue));
		return intValue;
	}
}
