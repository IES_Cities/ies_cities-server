package eu.iescities.server.querymapper.datasource.json;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

public class DatabaseLock {
	
	final static Logger logger = Logger.getLogger(DatabaseLock.class);

	private static DatabaseLock instance = null;
	
	private final ConcurrentHashMap<String, ReadWriteLock> locks = new ConcurrentHashMap<String, ReadWriteLock>();
	
	private DatabaseLock() {
		
	}
	
	public static DatabaseLock getInstance() {
		if (instance == null) {
			instance = new DatabaseLock();
		}
		return instance;
	}
	
	public void adquireReadLock(String id) {
		logger.debug("Adquiring read lock " + id);
				
		locks.putIfAbsent(id, new ReentrantReadWriteLock());
		
		locks.get(id).readLock().lock();
		
		logger.debug("Read lock adquired " + id);
	}
	
	public void releaseReadLock(String id) {
		logger.debug("Releasing read lock " + id);
		
		locks.get(id).readLock().unlock();
		
		logger.debug("Read lock released " + id);
	}
	
	public void adquireWriteLock(String id) {
		logger.debug("Adquiring write lock " + id);
				
		locks.putIfAbsent(id, new ReentrantReadWriteLock());
		
		locks.get(id).writeLock().lock();
		
		logger.debug("Write lock adquired " + id);
	}
	
	public void releaseWriteLock(String id) {
		logger.debug("Releasing write lock " + id);
		
		locks.get(id).writeLock().unlock();
		
		logger.debug("Read write released " + id);
	}
}
