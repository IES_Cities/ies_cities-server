/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.query;

import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.json.DatabaseLock;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;

public class DBDataSourceQuerier {
	
	private final DBConnector connector;
	private final DataSourceSecurityManager manager;
	
	private long executionTime;
	
	public DBDataSourceQuerier(DBConnector connector, DataSourceSecurityManager manager) {
		this.connector = connector;
		this.manager = manager;
	}

	public ResultSet executeSQLSelect(String sqlQuery, Permissions permissions, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {		
		final long startTime = System.currentTimeMillis();
		
		final SQLValidator sqlValidator = new SQLValidator();
		if (!sqlValidator.isValid(sqlQuery)) {
			throw new DataSourceManagementException("Could not execute SQL select. Invalid statement.");
		}
		
		try (final DBConnector open = connector.open()) {
			DatabaseLock.getInstance().adquireReadLock(connector.getConnectionURL());
			final ResultSet resultSet = open.executeQuery(sqlQuery, permissions, manager, userid);
					
			executionTime = System.currentTimeMillis() - startTime;
			
			return resultSet;
		} finally {
			DatabaseLock.getInstance().releaseReadLock(connector.getConnectionURL());
		}
	}
	
	public long getExecutionTime() {
		return executionTime;
	}
	
	public ResultSet getTables() throws DataSourceManagementException {
		try (final DBConnector open = connector.open()) {
			final ResultSet resultSet = new ResultSet();
			for (String table : open.getTables()) {
				final Row row = new Row();
				row.addColumn("Tables", table);
				resultSet.addRow(row);
			}		
			return resultSet;
		} catch (Exception e) {
			throw new DataSourceManagementException("Problem with SQL execution." + e.getMessage(), e.getCause());
		}
	}
	
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		try (final DBConnector open = connector.open()) {
			return open.getInfo(); 
		}
	}

	public String createQueryAll() throws DataSourceManagementException {
		try (final DBConnector open = connector.open()) {
			final String query = open.createQueryAll();			
			return query;
		}
	}
	
	public JsonObject queryAllCompact(Permissions p, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		try (final DBConnector open = connector.open()) {
			final JsonObject result = open.selectAllCompact(p, manager, userid);		
			return result;
		}
	}
}
