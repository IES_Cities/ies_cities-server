package eu.iescities.server.querymapper.datasource.security;

import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;

public class DefaultPermissions {

	public DefaultPermissions() {
		
	}
	
	public AccessType getSelectDefaultPermission() {
		return AccessType.ALL;
	}
	
	public AccessType getDeleteDefaultPermission() {
		return AccessType.ALL;
	}
	
	
	public AccessType getInsertDefaultPermission() {
		return AccessType.ALL;
	}
	
	public AccessType getUpdateDefaultPermission() {
		return AccessType.ALL;
	}
}
