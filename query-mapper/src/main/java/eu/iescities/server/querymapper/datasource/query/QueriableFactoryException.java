package eu.iescities.server.querymapper.datasource.query;

@SuppressWarnings("serial")
public class QueriableFactoryException extends Exception {

	public QueriableFactoryException(String message) {
		super(message);
	}
}
