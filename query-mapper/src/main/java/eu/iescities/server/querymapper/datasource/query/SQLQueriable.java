/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.query;

import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSource;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;

public interface SQLQueriable extends DataSource {
	
	public static enum DataOrigin { ORIGIN, USER, ANY };
	
	public ResultSet getTables() throws DataSourceManagementException;
	
	public ResultSet executeSQLSelect(String query, String userid, DataOrigin dataOrigin) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException;

	public long getExecutionTime();
	
	public String createQueryAll() throws DataSourceManagementException;
	
	public ResultSet queryAll(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException;
	
	public JsonObject queryAllJson(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException; 
}
