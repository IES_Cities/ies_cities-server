// Generated from SQLSelect.g by ANTLR 4.1
 
package eu.iescities.server.querymapper.sql.parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLSelectParser}.
 */
public interface SQLSelectListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull SQLSelectParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull SQLSelectParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void enterOrderByItem(@NotNull SQLSelectParser.OrderByItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void exitOrderByItem(@NotNull SQLSelectParser.OrderByItemContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#nestedCondition}.
	 * @param ctx the parse tree
	 */
	void enterNestedCondition(@NotNull SQLSelectParser.NestedConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#nestedCondition}.
	 * @param ctx the parse tree
	 */
	void exitNestedCondition(@NotNull SQLSelectParser.NestedConditionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#isNull}.
	 * @param ctx the parse tree
	 */
	void enterIsNull(@NotNull SQLSelectParser.IsNullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#isNull}.
	 * @param ctx the parse tree
	 */
	void exitIsNull(@NotNull SQLSelectParser.IsNullContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#orderBy}.
	 * @param ctx the parse tree
	 */
	void enterOrderBy(@NotNull SQLSelectParser.OrderByContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#orderBy}.
	 * @param ctx the parse tree
	 */
	void exitOrderBy(@NotNull SQLSelectParser.OrderByContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#allColumns}.
	 * @param ctx the parse tree
	 */
	void enterAllColumns(@NotNull SQLSelectParser.AllColumnsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#allColumns}.
	 * @param ctx the parse tree
	 */
	void exitAllColumns(@NotNull SQLSelectParser.AllColumnsContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(@NotNull SQLSelectParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(@NotNull SQLSelectParser.ExpressionListContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#subSelect}.
	 * @param ctx the parse tree
	 */
	void enterSubSelect(@NotNull SQLSelectParser.SubSelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#subSelect}.
	 * @param ctx the parse tree
	 */
	void exitSubSelect(@NotNull SQLSelectParser.SubSelectContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#conditionList}.
	 * @param ctx the parse tree
	 */
	void enterConditionList(@NotNull SQLSelectParser.ConditionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#conditionList}.
	 * @param ctx the parse tree
	 */
	void exitConditionList(@NotNull SQLSelectParser.ConditionListContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#from}.
	 * @param ctx the parse tree
	 */
	void enterFrom(@NotNull SQLSelectParser.FromContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#from}.
	 * @param ctx the parse tree
	 */
	void exitFrom(@NotNull SQLSelectParser.FromContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#date}.
	 * @param ctx the parse tree
	 */
	void enterDate(@NotNull SQLSelectParser.DateContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#date}.
	 * @param ctx the parse tree
	 */
	void exitDate(@NotNull SQLSelectParser.DateContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#where}.
	 * @param ctx the parse tree
	 */
	void enterWhere(@NotNull SQLSelectParser.WhereContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#where}.
	 * @param ctx the parse tree
	 */
	void exitWhere(@NotNull SQLSelectParser.WhereContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#columnRef}.
	 * @param ctx the parse tree
	 */
	void enterColumnRef(@NotNull SQLSelectParser.ColumnRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#columnRef}.
	 * @param ctx the parse tree
	 */
	void exitColumnRef(@NotNull SQLSelectParser.ColumnRefContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(@NotNull SQLSelectParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(@NotNull SQLSelectParser.FunctionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary(@NotNull SQLSelectParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary(@NotNull SQLSelectParser.UnaryContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#nestedExpression}.
	 * @param ctx the parse tree
	 */
	void enterNestedExpression(@NotNull SQLSelectParser.NestedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#nestedExpression}.
	 * @param ctx the parse tree
	 */
	void exitNestedExpression(@NotNull SQLSelectParser.NestedExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#tableName}.
	 * @param ctx the parse tree
	 */
	void enterTableName(@NotNull SQLSelectParser.TableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#tableName}.
	 * @param ctx the parse tree
	 */
	void exitTableName(@NotNull SQLSelectParser.TableNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(@NotNull SQLSelectParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(@NotNull SQLSelectParser.ValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#columnName}.
	 * @param ctx the parse tree
	 */
	void enterColumnName(@NotNull SQLSelectParser.ColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#columnName}.
	 * @param ctx the parse tree
	 */
	void exitColumnName(@NotNull SQLSelectParser.ColumnNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#between}.
	 * @param ctx the parse tree
	 */
	void enterBetween(@NotNull SQLSelectParser.BetweenContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#between}.
	 * @param ctx the parse tree
	 */
	void exitBetween(@NotNull SQLSelectParser.BetweenContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#limit}.
	 * @param ctx the parse tree
	 */
	void enterLimit(@NotNull SQLSelectParser.LimitContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#limit}.
	 * @param ctx the parse tree
	 */
	void exitLimit(@NotNull SQLSelectParser.LimitContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#alias}.
	 * @param ctx the parse tree
	 */
	void enterAlias(@NotNull SQLSelectParser.AliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#alias}.
	 * @param ctx the parse tree
	 */
	void exitAlias(@NotNull SQLSelectParser.AliasContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(@NotNull SQLSelectParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(@NotNull SQLSelectParser.ConditionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#select}.
	 * @param ctx the parse tree
	 */
	void enterSelect(@NotNull SQLSelectParser.SelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#select}.
	 * @param ctx the parse tree
	 */
	void exitSelect(@NotNull SQLSelectParser.SelectContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#tableRef}.
	 * @param ctx the parse tree
	 */
	void enterTableRef(@NotNull SQLSelectParser.TableRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#tableRef}.
	 * @param ctx the parse tree
	 */
	void exitTableRef(@NotNull SQLSelectParser.TableRefContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#comparator}.
	 * @param ctx the parse tree
	 */
	void enterComparator(@NotNull SQLSelectParser.ComparatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#comparator}.
	 * @param ctx the parse tree
	 */
	void exitComparator(@NotNull SQLSelectParser.ComparatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#like}.
	 * @param ctx the parse tree
	 */
	void enterLike(@NotNull SQLSelectParser.LikeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#like}.
	 * @param ctx the parse tree
	 */
	void exitLike(@NotNull SQLSelectParser.LikeContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#columnList}.
	 * @param ctx the parse tree
	 */
	void enterColumnList(@NotNull SQLSelectParser.ColumnListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#columnList}.
	 * @param ctx the parse tree
	 */
	void exitColumnList(@NotNull SQLSelectParser.ColumnListContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(@NotNull SQLSelectParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(@NotNull SQLSelectParser.ComparisonContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull SQLSelectParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull SQLSelectParser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#groupBy}.
	 * @param ctx the parse tree
	 */
	void enterGroupBy(@NotNull SQLSelectParser.GroupByContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#groupBy}.
	 * @param ctx the parse tree
	 */
	void exitGroupBy(@NotNull SQLSelectParser.GroupByContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#item}.
	 * @param ctx the parse tree
	 */
	void enterItem(@NotNull SQLSelectParser.ItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#item}.
	 * @param ctx the parse tree
	 */
	void exitItem(@NotNull SQLSelectParser.ItemContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#itemList}.
	 * @param ctx the parse tree
	 */
	void enterItemList(@NotNull SQLSelectParser.ItemListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#itemList}.
	 * @param ctx the parse tree
	 */
	void exitItemList(@NotNull SQLSelectParser.ItemListContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#tableAlias}.
	 * @param ctx the parse tree
	 */
	void enterTableAlias(@NotNull SQLSelectParser.TableAliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#tableAlias}.
	 * @param ctx the parse tree
	 */
	void exitTableAlias(@NotNull SQLSelectParser.TableAliasContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#having}.
	 * @param ctx the parse tree
	 */
	void enterHaving(@NotNull SQLSelectParser.HavingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#having}.
	 * @param ctx the parse tree
	 */
	void exitHaving(@NotNull SQLSelectParser.HavingContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#fromItem}.
	 * @param ctx the parse tree
	 */
	void enterFromItem(@NotNull SQLSelectParser.FromItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#fromItem}.
	 * @param ctx the parse tree
	 */
	void exitFromItem(@NotNull SQLSelectParser.FromItemContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(@NotNull SQLSelectParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(@NotNull SQLSelectParser.FunctionNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link SQLSelectParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(@NotNull SQLSelectParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLSelectParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(@NotNull SQLSelectParser.LiteralContext ctx);
}