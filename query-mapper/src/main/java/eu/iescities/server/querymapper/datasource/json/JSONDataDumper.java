package eu.iescities.server.querymapper.datasource.json;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class JSONDataDumper {
	
	private final JSONConnector jsonConnector;
	private final DBConnector dbConnector;
	private final KeyGenerator keyGenerator;
	
	public JSONDataDumper(JSONConnector connector, DBConnector dbConnector) {
		this.jsonConnector = connector;
		this.dbConnector = dbConnector;
		this.keyGenerator = new KeyGenerator();
	}

	public void dumpData() throws InvalidRootException, SQLException, DataSourceManagementException, MalformedURLException, IOException {	
    	final String tableName = StringQuoter.quote(JSONSchemaLoader.getRootTableName(jsonConnector));
    	
    	final boolean primaryKey = jsonConnector.getKey().equals(JSONSchemaLoader.TABLE_ID);
	
    	try (final JSONProcessor jsonProcessor = new JSONProcessor(jsonConnector.getReader(), jsonConnector.getRoot())) {
    		while (jsonProcessor.hasNext()) {
    			final JsonObject obj = jsonProcessor.next();
    			processElement(obj, tableName, "", primaryKey);
    		}
    	}
	}
	
	private void dumpArrayList(JsonArray array, String tableName, String parentID, boolean primaryKey) throws SQLException, DataSourceManagementException {		
		for (JsonElement e : array) {
			processElement(e, tableName, parentID, primaryKey);
		}
	}

	public void processElement(JsonElement e, String tableName, String parentID, boolean primaryKey) throws SQLException, DataSourceManagementException {
		if (e.isJsonObject()) {
			dumpTable(e.getAsJsonObject(), tableName, parentID, primaryKey);
		} else {		
			final Map<String, Literal> row = new HashMap<String, Literal>();
			
			if (!parentID.isEmpty()) {
				final String newKey = new Integer(keyGenerator.next(tableName)).toString();
				row.put(JSONSchemaLoader.TABLE_ID, new Literal(newKey, LiteralType.STRING));
				row.put(JSONSchemaLoader.PARENT_ID_COLUMN, new Literal(parentID, LiteralType.STRING));
			}
			
			final String[] names = StringQuoter.unquote(tableName).split(JSONSchemaLoader.NAME_SEPARATOR);
			
			Literal literal = null;
			if (e.isJsonPrimitive()) {
				literal = new Literal(e.getAsJsonPrimitive());
			} else if (e.isJsonArray()) {
				literal = new Literal("@" + e.getAsJsonArray().toString(), LiteralType.STRING);
			}
			
			row.put(StringQuoter.quote(names[1]), literal);
			
			final String insert = createInsert(row, tableName);
			dbConnector.executeUpdate(insert);
		}
	}

	private void dumpTable(JsonObject jsonObject, String tableName, String parentID, boolean primaryKey) throws SQLException, DataSourceManagementException {
		final List<Node> connectedObjs = new ArrayList<Node>();
		
		final Map<String, Literal> rowColumns = new HashMap<String, Literal>();
		
		if (!parentID.isEmpty()) {
			rowColumns.put(JSONSchemaLoader.PARENT_ID_COLUMN, new Literal(parentID, LiteralType.STRING));
		} else if (primaryKey) {
			rowColumns.put(JSONSchemaLoader.TABLE_ID, null);
		}
				
		for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
			final String key = StringQuoter.quote(entry.getKey());
			final JsonElement value = entry.getValue();
			if (value.isJsonArray() || value.isJsonObject()) {
				connectedObjs.add(new Node(value, key));
			} else {							
				if (value.isJsonPrimitive()) {
					rowColumns.put(key, new Literal(value.getAsJsonPrimitive()));
				} if (value.isJsonNull()) {
					rowColumns.put(key, new Literal("null", LiteralType.STRING));
				}
			}
		}
		
		String insertedKey = "";
		if (!rowColumns.isEmpty()) {
			if (!parentID.isEmpty() || primaryKey) {
				insertedKey = new Integer(keyGenerator.next(tableName)).toString();
				rowColumns.put(JSONSchemaLoader.TABLE_ID, new Literal(insertedKey, LiteralType.STRING));
			} else {
				insertedKey = rowColumns.get(StringQuoter.quote(jsonConnector.getKey())).getValue();
			}
			
			final String insert = createInsert(rowColumns, tableName);
			dbConnector.executeUpdate(insert);
		}
		
		for (Node node : connectedObjs) {
			if (node.getJsonElement().isJsonArray()) {
				dumpArrayList(node.getJsonElement().getAsJsonArray(), StringQuoter.quote(StringQuoter.unquote(tableName) + JSONSchemaLoader.NAME_SEPARATOR + StringQuoter.unquote(node.getName())), insertedKey, false);
			} else if (node.getJsonElement().isJsonObject()) {
				dumpTable(node.getJsonElement().getAsJsonObject(),  StringQuoter.quote(StringQuoter.unquote(tableName) + JSONSchemaLoader.NAME_SEPARATOR + StringQuoter.unquote(node.getName())), insertedKey, false);
			}
		}
	}
	
	private String createInsert(Map<String, Literal> rowColumns, String tableName) {
		final StringBuilder strBuilder = new StringBuilder();
		
		strBuilder.append("INSERT INTO " + tableName + " (");
		
		boolean first = true;
		for (String c : rowColumns.keySet()) {
			if (!first) {
				strBuilder.append(", ");
			} else {
				first = false;
			}
			strBuilder.append(c);
		}
		
		strBuilder.append(") VALUES (");
		
		first = true;
		for (Literal v : rowColumns.values()) {
			if (!first) {
				strBuilder.append(", ");
			} else {
				first = false;
			}
			
			if (v == null) {
				strBuilder.append("null");
			} else {
				switch (v.getType()) {
					case STRING:
					case DATE:
						strBuilder.append("'" + v.getValue().replaceAll("'", "''") + "'");
						break;
						
					default:
						strBuilder.append(v.getValue());
				}
			}
		}
		
		strBuilder.append(");");
				
		return strBuilder.toString();
	}
}
