/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.update.Update;
import eu.iescities.server.querymapper.datasource.database.update.UpdateBlock;
import eu.iescities.server.querymapper.datasource.database.update.UpdateSection;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class JSONUpdateProcessor {

	private final DBConnector connector;
	private final boolean verbose;
	private final DataSourceSecurityManager manager;
	
	public JSONUpdateProcessor(DBConnector connector, DataSourceSecurityManager manager, boolean verbose) {
		this.connector = connector;
		this.manager = manager;
		this.verbose = verbose;
	}
	
	public int processUpdateBlock(UpdateBlock updateBlock, String userid, Permissions p) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		try {
			connector.beginTransaction();
			
			final int deletedRows = processDeletes(userid, p, updateBlock);
			final int insertedRows = processInserts(userid, p, updateBlock);
			
			connector.commit();
			
			return deletedRows + insertedRows;
		} catch (DataSourceManagementException e) {
			connector.rollback();
			throw new DataSourceManagementException("Rollback of transactional update due to execution errors", e);
		}
	}
	
	private int processInserts(String userid, Permissions p, UpdateBlock updateBlock) throws DataSourceSecurityManagerException, DataSourceManagementException {
		int updatedRows = 0;
		for (UpdateSection section : updateBlock.getInserts()) {
			for (Update insert : section.getUpdates()) {
				if (verbose) {
					System.out.println("-------------------------------------------------------");
					System.out.println("Executing SQL insert:");
					System.out.println(insert);
					System.out.println("");
					System.out.println("-------------------------------------------------------");
					System.out.println();
				}
				 
				updatedRows += connector.executeInsert(insert.getSQL(), p, manager, userid);
			}
		}
		return updatedRows;
	}
	
	private int processDeletes(String userid, Permissions p, UpdateBlock updateBlock) throws DataSourceSecurityManagerException, DataSourceManagementException {
		int updatedRows = 0;
		for (UpdateSection section : updateBlock.getDeletes()) {
			for (Update delete : section.getUpdates()) {
				if (verbose) {
					System.out.println("-------------------------------------------------------");
					System.out.println("Executing SQL delete:");
					System.out.println(delete);
					System.out.println("");
					System.out.println("-------------------------------------------------------");
					System.out.println();
				}
												
				updatedRows += connector.executeDelete(delete.getSQL(), p, manager, userid);
			}
		}
		
		return updatedRows;
	}
}
