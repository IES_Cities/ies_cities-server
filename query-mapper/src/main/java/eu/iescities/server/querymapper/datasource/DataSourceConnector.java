/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.security.Permissions;

public class DataSourceConnector {
	
	public enum ConnectorType { database, sparql, httpapi, json, csv, json_schema, not_available } 

	@Expose
	protected ConnectorType mapping;
	
	@Expose
	protected Permissions permissions = new Permissions();
	
	public DataSourceConnector() {
		this.mapping = ConnectorType.not_available;
	}
	
	public DataSourceConnector(ConnectorType type) {
		this.mapping = type;
	}
	
	public ConnectorType getMapping() {
		return mapping;
	}

	public void setMapping(ConnectorType mapping) {
		this.mapping = mapping;
	}

	public ConnectorType getType() {
		return mapping;
	}
	
	public Permissions getPermissions() {
		return permissions;
	}

	public int getRefresh() {
		return Integer.MAX_VALUE;
	}
	
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		return new DataSourceInfo();
	}
	
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.setDateFormat("dd/MM/yyyy HH:mm:ss").setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}

	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DataSourceConnector)) {
			return false;
		}
		
		final DataSourceConnector connector = (DataSourceConnector) o;
		return this.mapping == connector.mapping && this.permissions.equals(connector.permissions);
	}
}
