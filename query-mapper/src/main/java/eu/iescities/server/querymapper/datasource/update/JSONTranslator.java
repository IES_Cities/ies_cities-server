/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.update;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;

import eu.iescities.server.querymapper.datasource.database.update.Update;
import eu.iescities.server.querymapper.datasource.database.update.UpdateBlock;
import eu.iescities.server.querymapper.datasource.database.update.UpdateSection;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class JSONTranslator {

	interface UpdateGenerator {
		
		public Update createUpdate(String table, Set<?> entrySet) throws DataSourceSecurityManagerException;
	}
	
	class InsertGenerator implements UpdateGenerator {
		
		@Override
		public Update createUpdate(String table, Set<?> entrySet) throws DataSourceSecurityManagerException {
			final String insertSQLTemplate = "INSERT INTO %s %s VALUES %s";
			
			final StringBuilder columns = new StringBuilder();
			columns.append("(");
			
			final StringBuilder values = new StringBuilder();
			values.append("(");
			
			final String key = "";
			String idValue = "";
			
			int count = 0;
			for (Object o : entrySet) {
				final Entry<?, ?> entry = (Entry<?, ?>)o;
				if (count > 0) {
					columns.append(", ");
					values.append(", ");
				}
				
				columns.append(entry.getKey());
				
				Object value = entry.getValue();
				if (value instanceof String) {
					if (((String) value).toLowerCase().equals("null")) {
						values.append("null");
						if (entry.getKey().equals(key)) {
							idValue = "null";
						}
					} else {
						values.append("'" + value + "'");
						if (entry.getKey().equals(key)) {
							idValue = value.toString();
						}
					}
				} else if (value instanceof Double) {
					final Double d = (Double) value;
					if (d == d.intValue()) {
						values.append(d.intValue());
						if (entry.getKey().equals(key)) {
							idValue = d.intValue() + "";
						}
					}
				}
				else {
					values.append(value);
				}
				
				count++;
			}
			
			columns.append(")");
			values.append(")");
			
			final String sql = String.format(insertSQLTemplate, table, columns.toString(), values.toString());
			return new Update(sql, key, idValue);
		}
		
	}
	
	class DeleteGenerator implements UpdateGenerator {
		
		@Override
		public Update createUpdate(String table, Set<?> entrySet)
				throws DataSourceSecurityManagerException {
			final String deleteSQLTemplate = "DELETE FROM %s WHERE %s";
			
			final String key = "";
			String idValue = "";
			
			int count = 0;
			final StringBuilder where = new StringBuilder();
			for (Object o : entrySet) {
				if (count > 0) {
					where.append(" AND ");
				}
				
				final Entry<?, ?> entry = (Entry<?, ?>)o;
				
				Object value = entry.getValue();
				
				if (value instanceof String) {
					if (entry.getKey().equals(key)) {
						idValue = value.toString();
					}
					where.append(entry.getKey() + " = '" + value.toString() + "'");
				} else if (value instanceof Double) {
					final Double d = (Double) value;
					if (d == d.intValue()) {
						if (entry.getKey().equals(key)) {
							idValue = d.intValue() + "";
						}
						where.append(entry.getKey() + " = " + d.intValue());
					}
				} else {
					if (entry.getKey().equals(key)) {
						idValue = value.toString();
					}
					where.append(entry.getKey() + " = " + value.toString());
				}
				
				count++;
			}
			
			final String sql = String.format(deleteSQLTemplate, table, where.toString());
			return new Update(sql, key, idValue);
		}
	}
		
	private UpdateSection translateJSON(Map<?, ?> root, UpdateGenerator generator) throws DataSourceSecurityManagerException, TranslationException {
		if (root.containsKey("table")) {
			final String table = (String) root.get("table");
			if (root.containsKey("rows")) {
				final Object rowsObject = root.get("rows");
				if (rowsObject instanceof ArrayList) {
					final ArrayList<?> rows = (ArrayList<?>) rowsObject;
					final UpdateSection updatedData = new UpdateSection(table);
					for (Object o : rows) {
						if (o instanceof Map) {
							final Map<?, ?> map = (Map<?, ?>) o;
							final Update update = generator.createUpdate(table, map.entrySet());								
							updatedData.addUpdate(update);
						}
					}
					
					return updatedData;
				}
			} 
		}
			
		throw new TranslationException("Could not transform JSON. Invalid JSON structure"); 
	}
	
	private List<UpdateSection> translateJSONInsertPart(ArrayList<?> inserts) throws TranslationException, DataSourceSecurityManagerException {		
		final List<UpdateSection> updates = new ArrayList<UpdateSection>();
		
		for (Object o : inserts) {
			final UpdateSection updatedData = translateJSON((Map<?, ?>) o, new InsertGenerator());
			updates.add(updatedData);
		}
		
		return updates;
	}
	
	private List<UpdateSection> translateJSONDeletePart(ArrayList<?> deletes) throws TranslationException, DataSourceSecurityManagerException {		
		final List<UpdateSection> updates = new ArrayList<UpdateSection>();
		
		for (Object delete : deletes) {
			final UpdateSection updatedData = translateJSON((Map<?, ?>) delete, new DeleteGenerator());
			updates.add(updatedData);
		}
		
		return updates;
	}
	
	public UpdateBlock translateJSON(String json) throws TranslationException, DataSourceSecurityManagerException {
		final Object jsonObject = new Gson().fromJson(json, Object.class);
		
		List<UpdateSection> deletes = Collections.emptyList();
		List<UpdateSection> inserts = Collections.emptyList();
		
		if (jsonObject instanceof Map) {
			final Map<?, ?> root = (Map<?, ?>) jsonObject;
			if (root.containsKey("deletes")) {
				final Object deletesObject = root.get("deletes");
				if (deletesObject instanceof ArrayList) {
					deletes = translateJSONDeletePart((ArrayList<?>) deletesObject);
				}
			}
			
			if (root.containsKey("inserts")) {
				final Object insertsObject = root.get("inserts");
				if (insertsObject instanceof ArrayList) {
					inserts = translateJSONInsertPart((ArrayList<?>) insertsObject);
				}
			}
			
			return new UpdateBlock(deletes, inserts);
		}
		
		throw new TranslationException("Could not transform JSON. Invalid JSON structure");
	}
}
