package eu.iescities.server.querymapper.datasource.database.pool;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;

public class PoolManager {

	private static PoolManager instance = null;

	private final Map<String, PoolingDataSource<PoolableConnection>> dataSourceMap = new ConcurrentHashMap<String, PoolingDataSource<PoolableConnection>>();
	private final Set<String> loadedDrivers = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());

	private PoolManager() { 
	}
	
	public void clear() throws Exception {
		for (PoolingDataSource<PoolableConnection> dataSource : dataSourceMap.values()) {
			dataSource.close();
		}
		
		dataSourceMap.clear();
		loadedDrivers.clear();
	}

	public static PoolManager getInstance() {
		if (instance == null) {
			instance = new PoolManager();
		}

		return instance;
	}

	private PoolingDataSource<PoolableConnection> createDataSource(String connectURI, String driver) throws ClassNotFoundException {
		if (!loadedDrivers.contains(driver)) {
			Class.forName(driver);
			loadedDrivers.add(driver);
		}
		
		final ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, null);
		final PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
		final ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);

		poolableConnectionFactory.setPool(connectionPool);

		final PoolingDataSource<PoolableConnection> dataSource = new PoolingDataSource<>(connectionPool);

		return dataSource;
	}

	public DataSource getConnection(String connectURI, String driver) throws ClassNotFoundException {
		if (!dataSourceMap.containsKey(connectURI)) {
			final PoolingDataSource<PoolableConnection> dataSource = createDataSource(connectURI, driver);
			dataSourceMap.put(connectURI, dataSource);
		}

		return dataSourceMap.get(connectURI);
	}
	
	public void closeDataSource(String connectURI) throws IOException {
		if (dataSourceMap.containsKey(connectURI)) {
			try {
				dataSourceMap.get(connectURI).close();
				dataSourceMap.remove(connectURI);
			} catch (Exception e) {
				throw new IOException(e);
			}
		}
	}
}
