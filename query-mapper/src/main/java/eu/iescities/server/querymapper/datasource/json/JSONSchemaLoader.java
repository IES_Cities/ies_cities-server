package eu.iescities.server.querymapper.datasource.json;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class JSONSchemaLoader {
	
	public static final String TABLE_ID = "_id";
	public static final String PARENT_ID_COLUMN = "parent_id";
	public static final String NAME_SEPARATOR = "_";
	public static final String ROOT_TABLE_DEFAULT_NAME = "rootTable";

	final JSONConnector connector;
	
	public JSONSchemaLoader(JSONConnector connector) {
		this.connector = connector;
	}
	
	public SPARQLConnector loadSchema() throws InvalidRootException, MalformedURLException, IOException {
		final SPARQLConnector sparqlConnector = new SPARQLConnector("", "");
    	
		final String tableName = StringQuoter.quote(getRootTableName(connector));
    	
    	try (final JSONProcessor jsonProcessor = new JSONProcessor(connector.getReader(), connector.getRoot())) {
    		while (jsonProcessor.hasNext()) {
    			final JsonObject obj = jsonProcessor.next();
    			processElement(obj, sparqlConnector, tableName, connector.getKey(), 0);
    		}
    	}
    	
    	return sparqlConnector;
	}
	
	private void processSchemaArrayList(JsonArray array, SPARQLConnector schema, String tableName, String primaryKey, int level) {					
		for (JsonElement e : array) {
			processElement(e, schema, tableName, primaryKey, level);
		}
	}

	private void processElement(JsonElement e, SPARQLConnector schema, String tableName, String primaryKey, int level) {
		final SPARQLConnector schemaCandidate = new SPARQLConnector("", ""); 
		if (e.isJsonObject()) {
			createTable(e.getAsJsonObject(), schemaCandidate, tableName, primaryKey, level + 1);				
		} else {
			final SPARQLConnector.Table table = new SPARQLConnector.Table(tableName, "");
			
			Literal literal = null;
			
			if (e.isJsonPrimitive()) {
				final JsonPrimitive primitiveValue = e.getAsJsonPrimitive();
				literal = new Literal(primitiveValue);
			} else if (e.isJsonArray()) {
				final JsonArray arrayValue = e.getAsJsonArray();
				literal = new Literal(arrayValue.toString(), LiteralType.STRING);
			}
			
			if (level > 1) {
				table.addKey(new SPARQLConnector.Table.Key(TABLE_ID, literal.getType()));
				table.addColumn(new SPARQLConnector.Table.Column(PARENT_ID_COLUMN, "", literal.getType().toString()));
			}
			
			final String[] names = StringQuoter.unquote(tableName).split(NAME_SEPARATOR);
			
			table.addColumn(new SPARQLConnector.Table.Column(StringQuoter.quote(names[1]), "", literal.getType().toString()));
			schemaCandidate.addTable(table);
		}
		schema.merge(schemaCandidate);
	}
	
	private void createTable(JsonObject jsonObject, SPARQLConnector schema, String tableName, String primaryKey, int level) {
		final SPARQLConnector.Table table = new SPARQLConnector.Table(tableName, "");
		
		boolean keyAdded = false;
		for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
			final String key = StringQuoter.quote(entry.getKey());
			final JsonElement value = entry.getValue();
			
			if (value.isJsonArray()) {
				processSchemaArrayList(value.getAsJsonArray(), schema, StringQuoter.quote(StringQuoter.unquote(tableName) + NAME_SEPARATOR + StringQuoter.unquote(key)), primaryKey, level + 1);
			} else if (value.isJsonObject()) {
				createTable(value.getAsJsonObject(), schema, StringQuoter.quote(StringQuoter.unquote(tableName) + NAME_SEPARATOR + StringQuoter.unquote(key)), primaryKey, level + 1);
			} else if (value.isJsonPrimitive()){
				final JsonPrimitive primitiveValue = value.getAsJsonPrimitive();
				table.addColumn(new SPARQLConnector.Table.Column(key, "", new Literal(primitiveValue).getType().toString()));
				if (level == 1 && !keyAdded) {
					if (key.equals(StringQuoter.quote(primaryKey))) {
						table.addKey(new SPARQLConnector.Table.Key(key, new Literal(primitiveValue).getType()));
					} else if (primaryKey.equals(TABLE_ID)) {
						table.addKey(new SPARQLConnector.Table.Key(TABLE_ID, LiteralType.INT));
					}
					
					keyAdded = true;
				}
			}
		}
		
		if (level > 1) {
			table.addKey(new SPARQLConnector.Table.Key(TABLE_ID));
			table.addColumn(new SPARQLConnector.Table.Column(PARENT_ID_COLUMN, "", LiteralType.STRING.toString()));
		}
		
		schema.addTable(table);
	}
	
	public static String getRootTableName(JSONConnector connector) {
    	if (!connector.getTable().isEmpty()) {
    		return connector.getTable();
    	} else {
    		final String[] keys = connector.getRoot().split("/");
    		if (keys.length == 0) {
    			return JSONSchemaLoader.ROOT_TABLE_DEFAULT_NAME;
    		} else {
    			return keys[keys.length - 1];
    		}
    	}
	}
}
