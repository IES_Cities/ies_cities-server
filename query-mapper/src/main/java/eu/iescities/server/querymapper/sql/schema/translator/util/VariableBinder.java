/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.util;

import java.util.HashMap;
import java.util.Map;

public class VariableBinder {

    private int now;
    private String prefix;
    private static char[] vs = new char['z' - 'a' + 1];

    private final Map<String, String> bindings = new HashMap<String, String>();

    private static VariableBinder instance = null;

    private VariableBinder() {
    	for(char i='a'; i<='z';i++) { 
        	vs[i - 'a'] = i;
        }
    	
        reset();
    }

    public static VariableBinder getInstance() {
        if (instance == null) {
            instance = new VariableBinder();
        }

        return instance;
    }

    public void reset() {
        now = -1;
        prefix = "";
        bindings.clear();
    }

    public String bind(String name) {
        if (!bindings.containsKey(name)) {
            String var = next();
            bindings.put(name, var);
        }

        return bindings.get(name);
    }

    private String fixPrefix(String prefix) {
        if(prefix.length() == 0) {
        	return Character.toString(vs[0]);
        }
        
        int last = prefix.length() - 1;
        char next = (char) (prefix.charAt(last) + 1);
        final String sprefix = prefix.substring(0, last);
        
        return next - vs[0] == vs.length ? fixPrefix(sprefix) + vs[0] : sprefix + next;
    }

    private String next() {
        if(++now == vs.length) {
        	prefix = fixPrefix(prefix);
        }
        
        now %= vs.length;
        return new StringBuilder().append(prefix).append(vs[now]).toString();
    }
}