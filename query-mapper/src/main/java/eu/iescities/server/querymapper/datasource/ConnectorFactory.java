/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import eu.iescities.server.querymapper.datasource.csv.CSVConnector;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.httpapi.HTTPApiConnector;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;

public class ConnectorFactory {

	public static DataSourceConnector load(String json) throws DataSourceConnectorException {
		if (json == null || json.isEmpty()) {
			return new DataSourceConnector();
		}
		
		try {
			final JSONObject jsonObject = new JSONObject(json);
			if (jsonObject.length() == 0) {
				return new DataSourceConnector();
			}
			
			ConnectorFactory.validate(json, "/schemas/basic_mapping_schema.json");
			
			final Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
			final DataSourceConnector partialConnector = gson.fromJson(json, DataSourceConnector.class);

			DataSourceConnector connector;
			switch (partialConnector.getType()) {
				case database:
					ConnectorFactory.validate(json, "/schemas/database_mapping_schema.json");
					connector = DBConnector.load(json);
					break;
	
				case sparql:
					connector = SPARQLConnector.load(json);
					break;
	
				case httpapi:
					connector = HTTPApiConnector.load(json);
					break;
					
				case json:
					ConnectorFactory.validate(json, "/schemas/json_mapping_schema.json");
					connector = JSONConnector.load(json);
					break;
					
				case csv:
					ConnectorFactory.validate(json, "/schemas/csv_mapping_schema.json");
					connector = CSVConnector.load(json);
					break;
					
				case json_schema:
					ConnectorFactory.validate(json, "/schemas/json_schema_mapping_schema.json");
					connector = JSONSchemaConnector.load(json);
					break;
					
				default:
					connector = new DataSourceConnector();
					break;
			}

			return connector;
		} catch (JsonSyntaxException | JSONException | IOException e) {
			throw new DataSourceConnectorException("Not a valid JSON mapping");
		}
	}
	
	public static void validate(String rawJson, String schemaFile) throws IOException, DataSourceConnectorException {
		try (final InputStreamReader reader = new InputStreamReader(ConnectorFactory.class.getResourceAsStream(schemaFile))) {
			final JsonNode schemaNode = JsonLoader.fromReader(reader);
			try {
				final JsonNode json = JsonLoader.fromString(rawJson);
				final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
				final JsonSchema schema = factory.getJsonSchema(schemaNode);
				final ProcessingReport report = schema.validate(json);
				if (!report.isSuccess()) {
					final String errorStr = createErrorString(report);
					throw new DataSourceConnectorException(errorStr);
				}
			} catch (ProcessingException e) {
				throw new DataSourceConnectorException("Invalid JSON: " + e.getMessage());
			}
		}
	}
	
	private static String createErrorString(ProcessingReport report) {
		final StringBuilder strBuilder = new StringBuilder();
		for (ProcessingMessage msg : report) {
			strBuilder.append(String.format("%s: %s\n", msg.asJson().get("schema").get("pointer"), msg.getMessage()));
		}
		return strBuilder.toString();
	}
}
