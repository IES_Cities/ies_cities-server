package eu.iescities.server.querymapper.util.scheduler;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.csv.CSVConnector;
import eu.iescities.server.querymapper.datasource.csv.CSVDumper;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.JSONDumper;
import eu.iescities.server.querymapper.datasource.json.JSONDumper.StorageType;
import eu.iescities.server.querymapper.datasource.security.SecurityFactory;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;

@DisallowConcurrentExecution
public class DataDownloadJob implements Job {
	
	final static Logger logger = Logger.getLogger(DataDownloadJob.class);

	public static final String ID = "id";
	public static final String CONNECTOR = "CONNECTOR";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		final JobDataMap data = context.getJobDetail().getJobDataMap();

		final String id = data.getString(ID);
		final DataSourceConnector connector = (DataSourceConnector) data.get(CONNECTOR);	
		
		execute(connector, id);
	}

	public void execute(final DataSourceConnector connector, final String dataSourceID) throws JobExecutionException {
		final long startTime = System.currentTimeMillis(); 
		
		try {
			DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.PROCESSING, "Data download job started");
			try {
				switch (connector.getType()) {
					case json: 	final JSONDumper jsonDumper = new JSONDumper((JSONConnector) connector, SecurityFactory.getInstance().getSecurityManager(), dataSourceID, StorageType.DUMP);
								jsonDumper.reloadDatabase();
								break;
								
					case csv:	final CSVDumper csvDumper = new CSVDumper((CSVConnector) connector, SecurityFactory.getInstance().getSecurityManager(), dataSourceID);
								csvDumper.reloadDatabase();
								break;
								
					default:	
								break;							
				}
				
				DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.OK, "Job correctly executed");
			} catch (DatabaseCreationException | DataSourceManagementException e) {
				DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.ERROR, e.getMessage());
			}
			
			long elapsed = System.currentTimeMillis() - startTime;
			logger.debug(String.format(String.format("Data processed in %d ms", elapsed)));			
		} catch (DatasetStatusManagerException e) {
			System.out.println("Could not store database status. " + e.getMessage());
		}
	}
}
