package eu.iescities.server.querymapper.datasource.json;

import com.google.gson.JsonElement;

class Node {
	
	private final JsonElement jsonElement;
	private final String name;
	
	public Node(JsonElement jsonElement, String name) {
		this.jsonElement = jsonElement;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public JsonElement getJsonElement() {
		return jsonElement;
	}
}
