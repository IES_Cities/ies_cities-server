package eu.iescities.server.querymapper.datasource.security;

import eu.iescities.server.querymapper.config.Config;

public class SecurityFactory {

	private Config config;
	private static SecurityFactory instance = null;
	
	private SecurityFactory() {
		config = Config.getInstance();
	}
	
	public static SecurityFactory getInstance() {
		if (instance == null) {
			instance = new SecurityFactory();
		}
		
		return instance;
	} 
	
	public DefaultPermissions getDefaultPermissions() {
		try {
			Class<?> clazz = Class.forName(config.getDefaultPermissions());
			return (DefaultPermissions) clazz.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace(); 
		}
		
		return new DefaultPermissions();
	}
	
	public DataSourceSecurityManager getSecurityManager() {
		try {
			Class<?> clazz = Class.forName(config.getSecurityManager());
			return (DataSourceSecurityManager) clazz.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace(); 
		}
		
		return new ACLSecurityManager();
	}
}
