package eu.iescities.server.querymapper.util.scheduler;

import java.util.Properties;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import eu.iescities.server.querymapper.config.Config;


public class QuartzScheduler {

private static QuartzScheduler instance = null;
	
	private final Scheduler sch;

	private QuartzScheduler() throws SchedulerException {
		final Properties prop = new Properties();
		prop.setProperty("org.quartz.threadPool.threadCount", String.valueOf(Config.getInstance().getSchedulerThreadCount()));
		final SchedulerFactory schFactory = new StdSchedulerFactory(prop);
		
		sch = schFactory.getScheduler();
	}
	
	public static QuartzScheduler getInstance() throws SchedulerException {
		if (instance == null) {
			instance = new QuartzScheduler();
		}
		
		return instance;
	}
	
	public void start(Class<? extends MainJob> mainJobClass) throws SchedulerException {
		System.out.println("Starting scheduler service");
	 
		sch.start();
		
		createMainJob(mainJobClass);
	}
	
	public void createMainJob(Class<? extends MainJob> mainJobClass) throws SchedulerException {
		final int checkInterval = Config.getInstance().getSchedulerCheckInterval();
		
		System.out.println(String.format("Starting scheduler main job with a %d s periodicity", checkInterval));
		
		final JobDetail job = JobBuilder
			.newJob(mainJobClass)
			.withIdentity(MainJob.MAIN_JOB_NAME)
			.build();
		
		final Trigger trigger = TriggerBuilder.newTrigger()
			.withSchedule(SimpleScheduleBuilder.simpleSchedule()
			.withIntervalInSeconds(checkInterval)
			.repeatForever()).build();

		sch.scheduleJob(job, trigger);
	}
	
	public Scheduler getScheduler() {
		return sch;
	}
}
