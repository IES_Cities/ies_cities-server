/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.update;

import java.util.ArrayList;
import java.util.List;

public class UpdateSection {

	private final String table;
	private final List<Update> updates;
	
	public UpdateSection(String table) {
		this.table = table;
		this.updates = new ArrayList<Update>();
	}

	public String getTable() {
		return table;
	}

	public List<Update> getUpdates() {
		return updates;
	}
	
	public void addUpdate(Update update) {
		updates.add(update);
	}
}
