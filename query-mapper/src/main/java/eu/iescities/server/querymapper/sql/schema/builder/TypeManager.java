/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Literal;

public class TypeManager {
	
	public enum LiteralType { BOOLEAN, BYTE, FLOAT, DOUBLE, SHORT, INT, LONG, STRING, DATE };

	public static String VARCHAR = "VARCHAR";
	public static String INTEGER = "INTEGER";

	private static boolean isBoolean(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
			
		return type.equals(XSDDatatype.XSDboolean.getURI());
	}

	private static boolean isByte(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDbyte.getURI());
	}

	private static boolean isShort(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDshort.getURI());
	}

	private static boolean isInt(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDint.getURI());
	}

	private static boolean isLong(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDlong.getURI());
	}

	private static boolean isFloat(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDfloat.getURI()); 
	} 

	private static boolean isDouble(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDdouble.getURI());
	}

	private static boolean isString(Literal literal) {
		String type = literal.getDatatypeURI();
		if (type == null)
			return false;
		
		return type.equals(XSDDatatype.XSDstring.getURI());
	}

	public static LiteralType getLiteralType(Literal literal) {
		if (isBoolean(literal))
			return LiteralType.BOOLEAN;
		else if (isByte(literal))
			return LiteralType.BYTE;
		else if (isShort(literal))
			return LiteralType.SHORT;
		else if (isInt(literal))
			return LiteralType.INT;
		else if (isLong(literal))
			return LiteralType.LONG;
		else if (isFloat(literal))
			return LiteralType.FLOAT;
		else if (isDouble(literal))
			return LiteralType.DOUBLE;
		else if (isString(literal))
			return LiteralType.STRING;
		return LiteralType.STRING;
	}

	public static String getSQLType(LiteralType type) {
		switch (type) {
			case BOOLEAN:	return "CHAR(5)";
			case BYTE:		return INTEGER;
			case SHORT:		return INTEGER;
			case INT:		return INTEGER;
			case LONG:		return "BIGINT";
			case FLOAT:		return "FLOAT";
			case DOUBLE:	return "DOUBLE";
			case STRING:	return VARCHAR;
			case DATE:		return "DATE";
			default: 		return "";
		}
	}
}