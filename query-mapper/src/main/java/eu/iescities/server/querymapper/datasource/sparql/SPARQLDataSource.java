/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.sparql;

import java.io.IOException;
import java.util.List;

import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.google.gson.JsonObject;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.query.SPARQLQueriable;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.update.JSONLDUpdateable;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDContext;
import eu.iescities.server.querymapper.sql.schema.translator.SPARQLTranslator;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.sql.schema.translator.listener.TranslatedQuery;

public class SPARQLDataSource implements SPARQLQueriable, JSONLDUpdateable {

	private final SPARQLTranslator translator;
	private TranslatedQuery translatedQuery;
	private Permissions permissions;

	private long translationTime;
	private long executionTime;
	private boolean verbose;
	
	private final String insertTemplate = 	"INSERT DATA\n" + 
			"{\n" +
			"%s" +
			"}";

	private final String insertGraphTemplate = 	"INSERT DATA\n" +
					"{\n" +
					"GRAPH <%s>" +
					"{" +
					"%s" +
					"}" +
					"}";

	public SPARQLDataSource(SPARQLConnector connector) {
		this.translator = new SPARQLTranslator(connector);
		this.verbose = false;
		this.permissions = connector.getPermissions();
	}

	public SPARQLDataSource(SPARQLConnector connector, boolean verbose) {
		this.translator = new SPARQLTranslator(connector);
		this.verbose = verbose;
		this.permissions = connector.getPermissions();
	}
	
	@Override
	public ResultSet executeSQLSelect(String sqlQuery, String user, DataOrigin dataOrigin) throws TranslationException, DataSourceManagementException {
		long startTime = System.currentTimeMillis();

		translatedQuery = translator.translate(sqlQuery);
		String sparqlEndpoint = translator.getSchemaConnector().getQueryEndpoint();

		translationTime = System.currentTimeMillis() - startTime;

		if (verbose) {
			System.out.println("-------------------------------------------------------");
			System.out.println("Executing SPARQL query:");
			System.out.println(translatedQuery.getSPARQLQuery());
			System.out.println("");

			System.out.println("Conversion performed in " + translationTime + " ms");
			System.out.println("-------------------------------------------------------");
			System.out.println();
		}

		startTime = System.currentTimeMillis();

		ResultSet resultSet = executeSPARQLQuery(translatedQuery.getSPARQLQuery(), sparqlEndpoint);

		executionTime = System.currentTimeMillis() - startTime;

		return resultSet;
	}
	
	protected ResultSet executeSPARQLQuery(String sparqlQuery, String sparqlEndpoint) throws DataSourceManagementException {
		try {
			QueryExecution queryExec = QueryExecutionFactory.sparqlService(sparqlEndpoint, sparqlQuery);
			com.hp.hpl.jena.query.ResultSet results = queryExec.execSelect();

			List<String> varNames = results.getResultVars();

			ResultSet resultSet = new ResultSet();

			while(results.hasNext()) {
				QuerySolution q = results.next();

				final Row row = new Row();
				for (String var : varNames) {
					RDFNode value = q.get(var);

					if (value == null) {
						row.addColumn(var, "");
					} else {
						if (value.isLiteral()) {
							row.addColumn(var, value.asLiteral().getValue().toString());
						}
						else if (value.isResource()) {
							row.addColumn(var, value.asResource().toString());
						}
					}
				}
				resultSet.addRow(row);
			}
			return resultSet;
		} catch (Exception e) {
			throw new DataSourceManagementException("Problem with resulting SPARQL execution.", e.getCause());
		}		
	}

	public ResultSet executeSPARQL(String sparqlQuery) throws DataSourceManagementException {
		translationTime = 0;

		String sparqlEndpoint = translator.getSchemaConnector().getQueryEndpoint();

		if (verbose) {
			System.out.println("-------------------------------------------------------");
			System.out.println("Executing SPARQL query:");
			System.out.println(sparqlQuery);
			System.out.println("-------------------------------------------------------");
			System.out.println();
		}

		long startTime = System.currentTimeMillis();

		ResultSet resultSet = executeSPARQLQuery(sparqlQuery, sparqlEndpoint);

		executionTime = System.currentTimeMillis() - startTime;

		return resultSet;
	}

	public String getTranslatedQuery() {
		return translatedQuery.getSPARQLQuery();
	}
	
	@Override
	public JSONLDContext getJSONLDContext() {
		return translatedQuery.getJSONLDContext();
	}

	@Override
	public ResultSet getTables() throws DataSourceManagementException {
		final ResultSet resultSet = new ResultSet();
		for (String table : translator.getSchemaConnector().getTableNames()) {
			final Row row = new Row();
			row.addColumn("Tables", table);
			resultSet.addRow(row);
		}
		return resultSet;		
	}
	
	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		return translator.getSchemaConnector().getInfo();
	}

	public long getTranslationTime() {
		return translationTime;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}
	
	@Override
	public String translateJSONLD(String jsonld) throws TranslationException {
		long startTime = System.currentTimeMillis();
		
		JsonLdOptions options = new JsonLdOptions();
		options.format = "text/turtle";
		
		try {
			Object data = JsonUtils.fromString(jsonld);
			
			String turtle = (String) JsonLdProcessor.toRDF(data, options);
			
			String translatedUpdate;
			if (translator.getSchemaConnector().getGraph().isEmpty()) {
				translatedUpdate = String.format(insertTemplate, turtle);
			} else {
				translatedUpdate = String.format(insertGraphTemplate,
						translator.getSchemaConnector().getGraph(),
						turtle);
			}
			translationTime = System.currentTimeMillis() - startTime;
			
			return translatedUpdate; 
		} catch (IOException | JsonLdError e) {
			throw new TranslationException("Could not transform JSON-LD", e);
		}
	}
	
	@Override
	public int executeJSONLDUpdate(String jsonld) throws TranslationException {	
		String translatedUpdate = translateJSONLD(jsonld);
		
		if (verbose) {
			System.out.println("-------------------------------------------------------");
			System.out.println("Executing SPARQL update:");
			System.out.println(translatedUpdate);
			System.out.println("");
			
			System.out.println("Conversion performed in " + translationTime + " ms");
			System.out.println("-------------------------------------------------------");
			System.out.println();
		}
		
		try {
			UpdateRequest request = new UpdateRequest();
			request.add(translatedUpdate);
			
			UpdateProcessor p = UpdateExecutionFactory.createRemote(request, translator.getSchemaConnector().getUpdateEndpoint());
			p.execute();
			
			return 0;
		} catch (Exception e) {
			throw new TranslationException(e);
		}
	}

	@Override
	public Permissions getPermissions() {
		return permissions;
	}

	@Override
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public String createQueryAll() throws DataSourceManagementException {
		throw new DataSourceManagementException("Not available");
	}

	@Override
	public ResultSet queryAll(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException {
		throw new DataSourceManagementException("Not available");
	}

	@Override
	public JsonObject queryAllJson(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		throw new DataSourceManagementException("Not available");
	}
}