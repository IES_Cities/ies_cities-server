package eu.iescities.server.querymapper.datasource.query.serialization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.eclipse.persistence.oxm.XMLRoot;
import org.eclipse.persistence.oxm.annotations.XmlVariableNode;

public class MapAdapter extends XmlAdapter<MapAdapter.AdaptedMap, Map<String, Object>> {

	public static class AdaptedMap {
		
		@XmlVariableNode("key")
		List<AdaptedEntry> entries = new ArrayList<AdaptedEntry>();
	}

	public static class AdaptedEntry {

		@XmlTransient
		public String key;

		@XmlValue
		@XmlAnyElement(lax=true)
		public Object value;

	}

	@Override
	public AdaptedMap marshal(Map<String, Object> map) throws Exception {
		final AdaptedMap adaptedMap = new AdaptedMap();
		for (Entry<String, Object> entry : map.entrySet()) {
			final AdaptedEntry adaptedEntry = new AdaptedEntry();
			
			if (entry.getValue() != null) {
				adaptedEntry.key = entry.getKey();
				adaptedEntry.value = entry.getValue();
				adaptedMap.entries.add(adaptedEntry);
			}
		}
		return adaptedMap;
	}

	@Override
	public Map<String, Object> unmarshal(AdaptedMap adaptedMap) throws Exception {
		final List<AdaptedEntry> adaptedEntries = adaptedMap.entries;
		final Map<String, Object> map = new HashMap<String, Object>(adaptedEntries.size());
		for (AdaptedEntry adaptedEntry : adaptedEntries) {
			if (adaptedEntry.value != null) {
				map.put(adaptedEntry.key, ((XMLRoot)adaptedEntry.value).getObject().toString());
			} else {
				map.put(adaptedEntry.key, "");
			}
		}
		return map;
	}

}
