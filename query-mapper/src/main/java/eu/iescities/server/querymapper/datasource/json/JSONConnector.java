/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;

public class JSONConnector extends DataSourceConnector {

	@Expose
	private String uri;
	
	@Expose
	private String root;
	
	@Expose
	private String key;
	
	@Expose
	private int refresh;
	
	@Expose
	private String charset;
	
	@Expose
	private String table;

	public JSONConnector() {
		super(ConnectorType.json);
		this.refresh = 60;
		this.charset = "UTF-8";
		this.root = "/";
		this.table = "";
	}
	
	protected JSONConnector(ConnectorType type) {
		super(type);
		this.refresh = 60;
		this.charset = "UTF-8";
		this.root = "/";
		this.table = "";
	}
	
	public String getURI() {
		return uri;
	}
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setURI(String uri) {
		this.uri = uri;
	}
	
	public String getRoot() {
		return root;
	}
	
	public void setRoot(String root) {
		this.root = root;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	public int getRefresh() {
		return refresh;
	}

	public void setRefresh(int refresh) {
		this.refresh = refresh;
	}
	
	public String getCharset() {
		return charset;
	}
	
	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public Reader getReader() throws MalformedURLException, IOException {
		return new URLReader(uri, charset);
	}

	public static JSONConnector load(String json) {
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
		return gson.fromJson(json, JSONConnector.class);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof JSONConnector)) {
			return false;
		}
		
		final JSONConnector jsonConnector = (JSONConnector) o;
		return super.equals(o)
			&& this.uri.equals(jsonConnector.uri)
			&& this.root.equals(jsonConnector.root)
			&& this.key.equals(jsonConnector.key)
			&& this.refresh == jsonConnector.refresh
			&& this.charset.equals(jsonConnector.charset);
	}
}