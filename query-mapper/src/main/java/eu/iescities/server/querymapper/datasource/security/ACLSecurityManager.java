/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;
import eu.iescities.server.querymapper.datasource.security.Permissions.ActionType;

public class ACLSecurityManager implements DataSourceSecurityManager {
	
	final static Logger logger = Logger.getLogger(ACLSecurityManager.class);
	
	public ACLSecurityManager() {
		
	}
	
	private String getWhereClause(String stmt) {
		final String[] parts = stmt.toLowerCase().split("where");
		if (parts.length == 2) {
			return "WHERE" + stmt.substring("WHERE".length() + parts[0].length(), stmt.length());
		} else {
			return "";
		}
	}
	
	private boolean checkOwnerPermissions(AccessInfo info, ActionType action) throws DataSourceSecurityManagerException {
		if (info.getUserID().isEmpty()) {
			throw new DataSourceSecurityManagerException("Anonymous user not allowed"); 
		}
				
		if (action == ActionType.INSERT) {
			return true;
		} else {
			final String whereClause = getWhereClause(info.getStmt());
			final String userQuery = String.format("SELECT DISTINCT(_row_user_) FROM %s %s", info.getTable(), whereClause);
			final Connection conn = info.getConn();
			try {
				final Statement stmt = conn.createStatement();
				final ResultSet results = stmt.executeQuery(userQuery);
				final List<String> users = new ArrayList<String>();
				while (results.next()) {
					users.add(results.getString(1));
				}
				
				if (users.isEmpty()) {
					return false;
				}
				
				for (String user : users) {
					if (!user.equals(info.getUserID())) {
						return false;
					}
				}
				
				return true;
			} catch (SQLException e) {
				throw new DataSourceSecurityManagerException("Could not obtain owner information");
			}
		}
	}
	
	@Override
	public boolean isAuthorized(AccessInfo info, Permissions permissions, ActionType action) throws DataSourceSecurityManagerException {
		final AccessType access = permissions.getPermission(action, info.getTable());
		
		if (info.getUserID().equals(ADMIN_USER)) {
			return true;
		}
		
		switch (access) {
			case ALL:	logger.debug("ALL users has access to " + action.toString() + " on table " + info.getTable());
						return true;
			
			case AUTH:	logger.debug("AUTH users has access to " + action.toString()  + " on table " + info.getTable() + ". User is anonymous:" + info.getUserID().isEmpty());
						return !info.getUserID().isEmpty();
			
			case NONE:	logger.debug("NONE user has access to " + action.toString() + " on table " + info.getTable());
						return false;
			
			case OWNER:	logger.debug("OWNER user has access to " + action.toString() + " on table " + info.getTable());
						return checkOwnerPermissions(info, action);
			
			case USER: 	logger.debug("USER has access to " + action.toString() + " on table " + info.getTable());
						final List<String> users = permissions.getUserList(action, info.getTable());
						return users.contains(info.getUserID());
			
			default:	return false;
		}
	}
}
