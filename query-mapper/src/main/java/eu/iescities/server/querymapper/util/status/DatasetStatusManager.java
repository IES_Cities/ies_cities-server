package eu.iescities.server.querymapper.util.status;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;

public class DatasetStatusManager {

	public enum Status { OK, ERROR, PROCESSING };
	
	private static DatasetStatusManager instance = null;
	
	private DatasetStatusManager() {
		
	}
	
	public static DatasetStatusManager getInstance() {
		if (instance == null) {
			instance = new DatasetStatusManager();
		}
		
		return instance;
	}
	
	private boolean existDB() {
		final File f = new File(getDBPath());
		return f.exists();
	}
	
	private void checkDB() throws DatasetStatusManagerException {
		if (!existDB()) {
			createDB();
		}
	}
	
	public void deleteDB() throws IOException {
		PoolManager.getInstance().closeDataSource(getConnectionString());
		final File f = new File(getDBPath());
		if (f.exists()) {
			f.delete();
		}
	}
	
	private String getDBPath() {
		return Config.getInstance().getDataDir() + File.separatorChar + "status.db";
	}
	
	private String getConnectionString() {
		return "jdbc:sqlite:" + getDBPath();
	}
	
	private synchronized void createDB() throws DatasetStatusManagerException {
		try {
			final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
			try (final Connection conn = dataSource.getConnection()) {		
				try (final Statement stmt = conn.createStatement()) {
					stmt.executeUpdate("CREATE TABLE dataset_status (id VARCHAR PRIMARY KEY NOT NULL, status VARCHAR, message VARCHAR, timestamp DATETIME)");
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
		}
	}
	
	public synchronized List<DatasetStatus> getAll() throws DatasetStatusManagerException {
		checkDB();
		
		final List<DatasetStatus> statusList = new ArrayList<DatasetStatus>();
		try {
			final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
			try (final Connection conn = dataSource.getConnection()) {		
				try (final Statement stmt = conn.createStatement()) {
					final ResultSet results = stmt.executeQuery("SELECT id, status, message, timestamp FROM dataset_status");
					while (results.next()) {
						final String id = results.getString("id");
						final String status = results.getString("status");
						final String message = results.getString("message");
						final String timestamp = results.getString("timestamp");
						final DatasetStatus datasetStatus = new DatasetStatus(id, status, message, timestamp);
						statusList.add(datasetStatus);
					}
				}
			}
			
			return statusList;
		} catch (ClassNotFoundException | SQLException e) {
			throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
		}
	}
	
	public synchronized List<DatasetStatus> getAllErrors() throws DatasetStatusManagerException {
		checkDB();
		
		final List<DatasetStatus> statusList = new ArrayList<DatasetStatus>();
		try {
			final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
			try (final Connection conn = dataSource.getConnection()) {		
				try (final Statement stmt = conn.createStatement()) {
					final ResultSet results = stmt.executeQuery("SELECT id, status, message, timestamp FROM dataset_status WHERE status = 'ERROR'");
					while (results.next()) {
						final String id = results.getString("id");
						final String status = results.getString("status");
						final String message = results.getString("message");
						final String timestamp = results.getString("timestamp");
						final DatasetStatus datasetStatus = new DatasetStatus(id, status, message, timestamp);
						statusList.add(datasetStatus);
					}
				}
			}
			
			return statusList;
		} catch (ClassNotFoundException | SQLException e) {
			throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
		}
	}
	
	public synchronized DatasetStatus getStatus(String id) throws DatasetStatusManagerException {
		synchronized (this) {
			checkDB();
			
			final String select = "SELECT id, status, message, timestamp FROM dataset_status WHERE id = ?";
			
			try {
				final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
				try (final Connection conn = dataSource.getConnection()) {		
					try (final PreparedStatement stmt = conn.prepareStatement(select)) {
						stmt.setString(1, id);
						final ResultSet results = stmt.executeQuery();
						if (results.next()) {
							final String status = results.getString("status");
							final String message = results.getString("message");
							final String timestamp = results.getString("timestamp");
							final DatasetStatus datasetStatus = new DatasetStatus(id, status, message, timestamp);
							return datasetStatus;
						} else {
							return new DatasetStatus("-1", "", "", "");
						}
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
			}
		}
	}
	
	public synchronized void insertStatus(String id, Status status, String message) throws DatasetStatusManagerException {
		checkDB();
		
		final String insert = "INSERT INTO dataset_status (id, status, message, timestamp) VALUES (?, ?, ?, datetime())";
		
		try {
			final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
			try (final Connection conn = dataSource.getConnection()) {		
				try (final PreparedStatement stmt = conn.prepareStatement(insert)) {
					stmt.setString(1, id);
					stmt.setString(2, status.toString());
					stmt.setString(3, message);
					stmt.execute();
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
		}
	}
	
	public synchronized void updateStatus(String id, Status status, String message) throws DatasetStatusManagerException {
		checkDB();
		
		final DatasetStatus datasetStatus = getStatus(id);
		if (datasetStatus.getId().equals("-1")) {
			insertStatus(id, status, message);
		} else {
			final String update = "UPDATE dataset_status SET status=?, message=?, timestamp=datetime() WHERE id=?";
			
			try {
				final DataSource dataSource = PoolManager.getInstance().getConnection(getConnectionString(), "org.sqlite.JDBC");
				try (final Connection conn = dataSource.getConnection()) {		
					try (final PreparedStatement stmt = conn.prepareStatement(update)) {
						stmt.setString(1, status.toString());
						stmt.setString(2, message);
						stmt.setString(3, id);
						stmt.execute();
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				throw new DatasetStatusManagerException("Could not create dataset status database. " + e.getMessage());
			}
		}
	}
}
