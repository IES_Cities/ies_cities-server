/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.listener;

class Value {

	public enum Type { Variable, StringLiteral, IntegerLiteral, FloatLiteral, DateLiteral };

	private final String value;
	private final Type type;

	public Value(String value, Type type) {
		this.value = value;
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public Type getType() {
		return type;
	}

	public String getFmtString() {
		switch (type) {
			case StringLiteral: 			return "str(%s)";
			case IntegerLiteral:			return "xsd:int(%s)";
			case FloatLiteral:				return "xsd:float(%s)";
			case DateLiteral:				return "xsd:date(%s)";

			default:						return "%s";
		}
	}
}