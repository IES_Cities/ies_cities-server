/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.listener;

import eu.iescities.server.querymapper.sql.schema.translator.util.VariableBinder;

class FunctionColumn extends Column {
	
	private enum INT_FUNCTIONS {COUNT, AVG}; 

	private final String functionName;

	public FunctionColumn(String name, String table, String alias, String functionName) {
		super(name, table, alias);
		this.functionName = functionName;
	}

	public String getFunctionName() {
		return functionName;
	}

	@Override
	public String getSPARQLField(boolean qualify) {
		if (getAlias().isEmpty())
			return String.format("(%s(str(?%s)) as ?%s) ", functionName, 
				VariableBinder.getInstance().bind(getName()), functionName);
		else 
			return String.format("(%s(str(?%s)) as ?%s) ", functionName, 
				VariableBinder.getInstance().bind(getName()), getAlias());
	}

	public String getFunctionType() {
		try {
			INT_FUNCTIONS.valueOf(functionName.toUpperCase());
			return "http://www.w3.org/2001/XMLSchema#integer";
		} catch (IllegalArgumentException e) {
			return "";
		}
	}
}