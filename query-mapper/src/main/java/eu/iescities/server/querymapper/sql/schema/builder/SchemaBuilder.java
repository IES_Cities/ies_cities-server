/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;

public class SchemaBuilder {

	private static final String PRIMARY_KEY_ID = "id";
	private static final String LEFT_ID = "left_id";
	private static final String RIGHT_ID = "right_id";

	public static final String MANY_TO_MANY = "manytomany";

	public static final String OBJECT_PROPERTY = "objectproperty";

	private HashMap<String, String> prefixes = new HashMap<String, String>();

	private List<String> ignored = new ArrayList<String>();

	private final String sparqlEndpoint;
	private final String graph;
	private final boolean useOntology;
	
	private BuilderStats stats;
	
	public SchemaBuilder(String sparqlEndpoint, String graph, boolean useOntology) throws IOException {
		this.sparqlEndpoint = sparqlEndpoint;
		this.graph = graph;
		this.useOntology = useOntology;
		
		loadPrefixes();
		loadIgnored();
	}

	private void loadPrefixes() throws IOException {
		try (final 	BufferedReader br = new BufferedReader(new InputStreamReader(SchemaBuilder.class.getResourceAsStream("/prefixes.txt")))) {
			String line;

			while ((line = br.readLine()) != null) {
				String[] prefixTuple = line.split(" ");
				prefixes.put(prefixTuple[1], prefixTuple[0]);
			}
		}
	}

	private void loadIgnored() throws IOException {
		try (final BufferedReader br = new BufferedReader(new InputStreamReader(SchemaBuilder.class.getResourceAsStream("/ignored.txt")))) {
			String line;

			while ((line = br.readLine()) != null) {
				ignored.add(line);
			}
		}	
	}

	protected String getShortPrefix(String prefix) {
		final StringTokenizer strTokenizer = new StringTokenizer(prefix, "/");

		final StringBuilder strbuilder = new StringBuilder();
		while (strTokenizer.hasMoreTokens()) {
			strbuilder.append(strTokenizer.nextToken().charAt(0));
		}
		return strbuilder.toString();
	}

	protected String getShortPrefixFromList(String prefix) {
		final String p = prefixes.get(prefix);

		if (p != null) {
			return p;
		}
		else {
			return getShortPrefix(prefix);
		}
	}

	private String getShortVersion(String url) {
		String name = "";
		String prefix = "";
		if (url.contains("#")) {
			name = url.substring(url.indexOf("#") + 1, url.length());
			prefix = url.substring(0, url.indexOf('#')+1);
		} else {
			name = url.substring(url.lastIndexOf("/") + 1, url.length());
			prefix = url.substring(0, url.lastIndexOf('/')+1);
		}
		return getShortPrefixFromList(prefix) + "_" + name;
	}

	private String getPropertyType(RDFNode value) {
		if (value.isLiteral()) {
			TypeManager.LiteralType type = TypeManager.getLiteralType(value.asLiteral());
			return type.toString();
		} else if (value.isResource()) {
			return OBJECT_PROPERTY;
		} else {
			return "";
		}
	}
	
	private boolean isIgnored(String clazz) {
		for (String prefix : ignored) {
			if (clazz.startsWith(prefix)) {
				return true;
			}
		}
		return false;
	}

	private String getFrom() {
		if (graph.isEmpty())
			return "";
		else
			return String.format("FROM <%s> ", graph);
	}

	public List<String> getGraphs() throws SchemaBuilderException {
		List<String> graphs = new ArrayList<String>();

		String graphQuery = "select distinct ?g where { graph ?g {?s ?p ?o} }";
		try {
			QueryExecution graphQueryExec = QueryExecutionFactory.sparqlService(sparqlEndpoint, graphQuery);
			ResultSet graphResults = graphQueryExec.execSelect();

			while (graphResults.hasNext()) {
				Resource graph = graphResults.next().getResource("g");
				graphs.add(graph.toString());
			}
			return graphs;			
		} catch (Exception e) {
			throw new SchemaBuilderException("Problem obtaining available graphs", e, graphQuery);
		}
	}
	
	public ResultSetRewindable getClasses(String sparqlEndpoint) {
		String classQuery;
		if (!useOntology) {
			classQuery = String.format("SELECT DISTINCT ?class %sWHERE { [] a ?class }", getFrom());
		}
		else {
			classQuery = String.format("SELECT DISTINCT ?class %sWHERE { { ?class a <http://www.w3.org/2000/01/rdf-schema#Class> } UNION { ?class a <http://www.w3.org/2002/07/owl#Class> } }", getFrom());
		}

		try (final QueryExecution classQueryExec = QueryExecutionFactory.sparqlService(sparqlEndpoint, classQuery)) {
			ResultSetRewindable classResults = ResultSetFactory.copyResults(classQueryExec.execSelect());
		
			return classResults;
		}
	}
	
	class PropertyInfo {
		
		private String propertyName;
		private String propertyType;
		boolean multiple;
		
		public PropertyInfo(String propertyName, String propertyType, boolean multiple) {
			this.propertyName = propertyName;
			this.propertyType = propertyType;
			this.multiple = multiple;
		}

		public String getPropertyName() {
			return propertyName;
		}

		public String getPropertyType() {
			return propertyType;
		}

		public boolean isMultiple() {
			return multiple;
		}

		public void setMultiple() {
			multiple = true;
		}
	}
	
	public Map<String, PropertyInfo> getPropertiesInfo(String sparqlEndpoint, String clazz) {		
		String propertyQuery = 	"SELECT DISTINCT ?property (SAMPLE(?o) AS ?value) (COUNT(?o) AS ?count) %sWHERE {" +
								"{" + 
									"SELECT ?s WHERE {?s a <%s>} LIMIT 100" +
								"}" +
								" ?s ?property ?o ." +
								"} GROUP BY ?property ?s";

		try (final QueryExecution propertyQueryExec = QueryExecutionFactory.sparqlService(
				sparqlEndpoint, 
				String.format(propertyQuery, getFrom(), clazz.toString()))) {
			ResultSetRewindable propertyResults = ResultSetFactory.copyResults(propertyQueryExec.execSelect());
			
			Map<String, PropertyInfo> propertiesInfo = new HashMap<String, SchemaBuilder.PropertyInfo>();
			while (propertyResults.hasNext()) {
				QuerySolution currentRow = propertyResults.next();
				
				Resource property = currentRow.getResource("property");
				if (!isIgnored(property.toString())) {
					String propertyName = getShortVersion(property.toString());
		 			
		 			RDFNode value = currentRow.get("value");
		 			String propertyType = getPropertyType(value);
	
		 			RDFNode count = currentRow.get("count");
		 			boolean multiple = count.asLiteral().getInt() > 1;
		 			
		 			if (!propertiesInfo.containsKey(property.toString())) {
		 				propertiesInfo.put(property.toString(), new PropertyInfo(propertyName, propertyType, multiple));
		 			}
		 			
		 			if (multiple) {
		 				propertiesInfo.get(property.toString()).setMultiple();
		 			}
				}
			}
			
			return propertiesInfo;
		}
	}

	public SPARQLConnector getSchema() throws SchemaBuilderException {
		long schemaStartTime = System.currentTimeMillis();
		
		SPARQLConnector connector = new SPARQLConnector(sparqlEndpoint, graph);

		ResultSetRewindable classResults = getClasses(sparqlEndpoint);

		System.out.println(String.format("Detected %s classes", classResults.size()));
		
		List<RowStats> data = new ArrayList<RowStats>();
		
		int classNum = 0;		
 		while (classResults.hasNext()) {
	 		Resource clazz = classResults.next().getResource("class");
	 		
	 		if (!isIgnored(clazz.toString())) {
	 			System.out.println("Processing " + clazz.toString());
	 			classNum++;
	 			
	 			String tableName = getShortVersion(clazz.toString());
	 			SPARQLConnector.Table table = new SPARQLConnector.Table(tableName, clazz.toString());
	 			table.addKey(new SPARQLConnector.Table.Key(PRIMARY_KEY_ID));
	 			
	 			long propertyExtractionStartTime = System.currentTimeMillis();
	 			
	 			Map<String, PropertyInfo> propertiesInfo = getPropertiesInfo(sparqlEndpoint, clazz.toString());
	
		 		System.out.println(String.format("\tDetected %s properties", propertiesInfo.size()));
		 		
		 		int propertyNum = 0;
		 		for (Entry<String, PropertyInfo> entry : propertiesInfo.entrySet()) {
	 				String property = entry.getKey();
		 			System.out.println("\tProcessing " + property);
		 			
		 			propertyNum++;
		 			
		 			PropertyInfo propertyInfo = entry.getValue();
	
		 			if (propertyInfo.isMultiple()) {
		 				if (propertyInfo.getPropertyType().equals(OBJECT_PROPERTY)) {
			 				SPARQLConnector.Table associationTable = new SPARQLConnector.Table(tableName + "_" + propertyInfo.getPropertyName(), MANY_TO_MANY);
			 				associationTable.addKey(new SPARQLConnector.Table.Key(LEFT_ID, tableName));
			 				associationTable.addKey(new SPARQLConnector.Table.Key(RIGHT_ID));
	
			 				SPARQLConnector.Table.Column left_id = new SPARQLConnector.Table.Column(LEFT_ID, clazz.toString(), OBJECT_PROPERTY);
			 				SPARQLConnector.Table.Column right_id = new SPARQLConnector.Table.Column(RIGHT_ID, property.toString(), OBJECT_PROPERTY);
	
			 				associationTable.addColumn(left_id);
			 				associationTable.addColumn(right_id);
	
			 				connector.addTable(associationTable);
						} else {
							String newTableName = propertyInfo.getPropertyName() + "_table";
							if (!connector.containsTable(newTableName)) {
								SPARQLConnector.Table propertyTable = new SPARQLConnector.Table(newTableName, clazz.toString());
								propertyTable.addKey(new SPARQLConnector.Table.Key(PRIMARY_KEY_ID, tableName));
	
								SPARQLConnector.Table.Column propertyColumn = new SPARQLConnector.Table.Column(propertyInfo.getPropertyName(), property.toString(), propertyInfo.getPropertyType());
	
								propertyTable.addColumn(propertyColumn);
	
								connector.addTable(propertyTable);
							}
						}
		 			} else {
		 				SPARQLConnector.Table.Column column = new SPARQLConnector.Table.Column(propertyInfo.getPropertyName(), property.toString(), propertyInfo.getPropertyType());
		 				table.addColumn(column);
		 			}
	 			}
		 		System.out.println();
	
		 		connector.addTable(table);
		 		
		 		long propertyExtractionTime = System.currentTimeMillis() - propertyExtractionStartTime;
		 		
		 		data.add(new RowStats(propertyExtractionTime, propertyNum));
	 		}
 		}
 		
 		long totalExtractionTime = System.currentTimeMillis() - schemaStartTime;
 		
 		stats = new BuilderStats(classNum, data, totalExtractionTime);
 		
 		return connector;
	}
	
	public BuilderStats getStats() {
		return stats;
	}
}