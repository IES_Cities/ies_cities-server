/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.exporter;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class SPARQLSchemaExporter {

	private final SPARQLConnector schema;

	public SPARQLSchemaExporter(SPARQLConnector schema) {
		this.schema = schema;
	}

	private void createTable(SPARQLConnector.Table table, StringBuilder strBuilder, boolean injectRowUser) {
		strBuilder.append(String.format("CREATE TABLE %s\n", table.getName()));
		strBuilder.append("(");

		for (SPARQLConnector.Table.Key key : table.getPrimaryKey()) {
			if (key.getReferences() == null)
				strBuilder.append(String.format("\n\t%s %s,", key.getName(), TypeManager.getSQLType(key.getType()) + " NOT NULL PRIMARY KEY"));
			else
				strBuilder.append(String.format("\n\t%s %s,", key.getName(), TypeManager.getSQLType(key.getType()) + " NOT NULL"));
		}

		for (SPARQLConnector.Table.Column column : table.getColumns()) {
			if (!table.containsKey(column.getName())) {
				String type = TypeManager.VARCHAR;

				if (!column.getType().equals(SchemaBuilder.OBJECT_PROPERTY.toLowerCase())) {
					TypeManager.LiteralType literalType = TypeManager.LiteralType.valueOf(column.getType());
					type = TypeManager.getSQLType(literalType);
				}
				
				strBuilder.append(String.format("\n\t%s %s,", column.getName(), type));
			}
		}
		
		if (injectRowUser) {
			strBuilder.append(String.format("\n\t%s %s,", "_row_user_", TypeManager.getSQLType(LiteralType.STRING)));
		}

		for (SPARQLConnector.Table.Key key : table.getPrimaryKey()) {
			if (key.getReferences() != null)
				strBuilder.append(String.format("\n\tCONSTRAINT %s FOREIGN KEY(%s) REFERENCES %s(%s),",
					key.getName(), key.getName(), key.getReferences(), "id"));
		}

		strBuilder.deleteCharAt(strBuilder.length() - 1);

		strBuilder.append("\n);");
	}

	public String getSQLCreateTables(boolean injectRowUser) {
		StringBuilder sqlTables = new StringBuilder();

		for (SPARQLConnector.Table table : schema.getSchemaTables()) {
			createTable(table, sqlTables, injectRowUser);
			sqlTables.append("\n\n");
		}

		return sqlTables.toString();
	}
}