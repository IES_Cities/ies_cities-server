/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.parser.SQLSelectBaseListener;
import eu.iescities.server.querymapper.sql.parser.SQLSelectParser;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDContext;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.sql.schema.translator.util.VariableBinder;

public class SPARQLTranslatorListener extends SQLSelectBaseListener {

	private final SPARQLConnector schema;

	private final List<Column> selectedColumns = new ArrayList<Column>();
	private final List<Column> whereColumns = new ArrayList<Column>();
	private final List<Table> fromTables = new ArrayList<Table>();

	private boolean selectAll = false;
	private final List<String> selectAllFrom = new ArrayList<String>();

	private final StringBuilder filter = new StringBuilder();
	private final StringBuilder having = new StringBuilder();
	private final StringBuilder limit = new StringBuilder();

	private final List<OrderByColumn> orderByColumns = new ArrayList<OrderByColumn>();
	private final List<Column> groupByColumns = new ArrayList<Column>();

	private TranslationException pendingError = null;

	private boolean distinct = false;

	public SPARQLTranslatorListener(SPARQLConnector schema) {
		this.schema = schema;
	}

	@Override
	public void enterSelect(SQLSelectParser.SelectContext ctx) {
		if (ctx.DISTINCT() != null)
			distinct = true;
	}

	@Override
	public void enterItemList(SQLSelectParser.ItemListContext ctx) {
		if (ctx.STAR() != null)
			selectAll = true;
	}

	@Override
	public void enterItem(SQLSelectParser.ItemContext ctx) {
		if (ctx.value() != null) {
			SQLSelectParser.ColumnRefContext columnRef = ctx.value().columnRef();
			if (columnRef != null) {
				String columnName = columnRef.columnName().getText();
				String table = "";
				if (columnRef.tableAlias() != null)
					table = columnRef.tableAlias().getText();

				String alias = "";
				if (ctx.alias() != null)
					alias = ctx.alias().getText();

				selectedColumns.add(new Column(columnName, table, alias));
			}
		} else if (ctx.function() != null) {
			String functionName = ctx.function().functionName().getText();
			if (!ctx.function().value().isEmpty()) {
				SQLSelectParser.ColumnRefContext columnRef = ctx.function().value(0).columnRef();
				if (columnRef != null) {
					String columnName = columnRef.columnName().getText();
					String table = "";
					if (columnRef.tableAlias() != null)
						table = columnRef.tableAlias().getText();

					String alias = "";
					if (ctx.alias() != null)
						alias = ctx.alias().getText();

					selectedColumns.add(new FunctionColumn(columnName, table, alias, functionName));	
				}
			}
		}
	}

	@Override
	public void enterAllColumns(SQLSelectParser.AllColumnsContext ctx) {
		String tableName = ctx.tableAlias().getText();
		selectAllFrom.add(tableName);
	}

	@Override
	public void enterFromItem(SQLSelectParser.FromItemContext ctx) {
		String tableName = ctx.tableRef().getText();
		String tableAlias = tableName;
		if (ctx.alias() != null)
			tableAlias = ctx.alias().getText();

		Table table = new Table(tableName, tableAlias, schema.getTable(tableName));

		if (!schema.containsTable(table.getName()))
			pendingError = new TranslationException(String.format("ERROR 1146 (42S02): Table '%s' doesn't exist", table.getName()));

		fromTables.add(table);
	}

	@Override
	public void enterLimit(SQLSelectParser.LimitContext ctx) {
		String value = ctx.Integer().getText();
		limit.append(" limit " + value);
	}

	private Column getColumn(SQLSelectParser.ExpressionContext ctx) throws TranslationException{
		SQLSelectParser.ColumnRefContext columnRef = ctx.value().columnRef();
		if (columnRef != null) {
			String columnName = columnRef.columnName().getText();
			if (columnRef.tableAlias() == null) {
				Table table = findTableWith(columnName);
				if (table == null)
					throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'where clause'", columnName));
				
				return new Column(columnName, table.getName(), "");
			} else
				return new Column(columnName, columnRef.tableAlias().getText(), "");
		}
		return null;
	}

	private Value getLiteral(SQLSelectParser.ExpressionContext ctx) {
		SQLSelectParser.LiteralContext literal = ctx.value().literal();
		if (literal != null) {
			if (literal.String() != null)
				return new Value(literal.String().getText(), Value.Type.StringLiteral);
			else if (literal.StringDate() != null)
				return new Value(literal.StringDate().getText(), Value.Type.DateLiteral);
			else if (literal.Float() != null)
				return new Value(literal.Float().getText(), Value.Type.FloatLiteral);
			else if (literal.Integer() != null)
				return new Value(literal.Integer().getText(), Value.Type.IntegerLiteral);
		}
		return null;
	}

	private String getVar(Column column) throws TranslationException {
		String var;
		Table table = getFromTable(column.getTable());
		if (table == null)
			throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'where clause'", column.getName()));

		if (!table.getTableSchema().isManyToMany() && 
			table.getTableSchema().containsKey(column.getName()))
			var = VariableBinder.getInstance().bind(table.getName());
		else
			var = VariableBinder.getInstance().bind(column.getName());
		return "?" + var;
	}

	private Value getValue(SQLSelectParser.ExpressionContext expression) throws TranslationException {
		Column column = getColumn(expression);
		if (column != null) {
			whereColumns.add(column);
			return new Value(getVar(column), Value.Type.Variable);
		}
		else
			return getLiteral(expression);
	}

	@Override
	public void enterOrderBy(SQLSelectParser.OrderByContext ctx) {
		for (SQLSelectParser.OrderByItemContext item : ctx.orderByItem()) {
			SQLSelectParser.ColumnRefContext columnRef = item.columnRef();
			if (columnRef != null) {
				String columnName = columnRef.columnName().getText();
				String table = "";
				if (columnRef.tableAlias() != null)
					table = columnRef.tableAlias().getText();

				if (item.DESC() != null)
					orderByColumns.add(new OrderByColumn(columnName, table, OrderByColumn.Order.DESC));
				else
					orderByColumns.add(new OrderByColumn(columnName, table, OrderByColumn.Order.ASC));
			}
		}
	}

	@Override
	public void enterGroupBy(SQLSelectParser.GroupByContext ctx) {
		for (SQLSelectParser.ColumnRefContext columnRef : ctx.columnRef()) {
			String columnName = columnRef.columnName().getText();
			String table = "";
			if (columnRef.tableAlias() != null)
				table = columnRef.tableAlias().getText();

			groupByColumns.add(new Column(columnName, table, columnName));	
		}
	}

	@Override
	public void enterWhere(SQLSelectParser.WhereContext ctx) {
		filter.append("\tfilter (\n");
		StringBuilder conditions = new StringBuilder();
		processConditionList(ctx.conditionList(), conditions);
		filter.append(conditions.toString());
		filter.append("\n\t) .\n");
	}

	@Override
	public void enterHaving(SQLSelectParser.HavingContext ctx) {
		having.append("\thaving (\n");
		StringBuilder conditions = new StringBuilder();
		processConditionList(ctx.conditionList(), conditions);
		having.append(conditions.toString());
		having.append("\n\t)");
	}

	private void processComparison(SQLSelectParser.ComparisonContext comparison, StringBuilder conditions, boolean negate) {
		SQLSelectParser.ExpressionContext expressionLeft = comparison.expression(0);
		SQLSelectParser.ComparatorContext comparator = comparison.comparator();
		SQLSelectParser.ExpressionContext expressionRight = comparison.expression(1);

		try {
			Value leftValue = getValue(expressionLeft);
			Value rightValue = getValue(expressionRight);

			String condition;
			if (negate)
				condition = "\t\t!" + rightValue.getFmtString() + " %s " + rightValue.getFmtString();
			else
				condition = "\t\t" + rightValue.getFmtString() + " %s " + rightValue.getFmtString();

			conditions.append(String.format(condition, leftValue.getValue(), comparator.getText(), rightValue.getValue()));
		} catch (TranslationException te) {
			pendingError = te;
		}
	}

	private void processLike(SQLSelectParser.LikeContext like, StringBuilder conditions, boolean negate) {
		SQLSelectParser.ExpressionContext expressionLeft = like.expression(0);
		SQLSelectParser.ExpressionContext expressionRight = like.expression(1);

		try {
			Value leftValue = getValue(expressionLeft);
			Value rightValue = getValue(expressionRight);

			if (like.NOT() != null)
				negate = !negate;

			String condition;
			if (negate)
				condition = "\t\t!regex(" + rightValue.getFmtString() + ", " + rightValue.getFmtString() + ")";
			else
				condition = "\t\tregex(" + rightValue.getFmtString() + ", " + rightValue.getFmtString() + ")";

			conditions.append(String.format(condition, leftValue.getValue(), rightValue.getValue()));
		} catch (TranslationException te) {
			pendingError = te;
		}
	}

	private void processIsNull(SQLSelectParser.IsNullContext isNull, StringBuilder conditions, boolean negate) {
		SQLSelectParser.ExpressionContext expression = isNull.expression();

		try {
			Value value = getValue(expression);

			if (isNull.NOT() != null)
				negate = !negate;

			String condition;
			if (negate)
				condition = "\t\tstr(" + value.getFmtString() + ") != \"\"";
			else
				condition = "\t\tstr(" + value.getFmtString() + ") = \"\"";

			conditions.append(String.format(condition, value.getValue()));
		} catch (TranslationException te) {
			pendingError = te;
		}
	}

	private void processBetween(SQLSelectParser.BetweenContext between, StringBuilder conditions, boolean negate) {
		SQLSelectParser.ExpressionContext expression = between.expression(0);
		SQLSelectParser.ExpressionContext expressionLow = between.expression(1);
		SQLSelectParser.ExpressionContext expressionUp = between.expression(2);

		try {
			Value value = getValue(expression);
			Value valueLow = getValue(expressionLow);
			Value valueUp = getValue(expressionUp);

			String condition;
			if (negate)
				condition = "\t\t!(" + valueLow.getFmtString() + " >= " + valueLow.getFmtString() + " && " + 
									valueLow.getFmtString() + " <= " + valueLow.getFmtString() + ")";
			else
				condition = "\t\t" + valueLow.getFmtString() + " >= " + valueLow.getFmtString() + " && " + 
									valueLow.getFmtString() + " <= " + valueLow.getFmtString();
			conditions.append(String.format(condition, value.getValue(), valueLow.getValue(), 
													value.getValue(), valueUp.getValue()));
		} catch (TranslationException te) {
			pendingError = te;
		}
	}

	private void processNestedCondition(SQLSelectParser.NestedConditionContext nestedCondition, StringBuilder conditions, boolean negate) {
		if (negate)
			conditions.append("\t\t!(\n");
		else
			conditions.append("\t\t(\n");
		processConditionList(nestedCondition.conditionList(), conditions);
		conditions.append("\n\t\t)\n");
	}

	public void processConditionList(SQLSelectParser.ConditionListContext ctx, StringBuilder conditions) {
		int counter = 0;
		while (counter < ctx.getChildCount() && pendingError == null) {
			ParseTree child = ctx.getChild(counter);
			if (child instanceof SQLSelectParser.ConditionContext) {
				SQLSelectParser.ConditionContext condition = (SQLSelectParser.ConditionContext)child;

				boolean negate = false;
				ParseTree conditionChild;
				if (condition.NOT() != null) {
					negate = true;
					conditionChild = child.getChild(1);
				} else
					conditionChild = child.getChild(0);

				if (conditionChild instanceof SQLSelectParser.ComparisonContext)
					processComparison((SQLSelectParser.ComparisonContext) conditionChild, conditions, negate);				
				else if (conditionChild instanceof SQLSelectParser.LikeContext)
					processLike((SQLSelectParser.LikeContext) conditionChild, conditions, negate);				
				else if (conditionChild instanceof SQLSelectParser.NestedConditionContext)
					processNestedCondition((SQLSelectParser.NestedConditionContext) conditionChild, conditions, negate);
				else if (conditionChild instanceof SQLSelectParser.IsNullContext)
					processIsNull((SQLSelectParser.IsNullContext) conditionChild, conditions, negate);
				else if (conditionChild instanceof SQLSelectParser.BetweenContext)
					processBetween((SQLSelectParser.BetweenContext) conditionChild, conditions, negate);
				else
					pendingError = new TranslationException(String.format("Unsupported condition '%s'", child.getText()));
			}
			else if (child instanceof TerminalNode) {
				TerminalNode terminalNode = (TerminalNode)child;
				
				switch (terminalNode.getSymbol().getType()) {
					case SQLSelectParser.AND: 	conditions.append(" &&\n");
												break;
					case SQLSelectParser.OR:	conditions.append(" ||\n");
												break;
				}
			}

			counter++;
		}
	}

	private List<Column> getAllColumns(Table table) {
		List<Column> selectedColumns = new ArrayList<Column>();

		if (!table.getTableSchema().isManyToMany())
			selectedColumns.add(new Column(table.getTableSchema().getPrimaryKey().get(0).getName(), table.getAlias(), "", true));

		for (SPARQLConnector.Table.Column column : table.getTableSchema().getColumns())
			selectedColumns.add(new Column(column.getName(), table.getAlias(), ""));

		return selectedColumns;
	}

	private Table getFromTable(String tableName) {
		for (Table table : fromTables) {
			if (table.getAlias().equals(tableName))
				return table;
		}
		return null;
	}

	private Table findTableWith(String columnName) {
		for (Table table : fromTables) {
			if (table.getTableSchema().containsColumn(columnName) || 
				(!table.getTableSchema().isManyToMany() && 
					table.getTableSchema().containsKey(columnName)))
				return table;
		}
		return null;
	}
	
	public TranslatedQuery getSPARQLQuery() throws TranslationException {
		StringBuilder sparqlQuery = new StringBuilder();
		JSONLDContext context = new JSONLDContext();

		if (pendingError != null)
			throw pendingError;

		sparqlQuery.append("PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>\n");

		sparqlQuery.append("select ");

		if (distinct)
			sparqlQuery.append("distinct ");

		if (selectAll) {
			for (Table table : fromTables)
				selectedColumns.addAll(getAllColumns(table));
		} else if (selectAllFrom.size() > 0) {
			for (String tableName : selectAllFrom) {
				Table table = getFromTable(tableName);

				selectedColumns.addAll(getAllColumns(table));
			}
		}

		for (Column column : selectedColumns) {
			if (column.getTable().equals("")) {
				Table table = findTableWith(column.getName());
				if (table == null)
					throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'field list'", column.getName()));


				column.setTable(table.getName());
			} 

			Table table = getFromTable(column.getTable());
			if (table == null)
					throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'field list'", column.getName()));
			else {
				if (!table.getTableSchema().isManyToMany() && 
					table.getTableSchema().containsKey(column.getName()))
					column.setPrimaryKey();

				table.addColumn(column);
			
				if (!column.isPrimaryKey()) {
					SPARQLConnector.Table schemaTable = table.getTableSchema();
					SPARQLConnector.Table.Column schemaColumn = schemaTable.getColumn(column.getName());
					if (column instanceof FunctionColumn) {
						FunctionColumn functionColumn = (FunctionColumn) column;
						String contextKey = functionColumn.getAlias().isEmpty() ? functionColumn.getFunctionName() : functionColumn.getAlias();
						context.addEntry(contextKey, functionColumn.getFunctionType());
					} else {
						String contextKey = column.getAlias().isEmpty() ? column.getName() : column.getAlias();
						context.addEntry(contextKey, schemaColumn.getRDFProperty());
					}
				}
			}
		}

		for (Column column : whereColumns) {
			if (column.getTable().equals("")) {
				Table table = findTableWith(column.getName());
				if (table == null)
					throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'where clause'", column.getName()));


				column.setTable(table.getName());
			} 

			Table table = getFromTable(column.getTable());
			if (table == null)
					throw new TranslationException(String.format("ERROR 1054 (42S22): Unknown column '%s' in 'where clause'", column.getName()));
			else
				if (!table.getTableSchema().isManyToMany() && 
					table.getTableSchema().containsKey(column.getName()))
					column.setPrimaryKey();

			if (!table.containsColumn(column.getName()))
				table.addColumn(column);
		}

		Map<String, Integer> selectCounter = new HashMap<String, Integer>();
		for (Column column : selectedColumns) {
			if (!selectCounter.containsKey(column.getName()))
				selectCounter.put(column.getName(), new Integer(0));
			selectCounter.put(column.getName(), new Integer(selectCounter.get(column.getName())) + 1);
		}

		for (Column column : selectedColumns) {
			boolean qualify = selectCounter.get(column.getName()) > 1;
			sparqlQuery.append(column.getSPARQLField(qualify));	
		}

		if (!schema.getGraph().isEmpty())
			sparqlQuery.append(String.format("from <%s> ", schema.getGraph()));

		sparqlQuery.append("where {\n");

		for (Table table : fromTables) {
			String classStr = "	?%s	a	<%s> .\n";
			String propertyStr = "	optional {?%s	<%s>	?%s .}\n";

			if (table.getTableSchema().isManyToMany()) {
				SPARQLConnector.Table.Column leftColumn = table.getTableSchema().getColumn("left_id");
				SPARQLConnector.Table.Column rightColumn = table.getTableSchema().getColumn("right_id");

				sparqlQuery.append(String.format(classStr,
									VariableBinder.getInstance().bind(leftColumn.getName()),
									leftColumn.getRDFProperty()));

				sparqlQuery.append(String.format(propertyStr, 
									VariableBinder.getInstance().bind(leftColumn.getName()), 
									rightColumn.getRDFProperty(),
									VariableBinder.getInstance().bind(rightColumn.getName())));
			} else {
				sparqlQuery.append(String.format(classStr, 
									VariableBinder.getInstance().bind(table.getName()),
									table.getTableSchema().getRDFClass()));

				for (Column column : table.getColumns()) {
					if (!column.isPrimaryKey()) {
						SPARQLConnector.Table.Column columnSchema = table.getTableSchema().getColumn(column.getName());

						sparqlQuery.append(String.format(propertyStr,
											VariableBinder.getInstance().bind(table.getName()),
											columnSchema.getRDFProperty(),
											VariableBinder.getInstance().bind(column.getName())));
					}
				}
			}
		}

		sparqlQuery.append(filter);

		sparqlQuery.append("}");

		if (groupByColumns.size() > 0) {
			sparqlQuery.append(" group by");

			for (Column column : groupByColumns)
				sparqlQuery.append(String.format(" ?%s", VariableBinder.getInstance().bind(column.getName())));
		}

		sparqlQuery.append(having);

		if (orderByColumns.size() > 0) {
			sparqlQuery.append(" order by");

			for (OrderByColumn column : orderByColumns)
				sparqlQuery.append(String.format(" %s(?%s)", column.getOrder().toString().toLowerCase(), VariableBinder.getInstance().bind(column.getName())));
		}

		sparqlQuery.append(limit);

		return new TranslatedQuery(sparqlQuery.toString(), context);
	}
}