package eu.iescities.server.querymapper.datasource.query.serialization;

import java.io.IOException;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.eclipse.persistence.jaxb.MarshallerProperties;

@XmlRootElement (name="ResultSet")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={
		"count",
		"rows"
	})
public class ResultSet
{

	private int count;	
	private List<Row> rows = new ArrayList<Row>();

	public ResultSet() {

	}
	
	public ResultSet(java.sql.ResultSet results) throws SQLException {
		final ResultSetMetaData rsmd = results.getMetaData();
		while(results.next()) {
			final Row row = new Row();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				final Object value = results.getObject(i);
				row.addColumn(rsmd.getColumnName(i), value);
			}
			addRow(row);
		}
	}

	public void addRow(Row row) {
		rows.add(row);
		count += 1;
	}

	public List<Row> getRows() {
		return Collections.unmodifiableList(rows);
	}

	public int getCount() {
		return count;
	}

	public void merge(ResultSet rs) {
		this.count += rs.count;
		this.rows.addAll(rs.rows);		
	}
	
	public String toJSON() throws JAXBException, IOException {
		final JAXBContext jaxbContext = JAXBContext.newInstance(ResultSet.class);
		final Marshaller m = jaxbContext.createMarshaller();
		
		m.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(this, System.out);
		
		return "";
	}
	
	private String[] getIDs(Row row) {
		return row.getNames().toArray(new String[row.getNames().size()]);
	}

	private String[] getValues(Row row) {
		final List<String> values = new ArrayList<String>();
		for (final String column : row.getNames()) {
			final Object value = row.getValue(column);
			if (value != null) {
				values.add(value.toString());
			} else {
				values.add("");
			}
		}
		return values.toArray(new String[values.size()]);
	}
	
	private void writeLine(Object[] elements, StringBuilder strBuilder) {
		int counter = 0;
		for (Object e : elements) {
			if (counter > 0) {
				strBuilder.append(",");
			}
			
			strBuilder.append(e);
			counter++;
		}
	}
	
	public String toCSV() {
		StringBuilder strBuilder = new StringBuilder();
		
		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				writeLine(getIDs(row), strBuilder);
				strBuilder.append("\n");
				firstRow = false;
			}
			
			writeLine(getValues(row), strBuilder);
			strBuilder.append("\n");
		}
		
		return strBuilder.toString();
	}
	
	private int[] getSizes() {
		int[] sizes = null;

		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				sizes = new int[row.getNames().size()];
				firstRow = false;
			}

			final List<String> names = new ArrayList<String>(row.getNames());
			for (int i = 0; i < sizes.length; i++) {
				if (i < names.size()) {
					final Object value = row.getValue(names.get(i));
					if (value != null && value.toString().length() > sizes[i]) {
						sizes[i] = value.toString().length();
					} else {
						sizes[i] = 4;
					}
				} else {
					sizes[i] = 4;
				}
			}
		}

		return sizes;
	}
	
	private String getLine(int[] sizes) {
		StringBuilder strBuilder = new StringBuilder();

		for (int i = 0; i < sizes.length; i++) {
			String column = new String(new char[sizes[i] + 2]).replace('\0', '-');
			strBuilder.append("+" + column);
		}

		strBuilder.append("+\n");
		return strBuilder.toString();
	}
	
	private String getStrFormat(int[] sizes) {
		final StringBuilder strFormat = new StringBuilder();
			
		for (int i = 0; i < sizes.length; i++) {
			strFormat.append(String.format("| %%-%ss ", sizes[i]));
		}
		strFormat.append("|%n");

		return strFormat.toString();
	}
	
	@Override
	public String toString() {
		final StringBuilder strBuilder = new StringBuilder();

		final int[] sizes = getSizes();
		final String line = getLine(sizes);

		final String strFormat = getStrFormat(sizes);

		strBuilder.append(line);

		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				final Object[] ids = (Object[]) getIDs(row);
				strBuilder.append(String.format(strFormat, ids));
				firstRow = false;

				strBuilder.append(line);
			}
			
			strBuilder.append(String.format(strFormat, (Object[]) getValues(row)));
		}

		strBuilder.append(line);

		return strBuilder.toString();
	}
}