/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.query.DBDataSourceQuerier;
import eu.iescities.server.querymapper.datasource.database.update.DBDataSourceUpdater;
import eu.iescities.server.querymapper.datasource.json.JSONDumper.StorageType;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.SecurityFactory;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class JSONDataSource implements SQLQueriable, SQLUpdateable, JSONUpdateable {
	
	final static Logger logger = Logger.getLogger(JSONDataSource.class);

	protected final JSONDumper jsonDumper;	
	protected final DataSourceSecurityManager manager;
	private final DBDataSourceQuerier dumpDBQuerier;
	private final DBDataSourceUpdater userDBUpdater;
	private Permissions permissions;
	
	private long executionTime;
	
	protected JSONDataSource(JSONDumper jsonDumper, Permissions permissions, DataSourceSecurityManager manager, boolean forceLoad) throws DatabaseCreationException, DataSourceManagementException {
		this.jsonDumper = jsonDumper;
		this.manager = manager; 
		this.permissions = permissions;
		
		if (forceLoad) {
			jsonDumper.reloadDatabase();
		}
		
		dumpDBQuerier = new DBDataSourceQuerier(jsonDumper.getDumpDBConnector(), manager);
		userDBUpdater = new DBDataSourceUpdater(jsonDumper.getUserDBConnector(), manager);
	}

	public JSONDataSource(JSONConnector connector, String dataSourceID, boolean forceLoad) throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException {
		this.manager = SecurityFactory.getInstance().getSecurityManager();
		this.jsonDumper = new JSONDumper(connector, manager, dataSourceID, StorageType.DUMP);
		this.permissions = connector.getPermissions();
		
		if (forceLoad) {
			jsonDumper.reloadDatabase();
		}
		
		dumpDBQuerier = new DBDataSourceQuerier(jsonDumper.getDumpDBConnector(), manager);
		userDBUpdater = new DBDataSourceUpdater(jsonDumper.getUserDBConnector(), manager);
	}
	
	protected void reloadDatabase() throws DatabaseCreationException, DataSourceManagementException {
		jsonDumper.reloadDatabase();
	}

	@Override
	public ResultSet executeSQLSelect(String sqlQuery, String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException {
		logger.debug("Executing SQL query: " + sqlQuery.substring(0, 10) + " ... by '" + userid + "' on data origin " + dataOrigin);
				
		final DBDataSourceQuerier userDBQuerier = new DBDataSourceQuerier(jsonDumper.getUserDBConnector(), manager);
		
		final ResultSet results = new ResultSet();
		
		if (dataOrigin == DataOrigin.ANY || dataOrigin == DataOrigin.ORIGIN) {
			final ResultSet originalResults = dumpDBQuerier.executeSQLSelect(sqlQuery, permissions, userid);
			executionTime = dumpDBQuerier.getExecutionTime();
			results.merge(originalResults);
			logger.debug("Original data queried");
		}
		
		if (dataOrigin == DataOrigin.ANY || dataOrigin == DataOrigin.USER) {
			final ResultSet userResults = userDBQuerier.executeSQLSelect(sqlQuery, permissions, userid);
			executionTime += userDBQuerier.getExecutionTime();
			results.merge(userResults);
			logger.debug("User data queried");
		}
		
		return results;
	}

	@Override
	public ResultSet getTables() throws DataSourceManagementException {
		return dumpDBQuerier.getTables();
	}
	
	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		final DataSourceInfo info = dumpDBQuerier.getInfo();
		info.setType(ConnectorType.json);
		return info;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public int executeJSONUpdate(String json, String userid) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException {		
		final int updated = userDBUpdater.executeJSONUpdate(json, userid, permissions);
		return updated;
	}

	@Override
	public int executeSQLUpdate(String sqlUpdate, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final int updated = userDBUpdater.executeSQLUpdate(sqlUpdate, permissions, userid);
		return updated;
	}

	@Override
	public int executeSQLTransactionUpdate(String sqlUpdate, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		logger.debug("Executing SQL transactional update: " + sqlUpdate.substring(0, 10) + " ... by '" + userid);
		
		final int updated = userDBUpdater.executeSQLTransactionUpdate(sqlUpdate, permissions, userid);
		return updated;
	}

	@Override
	public Permissions getPermissions() {
		return permissions;
	}

	@Override
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public String createQueryAll() throws DataSourceManagementException {
		DatabaseLock.getInstance().adquireReadLock(jsonDumper.getDumpDBPath());
		try {
			final String query = dumpDBQuerier.createQueryAll();
			return query;
		} finally {
			DatabaseLock.getInstance().releaseReadLock(jsonDumper.getDumpDBPath());
		}
	}

	@Override
	public ResultSet queryAll(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final String query = createQueryAll();
		final ResultSet results = executeSQLSelect(query, userid, dataOrigin);
		return results;
	}

	@Override
	public JsonObject queryAllJson(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		final JsonObject results = dumpDBQuerier.queryAllCompact(permissions, userid);
		return results;
	}
}