/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.tool;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.DBDataSource;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.query.QueriableFactory;
import eu.iescities.server.querymapper.datasource.query.QueriableFactoryException;
import eu.iescities.server.querymapper.datasource.query.SPARQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;
import eu.iescities.server.querymapper.datasource.update.JSONLDUpdateable;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilderException;
import eu.iescities.server.querymapper.sql.schema.exporter.SPARQLSchemaExporter;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDResult;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import jline.ConsoleReader;

public class SQLifyTool {
	
	public enum OUTPUT_FORMATS {TABLE, JSON, JSON_LD, CSV};
	
	private static String DATASET_ID = "0";
	
	private static Thread t = null;

	private static void printHelp(Options options) {
		printMessage();
		HelpFormatter f = new HelpFormatter();
		f.printHelp("SQLifyTool", options);
	}

	private static void printMessage() {
		System.out.println("SQLify v0.1");
		System.out.println();
	}

	private static OutputStreamWriter processOutput(CommandLine line) throws IOException {
		OutputStreamWriter os = new OutputStreamWriter(System.out);
    	if (!line.hasOption("output"))
    		System.out.println("Writing output to stdout");
    	else {
    		String filePath = line.getOptionValue("output");
    		System.out.println("Writing output to " + filePath);
    		os = new FileWriter(filePath);
    	}
    	return os;
	}

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		CommandLineParser parser = new BasicParser();

		Options options = new Options();
		options.addOption("h", "help", false, "print this help message");

		options.addOption("v", "verbose", false, "show extra information");
		
		options.addOption(OptionBuilder.withLongOpt("get-schema")
                            .withDescription("gets the JSON schema description from SPARQL endpoint")
                            .hasArg()
                            .withArgName("sparqlEndpoint")
                            .create());

		options.addOption(OptionBuilder.withLongOpt("list-graphs")
                            .withDescription("lists the available graphs in the specified SPARQL endpoint")
                            .hasArg()
                            .withArgName("sparqlEndpoint")
                            .create());

		options.addOption(OptionBuilder.withLongOpt("graph")
                            .withDescription("the graph to obtain the schema from")
                            .hasArg()
                            .withArgName("graphURI")
                            .create());

		options.addOption(OptionBuilder.withLongOpt("output")
                            .withDescription("the file to write the output to")
                            .hasArg()
                            .withArgName("outputFile")
                            .create());

		options.addOption(OptionBuilder.withLongOpt("connect")
                            .withDescription("prompts for SQL queries using the specified schema file")
                            .hasArg()
                            .withArgName("schemaFile")
                            .create());

		options.addOption(OptionBuilder.withLongOpt("export-schema")
                            .withDescription("exports the specified schema in SQL format")
                            .hasArg()
                            .withArgName("schemaFile")
                            .create());
		
		options.addOption("o", "ontology", false, "use ontology to obtain available classes");

		try {
		    CommandLine line = parser.parse(options, args);

		    if(line.hasOption("help"))
		    	printHelp(options);
		    else {
		    	printMessage();
		    	if (line.hasOption("list-graphs")) {
		    		final String sparqlEndpoint = line.getOptionValue("list-graphs");
		    		listGraphs(sparqlEndpoint);
		    	} else if(line.hasOption("export-schema")) {
		    		exporSchema(line);
		    	} else if(line.hasOption("get-schema")) {
		    		getSchema(line);
		    	} else if (line.hasOption("connect")) {
			    	connect(line);
			    } else {
			    	SQLifyTool.printHelp(options);
			    }
		    }
		}
		catch(ParseException exp) {
			System.out.println(exp.getMessage());
		    printHelp(options);
		}
	}

	private static void connect(CommandLine line) {
		final String mappingFile = line.getOptionValue("connect");		    	
		final boolean verbose = line.hasOption("verbose");

		try {
			final String mapping = readSchemaFile(mappingFile);			    		    				   			    	
			final DataSourceConnector connector = ConnectorFactory.load(mapping);
			final SQLQueriable queriable = QueriableFactory.createQueriable(connector, DATASET_ID, true);
			
			final ConsoleReader consoleReader = new ConsoleReader();
			consoleReader.setBellEnabled(false);
			
			OUTPUT_FORMATS outputFormat = OUTPUT_FORMATS.TABLE;

			String command;
			while ((command = consoleReader.readLine("sqlify> ")) != null) {
				switch (command) {
					case "show tables;":	showTables(queriable);
											break;
								
					case "output json;":	outputFormat = OUTPUT_FORMATS.JSON;
											break;
											
					case "output table;":	outputFormat = OUTPUT_FORMATS.TABLE;
											break;
						
					case "output csv;":		outputFormat = OUTPUT_FORMATS.CSV;
											break;
											
					case "output json_ld;":	if (connector instanceof SPARQLConnector) {
									    		outputFormat = OUTPUT_FORMATS.JSON_LD;
											} else {
													System.out.println("Dataset is not SPARQL compatible. Cannot transform output to JSON-LD");
											}
											break;
											
					case "get_all_json;":	getAllJSON(queriable);
											break;
											
					case "get_all;":		getAll(queriable, outputFormat);
											break;
											
					case "exit;":			System.exit(0);
											break;			
											
					case "start update;":	startJob(DATASET_ID, connector);
											break;
											
					case "stop update;":	stopJob(DATASET_ID);
											break;
											
					case "stop repeat;":	if (t != null) {
												t.interrupt();
												t = null;
											}
											break;
											
					default:				if (command.startsWith("insert json_ld")){
												insertJSONLD(verbose, connector, command);
											} else if (command.startsWith("insert json")) {
												insertJSON(verbose, connector, command);
											} else if (command.startsWith("repeat")) {
												if (t == null) {
													final String[] tokens = command.split(":");
													final int time = Integer.parseInt(tokens[1]);
													final String query = tokens[2];
												
													t = new Thread(new QueryRunnable(queriable, outputFormat, query, time));
													t.start();
												}
											} else if (!command.isEmpty()){
									    		processSQLQuery(queriable, outputFormat, command);
										    }
											break;
				}
		    }
			} catch (IOException | SchedulerException | DataSourceSecurityManagerException | DataSourceConnectorException | DatabaseCreationException | DataSourceManagementException | QueriableFactoryException e) {
				System.out.println(e.getMessage());
				System.exit(0);
			} 
	}
	
	private static void startJob(String dataSourceID, DataSourceConnector connector) throws SchedulerException, IOException {
		final Scheduler sch = QuartzScheduler.getInstance().getScheduler();
		final SimpleJob simpleJob = new SimpleJob();
		
		simpleJob.createDatasourceJob(dataSourceID, connector, sch);
		
		sch.start();
	}
	
	private static void stopJob(String dataSourceID) throws SchedulerException {
		final Scheduler sch = QuartzScheduler.getInstance().getScheduler();
		final SimpleJob simpleJob = new SimpleJob();
		simpleJob.deleteJob(dataSourceID, sch);
	}

	private static void showTables(final SQLQueriable queriable) {
		try {
			final ResultSet resultSet = queriable.getTables();
			System.out.println(resultSet);
		} catch (DataSourceManagementException e) {
			System.out.println("Problem getting table information. " + e.getMessage());
		}
	}

	private static void getSchema(CommandLine line) {
		String sparqlEndpoint = line.getOptionValue("get-schema");
		System.out.println("Obtaining data from SPARQL endpoint " + sparqlEndpoint);

		String graph = "";
		if (!line.hasOption("graph"))
			System.out.println("Using DEFAULT graph");
		else {
			graph = line.getOptionValue("graph");
			System.out.println("Using graph " + graph);
		}
		
		boolean useOntology = line.hasOption("ontology");
		if (!useOntology) {
			System.out.println("Using INSPECTION mode.");
		} else {
			System.out.println("Using ONTOLOGY mode.");
		}

		processSchema(line, sparqlEndpoint, graph, useOntology);
	}

	private static void insertJSONLD(final boolean verbose,
			final DataSourceConnector connector, String sqlQuery) {
		if (connector instanceof SPARQLConnector) {
			final SPARQLConnector schema = (SPARQLConnector) connector; 
			final String fileName = sqlQuery.substring("insert json_ld".length(), sqlQuery.length()).trim();
			
			try {
				String insert = FileLoader.loadReader(new FileReader(fileName));
			
				JSONLDUpdateable updateJsonld = new SPARQLDataSource(schema, verbose);
				updateJsonld.executeJSONLDUpdate(insert);
				System.out.println("Insert correctly executed");
			} catch (IOException e) {
				System.out.println("Could not open file " + fileName);
			} catch (TranslationException e) {
				System.out.println(e.getMessage());
			}
		} else {
			System.out.println("Dataset is not SPARQL compatible. Cannot insert JSON-LD update");
		}
	}

	private static void getAll(final SQLQueriable queriable,
			OUTPUT_FORMATS outputFormat) {
		try {
			final String query = queriable.createQueryAll();
			processSQLQuery(queriable, outputFormat, query);
		} catch (DataSourceManagementException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void getAllJSON(final SQLQueriable queriable)
			throws DataSourceSecurityManagerException {
		try {
			final JsonObject result = queriable.queryAllJson(DATASET_ID, DataOrigin.ANY);
			final Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String json = gson.toJson(result);
			System.out.println(json);
		} catch (DataSourceManagementException e) {
			System.out.println(e.getMessage());
		} catch (TranslationException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void insertJSON(final boolean verbose,
			final DataSourceConnector connector, String sqlQuery) {
		if (connector instanceof DBConnector) {
			final DBConnector dbConfig = (DBConnector) connector;
			final String fileName = sqlQuery.substring("insert json".length(), sqlQuery.length()).trim();
			
			try {
				String insert = FileLoader.loadReader(new FileReader(fileName));
			
				JSONUpdateable updateJson = new DBDataSource(dbConfig, verbose);
				updateJson.executeJSONUpdate(insert, "");
				System.out.println("Insert correctly executed");
			} catch (IOException e) {
				System.out.println("Could not open file " + fileName);
			} catch (TranslationException e) {
				System.out.println(e.getMessage());
			} catch (DataSourceManagementException e) {
				System.out.println(e.getMessage());
			} catch (DataSourceSecurityManagerException e) {
				System.out.println(e.getMessage());
			}
		} else {
			System.out.println("Dataset is not SQL compatible. Cannot insert JSON update");
		}
	}

	private static void processSchema(CommandLine line, String sparqlEndpoint,
			String graph, boolean useOntology) {
		try {
			OutputStreamWriter os = processOutput(line);

			SchemaBuilder schemaBuilder = new SchemaBuilder(sparqlEndpoint, graph, useOntology);

			SPARQLConnector schema = schemaBuilder.getSchema();
			try (final Writer writer = new BufferedWriter(os)) {
				writer.write(schema.toString());
			}
		} catch (SchemaBuilderException sbe) {
			System.out.println(sbe.getMessage() + " Error executing SPARQL query:");
			System.out.println(sbe.getQuery());
			sbe.getCause().printStackTrace();
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			System.exit(0);
		}
	}

	private static void exporSchema(CommandLine line) {
    	String schemaFile = line.getOptionValue("export-schema");
		try {
			OutputStreamWriter os = processOutput(line);

			String jsonSchema = FileLoader.loadReader(new FileReader(schemaFile));			    	
			SPARQLConnector schema = SPARQLConnector.load(jsonSchema);

			SPARQLSchemaExporter schemaExporter = new SPARQLSchemaExporter(schema);

			try (final Writer writer = new BufferedWriter(os)) {
				writer.write(schemaExporter.getSQLCreateTables(false));
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
	}

	private static void listGraphs(final String sparqlEndpoint) {
		try {
			SchemaBuilder schemaBuilder = new SchemaBuilder(sparqlEndpoint, "", false);
			List<String> graphs = schemaBuilder.getGraphs();		    	

			if (graphs.isEmpty())
				System.out.println("There are no graphs in " + sparqlEndpoint);
			else {
				System.out.println("Available graphs in " + sparqlEndpoint);
				for (String g : graphs)
					System.out.println("\t" + g);
			}
		} catch (SchemaBuilderException | IOException e) {
			System.out.println(e.getMessage());
			e.getCause().printStackTrace();
		}
	}

	private static String readSchemaFile(String mappingFile)
			throws FileNotFoundException, IOException {
		try (final Reader reader = new FileReader(mappingFile)) {
			final String mapping = FileLoader.loadReader(reader);
			return mapping;
		}
	}

	static void processSQLQuery(SQLQueriable queriable,
			OUTPUT_FORMATS outputFormat, String sqlQuery) {
		long startTime = System.currentTimeMillis();

		try {
			final ResultSet results = queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
			
			switch (outputFormat) {
				case TABLE:		System.out.println(results.toString());
								break;
								
				case JSON:		System.out.println(results.toJSON());
								break;
								
				case JSON_LD:	if (queriable instanceof SPARQLQueriable) {
									final SPARQLQueriable sparqlQueriable = (SPARQLQueriable) queriable; 
									JSONLDResult jsonldResult = new JSONLDResult(results, sparqlQueriable.getJSONLDContext());
									System.out.println(jsonldResult.toJSON());
								}
								break;
								
				case CSV:		System.out.println(results.toCSV());
								break;
								
				default:
								break;
			}
			
			long diffTime = System.currentTimeMillis() - startTime;

			final DecimalFormat df = new DecimalFormat("#.##");
			System.out.println(results.getRows().size() + " rows in set (" + df.format(diffTime / 1000.0) + " sec)");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Problem executing query: " + e.getMessage());
		}
	}
}