/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.config;

import java.io.File;

public class Config {
	
	private static Config instance; 
	
	private String dataDir = "data";
	
	private String defaultPermissions = "eu.iescities.server.querymapper.datasource.security.DefaultPermissions";
	private String securityManager = "eu.iescities.server.querymapper.datasource.security.ACLSecurityManager";
	
	private int schedulerCheckInterval = 10;
	private int schedulerThreadCount = 10;
	
	private Config() {
		
	}
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}
	
	public void setDataDir(String dir) {
		dataDir = dir;
	}

	public String getDataDir() {
		return dataDir;
	}
	
	public String getDefaultPermissions() {
		return defaultPermissions;
	}

	public void setDefaultPermissions(String defaultPermissions) {
		this.defaultPermissions = defaultPermissions;
	}

	public String getSecurityManager() {
		return securityManager;
	}

	public void setSecurityManager(String securityManager) {
		this.securityManager = securityManager;
	}

	public int getSchedulerCheckInterval() {
		return schedulerCheckInterval;
	}

	public void setSchedulerCheckInterval(int schedulerCheckInterval) {
		this.schedulerCheckInterval = schedulerCheckInterval;
	}

	public int getSchedulerThreadCount() {
		return schedulerThreadCount;
	}

	public void setSchedulerThreadCount(int schedulerThreadCount) {
		this.schedulerThreadCount = schedulerThreadCount;
	}

	public boolean createDataDir() {
		final File dataDir = new File(getDataDir());
		if (!dataDir.exists()) {
			dataDir.mkdir();
			return true;
		} else {
			return false;
		}
	}
	
	private boolean deleteDir(File dir) {
		final File[] files = dir.listFiles();
		
		boolean deleted = false;
		for (File f: files) { 
			f.delete();
			deleted = true;
		}
		
		dir.delete();
		
		return deleted;
	}
	
	public boolean deleteDBDir() {
		final File dir = new File(getDataDir());
		if (dir.exists()) {
			final File[] files = dir.listFiles();
			for (File f: files) { 
				if (f.isDirectory()) {
					deleteDir(f);
				} else {
					f.delete();
				}
			}
			dir.delete();
			
			return true;
		}
		
		return false;
	}

	public boolean checkDataDir() {
		final File dir = new File(getDataDir());
		return dir.exists();
	}
}
