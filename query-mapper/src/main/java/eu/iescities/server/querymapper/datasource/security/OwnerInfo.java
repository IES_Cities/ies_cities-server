/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

public class OwnerInfo {

	private final String userid;
	private final int dataSourceID;
	private final String table;
	private final String id;
	
	public OwnerInfo(String userid, int dataSourceID, String table, String id) {
		this.userid = userid;
		this.dataSourceID = dataSourceID;
		this.table = table;
		this.id = id;
	}
	
	public String getUserID() {
		return userid;
	}

	public int getdataSourceID() {
		return dataSourceID;
	}
	
	public String getTable() {
		return table;
	}
	
	public String getID() {
		return id;
	}	
}
