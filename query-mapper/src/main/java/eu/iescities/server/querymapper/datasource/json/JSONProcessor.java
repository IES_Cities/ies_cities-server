package eu.iescities.server.querymapper.datasource.json;

import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class JSONProcessor implements AutoCloseable {

	private final JsonReader reader;
	private final Gson gson;
	
	public JSONProcessor(Reader in, String root) throws IOException {
		reader = new JsonReader(in);
		locateRoot(root);
		
		gson = new Gson();
		reader.beginArray();
	}
	
	@Override
	public void close() throws IOException {
		reader.endArray();
		reader.close();
	}
	
	private boolean locateRoot(String root) throws IOException {
    	final String[] keys = root.split("/");
    	
    	if (keys.length == 0) {
    		return true;
    	}
    	
    	int currentKey = 0;
        while (reader.hasNext()) {
        	final JsonToken token = reader.peek();
	        	
        	switch (token) {
				case BEGIN_OBJECT:
					if (handleObject(reader, keys, currentKey)) {
						return true;
					}
					break;
	
				case BEGIN_ARRAY:
					if (handleArray(reader, keys, currentKey)) {
						return true;
					}
					break;
	
				case END_DOCUMENT:
					return false;
	
				default:
					break;
        	}
    	}
        
        return false;
    }
	
	private boolean handleObject(JsonReader reader, String[] keys, int currentKey) throws IOException {
		reader.beginObject(); 
		
		while(reader.hasNext()) {
			final JsonToken token = reader.peek();
			
			switch (token) {
				case NAME:
					final String name = reader.nextName();
					if (name.equals(keys[currentKey])) {
						currentKey++;
						if (currentKey >= keys.length) {
							return true;
						}
					}
					break;
					
				case STRING:
					reader.nextString();
					break;
	
				case NUMBER:
					reader.nextString();
					break;
	
				case BOOLEAN:
					reader.nextBoolean();
					break;
	
				case NULL:
					reader.nextNull();
					break;
					
				case BEGIN_OBJECT: 	
					if (handleObject(reader, keys, currentKey)) {
						return true;
					}
					break;
				
				case BEGIN_ARRAY:	
					if (handleArray(reader, keys, currentKey)) {
						return true;
					}
					break;
	
				default:
					break;
			}
		}
		
		reader.endObject();
		return false;
	}
	
	private boolean handleArray(JsonReader reader, String[] keys, int currentKey) throws IOException {
		reader.beginArray();
		
		while (reader.hasNext()) {
			final JsonToken token = reader.peek();
        	
        	switch (token) {
				case BEGIN_OBJECT:
					if (handleObject(reader, keys, currentKey)) {
						return true;
					}
					break;
	
				case BEGIN_ARRAY:
					if (handleArray(reader, keys, currentKey)) {
						return true;
					}
					break;
					
				case STRING:
					reader.nextString();
					break;
	
				case NUMBER:
					reader.nextString();
					break;
	
				case BOOLEAN:
					reader.nextBoolean();
					break;
	
				case NULL:
					reader.nextNull();
					break;
	
				default:
					break;
        	}      
		}
		reader.endArray();
		return false;
	}
	
	public JsonObject next() throws IOException {
		 final JsonElement e = gson.fromJson(reader, JsonElement.class);
         
		 if (!e.isJsonObject()) {
         	throw new IOException("Expected array of JSON objects at line X column X");
         }
         
         return e.getAsJsonObject();
	}
	
	public boolean hasNext() throws IOException {
		return reader.hasNext();
	}
}
