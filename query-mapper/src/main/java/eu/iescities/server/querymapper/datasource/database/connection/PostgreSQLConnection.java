package eu.iescities.server.querymapper.datasource.database.connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;

public class PostgreSQLConnection extends DBConnection {

	public PostgreSQLConnection(String user, String pass, String host, String database) {
		super(DBType.PostgreSQL, user, pass, host, database);
	}
	
	@Override
	public String getConnectionURL() {
		return String.format("jdbc:postgresql://%s/%s?user=%s&password=%s",
				host, database, user, pass);
	}

	@Override
	public String getDriver() {
		return "org.postgresql.Driver";
	}

	@Override
	public int getLastInsertedId() throws DataSourceSecurityManagerException {
		throw new DataSourceSecurityManagerException("PostgreSQL database not supported");
	}

	@Override
	public DataSourceInfo getDBInfo() throws DataSourceManagementException {
		final DataSourceInfo datasourceInfo = new DataSourceInfo(ConnectorType.database);
		
		try {
			final List<String> tables = getTables();
			
			for (String tableName : tables) { 
				final ResultSet tableResult = stmt.executeQuery(String.format("SELECT column_name, data_type FROM information_schema.columns WHERE table_name = '%s'", tableName));
				
				final TableInfo tableInfo = new TableInfo(tableName);
				while (tableResult.next()) {
					final String columnName = tableResult.getString("column_name");
					final String type = tableResult.getString("data_type");
					tableInfo.addColumnInfo(new ColumnInfo(columnName, type, false));
				}
				
				datasourceInfo.addTable(tableInfo);
			}
			
			return datasourceInfo;
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		} 
	}

	@Override
	public List<String> getTables() throws DataSourceManagementException {
		final List<String> tables = new ArrayList<String>();
		try {
			final ResultSet result = stmt.executeQuery("SELECT * FROM pg_catalog.pg_tables;");
			while (result.next()) {
				tables.add(result.getString(1));
			}
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		} 
		
		return tables;
	}
}
