package eu.iescities.server.querymapper.datasource.query.serialization;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement (name="Row")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={
		"columns"
	})
public class Row
{
	@XmlPath(".")
    @XmlJavaTypeAdapter(MapAdapter.class)
	private Map<String, Object> columns = new LinkedHashMap<String, Object>();

	public Row() {}

	public void addColumn(String id, Object value) {
		columns.put(id, value);
	}

	public Object getValue(String id) {
		return columns.get(id);
	}

	public Set<String> getNames() {
		return columns.keySet();
	}
}