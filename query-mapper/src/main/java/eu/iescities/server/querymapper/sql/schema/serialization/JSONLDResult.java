/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.serialization;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;

public class JSONLDResult {
	
	private int count;
	
	private List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();

	public JSONLDResult() {

	}

	public JSONLDResult(ResultSet rs, JSONLDContext context) {
		for (final Row r : rs.getRows()) {	
			final Map<String, Object> row = new LinkedHashMap<String, Object>();
			
			final Map<String, Object> processedEntries = new LinkedHashMap<String, Object>();
			for (final Entry<String, Object> entry : context.getEntries().entrySet()) {
				if (!entry.getKey().equals("id")) {
					processedEntries.put(entry.getKey(), entry.getValue());
				}
			}
			
			row.put("@context", processedEntries);
			
			for (final String column : r.getNames()) {
				if (column.equals("id")) { 
					row.put("@id", r.getValue(column));
				} else {
					row.put(column, r.getValue(column));
				}
			}

			rows.add(row);
		}
		
		count = rows.size();
	}
	
	public int getCount() {
		return count;
	}
	
	public String toJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}
