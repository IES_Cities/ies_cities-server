package eu.iescities.server.querymapper.datasource.database.connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;

public class MySQLConnection extends DBConnection {
	
	public MySQLConnection(String user, String pass, String host, String database) {
		super(DBType.MySQL, user, pass, host, database);
	}

	@Override
	public String getConnectionURL() {
		return String.format("jdbc:mysql://%s/%s?user=%s&password=%s", host, database, user, pass);
	}

	@Override
	public String getDriver() {
		return "com.mysql.jdbc.Driver";
	}

	@Override
	public int getLastInsertedId() throws DataSourceSecurityManagerException {
		throw new DataSourceSecurityManagerException("MySQL database not supported");
	}

	@Override
	public DataSourceInfo getDBInfo() throws DataSourceManagementException {
		final DataSourceInfo datasourceInfo = new DataSourceInfo(ConnectorType.database);
		
		try {
			final List<String> tables = getTables();
			
			for (String tableName : tables) { 
				final java.sql.ResultSet tableResult = stmt.executeQuery(String.format("SHOW COLUMNS FROM %s", tableName));
				
				final TableInfo tableInfo = new TableInfo(tableName);
				while (tableResult.next()) {
					final String columnName = tableResult.getString("Field");
					final String type = tableResult.getString("Type");
					tableInfo.addColumnInfo(new ColumnInfo(columnName, type, false));
				}
				
				datasourceInfo.addTable(tableInfo);
			}
			
			return datasourceInfo;
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		}
	}

	@Override
	public List<String> getTables() throws DataSourceManagementException {
		final List<String> tables = new ArrayList<String>();
		try {
			final ResultSet result = stmt.executeQuery("show tables;");
			while (result.next()) {
				tables.add(result.getString(1));
			}
		} catch (SQLException e) {
			throw new DataSourceManagementException("Cannot obtain table information");
		} 
		
		return tables;
	}
}
