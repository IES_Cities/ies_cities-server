/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.csv;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.json.JSONConnector;

public class CSVConnector extends JSONConnector {

	@Expose
	private String delimiter;
	
	public CSVConnector() {
		super(ConnectorType.csv);
	}
	
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public static CSVConnector load(String json) {
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
		return gson.fromJson(json, CSVConnector.class);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof CSVConnector)) {
			return false;
		}
		
		final CSVConnector csvConnector = (CSVConnector) o;
		return super.equals(o)
			&& this.delimiter.equals(csvConnector.delimiter);
	}
}