/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.update;

import java.util.ArrayList;
import java.util.List;

import eu.iescities.server.querymapper.datasource.database.query.SQLValidator;

public class TransactionParser {

	public static class SQLStatement {
		
		private final String sql;
		private final SQLValidator.Type type;
		
		public SQLStatement(String sql, SQLValidator.Type type) {
			this.sql = sql;
			this.type = type;
		}

		public String getSql() {
			return sql;
		}

		public SQLValidator.Type getType() {
			return type;
		}
	}
	
	public List<SQLStatement> parse(String text) {
		final List<SQLStatement> stmts = new ArrayList<SQLStatement>();
		
		final String[] queries = text.split(";\\s*");
		
		final SQLValidator sqlValidator = new SQLValidator();
		for (String query : queries) {
			if (sqlValidator.isValid(query)) {
				stmts.add(new SQLStatement(query, sqlValidator.getStmtType(query)));
			}
		}
		
		return stmts;
	}
}
