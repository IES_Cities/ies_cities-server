/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.httpapi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;

public class HTTPApiConnector extends DataSourceConnector {

	@Expose
	private String baseURL;

	public HTTPApiConnector() {
		super(ConnectorType.httpapi);
	}

	public HTTPApiConnector(String baseURL) {
		super(ConnectorType.httpapi);
		this.baseURL = baseURL;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public static HTTPApiConnector load(String json) {
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss")
				.create();
		return gson.fromJson(json, HTTPApiConnector.class);
	}

	public static HTTPApiConnector load(InputStream is) throws IOException {
		InputStreamReader reader = new InputStreamReader(is);
		try {
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss")
					.create();
			return gson.fromJson(reader, HTTPApiConnector.class);
		} catch (JsonSyntaxException jse) {
			throw new IOException(jse);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof HTTPApiConnector)) {
			return false;
		}
		
		final HTTPApiConnector httpApiConnector = (HTTPApiConnector) o;
		return super.equals(o)
				&& this.baseURL.equals(httpApiConnector.baseURL);
	}
}
