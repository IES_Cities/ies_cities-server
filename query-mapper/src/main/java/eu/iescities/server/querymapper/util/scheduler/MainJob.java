package eu.iescities.server.querymapper.util.scheduler;

import java.io.IOException;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;

public abstract class MainJob implements Job {

	public static final String MAIN_JOB_NAME = "MAIN_JOB";
	
	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		checkJobs(jobContext.getScheduler());		
	}
	
	public abstract void checkJobs(Scheduler sch);
	
	protected boolean isValidConnector(DataSourceConnector connector) {
		return connector.getType() == ConnectorType.json || connector.getType() == ConnectorType.csv;
	}
	
	protected void stopJobs(List<String> availableMappings, Scheduler sch) throws SchedulerException {
		for (final String groupName : sch.getJobGroupNames()) {
			for (final JobKey jobKey : sch.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
				final String jobName = jobKey.getName();
				if (!jobName.equals(MAIN_JOB_NAME) && !availableMappings.contains(jobName)) {
					deleteJob(jobName, sch);
				}
			}
		}
	}
	
	public void createDatasourceJob(String dataSourceID, DataSourceConnector connector, Scheduler sch) throws IOException, SchedulerException {
		final JobDetail job = JobBuilder
				.newJob(DataDownloadJob.class)
				.withIdentity(dataSourceID)
				.build();
		
		if (!sch.checkExists(job.getKey())) {	
			job.getJobDataMap().put(DataDownloadJob.CONNECTOR, connector);
			job.getJobDataMap().put(DataDownloadJob.ID, dataSourceID);
	
			final Trigger trigger = TriggerBuilder.newTrigger()
				.withSchedule(SimpleScheduleBuilder.simpleSchedule()
				.withIntervalInSeconds(connector.getRefresh())
				.withMisfireHandlingInstructionFireNow()
				.repeatForever()).build();

			sch.scheduleJob(job, trigger);
		}
	}

	public boolean isRunning(String dataSourceID, Scheduler sch) throws SchedulerException {
		final JobKey jobKey = new JobKey(dataSourceID);
		return sch.checkExists(jobKey);
	}
	
	public void deleteJob(String dataSourceID, Scheduler sch) {
		final JobKey jobKey = new JobKey(dataSourceID); 
		try {
			if (sch.checkExists(jobKey)) {
				sch.deleteJob(new JobKey(dataSourceID));
			}
		} catch (SchedulerException e) {
			System.out.println(String.format("Problem stopping job for datasource '%s'", dataSourceID));
		}
	}
}
