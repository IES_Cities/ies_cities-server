/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database;

import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.query.DBDataSourceQuerier;
import eu.iescities.server.querymapper.datasource.database.update.DBDataSourceUpdater;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.SecurityFactory;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;

public class DBDataSource implements SQLQueriable, SQLUpdateable, JSONUpdateable {

	private final DBConnector connector;
	
	private final DataSourceSecurityManager manager;
	private final DBDataSourceQuerier querier;
	private final DBDataSourceUpdater updater;
	
	public DBDataSource(DBConnector connector, boolean verbose) throws DataSourceSecurityManagerException {
		this.connector = connector;
		this.manager = SecurityFactory.getInstance().getSecurityManager();
		this.querier = new DBDataSourceQuerier(connector, manager);
		this.updater = new DBDataSourceUpdater(connector, manager);
	}

	@Override
	public ResultSet executeSQLSelect(String sqlQuery, String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException {
		return querier.executeSQLSelect(sqlQuery, connector.getPermissions(), userid);
	}
	
	@Override
	public ResultSet getTables() throws DataSourceManagementException {
		return querier.getTables();
	}
	
	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {
		return querier.getInfo();
	}

	@Override
	public long getExecutionTime() {
		return querier.getExecutionTime();
	}
	
	@Override
	public int executeSQLUpdate(String sqlUpdate, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeSQLUpdate(sqlUpdate, connector.getPermissions(), userid);
	}

	@Override
	public int executeJSONUpdate(String json, String userid) throws TranslationException, DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeJSONUpdate(json, userid, connector.getPermissions());
	}

	@Override
	public int executeSQLTransactionUpdate(String text, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		return updater.executeSQLTransactionUpdate(text, connector.getPermissions(), userid);
	}

	@Override
	public Permissions getPermissions() {
		return connector.getPermissions();
	}

	@Override
	public void setPermissions(Permissions permissions) {
		connector.setPermissions(permissions);
	}

	@Override
	public String createQueryAll() throws DataSourceManagementException {
		return connector.createQueryAll();
	}
	
	@Override
	public ResultSet queryAll(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final String query = createQueryAll();
		return executeSQLSelect(query, userid, dataOrigin);
	}

	@Override
	public JsonObject queryAllJson(String userid, DataOrigin dataOrigin) throws DataSourceManagementException, DataSourceSecurityManagerException, TranslationException {
		return querier.queryAllCompact(connector.getPermissions(), userid);
	}
}