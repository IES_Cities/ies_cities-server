package eu.iescities.server.querymapper.datasource;

@SuppressWarnings("serial")
public class DataSourceConnectorException extends Exception {

	public DataSourceConnectorException(String message) {
		super(message);
	}
}
