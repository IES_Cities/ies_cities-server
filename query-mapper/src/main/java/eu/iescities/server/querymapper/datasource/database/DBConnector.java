/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database;

import java.io.StringWriter;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.Expose;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.connection.DBConnection;
import eu.iescities.server.querymapper.datasource.database.connection.DBConnectionFactory;
import eu.iescities.server.querymapper.datasource.json.StringQuoter;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.datasource.security.AccessInfo;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.Permissions.ActionType;

public class DBConnector extends DataSourceConnector implements AutoCloseable {
	
	final static Logger logger = Logger.getLogger(DBConnector.class);

	public enum DBType {
		PostgreSQL, SQLite, MySQL
	};

	@Expose
	private DBType dbType;

	@Expose
	private String host;
	
	@Expose
	private String database;
	
	@Expose
	private String user;
	
	@Expose
	private String pass;
	
	private DBConnection connection = null;

	public DBConnector() {
		super(ConnectorType.database);
	}

	public DBConnector(DBType dbType, String host, String database, String user, String pass) throws DataSourceManagementException {
		super(ConnectorType.database);
		
		this.dbType = dbType;
		this.host = host;
		this.database = database;
		this.user = user;
		this.pass = pass;
		
		this.connection = DBConnectionFactory.createDatabase(this);
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String getDatabase() {
		return database;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}
	
	public DBType getDbType() {
		return dbType;
	}

	public void setDbType(DBType dbType) {
		this.dbType = dbType;
	}

	public String getConnectionURL() {
		return connection.getConnectionURL();
	}
	
	@Override
	public DataSourceInfo getInfo() throws DataSourceManagementException {					
		return connection.getDBInfo();			 
	} 
	
	public List<String> getTables() throws DataSourceManagementException {
		return connection.getTables();
	}

	public DBConnector open() throws DataSourceManagementException {
		if (connection == null) {
			this.connection = DBConnectionFactory.createDatabase(this);
		}
		connection.open();
		logger.debug("Connection open");
		return this;
	}
	
	@Override
	public void close() throws DataSourceManagementException {
		connection.close();
		logger.debug("Connection closed");
	}
	
	public DBConnector beginTransaction() throws DataSourceManagementException {
		if (connection == null) {
			this.connection = DBConnectionFactory.createDatabase(this);
		}
		connection.beginTransaction();
		return this;
	}
	
	public void commit() throws DataSourceManagementException {
		connection.commit();
	}
	
	public void rollback() throws DataSourceManagementException {
		connection.rollback();
	}

	public static DBConnector load(String json) {
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
		return gson.fromJson(json, DBConnector.class);
	}
	
	private List<String> getTables(ResultSetMetaData metadata) throws SQLException {
		final List <String> tables = new ArrayList<String>();
		for (int i = 1; i <= metadata.getColumnCount(); i++) {
			final String table = metadata.getTableName(i);
			tables.add(table);
		}

		return tables;
	}
	
	public ResultSet executeQuery(String query, Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		logger.debug("Executing query " + query.substring(0, 10) + " by '" + userid + "'");
		
		try {
			final java.sql.ResultSet results = connection.getStatement().executeQuery(query);
			
			for (String table : getTables(results.getMetaData())) {	
				table = StringQuoter.unquote(table);
				final AccessInfo info = new AccessInfo(userid, table, query, connection.getConnection());
				if (!manager.isAuthorized(info, p, ActionType.SELECT)) {
					throw new DataSourceSecurityManagerException(String.format("User '%s' not authorized to execute select", userid));
				}
			}
		
			final java.sql.ResultSet newResults = connection.getStatement().executeQuery(query);
			final ResultSet resultSet = new ResultSet(newResults);
			return resultSet;
		} catch (SQLException e) {
			logger.debug("Could not execute query on database. " + e.getMessage());
			throw new DataSourceManagementException("Could not execute query on database", e);
		}
	}
	
	private String getDeletedTable(String delete) {
		final String partialStmt = delete.substring("delete from ".length(), delete.length());
		final int endIndex = partialStmt.indexOf(" ");
		
		String tableName = ""; 
		if (endIndex >= 0) {
			tableName = partialStmt.substring(0, endIndex);
		} else {
			tableName = partialStmt;
		}
		
		return tableName.replace(";", "");
	}
	
	private String getInsertedTable(String insert) {
		final String partialStmt = insert.substring("insert into ".length(), insert.length());
		final int endIndex = partialStmt.indexOf(" ");
		
		return partialStmt.substring(0, endIndex);
	}
	
	private String getUpdatedTable(String update) {
		final String partialStmt = update.substring("update ".length(), update.length());
		final int endIndex = partialStmt.indexOf(" ");
		
		return partialStmt.substring(0, endIndex);
	} 
	
	public int executeInsert(String insert, Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		logger.debug("Executing insert " + insert.substring(0, 10) + " by '" + userid + "'");
		
		final String table = StringQuoter.unquote(getInsertedTable(insert));
		
		final AccessInfo info = new AccessInfo(userid, table, insert, connection.getConnection());
		if (manager.isAuthorized(info, p, ActionType.INSERT)) {
			try {
				final int rows = connection.getStatement().executeUpdate(insert);
				
				try {
					insertOwner(userid, table, connection.getStatement());
				} catch (DataSourceSecurityManagerException e) {
					System.out.println(e.getMessage());
				}
				
				return rows;
			} catch (SQLException e) {
				throw new DataSourceManagementException("Could not execute update on database. " + e.getMessage());
			}			
		} else {
			throw new DataSourceSecurityManagerException(String.format("User '%s' not authorized to execute insert", userid));
		}
	}

	private void insertOwner(String userid, final String table, final Statement stmt) throws DataSourceSecurityManagerException {
		final int insertedRowId = connection.getLastInsertedId();
		try {
			final DataSourceInfo datasourceInfo = getInfo();
			final TableInfo tableInfo = datasourceInfo.getTable(table);
			if (tableInfo != null) {
				final ColumnInfo columnInfo = tableInfo.getPrimaryKey();
				if (columnInfo != null) {
					final String updateUser = String.format("UPDATE %s SET _row_user_='%s' WHERE rowid=%d", table, userid, insertedRowId);
					try {
						stmt.executeUpdate(updateUser);
					} catch (SQLException e) {
						throw new DataSourceSecurityManagerException("Unable to insert security information. " + e.getMessage());
					}
				} else {
					throw new DataSourceSecurityManagerException(String.format("Unable to insert security information. Could not obtain primary key info for table '%s'.", table));
				}
			} else {
				throw new DataSourceSecurityManagerException(String.format("Unable to insert security information. Could not obtain info for table '%s'", table));
			}
		} catch (DataSourceManagementException e) {
			throw new DataSourceSecurityManagerException("Unable to insert security information. Could not obtain dataset information."); 
		}
	}
	
	public int executeDelete(String delete, Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final String table = StringQuoter.unquote(getDeletedTable(delete));
		
		final AccessInfo info = new AccessInfo(userid, table, delete, connection.getConnection());
		if (manager.isAuthorized(info, p, ActionType.DELETE)) {
			try {
				final int rows = connection.getStatement().executeUpdate(delete);
				return rows;
			} catch (SQLException e) {
				throw new DataSourceManagementException("Could not execute delete on database", e);
			}
		} else {
			throw new DataSourceSecurityManagerException(String.format("User '%s' not authorized to execute delete", userid));
		}
	}
	
	public int executeUpdate(String update, Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		final String table = getUpdatedTable(update);
		
		final AccessInfo info = new AccessInfo(userid, table, update, connection.getConnection());
		if (manager.isAuthorized(info, p, ActionType.UPDATE)) {
			try {
				final int rows = connection.getStatement().executeUpdate(update);
				return rows;
			} catch (SQLException e) {
				throw new DataSourceManagementException("Could not execute delete on database", e); 
			}
		} else {
			throw new DataSourceSecurityManagerException(String.format("User '%s' not authorized to execute update", userid));
		}
	}
	
	public int executeUpdate(String update) throws SQLException {
		final int rows = connection.getStatement().executeUpdate(update);
		return rows;
	}
	
	public List<TableInfo> getChildTables(String tableName) throws DataSourceManagementException {
		final List<TableInfo> childTables = new ArrayList<TableInfo>();
		
		final DataSourceInfo info = getInfo();
		for (TableInfo tableInfo : info.getTables()) {
			if (tableInfo.getName().startsWith(tableName)
				&& !tableInfo.getName().equals(tableName)) {
				final String subStr = tableInfo.getName().substring(tableName.length() + 1, tableInfo.getName().length());
				if (!subStr.contains("_")) {
					childTables.add(tableInfo);
				}
			}
		}
		
		return childTables;
	}
	
	private boolean isRootTable(TableInfo tableInfo) {
		for (ColumnInfo columnInfo : tableInfo.getColumns()) {
			if (columnInfo.getName().equals("parent_id")) {
				return false;
			}
		}
		
		return true;
	}
	
	public TableInfo getRootTable() throws DataSourceManagementException {
		final DataSourceInfo info = getInfo();
		
		for (TableInfo tableInfo : info.getTables()) {
			if (isRootTable(tableInfo)) {
				return tableInfo;
			}
		}
		
		return null;
	}
	
	private List<String> getWhereClauses(TableInfo tableInfo) throws DataSourceManagementException {
		final List<String> clauses = new ArrayList<String>();
		
		final List<TableInfo> childs = getChildTables(tableInfo.getName());
		for (TableInfo child : childs) {
			clauses.add(tableInfo.getName()
					+ "." + tableInfo.getPrimaryKey().getName()
					+ " = "
					+ child.getName() + ".parent_id");
			
			clauses.addAll(getWhereClauses(child));
		}
		
		return clauses;
	}
	
	public String createQueryAll() throws DataSourceManagementException {
		final TableInfo rootTable = getRootTable();
		final List<String> whereClauses = getWhereClauses(rootTable); 
				 
		final StringWriter writer = new StringWriter();
		
		final DataSourceInfo info = getInfo();
		
		writer.write("SELECT ");
		int counter = 0;
		for (TableInfo tableInfo : info.getTables()) {
			for (ColumnInfo columnInfo : tableInfo.getColumns()) {
				if (!columnInfo.getName().equals("_id")
					&& !columnInfo.getName().equals("parent_id")) {

					if (counter > 0) {
						writer.write(", ");
					}
					
					writer.write(tableInfo.getName() + "." + columnInfo.getName());
					counter++;
				}
			}
		}
		
		writer.write(" FROM ");
		counter = 0;
		for (TableInfo tableInfo : info.getTables()) {
			if (counter > 0) {
				writer.write(", ");
			}
			writer.write(tableInfo.getName());
			counter++;
		}
		
		if (!whereClauses.isEmpty()) {
			writer.write(" WHERE ");
			counter = 0;
			for (String clause : whereClauses) {
				if (counter > 0) {
					writer.write(" AND ");
				}
				
				writer.write(clause);				
				counter++;
			}
		}
		
		writer.write(";");
		
		return writer.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DBConnector)) {
			return false;
		}
		
		final DBConnector dbConnector = (DBConnector) o;
		
		return super.equals(o)
				&& this.dbType.equals(dbConnector.dbType)
				&& this.host.equals(dbConnector.host)
				&& this.database.equals(dbConnector.database)
				&& ((this.user == null && dbConnector.user == null) || this.user.equals(dbConnector.user))
				&& ((this.pass == null && dbConnector.pass == null)) || this.pass.equals(dbConnector.pass);
	}
	
	private String createSelectAll(TableInfo tableInfo) {
		return "SELECT * FROM " + tableInfo.getName();
	}
	
	private String createSelectSubTables(TableInfo tableInfo, String parentID, ColumnInfo parentInfo) {
		if (parentInfo.getType().equals("VARCHAR")) {
			return "SELECT * FROM " + tableInfo.getName() + " WHERE parent_id = '" + parentID + "'";
		} else {
			return "SELECT * FROM " + tableInfo.getName() + " WHERE parent_id = " + parentID; 
		}
	}
	
	private JsonArray compactArray(JsonArray array) {
		final JsonArray compactArray = new JsonArray();
		
		for (int i = 0; i < array.size(); i++) {
			final JsonObject obj = (JsonObject) array.get(i);
			if (obj.entrySet().size() > 1) {
				return array;
			} else {
				final String key = obj.entrySet().iterator().next().getKey();
				compactArray.add(obj.get(key));
			}
		}
		
		return compactArray;
	}
	
	private boolean isArray(String value) {
		return value.startsWith("@[") && value.endsWith("]");
	}
	
	private JsonArray createArray(String value) {
		final JsonArray array = new JsonArray();
		for (String v : value.substring(2, value.length() - 1).split(",")) {
			array.add(new JsonPrimitive(v));
		}
		return array;
	}
	
	private JsonArray getChildArray(TableInfo tableInfo, String parentID, ColumnInfo parentIDInfo, Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, SQLException, DataSourceSecurityManagerException {
		final JsonArray array = new JsonArray();
		final List<TableInfo> childs = getChildTables(tableInfo.getName());
		
		final String query;
		
		if (parentID == null)  {
			query = createSelectAll(tableInfo);
		} else {
			query = createSelectSubTables(tableInfo, parentID, parentIDInfo);
		}
		
		final ResultSet result = executeQuery(query, p, manager, userid);
		for (final Row row : result.getRows()) {
			final JsonObject rowObj = new JsonObject();
			for (String column : row.getNames()) {
				if (!column.equals("_id") && !column.equals("parent_id")) {
					final Object value = row.getValue(column);
					if (value != null) {
						if (isArray(value.toString())) {
							final JsonArray arrayValue = createArray(value.toString());
							rowObj.add(column, arrayValue);
						} else {
							rowObj.addProperty(column, value.toString());
						}
					} else {
						rowObj.addProperty(column, "");
					}
				}
			}
	
			for (TableInfo child : childs) {
				final String primaryKey = tableInfo.getPrimaryKey().getName();
				final ColumnInfo cInfo = tableInfo.getColumn(primaryKey);
				final JsonArray childArray = getChildArray(child, row.getValue(primaryKey).toString(), cInfo, p, manager, userid);
				final int index = child.getName().lastIndexOf('_');
				final String keyName = child.getName().substring(index + 1, child.getName().length());
				rowObj.add(keyName, compactArray(childArray));
			}
			
			array.add(rowObj);
		}
		
		return array;
	}

	public JsonObject selectAllCompact(Permissions p, DataSourceSecurityManager manager, String userid) throws DataSourceManagementException, DataSourceSecurityManagerException {
		try {
			final TableInfo rootTable = getRootTable(); 
			final JsonObject mainObj = new JsonObject();
			final JsonArray array = getChildArray(rootTable, null, null, p, manager, userid);
			
			mainObj.add(rootTable.getName(), array);
			return mainObj;
		} catch (SQLException e) {
			throw new DataSourceManagementException("Could not select all information from database", e);
		}
	}

	public void closeAll() throws DataSourceManagementException {
		connection.closeAll();
	}
}