package eu.iescities.server.querymapper.datasource.database.connection;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;

public class DBConnectionFactory {
 
	public static DBConnection createDatabase(DBConnector connector) throws DataSourceManagementException {
		switch (connector.getDbType()) {
			case SQLite: 		return new SQLiteConnection(connector.getDatabase());
			
			case PostgreSQL:	return new PostgreSQLConnection(connector.getUser(), 
									connector.getPass(), connector.getHost(), connector.getDatabase());
							
			case MySQL:			return new MySQLConnection(connector.getUser(), 
									connector.getPass(), connector.getHost(), connector.getDatabase());
		}
		
		throw new DataSourceManagementException("Unsupported database type");
	}
}
