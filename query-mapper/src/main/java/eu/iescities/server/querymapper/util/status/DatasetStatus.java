package eu.iescities.server.querymapper.util.status;

public class DatasetStatus {

	private String id;
	private String status;
	private String message;
	private String timestamp;
	
	public DatasetStatus() {
	
	}
	
	public DatasetStatus(String id, String status, String message, String timestamp) {
		this.id = id;
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
