/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLValidator {

	public enum Type { INSERT, DELETE, UPDATE, INVALID };
	
	private final Pattern pattern = Pattern.compile("(?i).+;\\s*(INSERT|SELECT|UPDATE|DELETE|CREATE|DROP).*");
	
	public boolean isValid(String stmt) {
		if (stmt.isEmpty()) {
			return false;
		}
		
		final Matcher matcher = pattern.matcher(stmt);
		
		return !matcher.matches();
	}
	
	public Type getStmtType(String stmt) {
		if (stmt.toUpperCase().startsWith("INSERT")) {
			return Type.INSERT;
		} else if (stmt.toUpperCase().startsWith("DELETE")) {
			return Type.DELETE;
		} else if (stmt.toUpperCase().startsWith("UPDATE")) {
			return Type.UPDATE;
		}
		
		return Type.INVALID;
	}
}
