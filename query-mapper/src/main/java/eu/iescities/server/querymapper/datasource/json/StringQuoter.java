package eu.iescities.server.querymapper.datasource.json;

public class StringQuoter {

	public static String quote(String str) {
		str = unquote(str);
		return String.format("\"%s\"", str);
	}

	public static String unquote(String str) {
		if (str != null && ((str.startsWith("\"") 
				&& str.endsWith("\"")) 
				|| (str.startsWith("'") && str.endsWith("'")))) {
			str = str.substring(1, str.length() - 1);
		}
		return str;
	}
}
