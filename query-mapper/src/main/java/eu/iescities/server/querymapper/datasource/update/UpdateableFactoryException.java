package eu.iescities.server.querymapper.datasource.update;

@SuppressWarnings("serial")
public class UpdateableFactoryException extends Exception {

	public UpdateableFactoryException(String message) {
		super(message);
	}
}
