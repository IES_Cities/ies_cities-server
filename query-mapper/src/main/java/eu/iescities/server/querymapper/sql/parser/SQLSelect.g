/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
 
 
/*
  Compatible with ANTLR 4
  
  Grammar for SQL select statement based on code obtained from
  https://code.google.com/p/fado/source/browse/trunk/source/fado/parse/GenericSQL.g
*/

grammar SQLSelect;

@parser::header 
{ 
package es.deustotech.sqlify.parser;
}

@lexer::header 
{ 
package es.deustotech.sqlify.parser;
}

statement
  : select ( SEMI )? EOF
  ;

subSelect
  : select
  | LPAREN select RPAREN
  ;
    
select
  : SELECT
    ( ALL
      | DISTINCT
//      | UNIQUE 
    )?
//    ( TOP Integer ( PERCENT )? )?
    itemList
    from
//    ( joinList )?
    ( where )?
    ( groupBy )?
    ( having )?
    ( orderBy )?
    ( limit )?
  ;
  
columnList
  : LPAREN columnName ( COMMA columnName )* RPAREN
  ;
  
itemList
  : STAR
  | item ( COMMA item )*
  ;
  
item
  : value ( ( AS )? alias )?
  | allColumns
  | function ( AS? alias )? 
  ;

allColumns
  : tableAlias DOT STAR
  ;

alias
  : Identifier
  ;
  
function
  : functionName LPAREN ( value ( COMMA value )* )? RPAREN
  ;  

functionName
  : COUNT
  | MIN
  | MAX
  | AVG
  ;
  
from
  : FROM fromItem ( COMMA fromItem )*
  ;
  
fromItem
  : ( ( LPAREN subSelect RPAREN ) 
    | tableRef 
    )
    ( ( AS )? alias )?
  ;

//joinList
//  : ( join )*
//  ;
  
//join
//  : 
//    ( JOIN
//    | INNER JOIN
//    | LEFT JOIN
//    | LEFT OUTER JOIN
//    | RIGHT JOIN 
//    | RIGHT OUTER JOIN
//    | OUTER JOIN 
//    | NATURAL JOIN
//    ) 
//  fromItem
//  ( ON conditionList
//  | USING LPAREN columnRef ( COMMA columnRef )* RPAREN
//  )?
//  ;

where
  : WHERE conditionList
  ;
  
groupBy
  : GROUP BY columnRef ( COMMA columnRef )*
  ;
  
having
  : HAVING conditionList
  ;
  
orderBy
  : ORDER BY orderByItem ( COMMA orderByItem )*
  ;
  
orderByItem
  : columnRef ( ASC | DESC )?
  ;

limit
  : LIMIT Integer
  ;
  
nestedCondition
  : LPAREN conditionList RPAREN
  ;
  
conditionList
  : condition ( ( OR | AND ) condition )*
  ;
  
condition
  : ( NOT )?
    ( nestedCondition
//    | in
    | between
    | isNull
//    | exists
    | like
//    | quantifier
	  | comparison
    )
  ;

//in
//  : expression ( NOT )? IN LPAREN ( subSelect | expressionList ) RPAREN
//  ;
  
between
  : expression ( NOT )? BETWEEN expression AND expression
  ;
  
isNull
  : expression IS ( NOT )? NULL
  ;
  
//exists
//  : EXISTS expression
//  ;
  
like  
  : expression ( NOT )? LIKE expression
  ;
    
comparison
  : expression comparator expression
  ;

comparator 
  : EQ
  | NEQ1
  | NEQ2
  | LTE
  | LT
  | GTE
  | GT
  ;
  
//quantifier
//  : expression ( ALL | ANY | SOME ) LPAREN subSelect RPAREN
//  ;
  
expressionList
  : expression ( COMMA expression )*
  ;

nestedExpression
  : LPAREN expression RPAREN
  ;
  
expression
//  : multiply ( ( PLUS | MINUS ) multiply )*
    : value 
  ;
 
//multiply
//  : value ( ( STAR | DIVIDE ) value )* 
//  ;
    
value
  : literal 
  |// ( unary )?
    ( columnRef
    | nestedExpression
    )
  ;
  
literal
  : ( unary )? Float
  | ( unary )? Integer
  | String
  | TRUE
  | FALSE
  | date
  | StringDate 
  ;

date
  : '{d' Timestamp '}' // Date
  | '{t' Timestamp '}' // Time
  | '{ts' Timestamp '}' // Timestamp
  ;
    
unary
  : MINUS
  | PLUS
  ;  

tableRef
  : tableName
//  | databaseName DOT tableName
  ;
  
columnRef
  : columnName 
  | tableAlias DOT columnName
  ;

//databaseName
//  : Identifier
//  | QuotedIdentifier
//  ;
  
tableName
  : Identifier
  | QuotedIdentifier
  ;
  
tableAlias
  : Identifier
  ;
  
columnName
  : Identifier
  | QuotedIdentifier
  ;
  
ALL       : A L L ;
AND       : A N D ;
ANY       : A N Y ;
AS        : A S ;
ASC       : A S C ;
BETWEEN   : B E T W E E N ;
BY        : B Y ;
CASE      : C A S E ;
DESC      : D E S C ;
DISTINCT  : D I S T I N C T ;
ELSE      : E L S E ;
END       : E N D ;
EXISTS    : E X I S T S ;
FALSE     : F A L S E ;
FROM      : F R O M ;
FULL      : F U L L ;
GROUP     : G R O U P ;
LIMIT     : L I M I T ;
HAVING    : H A V I N G ;
IN        : I N ;
INNER     : I N N E R ;
IS        : I S ;
JOIN      : J O I N ;
LEFT      : L E F T ;
LIKE      : L I K E ;
NATURAL   : N A T U R A L ;
NOT       : N O T ;
NULL      : N U L L ;
ON        : O N ;
OR        : O R ;
ORDER     : O R D E R ;
OUTER     : O U T E R ;
PERCENT   : P E R C E N T ;
RIGHT     : R I G H T ;
SELECT    : S E L E C T ;
SET       : S E T ;
SOME      : S O M E ;
THEN      : T H E N ;
TRUE      : T R U E ;
TOP       : T O P ;
UNION     : U N I O N ;
UNIQUE    : U N I Q U E ;
USING     : U S I N G ;
WHEN      : W H E N ;
WHERE     : W H E R E ;

MAX       : M A X ;
MIN       : M I N ;
COUNT     : C O U N T ;
AVG       : A V G ;

fragment A:('a'|'A');
fragment B:('b'|'B');
fragment C:('c'|'C');
fragment D:('d'|'D');
fragment E:('e'|'E');
fragment F:('f'|'F');
fragment G:('g'|'G');
fragment H:('h'|'H');
fragment I:('i'|'I');
fragment J:('j'|'J');
fragment K:('k'|'K');
fragment L:('l'|'L');
fragment M:('m'|'M');
fragment N:('n'|'N');
fragment O:('o'|'O');
fragment P:('p'|'P');
fragment Q:('q'|'Q');
fragment R:('r'|'R');
fragment S:('s'|'S');
fragment T:('t'|'T');
fragment U:('u'|'U');
fragment V:('v'|'V');
fragment W:('w'|'W');
fragment X:('x'|'X');
fragment Y:('y'|'Y');
fragment Z:('z'|'Z');

DOT      : '.'  ;
COMMA    : ','  ;
LPAREN   : '('  ;
RPAREN   : ')'  ;
LCURLY   : '{'  ;
RCURLY   : '}'  ;
STRCAT   : '||' ;
QUESTION : '?'  ;
COLON    : ':'  ;
SEMI     : ';'  ;

EQ       : '='  ;
NEQ1     : '<>' ;
NEQ2     : '!=' ;
LTE      : '<=' ;
LT       : '<'  ;
GTE      : '>=' ;
GT       : '>'  ;

PLUS     : '+'  ;
MINUS    : '-'  ;
DIVIDE   : '/'  ;
STAR     : '*'  ;
MOD      : '%'  ;

fragment
Digit : '0'..'9' ;

Integer 
  : ( Digit )+ 
  ;
  
Float
  : ('0'..'9')+ '.' ('0'..'9')* Exponent?
  | '.' ('0'..'9')+ Exponent?
  | ('0'..'9')+ Exponent
  ;
  
fragment
Exponent 
  : ('e'|'E') ('+'|'-')? ('0'..'9')+ 
  ;

StringDate
  : '\'' Digit Digit Digit Digit '-' Digit Digit '-' Digit Digit '\''
  ;

String 
  : '\'' .*? '\''
  ;
  
Timestamp 
  : Digit Digit Digit Digit '-'
		Digit Digit '-' 
		Digit Digit ( 't' | ' ' )
		Digit Digit ':' Digit Digit ':' Digit Digit
	;

Identifier 
  : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_'|'$')*
  ;

QuotedIdentifier 
  : '[' .*? ']'
  | '"' .*? '"'
  ;
 
Comment
  : ('--' ~('\n'|'\r')* '\r'? '\n' 
      | '//' ~('\n'|'\r')* '\r'? '\n' 
      | '/*' .*? '*/'
  ) -> channel(HIDDEN)
  ;
  
Whitespace 
  : ( '\t' | ' ' | '\r' | '\n' )+ -> channel(HIDDEN)
	;
