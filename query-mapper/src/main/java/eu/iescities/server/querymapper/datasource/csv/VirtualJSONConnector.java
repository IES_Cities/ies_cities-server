/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.math.NumberUtils;

import eu.iescities.server.querymapper.datasource.csv.parser.CSVFormat;
import eu.iescities.server.querymapper.datasource.csv.parser.CSVParser;
import eu.iescities.server.querymapper.datasource.csv.parser.CSVRecord;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class VirtualJSONConnector extends JSONConnector {
	
	private final CSVConnector connector;
	private final String fileName;
	
	public VirtualJSONConnector(CSVConnector connector, String fileName) {
		this.connector = connector;
		this.fileName = fileName;
		
		setKey(connector.getKey());
		setRoot("results");
		setTable(connector.getTable());
	}
	
	private static LiteralType guessType(String value) {
		if (NumberUtils.isNumber(value)) {
			try {
				final double d = Double.parseDouble(value);
				if (d % 1 == 0) {
					return LiteralType.INT;
				} else {
					return LiteralType.FLOAT;
				}
			} catch (NumberFormatException e) {
				return LiteralType.STRING;
			}
		} else {
			return LiteralType.STRING;
		}
	}
	
	private void transformCSV(Reader reader, Writer writer) throws IOException {
		writer.write("{ \"results\": [\n");
		
		try {
			try (final CSVParser parser = new CSVParser(reader, 
					CSVFormat.DEFAULT
						.withHeader()
						.withDelimiter(connector.getDelimiter().charAt(0))
						.withIgnoreEmptyLines(true)
						.withAllowMissingColumnNames(true))) {
				final Set<String> headers = parser.getHeaderMap().keySet();
				
				final SafeHeaderManager safeHeaderManager = new SafeHeaderManager();
				
				int objCounter = 0;
				for (CSVRecord record : parser) {
					if (objCounter > 0) {
		    			writer.write(",\n");
		    		}
		    		
		    		writer.write("\t{\n");
					
		    		int propertyCounter = 0;
					for (String header : headers) {
						if (!header.isEmpty()) {
							if (propertyCounter > 0) {
			    				writer.write(",\n");
			    			}
							
							final String value = record.get(header);
							
							final String safeHeader = safeHeaderManager.getHeader(header);
							
							switch(guessType(value)) {
								case INT: 
								case FLOAT:		writer.write("\t\t\"" + safeHeader + "\": " +  value);
												break;
												
								default:		writer.write("\t\t\"" + safeHeader + "\": \"" + StringEscapeUtils.escapeJava(value) + "\"");
												break;
							}										
							
							propertyCounter++;
						}
					}
					writer.write("\n\t}");
					
					objCounter++;
				}
				
				writer.write("\n\t]\n}\n");
			}
		} catch (RuntimeException e) {
			throw new IOException("Error parsing CSV file. " + e.getMessage());
		}
	}
		
	@Override
	public Reader getReader() throws MalformedURLException, IOException {
		return new InputStreamReader(new FileInputStream(fileName));
	}

	public void transformCSV() throws MalformedURLException, IOException {
		try (final Reader reader = new BufferedReader(connector.getReader())) {
			try (final Writer writer = new BufferedWriter(new FileWriter(fileName))) {
				transformCSV(reader, writer);
			}
		}
	}
}