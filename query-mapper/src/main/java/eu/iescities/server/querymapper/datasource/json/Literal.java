package eu.iescities.server.querymapper.datasource.json;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.google.gson.JsonPrimitive;

import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class Literal {
	
	private String value;
	private LiteralType type;
	
	public Literal(JsonPrimitive jsonPrimitive) {
		this.value = jsonPrimitive.getAsString();
		this.type = guessType(jsonPrimitive);
	}
	
	public Literal(String value, LiteralType type) {
		this.value = value;
		this.type = type;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public LiteralType getType() {
		return type;
	}
	
	public void setType(LiteralType type) {
		this.type = type;
	}
	
	private LiteralType guessType(JsonPrimitive value) {
		if (value.isNumber()) {
			final double d = Double.parseDouble(value.getAsString());
			if (d % 1 == 0) {
				return LiteralType.INT;
			} else {
				return LiteralType.FLOAT;
			}
		} else if (value.isString()) {
			final String strValue = value.getAsString();
			if (strValue.contains("-")) {
				final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					if (sdf.parse(value.getAsString()) != null) {
						return LiteralType.DATE;
					}
			
				} catch (ParseException e) {
					return LiteralType.STRING;
				}
			} else {
				return LiteralType.STRING;
			}
		}
		
		return LiteralType.STRING;
	}
}
