// Generated from SQLSelect.g by ANTLR 4.1
 
package eu.iescities.server.querymapper.sql.parser;

import java.util.List;

import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNSimulator;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLSelectParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__2=1, T__1=2, T__0=3, ALL=4, AND=5, ANY=6, AS=7, ASC=8, BETWEEN=9, BY=10, 
		CASE=11, DESC=12, DISTINCT=13, ELSE=14, END=15, EXISTS=16, FALSE=17, FROM=18, 
		FULL=19, GROUP=20, LIMIT=21, HAVING=22, IN=23, INNER=24, IS=25, JOIN=26, 
		LEFT=27, LIKE=28, NATURAL=29, NOT=30, NULL=31, ON=32, OR=33, ORDER=34, 
		OUTER=35, PERCENT=36, RIGHT=37, SELECT=38, SET=39, SOME=40, THEN=41, TRUE=42, 
		TOP=43, UNION=44, UNIQUE=45, USING=46, WHEN=47, WHERE=48, MAX=49, MIN=50, 
		COUNT=51, AVG=52, DOT=53, COMMA=54, LPAREN=55, RPAREN=56, LCURLY=57, RCURLY=58, 
		STRCAT=59, QUESTION=60, COLON=61, SEMI=62, EQ=63, NEQ1=64, NEQ2=65, LTE=66, 
		LT=67, GTE=68, GT=69, PLUS=70, MINUS=71, DIVIDE=72, STAR=73, MOD=74, Integer=75, 
		Float=76, StringDate=77, String=78, Timestamp=79, Identifier=80, QuotedIdentifier=81, 
		Comment=82, Whitespace=83;
	public static final String[] tokenNames = {
		"<INVALID>", "'{ts'", "'{t'", "'{d'", "ALL", "AND", "ANY", "AS", "ASC", 
		"BETWEEN", "BY", "CASE", "DESC", "DISTINCT", "ELSE", "END", "EXISTS", 
		"FALSE", "FROM", "FULL", "GROUP", "LIMIT", "HAVING", "IN", "INNER", "IS", 
		"JOIN", "LEFT", "LIKE", "NATURAL", "NOT", "NULL", "ON", "OR", "ORDER", 
		"OUTER", "PERCENT", "RIGHT", "SELECT", "SET", "SOME", "THEN", "TRUE", 
		"TOP", "UNION", "UNIQUE", "USING", "WHEN", "WHERE", "MAX", "MIN", "COUNT", 
		"AVG", "'.'", "','", "'('", "')'", "'{'", "'}'", "'||'", "'?'", "':'", 
		"';'", "'='", "'<>'", "'!='", "'<='", "'<'", "'>='", "'>'", "'+'", "'-'", 
		"'/'", "'*'", "'%'", "Integer", "Float", "StringDate", "String", "Timestamp", 
		"Identifier", "QuotedIdentifier", "Comment", "Whitespace"
	};
	public static final int
		RULE_statement = 0, RULE_subSelect = 1, RULE_select = 2, RULE_columnList = 3, 
		RULE_itemList = 4, RULE_item = 5, RULE_allColumns = 6, RULE_alias = 7, 
		RULE_function = 8, RULE_functionName = 9, RULE_from = 10, RULE_fromItem = 11, 
		RULE_where = 12, RULE_groupBy = 13, RULE_having = 14, RULE_orderBy = 15, 
		RULE_orderByItem = 16, RULE_limit = 17, RULE_nestedCondition = 18, RULE_conditionList = 19, 
		RULE_condition = 20, RULE_between = 21, RULE_isNull = 22, RULE_like = 23, 
		RULE_comparison = 24, RULE_comparator = 25, RULE_expressionList = 26, 
		RULE_nestedExpression = 27, RULE_expression = 28, RULE_value = 29, RULE_literal = 30, 
		RULE_date = 31, RULE_unary = 32, RULE_tableRef = 33, RULE_columnRef = 34, 
		RULE_tableName = 35, RULE_tableAlias = 36, RULE_columnName = 37;
	public static final String[] ruleNames = {
		"statement", "subSelect", "select", "columnList", "itemList", "item", 
		"allColumns", "alias", "function", "functionName", "from", "fromItem", 
		"where", "groupBy", "having", "orderBy", "orderByItem", "limit", "nestedCondition", 
		"conditionList", "condition", "between", "isNull", "like", "comparison", 
		"comparator", "expressionList", "nestedExpression", "expression", "value", 
		"literal", "date", "unary", "tableRef", "columnRef", "tableName", "tableAlias", 
		"columnName"
	};

	@Override
	public String getGrammarFileName() { return "SQLSelect.g"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public SQLSelectParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SQLSelectParser.EOF, 0); }
		public SelectContext select() {
			return getRuleContext(SelectContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(SQLSelectParser.SEMI, 0); }
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76); select();
			setState(78);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
				setState(77); match(SEMI);
				}
			}

			setState(80); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubSelectContext extends ParserRuleContext {
		public SelectContext select() {
			return getRuleContext(SelectContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public SubSelectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSelect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterSubSelect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitSubSelect(this);
		}
	}

	public final SubSelectContext subSelect() throws RecognitionException {
		SubSelectContext _localctx = new SubSelectContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_subSelect);
		try {
			setState(87);
			switch (_input.LA(1)) {
			case SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(82); select();
				}
				break;
			case LPAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(83); match(LPAREN);
				setState(84); select();
				setState(85); match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectContext extends ParserRuleContext {
		public HavingContext having() {
			return getRuleContext(HavingContext.class,0);
		}
		public TerminalNode ALL() { return getToken(SQLSelectParser.ALL, 0); }
		public OrderByContext orderBy() {
			return getRuleContext(OrderByContext.class,0);
		}
		public FromContext from() {
			return getRuleContext(FromContext.class,0);
		}
		public WhereContext where() {
			return getRuleContext(WhereContext.class,0);
		}
		public TerminalNode DISTINCT() { return getToken(SQLSelectParser.DISTINCT, 0); }
		public LimitContext limit() {
			return getRuleContext(LimitContext.class,0);
		}
		public GroupByContext groupBy() {
			return getRuleContext(GroupByContext.class,0);
		}
		public ItemListContext itemList() {
			return getRuleContext(ItemListContext.class,0);
		}
		public TerminalNode SELECT() { return getToken(SQLSelectParser.SELECT, 0); }
		public SelectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterSelect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitSelect(this);
		}
	}

	public final SelectContext select() throws RecognitionException {
		SelectContext _localctx = new SelectContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_select);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89); match(SELECT);
			setState(91);
			_la = _input.LA(1);
			if (_la==ALL || _la==DISTINCT) {
				{
				setState(90);
				_la = _input.LA(1);
				if ( !(_la==ALL || _la==DISTINCT) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			setState(93); itemList();
			setState(94); from();
			setState(96);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(95); where();
				}
			}

			setState(99);
			_la = _input.LA(1);
			if (_la==GROUP) {
				{
				setState(98); groupBy();
				}
			}

			setState(102);
			_la = _input.LA(1);
			if (_la==HAVING) {
				{
				setState(101); having();
				}
			}

			setState(105);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(104); orderBy();
				}
			}

			setState(108);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(107); limit();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnListContext extends ParserRuleContext {
		public ColumnNameContext columnName(int i) {
			return getRuleContext(ColumnNameContext.class,i);
		}
		public List<ColumnNameContext> columnName() {
			return getRuleContexts(ColumnNameContext.class);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public ColumnListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterColumnList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitColumnList(this);
		}
	}

	public final ColumnListContext columnList() throws RecognitionException {
		ColumnListContext _localctx = new ColumnListContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_columnList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110); match(LPAREN);
			setState(111); columnName();
			setState(116);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(112); match(COMMA);
				setState(113); columnName();
				}
				}
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(119); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ItemListContext extends ParserRuleContext {
		public ItemContext item(int i) {
			return getRuleContext(ItemContext.class,i);
		}
		public List<ItemContext> item() {
			return getRuleContexts(ItemContext.class);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode STAR() { return getToken(SQLSelectParser.STAR, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public ItemListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_itemList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterItemList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitItemList(this);
		}
	}

	public final ItemListContext itemList() throws RecognitionException {
		ItemListContext _localctx = new ItemListContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_itemList);
		int _la;
		try {
			setState(130);
			switch (_input.LA(1)) {
			case STAR:
				enterOuterAlt(_localctx, 1);
				{
				setState(121); match(STAR);
				}
				break;
			case 1:
			case 2:
			case 3:
			case FALSE:
			case TRUE:
			case MAX:
			case MIN:
			case COUNT:
			case AVG:
			case LPAREN:
			case PLUS:
			case MINUS:
			case Integer:
			case Float:
			case StringDate:
			case String:
			case Identifier:
			case QuotedIdentifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(122); item();
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(123); match(COMMA);
					setState(124); item();
					}
					}
					setState(129);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ItemContext extends ParserRuleContext {
		public TerminalNode AS() { return getToken(SQLSelectParser.AS, 0); }
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public AllColumnsContext allColumns() {
			return getRuleContext(AllColumnsContext.class,0);
		}
		public AliasContext alias() {
			return getRuleContext(AliasContext.class,0);
		}
		public ItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_item; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitItem(this);
		}
	}

	public final ItemContext item() throws RecognitionException {
		ItemContext _localctx = new ItemContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_item);
		int _la;
		try {
			setState(147);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(132); value();
				setState(137);
				_la = _input.LA(1);
				if (_la==AS || _la==Identifier) {
					{
					setState(134);
					_la = _input.LA(1);
					if (_la==AS) {
						{
						setState(133); match(AS);
						}
					}

					setState(136); alias();
					}
				}

				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(139); allColumns();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(140); function();
				setState(145);
				_la = _input.LA(1);
				if (_la==AS || _la==Identifier) {
					{
					setState(142);
					_la = _input.LA(1);
					if (_la==AS) {
						{
						setState(141); match(AS);
						}
					}

					setState(144); alias();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllColumnsContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(SQLSelectParser.DOT, 0); }
		public TerminalNode STAR() { return getToken(SQLSelectParser.STAR, 0); }
		public TableAliasContext tableAlias() {
			return getRuleContext(TableAliasContext.class,0);
		}
		public AllColumnsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allColumns; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterAllColumns(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitAllColumns(this);
		}
	}

	public final AllColumnsContext allColumns() throws RecognitionException {
		AllColumnsContext _localctx = new AllColumnsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_allColumns);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149); tableAlias();
			setState(150); match(DOT);
			setState(151); match(STAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AliasContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(SQLSelectParser.Identifier, 0); }
		public AliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitAlias(this);
		}
	}

	public final AliasContext alias() throws RecognitionException {
		AliasContext _localctx = new AliasContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_alias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153); match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public FunctionNameContext functionName() {
			return getRuleContext(FunctionNameContext.class,0);
		}
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitFunction(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155); functionName();
			setState(156); match(LPAREN);
			setState(165);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 3) | (1L << FALSE) | (1L << TRUE) | (1L << LPAREN))) != 0) || ((((_la - 70)) & ~0x3f) == 0 && ((1L << (_la - 70)) & ((1L << (PLUS - 70)) | (1L << (MINUS - 70)) | (1L << (Integer - 70)) | (1L << (Float - 70)) | (1L << (StringDate - 70)) | (1L << (String - 70)) | (1L << (Identifier - 70)) | (1L << (QuotedIdentifier - 70)))) != 0)) {
				{
				setState(157); value();
				setState(162);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(158); match(COMMA);
					setState(159); value();
					}
					}
					setState(164);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(167); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionNameContext extends ParserRuleContext {
		public TerminalNode COUNT() { return getToken(SQLSelectParser.COUNT, 0); }
		public TerminalNode AVG() { return getToken(SQLSelectParser.AVG, 0); }
		public TerminalNode MIN() { return getToken(SQLSelectParser.MIN, 0); }
		public TerminalNode MAX() { return getToken(SQLSelectParser.MAX, 0); }
		public FunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitFunctionName(this);
		}
	}

	public final FunctionNameContext functionName() throws RecognitionException {
		FunctionNameContext _localctx = new FunctionNameContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_functionName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MAX) | (1L << MIN) | (1L << COUNT) | (1L << AVG))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromContext extends ParserRuleContext {
		public FromItemContext fromItem(int i) {
			return getRuleContext(FromItemContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public TerminalNode FROM() { return getToken(SQLSelectParser.FROM, 0); }
		public List<FromItemContext> fromItem() {
			return getRuleContexts(FromItemContext.class);
		}
		public FromContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_from; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterFrom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitFrom(this);
		}
	}

	public final FromContext from() throws RecognitionException {
		FromContext _localctx = new FromContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_from);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171); match(FROM);
			setState(172); fromItem();
			setState(177);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(173); match(COMMA);
				setState(174); fromItem();
				}
				}
				setState(179);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromItemContext extends ParserRuleContext {
		public TerminalNode AS() { return getToken(SQLSelectParser.AS, 0); }
		public SubSelectContext subSelect() {
			return getRuleContext(SubSelectContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public TableRefContext tableRef() {
			return getRuleContext(TableRefContext.class,0);
		}
		public AliasContext alias() {
			return getRuleContext(AliasContext.class,0);
		}
		public FromItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fromItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterFromItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitFromItem(this);
		}
	}

	public final FromItemContext fromItem() throws RecognitionException {
		FromItemContext _localctx = new FromItemContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_fromItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			switch (_input.LA(1)) {
			case LPAREN:
				{
				{
				setState(180); match(LPAREN);
				setState(181); subSelect();
				setState(182); match(RPAREN);
				}
				}
				break;
			case Identifier:
			case QuotedIdentifier:
				{
				setState(184); tableRef();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(191);
			_la = _input.LA(1);
			if (_la==AS || _la==Identifier) {
				{
				setState(188);
				_la = _input.LA(1);
				if (_la==AS) {
					{
					setState(187); match(AS);
					}
				}

				setState(190); alias();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(SQLSelectParser.WHERE, 0); }
		public ConditionListContext conditionList() {
			return getRuleContext(ConditionListContext.class,0);
		}
		public WhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_where; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterWhere(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitWhere(this);
		}
	}

	public final WhereContext where() throws RecognitionException {
		WhereContext _localctx = new WhereContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_where);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193); match(WHERE);
			setState(194); conditionList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupByContext extends ParserRuleContext {
		public List<ColumnRefContext> columnRef() {
			return getRuleContexts(ColumnRefContext.class);
		}
		public ColumnRefContext columnRef(int i) {
			return getRuleContext(ColumnRefContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode BY() { return getToken(SQLSelectParser.BY, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public TerminalNode GROUP() { return getToken(SQLSelectParser.GROUP, 0); }
		public GroupByContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupBy; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterGroupBy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitGroupBy(this);
		}
	}

	public final GroupByContext groupBy() throws RecognitionException {
		GroupByContext _localctx = new GroupByContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_groupBy);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196); match(GROUP);
			setState(197); match(BY);
			setState(198); columnRef();
			setState(203);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(199); match(COMMA);
				setState(200); columnRef();
				}
				}
				setState(205);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HavingContext extends ParserRuleContext {
		public TerminalNode HAVING() { return getToken(SQLSelectParser.HAVING, 0); }
		public ConditionListContext conditionList() {
			return getRuleContext(ConditionListContext.class,0);
		}
		public HavingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_having; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterHaving(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitHaving(this);
		}
	}

	public final HavingContext having() throws RecognitionException {
		HavingContext _localctx = new HavingContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_having);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206); match(HAVING);
			setState(207); conditionList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByContext extends ParserRuleContext {
		public OrderByItemContext orderByItem(int i) {
			return getRuleContext(OrderByItemContext.class,i);
		}
		public List<OrderByItemContext> orderByItem() {
			return getRuleContexts(OrderByItemContext.class);
		}
		public TerminalNode ORDER() { return getToken(SQLSelectParser.ORDER, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public TerminalNode BY() { return getToken(SQLSelectParser.BY, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public OrderByContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderBy; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterOrderBy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitOrderBy(this);
		}
	}

	public final OrderByContext orderBy() throws RecognitionException {
		OrderByContext _localctx = new OrderByContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_orderBy);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209); match(ORDER);
			setState(210); match(BY);
			setState(211); orderByItem();
			setState(216);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(212); match(COMMA);
				setState(213); orderByItem();
				}
				}
				setState(218);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByItemContext extends ParserRuleContext {
		public TerminalNode DESC() { return getToken(SQLSelectParser.DESC, 0); }
		public ColumnRefContext columnRef() {
			return getRuleContext(ColumnRefContext.class,0);
		}
		public TerminalNode ASC() { return getToken(SQLSelectParser.ASC, 0); }
		public OrderByItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterOrderByItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitOrderByItem(this);
		}
	}

	public final OrderByItemContext orderByItem() throws RecognitionException {
		OrderByItemContext _localctx = new OrderByItemContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_orderByItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(219); columnRef();
			setState(221);
			_la = _input.LA(1);
			if (_la==ASC || _la==DESC) {
				{
				setState(220);
				_la = _input.LA(1);
				if ( !(_la==ASC || _la==DESC) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitContext extends ParserRuleContext {
		public TerminalNode Integer() { return getToken(SQLSelectParser.Integer, 0); }
		public TerminalNode LIMIT() { return getToken(SQLSelectParser.LIMIT, 0); }
		public LimitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterLimit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitLimit(this);
		}
	}

	public final LimitContext limit() throws RecognitionException {
		LimitContext _localctx = new LimitContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_limit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223); match(LIMIT);
			setState(224); match(Integer);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NestedConditionContext extends ParserRuleContext {
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public ConditionListContext conditionList() {
			return getRuleContext(ConditionListContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public NestedConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nestedCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterNestedCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitNestedCondition(this);
		}
	}

	public final NestedConditionContext nestedCondition() throws RecognitionException {
		NestedConditionContext _localctx = new NestedConditionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_nestedCondition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226); match(LPAREN);
			setState(227); conditionList();
			setState(228); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionListContext extends ParserRuleContext {
		public List<ConditionContext> condition() {
			return getRuleContexts(ConditionContext.class);
		}
		public List<TerminalNode> AND() { return getTokens(SQLSelectParser.AND); }
		public ConditionContext condition(int i) {
			return getRuleContext(ConditionContext.class,i);
		}
		public List<TerminalNode> OR() { return getTokens(SQLSelectParser.OR); }
		public TerminalNode AND(int i) {
			return getToken(SQLSelectParser.AND, i);
		}
		public TerminalNode OR(int i) {
			return getToken(SQLSelectParser.OR, i);
		}
		public ConditionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterConditionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitConditionList(this);
		}
	}

	public final ConditionListContext conditionList() throws RecognitionException {
		ConditionListContext _localctx = new ConditionListContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_conditionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230); condition();
			setState(235);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND || _la==OR) {
				{
				{
				setState(231);
				_la = _input.LA(1);
				if ( !(_la==AND || _la==OR) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(232); condition();
				}
				}
				setState(237);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public ComparisonContext comparison() {
			return getRuleContext(ComparisonContext.class,0);
		}
		public TerminalNode NOT() { return getToken(SQLSelectParser.NOT, 0); }
		public BetweenContext between() {
			return getRuleContext(BetweenContext.class,0);
		}
		public IsNullContext isNull() {
			return getRuleContext(IsNullContext.class,0);
		}
		public NestedConditionContext nestedCondition() {
			return getRuleContext(NestedConditionContext.class,0);
		}
		public LikeContext like() {
			return getRuleContext(LikeContext.class,0);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(238); match(NOT);
				}
			}

			setState(246);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				setState(241); nestedCondition();
				}
				break;

			case 2:
				{
				setState(242); between();
				}
				break;

			case 3:
				{
				setState(243); isNull();
				}
				break;

			case 4:
				{
				setState(244); like();
				}
				break;

			case 5:
				{
				setState(245); comparison();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BetweenContext extends ParserRuleContext {
		public TerminalNode BETWEEN() { return getToken(SQLSelectParser.BETWEEN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(SQLSelectParser.AND, 0); }
		public TerminalNode NOT() { return getToken(SQLSelectParser.NOT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public BetweenContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_between; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterBetween(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitBetween(this);
		}
	}

	public final BetweenContext between() throws RecognitionException {
		BetweenContext _localctx = new BetweenContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_between);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248); expression();
			setState(250);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(249); match(NOT);
				}
			}

			setState(252); match(BETWEEN);
			setState(253); expression();
			setState(254); match(AND);
			setState(255); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IsNullContext extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(SQLSelectParser.NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode IS() { return getToken(SQLSelectParser.IS, 0); }
		public TerminalNode NULL() { return getToken(SQLSelectParser.NULL, 0); }
		public IsNullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_isNull; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterIsNull(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitIsNull(this);
		}
	}

	public final IsNullContext isNull() throws RecognitionException {
		IsNullContext _localctx = new IsNullContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_isNull);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257); expression();
			setState(258); match(IS);
			setState(260);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(259); match(NOT);
				}
			}

			setState(262); match(NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LikeContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode NOT() { return getToken(SQLSelectParser.NOT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode LIKE() { return getToken(SQLSelectParser.LIKE, 0); }
		public LikeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_like; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterLike(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitLike(this);
		}
	}

	public final LikeContext like() throws RecognitionException {
		LikeContext _localctx = new LikeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_like);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(264); expression();
			setState(266);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(265); match(NOT);
				}
			}

			setState(268); match(LIKE);
			setState(269); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ComparatorContext comparator() {
			return getRuleContext(ComparatorContext.class,0);
		}
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitComparison(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_comparison);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(271); expression();
			setState(272); comparator();
			setState(273); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparatorContext extends ParserRuleContext {
		public TerminalNode GTE() { return getToken(SQLSelectParser.GTE, 0); }
		public TerminalNode LT() { return getToken(SQLSelectParser.LT, 0); }
		public TerminalNode NEQ2() { return getToken(SQLSelectParser.NEQ2, 0); }
		public TerminalNode LTE() { return getToken(SQLSelectParser.LTE, 0); }
		public TerminalNode GT() { return getToken(SQLSelectParser.GT, 0); }
		public TerminalNode NEQ1() { return getToken(SQLSelectParser.NEQ1, 0); }
		public TerminalNode EQ() { return getToken(SQLSelectParser.EQ, 0); }
		public ComparatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterComparator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitComparator(this);
		}
	}

	public final ComparatorContext comparator() throws RecognitionException {
		ComparatorContext _localctx = new ComparatorContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_comparator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(275);
			_la = _input.LA(1);
			if ( !(((((_la - 63)) & ~0x3f) == 0 && ((1L << (_la - 63)) & ((1L << (EQ - 63)) | (1L << (NEQ1 - 63)) | (1L << (NEQ2 - 63)) | (1L << (LTE - 63)) | (1L << (LT - 63)) | (1L << (GTE - 63)) | (1L << (GT - 63)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLSelectParser.COMMA); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(SQLSelectParser.COMMA, i);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitExpressionList(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_expressionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(277); expression();
			setState(282);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(278); match(COMMA);
				setState(279); expression();
				}
				}
				setState(284);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NestedExpressionContext extends ParserRuleContext {
		public TerminalNode RPAREN() { return getToken(SQLSelectParser.RPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(SQLSelectParser.LPAREN, 0); }
		public NestedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nestedExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterNestedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitNestedExpression(this);
		}
	}

	public final NestedExpressionContext nestedExpression() throws RecognitionException {
		NestedExpressionContext _localctx = new NestedExpressionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_nestedExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(285); match(LPAREN);
			setState(286); expression();
			setState(287); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289); value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public NestedExpressionContext nestedExpression() {
			return getRuleContext(NestedExpressionContext.class,0);
		}
		public ColumnRefContext columnRef() {
			return getRuleContext(ColumnRefContext.class,0);
		}
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_value);
		try {
			setState(296);
			switch (_input.LA(1)) {
			case 1:
			case 2:
			case 3:
			case FALSE:
			case TRUE:
			case PLUS:
			case MINUS:
			case Integer:
			case Float:
			case StringDate:
			case String:
				enterOuterAlt(_localctx, 1);
				{
				setState(291); literal();
				}
				break;
			case LPAREN:
			case Identifier:
			case QuotedIdentifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(294);
				switch (_input.LA(1)) {
				case Identifier:
				case QuotedIdentifier:
					{
					setState(292); columnRef();
					}
					break;
				case LPAREN:
					{
					setState(293); nestedExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(SQLSelectParser.String, 0); }
		public TerminalNode Integer() { return getToken(SQLSelectParser.Integer, 0); }
		public TerminalNode TRUE() { return getToken(SQLSelectParser.TRUE, 0); }
		public TerminalNode Float() { return getToken(SQLSelectParser.Float, 0); }
		public TerminalNode StringDate() { return getToken(SQLSelectParser.StringDate, 0); }
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public DateContext date() {
			return getRuleContext(DateContext.class,0);
		}
		public TerminalNode FALSE() { return getToken(SQLSelectParser.FALSE, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_literal);
		int _la;
		try {
			setState(311);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(299);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(298); unary();
					}
				}

				setState(301); match(Float);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(303);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(302); unary();
					}
				}

				setState(305); match(Integer);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(306); match(String);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(307); match(TRUE);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(308); match(FALSE);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(309); date();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(310); match(StringDate);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateContext extends ParserRuleContext {
		public TerminalNode Timestamp() { return getToken(SQLSelectParser.Timestamp, 0); }
		public DateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_date; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterDate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitDate(this);
		}
	}

	public final DateContext date() throws RecognitionException {
		DateContext _localctx = new DateContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_date);
		try {
			setState(322);
			switch (_input.LA(1)) {
			case 3:
				enterOuterAlt(_localctx, 1);
				{
				setState(313); match(3);
				setState(314); match(Timestamp);
				setState(315); match(RCURLY);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(316); match(2);
				setState(317); match(Timestamp);
				setState(318); match(RCURLY);
				}
				break;
			case 1:
				enterOuterAlt(_localctx, 3);
				{
				setState(319); match(1);
				setState(320); match(Timestamp);
				setState(321); match(RCURLY);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(SQLSelectParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(SQLSelectParser.PLUS, 0); }
		public UnaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterUnary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitUnary(this);
		}
	}

	public final UnaryContext unary() throws RecognitionException {
		UnaryContext _localctx = new UnaryContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_unary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(324);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableRefContext extends ParserRuleContext {
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TableRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterTableRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitTableRef(this);
		}
	}

	public final TableRefContext tableRef() throws RecognitionException {
		TableRefContext _localctx = new TableRefContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_tableRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(326); tableName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnRefContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(SQLSelectParser.DOT, 0); }
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public TableAliasContext tableAlias() {
			return getRuleContext(TableAliasContext.class,0);
		}
		public ColumnRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterColumnRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitColumnRef(this);
		}
	}

	public final ColumnRefContext columnRef() throws RecognitionException {
		ColumnRefContext _localctx = new ColumnRefContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_columnRef);
		try {
			setState(333);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(328); columnName();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(329); tableAlias();
				setState(330); match(DOT);
				setState(331); columnName();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNameContext extends ParserRuleContext {
		public TerminalNode QuotedIdentifier() { return getToken(SQLSelectParser.QuotedIdentifier, 0); }
		public TerminalNode Identifier() { return getToken(SQLSelectParser.Identifier, 0); }
		public TableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterTableName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitTableName(this);
		}
	}

	public final TableNameContext tableName() throws RecognitionException {
		TableNameContext _localctx = new TableNameContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_tableName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			_la = _input.LA(1);
			if ( !(_la==Identifier || _la==QuotedIdentifier) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableAliasContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(SQLSelectParser.Identifier, 0); }
		public TableAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableAlias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterTableAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitTableAlias(this);
		}
	}

	public final TableAliasContext tableAlias() throws RecognitionException {
		TableAliasContext _localctx = new TableAliasContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_tableAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(337); match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameContext extends ParserRuleContext {
		public TerminalNode QuotedIdentifier() { return getToken(SQLSelectParser.QuotedIdentifier, 0); }
		public TerminalNode Identifier() { return getToken(SQLSelectParser.Identifier, 0); }
		public ColumnNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).enterColumnName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLSelectListener ) ((SQLSelectListener)listener).exitColumnName(this);
		}
	}

	public final ColumnNameContext columnName() throws RecognitionException {
		ColumnNameContext _localctx = new ColumnNameContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_columnName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			_la = _input.LA(1);
			if ( !(_la==Identifier || _la==QuotedIdentifier) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3U\u0158\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\5\2Q\n\2\3\2\3\2\3"+
		"\3\3\3\3\3\3\3\3\3\5\3Z\n\3\3\4\3\4\5\4^\n\4\3\4\3\4\3\4\5\4c\n\4\3\4"+
		"\5\4f\n\4\3\4\5\4i\n\4\3\4\5\4l\n\4\3\4\5\4o\n\4\3\5\3\5\3\5\3\5\7\5u"+
		"\n\5\f\5\16\5x\13\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6\u0080\n\6\f\6\16\6\u0083"+
		"\13\6\5\6\u0085\n\6\3\7\3\7\5\7\u0089\n\7\3\7\5\7\u008c\n\7\3\7\3\7\3"+
		"\7\5\7\u0091\n\7\3\7\5\7\u0094\n\7\5\7\u0096\n\7\3\b\3\b\3\b\3\b\3\t\3"+
		"\t\3\n\3\n\3\n\3\n\3\n\7\n\u00a3\n\n\f\n\16\n\u00a6\13\n\5\n\u00a8\n\n"+
		"\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\7\f\u00b2\n\f\f\f\16\f\u00b5\13\f\3"+
		"\r\3\r\3\r\3\r\3\r\5\r\u00bc\n\r\3\r\5\r\u00bf\n\r\3\r\5\r\u00c2\n\r\3"+
		"\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\7\17\u00cc\n\17\f\17\16\17\u00cf"+
		"\13\17\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\7\21\u00d9\n\21\f\21\16"+
		"\21\u00dc\13\21\3\22\3\22\5\22\u00e0\n\22\3\23\3\23\3\23\3\24\3\24\3\24"+
		"\3\24\3\25\3\25\3\25\7\25\u00ec\n\25\f\25\16\25\u00ef\13\25\3\26\5\26"+
		"\u00f2\n\26\3\26\3\26\3\26\3\26\3\26\5\26\u00f9\n\26\3\27\3\27\5\27\u00fd"+
		"\n\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\5\30\u0107\n\30\3\30\3\30"+
		"\3\31\3\31\5\31\u010d\n\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\33\3\33"+
		"\3\34\3\34\3\34\7\34\u011b\n\34\f\34\16\34\u011e\13\34\3\35\3\35\3\35"+
		"\3\35\3\36\3\36\3\37\3\37\3\37\5\37\u0129\n\37\5\37\u012b\n\37\3 \5 \u012e"+
		"\n \3 \3 \5 \u0132\n \3 \3 \3 \3 \3 \3 \5 \u013a\n \3!\3!\3!\3!\3!\3!"+
		"\3!\3!\3!\5!\u0145\n!\3\"\3\"\3#\3#\3$\3$\3$\3$\3$\5$\u0150\n$\3%\3%\3"+
		"&\3&\3\'\3\'\3\'\2(\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60"+
		"\62\64\668:<>@BDFHJL\2\t\4\2\6\6\17\17\3\2\63\66\4\2\n\n\16\16\4\2\7\7"+
		"##\3\2AG\3\2HI\3\2RS\u0162\2N\3\2\2\2\4Y\3\2\2\2\6[\3\2\2\2\bp\3\2\2\2"+
		"\n\u0084\3\2\2\2\f\u0095\3\2\2\2\16\u0097\3\2\2\2\20\u009b\3\2\2\2\22"+
		"\u009d\3\2\2\2\24\u00ab\3\2\2\2\26\u00ad\3\2\2\2\30\u00bb\3\2\2\2\32\u00c3"+
		"\3\2\2\2\34\u00c6\3\2\2\2\36\u00d0\3\2\2\2 \u00d3\3\2\2\2\"\u00dd\3\2"+
		"\2\2$\u00e1\3\2\2\2&\u00e4\3\2\2\2(\u00e8\3\2\2\2*\u00f1\3\2\2\2,\u00fa"+
		"\3\2\2\2.\u0103\3\2\2\2\60\u010a\3\2\2\2\62\u0111\3\2\2\2\64\u0115\3\2"+
		"\2\2\66\u0117\3\2\2\28\u011f\3\2\2\2:\u0123\3\2\2\2<\u012a\3\2\2\2>\u0139"+
		"\3\2\2\2@\u0144\3\2\2\2B\u0146\3\2\2\2D\u0148\3\2\2\2F\u014f\3\2\2\2H"+
		"\u0151\3\2\2\2J\u0153\3\2\2\2L\u0155\3\2\2\2NP\5\6\4\2OQ\7@\2\2PO\3\2"+
		"\2\2PQ\3\2\2\2QR\3\2\2\2RS\7\2\2\3S\3\3\2\2\2TZ\5\6\4\2UV\79\2\2VW\5\6"+
		"\4\2WX\7:\2\2XZ\3\2\2\2YT\3\2\2\2YU\3\2\2\2Z\5\3\2\2\2[]\7(\2\2\\^\t\2"+
		"\2\2]\\\3\2\2\2]^\3\2\2\2^_\3\2\2\2_`\5\n\6\2`b\5\26\f\2ac\5\32\16\2b"+
		"a\3\2\2\2bc\3\2\2\2ce\3\2\2\2df\5\34\17\2ed\3\2\2\2ef\3\2\2\2fh\3\2\2"+
		"\2gi\5\36\20\2hg\3\2\2\2hi\3\2\2\2ik\3\2\2\2jl\5 \21\2kj\3\2\2\2kl\3\2"+
		"\2\2ln\3\2\2\2mo\5$\23\2nm\3\2\2\2no\3\2\2\2o\7\3\2\2\2pq\79\2\2qv\5L"+
		"\'\2rs\78\2\2su\5L\'\2tr\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2wy\3\2\2"+
		"\2xv\3\2\2\2yz\7:\2\2z\t\3\2\2\2{\u0085\7K\2\2|\u0081\5\f\7\2}~\78\2\2"+
		"~\u0080\5\f\7\2\177}\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081"+
		"\u0082\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0084{\3\2\2\2"+
		"\u0084|\3\2\2\2\u0085\13\3\2\2\2\u0086\u008b\5<\37\2\u0087\u0089\7\t\2"+
		"\2\u0088\u0087\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008c"+
		"\5\20\t\2\u008b\u0088\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u0096\3\2\2\2"+
		"\u008d\u0096\5\16\b\2\u008e\u0093\5\22\n\2\u008f\u0091\7\t\2\2\u0090\u008f"+
		"\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0092\3\2\2\2\u0092\u0094\5\20\t\2"+
		"\u0093\u0090\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095\u0086"+
		"\3\2\2\2\u0095\u008d\3\2\2\2\u0095\u008e\3\2\2\2\u0096\r\3\2\2\2\u0097"+
		"\u0098\5J&\2\u0098\u0099\7\67\2\2\u0099\u009a\7K\2\2\u009a\17\3\2\2\2"+
		"\u009b\u009c\7R\2\2\u009c\21\3\2\2\2\u009d\u009e\5\24\13\2\u009e\u00a7"+
		"\79\2\2\u009f\u00a4\5<\37\2\u00a0\u00a1\78\2\2\u00a1\u00a3\5<\37\2\u00a2"+
		"\u00a0\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2"+
		"\2\2\u00a5\u00a8\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a7\u009f\3\2\2\2\u00a7"+
		"\u00a8\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\7:\2\2\u00aa\23\3\2\2\2"+
		"\u00ab\u00ac\t\3\2\2\u00ac\25\3\2\2\2\u00ad\u00ae\7\24\2\2\u00ae\u00b3"+
		"\5\30\r\2\u00af\u00b0\78\2\2\u00b0\u00b2\5\30\r\2\u00b1\u00af\3\2\2\2"+
		"\u00b2\u00b5\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\27"+
		"\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6\u00b7\79\2\2\u00b7\u00b8\5\4\3\2\u00b8"+
		"\u00b9\7:\2\2\u00b9\u00bc\3\2\2\2\u00ba\u00bc\5D#\2\u00bb\u00b6\3\2\2"+
		"\2\u00bb\u00ba\3\2\2\2\u00bc\u00c1\3\2\2\2\u00bd\u00bf\7\t\2\2\u00be\u00bd"+
		"\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c2\5\20\t\2"+
		"\u00c1\u00be\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\31\3\2\2\2\u00c3\u00c4"+
		"\7\62\2\2\u00c4\u00c5\5(\25\2\u00c5\33\3\2\2\2\u00c6\u00c7\7\26\2\2\u00c7"+
		"\u00c8\7\f\2\2\u00c8\u00cd\5F$\2\u00c9\u00ca\78\2\2\u00ca\u00cc\5F$\2"+
		"\u00cb\u00c9\3\2\2\2\u00cc\u00cf\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce"+
		"\3\2\2\2\u00ce\35\3\2\2\2\u00cf\u00cd\3\2\2\2\u00d0\u00d1\7\30\2\2\u00d1"+
		"\u00d2\5(\25\2\u00d2\37\3\2\2\2\u00d3\u00d4\7$\2\2\u00d4\u00d5\7\f\2\2"+
		"\u00d5\u00da\5\"\22\2\u00d6\u00d7\78\2\2\u00d7\u00d9\5\"\22\2\u00d8\u00d6"+
		"\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db"+
		"!\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00df\5F$\2\u00de\u00e0\t\4\2\2\u00df"+
		"\u00de\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0#\3\2\2\2\u00e1\u00e2\7\27\2\2"+
		"\u00e2\u00e3\7M\2\2\u00e3%\3\2\2\2\u00e4\u00e5\79\2\2\u00e5\u00e6\5(\25"+
		"\2\u00e6\u00e7\7:\2\2\u00e7\'\3\2\2\2\u00e8\u00ed\5*\26\2\u00e9\u00ea"+
		"\t\5\2\2\u00ea\u00ec\5*\26\2\u00eb\u00e9\3\2\2\2\u00ec\u00ef\3\2\2\2\u00ed"+
		"\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee)\3\2\2\2\u00ef\u00ed\3\2\2\2"+
		"\u00f0\u00f2\7 \2\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f8"+
		"\3\2\2\2\u00f3\u00f9\5&\24\2\u00f4\u00f9\5,\27\2\u00f5\u00f9\5.\30\2\u00f6"+
		"\u00f9\5\60\31\2\u00f7\u00f9\5\62\32\2\u00f8\u00f3\3\2\2\2\u00f8\u00f4"+
		"\3\2\2\2\u00f8\u00f5\3\2\2\2\u00f8\u00f6\3\2\2\2\u00f8\u00f7\3\2\2\2\u00f9"+
		"+\3\2\2\2\u00fa\u00fc\5:\36\2\u00fb\u00fd\7 \2\2\u00fc\u00fb\3\2\2\2\u00fc"+
		"\u00fd\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u00ff\7\13\2\2\u00ff\u0100\5"+
		":\36\2\u0100\u0101\7\7\2\2\u0101\u0102\5:\36\2\u0102-\3\2\2\2\u0103\u0104"+
		"\5:\36\2\u0104\u0106\7\33\2\2\u0105\u0107\7 \2\2\u0106\u0105\3\2\2\2\u0106"+
		"\u0107\3\2\2\2\u0107\u0108\3\2\2\2\u0108\u0109\7!\2\2\u0109/\3\2\2\2\u010a"+
		"\u010c\5:\36\2\u010b\u010d\7 \2\2\u010c\u010b\3\2\2\2\u010c\u010d\3\2"+
		"\2\2\u010d\u010e\3\2\2\2\u010e\u010f\7\36\2\2\u010f\u0110\5:\36\2\u0110"+
		"\61\3\2\2\2\u0111\u0112\5:\36\2\u0112\u0113\5\64\33\2\u0113\u0114\5:\36"+
		"\2\u0114\63\3\2\2\2\u0115\u0116\t\6\2\2\u0116\65\3\2\2\2\u0117\u011c\5"+
		":\36\2\u0118\u0119\78\2\2\u0119\u011b\5:\36\2\u011a\u0118\3\2\2\2\u011b"+
		"\u011e\3\2\2\2\u011c\u011a\3\2\2\2\u011c\u011d\3\2\2\2\u011d\67\3\2\2"+
		"\2\u011e\u011c\3\2\2\2\u011f\u0120\79\2\2\u0120\u0121\5:\36\2\u0121\u0122"+
		"\7:\2\2\u01229\3\2\2\2\u0123\u0124\5<\37\2\u0124;\3\2\2\2\u0125\u012b"+
		"\5> \2\u0126\u0129\5F$\2\u0127\u0129\58\35\2\u0128\u0126\3\2\2\2\u0128"+
		"\u0127\3\2\2\2\u0129\u012b\3\2\2\2\u012a\u0125\3\2\2\2\u012a\u0128\3\2"+
		"\2\2\u012b=\3\2\2\2\u012c\u012e\5B\"\2\u012d\u012c\3\2\2\2\u012d\u012e"+
		"\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u013a\7N\2\2\u0130\u0132\5B\"\2\u0131"+
		"\u0130\3\2\2\2\u0131\u0132\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u013a\7M"+
		"\2\2\u0134\u013a\7P\2\2\u0135\u013a\7,\2\2\u0136\u013a\7\23\2\2\u0137"+
		"\u013a\5@!\2\u0138\u013a\7O\2\2\u0139\u012d\3\2\2\2\u0139\u0131\3\2\2"+
		"\2\u0139\u0134\3\2\2\2\u0139\u0135\3\2\2\2\u0139\u0136\3\2\2\2\u0139\u0137"+
		"\3\2\2\2\u0139\u0138\3\2\2\2\u013a?\3\2\2\2\u013b\u013c\7\5\2\2\u013c"+
		"\u013d\7Q\2\2\u013d\u0145\7<\2\2\u013e\u013f\7\4\2\2\u013f\u0140\7Q\2"+
		"\2\u0140\u0145\7<\2\2\u0141\u0142\7\3\2\2\u0142\u0143\7Q\2\2\u0143\u0145"+
		"\7<\2\2\u0144\u013b\3\2\2\2\u0144\u013e\3\2\2\2\u0144\u0141\3\2\2\2\u0145"+
		"A\3\2\2\2\u0146\u0147\t\7\2\2\u0147C\3\2\2\2\u0148\u0149\5H%\2\u0149E"+
		"\3\2\2\2\u014a\u0150\5L\'\2\u014b\u014c\5J&\2\u014c\u014d\7\67\2\2\u014d"+
		"\u014e\5L\'\2\u014e\u0150\3\2\2\2\u014f\u014a\3\2\2\2\u014f\u014b\3\2"+
		"\2\2\u0150G\3\2\2\2\u0151\u0152\t\b\2\2\u0152I\3\2\2\2\u0153\u0154\7R"+
		"\2\2\u0154K\3\2\2\2\u0155\u0156\t\b\2\2\u0156M\3\2\2\2)PY]behknv\u0081"+
		"\u0084\u0088\u008b\u0090\u0093\u0095\u00a4\u00a7\u00b3\u00bb\u00be\u00c1"+
		"\u00cd\u00da\u00df\u00ed\u00f1\u00f8\u00fc\u0106\u010c\u011c\u0128\u012a"+
		"\u012d\u0131\u0139\u0144\u014f";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}