package eu.iescities.server.querymapper.datasource.csv;

import java.util.HashMap;
import java.util.Map;

public class SafeHeaderManager {

	private final Map<String, String> safeHeaders = new HashMap<String, String>();
	
	public SafeHeaderManager() {
		
	}
	
	public String getHeader(String header) {
		if (!safeHeaders.containsKey(header)) {
			safeHeaders.put(header, safeHeader(header));
		}
		return safeHeaders.get(header);
	}
	
	private String safeHeader(String header) {
		header = header.replaceAll("\"", "");
		
		return header;
	}
}
