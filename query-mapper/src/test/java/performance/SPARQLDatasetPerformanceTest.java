package performance;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStreamReader;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Rule;
import org.junit.Test;

import com.google.gson.Gson;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSourceTest.SPARQLTestData;
import eu.iescities.server.querymapper.util.FileLoader;

public class SPARQLDatasetPerformanceTest {

	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();
	
	@Test
	@PerfTest(invocations = 2500, threads = 10)	
	public void testPerformanceSPARQL() throws Exception {
		final SPARQLTestData testData = new SPARQLTestData("/queries/sparql/sparql_query1.txt",
				"/sparql/foaf_schema.json",
				"/queries/results/query_result1.json");
		
		final String sparqlQuery = FileLoader.loadResource(testData.getSPARQLFile());
		final String jsonSchema = FileLoader.loadResource(testData.getSchemaFile());
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		SPARQLDataSource query = new SPARQLDataSource(schema, true);
		
		ResultSet results = new ResultSet(query.executeSPARQL(sparqlQuery));

		Gson gson = new Gson();

		InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream(testData.getResultsFile()));
		ResultSet expected = gson.fromJson(reader, ResultSet.class);
		reader.close();

		assertEquals(expected, results);
	}
	
	@Test
	@PerfTest(invocations = 2500, threads = 10)
	public void testGetTables() throws IOException, DataSourceManagementException {
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");
		
		final SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		final SPARQLDataSource queriable = new SPARQLDataSource(schema, true);
		final ResultSet rs = new ResultSet(queriable.getTables());
		
		assertEquals(11, rs.getRows().size());
	}
}
