/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.database.update.Update;
import eu.iescities.server.querymapper.datasource.database.update.UpdateBlock;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.JSONTranslator;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;

public class JSONTranslatorTest {

	@Before
	public void setUp() {
		Config.getInstance().deleteDBDir();
	}
	
	@Test
	public void testTranslateJSONInsertNoKey() throws IOException, TranslationException, DataSourceSecurityManagerException {
		final String json = FileLoader.loadResource("/json/update/update1.json");
		
		final JSONTranslator translator = new JSONTranslator();
		final UpdateBlock updateBlock = translator.translateJSON(json);
		
		assertEquals(1, updateBlock.getInserts().size());
		assertTrue(updateBlock.getDeletes().isEmpty());
		
		final List<Update> expected = new ArrayList<Update>();
		expected.add(new Update("INSERT INTO people (id, born, occupation, name) VALUES (null, 1, 'physicist', 'Einstein')", "", ""));
		expected.add(new Update("INSERT INTO people (id, born, occupation, name) VALUES (null, 1, 'physicist', 'Schrodinger')", "", ""));
		
		assertEquals(expected, updateBlock.getInserts().get(0).getUpdates());
	}
	
	@Test
	public void testTranslateJSONDelete() throws IOException, TranslationException, DataSourceSecurityManagerException {
		final String json = FileLoader.loadResource("/json/delete/delete2.json");
		final JSONTranslator translator = new JSONTranslator();
				
		final UpdateBlock updateBlock = translator.translateJSON(json);
		
		assertEquals(1, updateBlock.getDeletes().size());
		assertTrue(updateBlock.getInserts().isEmpty());
		
		final List<Update> expected = new ArrayList<Update>();
		expected.add(new Update("DELETE FROM people WHERE id = 500 AND born = 1 AND occupation = 'physicist' AND name = 'Einstein'", "", ""));
		expected.add(new Update("DELETE FROM people WHERE id = 501 AND born = 1 AND occupation = 'physicist' AND name = 'Schrodinger'", "", ""));
		
		assertEquals(expected, updateBlock.getDeletes().get(0).getUpdates());
	}
	
	@Test
	public void testTranslateJSONInsertWithKey() throws IOException, TranslationException, DataSourceSecurityManagerException {
		final String json = FileLoader.loadResource("/json/update/update2.json");
				
		final JSONTranslator translator = new JSONTranslator();
		
		final UpdateBlock updateBlock = translator.translateJSON(json);
		
		assertEquals(1, updateBlock.getInserts().size());
		assertTrue(updateBlock.getDeletes().isEmpty());
		
		final List<Update> expected = new ArrayList<Update>();
		expected.add(new Update("INSERT INTO people (id, born, occupation, name) VALUES (500, 1, 'physicist', 'Einstein')", "", ""));
		expected.add(new Update("INSERT INTO people (id, born, occupation, name) VALUES (501, 1, 'physicist', 'Schrodinger')", "", ""));
		
		assertEquals(expected, updateBlock.getInserts().get(0).getUpdates());
	}
	
	@Test
	public void testTranslateJSONDeleteOnlyID() throws IOException, TranslationException, DataSourceSecurityManagerException {
		final String json = FileLoader.loadResource("/json/delete/delete1.json");
		final JSONTranslator translator = new JSONTranslator();
		
		final UpdateBlock updateBlock = translator.translateJSON(json);
		
		assertEquals(1, updateBlock.getDeletes().size());
		assertTrue(updateBlock.getInserts().isEmpty());
		
		final List<Update> expected = new ArrayList<Update>();
		expected.add(new Update("DELETE FROM people WHERE id = 500", "", ""));
		expected.add(new Update("DELETE FROM people WHERE id = 501", "", ""));
		
		assertEquals(expected, updateBlock.getDeletes().get(0).getUpdates());
	}
}
