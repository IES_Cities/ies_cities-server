/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.query;

import java.io.IOException;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.DBDataSource;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;

public class DBConnectorQuerierTest {

	@Test(expected=DataSourceManagementException.class)
	public void testInvalidQuery() throws DataSourceSecurityManagerException, TranslationException, DataSourceManagementException, IOException, DataSourceConnectorException {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		final DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		final SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select * from people; select * from people;";

		queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
	}
	
	@Test(expected=DataSourceManagementException.class)
	public void testInvalidSelectedTables() throws IOException, DataSourceSecurityManagerException, TranslationException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		final DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		final SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select * from table;";

		queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
	}
}
