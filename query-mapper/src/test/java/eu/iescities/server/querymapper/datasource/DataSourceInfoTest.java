package eu.iescities.server.querymapper.datasource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;

public class DataSourceInfoTest {
	
	private DataSourceInfo dataSourceInfo;
	
	@Before
	public void setUp() {
		dataSourceInfo = new DataSourceInfo();
		final List<TableInfo> tables = new ArrayList<TableInfo>();
		
		final TableInfo tableInfo = new TableInfo("testTable");
		tableInfo.addColumnInfo(new ColumnInfo("cNameA", "typeA", true));
		tableInfo.addColumnInfo(new ColumnInfo("cNameB", "typeB", false));
	
		tables.add(tableInfo);
		dataSourceInfo.setTables(tables);
		
		dataSourceInfo.setType(ConnectorType.json);
	}

	@Test
	public void testGetTable() {
		final TableInfo tableInfo = dataSourceInfo.getTable("testTable");
		assertEquals(2, tableInfo.getColumns().size());
	}
	
	@Test
	public void testGetTableNotFound() {
		assertNull(dataSourceInfo.getTable("notFound"));
	}
	
	@Test
	public void testGetPrimaryKey() {
		final TableInfo tableInfo = dataSourceInfo.getTable("testTable");
		final ColumnInfo primaryColumn = tableInfo.getPrimaryKey();
		assertEquals("cNameA", primaryColumn.getName());
	}
	
	@Test
	public void testToString() {
		final String expected = "Type: json\n" +
			"Table: testTable\n" + 
			"Column: cNameA\n" + 
			"Type: typeA\n" + 
			"Key: true\n" + 
			"Column: cNameB\n" +
			"Type: typeB\n" + 
			"Key: false\n";
		
		assertEquals(expected, dataSourceInfo.toString());
	}
}
