/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.util.FileLoader;

public class SchemaTest {

	@Test
	public void testLoadSchema() throws IOException {			
		final String jsonSchema = FileLoader.loadResource("/sparql/simple_schema.json");
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		
		assertEquals("http://domain/sparql", schema.getQueryEndpoint());
		assertEquals(3, schema.getSchemaTables().size());

		SPARQLConnector.Table table = schema.getSchemaTables().get(1);
		assertEquals("table2", table.getName());
		assertEquals("http://domain/table1#Class2", table.getRDFClass());
		assertEquals(1, table.getPrimaryKey().size());
		assertEquals("id", table.getPrimaryKey().get(0).getName());
		assertEquals("table1", table.getPrimaryKey().get(0).getReferences());

		assertEquals(3, table.getColumns().size());

		SPARQLConnector.Table.Column column2 = table.getColumns().get(1);
		assertEquals("colName2", column2.getName());
		assertEquals("http://domain/table2#Property2", column2.getRDFProperty());
		assertEquals("STRING", column2.getType());

		SPARQLConnector.Table.Column column3 = table.getColumns().get(2);
		assertEquals("colName3", column3.getName());
		assertEquals("http://domain/table2#Property3", column3.getRDFProperty());
		assertEquals("STRING", column2.getType());
	}
}
