/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.evaluation.query;

public class RowStats {
	
	private final float rows;
	private final float transformTime;
	private final float executionTime;

	public RowStats(float rows, float transformTime, float executionTime) {
		this.rows = rows;
		this.transformTime = transformTime;
		this.executionTime = executionTime;
	}
	
	public float getRows() {
		return rows;
	}

	public float getTransformTime() {
		return transformTime;
	}

	public float getExecutionTime() {
		return executionTime;
	}
	
	public float getTotalTime() {
		return transformTime + executionTime;
	}
	
	public float getRatio() {
		return getTransformTime() / getTotalTime();
	}
}