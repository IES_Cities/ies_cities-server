/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.serialization;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDContext;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDResult;

public class JSONLDResultTest {

	private JSONLDResult jsonLDResult = new JSONLDResult();
	
	@Before
	public void setUp() throws Exception {
		JSONLDContext context = new JSONLDContext();
		
		context.addEntry("id", "http://example.com/id");
		
		context.addEntry("key1", "http://example.com/value1");
		context.addEntry("key2", "http://example.com/value2");
		
		final ResultSet rs = new ResultSet();
		
		for (int i = 0; i < 2; i++) {
			final Row row = new Row();
			row.addColumn("key1", "value1");
			row.addColumn("key2", "value2");
			rs.addRow(row);
		}
		
		jsonLDResult = new JSONLDResult(rs, context);
	}

	@Test
	public void testGetCount() {
		assertEquals(2, jsonLDResult.getCount());
	}

	@Test
	public void testToJSON() {
		String expected = "{\n" + 
				"  \"count\": 2,\n" + 
				"  \"rows\": [\n" + 
				"    {\n" + 
				"      \"@context\": {\n" + 
				"        \"key1\": \"http://example.com/value1\",\n" + 
				"        \"key2\": \"http://example.com/value2\"\n" + 
				"      },\n" + 
				"      \"key1\": \"value1\",\n" + 
				"      \"key2\": \"value2\"\n" + 
				"    },\n" + 
				"    {\n" + 
				"      \"@context\": {\n" + 
				"        \"key1\": \"http://example.com/value1\",\n" + 
				"        \"key2\": \"http://example.com/value2\"\n" + 
				"      },\n" + 
				"      \"key1\": \"value1\",\n" + 
				"      \"key2\": \"value2\"\n" + 
				"    }\n" + 
				"  ]\n" + 
				"}";
		
		assertEquals(expected, jsonLDResult.toJSON());
	}
}
