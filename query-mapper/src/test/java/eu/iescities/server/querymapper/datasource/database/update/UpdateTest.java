/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.database.update.Update;

public class UpdateTest {

	@Test
	public void testEquals() {
		final Update update = new Update("insert into table values (1, 2, '3')" , "key", "1");
		
		assertEquals(update, update);
		
		assertFalse(update.equals(new Object()));
	}
	
	@Test
	public void testToString() {
		final Update update = new Update("insert into table values (1, 2, '3')" , "key", "1");
		
		assertEquals("SQL: insert into table values (1, 2, '3') Key: key Value: 1", update.toString());
	}
}
