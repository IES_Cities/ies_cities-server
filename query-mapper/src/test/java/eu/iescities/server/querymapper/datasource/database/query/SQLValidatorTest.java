/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.database.query.SQLValidator;
import eu.iescities.server.querymapper.datasource.database.query.SQLValidator.Type;

public class SQLValidatorTest {
	
	@Test
	public void testIsvalid() {
		final SQLValidator sqlValidator = new SQLValidator();
		
		assertTrue(sqlValidator.isValid("SELECT * FROM table;"));
		assertTrue(sqlValidator.isValid("SELECT id, name, surname FROM table;"));
		
		assertTrue(sqlValidator.isValid("INSERT INTO table VALUES ('value1', value2, value3);"));
		assertTrue(sqlValidator.isValid("INSERT INTO table_1 VALUES ('value1', value2, value3);"));
		assertTrue(sqlValidator.isValid("insert into table VALUES ('value1', value2, value3);"));
		assertTrue(sqlValidator.isValid("INSERT into table (1, 2,3) VALUES ('value1', value2, value3);"));
		assertTrue(sqlValidator.isValid("INSERT into table (column_name, name, name) VALUES ('value1', value2, value);"));
		assertTrue(sqlValidator.isValid("INSERT into table (1, 2,3) VALUES ('value1', value2, '1-1-2015');"));
		
		assertTrue(sqlValidator.isValid("INSERT INTO COMENTARIOS (ID_EVENTO, USUARIO, COMENTARIO, FECHA) VALUES (1, \"nombreUser\", \"txtComment\", \"2015-01-01\");"));
		
		assertTrue(sqlValidator.isValid("INSERT INTO PREDICCION_USUARIO ((ID_USER, ID_EST_TIE, GRADOS, COORDENADAS, DATE, TIME) VALUES (1, 1, \"15°\", \"42.9144481,-2.6627892\",\"2015-4-20\",\"17:19:26\"));"));
		
		assertFalse(sqlValidator.isValid("SELECT id, name, surname FROM table; SELECT * FROM table;"));
		assertFalse(sqlValidator.isValid("SELECT id, name, surname FROM table;INSERT into table (1, 2,3) VALUES ('value1', value2, value3);"));
		assertFalse(sqlValidator.isValid("INSERT into table (1, 2,3) VALUES ('value1', value2, value3);SELECT id, name, surname FROM table;"));
		assertFalse(sqlValidator.isValid("INSERT into table (1, 2,3) VALUES ('value1', value2, value3);INSERT into table (1, 2,3) VALUES ('value1', value2, value3);"));
		assertFalse(sqlValidator.isValid("INSERT INTO table VALUES ('value1', value2, value3);delete from people where name = 'updated_name';"));	
		assertFalse(sqlValidator.isValid("delete from people where name = 'updated_name';INSERT INTO table VALUES ('value1', value2, value3);"));
		assertFalse(sqlValidator.isValid("update people set name = 'value' id = 2 where name = 'updated_name' AND id = 3;delete from people where name = 'updated_name';"));
	}
	
	public void getStmtType() {
		final SQLValidator sqlValidator = new SQLValidator();
		
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT INTO table VALUES ('value1', value2, value3);"));
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT INTO table_1 VALUES ('value1', value2, value3);"));
		assertEquals(Type.INSERT, sqlValidator.getStmtType("insert into table VALUES ('value1', value2, value3);"));
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT into table (1, 2,3) VALUES ('value1', value2, value3);"));
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT into table (column_name, name, name) VALUES ('value1', value2, value);"));
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT into table (1, 2,3) VALUES ('value1', value2, '1-1-2015');"));
		
		assertEquals(Type.INSERT, sqlValidator.getStmtType("INSERT INTO COMENTARIOS (ID_EVENTO, USUARIO, COMENTARIO, FECHA) VALUES (1, \"nombreUser\", \"txtComment\", \"2015-01-01\");"));
	}

	@Test
	public void testIsDeleteStmt() {
		final SQLValidator sqlValidator = new SQLValidator();
		
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where name = 'updated_name';"));
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where name = 'updated_name' AND id=1;"));
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where (name = 'updated_name' AND id=1);"));
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where (name = 'updated_name' AND id=1 OR A = '1';"));
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where (name = 'updated_name' AND (id=1 OR A = '1'));"));
		
		assertEquals(Type.DELETE, sqlValidator.getStmtType("delete from people where (ID='id' and NUM_ASISTENTES > 1);"));
	}

	@Test
	public void testIsUpdateStmt() {
		final SQLValidator sqlValidator = new SQLValidator();
		
		assertEquals(Type.UPDATE, sqlValidator.getStmtType("update people set name = 'value', id = 2 where name = 'updated_name' AND id = 3;"));
		assertEquals(Type.UPDATE, sqlValidator.getStmtType("update EVENTOS set NUM_ASISTENTES=NUM_ASISTENTES-1 where (ID='id' and NUM_ASISTENTES > 1);"));
	}
}
