package eu.iescities.server.querymapper.datasource.database.update;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.database.query.SQLValidator;
import eu.iescities.server.querymapper.datasource.database.update.TransactionParser.SQLStatement;

public class TransactionParserTest {

	@Test
	public void testParseSingleInsert() {
		final String text = "INSERT INTO table VALUES ('value1', value2, value3);";
		
		final TransactionParser parser = new TransactionParser();
		final List<SQLStatement> stmts = parser.parse(text);
		
		assertEquals(1, stmts.size());
		
		assertEquals(SQLValidator.Type.INSERT, stmts.get(0).getType());
	}
	
	@Test
	public void testParseMultipleInserts() {
		final String text = 	"INSERT INTO table VALUES ('value1', value2, value3);\n" +
								"INSERT INTO table_1 VALUES ('value1', value2, value3);\n" +
								"INSERT into table (1, 2,3) VALUES ('value1', value2, value3);";
		
		final TransactionParser parser = new TransactionParser();
		final List<SQLStatement> stmts = parser.parse(text);
		
		assertEquals(3, stmts.size());
		
		assertEquals(SQLValidator.Type.INSERT, stmts.get(0).getType());
		assertEquals(SQLValidator.Type.INSERT, stmts.get(1).getType());
		assertEquals(SQLValidator.Type.INSERT, stmts.get(2).getType());
	}
	
	@Test
	public void testParseMixedStatements() {
		final String text = 	"INSERT INTO table VALUES ('value1', value2, value3);\n" +
								"INSERT INTO table_1 VALUES ('value1', value2, value3);\n" +
								"INSERT into table (1, 2,3) VALUES ('value1', value2, value3);\n" + 
								"update people set name = 'value', id = 2 where name = 'updated_name' AND id = 3;\n" +
								"delete from people where (ID='id' and NUM_ASISTENTES > 1);";
		
		final TransactionParser parser = new TransactionParser();
		final List<SQLStatement> stmts = parser.parse(text);
		
		assertEquals(5, stmts.size());
		
		assertEquals(SQLValidator.Type.INSERT, stmts.get(0).getType());
		assertEquals(SQLValidator.Type.INSERT, stmts.get(1).getType());
		assertEquals(SQLValidator.Type.INSERT, stmts.get(2).getType());
		assertEquals(SQLValidator.Type.UPDATE, stmts.get(3).getType());
		assertEquals(SQLValidator.Type.DELETE, stmts.get(4).getType());
	}
}
