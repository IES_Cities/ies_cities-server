/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.sql.schema.builder.RowStats;

public class RowStatsTest {

private RowStats rowStats;
	
	@Before
	public void setUp() {
		rowStats = new RowStats(100, 10);
	}

	@Test
	public void testGetPropertyExtractionTime() {
		assertEquals(100, rowStats.getPropertyExtractionTime(), 0.0);
	}

	@Test
	public void testGetPropertyNum() {
		assertEquals(10, rowStats.getPropertyNum(), 0.0);
	}
}
