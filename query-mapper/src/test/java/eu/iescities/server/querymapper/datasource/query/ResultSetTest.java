/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.query;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.query.ResultSet;

public class ResultSetTest {

	private ResultSet results;
	
	@Before
	public void setUp() {
		results = new ResultSet();
		ResultSet.Row row = results.createRow();
		ResultSet.Row.Column column1 = new ResultSet.Row.Column("colunmName1", "columnValue2");
		row.addColumn(column1);
		
		ResultSet.Row.Column column2 = new ResultSet.Row.Column("colunmName2", "columnValue2");
		row.addColumn(column2);
	}
	
	@Test
	public void testGetRows() {
		assertEquals(1, results.getRows().size());
	}
	
	@Test
	public void testToString() {
		String expected = 	"+--------------+--------------+\n" + 
							"| colunmName1  | colunmName2  |\n" +
							"+--------------+--------------+\n" +
							"| columnValue2 | columnValue2 |\n" +
							"+--------------+--------------+\n";

		assertEquals(expected, results.toString());
	}
}
