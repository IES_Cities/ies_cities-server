/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;

public class SchemaBuilderTest {

	private final String sparqlEndpoint = "http://localhost:3030/ds/query";
	private SchemaBuilder schemaBuilder;
	
	@Before
	public void setUp() throws IOException {
		schemaBuilder = new SchemaBuilder(sparqlEndpoint, "", false);
	}

	@Test
	public void testGetSchema() throws Exception {
		final SPARQLConnector schema = schemaBuilder.getSchema();

		assertEquals(sparqlEndpoint, schema.getQueryEndpoint());

		assertEquals(11, schema.getSchemaTables().size());

		SPARQLConnector.Table table = schema.getTable("foaf_Person");
		assertEquals("foaf_Person", table.getName());
		assertEquals("http://xmlns.com/foaf/0.1/Person", table.getRDFClass());

		assertEquals(14, table.getColumns().size());

		SPARQLConnector.Table.Column columnName = table.getColumn("foaf_name");
		assertEquals("foaf_name", columnName.getName());
		assertEquals("http://xmlns.com/foaf/0.1/name", columnName.getRDFProperty());
		assertEquals("STRING", columnName.getType());
	}
	
	@Test
	public void testGetGraphs() throws SchemaBuilderException {
		assertTrue(schemaBuilder.getGraphs().isEmpty());
	}
	
	@Test
	public void testGetShortPrefix() {
		assertEquals("http", schemaBuilder.getShortPrefix(("http://test.com/test/prefix")));
		assertEquals("hsso", schemaBuilder.getShortPrefix(("http://somedomain.com/subdomain/other")));
	}
	
	@Test
	public void testGetShortPrefixFromList() {
		assertEquals("foaf", schemaBuilder.getShortPrefixFromList("http://xmlns.com/foaf/0.1/"));
		assertEquals("hsso", schemaBuilder.getShortPrefixFromList(("http://somedomain.com/subdomain/other")));
	}
}