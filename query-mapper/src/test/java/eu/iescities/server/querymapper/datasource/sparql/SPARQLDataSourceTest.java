/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.sparql;

import static org.junit.Assert.assertEquals;


import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.google.gson.Gson;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.update.JSONLDUpdateable;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDResult;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;

@RunWith(Theories.class)
public class SPARQLDataSourceTest {

	static public class SQLTestData {

		private final String sqlFile;
		private final String schemaFile;
		private final String resultsFile;

		public SQLTestData(String sqlFile, String schemaFile, String resultsFile) {
			this.sqlFile = sqlFile;
			this.schemaFile = schemaFile;
			this.resultsFile = resultsFile;
		}

		public String getSQLFile() { return sqlFile; }
		public String getSchemaFile() { return schemaFile; }
		public String getResultsFile() { return resultsFile; }
	}

	static public class SPARQLTestData {

		private final String sparqlFile;
		private final String schemaFile;
		private final String resultsFile;

		public SPARQLTestData(String sparqlFile, String schemaFile, String resultsFile) {
			this.sparqlFile = sparqlFile;
			this.schemaFile = schemaFile;
			this.resultsFile = resultsFile;
		}

		public String getSPARQLFile() { return sparqlFile; }
		public String getSchemaFile() { return schemaFile; }
		public String getResultsFile() { return resultsFile; }
	}
	
	static public class SQLJSONLDData {
		
		private final String sqlFile;
		private final String resultsFile;
		
		public SQLJSONLDData(String sqlFile, String resultsFile) {
			this.sqlFile = sqlFile;
			this.resultsFile = resultsFile;
		}
		
		public String getSQLFile() { return sqlFile; }
		public String getResultsFile() { return resultsFile; }
	}

	@Theory
	public void testExecuteSelectSQL(SQLTestData data) throws Exception {
		System.out.println(String.format("SQLData: {%s, %s, %s}",
			data.getSQLFile(), data.getSchemaFile(), data.getResultsFile()));
		
		final String sqlQuery = FileLoader.loadResource(data.getSQLFile());		
		final String jsonSchema = FileLoader.loadResource(data.getSchemaFile());
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		SPARQLDataSource query = new SPARQLDataSource(schema, true);

		ResultSet results = new ResultSet(query.executeSQLSelect(sqlQuery, "", DataOrigin.ANY));

		Gson gson = new Gson();

		InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream(data.getResultsFile()));
		ResultSet expected = gson.fromJson(reader, ResultSet.class);
		reader.close();

		assertEquals(expected, results);
	}
	
	@Theory
	public void testExecuteSQLJSONLD(SQLJSONLDData data) throws Exception {
		System.out.println(String.format("SQLJSONLDData: {%s, %s}",
			data.getSQLFile(), data.getResultsFile()));
		
		final String sqlQuery = FileLoader.loadResource(data.getSQLFile());
		final String expected = FileLoader.loadResource(data.getResultsFile());
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");	
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		SPARQLDataSource query = new SPARQLDataSource(schema, true);

		eu.iescities.server.querymapper.datasource.query.serialization.ResultSet results = query.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
		JSONLDResult jsonldResults = new JSONLDResult(results, query.getJSONLDContext());
		
		assertEquals(expected, jsonldResults.toJSON());
	}

	@Theory
	public void testExecuteSelectSPARQL(SPARQLTestData data) throws Exception {
		System.out.println(String.format("SPARQLData: {%s, %s, %s}",
			data.getSPARQLFile(), data.getSchemaFile(), data.getResultsFile()));
		
		final String sparqlQuery = FileLoader.loadResource(data.getSPARQLFile());
		final String jsonSchema = FileLoader.loadResource(data.getSchemaFile());
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		SPARQLDataSource query = new SPARQLDataSource(schema, true);
		
		ResultSet results = new ResultSet(query.executeSPARQL(sparqlQuery));

		Gson gson = new Gson();

		InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream(data.getResultsFile()));
		ResultSet expected = gson.fromJson(reader, ResultSet.class);
		reader.close();

		assertEquals(expected, results);
	}
	
	@Test
	public void testGetTables() throws IOException, DataSourceManagementException {
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");
		
		final SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		final SPARQLDataSource queriable = new SPARQLDataSource(schema, true);
		final ResultSet rs = new ResultSet(queriable.getTables());
		
		assertEquals(11, rs.getRows().size());
	}

	public static int MAX_QUERIES = 21;
	public static @DataPoints SQLTestData[] sqlTestData = new SQLTestData[MAX_QUERIES];
	public static @DataPoints SPARQLTestData[] sparqlTestData = new SPARQLTestData[MAX_QUERIES];
	public static @DataPoints SQLJSONLDData[] jsonLDTestData = new SQLJSONLDData[MAX_QUERIES];
	
	static {

		for (int i = 0; i < MAX_QUERIES; i++)
			sqlTestData[i] = new SQLTestData("/queries/sql/sql_query" + (i + 1) + ".txt",
							"/sparql/foaf_schema.json",
							"/queries/results/query_result" + (i + 1) + ".json");

		for (int i = 0; i < MAX_QUERIES; i++)
			sparqlTestData[i] = new SPARQLTestData("/queries/sparql/sparql_query" + (i + 1) + ".txt",
							"/sparql/foaf_schema.json",
							"/queries/results/query_result" + (i + 1) + ".json");
		
		for (int i = 0; i < MAX_QUERIES; i++)
			jsonLDTestData[i] = new SQLJSONLDData("/queries/sql/sql_query" + (i + 1) + ".txt",
							"/jsonld/query_result" + (i + 1) + ".json");
	}
	
	static public class JSONLDData {

		private final String jsonLDData;
		private final String sparqlFile;
		private final String schemaFile;

		public JSONLDData(String sqlFile, String sparqlFile, String schemaFile) {
			this.jsonLDData = sqlFile;
			this.sparqlFile = sparqlFile;
			this.schemaFile = schemaFile;
		}

		public String getJSONLDFile() { return jsonLDData; }
		public String getSPARQLFile() { return sparqlFile; }
		public String getSchemaFile() { return schemaFile; }
	}

	@Theory
	public void testTranslateJSONLD(JSONLDData data) throws IOException, TranslationException {
		String jsonld = FileLoader.loadResource(data.getJSONLDFile());
		
		String jsonSchema = FileLoader.loadResource(data.getSchemaFile());
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		
		JSONLDUpdateable updateJsonld = new SPARQLDataSource(schema, false);
		
		String translated = updateJsonld.translateJSONLD(jsonld);
		
		String insertSPARQL = FileLoader.loadResource(data.getSPARQLFile());
		
		assertEquals(insertSPARQL, translated);
	}
	
	@Theory
	@Ignore
	public void testInsertJSONLD(JSONLDData data) throws IOException, TranslationException {
		String jsonld = FileLoader.loadResource(data.getJSONLDFile());
		
		String jsonSchema = FileLoader.loadResource(data.getSchemaFile());
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		
		JSONLDUpdateable updateJsonld = new SPARQLDataSource(schema, false);
		 
		updateJsonld.executeJSONLDUpdate(jsonld);
		
		UpdateRequest request = new UpdateRequest();
		request.add("DELETE WHERE { <http://www.morelab.deusto.es/resource/unai-aguilera#newentry> ?p ?o }");
		
		UpdateProcessor p = UpdateExecutionFactory.createRemote(request, schema.getUpdateEndpoint());
		p.execute();
	}
	
	public static int MAX_DATA = 1;
	public static @DataPoints JSONLDData[] testData = new JSONLDData[MAX_DATA];
	
	static {

		for (int i = 0; i < MAX_DATA; i++)
			testData[i] = new JSONLDData("/jsonld/update/update" + (i + 1) + ".json",
								"/queries/sparql_update/sparql_update" + (i + 1) + ".txt",
								"/sparql/foaf_schema.json");
	}
}