/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.evaluation.schema;

import java.io.IOException;
import java.util.Locale;

import eu.iescities.server.querymapper.sql.schema.builder.BuilderStats;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilderException;

public class SchemaExperiment {
	
	final String sparqlEndpoint;
	final String graph;
	final boolean useOntology;
	
	public SchemaExperiment(String sparqlEndpoint, String graph, boolean useOntology) {
		this.sparqlEndpoint = sparqlEndpoint;
		this.graph = graph;
		this.useOntology = useOntology;
	} 
	
	public void run() throws SchemaBuilderException, IOException {
		SchemaBuilder schemaBuilder = new SchemaBuilder(sparqlEndpoint, graph, useOntology);
		schemaBuilder.getSchema();
		
		BuilderStats stats = schemaBuilder.getStats();
		
		System.out.println("Classes,PropertyNum,PropertyExtractionTime,Total");
		System.out.println(String.format((Locale)null, "%.0f & %.2f & %.2f & %.2f",
			stats.getClassNum(),
			stats.getPropertyNum(),
			stats.getPropertyExtractionTime(),
			stats.getTotalExtractionTime() / (float) 1000.0f));
	}
}