/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.security.Permissions;

public class DatasourceConnectorTest {

	private DataSourceConnector connector;
	
	@Before
	public void setUp() {
		connector = new DataSourceConnector(ConnectorType.not_available);
	}
	
	@Test
	public void testGetType() {
		assertEquals(ConnectorType.not_available, connector.getType());
	}

	@Test
	public void testGetPermissions() {
		final Permissions p = connector.getPermissions();
		assertTrue(p.isEmpty());
	}

	@Test
	public void testGetRefresh() {
		assertEquals(Integer.MAX_VALUE, connector.getRefresh());
	}

	@Test
	public void testGetInfo() throws DataSourceManagementException {
		final DataSourceInfo info = connector.getInfo();
		assertTrue(info.getTables().isEmpty());
		assertEquals(ConnectorType.not_available, info.getType());
	}
	
	@Test
	public void testEquals() {
		assertEquals(connector, connector);
		
		assertFalse(connector.equals(new Object()));
		
		final DataSourceConnector c = new DataSourceConnector(ConnectorType.json);
		assertFalse(c.equals(connector));
		
	}
	
	@Test
	public void testGetMapping() {
		assertEquals(ConnectorType.not_available, connector.getMapping());
	}
	
	@Test
	public void testSetMapping() {
		connector.setMapping(ConnectorType.json_schema);
		assertEquals(ConnectorType.json_schema, connector.getMapping());
	}
	
	@Test
	public void testSetPermissions() {
		final Permissions p = new Permissions();
		connector.setPermissions(p);
		
		assertEquals(p, connector.getPermissions());
	}
}
