/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;

@RunWith(Theories.class)
public class JSONDatasetTest {
	
	static public class SQLTestData {

		private final String sqlFile;
		private final String schemaFile;
		private final String resultsFile;

		public SQLTestData(String sqlFile, String schemaFile, String resultsFile) {
			this.sqlFile = sqlFile;
			this.schemaFile = schemaFile;
			this.resultsFile = resultsFile;
		}

		public String getSQLFile() { return sqlFile; }
		public String getSchemaFile() { return schemaFile; }
		public String getResultsFile() { return resultsFile; }
	}
	
	private final String USER = "user1";
	private final String DATASET_ID = "0";
	
	@Before
	public void setUp() throws Exception {
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
	}

	@Theory
	public void testExecuteSelectSQL(SQLTestData data) throws Exception {
		System.out.println(String.format("JSONSQLData: {%s, %s, %s}",
			data.getSQLFile(), data.getSchemaFile(), data.getResultsFile()));
		
		final String sqlQuery = FileLoader.loadResource(data.getSQLFile());
		final String json = FileLoader.loadResource(data.getSchemaFile());
		
		JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		JSONDataSource query = new JSONDataSource(jsonMapping, DATASET_ID, true);

		ResultSet results = new ResultSet(query.executeSQLSelect(sqlQuery, "", DataOrigin.ANY));

		Gson gson = new Gson();

		InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream(data.getResultsFile()));
		ResultSet expected = gson.fromJson(reader, ResultSet.class);
		reader.close();

		assertEquals(expected, results);
	}
	
	@Test
	public void testGetTables() throws IOException, DataSourceManagementException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");
		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		final JSONDataSource queriable = new JSONDataSource(jsonMapping, DATASET_ID, true);
		final ResultSet rs = new ResultSet(queriable.getTables());
		
		assertEquals(1, rs.getRows().size());
	}
	
	@Test
	public void testInsertUserData() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);

		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String jsonInsert = FileLoader.loadResource("/json/update/update4.json");
		jsonDataset.executeJSONUpdate(jsonInsert, USER);
		
		final ResultSet rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(4, rs.getRows().size());
	}
	
	@Test
	public void testExecuteSQLSelectOrigin() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);

		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String jsonInsert = FileLoader.loadResource("/json/update/update4.json");
		jsonDataset.executeJSONUpdate(jsonInsert, USER);
		
		final ResultSet allRows = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		assertEquals(4, allRows.getRows().size());
		
		final ResultSet originalRows = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ORIGIN));
		assertEquals(2, originalRows.getRows().size());
		
		assertEquals("value1", originalRows.getRows().get(0).getColumn("key1").getValue());
		assertEquals("value4", originalRows.getRows().get(1).getColumn("key1").getValue());
		
		final ResultSet userRows = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.USER));
		assertEquals(2, userRows.getRows().size());
		
		assertEquals("somevalue1", userRows.getRows().get(0).getColumn("key1").getValue());
		assertEquals("somevalue4", userRows.getRows().get(1).getColumn("key1").getValue());
	}
	
	@Test
	public void testInsertUserDataIntKeys() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping6.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);

		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String jsonInsert = FileLoader.loadResource("/json/update/update6.json");
		jsonDataset.executeJSONUpdate(jsonInsert, USER);
		
		final ResultSet rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(4, rs.getRows().size());
	}
	
	@Test
	public void testDeleteUserData() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		
		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String jsonInsert = FileLoader.loadResource("/json/update/update4.json");
		jsonDataset.executeJSONUpdate(jsonInsert, USER);
		
		ResultSet rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(4, rs.getRows().size());
		
		final String jsonDelete = FileLoader.loadResource("/json/delete/delete5.json");
		jsonDataset.executeJSONUpdate(jsonDelete, USER);
		
		rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(3, rs.getRows().size());
		
		jsonDataset.reloadDatabase();
		
		rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(3, rs.getRows().size());
	}
	
	@Test
	public void testInsertUserDataReloadDump() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);

		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String jsonInsert = FileLoader.loadResource("/json/update/update4.json");
		jsonDataset.executeJSONUpdate(jsonInsert, USER);
		
		ResultSet rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(4, rs.getRows().size());
	
		jsonDataset.reloadDatabase();
		
		rs = new ResultSet(jsonDataset.executeSQLSelect("SELECT * FROM results", "", DataOrigin.ANY));
		
		assertEquals(4, rs.getRows().size());
	}
	
	@Test
	public void testGetInfo() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DatabaseCreationException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);	
		final DataSourceInfo datasetInfo = jsonDataset.getInfo();
		
		assertEquals(ConnectorType.json, datasetInfo.getType());
		
		assertEquals(1, datasetInfo.getTables().size());
		
		final ColumnInfo columnInfo = datasetInfo.getTable("results").getPrimaryKey();
		assertEquals("key1", columnInfo.getName());
		assertEquals("VARCHAR", columnInfo.getType());
		assertTrue(columnInfo.isKey());
	}
	
	@Test
	public void testCreateQueryAll() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping4.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final String query = jsonDataset.createQueryAll();
		
		final String expected = "SELECT results.key1, results.key2, "
				+ "results_key3.key1, results_key3.key2, "
				+ "results_key3_key3.key3 "
				+ "FROM results, results_key3, results_key3_key3 "
				+ "WHERE results.key1 = results_key3.parent_id AND results_key3._id = results_key3_key3.parent_id;";
		
		assertEquals(expected, query);
	}
	
	@Test
	public void testGetAllDataCompactJSON() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, TranslationException, DataSourceConnectorException {
		final String mapping = FileLoader.loadResource("/json/mapping/jsonmapping4.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(mapping);
		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);
		
		final JsonObject result = jsonDataset.queryAllJson(USER, DataOrigin.ANY);
		
		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final String jsonResult = gson.toJson(result);
		
		final String json = FileLoader.loadResource("/json/data/jsondata4.json");
				
		assertEquals(json, jsonResult);
	}
	
	@Test
	public void testGetInfoRenamedRootTable() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DatabaseCreationException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping9.json");		
		final JSONConnector jsonMapping = (JSONConnector) ConnectorFactory.load(json);
		final JSONDataSource jsonDataset = new JSONDataSource(jsonMapping, DATASET_ID, true);	
		final DataSourceInfo datasetInfo = jsonDataset.getInfo();
		
		assertEquals(ConnectorType.json, datasetInfo.getType());
		
		assertEquals(2, datasetInfo.getTables().size());
		
		final ColumnInfo columnInfo = datasetInfo.getTable("test").getPrimaryKey();
		assertEquals("key1", columnInfo.getName());
		assertEquals("INTEGER", columnInfo.getType());
		assertTrue(columnInfo.isKey());
	}
	
	public static int MAX_FILES = 9;
	public static @DataPoints SQLTestData[] testData = new SQLTestData[MAX_FILES];
	
	static {

		for (int i = 0; i < MAX_FILES; i++)
			testData[i] = new SQLTestData("/json/queries/sql_query" + (i + 1) + ".txt",
								"/json/mapping/jsonmapping" + (i + 1) + ".json",
								"/json/results/query_result" + (i + 1) + ".json");
	}
}