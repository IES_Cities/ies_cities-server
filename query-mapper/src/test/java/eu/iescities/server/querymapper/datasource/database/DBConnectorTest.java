/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector.DBType;

public class DBConnectorTest {
	
	@Test
	public void testDBConnector() throws DataSourceManagementException {
		try (DBConnector connector = new DBConnector(DBType.MySQL, "localhost", "database", "user", "pass")) {
			assertEquals(DBType.MySQL, connector.getDbType());
			assertEquals("database", connector.getDatabase());
			assertEquals("user", connector.getUser());
			assertEquals("pass", connector.getPass());
		}
	}
	
	@Test
	public void testGetConnectionURL() throws DataSourceManagementException {
		try (DBConnector mysqlConnector = new DBConnector(DBType.MySQL, "localhost", "database", "user", "pass")) {
			assertEquals("jdbc:mysql://localhost/database?user=user&password=pass", mysqlConnector.getConnectionURL());
		}
		
		try (DBConnector postgresqlConnector = new DBConnector(DBType.PostgreSQL, "localhost", "database", "user", "pass")) {
			assertEquals("jdbc:postgresql://localhost/database?user=user&password=pass", postgresqlConnector.getConnectionURL());
		}
		
		try (DBConnector sqliteConnector = new DBConnector(DBType.SQLite, "localhost", "database", "user", "pass")) {		
			assertEquals("jdbc:sqlite:database", sqliteConnector.getConnectionURL());
		}
	}
		
	@Test
	public void testToString() throws DataSourceManagementException {
		try (final DBConnector connector = new DBConnector(DBType.MySQL, "localhost", "database", "user", "pass")) {
			final String expected = "{\n" +
									  "  \"dbType\": \"MySQL\",\n" +
									  "  \"host\": \"localhost\",\n" + 
									  "  \"database\": \"database\",\n" +
									  "  \"user\": \"user\",\n" + 
									  "  \"pass\": \"pass\",\n" + 
									  "  \"mapping\": \"database\",\n" +
									  "  \"permissions\": {\n" +
									  "    \"select\": [],\n" + 
									  "    \"insert\": [],\n" +
									  "    \"delete\": [],\n" +
									  "    \"update\": []\n" +
									  "  }\n" +
									"}";
			
			assertEquals(expected, connector.toString());
		}
	}
	
	@Test
	public void loadJSON() {
		final String json = "{\n" +
				  "  \"dbType\": \"MySQL\",\n" +
				  "  \"host\": \"localhost\",\n" + 
				  "  \"database\": \"database\",\n" +
				  "  \"user\": \"user\",\n" + 
				  "  \"pass\": \"pass\",\n" + 
				  "  \"mapping\": \"database\",\n" +
				  "  \"permissions\": {\n" +
				  "    \"select\": [],\n" + 
				  "    \"insert\": [],\n" +
				  "    \"delete\": [],\n" +
				  "    \"update\": []\n" +
				  "  }\n" +
				"}";
		
		final DBConnector connector = DBConnector.load(json);
		
		assertEquals(json, connector.toString());
	}
}
