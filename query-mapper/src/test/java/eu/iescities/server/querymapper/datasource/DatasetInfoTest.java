/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;

public class DatasetInfoTest {

	private DataSourceInfo info;
	
	@Before
	public void setUp() {
		info = new DataSourceInfo(ConnectorType.json);
		
		DataSourceInfo.TableInfo tableInfo = new DataSourceInfo.TableInfo();
		tableInfo.setName("testTable");
		
		DataSourceInfo.ColumnInfo columnInfo = new DataSourceInfo.ColumnInfo();
		columnInfo.setName("testColumn");
		columnInfo.setType("testType");
		tableInfo.addColumnInfo(columnInfo);
		
		info.addTable(tableInfo);
	}
	
	@Test
	public void testGetType() {
		assertEquals(ConnectorType.json, info.getType());
	}
	
	@Test
	public void testGetTables() {
		assertEquals(1, info.getTables().size());
		
		DataSourceInfo.TableInfo tableInfo = info.getTables().get(0);
		assertEquals("testTable", tableInfo.getName());
		
		assertEquals(1, tableInfo.getColumns().size());
		DataSourceInfo.ColumnInfo columnInfo = tableInfo.getColumns().get(0);
		
		assertEquals("testColumn", columnInfo.getName());
		assertEquals("testType", columnInfo.getType());
	}
}
