/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.evaluation.query;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.iescities.server.querymapper.util.FileLoader;

public class QueryExperimentFoaf extends QueryExperiment {
	
	private static int MAX_QUERIES = 22;

	public static void main(String[] args) throws Exception {
		QueryExperiment experiment = new QueryExperimentFoaf();
		experiment.run();
	}
	
	private List<String> getQueries() throws IOException {
		List<String> queries = new ArrayList<String>();
		for (int i = 0; i < MAX_QUERIES; i++) {
			final String fileName = "src/test/resources//queries/sql/sql_query" + (i + 1) + ".txt";
			final String sqlQuery = FileLoader.loadReader(new FileReader(fileName));
			
			queries.add(sqlQuery);
		}
		
		return queries;
	}

	@Override
	public void run() throws Exception {
		executeQueries("src/test/resources/sparql/foaf_schema.json",
				getQueries());
	}
}
