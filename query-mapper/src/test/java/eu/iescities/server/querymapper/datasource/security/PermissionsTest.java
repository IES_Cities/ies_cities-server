/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;
import eu.iescities.server.querymapper.datasource.security.Permissions.ActionType;
import eu.iescities.server.querymapper.util.FileLoader;

public class PermissionsTest {

	private Permissions p;
	
	@Before
	public void setUp() throws IOException {
		final String jsonPermissions = FileLoader.loadResource("/security/permissions/permissions.json");
		
		final Gson gson = new Gson();
		p = gson.fromJson(jsonPermissions, Permissions.class);
	}
	
	@Test
	public void testGetSelectPermissions() {		
		assertEquals(3, p.getSelectPermissions().size());
		assertEquals(2, p.getDeletePermissions().size());
		assertEquals(2, p.getInsertPermissions().size());
		
		assertEquals("table_name1", p.getSelectPermissions().get(0).getTable());
		assertEquals(AccessType.ALL, p.getSelectPermissions().get(0).getAccess());
		
		assertEquals("table_name2", p.getSelectPermissions().get(1).getTable());
		assertEquals(AccessType.OWNER, p.getSelectPermissions().get(1).getAccess());
		
		assertEquals("table_name3", p.getSelectPermissions().get(2).getTable());
		assertEquals(AccessType.NONE, p.getSelectPermissions().get(2).getAccess());
		
		assertEquals("table_name1", p.getDeletePermissions().get(0).getTable());
		assertEquals(AccessType.NONE, p.getDeletePermissions().get(0).getAccess());
		
		assertEquals("table_name2", p.getDeletePermissions().get(1).getTable());
		assertEquals(AccessType.OWNER, p.getDeletePermissions().get(1).getAccess());
					
		assertEquals("table_name2", p.getInsertPermissions().get(0).getTable());
		assertEquals(AccessType.ALL, p.getInsertPermissions().get(0).getAccess());

		assertEquals("table_name4", p.getInsertPermissions().get(1).getTable());
		assertEquals(AccessType.USER, p.getInsertPermissions().get(1).getAccess());
		
		assertEquals("table_name2", p.getUpdatePermissions().get(0).getTable());
		assertEquals(AccessType.ALL, p.getUpdatePermissions().get(0).getAccess());
		
		assertEquals("table_name5", p.getUpdatePermissions().get(1).getTable());
		assertEquals(AccessType.USER, p.getUpdatePermissions().get(1).getAccess());
	}
	
	@Test
	public void testGetUserList() throws IOException {
		final String jsonPermissions = FileLoader.loadResource("/security/permissions/permissions.json");
		
		final Gson gson = new Gson();
		final Permissions p = gson.fromJson(jsonPermissions, Permissions.class);
		
		assertTrue(p.getUserList(ActionType.SELECT, "table_name1").isEmpty());
		assertTrue(p.getUserList(ActionType.SELECT, "table_name2").isEmpty());
		assertTrue(p.getUserList(ActionType.SELECT, "table_name3").isEmpty());
		
		assertTrue(p.getUserList(ActionType.DELETE, "table_name1").isEmpty());
		assertTrue(p.getUserList(ActionType.DELETE, "table_name2").isEmpty());
		
		assertTrue(p.getUserList(ActionType.INSERT, "table_name2").isEmpty());
		assertEquals(4, p.getUserList(ActionType.INSERT, "table_name4").size());
		
		assertTrue(p.getUserList(ActionType.UPDATE, "table_name2").isEmpty());
		assertEquals(2, p.getUserList(ActionType.UPDATE, "table_name5").size());
	}
	
	@Test
	public void testEquals() throws IOException {
		final String json1 = FileLoader.loadResource("/security/permissions/permissions.json");
		final Gson gson = new Gson();
		final Permissions p1 = gson.fromJson(json1, Permissions.class);
		
		assertEquals(p1, p1);
		
		final String json2 = FileLoader.loadResource("/security/permissions/permissions2.json");
		final Permissions p2 = gson.fromJson(json2, Permissions.class);
		
		assertEquals(p2, p2);
		
		assertFalse(p1.equals(p2));
		assertFalse(p2.equals(p1));
	}
	
	@Test
	public void testGetPermissions() {
		assertEquals(3, p.getSelect().size());
		assertEquals(2, p.getDelete().size());
		assertEquals(2, p.getInsert().size());
		assertEquals(2, p.getUpdate().size());
	}
}
