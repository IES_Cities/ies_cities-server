/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.httpapi;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.httpapi.HTTPApiConnector;
import eu.iescities.server.querymapper.util.FileLoader;

public class HTTPApiMappingTest {
	
	@Test 
	public void testCreateObject() {
		HTTPApiConnector mapping = new HTTPApiConnector("http://localhost/something");
		
		assertEquals(ConnectorType.httpapi, mapping.getType());
		assertEquals("http://localhost/something", mapping.getBaseURL());
	}
	 
	@Test
	public void testMappingLoad() throws IOException {
		final String jsonConnector = FileLoader.loadResource("/redirect/redirect1.json");
		
		HTTPApiConnector connector = HTTPApiConnector.load(jsonConnector);
		
		assertEquals(ConnectorType.httpapi, connector.getType());
		assertEquals("http://echo.jsontest.com", connector.getBaseURL());
	}
	
	@Test
	public void testMappingLoadInputStream() throws IOException {
		InputStream is = HTTPApiConnector.class.getResourceAsStream("/redirect/redirect1.json");
		
		HTTPApiConnector mapping = HTTPApiConnector.load(is);
		
		assertEquals(ConnectorType.httpapi, mapping.getType());
		assertEquals("http://echo.jsontest.com", mapping.getBaseURL());
		
		is.close();
	}
	
	@Test(expected=IOException.class)
	public void testMappingLoadException() throws IOException {
		InputStream is = HTTPApiConnector.class.getResourceAsStream("/redirect/invalid.json");
	
		try {
			HTTPApiConnector.load(is);
		} finally {
			is.close();
		}
	}
	
	@Test
	public void testToString() throws IOException {
		InputStream is = HTTPApiConnector.class.getResourceAsStream("/redirect/redirect1.json");
		
		HTTPApiConnector mapping = HTTPApiConnector.load(is);
		
		String expected = "{\n" + 
						"  \"baseURL\": \"http://echo.jsontest.com\",\n" + 
						"  \"mapping\": \"httpapi\",\n" +
						  "  \"permissions\": {\n" +
						  "    \"select\": [],\n" + 
						  "    \"insert\": [],\n" +
						  "    \"delete\": [],\n" +
						  "    \"update\": []\n" +
						  "  }\n" +
						"}";
		
		assertEquals(expected, mapping.toString());
	}
}
