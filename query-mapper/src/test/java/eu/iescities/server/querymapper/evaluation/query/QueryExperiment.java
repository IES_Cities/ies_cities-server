/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.evaluation.query;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;

public abstract class QueryExperiment {
	
	private static int REPEATS = 120;
	private static int DISCARD = 20;
	
	protected RowStats getMean(List<RowStats> data) {
		float totalRows = 0;
		float totalTransformTime = 0;
		float totalExecutionTime = 0;
		
		for (RowStats row : data) {
			totalRows += row.getRows();
			totalTransformTime += row.getTransformTime();
			totalExecutionTime += row.getExecutionTime();
		}
		
		RowStats mean = new RowStats(totalRows / (float)data.size(), 
				totalTransformTime / (float)data.size(), 
				totalExecutionTime / (float)data.size());
		
		return mean;
	}
	
	public abstract void run() throws Exception;

	protected void executeQueries(String schemaFile, List<String> queries) throws Exception {
		FileInputStream is = new FileInputStream(schemaFile);
		SPARQLConnector schema = SPARQLConnector.load(is);
		is.close();
		
		SPARQLDataSource query = new SPARQLDataSource(schema);
		
		System.out.println("#Experiment 1");
		System.out.println("#Repeats: " + (REPEATS - DISCARD));
		System.out.println("#Warm up: " + DISCARD);
		System.out.println("#Queries: " + queries.size());
		
		List<RowStats> data = new ArrayList<RowStats>();
		int queryCounter = 1;
		for (String sqlQuery : queries) {
			System.out.println("Executing Query " + queryCounter);
			List<RowStats> queryData = new ArrayList<RowStats>();
			for (int i = 0; i < REPEATS; i++) {
				if (i >= DISCARD) {
					final ResultSet results = new ResultSet(query.executeSQLSelect(sqlQuery, "", DataOrigin.ANY));
					long transformTime = query.getTranslationTime();
					long executionTime = query.getExecutionTime();
					queryData.add(new RowStats(results.getRows().size(), 
							transformTime, 
							executionTime));
				}
			}
			
			RowStats queryMean = getMean(queryData);	
			data.add(queryMean);
			
			queryCounter++;
		}
		
		RowStats mean = getMean(data);
		
		System.out.println("Rows,Transform,Query,Total,Ratio");
		System.out.println(String.format((Locale)null, "%.0f & %.2f & %.2f & %.2f & %.2f",
			mean.getRows(),
			mean.getTransformTime(),
			mean.getExecutionTime(),
			mean.getTotalTime(),
			mean.getRatio()));
	}
}
