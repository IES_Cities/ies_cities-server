/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.exporter;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.util.FileLoader;

public class SchemaExporterTest {

	private SPARQLConnector schema;
	
	@Before
	public void setUp() throws IOException, DataSourceConnectorException {		
		final String jsonConnector = FileLoader.loadResource("/sparql/foaf_schema.json");
		
		schema = (SPARQLConnector) ConnectorFactory.load(jsonConnector);
	}
	
	@Test
	public void testGetSQLTables() {
		SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schema);
		
		String expected = "CREATE TABLE sioc_UserAccount\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	foaf_accountServiceHomepage VARCHAR,\n" + 
				"	foaf_page VARCHAR,\n" + 
				"	sioc_id INTEGER,\n" + 
				"	sioc_name VARCHAR\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE dcterms_title_table\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL,\n" + 
				"	dcterms_title VARCHAR,\n" + 
				"	CONSTRAINT id FOREIGN KEY(id) REFERENCES airport_Airport(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE airport_Airport\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	airport_iataCode VARCHAR\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Image\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	dcterms_format VARCHAR,\n" + 
				"	dcterms_title VARCHAR\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Person_rel_worksWith\n" + 
				"(\n" + 
				"	left_id VARCHAR NOT NULL,\n" + 
				"	right_id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	CONSTRAINT left_id FOREIGN KEY(left_id) REFERENCES foaf_Person(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Person_foaf_account\n" + 
				"(\n" + 
				"	left_id VARCHAR NOT NULL,\n" + 
				"	right_id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	CONSTRAINT left_id FOREIGN KEY(left_id) REFERENCES foaf_Person(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Person_foaf_interest\n" + 
				"(\n" + 
				"	left_id VARCHAR NOT NULL,\n" + 
				"	right_id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	CONSTRAINT left_id FOREIGN KEY(left_id) REFERENCES foaf_Person(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Person_foaf_knows\n" + 
				"(\n" + 
				"	left_id VARCHAR NOT NULL,\n" + 
				"	right_id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	CONSTRAINT left_id FOREIGN KEY(left_id) REFERENCES foaf_Person(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_nick_table\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL,\n" + 
				"	foaf_nick VARCHAR,\n" + 
				"	CONSTRAINT id FOREIGN KEY(id) REFERENCES foaf_Person(id)\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Person\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	w3con_nearestAirport VARCHAR,\n" + 
				"	foaf_age INTEGER,\n" + 
				"	foaf_based_near VARCHAR,\n" + 
				"	foaf_depiction VARCHAR,\n" + 
				"	foaf_firstName VARCHAR,\n" + 
				"	foaf_gender VARCHAR,\n" + 
				"	foaf_homepage VARCHAR,\n" + 
				"	foaf_isDescribedIn VARCHAR,\n" + 
				"	foaf_mbox VARCHAR,\n" + 
				"	foaf_name VARCHAR,\n" + 
				"	foaf_schoolHomepage VARCHAR,\n" + 
				"	foaf_surname VARCHAR,\n" + 
				"	foaf_title VARCHAR,\n" + 
				"	foaf_workplaceHomepage VARCHAR\n" + 
				");\n" + 
				"\n" + 
				"CREATE TABLE foaf_Document\n" + 
				"(\n" + 
				"	id VARCHAR NOT NULL PRIMARY KEY,\n" + 
				"	dcterms_created VARCHAR,\n" + 
				"	dcterms_license VARCHAR,\n" + 
				"	dcterms_modified VARCHAR,\n" + 
				"	admin_errorReportsTo VARCHAR,\n" + 
				"	foaf_maker VARCHAR,\n" + 
				"	foaf_primaryTopic VARCHAR\n" + 
				");\n\n";
		
		assertEquals(expected, exporter.getSQLCreateTables(false));
	}
}
