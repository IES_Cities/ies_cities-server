/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.database;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;

import org.databene.contiperf.PerfTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.ResultSet.Row;
import eu.iescities.server.querymapper.datasource.query.ResultSet.Row.Column;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;
import util.DBUtility;

@RunWith(Theories.class)
public class DBDataSourceTest {
	
	private final String USER_1 = "user_1";
	private final String USER_2 = "user_2";
	
	@BeforeClass
	public static void setUpBeforeClass() throws SQLException, ClassNotFoundException {
		DBUtility.createDatabase();
	} 
	
	@Test(expected=DataSourceManagementException.class)
	public void testExecuteSQLInvalidCreate() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		String sqlUpdate = "CREATE TABLE Test ("
				+ "PersonID int, "
				+ "LastName varchar(255), "
				+ "FirstName varchar(255), "
				+ "Address varchar(255), "
				+ "City varchar(255),"
				+ "_row_user_ varchar(255));";
		
		updateDatabase.executeSQLUpdate(sqlUpdate, USER_1);
	}
	
	@Test(expected=DataSourceManagementException.class)
	public void testExecuteSQLInvalidDrop() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		String sqlDelete = "drop table Test;";
		
		updateDatabase.executeSQLUpdate(sqlDelete, USER_1);
	}
	
	@Test
	public void testExecuteSQLUpdate() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		int insertedRows = updateDatabase.executeSQLUpdate("insert into people (id, name) values (null, 'test_name');", USER_1);
		
		assertEquals(1, insertedRows);
		
		int updatedRows = updateDatabase.executeSQLUpdate("update people set name = 'updated_name' where name = 'test_name';", USER_1);
		
		assertEquals(1, updatedRows);
		
		int deletedRows = updateDatabase.executeSQLUpdate("delete from people where name = 'updated_name';", USER_1);
		
		assertEquals(1, deletedRows);
	}
	
	@Test
	public void testExecuteSQLMultipleUpdate() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		int insertedRows = updateDatabase.executeSQLUpdate("insert into people (id, name) values (null, 'test_name');", USER_1);
		
		assertEquals(1, insertedRows);
		
		int updatedRows = updateDatabase.executeSQLUpdate("update people set name = 'updated_name' where name = 'test_name';", USER_1);
		
		assertEquals(1, updatedRows);
		
		int deletedRows = updateDatabase.executeSQLUpdate("DELETE FROM people where name = 'updated_name';", USER_1);
		
		assertEquals(1, deletedRows);
	}
	
	@Test
	public void testExecuteSQLTransaction() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		final String text = 	"insert into people (id, name) values (null, 'test_name');\n" +
								"update people set name = 'updated_name' where name = 'test_name';\n" +
								"DELETE FROM people where name = 'updated_name';";
								
		int modifiedRows = updateDatabase.executeSQLTransactionUpdate(text, USER_1);
		
		assertEquals(3, modifiedRows);
	}
	
	@Test(expected=DataSourceManagementException.class)
	public void testExecuteSQLTransactionRollback() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		final String text = 	"insert into people (id, name) values (null, 'test_name');\n" +
								"update invalid_table set name = 'updated_name' where name = 'test_name';\n" +
								"DELETE FROM people where name = 'updated_name';";
								
		updateDatabase.executeSQLTransactionUpdate(text, USER_1);
	}
	
	@Test(expected=DataSourceManagementException.class)
	public void testExecuteSQLTransactionEmtpy() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		final String text = "";
								
		updateDatabase.executeSQLTransactionUpdate(text, USER_1);
	}
	
	@Test(expected=DataSourceSecurityManagerException.class)
	public void testExecuteSQLInsertInvalidUser() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		
		String sqlInsert = "insert into people (id, name) values (null, 'test_name');";
		
		updateDatabase.executeSQLUpdate(sqlInsert, USER_2);
	}

	@Test
	@PerfTest(invocations = 30000, threads = 100)
	public void testExecuteSelectSQL() throws Exception {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select * from people";

		ResultSet results = new ResultSet(queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY));

		ResultSet expected = new ResultSet();
		
		Row row1 = expected.createRow();
		row1.addColumn(new Column("name", "Gandhi"));
		row1.addColumn(new Column("occupation", "politics"));
		row1.addColumn(new Column("id", "1"));
		row1.addColumn(new Column("born", "1"));
		
		Row row2 = expected.createRow();
		row2.addColumn(new Column("name", "Turing"));
		row2.addColumn(new Column("occupation", "computers"));
		row2.addColumn(new Column("id", "2"));
		row2.addColumn(new Column("born", "2"));
		
		Row row3 = expected.createRow();
		row3.addColumn(new Column("name", "Wittgenstein"));
		row3.addColumn(new Column("occupation", "smartypants"));
		row3.addColumn(new Column("id", "3"));
		row3.addColumn(new Column("born", "3"));
		
		Row row4 = expected.createRow();
		row4.addColumn(new Column("name", "Sherlock Holmes"));
		row4.addColumn(new Column("occupation", "detective"));
		row4.addColumn(new Column("id", "4"));
		row4.addColumn(new Column("born", "2"));
		
		assertEquals(expected, results);
	}
	
	@Test
	public void testExecuteSelectNotManagedData() throws Exception {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select * from people";
		
		queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
	}
	
	@Test
	public void testExecuteSelectKeyPresent() throws Exception {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select name, id from people";
		
		queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
	}
	
	@Test(expected=DataSourceSecurityManagerException.class)
	public void testExecuteSelectNONEPermission() throws Exception {
		final String jsonMapping = FileLoader.loadResource("/security/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		String sqlQuery = "select * from people";
		
		queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
	}
	
	static public class JSONData {

		private final String jsonData;
		private final String dbConfig;

		public JSONData(String sqlFile, String dbConfig) {
			this.jsonData = sqlFile;
			this.dbConfig = dbConfig;
		}

		public String getJSONFile() { return jsonData; }
		public String getDBConfigFile() { return dbConfig; }
	}
	
	@Test
	public void testDeleteValidUser() throws IOException, TranslationException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String jsonInsert = FileLoader.loadResource("/json/update/update2.json");
		final String jsonDelete = FileLoader.loadResource("/json/delete/delete1.json");
		final String jsonMapping = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");
				
		DBConnector connector = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		updateDatabase.executeSQLUpdate("DELETE FROM people WHERE occupation = 'physicist';", USER_1);
		
		JSONUpdateable updateJson = new DBDataSource(connector, false);
				 
		int insertedRows = updateJson.executeJSONUpdate(jsonInsert, USER_1);
		assertEquals(2, insertedRows);
		
		int deletedRows = updateJson.executeJSONUpdate(jsonDelete, USER_1);
		assertEquals(2, deletedRows);
	}
	
	@Test(expected=DataSourceSecurityManagerException.class)
	public void testDeleteInvalidUser() throws IOException, TranslationException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String jsonInsert = FileLoader.loadResource("/json/update/update2.json");
		final String jsonDelete = FileLoader.loadResource("/json/delete/delete1.json");
		final String jsonMapping = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");
				
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLUpdateable updateDatabase = new DBDataSource(dbConfig, false);
		updateDatabase.executeSQLUpdate("DELETE FROM people WHERE occupation = 'physicist';", USER_1);
		
		JSONUpdateable updateJson = new DBDataSource(dbConfig, false);
				 
		int insertedRows = updateJson.executeJSONUpdate(jsonInsert, USER_1);
		assertEquals(2, insertedRows);
		
		updateJson.executeJSONUpdate(jsonDelete, USER_2);
	}

	@Test
	public void testJSONRollback() throws IOException, TranslationException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String jsonInsert = FileLoader.loadResource("/json/update/update2.json");
		final String jsonMapping = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");
				
		DBConnector connector = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLUpdateable updateDatabase = new DBDataSource(connector, false);
		updateDatabase.executeSQLUpdate("DELETE FROM people WHERE occupation = 'physicist';", USER_1);
		
		JSONUpdateable updateJson = new DBDataSource(connector, false);
		
		final SQLQueriable queriable = new DBDataSource(connector, false);
		ResultSet rs = new ResultSet(queriable.executeSQLSelect("SELECT * FROM people WHERE occupation = 'physicist';", "", DataOrigin.ANY));
		assertEquals(0, rs.getRows().size());
				 
		int insertedRows = updateJson.executeJSONUpdate(jsonInsert, USER_1);
		assertEquals(2, insertedRows);
		
		final String jsonUpdate = FileLoader.loadResource("/json/update/update3.json");
		
		try {
			updateJson.executeJSONUpdate(jsonUpdate, USER_1);
		} catch (DataSourceManagementException e) {			
			rs = new ResultSet(queriable.executeSQLSelect("SELECT * FROM people WHERE occupation = 'physicist';", "", DataOrigin.ANY));
			assertEquals(2, insertedRows);
		}
	}
	
	@Test(expected=DataSourceManagementException.class)
	public void testExecuteInvalidSQLUpdate() throws Exception {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLUpdateable updateable = new DBDataSource(dbConfig, false);
		
		String sqlInsert = "INSERT into table (1, 2,3) VALUES ('value1', value2, value3);INSERT into table (1, 2,3) VALUES ('value1', value2, value3);";

		updateable.executeSQLUpdate(sqlInsert, USER_1);
	}
	
	@Test
	public void testGetTables() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLQueriable queriable = new DBDataSource(dbConfig, false);
		
		assertEquals(2, queriable.getTables().getRows().size());
	}
	
	@Test
	public void testGetInfo() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DataSourceConnectorException {
		final String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		final DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		final SQLQueriable queriable = new DBDataSource(dbConfig, false);
			
		final DataSourceInfo datasetInfo = queriable.getInfo();
		
		assertEquals(ConnectorType.database, datasetInfo.getType());
		
		assertEquals(2, datasetInfo.getTables().size());
		
		assertEquals(2, datasetInfo.getTables().get(0).getColumns().size());
		assertEquals(4, datasetInfo.getTables().get(1).getColumns().size());
	}
}