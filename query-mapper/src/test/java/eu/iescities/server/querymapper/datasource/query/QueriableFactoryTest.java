/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.query;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.csv.CSVConnector;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.util.FileLoader;

public class QueriableFactoryTest {
	
	@Before
	public void setUp() throws Exception {
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
	}

	@Test
	public void testCreateJSONQueriable() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, IOException, QueriableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector connector = (JSONConnector) ConnectorFactory.load(json);
		final SQLQueriable queriable = QueriableFactory.createQueriable(connector, "0", true);
		
		assertEquals(ConnectorType.json, queriable.getInfo().getType());
	}
	
	@Test
	public void testCreateCSVQueriable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/csv/mapping/csvmapping1.json");
		final CSVConnector connector = (CSVConnector) ConnectorFactory.load(json);
		final SQLQueriable queriable = QueriableFactory.createQueriable(connector, "0", true);
		
		assertEquals(ConnectorType.csv, queriable.getInfo().getType());
	}
	
	@Test
	public void testCreateUserSchemaQueriable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");		
		final JSONSchemaConnector connector = (JSONSchemaConnector) ConnectorFactory.load(json);
		final SQLQueriable queriable = QueriableFactory.createQueriable(connector, "0", true);
		
		assertEquals(ConnectorType.json_schema, queriable.getInfo().getType());
	}
	
	@Test
	public void testCreateDatabaseQueriable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/dbconfig/sqlite_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		final SQLQueriable queriable = QueriableFactory.createQueriable(connector, "0", true);
		
		assertEquals(ConnectorType.database, queriable.getInfo().getType());
	}
	
	@Test
	public void testCreateSPARQLQueriable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException {
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");		
		final SPARQLConnector connector = SPARQLConnector.load(jsonSchema);
		final SQLQueriable queriable = QueriableFactory.createQueriable(connector, "0", true);
		
		assertEquals(ConnectorType.sparql, queriable.getInfo().getType());
	}
}
