/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResultSet {

	static public class Row {

		static public class Column {

			private final String id;
			private final String value;

			public Column(String id, String value) {
				this.id = id;
				this.value = value;
			}

			public String getID() { return id; }
			public String getValue() { return value; }
		}

		private List<Column> columns = new ArrayList<Column>();

		public void addColumn(Column column) {
			columns.add(column);
		}

		public List<Column> getColumns() {
			return Collections.unmodifiableList(columns);
		}

		public Column getColumn(String id) {
			for (Column column : columns)
				if (column.getID().equals(id))
					return column;
			return null;
		}
	}

	private List<Row> rows = new ArrayList<Row>();
	
	public ResultSet() {
		
	}
	
	public ResultSet(eu.iescities.server.querymapper.datasource.query.serialization.ResultSet results) {
		for (eu.iescities.server.querymapper.datasource.query.serialization.Row resultsRow : results.getRows()) {
			final ResultSet.Row row = this.createRow();
			for (String resultsColumn : resultsRow.getNames()) {
				final ResultSet.Row.Column column = new ResultSet.Row.Column(resultsColumn, resultsRow.getValue(resultsColumn).toString());
				row.addColumn(column);
			}
		}
	}

	public Row createRow() {
		Row row = new Row();
		rows.add(row);
		return row;
	}

	public List<Row> getRows() {
		return Collections.unmodifiableList(rows);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ResultSet))
			return false;

		ResultSet rs = (ResultSet) o;

		List<Row> rows = rs.getRows();

		if (rows.size() != getRows().size())
			return false;

		int rowIndex = 0;
		for (ResultSet.Row row : getRows()) {
			Row rsRow = rows.get(rowIndex);

			List<Row.Column> rsColumns = rsRow.getColumns();

			if (rsColumns.size() != row.getColumns().size())
				return false;

			for (Row.Column column : row.getColumns() ) {
				Row.Column rsColumn = rsRow.getColumn(column.getID());

				if (!column.getID().equals(rsColumn.getID()) ||
					!column.getValue().equals(rsColumn.getValue()))
					return false;
			}
			rowIndex++;
		}

		return true;
	}

	private String getStrFormat(int[] sizes) {
		StringBuilder strFormat = new StringBuilder();
			
		for (int i = 0; i < sizes.length; i++)
			strFormat.append(String.format("| %%-%ss ", sizes[i]));
		strFormat.append("|%n");

		return strFormat.toString();
	}

	private String[] getIDs(Row row) {
		String[] ids = new String[row.getColumns().size()];
		for (int i = 0; i < ids.length; i++)
			ids[i] = row.getColumns().get(i).getID();
		return ids;
	}

	private String[] getValues(Row row) {
		String[] values = new String[row.getColumns().size()];
		for (int i = 0; i < values.length; i++)
			values[i] = row.getColumns().get(i).getValue();
		return values;
	}

	private int[] getSizes() {
		int[] sizes = null;

		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				sizes = new int[row.getColumns().size()];
				firstRow = false;
			}

			for (int i = 0; i < sizes.length; i++) {
				final String value = row.getColumns().get(i).getValue();
				if (value != null) {
					if (value.length() > sizes[i]) {
						sizes[i] = row.getColumns().get(i).getValue().length();
					}
				} else {
					sizes[i] = 4;
				}
			}
		}

		return sizes;
	}

	private String getLine(int[] sizes) {
		StringBuilder strBuilder = new StringBuilder();

		for (int i = 0; i < sizes.length; i++) {
			String column = new String(new char[sizes[i] + 2]).replace('\0', '-');
			strBuilder.append("+" + column);
		}

		strBuilder.append("+\n");
		return strBuilder.toString();
	}

	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();

		int[] sizes = getSizes();
		String line = getLine(sizes);

		String strFormat = getStrFormat(sizes);

		strBuilder.append(line);

		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				strBuilder.append(String.format(strFormat, (Object[]) getIDs(row)));
				firstRow = false;

				strBuilder.append(line);
			}
			
			strBuilder.append(String.format(strFormat, (Object[]) getValues(row)));
		}

		strBuilder.append(line);

		return strBuilder.toString();
	}
	
	public String toJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
	
	public void merge(ResultSet rs) {
		this.rows.addAll(rs.rows);
	}
	
	private void writeLine(String[] elements, StringBuilder strBuilder) {
		int counter = 0;
		for (String e : elements) {
			if (counter > 0) {
				strBuilder.append(",");
			}
			
			strBuilder.append(e);
			counter++;
		}
	}
	
	public String toCSV() {
		StringBuilder strBuilder = new StringBuilder();
		
		boolean firstRow = true;
		for (Row row : rows) {
			if (firstRow) {
				writeLine(getIDs(row), strBuilder);
				strBuilder.append("\n");
				firstRow = false;
			}
			
			writeLine(getValues(row), strBuilder);
			strBuilder.append("\n");
		}
		
		return strBuilder.toString();
	}
}