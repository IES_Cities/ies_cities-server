/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.translator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.translator.SPARQLTranslator;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.sql.schema.translator.listener.TranslatedQuery;
import eu.iescities.server.querymapper.util.FileLoader;

@RunWith(Theories.class)
public class SPARQLTranslatorTest {

	static public class TestData {

		private final String sqlFile;
		private final String sparqlFile;
		private final String schemaFile;

		public TestData(String sqlFile, String sparqlFile, String schemaFile) {
			this.sqlFile = sqlFile;
			this.sparqlFile = sparqlFile;
			this.schemaFile = schemaFile;
		}

		public String getSQLFile() { return sqlFile; }
		public String getSPARQLFile() { return sparqlFile; }
		public String getSchemaFile() { return schemaFile; }
	}

	static public class InvalidData {

		private final String sqlFile;
		private final String schemaFile;

		public InvalidData(String sqlFile, String schemaFile) {
			this.sqlFile = sqlFile;
			this.schemaFile = schemaFile;
		}

		public String getSQLFile() { return sqlFile; }
		public String getSchemaFile() { return schemaFile; }
	}

	private TranslatedQuery translate(TestData data) throws Exception {		
		final String sqlQuery = FileLoader.loadResource(data.getSQLFile());

		System.out.println(String.format("TestData: {%s, %s, %s}",
			data.getSQLFile(), data.getSchemaFile(), data.getSPARQLFile()));

		final String jsonSchema = FileLoader.loadResource(data.getSchemaFile());
		
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		SPARQLTranslator translator = new SPARQLTranslator(schema);

		return translator.translate(sqlQuery);
	}

	@Theory
	public void testTranslate(TestData data) throws Exception {
		TranslatedQuery translatedQuery = translate(data);
		
		final String sparqlQuery = FileLoader.loadResource(data.getSPARQLFile());
		
		assertEquals(sparqlQuery.replaceAll("\\r\\n?", "\n"), translatedQuery.getSPARQLQuery());
	}

	@Theory
	public void testInvalidSQL(InvalidData data) throws Exception {
		try {
			translate(new TestData(data.getSQLFile(), "", data.getSchemaFile()));
			
			fail("TranslationException expected");
		} catch (TranslationException te) {}
	}

	public static int MAX_INVALID = 6;
	public static @DataPoints InvalidData[] invalidData = new InvalidData[MAX_INVALID];

	public static int MAX_QUERIES = 22;
	public static @DataPoints TestData[] testData = new TestData[MAX_QUERIES];

	static {

		for (int i = 0; i < MAX_INVALID; i++)
			invalidData[i] = new InvalidData("/queries/invalid_sql/sql_invalid" + (i + 1) + ".txt",
									"/sparql/simple_schema.json");

		for (int i = 0; i < MAX_QUERIES; i++)
			testData[i] = new TestData("/queries/sql/sql_query" + (i + 1) + ".txt",
								"/queries/sparql/sparql_query" + (i + 1) + ".txt",
								"/sparql/foaf_schema.json");
	}
	
	@Test(expected=TranslationException.class)
	public void testTranslateNullSchema() throws TranslationException {
		SPARQLTranslator translator = new SPARQLTranslator(null);
		
		translator.translate("select * from table;");
	}
}