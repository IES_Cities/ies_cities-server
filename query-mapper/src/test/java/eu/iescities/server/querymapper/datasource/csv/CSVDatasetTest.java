/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.csv;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.google.gson.Gson;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.util.FileLoader;

@RunWith(Theories.class)
public class CSVDatasetTest {
	
	static public class SQLTestData {

		private final String sqlFile;
		private final String mappingFile;
		private final String resultsFile;

		public SQLTestData(String sqlFile, String schemaFile, String resultsFile) {
			this.sqlFile = sqlFile;
			this.mappingFile = schemaFile;
			this.resultsFile = resultsFile;
		}

		public String getSQLFile() { return sqlFile; }
		public String getMappingFile() { return mappingFile; }
		public String getResultsFile() { return resultsFile; }
	}
	
	@Before
	public void setUp() throws Exception {
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
	}

	@Theory
	public void testExecuteSelectSQL(SQLTestData data) throws Exception {
		System.out.println(String.format("CSVSQLData: {%s, %s, %s}",
			data.getSQLFile(), data.getMappingFile(), data.getResultsFile()));
		
		final String sqlQuery = FileLoader.loadResource(data.getSQLFile());
		final String json = FileLoader.loadResource(data.getMappingFile());
		
		CSVConnector jsonMapping = (CSVConnector) ConnectorFactory.load(json);
		CSVDataSource query = new CSVDataSource(jsonMapping, "0", true);

		final ResultSet results = new ResultSet(query.executeSQLSelect(sqlQuery, "", DataOrigin.ANY));

		Gson gson = new Gson();

		InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream(data.getResultsFile()));
		final ResultSet expected = gson.fromJson(reader, ResultSet.class);
		reader.close();

		assertEquals(expected, results);
	}
	
	@Test
	public void testGetTables() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException, DatabaseCreationException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/csv/mapping/csvmapping1.json");
		final CSVConnector jsonMapping = (CSVConnector) ConnectorFactory.load(json);
		final CSVDataSource queriable = new CSVDataSource(jsonMapping, "0", true);
		final ResultSet rs = new ResultSet(queriable.getTables());
		
		assertEquals(1, rs.getRows().size());
	}
	
	@Test
	public void testGetInfo() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DatabaseCreationException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/csv/mapping/csvmapping1.json");
		final CSVConnector jsonMapping = (CSVConnector) ConnectorFactory.load(json);
		final CSVDataSource queriable = new CSVDataSource(jsonMapping, "0", true);
		
		final DataSourceInfo datasetInfo = queriable.getInfo();
		
		assertEquals(ConnectorType.csv, datasetInfo.getType());
		
		assertEquals(1, datasetInfo.getTables().size());
	}

	public static int MAX_FILES = 3;
	public static @DataPoints SQLTestData[] testData = new SQLTestData[MAX_FILES];
	
	static {

		for (int i = 0; i < MAX_FILES; i++)
			testData[i] = new SQLTestData("/csv/queries/sql_query" + (i + 1) + ".txt",
								"/csv/mapping/csvmapping" + (i + 1) + ".json",
								"/csv/results/query_result" + (i + 1) + ".json");
	}
}