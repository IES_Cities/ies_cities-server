/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.update;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.csv.CSVConnector;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.util.FileLoader;

public class UpdateableFactoryTest {

	@Before
	public void setUp() throws Exception {
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
	}
	
	@Test
	public void testCreateJSONUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector connector = (JSONConnector) ConnectorFactory.load(json);
		final SQLUpdateable updateable = UpdateableFactory.createUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.json, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateCSVUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/csv/mapping/csvmapping1.json");
		final CSVConnector connector = (CSVConnector) ConnectorFactory.load(json);
		final SQLUpdateable updateable = UpdateableFactory.createUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.csv, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateUserSchemaUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");		
		final JSONSchemaConnector connector = (JSONSchemaConnector) ConnectorFactory.load(json);
		final SQLUpdateable updateable = UpdateableFactory.createUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.json_schema, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateDatabaseUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/dbconfig/sqlite_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		final SQLUpdateable updateable = UpdateableFactory.createUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.database, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateJSONJSONUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json/mapping/jsonmapping1.json");		
		final JSONConnector connector = (JSONConnector) ConnectorFactory.load(json);
		final JSONUpdateable updateable = UpdateableFactory.createJSONUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.json, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateCSVJSONUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/csv/mapping/csvmapping1.json");
		final CSVConnector connector = (CSVConnector) ConnectorFactory.load(json);
		final JSONUpdateable updateable = UpdateableFactory.createJSONUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.csv, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateUserSchemaJSONUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");		
		final JSONSchemaConnector connector = (JSONSchemaConnector) ConnectorFactory.load(json);
		final JSONUpdateable updateable = UpdateableFactory.createJSONUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.json_schema, updateable.getInfo().getType());
	}
	
	@Test
	public void testCreateDatabaseJSONUpdateable() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final String jsonConnector = FileLoader.loadResource("/dbconfig/sqlite_conf.json");		
		final DBConnector connector = (DBConnector) ConnectorFactory.load(jsonConnector);
		final JSONUpdateable updateable = UpdateableFactory.createJSONUpdateable(connector, "0", true);
		
		assertEquals(ConnectorType.database, updateable.getInfo().getType());
	}
}
