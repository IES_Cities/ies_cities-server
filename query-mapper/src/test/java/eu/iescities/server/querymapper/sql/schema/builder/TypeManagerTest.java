/**
 *  Copyright 2013, 2014 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.sql.schema.builder;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import eu.iescities.server.querymapper.sql.schema.builder.TypeManager;
import eu.iescities.server.querymapper.sql.schema.builder.TypeManager.LiteralType;

public class TypeManagerTest {

	private Model model;
	
	@Before
	public void setUp() {
		model = ModelFactory.createDefaultModel();
	}
	
	@Test
	public void testGetLiteralBoolean() {		
		Literal literal = model.createTypedLiteral(Boolean.TRUE);
		
		assertEquals(LiteralType.BOOLEAN, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralString() {		
		Literal literal = model.createTypedLiteral(new String("test"));
		
		assertEquals(LiteralType.STRING, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralByte() {		
		Literal literal = model.createTypedLiteral(new Byte((byte)255));
		
		assertEquals(LiteralType.BYTE, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralShort() {		
		Literal literal = model.createTypedLiteral(new Short((short)255));
		
		assertEquals(LiteralType.SHORT, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralInt() {		
		Literal literal = model.createTypedLiteral(Integer.MAX_VALUE);
		
		assertEquals(LiteralType.INT, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralLong() {		
		Literal literal = model.createTypedLiteral(Long.MAX_VALUE);
		
		assertEquals(LiteralType.LONG, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralFloat() {		
		Literal literal = model.createTypedLiteral(new Float(1.112356));
		
		assertEquals(LiteralType.FLOAT, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralDouble() {		
		Literal literal = model.createTypedLiteral(new Double(1.0));
		
		assertEquals(LiteralType.DOUBLE, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetLiteralUnknown() {		
		Literal literal = model.createTypedLiteral(new Date());
		
		assertEquals(LiteralType.STRING, TypeManager.getLiteralType(literal));
	}
	
	@Test
	public void testGetSQLTypeBoolean() {
		assertEquals("CHAR(5)", TypeManager.getSQLType(TypeManager.LiteralType.BOOLEAN));
	}
	
	@Test
	public void testGetSQLTypeByte() {
		assertEquals("INTEGER", TypeManager.getSQLType(TypeManager.LiteralType.BYTE));
	}
	
	@Test
	public void testGetSQLTypeShort() {
		assertEquals("INTEGER", TypeManager.getSQLType(TypeManager.LiteralType.SHORT));
	}
	
	@Test
	public void testGetSQLTypeInt() {
		assertEquals("INTEGER", TypeManager.getSQLType(TypeManager.LiteralType.INT));
	}
	
	@Test
	public void testGetSQLTypeLong() {
		assertEquals("BIGINT", TypeManager.getSQLType(TypeManager.LiteralType.LONG));
	}
	
	@Test
	public void testGetSQLTypeFloat() {
		assertEquals("FLOAT", TypeManager.getSQLType(TypeManager.LiteralType.FLOAT));
	}
	
	@Test
	public void testGetSQLTypeDouble() {
		assertEquals("DOUBLE", TypeManager.getSQLType(TypeManager.LiteralType.DOUBLE));
	}
	
	@Test
	public void testGetSQLTypeString() {
		assertEquals("VARCHAR", TypeManager.getSQLType(TypeManager.LiteralType.STRING));
	}
	
	@Test
	public void testInvalidTypes() {
		
	}
}
