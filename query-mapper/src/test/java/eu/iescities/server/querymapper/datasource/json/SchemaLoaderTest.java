package eu.iescities.server.querymapper.datasource.json;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.exporter.SPARQLSchemaExporter;
import eu.iescities.server.querymapper.util.FileLoader;

@RunWith(Theories.class)
public class SchemaLoaderTest {

	class MockConnector extends JSONConnector {
		
		private String file;
		private String root;
		private String key;
		
		public MockConnector(String file, String root, String key) {
			this.file = file;
			this.root = root;
			this.key = key;
		}
		
		@Override
		public Reader getReader() {
			return new InputStreamReader(this.getClass().getResourceAsStream(file));
		}
		
		@Override
		public String getRoot() {
			return root;
		}
		
		@Override
		public String getKey() {
			return key;
		}
	}
	
	static public class TestData {

		private final String jsonFile;
		private final String sqlFile;

		public TestData(String jsonFile, String sqlFile) {
			this.jsonFile = jsonFile;
			this.sqlFile = sqlFile;
		}

		public String getJSONFile() { return jsonFile; }
		public String getSQLFile() { return sqlFile; }
	}
	
	@Before
	public void setUp() {
		Config.getInstance().deleteDBDir();
	}

	@Theory
	public void testLoadSchema(TestData testData) throws InvalidRootException, IOException {
		System.out.println(String.format("JSONSchemaData: {%s, %s}",
				testData.getSQLFile(), testData.getSQLFile()));
		
		final JSONSchemaLoader loader = new JSONSchemaLoader(new MockConnector(testData.getJSONFile(), "results", "key1"));
    	final SPARQLConnector schema = loader.loadSchema();
    	final SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schema);
    	
    	final String tables = exporter.getSQLCreateTables(false).trim();
    	
		final String sqlFile = FileLoader.loadResource(testData.getSQLFile());
    	
    	assertEquals(sqlFile, tables);
	}
	
	@Test
	public void testLoadSchemaNewKey() throws InvalidRootException, IOException {
		final JSONSchemaLoader loader = new JSONSchemaLoader(new MockConnector("/json/data/jsondata7.json", "results", JSONSchemaLoader.TABLE_ID));
    	final SPARQLConnector schema = loader.loadSchema();
    	final SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schema);
    	
    	final String tables = exporter.getSQLCreateTables(false).trim();
    	
		final String sqlFile = FileLoader.loadResource("/json/tables/new_key.txt");
    	
    	assertEquals(sqlFile, tables);
	}
	
	@Test
	public void testLoadEmptyRoot() throws InvalidRootException, IOException {
		final JSONSchemaLoader loader = new JSONSchemaLoader(new MockConnector("/json/data/empty_root.json", "/", "key1"));
    	final SPARQLConnector schema = loader.loadSchema();
    	final SPARQLSchemaExporter exporter = new SPARQLSchemaExporter(schema);
    	
    	final String tables = exporter.getSQLCreateTables(false).trim();
    	
    	final String sqlFile = FileLoader.loadResource("/json/tables/empty_root.txt");
    	
    	assertEquals(sqlFile, tables);
	}
	
	public static int MAX_FILES = 9;
	public static @DataPoints TestData[] testData = new TestData[MAX_FILES];
	
	static {

		for (int i = 0; i < MAX_FILES; i++)
			testData[i] = new TestData("/json/data/jsondata" + (i + 1) + ".json",
								"/json/tables/sql" + (i + 1) + ".txt");
	}
}
