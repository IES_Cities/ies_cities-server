/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.security;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.security.ACLSecurityManager;
import eu.iescities.server.querymapper.datasource.security.AccessInfo;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.security.TablePermission;
import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;
import eu.iescities.server.querymapper.datasource.security.Permissions.ActionType;

public class ACLSecurityManagerTest {
	
	private final String UNAUTHORIZED_USER = "user1";
	private final String AUTHORIZED_USER = "user2";
	private final String TABLE_NAME = "country";
	private final String QUERY = "";
	
	private ACLSecurityManager manager = null;
	
	@Before
	public void setUp() {
		manager = new ACLSecurityManager();
	}

	@Test
	public void testSelectUserList() throws DataSourceSecurityManagerException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.USER);
		tablePermission.addUser(AUTHORIZED_USER);
		p.addSelectPermission(tablePermission);	
		
		assertTrue(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, QUERY, null), p, ActionType.SELECT));
		assertFalse(manager.isAuthorized(new AccessInfo(UNAUTHORIZED_USER, TABLE_NAME, QUERY, null), p, ActionType.SELECT));
	}
	
	@Test
	public void testSelectEmptyUserList() throws DataSourceSecurityManagerException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.USER);
		p.addSelectPermission(tablePermission);	
		
		assertFalse(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, QUERY, null), p, ActionType.SELECT));
		assertFalse(manager.isAuthorized(new AccessInfo(UNAUTHORIZED_USER, TABLE_NAME, QUERY, null), p, ActionType.SELECT));
	}
	
	@Test
	public void testAccessTypeOwnerInsert() throws DataSourceSecurityManagerException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.OWNER);
		p.addInsertPermission(tablePermission);
		
		assertTrue(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, QUERY, null), p, ActionType.INSERT));
	}
	
	@Test
	public void testAccessTypeOwner() throws DataSourceSecurityManagerException, SQLException, ClassNotFoundException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.OWNER);
		p.addSelectPermission(tablePermission);
		
		final String query = "SELECT * FROM country WHERE name='Germany'";
		
		Class.forName("com.mysql.jdbc.Driver");
		
		final Connection conn = DriverManager.getConnection("jdbc:sqlite:db/test_update_security.db");
		
		assertTrue(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, query, conn), p, ActionType.SELECT));
		assertFalse(manager.isAuthorized(new AccessInfo(UNAUTHORIZED_USER, TABLE_NAME, query, conn), p, ActionType.SELECT));
		
		conn.close();
	}
	
	@Test
	public void testAccessTypeOwnerSelectEmptyWhere() throws DataSourceSecurityManagerException, SQLException, ClassNotFoundException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.OWNER);
		p.addSelectPermission(tablePermission);
		
		final String query = "SELECT * FROM country";
		
		Class.forName("com.mysql.jdbc.Driver");
		
		final Connection conn = DriverManager.getConnection("jdbc:sqlite:db/test_update_security.db");
		
		assertFalse(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, query, conn), p, ActionType.SELECT));
		
		conn.close();
	}
	
	@Test
	public void testAccessTypeOwnerSelectEmptyTable() throws DataSourceSecurityManagerException, ClassNotFoundException, SQLException {
		final Permissions p = new Permissions();
		
		final TablePermission tablePermission = new TablePermission(TABLE_NAME, AccessType.OWNER);
		p.addSelectPermission(tablePermission);
		
		final String query = "SELECT * FROM people";
		
		Class.forName("com.mysql.jdbc.Driver");
		
		final Connection conn = DriverManager.getConnection("jdbc:sqlite:db/test_update_security.db");
		
		assertFalse(manager.isAuthorized(new AccessInfo(AUTHORIZED_USER, TABLE_NAME, query, conn), p, ActionType.SELECT));
		
		conn.close();
	}
}