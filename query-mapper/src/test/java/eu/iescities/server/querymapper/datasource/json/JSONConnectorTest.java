/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;

public class JSONConnectorTest {

	@Test
	public void testLoad() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"/test/file.json\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		final DataSourceConnector connector = ConnectorFactory.load(json);
		assertEquals(ConnectorType.json, connector.getType());
		
		final JSONConnector jsonConnector = (JSONConnector) connector;
		assertEquals("/test/file.json", jsonConnector.getURI());
		assertEquals("results", jsonConnector.getRoot());
		assertEquals(60, jsonConnector.getRefresh());
		assertEquals("key1", jsonConnector.getKey());
		assertEquals("UTF-8", jsonConnector.getCharset());
	}
	
	@Test
	public void testSet() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"/test/file.json\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		final DataSourceConnector connector = ConnectorFactory.load(json);
		assertEquals(ConnectorType.json, connector.getType());
		
		final JSONConnector jsonConnector = (JSONConnector) connector;
		jsonConnector.setURI("/test/file2.json");
		jsonConnector.setRoot("values");
		jsonConnector.setRefresh(100);
		jsonConnector.setKey("key2");
		jsonConnector.setCharset("ISO-8859-1");
		
		assertEquals("/test/file2.json", jsonConnector.getURI());
		assertEquals("values", jsonConnector.getRoot());
		assertEquals(100, jsonConnector.getRefresh());
		assertEquals("key2", jsonConnector.getKey());
		assertEquals("ISO-8859-1", jsonConnector.getCharset());
	}
	
	@Test(expected=DataSourceConnectorException.class)
	public void testInvalid() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		ConnectorFactory.load(json);
	}
	
	@Test(expected=IOException.class)
	public void testInvalidReader() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"test.file\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		final JSONConnector connector = (JSONConnector) ConnectorFactory.load(json);
		connector.getReader();
	}
}
