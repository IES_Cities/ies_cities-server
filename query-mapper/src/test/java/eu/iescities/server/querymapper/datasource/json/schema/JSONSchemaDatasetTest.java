/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource.json.schema;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.query.ResultSet;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.FileLoader;

public class JSONSchemaDatasetTest {

	private final String DATASET_ID = "0";
	
	@Before
	public void setUp() throws Exception {
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
	}
	
	@Test
	public void testInsertUserData() throws IOException, DataSourceManagementException, DatabaseCreationException, TranslationException, DataSourceSecurityManagerException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");		
		final JSONSchemaConnector jsonMapping = (JSONSchemaConnector) ConnectorFactory.load(json);

		final JSONSchemaDataSource jsonDataset = new JSONSchemaDataSource(jsonMapping, DATASET_ID);
		
		assertEquals(2, jsonDataset.getTables().getRows().size());
		
		final JSONSchemaDataSource updateJsonDataset = new JSONSchemaDataSource(jsonMapping, DATASET_ID);
		updateJsonDataset.executeSQLUpdate("INSERT INTO table1 (key1, key2, key3) VALUES ('A', 'B', 'C')", "0");
		
		final JSONSchemaDataSource queriableJSONDataset = new JSONSchemaDataSource(jsonMapping, DATASET_ID);
		final ResultSet rs = new ResultSet(queriableJSONDataset.executeSQLSelect("SELECT * FROM table1", "0", DataOrigin.ANY));
		
		assertEquals(1, rs.getRows().size());
	}
	
	@Test
	public void testGetInfo() throws IOException, DataSourceSecurityManagerException, DataSourceManagementException, DatabaseCreationException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");		
		final JSONSchemaConnector jsonMapping = (JSONSchemaConnector) ConnectorFactory.load(json);
		final JSONSchemaDataSource jsonDataset = new JSONSchemaDataSource(jsonMapping, DATASET_ID);
		
		final DataSourceInfo datasetInfo = jsonDataset.getInfo();
		
		assertEquals(ConnectorType.json_schema, datasetInfo.getType());
		
		assertEquals(2, datasetInfo.getTables().size());
	}
}
