/**

 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.querymapper.datasource;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.util.FileLoader;

public class ConnectorFactoryTest {

	@Test(expected=DataSourceConnectorException.class)
	public void testInvalidMapping() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		ConnectorFactory.load(json);
	}
	
	@Test(expected=DataSourceConnectorException.class)
	public void testInvalidJson() throws DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"/test/file.json\"," +
									"\"root\": \"results\"" + 
									"\"key\": \"key1\"" +
								"}";
		
		ConnectorFactory.load(json);
	}
	
	@Test
	public void testValidMapping() throws IOException, DataSourceConnectorException {
		final String json = "{" +
									"\"mapping\": \"json\"," + 
									"\"uri\": \"/test/file.json\"," +
									"\"root\": \"results\"," + 
									"\"key\": \"key1\"" +
								"}";
		
		final DataSourceConnector connector = ConnectorFactory.load(json);
		assertEquals(ConnectorType.json, connector.getType());
	}
	
	@Test(expected=DataSourceConnectorException.class)
	public void testUnknownMapping() throws IOException, DataSourceConnectorException {
		ConnectorFactory.load(FileLoader.loadResource("/dbconfig/invalid.json"));
	}
	
	@Test
	public void testSchemaMapping() throws IOException, DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load(FileLoader.loadResource("/sparql/simple_schema.json"));
		
		assertEquals(ConnectorType.sparql, mapping.getType());
	}
	
	@Test
	public void testDatabaseMapping() throws IOException, DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load(FileLoader.loadResource("/dbconfig/sqlite_conf.json"));
		
		assertEquals(ConnectorType.database, mapping.getType());
	}
	
	@Test
	public void testRedirectMapping() throws IOException, DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load(FileLoader.loadResource("/redirect/redirect1.json"));
		
		assertEquals(ConnectorType.httpapi, mapping.getType());
	}
	
	@Test
	public void testJSONMapping() throws IOException, DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load(FileLoader.loadResource("/json/mapping/jsonmapping1.json"));
		
		assertEquals(ConnectorType.json, mapping.getType());
	}
	
	@Test
	public void testJSONSchemaMapping() throws IOException, DataSourceConnectorException {
		ConnectorFactory.load(FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json"));
	}
	
	@Test
	public void testEmptyMapping() throws DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load("");
		assertEquals(ConnectorType.not_available, mapping.getType());
	}
	
	@Test
	public void testEmptyJSONMapping() throws DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load("{}");
		assertEquals(ConnectorType.not_available, mapping.getType());
	}
	
	@Test
	public void testNullMapping() throws DataSourceConnectorException {
		DataSourceConnector mapping = ConnectorFactory.load(null);
		assertEquals(ConnectorType.not_available, mapping.getType());
	}
}
