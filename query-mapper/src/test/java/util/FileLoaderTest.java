package util;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import org.junit.Test;

import eu.iescities.server.querymapper.util.FileLoader;

public class FileLoaderTest {

	@Test
	public void testLoadResource() throws IOException {
		final String data = FileLoader.loadResource("/json/mapping/jsonmapping1.json");
		final String expected = "{\n\t\"mapping\": \"json\",\n\t\"uri\": \"/json/data/jsondata1.json\",\n\t\"root\": \"results\",\n\t\"key\": \"key1\"\n}\n";
	
		assertEquals(expected, data);
	}

	@Test
	public void testLoadReader() {
		final StringReader reader = new StringReader("This is some test data.");
		final String data = FileLoader.loadReader(reader);
		reader.close();
		
		assertEquals("This is some test data.", data);
	}
}
