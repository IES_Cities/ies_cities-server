package util.status;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.util.status.DatasetStatus;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;

public class DatasetStatusManagerTest {

	@Before
	public void setUp() throws ClassNotFoundException, SQLException, IOException {
		Config.getInstance().createDataDir();
		DatasetStatusManager.getInstance().deleteDB();
	}
	
	@Test
	public void testGetAllEmpty() throws DatasetStatusManagerException {
		final List<DatasetStatus> statusList = DatasetStatusManager.getInstance().getAll();
		assertEquals(0, statusList.size());
	}
	
	@Test
	public void testGetAll() throws DatasetStatusManagerException {
		DatasetStatusManager.getInstance().insertStatus("0", Status.OK, "some message");
		DatasetStatusManager.getInstance().insertStatus("1", Status.ERROR, "some message");
		
		final List<DatasetStatus> statusList = DatasetStatusManager.getInstance().getAll();
		assertEquals(2, statusList.size());
	}
	
	@Test
	public void testGetAllErrors() throws DatasetStatusManagerException {
		DatasetStatusManager.getInstance().insertStatus("0", Status.OK, "some message");
		DatasetStatusManager.getInstance().insertStatus("1", Status.ERROR, "some message");
		
		final List<DatasetStatus> statusList = DatasetStatusManager.getInstance().getAllErrors();
		assertEquals(1, statusList.size());
	}
	
	@Test
	public void testUpdateStatus() throws DatasetStatusManagerException {
		DatasetStatusManager.getInstance().insertStatus("0", Status.OK, "some message");
		DatasetStatusManager.getInstance().insertStatus("1", Status.OK, "some message");
		
		final List<DatasetStatus> allList = DatasetStatusManager.getInstance().getAll();
		assertEquals(2, allList.size());
		
		List<DatasetStatus> errorList = DatasetStatusManager.getInstance().getAllErrors();
		assertEquals(0, errorList.size());
		
		DatasetStatusManager.getInstance().updateStatus("1", Status.ERROR, "new message");
		errorList = DatasetStatusManager.getInstance().getAllErrors();
		assertEquals(1, errorList.size());
		
		final DatasetStatus errorStatus = errorList.get(0);
		assertEquals("1", errorStatus.getId());
		assertEquals("ERROR", errorStatus.getStatus());
		assertEquals("new message", errorStatus.getMessage());
	}
	
	@Test
	public void testUpdateStatusFirstTime() throws DatasetStatusManagerException {
		DatasetStatusManager.getInstance().updateStatus("0", Status.OK, "some message");
		DatasetStatusManager.getInstance().updateStatus("1", Status.OK, "some message");
		
		final List<DatasetStatus> allList = DatasetStatusManager.getInstance().getAll();
		assertEquals(2, allList.size());
	}
	
	@Test
	public void testGetStatus() throws DatasetStatusManagerException {
		DatasetStatusManager.getInstance().insertStatus("0", Status.OK, "some message");
		
		final DatasetStatus datasetStatus = DatasetStatusManager.getInstance().getStatus("0");
		assertEquals("0", datasetStatus.getId());
		assertEquals("OK", datasetStatus.getStatus());
		assertEquals("some message", datasetStatus.getMessage());
	}
}
