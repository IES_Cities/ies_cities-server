PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
select (str(?a) as ?foaf_name) (str(?b) as ?foaf_firstName) (str(?c) as ?foaf_surname) (str(?d) as ?foaf_gender) where {
	?e	a	<http://xmlns.com/foaf/0.1/Person> .
	optional {?e	<http://xmlns.com/foaf/0.1/name>	?a .}
	optional {?e	<http://xmlns.com/foaf/0.1/firstName>	?b .}
	optional {?e	<http://xmlns.com/foaf/0.1/surname>	?c .}
	optional {?e	<http://xmlns.com/foaf/0.1/gender>	?d .}
}