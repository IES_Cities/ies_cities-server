/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import java.util.List;
//import java.util.String;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;



// Messy Generic stuff, need to use the correct generic library for Jersey 2.0 
import javax.ws.rs.core.GenericType;

//import com.sun.jersey.test.framework.JerseyTest;
import org.glassfish.jersey.test.JerseyTest;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.Application;



import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import eu.iescities.server.loggingrest.AppLog;
import eu.iescities.server.loggingrest.AppLogging;
import eu.iescities.server.loggingrest.DatasetLog;

// This uses the Jersey Test Framework instead of the basic Junit
public class JTLogging extends JerseyTest {

    //private HttpServer server;
    //private WebTarget target;

    private Random gen;
  
    // Shouldn't need to set this up explicitly
    /*
    @Override
	protected Application configure() {
        return new ResourceConfig(AppLogging.class);
    }
    */



    /*
    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
        // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());
	// Assume default localhost setup
	String URI="http://192.168.20.119:8080/ies/";
        target = c.target(URI);
	System.out.println("The target is set to " + target.getUri());
	gen = new Random();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }
    */


    @Test
    // Basic test that the API is present
    public void appHereTest()  {

 	//WebResource wr = resource();

	String response = target("log/app/here")
	    .request()
	    .get(String.class);
	assertEquals("yes", response);
    }

    @Test
    // Test to see that an event is logged
    public void appLogTest() throws IOException {

	String response = target("log/app/stamp/45151/hills/32453/message/TestPost")
	    .request()
	    .get(String.class);
	assertEquals("logged", response);
    }


    @Test
    public void appCountLogTest() throws IOException {

	String response = target("log/app/getcount")
	    .request()
	    .get(String.class);
	System.out.println("Total logs: <<<<<<" + response);
    }

    @Test
    public void appQueryTest() throws IOException {

	String response;
	// This is how you need to build up the query
	// Just giving the "?%" test gets encoded wrongly

	List<AppLog> logs;
	logs = target("log/app/getquery")
	    .queryParam("app", "IESCitiesTemplate")
	    .queryParam("type", "Appstart")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	    	
	System.out.println("<<<<<<<<<" + logs.size() + ">>>>>>>>>");
	
	
	//assertEquals("fake", response);

	// This is how you need to build up the query
	//response = target.path("log/app/getcount").request().get(String.class);
	//assertEquals("logged", response);
    }

    @Test
    // Do a log then check to see that it has actually been logged
    public void appFullTest() throws IOException {

	String response;
	List<AppLog> logs;	
	int appid;
	String call;
	// Generate a new random app key
	appid = gen.nextInt(100000);

	// Log with that as an appid
	call = "log/app/stamp/345345/"+ appid + "/14545/Appstart/TestPost";
	System.out.println("App call " + call);
	response = target(call)
	    .request()
	    .get(String.class);


	// Now see if that log is present in the database
	logs = target("log/app/getquery")
	    .queryParam("app", appid)
	    .queryParam("type", "Appstart")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	   
	// Should be one and only one such log in the database
	assertEquals(logs.size(), 1);
    }

    @Test
    public void datastoreHereTest() throws IOException {
	
	String response;
	
	response = target("log/datastore/here")
	    .request()
	    .get(String.class);
	assertEquals("yes", response);
    }
    
    @Test
	public void datastoreAccessTest() throws IOException {
	
	String writeResponse, readResponse;
	
	writeResponse = target("log/datastore/stamp/write/Democratree/bristol")
	    .request()
	    .get(String.class);
	readResponse = target("log/datastore/stamp/read/Democratree/bristol")
	    .request()
	    .get(String.class);
	assertEquals("written", writeResponse);
	assertEquals("read", readResponse);
    }
    
    @Test
	public void datastoreQueryFullTest() throws IOException {
	
	String response, call;
	List <DatasetLog> logs;
	
	// Log a write under a fake unique appid, and check to see that it is there
	int appid = gen.nextInt(100000);
	
	call = "log/datastore/stamp/write/" + appid +  "/bristol";

	response = target(call)
	    .request()
	    .get(String.class);

	assertEquals(response, "written");

	logs = target("log/datastore/getquery")
	    .queryParam("dsid", appid)
	    .queryParam("city", "bristol")
	    .request()
	    .get(new GenericType<List<DatasetLog>>(){});
	
	// Should be one and only one such log in the database
	assertEquals(logs.size(), 1);
    }
}
