Logging REST API 
================

Implementation that includes both the app level and
dataset level APIs.

Swagger is used to document this REST API.


App Event Logging 
-----------------

`AppLogging.java` implements the `/log/app` tree of the REST
interface.

	/event/{timestamp}/{appid}/{session}/{type}/{message}	


This creates a log message from the application.


The `{appid}`, is a short text field that uniquely determines the
application. Current examples of app IDs are "Democratree" or
"Complaints & Suggestions".


The `{timestamp}` is supplied from the client end, to permit timestamping
to happen when the request is initiated in the client.  Its the POSIX
time in seconds from 1 Jan 1970. There is an optional fractional part
that can denote milliseconds. An example would be
"1401375196.143". Note that the Javascript client API generates times
in this format directly, and see below for a simpler call that does not
require the timestamp at all.


`{session}` holds a (random) session id generated when the app is
launched. This is a privacy mechanism that permits complete usage
sessions to be differentiated without directly identifying the user.


`{type}` holds the type of the event, selected from the set in the
following table. Note that this is checked and events with unknown
types are not permitted. An event that does not fit into one of the
other types should be assigned AppCustom with a message field to
describe the message.


`{message}` is a free text field that allows the developer to annotate
the log with any text, suggested content is given where applicable


Event Type | Explanation | Component that Initiates the Log | Suggested Message content 
---        | --- | --- | --- 
AppStart   | App is run or brought to the foreground | App | N/A 
AppStop    | App is exited or moved away from | App | N/A
AppLogin   | User logs in/registers with the App | App | Optional username
AppConsume  | User consumes data | App | More detail on consumption event
AppProsume  | User provides content | App |  More detail on prosumption event 
AppODConsume  | User consumes a source of Open Data | App | More detail on consumption event
AppCollaborate | User performs a collaborative action | App | More detail on collaborative action
AppDataQueryInitiate  | User initiates a query of a Dataset | App | Query string
AppDataQueryComplete  | Dataset query completes | App | N/A
AppDataQueryError     | There is some problem with the data returned |App | Detail of the issue
AppQuestionnaire     | User completes an in app questionnaire | App| N/A

AppLaunch  | App is Launched  | IESPlayer | N/A
PlayerAppSearch | Search for an App | IESPlayer | Detail of search string
PlayerAppDownload | Downloads the App | IESPlayer | N/A

WebCitySelect        | User selects home city | Web Interface| N/A
DevDoc               | Developer downloads documentation | Web Interface| Detail of section requested/page viewed 

AppCustom  | Custom event | App |  Add a full explanation of the event



	/event/{appid}/{session}/{type}/{message}

This is a second form of the basic log call, but one that does not
require a timestamp. It supplies the timestamp direct from the server
side. Note that this will not capture the delay between the client
side and the server, but offers a simpler interface for apps that do
not want to generate timestamps themselves.


	/perf/{timestamp}/{appid}/{session}/{type}/{duration}/{message}

This permits applications to log events that have a duration measured
for them. This permits apps to make performance measurements and log
them centrally to the platfrom.

`{duration}` Time in milliseconds that the event is active for.

Note that using the Javascript library within an Android Cordova
application will generate a performance log every time the app is
exited that holds the total time the app was active for.



Data store event logging
------------------------


`DataStoreLogging.java` allows accesses to the dataset for the datasets with 



	/log/dataset/stampp/{eventtype}/{DSID}/{city}{message}

This logs an event relating to a dataset.

`{eventtype}` is the type of dataset event, taken from the list below.

`{dsid}` The ID of the dataset, as generated and stored when the dataset
is registered.

`{city}` is one of the city codes.

Note that with these calls a timestamp is generated and applied at the server end.



Event | Explanation| Component that Initiates the Log | Suggested Message content 
--- | --- | --- | --- |
DatasetRead | A dataset is read from  | QueryMapper | Some details of the access
DatasetWrite | A dataset is updated | QueryMapper | Some details of the access
DatasetQuery | A dataset is queried | QueryMapper |  The full text of the query 
DatasetInspect | A dataset is inspected, viewed or browsed | Web Interface | Details of the action
DatasetRegister | A dataset is registered | Web interface | Details of the dataset





Rating API
==========

This enables the in-app questionnaires to store responses within the
platform.


	/log/rating/response/{appid}/{question}/{answer}/{message}

This logs a particular question response for an in app questionnaire.

`{appid}` The application ID

`{question}` Question number. Questionaire designers need to keep track
of which question maps to which number.

`{answer}` Integer that stores the response. Either a satisfaction
level, or a multiple choice answer.

`{message}` Free text answer to question. Empty for multiple choice or
satifaction level questions.


	/log/rating/getquery/{appid}
	
This returns all of the responses for a given application.




App Log Querying API
====================


Currently have a simple interface to query and return logs for a
specfic app and event type. The main query call is:
	
	/getquery?app=APPID%type=event%from=Datefrom%to=Dateto%pattern=PATTERN
	
This gets the full set of logs for a specfic 'app', or all apps if
APPID="all". The PATTERN is a regular expression search over the
message test, but typically just a substring match, for example
%pattern=event will return all logs that contain "event" in the
message text.


The event type is optional and is drawn from the list of app events
given above. If not specified you get all of the logs from that app. 

You can narrow the date range using from and to. (Format is
anything that Java's DateFormat can parse. Dates in the format of
"01/10/2014 1:30 PM, GMT" are tested. Anything wrong will throw a
BAD_REQUEST status code.

	/getagg/{period}/{app}/{type}?from=Datefrom%to=Dateto%pattern=PATTERN
    
{period} can be monthly, weekly or daily. This gets aggregate logs,
i.e. numbers of events that took place each time period. As above a
date range can optionally be given, and a pattern to match a substring
of the logged message.
	
	/getagg/total/{app}/{type}?from=Datefrom%to=Dateto

This counts the total number of events of that type over the date
range.

	/getagg/dailysessions/{app}?from=Datefrom%to=Dateto
	
Returns the number of sessions (AppStarts with a unique ID) for the
app over the date range.

	/getcountlogs
	
Gets total count of all logged application events. 

	/getapps

Shows all apps that have generated logs within the system.

	/getlaunches/{appid}

	/getalllaunches

	/getstarts/{appid}
	
Gets total count of application launches/starts.

	/getallstarts{appid}
	
	/getcustom?appid=APP%from=Datefrom%to=Dateto%pattern=PATT
	
Returns the set of AppCustom events that match the option appid and
message pattern within the date range.
	
	/getallcustom


Dataset querying
----------------

The dataset logs can be queried in a similar manner to the application
logs. This call returns the set of logs that match:

	/getquery?dsid=DSID%city=CITY%type=TYPE%from=Datefrom%to=Dateto%pattern=PATTERN

This allows you to search based on dataset ID (DSID), city code (CITY)
event type (TYPE), with a date range and pattern to match the log
message.

The event types are as given above.

If you just need to know how many events match, use the call:

	/getquerytotal?dsid=DSID%city=CITY%type=TYPE%from=Datefrom%to=Dateto%pattern=PATTERN

This returns the number of matching events, and takes the same
parameters as `getquery`.



Testing
=======

The code in the `test` directory forms a reasonably complete test of
the main functionality. This uses cobertura to check coverage.
Currently around >90% for the main codebase.


To check coverage of the tests, use:

	> mvn -Dcobertura-build clean deploy

To generate a more complete report:

	> mvn cobertura:cobertura 
	
This dumps the analysis in target/site/cobertura


To activation confiperf tests, use:

	> mvn test -DargLine="-Dcontiperf.active=true"

