/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.Key;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;
import javax.jdo.annotations.Value;

// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;


// This is to return various stats about Apps

@PersistenceCapable(detachable = "true")
//@XmlRootElement(name="AppStat")
//@XmlType(propOrder = { "timestamp", "count"})
    public class AppStat {
	
	@Persistent
	    private String app;
	//@Persistent
	//  private String date;
	@Persistent
	    private int count;
	
	// Get and set methods
	/*
	public void set (String app,
			 int count
			 //,String date
			 ){
	    this.app = app;
	    this.count = count;
	    //this.date = date;
	}
	*/


	public String getapp() {return app;}
	public int getcount()     {return count;}
	//public String getdate() {return date;}
	
	public void setapp(String app) {this.app = app;}
	public void setcount(int count)         {this.count = count;}
	//public void setdate(String date) {this.date = date;}
}
