/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
// @XmlRootElement(name="DatasetLog")
// @XmlType(propOrder = { "timestamp", "datasetid", "sessionid", "type", "city",
// "message"})
    public class DatasetLog extends Log {

	private int datasetid;
	@Persistent
	private int sessionid;

	@Persistent
	private int city;

	// Get and set methods
	public void set(double timestamp, int datasetid, int sessionid,
			String type, int city, String message) {

		this.timestamp = timestamp;
		this.datasetid = datasetid;
		this.sessionid = sessionid;
		this.eventtype = type;
		this.city = city;
		this.message = message;
		update();
	}

	// Get and Setters

	public int getdatasetid() {
		return datasetid;
	}

	public int getsessionid() {
		return sessionid;
	}

	public int getcity() {
		return city;
	}


	public void setdatasetid(int datasetid) {
		this.datasetid = datasetid;
	}

	public void setsessionid(int sessionid) {
		this.sessionid = sessionid;
	}

	public void setcity(int city) {
		this.city = city;
	}

}
