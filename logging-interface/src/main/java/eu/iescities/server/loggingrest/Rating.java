/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
public class Rating extends Log {

	@Persistent
	private String appid;
	@Persistent
	private int question;
	@Persistent
	private int answer;
    @Persistent
	private int sessionid;


	// Get and set methods
    public void set(double timestamp, int sessionid, String appid, int question, int answer,
			String message) {

		this.timestamp = timestamp;
		this.appid = appid;
		this.question = question;
		this.answer = answer;
		this.message = message;
		this.sessionid = sessionid;
		
		update();
	}

    	public int getquestion() {
		return question;
	}

    	public int getsessionid() {
		return sessionid;
	}

	public int getanswer() {
		return answer;
	}

	public void setappid(String appid) {
		this.appid = appid;
	}

	public void setquestion(int question) {
		this.question = question;
	}

	public void setanswer(int answer) {
		this.answer = answer;
	}
	public void setsessionid(int sessionid) {
		this.sessionid = sessionid;
	}
}
