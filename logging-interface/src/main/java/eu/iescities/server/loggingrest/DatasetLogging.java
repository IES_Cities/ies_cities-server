/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.JDODataStoreException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.MediaType;

// This is for the swagger Api annotations
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
// This is for the swagger Api annotations
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.RetryInterceptor;



@PersistenceAware
    @Path("/log/dataset")
    @Api(value = "/log", description = "Operations about logging")
    public class DatasetLogging extends Logging {
	String last;
	private Set eventtypes;
	int sessionkey = 1223345;

	public DatasetLogging () {

 	    String events[] = {
		"DatasetRead",
		"DatasetWrite",
		"DatasetQuery",
		// Spawned by the web interface
		"DatasetInspect",  // A datasets is inspected/viewed/browsed
		"DatasetRegister" // A dataset is registered
	    };

	    eventtypes = new HashSet(Arrays.asList(events));
	}


		// Delete all logs of a specific app, used for cleanup after testing
	// Need to restrict this to admin user
	public void deleteLogs (int dsid) {
	    final RetryInterceptor intercept = new RetryInterceptor();
	    while (true) {
		final PersistenceManager pm = PMF.getPM();
		final Transaction tx = pm.currentTransaction();
		try {
		    tx.begin();
		    // Delete all logs for this dataset

		    // Need to do this as it does something weird with strings that parse to int
		    Query query = pm.newQuery("SELECT FROM "
			    + DatasetLog.class.getName()
			    + " WHERE datasetid == " + dsid );
		    query.deletePersistentAll();

		    tx.commit();
		    return;
		} catch (final JDODataStoreException e) {
		    intercept.waitToRetry(e);
		} finally {
		    if (tx.isActive()) {
			tx.rollback();
		    }
		}
	    }
	}


	boolean isEventType(String type) {return (eventtypes.contains(type));}

	boolean isDSID(int dsid) {

		return DatasetManagement.verifyDatasetId(dsid);
	    //if (cit < 300) return false;
	    
	    //return true;
	}

	boolean isCity(int cit) {  
	    
	    // Need to do a proper check in the 
	    //  api/council/getcouncil?councilid=cit
	    // May not want to do this for all calls
		
		//eu.iescities.server.accountinterface.CouncilManagement
	    	
	    if (cit == -1)
	    	return true;
	    
		return CouncilManagement.verifyCouncilId(cit);
	    
	    //if (cit < 300) return false;
	    
	    //return true;
	}
    
	private String datasetLogStamp(int dsid, 
				       int session, 
				       String type, 
				       int city,
				       String message) {
	    // Check dataset id ??
	    if (!isDSID(dsid)) {

		throwBadRequest();
	    }

	    
	    if (!isEventType(type)) {

		throwBadRequest();
	    }

	    final RetryInterceptor intercept = new RetryInterceptor();
	    while (true) {
		final PersistenceManager pm = PMF.getPM();
		final Transaction tx = pm.currentTransaction();
		try {
		    tx.begin();

		    final DatasetLog dslog = new DatasetLog();
		    double timestamp = serverTime();
		    dslog.set(timestamp, dsid, session, type, city, message);
		    pm.makePersistent(dslog);
		    tx.commit();
		    //return pm.detachCopy(applog);
		    //return "done";
		    return ("\"logged\"");
		} catch (final JDODataStoreException e) {
		    intercept.waitToRetry(e);
		} finally {
		    if (tx.isActive()) {
			tx.rollback();
		    }
		    pm.close();
		}
	    }
	}

	// This is the main call
	@POST 
	    @Path("/stampp/{event}/{dsid}/{city}/{message}")
	    @ApiOperation(value = "Create timestamped log of dataset event")
	    @Consumes(MediaType.APPLICATION_JSON)
	    public String 
	    poststamp(
		      @ApiParam(value="Type of Event", required=true) @PathParam("event") String event,
		      @ApiParam(value="ID of the dataset", required=true) @PathParam("dsid") int dsid,
		      @ApiParam(value="City code of accessor", required=true) @PathParam("city") int city,
		      @ApiParam(value="Free text field message", required=true) @PathParam("message") String message
		      ){

	    //if (!validate(message)) throwBadRequest();
	    
	    return (datasetLogStamp(dsid, sessionkey, event,  city, message));
	}

	// This is a deprecated call
	@GET 
	    @Path("/stamp/{event}/{dsid}/{city}/{message}")
	//@ApiOperation(value = "Create timestamped log of dataset event") 
	    @Consumes(MediaType.APPLICATION_JSON)
	    public String 
	    post(
		 @ApiParam(value="Type of Event", required=true) @PathParam("event") String event,
		 @ApiParam(value="ID of the dataset", required=true) @PathParam("dsid") int dsid,
		 @ApiParam(value="City code of accessor", required=true) @PathParam("city") int city,
		 @ApiParam(value="Free text field message", required=true) @PathParam("message") String message
		 ){

	    //if (!validate(message)) throwBadRequest();
	    
	    return (datasetLogStamp(dsid, sessionkey, event,  city, message));
	}
	
 

	// Query Interface


	// This is a generic query ported from the AppLog class
	// (Future feature: genericise query across the two object types)
	public List<DatasetLog> 
	    getQueryLogs(int dsid, String eventtype, int city,
			 String from, String to, String pattern){

	    final PersistenceManager pm = PMF.getPM();
	    Integer total;
	    List<DatasetLog> logs;

	    double fromd, tod;
	    String dateconstraint = "", eventconstraint="", idconstraint="", messconstraint="", cityconstraint;


	    idconstraint = "datasetid == " + dsid;

	    cityconstraint = " && city == " + city;

	    messconstraint = "&& message.matches('" + pattern + "') ";

	    if (dsid == -1) {

		idconstraint = "true ";
	    }
	    
	    if (pattern.equals("none")) {

		messconstraint = " && true ";
	    }

	    if (city == -1){

		cityconstraint = " && true ";

	    }
	
	    if (eventtype.equals("all")) {
	    
		eventconstraint=" && true ";
	    }
	    else {
		// Throw out undefined event types
		if (!isEventType(eventtype)) {
	    
		    //System.out.println("Unknown event: " + type);
		    throwBadRequest();
		}
		eventconstraint = " && eventtype == '" + eventtype + "' ";
	    }

	    if (from.equals("none") || to.equals("none")) {
		    
		//Retrieve all
		dateconstraint = " && true ";
		//System.out.println("Default time range set");
	    }
	    else {

		try {
		    fromd = parseDate(from);
		    tod = parseDate(to);
		    dateconstraint = " && timestamp > " 
			+ Double.toString(fromd) 
			+ " && timestamp < " + Double.toString(tod);
		}
		catch (Exception e) {

		    //System.out.println("Bad date range " + from + ":" + to);
		    throwBadRequest();
		}
	    }
	
	    try {
		String querystr = "SELECT FROM "
		    + DatasetLog.class.getName()
		    + " WHERE " 
		    + idconstraint   
		    + cityconstraint
		    + eventconstraint
		    + dateconstraint
		    + messconstraint
		    + "ORDER BY timestamp DESC";

		System.out.println("Query is: " + querystr);
	
		final Query query = pm.newQuery(querystr);

		// query.setRange(offset, offset+limit);
		logs = (List<DatasetLog>) query
		    .execute();
	    }
	    // catch (Exception e) {
	
	    // }
	    finally {
		pm.close();
	    }
	    // Make sure the times are expressed locally
	    for (DatasetLog a: logs) {
		a.update();
	    }
	    return logs;
	}

	@GET
	    @Path("/getquery")
	    @ApiOperation(value = "Gets sets of logs for selected datasets")
	    
	    @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid Event Type supplied") })
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<DatasetLog> 
	    getQuery(
		     @ApiParam(value="ID of the dataset", required=false)  @DefaultValue("-1") @QueryParam("dsid") int dsid,
		     @ApiParam(value="City code of accessor", required=false) @DefaultValue("-1") @QueryParam("city") int city,
		     @ApiParam(value="Type of the Event", required=false) @DefaultValue("all") @QueryParam("type") String type,			     
		     @ApiParam(value="From date", required=false) @DefaultValue("none") @QueryParam("from") String from,
		     @ApiParam(value="To date", required=false) @DefaultValue("none") @QueryParam("to") String to,
		     @ApiParam(value="Message Pattern", required=false) @DefaultValue("none") @QueryParam("pattern") String pattern
		     ) {
	    
	    if (!validate(pattern)) throwBadRequest();

	    return getQueryLogs(dsid, type, city, from, to, pattern);
	}
	
	//Same as above, but we are just interested in the size of the match
	@GET
	    @Path("/getquerytotal")
	    @ApiOperation(value = "Gets sets of logs for selected datasets")
	    
	    @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid Event Type supplied") })
	    @Produces(MediaType.APPLICATION_JSON)
	    public String
	    getQueryTotal(
		     @ApiParam(value="ID of the dataset", required=false)  @DefaultValue("-1") @QueryParam("dsid") int dsid,
		     @ApiParam(value="City code of accessor", required=false) @DefaultValue("-1") @QueryParam("city") int city,
		     @ApiParam(value="Type of the Event", required=false) @DefaultValue("all") @QueryParam("type") String type,			     	
		     @ApiParam(value="From date", required=false) @DefaultValue("none") @QueryParam("from") String from,
		     @ApiParam(value="To date", required=false) @DefaultValue("none") @QueryParam("to") String to,
		     @ApiParam(value="Message Pattern", required=false) @DefaultValue("none") @QueryParam("pattern") String pattern
		     ) {

	    if (!validate(pattern)) throwBadRequest();

	    return ("[" + getQueryLogs(dsid, type, city, from, to, pattern).size() + "]");
	}
    }

