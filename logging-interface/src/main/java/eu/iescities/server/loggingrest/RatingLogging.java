/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import java.util.List;
import java.util.ArrayList;

import javax.jdo.JDODataStoreException;
// Give access to the Persistence Library
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// This is for the swagger "Api" annotations
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import eu.iescities.server.accountinterface.RetryInterceptor;
import eu.iescities.server.accountinterface.datainterface.PMF;


@PersistenceAware
@Path("/log/rating")

@Api(value = "/log", description = "Operations about logging")
public class RatingLogging extends Logging {

	/*
	 * public RatingLogging () {
	 * 
	 * //Nothing to do here as yet }
	 */


    public List<Rating> getRatings(String app) {

	final PersistenceManager pm = PMF.getPM();
	Integer total;
	
	if (!validate(app)) throwBadRequest();


	List<eu.iescities.server.loggingrest.Rating> ratings;
	
	try {
	    // Just test a very simple query
	    final Query query;

	    if (app.equals("all")) { //Return all of the logs
		query = pm.newQuery("SELECT FROM "
				    + Rating.class.getName() );
	    }
	    else {
		query = pm.newQuery("SELECT FROM "
				    + Rating.class.getName() + " WHERE appid == :param1 " 
				    + " ORDER BY timestamp ");
	    }
	    // query.setRange(offset, offset+limit);
	    ratings = (List<eu.iescities.server.loggingrest.Rating>) query
		.execute(app);
	}
		// catch (Exception e) {
	
		// }
	finally {
	    pm.close();
	}
	return ratings;
    }

		// Delete all logs of a specific app, used for cleanup after testing
	// Need to restrict this to admin user
	public void deleteLogs (String appid) {
	    final RetryInterceptor intercept = new RetryInterceptor();
	    while (true) {
		final PersistenceManager pm = PMF.getPM();
		final Transaction tx = pm.currentTransaction();
		try {
		    tx.begin();
		    // Delete all logs for this dataset

		    // Need to do this as it does something weird with strings that parse to int
		    Query query = pm.newQuery("SELECT FROM "
			    + Rating.class.getName()
			    + " WHERE appid == '" + appid + "'");
		    query.deletePersistentAll();

		    tx.commit();
		    return;
		} catch (final JDODataStoreException e) {
		    intercept.waitToRetry(e);
		} finally {
		    if (tx.isActive()) {
			tx.rollback();
		    }
		}
	    }
	}



	public List<String> getApps(){
	    final PersistenceManager pm = PMF.getPM();
	    Integer total;
	    List<String> apps; 
	    
	    try {
		final Query query = 
		    
		    pm.newQuery("SELECT DISTINCT appid FROM " 
				+ Rating.class.getName()); 
		apps = (List<String>) 
		    query.execute();
	    }
	    finally {
		pm.close();
	    }
	    return apps;
	}

	@GET @Path("/getapps")
	    @ApiOperation(value = "Gets a list of all apps that have ever been rated")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String getallapps() {

	    List<String> apps = getApps();
	    
	    return JSONify(apps);
	}



    public  int [][] getStats(String app) {

	List<Rating> ratings = getRatings(app);

	int[][] scores = new int[20][6]; // Never more than 20 questions, 5 responses

	for (Rating r: ratings) {
	    scores [r.getquestion()][r.getanswer()]++;
	}
	return scores;
    }

    // Wrap the stats up as a list
    public List<ResponseStat> 
	getResponses (String app) {

	List<ResponseStat> rs = new ArrayList<ResponseStat>();
	ResponseStat r;
	int [][] stats = getStats(app);
	int q;

	for (q = 1; q<stats.length; q++) {
	    
	    r = new ResponseStat();
	    r.setquestion(q);
	    r.setfreq(stats[q]);

	    rs.add(r);
	}
	return rs;
    }

	// Add a query to pull down all ratings for a specific app
	@GET
	@Path("/getstats/{app}")
	@ApiOperation(value = "Query summarise responses for a specific app")
	@Produces("application/json")
	public List<ResponseStat> 
	    getReponseStats(@ApiParam(value="Application ID", required=true) @PathParam("app") String app) {
	    
	    return getResponses(app);
	}


	// Add a query to pull down all ratings for a specific app
	@GET
	@Path("/getquery/{app}")
	@ApiOperation(value = "Query ratings for a specific app")
	@Produces("application/json")
	public List<Rating> 
	    getQuery(@ApiParam(value="Application ID", required=true) @PathParam("app") String app) {

	    return getRatings(app);
	}

	// These are the types we are aiming for
    private String ratingStamp(String app, int session, 
			       int question, int answer,
			       String message) {

	double timestamp = serverTime();

	// New Persistence approach
	final RetryInterceptor intercept = new RetryInterceptor();
	while (true) {
	    final PersistenceManager pm = PMF.getPM();
	    final Transaction tx = pm.currentTransaction();
	    try {
		tx.begin();

		final Rating rating = new Rating();
		rating.set(timestamp, session, app, question, answer, message);
		pm.makePersistent(rating);
		tx.commit();
		// return pm.detachCopy(applog);
		return "\"rated\"";
	    } catch (final JDODataStoreException e) {
		intercept.waitToRetry(e);
	    } finally {
		if (tx.isActive()) {
		    tx.rollback();
		}
		pm.close();
	    }
	}
    }

    // This is a legacy get call
    @GET
	@Path("/stamp/{appid}/{question}/{answer}/{message}")
	//@ApiOperation(value = "Post ratings entry")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String 
	    stamp(
 		  @ApiParam(value="Application ID", required=true) @PathParam("appid") String app,
		  @ApiParam(value="Question ID", required=true) @PathParam("question") int question,
		  @ApiParam(value="Answer ID", required=true) @PathParam("answer") int answer,
		  @ApiParam(value="Free text field message/answer", required=true) @PathParam("message") String message
		  ) {

	//if (!validate(app) || !validate(message)) throwBadRequest();


	    return (ratingStamp(app, 0, question, answer, message));
	}

	@POST
	@Path("/response/{appid}/{question}/{answer}/{message}")
	@ApiOperation(value = "Post ratings entry", notes = "{appid}: ID of the calling application\n"
			+ "{question}: ID of question\n"
			+ "{answer}: ID of answer\n"
			+ "{message}: Free text field message/answer")
	@Produces(MediaType.APPLICATION_JSON)
	public String stampp(
			     @ApiParam(value="Application ID", required=true) @PathParam("appid") String app,
			     @ApiParam(value="Question ID", required=true) @PathParam("question") int question,
			     @ApiParam(value="Answer ID", required=true) @PathParam("answer") int answer,
			     @ApiParam(value="Free text field message/answer", required=true) @PathParam("message") String message
			     ) {
	    // Perhaps we should do a AppQuestionnaire Event here to make sure it happens?
	    // Use a zero session id for apps that don't track this
	    //if (!validate(app) || !validate(message)) throwBadRequest();

	    return (ratingStamp(app, 0, question, answer, message));
	}

    
	@POST
	@Path("/trackedresponse/{appid}/{sessionid}/{question}/{answer}/{message}")
	@ApiOperation(value = "Post ratings entry", 
		      notes = "{appid}: ID of the calling application\n"
			+ "{sessionid}: Session/device ID of responder\n"
			+ "{question}: ID of question\n"
			+ "{answer}: ID of answer\n"
			+ "{message}: Free text field message/answer")
	@Produces(MediaType.APPLICATION_JSON)
	    public String 
	    stamptracked(
			 @ApiParam(value="Application ID", required=true) @PathParam("appid") String app,
			 @ApiParam(value="Session ID", required=true) @PathParam("sessionid") int sessionid,
			 @ApiParam(value="Question ID", required=true) @PathParam("question") int question,
			 @ApiParam(value="Answer ID", required=true) @PathParam("answer") int answer,
			 @ApiParam(value="Free text field message/answer", required=true) @PathParam("message") String message
			 ) {

	    //if (!validate(app) || !validate(message)) throwBadRequest();
	    // Perhaps we should do a AppQuestionnaire Event here to make sure it happens?

	    return (ratingStamp(app, sessionid, question, answer, message));
	}
}

