/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.Key;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;
import javax.jdo.annotations.Value;

// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
//@XmlRootElement(name="TimeStat")
//@XmlType(propOrder = { "timestamp", "count"})
    public class TimeStat {
	
	@Persistent
	    private String appid;
	@Persistent
	    private double timestamp;
	@Persistent
	    private String date;
	@Persistent
	    private int count;
	
	// Get and set methods
	public void set (String appid, 
			 double timestamp,
			 int count,
			 String date
			 ){
	    
	    this.timestamp = timestamp;
	    this.count = count;
	    this.date = date;
	}
	
	//I think we must define individual getters and setters for everything :)
	public double gettimestamp() {return timestamp;}
	public int getcount()     {return count;}
	public String getdate() {return date;}
	public String getappid() {return appid;}
	
	public void settimestamp(double timestamp) {this.timestamp = timestamp;}
	public void setcount(int count)         {this.count = count;}
	public void setdate(String date) {this.date = date;}
	public void setappid(String appid) {this.appid = appid;}
}
