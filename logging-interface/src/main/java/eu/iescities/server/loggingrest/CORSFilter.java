/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

/**
 * Allow the system to serve xhr level 2 from all cross domain site
 * 
 * @author Deisss (LGPLv3)
 * @version 0.1
 */

// A similar mechanism is (of course) in the rest-interface component
// - need to synch these parts.

public class CORSFilter implements ContainerResponseFilter {
	/**
	 * Add the cross domain data to the output if needed
	 * 
	 * @param creq
	 *            The container request (input)
	 * @param cres
	 *            The container request (output)
	 * @return The output request with cross domain if needed
	 */
	@Override
	public void filter(ContainerRequestContext creq,
			ContainerResponseContext cres) throws IOException {
		cres.getHeaders().putSingle("Access-Control-Allow-Origin", "*");

		cres.getHeaders().putSingle("Access-Control-Allow-Headers",
				"origin, content-type, accept, authorization");
		cres.getHeaders().putSingle("Access-Control-Allow-Credentials", "true");
		cres.getHeaders().putSingle("Access-Control-Allow-Methods",
				"GET, POST, PUT, DELETE, OPTIONS, HEAD");
		cres.getHeaders().putSingle("Access-Control-Max-Age", "1209600");

		// System.out.println("CORSFiltering Operational");
	}
}