/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.ws.rs.core.MediaType;

import java.util.List;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


// This is for the swagger "Api" annotations
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;


import javax.ws.rs.WebApplicationException;


//Error handling
import javax.ws.rs.core.Response;
// This is for the swagger "Api" annotations

// This is just a stub really, to see if this improves the nesting properties of the swagger interface

//@Path("/log")
//@Api(value = "/log", description = "Logs all IESCities events")
public class Logging {
	
    /*
    @GET @Path("/here")
    @ApiOperation(value = "Test presence of the Logging API")
	  @Produces(MediaType.APPLICATION_JSON)
	  public String here(){
	    
	    System.out.println("Requesting presence of Logging API");
	  return "\"yes\"";
	}
    */

    public void throwBadRequest() {

	throw new WebApplicationException(Response
					  .status(Response.Status.BAD_REQUEST).build());			}
    
    public void throwNotFound() {
	
	throw new WebApplicationException(Response
					      .status(Response.Status.NOT_FOUND).build());
    }
    
    // This is the epoc time locally
    public double serverTime() {
	
	double epoc = System.currentTimeMillis() / 1000.00;
	return epoc;
    }

    public double parseDate(String s) throws ParseException {


	if (s.equals("none")) return 0.0;  //This is a special value for an unspecified date
	Date d = DateFormat.getInstance().parse(s);
	long ltime = d.getTime();
	double secs = ltime / 1000;

	return secs;
    }

    public String JSONify(List<String> l) {

	int len = l.size();
	String ret = "[";
	for (int i=0; i<len; i++) {
		
		ret = ret + "\"" + l.get(i) + "\"";
		if(i<len-1) {
		    ret = ret + ","; //Swagger is picky about JSON
		}
	    }
	    ret = ret + "]";
	    return ret;
    }

    //Check that the input conforms to a non-suspicous string
    public boolean validate (String s) {

	String pattern = "([\\w .,+:\\-&])*";
	
	return s.matches(pattern);
    }

    public String clean (String s) {

	String remove = "[']";

	String result = s.replaceAll(remove, "");

	return result;
    }


    public void showvalidate(String s) {

	if (validate(s)) {

	    System.out.println("String " + s + " is OK");
	}
	else {
	    
	    System.out.println("Rejecting string  " + s);
	}
    }

}
