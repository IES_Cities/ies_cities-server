/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;


import java.util.Date;
import java.text.DateFormat;


// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
// @XmlRootElement(name="AppLog")
// @XmlType(propOrder = { "timestamp", "appid", "sessionid", "type", "message"})
public class AppLog extends Log {


    @Persistent
	private String appid;
    @Persistent
	private int sessionid;

    @Persistent
	private int duration; //Duration of event in milliseconds
    
    // This was for the original timestamped log, but is not required
    /*
    public void set(double timestamp, String appid, int sessionid, String type,
			String message) {

		this.timestamp = timestamp;
		this.appid = appid;
		this.sessionid = sessionid;
		this.type = type;
		this.message = message;
		this.duration = 0;
		update();
	}
    */
    // This one for the performance added timestamp
    public void setperf(double timestamp, int duration, String appid, int sessionid, String type,
			String message) {

		this.timestamp = timestamp;
		this.appid = appid;
		this.sessionid = sessionid;
		this.eventtype = type;
		this.message = message;
		this.duration = duration;
		update();
	}
    
    // I think we must define individual getters and setters for everything :)
    
    public String getappid() {
	return appid;
    }
    
    public int getsessionid() {
	return sessionid;
    }
    
    public int getduration() {

	return duration;
    } 
    
    public void setappid(String appid) {
	this.appid = appid;
    }
    
    public void setsessionid(int sessionid) {
	this.sessionid = sessionid;
    }
    
    
    public void setduration (int duration) {
	
	this.duration = duration;
    }
}
