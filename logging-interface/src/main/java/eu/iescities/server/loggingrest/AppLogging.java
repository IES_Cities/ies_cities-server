/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.iescities.server.loggingrest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import javax.jdo.JDODataStoreException;
// Give access to the Persistence Library
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// This is for the swagger "Api" annotations
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.RetryInterceptor;
import eu.iescities.server.accountinterface.datainterface.PMF;
//Error handling

import eu.iescities.server.loggingrest.LogStat;
import eu.iescities.server.loggingrest.AppActivity;

// Need to change names, but this is just a very basic start
// To build *mvn compile*
// To run: *mvn exec:java*
// mvn jetty:run

// Need to handle both app and datastore logging
// Use /log/app for application level
//     /log/dataset for dataset logging

@PersistenceAware
    @Path("/log/app")
    @Api(value = "/log", description = "Operations about logging")
    public class AppLogging extends Logging {

	String last;
	LogStat stats;

	// Logger logger;
	private Set eventtypes; // Stores the standardised app event types

	boolean isEventType(String type) {

	    return (eventtypes.contains(type));
	}

	boolean checkTimeStamp(double value) {

	    // Use double here as
	    // JS library gives stamps in POSIX seconds to millisecond resolution
	    // double value = Double.parseDouble(stamp);
	    // Check the value is sensible
	    double start = 946684800.000; // Year 2000
	    double end = 4102444800.000; // Year 2100

	    return (value > start && value < end);
	}

	// A new object is created everytime the API is called
	// May as well create logger afresh on each call
	public AppLogging() {

	    // These are the reserved standard events, need to sync these with
	    // client libraries
	    String elements[] = {
		// App Events
		"AppLaunch",  //App is Launched from the IESPlayer
		"AppStart",   //App is run or brought to the foreground 
		"AppStop",    //App is exited or moved away from 
		"AppLogin", //User has registered/logged in 
		"AppConsume",  //User consumes data
		"AppProsume",  //User provides content 
		"AppODConsume",  //User consumes a source of Open Data
		"AppCollaborate", //User performs a collaborative action
		"AppDataQueryInitiate",  //User initiates a query of a Dataset
		"AppDataQueryComplete",  //Dataset query completes
		"AppDataQueryError",     //There is some problem with the data returned
		"AppQuestionnaire",     //User completes an in app questionnaire
		"AppQReject",           //User declines to complete the app questionnaire

		// IESPlayer Events
		"PlayerAppSearch", // Search for an App
		"PlayerAppDownload", //Downloads the App

		// Web Interface Events
		"WebCitySelect",        //User selects home city
		"DevDoc",               //Developer downloads documentation

		"AppCustom"  //Custom event, explain in the test field
	    };
	    eventtypes = new HashSet(Arrays.asList(elements));
	    stats = new LogStat();
	}
    
  
	/*
	  @GET
	  @Path("/geteventtypes")
	  @ApiOperation(value = "Gets a list of all the acceptable Event Types")
	  @Produces(MediaType.APPLICATION_JSON)
	  public List<String> getEventTypes() {

	  List<String> l = new ArrayList<String>(eventtypes);
	
	  return l;
	  }
	*/

	// Delete all logs of a specific app, used for cleanup after testing
	// Need to restrict this to admin user
	public void deleteLogs (String app) {
	    final RetryInterceptor intercept = new RetryInterceptor();
	    while (true) {
		final PersistenceManager pm = PMF.getPM();
		final Transaction tx = pm.currentTransaction();
		try {
		    tx.begin();
		    // Delete all logs for this application
		    /*
        		Query query = pm.newQuery("SELECT FROM "
        					  + AppLog.class.getName()
        					  + " WHERE appid == :param1 ");
        		query.deletePersistentAll(app);
		     */
		    // Need to do this as it does something weird with strings that parse to int
		    Query query = pm.newQuery("SELECT FROM "
			    + AppLog.class.getName()
			    + " WHERE appid == '" + app + "'");
		    query.deletePersistentAll();

		    tx.commit();
		    return;
		} catch (final JDODataStoreException e) {
		    intercept.waitToRetry(e);
		} finally {
		    if (tx.isActive()) {
			tx.rollback();
		    }
		}
	    }
	}


	// ***************
	// LOGGING INTERFACE
	// ***************
	// These calls log events in the system
    
    
	// This is the full log with performance duration added
	private String appLogPerfStamp(double timestamp, String app, int session,
				       String type, String message, int duration) {

	    //System.out.println("App event: " + type + "logged");

	    // Check timestamp
	    if (!checkTimeStamp(timestamp)) {

		//System.out.println("Dodgy timestamp: " + timestamp);
		throwBadRequest();
	    }

	    // Check appid ?
	    // Need to pull list of registered apps

	    // Check session?

	    // Throw out undefined event types
	    if (!isEventType(type)) {

		//System.out.println("Unknown event: " + type);
		throwBadRequest();
	    }

	    final RetryInterceptor intercept = new RetryInterceptor();
	    while (true) {
		final PersistenceManager pm = PMF.getPM();
		final Transaction tx = pm.currentTransaction();
		try {
		    tx.begin();

		    final AppLog applog = new AppLog();
		    applog.setperf(timestamp, duration, app, session, type, message);
		    pm.makePersistent(applog);
		    tx.commit();
		    // return pm.detachCopy(applog);
		    return "\"logged\"";
		} catch (final JDODataStoreException e) {
		    intercept.waitToRetry(e);
		} finally {
		    if (tx.isActive()) {
			tx.rollback();
		    }
		    pm.close();
		}
	    }
	    // System.out.println("Logged");
	    // return "logged";
	}

	// These are the types we are aiming for
	private String appLogStamp(double timestamp, String app, int session,
				   String type, String message) {

	    return(appLogPerfStamp(timestamp, app, session, type, message, 0));
	}



	//Want to remove this at some point
	// Try to remove from swagger, to simplify interface and discourage use
	@GET
	    @Path("/stamp/{timestamp}/{appid}/{session}/{type}/{message}")
	//@ApiOperation(value = "Create timestamped log, deprecated, use POST version")
	//	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid timestamp or Event supplied") })
	    @Produces("application/json")
	    public String 
	    stamp(
		  @ApiParam(value="Timestamp POSIX seconds", required=true) @PathParam("timestamp") double timestamp,
		  @ApiParam(value="ID of the calling application", required=true) @PathParam("appid") String app, 
		  @ApiParam(value="Unique session ID", required=true) @PathParam("session") int session,
		  @ApiParam(value="App Event Type", required=true) @PathParam("type") String type, 
		  @ApiParam(value="Free text field message", required=true) @PathParam("message") String message) {

	    //if (!validate(app) || !validate(message)) throwBadRequest();

	    return (appLogStamp(timestamp, app, session, type, message));
	}


	@POST
	    @Path("/perf/{timestamp}/{appid}/{session}/{type}/{duration}/{message}")
	    @ApiOperation(value = "Post timestamped performance log of event")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String 
	    stampperf(
		      @ApiParam(value="Timestamp POSIX seconds", required=true) @PathParam("timestamp") double timestamp,
		      @ApiParam(value="ID of the calling application", required=true) @PathParam("appid") String app, 
		      @ApiParam(value="Unique session ID", required=true) @PathParam("session") int session,
		      @ApiParam(value="App Event Type", required=true) @PathParam("type") String type, 
		      @ApiParam(value="Event duration, milliseconds", required=true) @PathParam("duration") int  duration, 
		      @ApiParam(value="Free text field message", required=true) @PathParam("message") String message) 
	{
	    //if (!validate(app) || !validate(message)) throwBadRequest();
	    return (appLogPerfStamp(timestamp, app, session, type, message, duration));
	}


	@POST
	    @Path("/event/{timestamp}/{appid}/{session}/{type}/{message}")
	    @ApiOperation(value = "Post timestamped log of event")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String 
	    stampp(
		   @ApiParam(value="Timestamp POSIX seconds", required=true) @PathParam("timestamp") double timestamp,
		   @ApiParam(value="ID of the calling application", required=true) @PathParam("appid") String app, 
		   @ApiParam(value="Unique session ID", required=true) @PathParam("session") int session,
		   @ApiParam(value="App Event Type", required=true) @PathParam("type") String type, 
		   @ApiParam(value="Free text field message", required=true) @PathParam("message") String message) 
	{
	    //if (!validate(app) || !validate(message)) throwBadRequest();
	    return (appLogStamp(timestamp, app, session, type, message));
	}

	@POST
	    @Path("/event/{appid}/{session}/{type}/{message}")
	    @ApiOperation(value = "Post log of event, timestamped at server")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String 
	    stampNoTimestamp(
		   @ApiParam(value="ID of the calling application", required=true) @PathParam("appid") String app, 
		   @ApiParam(value="Unique session ID", required=true) @PathParam("session") int session,
		   @ApiParam(value="App Event Type", required=true) @PathParam("type") String type, 
		   @ApiParam(value="Free text field message", required=true) @PathParam("message") String message) 
	{
	    //if (!validate(app) || !validate(message)) throwBadRequest();
	    double timestamp = serverTime();
	    return (appLogStamp(timestamp, app, session, type, message));
	}


	public List<String> getApps(){
	    final PersistenceManager pm = PMF.getPM();
	    Integer total;
	    List<String> apps; 
	    
	    try {
		final Query query = 
		    
		    pm.newQuery("SELECT DISTINCT appid FROM " 
				+ AppLog.class.getName()); 
		apps = (List<String>) 
		    query.execute();
	    }
	    finally {
		pm.close();
	    }
	    return apps;
	}
	public Long getNumEvents(String app, String event) {

	    final PersistenceManager pm = PMF.getPM();
	    Long total;
	
	    try {
		final Query query =
		
		    pm.newQuery("SELECT COUNT (appid) FROM " + AppLog.class.getName()
				+ " WHERE appid == :param1 && eventtype == :param2");
		total = (Long) query.execute(app, event);
	    } finally {
		pm.close();
	    }
	    return total;
	}

	public String 
	    getAllEvents(String event) {
	    
	    String json = "[";
	    
	    final PersistenceManager pm = PMF.getPM();

	    List<String> apps = getApps();

	    for (String app: apps) {
	    
		Long total=getNumEvents(app, event);
		json = json + "{ appid: '" + app + "', " + event + " : " + total + "},";
	    }
	    json = json + "]";
	    
	    return json;
	}


	// This is the main log query function
	// Merge and enhance the grabSLice function from LogStat
	// Need to just validate here and pass on to the LogStat getQueryfunction
	public List<AppLog> 
	    getQueryLogs(String app, String type,
			 String from, String to, String pattern){


	    double fromd=0.0, tod=0.0;

	    // Throw out undefined event types
	    if (!isEventType(type) && !type.equals("all")) {
	    
		    //System.out.println("Unknown event: " + type);
		    throwBadRequest();
	    }
	    
	    try {
		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {

		//System.out.println("Bad date range " + from + ":" + to);
		throwBadRequest();
	    }
	    return stats.getQueryLogs(app, type, fromd, tod, pattern);
	}
	


	// ***************
	// QUERY INTERFACE
	// ***************
	// These calls allow querying of the logged events



	// This does a more complete query of the logging system
	// Now Event Type and App are optional
	@GET
	    @Path("/getquery")
	    @ApiOperation(value = "Gets sets of logs for a specfic app and type of event" 
			  //,notes = "{app}: ID of the applications"
			  )
	    @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid Event Type supplied") })
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<AppLog> 
	    getQuery(
		     @ApiParam(value="ID of the application", required=false) @DefaultValue("all")@QueryParam("app") String app,
		     @ApiParam(value="Type of the Event", required=false) @DefaultValue("all")@QueryParam("type") String type,			     
		     @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
		     @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to,
		     @ApiParam(value="Message Pattern", required=false) @DefaultValue("none")@QueryParam("pattern") String pattern
		     ) 
	{
	    if (!validate(app) || !validate(pattern)) throwBadRequest();
	    return getQueryLogs(app, type, from, to, pattern);
	}	    

	// Interface to the getQueryNumber - just returns the count of matched events
	@GET
	    @Path("/getnumber")
	    @ApiOperation(value = "Gets number of matching logs for a specfic app and type of event" 
			  //,notes = "{app}: ID of the applications"
			  )
	    @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid Event Type supplied") })
	    @Produces(MediaType.APPLICATION_JSON)
	    public String
	    getQueryNumber(
		     @ApiParam(value="ID of the application", required=false) @DefaultValue("all")@QueryParam("app") String app,
		     @ApiParam(value="Type of the Event", required=false) @DefaultValue("all")@QueryParam("type") String type,			     
		     @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
		     @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to,
		     @ApiParam(value="Message Pattern", required=false) @DefaultValue("none")@QueryParam("pattern") String pattern
		     ) 
	{
	    if (!validate(app) || !validate(pattern)) throwBadRequest();

	    Double fromd=0.0, tod=0.0;

	    try {

		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {
		
		throwBadRequest();  //Dates don't make sense
	    }

	    return "[" + stats.getQueryNumber(app, type, fromd, tod, pattern) + "]" ;
	}	    
    


	// Here we combine the monthly/weekly/daily into one function, and add pattern matching for the message
	@GET
	    @Path("/getagg/{period}/{app}/{type}")
	    @ApiOperation(value = "Gets aggregate logs on a daily/weekly/monthly basis")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<TimeStat> 
	    getAggMonthly(
			 @ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			 @ApiParam(value="Period: monthly/weekly/daily", required=true) @PathParam("period") String period,
			 @ApiParam(value="Type of the Event", required=true) @PathParam("type") String type,		
			 @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			 @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to,
			 @ApiParam(value="Message Pattern", required=false) @DefaultValue("none")@QueryParam("pattern") String pattern){
	
	    List<TimeStat> ts = new ArrayList<TimeStat>();
	

	    if (!validate(app) || !validate(pattern)) throwBadRequest();

	    //Throw out undefined event types
	    if (!isEventType(type)) {
	    
		//System.out.println("Unknown event: " + type);
		throwBadRequest();
	    }
	    Double fromd=0.0, tod=0.0;
	    
	    try {

		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {
		
		throwBadRequest();  //Dates don't make sense
	    }
	    
	    if (period.equals("monthly")) {
		ts = stats.monthlyLogs(app, type, fromd, tod, pattern);
	    }
	    else if (period.equals("weekly")) {
		ts = stats.weeklyLogs(app, type, fromd, tod, pattern);
	    }
	    else if (period.equals("daily")) {
		ts = stats.dailyLogs(app, type, fromd, tod, pattern);
	    }
	    else {
		throwBadRequest();  //Period is not defined
	    }
	    return ts; 
	}

	@GET
	    @Path("/getalltotals/{type}")
	    @ApiOperation(value = "Gets aggregate total number of matched events for each of the apps")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<AppStat> 
	    getAggMonthly(
			 @ApiParam(value="Type of the Event", required=true) @PathParam("type") String type,		
			 @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			 @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to,
			 @ApiParam(value="Message Pattern", required=false) @DefaultValue("none")@QueryParam("pattern") String pattern){
	
	    List<AppStat> ts = new ArrayList<AppStat>();
	
	    if (!validate(pattern)) throwBadRequest();

	    //Throw out undefined event types
	    if (!isEventType(type)) {
	    
		//System.out.println("Unknown event: " + type);
		throwBadRequest();
	    }
	    Double fromd=0.0, tod=0.0;
	    
	    try {

		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {
		
		throwBadRequest();  //Dates don't make sense
	    }
	    
	    ts = stats.totalAllApps(type, fromd, tod, pattern);
	   
	    return ts; 
	}

    
	@GET 
	    @Path("/getagg/total/{app}/{type}")
	    @ApiOperation(value = "Gets total number of events between optional date range")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<TimeStat> 
	    getAggTotal(
			@ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			@ApiParam(value="Type of the Event", required=true) @PathParam("type") String type,		
			@ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			@ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to){
	
	    List<TimeStat> ts;

	    if (!validate(app)) throwBadRequest();	

	    //Throw out undefined event types
	    if (!isEventType(type)) {
	    
		//System.out.println("Unknown event: " + type);
		throwBadRequest();
	    }

	    Double fromd=0.0, tod=0.0;
	    
	    try {

		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {

		throwBadRequest();  //Dates don't make sense
	    }
	    
	    ts = stats.eventTotals(app, type, fromd, tod);
	    
	    return ts; 
	}


	


	// New here
	/*
	@GET 
	    @Path("/getaggall/weekly/{type}")
	    @ApiOperation(value = "Gets aggregate logs on a weekly basis")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<TimeStat> 
	    getAggWeekly(
			 @ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			 @ApiParam(value="Type of the Event", required=true) @PathParam("type") String type,		
			 @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			 @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to){
	
	    List<TimeStat> ts;
	
	    //Throw out undefined event types
	    if (!isEventType(type)) {
	    
		//System.out.println("Unknown event: " + type);
		throwBadRequest();
	    }
	    ts = stats.allWeeklyLogs(app, type);
	    return ts; 
	}
	*/




	// GRP.APP.HC.1 
	@GET @Path("/getagg/dailysessions/{app}")
	    @ApiOperation(value = "Gets aggregate applications sessions on a daily basis")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<TimeStat> 
	    getAggDaily(
			@ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			@ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			@ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to){
    
	    List<TimeStat> ts;

	    if (!validate(app)) throwBadRequest();	
	        
	    ts = stats.allDailySessions(app);
	    return ts; 
	}

	// Need to do the same for unique devices as a proxy for users
	// Give an app name and a event type (default AppStart) and a date range, and return number of unique devices
	@GET @Path("/getagg/devices/{app}")
	    @ApiOperation(value = "Gets total number of distinct devices over date range")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public String
	    getAggDevices(
			@ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			@ApiParam(value="Event Type", required=false) @DefaultValue("AppStart")@QueryParam("type") String type,
			@ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			@ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to){
    
	    if (!validate(app)) throwBadRequest();	

	    //Throw out undefined event types
	    if (!isEventType(type)) {
	    
		throwBadRequest();
	    }

	    Double fromd=0.0, tod=0.0;

	    try {
		
		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {
		
		throwBadRequest();  //Dates don't make sense
	    }
	    int total;

	    total = stats.uniqueDevices(app, type, fromd, tod);

	    return "[" + total + "]"; 
	}


	// Produces a summary of active users for the app
	@GET @Path("/getactivity/{app}")
	    @ApiOperation(value = "Produces a summary of active users for the app over date range, total is aboslute figure, others are quoted as percentages")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public AppActivity
	    getActivity(
			@ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
			@ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			@ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to){
    
	    if (!validate(app)) throwBadRequest();	

	    Double fromd=0.0, tod=0.0;

	    try {
		
		fromd = parseDate(from);
		tod = parseDate(to);
	    }
	    catch (Exception e) {
		
		throwBadRequest();  //Dates don't make sense
	    }
	    
	    AppActivity activity = new AppActivity();
	    int total;
	    	    
	    activity.setappid(app);
	    activity.settotal(stats.uniqueDevices(app, "", fromd, tod));
	    activity.setstart(stats.uniqueDevices(app, "AppStart", fromd, tod));
	    activity.setconsume(stats.uniqueDevices(app, "AppConsume", fromd, tod));
	    activity.setcollaborate(stats.uniqueDevices(app, "AppCollaborate", fromd, tod));
	    activity.setprosume(stats.uniqueDevices(app, "AppProsume", fromd, tod));
				
	    return activity;
	}

	

	// This will query the logging system
	@GET
	    @Path("/getcountlogs")
	    @ApiOperation(value = "Gets total count of all logged application events")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String getCount() {
	    final PersistenceManager pm = PMF.getPM();
	    Integer total;
	    List<eu.iescities.server.loggingrest.AppLog> logs;

	    try {
		final Query query =

		    pm.newQuery("SELECT FROM " + AppLog.class.getName());
		logs = (List<eu.iescities.server.loggingrest.AppLog>) query
		    .execute();
		total = logs.size();
	    } finally {
		pm.close();
	    }
	    return "[" + total + "]";
	}
	

	// Need a version that considers over a particular time period
	// i.e. to list apps that logged last month, for example
	// Can add optional period to this call
	@GET @Path("/getapps")
	    @ApiOperation(value = "Gets a list of all apps that have ever logged")
	    @Produces(MediaType.APPLICATION_JSON)
	    public String getallapps() {

	    List<String> apps = getApps();
	    
	    return JSONify(apps);
	}


	@GET
	    @Path("/getlaunches/{appid}")
	    @ApiOperation(value = "Gets total count of application launches from IESPlayer")
	    @Produces("application/json")
	    public String 
	    getLaunches(
			@ApiParam(value="ID of the application", required=true) @PathParam("appid") String app) {

	    if (!validate(app)) throwBadRequest();	
	 

	    final PersistenceManager pm = PMF.getPM();
	    Long total=getNumEvents(app, "AppLaunch");
	    
	    return "[" + total + "]";
	}



	@GET
	    @Path("/getalllaunches")
	    @ApiOperation(value = "Gets total count of all app launches from IESPlayer")
	    @Produces("application/json")
	    public String
	    getAllLaunches() {
	    
	    return getAllEvents("AppLaunch");
		
		}


	//  **KPI.ICP.4 "Icon Launches"
	@GET
	    @Path("/getallstarts")
	    @ApiOperation(value = "Gets number of application launches from the icon on the device (AppStarts)")
	    @Produces("application/json")
	    public String
	    getAllStarts() {
	    
	    return getAllEvents("AppStart");
	}
	
	@GET
	    @Path("/getstarts/{appid}")
	    @ApiOperation(value = "Gets total count of application starts")
	    @Produces("application/json")
	    public String getStarts(@ApiParam(value="ID of the application", required=true) @PathParam("appid") String app) {

	    if (!validate(app)) throwBadRequest();	
	 

	    final PersistenceManager pm = PMF.getPM();
	    Long total=getNumEvents(app, "AppStart");
	    
	    return "[" + total + "]";
	}


	// This should make it possible to handle a lot of the AppCustom queries that specific apps require
	@GET @Path ("/getcustom") 
	    @ApiOperation(value = "Gets all AppCustom logs for an app whose text match the pattern")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<AppLog> 
	    getCustomLogs(

			  @ApiParam(value="ID of the application", required=false) @DefaultValue("all") @QueryParam("app") String app,
			  @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			  @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to, 
			  @ApiParam(value="Message Pattern", required=false) @DefaultValue("none")@QueryParam("pattern") String pattern

			  ) {
	    
	    if (!validate(app) || !validate(pattern)) throwBadRequest();	
	 
	    return (getQueryLogs(app, "AppCustom", from, to, pattern));
	}

	// This can be used to track the different things that the AppCustom event is used for 
	@GET @Path ("/getallcustom") 
	    @ApiOperation(value = "Gets all AppCustom logs from all applications")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	    public List<AppLog> 
	    getAllCustomLogs(
			  ) {
	    return (getQueryLogs("all", "AppCustom", "none", "none", "none"));
	}


	/*

	//  **KPI.ICP.4
	@GET @Path ("/geticonlaunches") 
	    @ApiOperation(value = "Gets number of application launches from the icon on the device (AppStarts)")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	//public List<AppLog> 
	    getIconLaunches(

			    @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			    @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to ) {
	
	    //Yet to be implemented
	    return ();
	}

	//  **KPI.ICP.4
	@GET @Path ("/getplayerlaunches") 
	    @ApiOperation(value = "Gets number of application launches from the player (AppLaunches)")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	//public List<AppLog> 
	    getPlayerLaunches(

			      @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			      @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to ) {
	
	    //Yet to be implemented
	    return ();
	}


	//  **GRP.APP.5
	// (move to the ratings class??)
	// Do we want these to be monthly???
	@GET @Path ("/getinapp/{app}") 
	    @ApiOperation(value = "Gets number of in-app questionnaires completed per week for this app")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	public List<TimeStats>
	    getInApps(
		  
		      @ApiParam(value="ID of the application", required=true) @PathParam("app") String app,
		      @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
		      @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to ) {
	
	    List<TimeStat> ts;
	    
	    ts = stats.allDailyLogs(app, "AppQuestionnaire")

	    return (ts);
	}

    
	// Interest in the datasets
	// KPI.D.5
	// (move to Dataset section???)

	// Might want to do this from an application or dataset perspective.

	@GET @Path ("/getdatasetusage") 
	    @ApiOperation(value = "Gets number of accesses to the dataset over the period")
	    @ApiResponses(value = {
		    @ApiResponse(code = 400, message = "Invalid Event Type supplied")        	
		})
	    @Produces(MediaType.APPLICATION_JSON)
	//public List<AppLog> 
	    getDatasetUsage(

			    @ApiParam(value="From date", required=false) @DefaultValue("none")@QueryParam("from") String from,
			    @ApiParam(value="To date", required=false) @DefaultValue("none")@QueryParam("to") String to ) {
	
	    //Yet to be implemented
	    return ();
	}


	// Also need an interface to query the prosume-type (AppCollaborate, AppConsume, etc) events for the apps
	
	*/



    }

