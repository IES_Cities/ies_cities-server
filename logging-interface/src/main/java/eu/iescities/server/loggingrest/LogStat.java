/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.iescities.server.loggingrest;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import eu.iescities.server.accountinterface.datainterface.PMF;

import eu.iescities.server.loggingrest.Logging;
import eu.iescities.server.loggingrest.AppLog;
import eu.iescities.server.loggingrest.AppStat;


public class LogStat {

    double DAY = 60.0 * 60 * 24;
    double WEEK = DAY * 7;
    double MONTH = WEEK * 4; 

    // Create a histogram of logged events in the range, binned every period (period is typically day or week)
    public List<TimeStat> createHistogram(List<AppLog> logs, 
					  double start, double end, 
					  double period) {
	
	int index = 0, count=0;
	double current;
	List<TimeStat> list = new ArrayList<TimeStat>();

	// If we don't have enough logs to build a meaningful histogram
	// return an empty list
	if (logs.size()<2) {

	    return list;
	}

	if (end == 0.0) {

	    //Need to create the first and last timestamp since these have not been given
	    start = logs.get(0).gettimestamp();
	    end = logs.get(logs.size()-1).gettimestamp();
	}

	String appid = logs.get(0).getappid(); //Use the appid from the first log

	//System.out.println(start + "->" + end);

	for (current=start; current<end; current = current + period) {

	    count = 0;
	    while ( (index < logs.size())
		   && (logs.get(index).gettimestamp() < current + period)) {

		index = index + 1;
		count = count + 1;
	    }
	    
	    // Add TimeStat to list
	    TimeStat next = new TimeStat();

	    //System.out.println("[" + current + ", " + count + "]");

	    //next.settimestamp(current);
	    //next.setcount(count);

	    //Create human-readable datestamp
	    long datestamp = (long) (current * 1000);

	    //System.out.println("Dates: " + current + "->"  +  datestamp);
	    
	    Date thisDate = new Date(datestamp);
	    String form;


	    form = DateFormat.getDateInstance().format(thisDate);

	    next.set(appid, current, count, form);

	    list.add(next);
	}
	return list;
    } 

    // Get a slice of the logs for a specific event and time slot
    // Set to=0.0 to get everything
    // Not required now
    /*
    public List<AppLog> grabSlice(String appid, String eventtype, double from, double to) {

	final PersistenceManager pm = PMF.getPM();
	List<AppLog> logs;
	String querystring;

	querystring = "SELECT FROM " 
			    + AppLog.class.getName() 
	    + " WHERE appid == :param1 && type == :param2";
	
	if (to>0.0) {

	    querystring = querystring + " && timestamp > " 
		+ Double.toString(from) + " && timestamp < " + Double.toString(to);
	}

	try {
	    // Need to order to make sure the histogramming works
	    final Query query = pm.newQuery(querystring + " order by timestamp");
	    logs = (List<AppLog>) query.execute(appid, eventtype);
	}
	finally {
	    pm.close();
	}
	return logs;
    }
    */
    


    //Assume inputs are checked at this point
	public List<AppLog> 
	    getQueryLogs(String app, String type,
			 double from, double to, String pattern){

	    final PersistenceManager pm = PMF.getPM();
	    Integer total;
	    List<AppLog> logs = new ArrayList<AppLog>();

	    String dateconstraint = "", eventconstraint="", appconstraint="", messconstraint="";


	    appconstraint = "appid == '" + app + "'";

	    messconstraint = "&& message.matches('.*" + pattern + ".*') ";

	    if (app.equals("all")) {

		appconstraint = " true ";
	    }
	    
	    if (pattern.equals("none")) {

		messconstraint = " && true ";
	    }

	
	    if (type.equals("all")) {
	    
		eventconstraint=" && true ";
	    }
	    else {
		eventconstraint = " && eventtype == '" + type + "' ";
	    }
	    
	    if (from==0.0 || to==0.0) {
		    
		//Retrieve all
		dateconstraint = "";
		//System.out.println("Default time range set");
	    }
	    else {
		
		dateconstraint = " && timestamp > " 
		    + Double.toString(from) 
		    + " && timestamp < " + Double.toString(to);
	    }
		
	    try {
		String qry = "SELECT FROM "
		    + AppLog.class.getName()
		    + " WHERE " 
		    + appconstraint   
		    + eventconstraint
		    + dateconstraint
		    + messconstraint 
		    + " order by timestamp"; //Needed to make sure that the histogramming works

		System.out.println("AppQuery is:" + qry);
		final Query query = pm.newQuery(qry);

		// query.setRange(offset, offset+limit);
		logs = (List<eu.iescities.server.loggingrest.AppLog>) query
		    .execute();
	    }
	     catch (Exception e) {
		 
		 System.out.println("Dodgy query");
		 //return logs;
	     }
	    finally {
		pm.close();
	    }
	    // Make sure the times are expressed locally
	    for (AppLog a: logs) {
		a.update();
	    }
	    return logs;
	}

    // Return number of matches for the query
    // (There is potential for optimisation by performing counting on the DB side)
    public int
	getQueryNumber(String app, String type,
		       double from, double to, String pattern){
	
	List<AppLog> logs;

	logs = getQueryLogs(app, type, from, to, pattern);
	
	return logs.size();
    }

    // Want to deprecate this as we don't want to work on a session basis
    public List<AppLog> grabSessions(String appid, double from, double to) {

	final PersistenceManager pm = PMF.getPM();
	List<AppLog> logs;
	String querystring, postgresquery;
	

	//select distinct on ("SESSIONID") * from "APPLOG" group by "APPLOG_ID"


	//querystring = "SELECT distinct on (sessionid) FROM " 
	querystring = "SELECT FROM " 
			    + AppLog.class.getName() 
	    + " WHERE appid == :param1 ";
	
	if (to>0.0) {

	    querystring = querystring + " && timestamp > " 
		+ Double.toString(from) + " && timestamp < " + Double.toString(to);
	}

	//querystring = querystring + " GROUP BY applog_id";
	querystring = querystring + " ORDER BY sessionid";

	// TODO: this should probably be 'select distinct on ("SESSIONID") * from "APPLOG" ORDER BY "SESSIONID","TIMESTAMP"'
	//       but the order should then be fixed by sorting by the timestamps afterwards!
	// TODO: make it a generic JDOQL query!
	postgresquery = 
	    "SELECT distinct on (\"SESSIONID\") * FROM \"APPLOG\" WHERE \"APPID\" = ? GROUP BY \"APPLOG_ID\"";

	try {
	    //final Query query = pm.newQuery(querystring);
	    final Query query = pm.newQuery(Query.SQL, postgresquery);
	    logs = (List<AppLog>) query.execute(appid);
	}
	finally {
	    pm.close();
	}
	return logs;
    }

    // Create aggregate statistics from the query
    public List<TimeStat> aggregateLogs (String appid, String eventtype, 
					 double from, double to, double period, String pattern) {

	List<TimeStat> results;

	List<AppLog> logs;

	//System.out.println("Grabbing slice: " );

	logs = getQueryLogs(appid, eventtype, from, to, pattern);
	
	//System.out.println("Slice: " + logs.size());

	results  = createHistogram(logs, from, to, period);

	//System.out.println("Hist: " + results.size());

	return results;
    }
    
    // Runs the previous over all apps in the table
    public List<List<TimeStat>> aggregateLogsAllApps (String eventtype, double from, double to, double period) {

	// Get a List of all logged apps
	AppLogging al = new AppLogging();
	List<String> apps = al.getApps();
	
	List<List<TimeStat>> l = new ArrayList<List<TimeStat>>();

	for (String app : apps) {

	    l.add (aggregateLogs(app, eventtype, from, to, period, "none"));
	}
	return l;
    }
    

    public List<AppStat> totalAllApps (String eventtype, double from, double to, String pattern) {

	List<AppStat> appstats = new ArrayList<AppStat>();
	AppLogging al = new AppLogging();
	List<String> apps = al.getApps();
	
	for (String app : apps) {

	    AppStat as = new AppStat();
	    as.setapp(app);
	    as.setcount(getQueryNumber(app, eventtype, from, to, pattern));
	    
	    appstats.add(as);
	}
	return appstats;
    }


    public List<TimeStat> aggregateSessions (String appid, 
					  double from, double to, double period) {

	List<TimeStat> results;

	List<AppLog> logs;

	//System.out.println("Grabbing slice: " );

	logs = grabSessions(appid, from, to);
	
	//System.out.println("Slice: " + logs.size());

	results  = createHistogram(logs, from, to, period);

	//System.out.println("Hist: " + results.size());

	return results;
    }

    public List<List<TimeStat>> allappLogs (String eventtype, double from, double to, double period) {

	return aggregateLogsAllApps(eventtype, from, to, period);
    }

    public List<TimeStat> monthlyLogs (String appid, String eventtype, double from, double to, String pattern) {

	return aggregateLogs(appid, eventtype, from, to, MONTH, pattern);
    }


    public List<TimeStat> weeklyLogs (String appid, String eventtype, double from, double to, String pattern) {

	return aggregateLogs(appid, eventtype, from, to, WEEK, pattern);
    }

    public List<TimeStat> dailyLogs (String appid, String eventtype, double from, double to, String pattern) {
    	return aggregateLogs(appid, eventtype, from, to, DAY, pattern);
    }

    // Here the period should be large enough to provide a total number of events between 'from' and 'to'
    public List<TimeStat> eventTotals (String appid, String eventtype, double from, double to) {
    	return aggregateLogs(appid, eventtype, from, to, to - from, "none");
    }

    public List<TimeStat> allDailySessions (String appid) {

    	return aggregateSessions(appid, 0.0, 0.0, DAY);
    }

    public List<TimeStat> allWeeklyLogs (String appid, String eventtype) {

	return aggregateLogs(appid, eventtype, 0.0, 0.0, WEEK, "none");
    }

    public List<TimeStat> allDailyLogs (String appid, String eventtype) {
    	return aggregateLogs(appid, eventtype, 0.0, 0.0, DAY, "none");
    }

    // Considers only negative session ids (i.e. device/installation ids)
    // Give null type for all event types
    public int uniqueDevices(String app, String type, double from, double to) {

	final PersistenceManager pm = PMF.getPM();
	
	String postgresquery, typeconstraint, timeconstraint="";
	
	if (type.equals("")) {
	    
	    typeconstraint="";
	}
	else {

	    typeconstraint = "&& eventtype == '" + type  + "'";
	}	

	if (to>0.0) {
	    
	    timeconstraint = " && timestamp > "
		+ Double.toString(from) + " && timestamp < " + Double.toString(to);
	}

	postgresquery = "SELECT UNIQUE COUNT(DISTINCT sessionid) FROM "
		+ AppLog.class.getName() + " WHERE appid == :param "
		+ typeconstraint
		+ timeconstraint
		+ " && sessionid < 0";

	try {
	    System.out.println("Query is " + postgresquery);
	    final Query query = pm.newQuery(postgresquery);
	    return ((Long) query.execute(app)).intValue();
	}
	finally {
	    pm.close();
	}
    }
}
