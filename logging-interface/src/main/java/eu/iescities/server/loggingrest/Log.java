/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;

import java.util.Date;
import java.text.DateFormat;


// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
// @XmlRootElement(name="AppLog")
// @XmlType(propOrder = { "timestamp", "appid", "sessionid", "type", "message"})
    @Inheritance(strategy=InheritanceStrategy.SUBCLASS_TABLE) //This avoids creating an actual LOG database table
public class Log {

    @Persistent
	protected double timestamp;
    @Persistent
	protected String eventtype;
    @Persistent
	@Column(length = 65535, allowsNull = "false") //Increase the maximum strong length
	protected String message;
    @Persistent
	protected Date date;


    public void update() {
	
	long datestamp = (long) (timestamp * 1000);
	
	date = new Date(datestamp);
	
    }
    
    
    // I think we must define individual getters and setters for everything :)
    public double gettimestamp() {
	return timestamp;
    }
    
    public String geteventtype() {
	return eventtype;
    }
    
    public String getmessage() {
	return message;
    }
    
    public Date getdate() {

	update();
	return date;
    }
    
    public void settimestamp(double timestamp) {
	this.timestamp = timestamp;
	update();
    }
    
    public void seteventtype(String type) {
	this.eventtype = eventtype;
    }
    
    public void setmessage(String message) {
	this.message = message;
    }
    public void setdate(Date date) {
	
	this.date = date;
    }
}
