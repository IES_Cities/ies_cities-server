/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;


import java.util.Date;
import java.text.DateFormat;


// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
// @XmlRootElement(name="AppLog")
// @XmlType(propOrder = { "timestamp", "appid", "sessionid", "type", "message"})
public class AppActivity {

    @Persistent
	private String appid;
    @Persistent
	private int total;  //Total number of unique users

    @Persistent
	private int start;// Check - should be 100%

    @Persistent
	private int consume;// Users consuming data
    @Persistent
	private int collaborate;// Users collaborating

    @Persistent
	private int prosume; //Users producing data

    // This one for the performance added timestamp
    public void set(String appid, int total, int start, int consume, int collaborate, int prosume) {

		this.total = total;
		this.appid = appid;
		this.start=start;
		this.consume=consume;
		this.collaborate=collaborate;
		this.prosume=prosume;
	}
    
    public String getappid() {
	return appid;
    }
    

    public int gettotal() {
	return total;
    }

    public int getstart() {
	return start;
    }

    public int getconsume() {
	return consume;
    }
    public int getcollaborate() {
	return collaborate;
    }
    public int getprosume() {
	return prosume;
    }

    
    public void setappid(String appid) {
	this.appid = appid;
    }
    
    public void settotal(int total) {
	this.total = total;
    }

    public void setstart(int start) {

	this.start = start;
    }
    public void setconsume(int consume) {
	this.consume = consume;
    }
    public void setcollaborate(int collaborate) {
	this.collaborate = collaborate;
    }
    public void setprosume(int prosume) {
	this.prosume = prosume;
    }
}
