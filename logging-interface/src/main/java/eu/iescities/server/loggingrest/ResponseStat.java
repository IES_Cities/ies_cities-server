/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

// These handle the XML conversion (JSON?)
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@PersistenceCapable(detachable = "true")
public class ResponseStat {

    @Persistent
	private int question;
    @Persistent
	private int[] freq;


    	public int getquestion() {
		return question;
	}

    	public int[] getfreq() {
	    return freq;
	}

	public void setquestion(int question) {
		this.question = question;
	}

	public void setfreq(int[] freq) {
		this.freq = freq;
	}
}
