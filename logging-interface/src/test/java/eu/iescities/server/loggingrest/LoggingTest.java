/* 
   Copyright 2014 Toshiba Research Europe Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
   http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package eu.iescities.server.loggingrest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

//import javax.ws.rs.core.Application;
// Messy Generic stuff, need to use the correct generic library for Jersey 2.0 
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.loggingrest.application.TestApplication;

public class LoggingTest extends JerseyTest {


    // User data paste from AppRestInterfaceTest
    private static final String ZGZ_ADMIN_USER = "zgz_user";
    private static final String ZGZ_ADMIN_PASS = "zgz_pass";

    private static final String BRISTOL_ADMIN_USER = "bristol_user";
    private static final String BRISTOL_ADMIN_PASS = "bristol_pass";

    private static final String ROVERETO_ADMIN_USER = "rovereto_user";
    private static final String ROVERETO_ADMIN_PASS = "rovereto_pass";

    private static final String MAJADAHONDA_ADMIN_USER = "majadahonda_user";
    private static final String MAJADAHONDA_ADMIN_PASS = "majadahonda_pass";
    
    private static final String NORMAL_USER = "some_user";
    private static final String NORMAL_USER_PASS = "some_pass";
    
    private static List<Integer> registeredApps = new ArrayList<Integer>();


    private static Council zgzCouncil = new Council();
    private static GeoScope zgzGeoScope;

    private static User normalUser;
    
    private static Dataset dataset;

    private static int registerApp(String userSession, int id, Council council, String[] tags, String packageName) {

	eu.iescities.server.accountinterface.datainterface.Application tempApp 
	    = new eu.iescities.server.accountinterface.datainterface.Application();
        tempApp.setName("Test app " + id);
        tempApp.setDescription("Description of the test app " + id);
        tempApp.setUrl("http://iescities.eu/" + id);
        tempApp.setImage("http://server.iescities.eu/testApp" + id + ".jpg");
        tempApp.setRelatedCouncilId(council.getCouncilId());
        tempApp.setVersion("0.1");
        tempApp.setTermsOfService("Terms of Service for app " + id);
        tempApp.setPermissions("Permisions of test app " + id);
        tempApp.setPackageName(packageName + "." + id);
        tempApp.setTags(new HashSet<String>(Arrays.asList(tags)));
        tempApp.setGeographicalScope(council.getGeographicalScope());
        tempApp.setLang(Language.EN);
        
        return ApplicationManagement.registerApp(userSession, tempApp).getAppId();
    }
    
    // End paste 
    private static int BRIS_ID;
    private static int ZGZ_ID;
    private static int MAJA_ID;
    private static int ROV_ID;
    private static int DSID;

    private static Random gen;

    @Rule
    public ContiPerfRule i = new ContiPerfRule();
    
    @Override
	protected javax.ws.rs.core.Application configure() {
	return new TestApplication();
    }

    public void assertBadRequest(int code) {

	assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), code);
    }

    public void assertOK(int code) {

	assertEquals(Response.Status.OK.getStatusCode(), code);
    }

    
    public String now() {
	
	long epoc = System.currentTimeMillis() / 1000L;
	
	return epoc + ".000";
    }
    
    public static String exappid() {
	
	// Integer appids
	// return "1234";
	return "HillsHaveEyes";
    }
    
    public int goodCity1 () {
	
	
	return ZGZ_ID;
    }

    public int goodCity2 () {
	
	
	return BRIS_ID;  
    }

    public int badCity () {


	return 0;  //This should not be a city
    }

    public int goodDSID() {

	return DSID; //This is a Bristol dataset
    }

    public int badDSID() {

	return 0; //This is not currently a dataset
    }

    // Taken from AppRestInterfaceTest
    public static void addCityCouncilData () throws Exception {
	
	TestUtils.cleanUpDatastore();
        
    	TestUtils.addAdminUser().getUserId();
        String adminSession = TestUtils.loginAdminUser();
        
        zgzGeoScope = GeoScopeManagement.createScope(
                adminSession, "Zaragoza Region",
                new GeoPoint(41.6894079, -0.8427317999999999),
                new GeoPoint(41.6139746, -0.9472301000000001));
        
        zgzCouncil.setLang(Language.ES);
        zgzCouncil.setName("Zaragoza");
        zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
        zgzCouncil.setGeographicalScope(zgzGeoScope);
        zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

        final User zgzAdminUser = new User();
        zgzAdminUser.setUsername(ZGZ_ADMIN_USER);
        zgzAdminUser.setPassword(ZGZ_ADMIN_PASS);
        zgzAdminUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
        int zgzAdminUserId = UserManagement.registerUser(zgzAdminUser).getUserId();
	
	ZGZ_ID = zgzCouncil.getCouncilId();

        
        CouncilManagement.addCouncilAdmin(adminSession, zgzCouncil.getCouncilId(), zgzAdminUserId);

        final GeoScope bristolGeoScope = GeoScopeManagement.createScope(
                adminSession, "Bristol Region",
                new GeoPoint(51.54443269999999, -2.4509025),
                new GeoPoint(51.3925452, -2.7305165));
        
        Council bristolCouncil = new Council();
        bristolCouncil.setLang(Language.EN);
        bristolCouncil.setName("Bristol");
        bristolCouncil.setDescription("Bristol Council");
        bristolCouncil.setGeographicalScope(bristolGeoScope);
        bristolCouncil = CouncilManagement.createCouncil(adminSession, bristolCouncil);

        final User bristolAdminUser = new User();
        bristolAdminUser.setUsername(BRISTOL_ADMIN_USER);
        bristolAdminUser.setPassword(BRISTOL_ADMIN_PASS);
        bristolAdminUser.setPreferredCouncilId(bristolCouncil.getCouncilId());
        int bristolAdminId = UserManagement.registerUser(bristolAdminUser).getUserId();

	BRIS_ID = bristolCouncil.getCouncilId();
        
        CouncilManagement.addCouncilAdmin(adminSession, bristolCouncil.getCouncilId(), bristolAdminId);

        final GeoScope roveretoGeoScope = GeoScopeManagement.createScope(
                adminSession, "Rovereto Region",
                new GeoPoint(45.91260949999999, 11.0738002),
                new GeoPoint(45.85202, 10.9991401));
        Council roveretoCouncil = new Council();

        roveretoCouncil.setLang(Language.IT);
        roveretoCouncil.setName("Rovereto");
        roveretoCouncil.setDescription("Rovereto Council");
        roveretoCouncil.setGeographicalScope(roveretoGeoScope);
        roveretoCouncil = CouncilManagement.createCouncil(adminSession, roveretoCouncil);

        final User roveretoAdminUser = new User();
        roveretoAdminUser.setUsername(ROVERETO_ADMIN_USER);
        roveretoAdminUser.setPassword(ROVERETO_ADMIN_PASS);
        roveretoAdminUser.setPreferredCouncilId(roveretoCouncil.getCouncilId());
        int roveretoAdminId = UserManagement.registerUser(roveretoAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, roveretoCouncil.getCouncilId(), roveretoAdminId);

        final GeoScope majadahondaGeoScope = GeoScopeManagement.createScope(
                adminSession, "Majadahonda Region",
                new GeoPoint(40.4837203, -3.8448064),
                new GeoPoint(40.4450864, -3.8880567));
        
        Council majadahondaCouncil = new Council();
        majadahondaCouncil.setLang(Language.ES);
        majadahondaCouncil.setName("Majadahonda");
        majadahondaCouncil.setDescription("Majadahonda Council");
        majadahondaCouncil.setGeographicalScope(majadahondaGeoScope);
        majadahondaCouncil = CouncilManagement.createCouncil(adminSession, majadahondaCouncil);

        final User majadahondaAdminUser = new User();
        majadahondaAdminUser.setUsername(MAJADAHONDA_ADMIN_USER);
        majadahondaAdminUser.setPassword(MAJADAHONDA_ADMIN_PASS);
        majadahondaAdminUser.setPreferredCouncilId(majadahondaCouncil.getCouncilId());
        int majadahondaAdminId = UserManagement.registerUser(majadahondaAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, majadahondaCouncil.getCouncilId(), majadahondaAdminId);
        
        normalUser = new User();
        normalUser.setUsername(NORMAL_USER);
        normalUser.setPassword(NORMAL_USER_PASS);
        normalUser = UserManagement.registerUser(normalUser);
        
        UserManagement.logout(adminSession);
		
        /////////// Apps //////////////        
		
        String userSession = UserManagement.login(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS);
        registeredApps.add(registerApp(userSession, 1, zgzCouncil, new String[]{"Zaragoza", "311"}, "com.tuenti.messenger"));
        UserManagement.logout(userSession);
		
        userSession = UserManagement.login(BRISTOL_ADMIN_USER, BRISTOL_ADMIN_PASS);		
        registeredApps.add(registerApp(userSession, 2, bristolCouncil, new String[]{"Bristol", "311"}, "com.tuenti.messenger"));
        UserManagement.logout(userSession);
								
        userSession = UserManagement.login(ROVERETO_ADMIN_USER, ROVERETO_ADMIN_PASS);
        registeredApps.add(registerApp(userSession, 3, roveretoCouncil, new String[]{"Rovereto", "Cycling"}, "com.facebook.katana"));
        UserManagement.logout(userSession);

        userSession = UserManagement.login(MAJADAHONDA_ADMIN_USER, MAJADAHONDA_ADMIN_PASS);
        String description = "The service allows users to obtain information of the " +
                "leisure and cultural offer of the city, both private and public, " +
                "filtered by their preferences. The user gets all the information of " +
                "the selected event (date, time, price, geolocation, availability, " + 
                "type of target user, etc.). In addition, the service sends alerts to " + 
                "registered users which fit their profile.";
	
	//Fully qualify since we have name clash
        eu.iescities.server.accountinterface.datainterface.Application tempApp = 
	    new eu.iescities.server.accountinterface.datainterface.Application();
        tempApp.setName("Majadahonda Leisure & Events");
        tempApp.setDescription(description);
        tempApp.setUrl("http://iescities.eu");
        tempApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-majadahonda-leisure-logo-3357157233-1_avatar.png");
        tempApp.setRelatedCouncilId(majadahondaCouncil.getCouncilId());
        tempApp.setVersion("0.1");
        tempApp.setTermsOfService("Terms of Service for app 1");
        tempApp.setPermissions("Permisions of service 1");
        tempApp.setPackageName("com.example.viveMajadahonda");
        tempApp.setTags(new HashSet<String>(Arrays.asList(new String[]{"Majadahonda", "leisure", "events", "citizens", "shops", "shopping"})));
        tempApp.setGeographicalScope(majadahondaCouncil.getGeographicalScope());
        tempApp.setLang(Language.EN);
        
        eu.iescities.server.accountinterface.datainterface.Application app 
	    = ApplicationManagement.registerApp(userSession, tempApp);
        
        registeredApps.add(app.getAppId());
        
        dataset = new Dataset();
        dataset.setLang(Language.EN);
	dataset.setName("test_dataset");
	dataset.setJsonMapping("");
	dataset = DatasetManagement.registerDataset(userSession, dataset);
       	
	DSID = dataset.getDatasetId();
	
	ApplicationManagement.addDataset(userSession, app.getAppId(), dataset.getDatasetId());
       
        UserManagement.logout(userSession);
     }


    
    @BeforeClass
	public static void setUpTests() throws Exception {

	gen = new Random();

	addCityCouncilData();

    }
    
    @After
	public void debogus() throws Exception {

	cleanBogusLogs();
    }
    
    @AfterClass
	public static void cleaner () throws Exception {
	
	cleanAppLogs(exappid());
	
	cleanDSLogs(DSID);
    }
   

    // ==================
    // App Tests
    // ==================
    



    // Helper for tests
    public String addAppLogs(int number, String type) {
    
    
    String response = "none";
    // Put a handful of logs in the database
    for (int i=0; i<number; i++) {
        
        response = target("log/app/stamp/" + now() + "/" + exappid()
                  + "/32453/" + type + "/FirstPost")
        .request().get(String.class);
    }
    return response;
    }
    
    public void addAppLogs2(int number, String type) {
	
	
	// Put a handful of logs in the database
	for (int i=0; i<number; i++) {
            new AppLogging().stamp(System.currentTimeMillis() / 1000.0, exappid(), 32453, type, "FirstPost");
	}
    }

    // Tidy up after generating logs
    public static void cleanAppLogs(String app) {

	AppLogging l = new AppLogging();

	l.deleteLogs(app);
    }

    public static void cleanRatings(String app) {

	RatingLogging l = new RatingLogging();

	l.deleteLogs(app);
    }

    public static void cleanDSLogs(int dsid) {

	DatasetLogging l = new DatasetLogging();

	l.deleteLogs(dsid);
    }

    public boolean isBogusAppName(String app) {

	try  {  
	    int i = Integer.parseInt(app);  
	}  
	catch(NumberFormatException nfe)  {  
	    
	    return false;
	}  
	return true;
    }

    public void cleanBogusLogs() {
	
	// Get Apps
	List<String> apps;
	apps = target("log/app/getapps")
	    .request()
	    .get(new GenericType<List<String>>(){});

	for (String app : apps) {

	    // If app name is just a number, remove
	    if (isBogusAppName(app)) {
		System.out.println("Removing logs for app:" + app);
		cleanAppLogs(app);
	    }
	}
    }

    
    public Response addAppLogPost(String type) {
	
	Response response = target("log/app/event/" + now() + "/" + exappid()
			      + "/32453/" + type + "/FirstPost")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
	return response;
    }


    // Create a random log for sometime in dec 2013 and post it
    public void genRandomLog(String app, String type) {

	//Generate the log in 2013
	//int start =   1356998400; 
	int start  =  1385856000;
	int finish =  1388448000;

	// Use a random deviceid for all
	int deviceid = - gen.nextInt(10000);
	
	int timestamp = gen.nextInt(finish-start) + start;

	Response response = target("log/app/event/" + timestamp + ".000/" + app
				   + "/" + deviceid + "/" + type + "/FirstPost")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
    }


    public void genRandomLogs(int number, String app, String type) {

	for (int i=0; i<number; i++ ) {

	    genRandomLog(app, type);
	}
    } 


    public Response addAppLogPostServerTime(String type) {

	Response response = target("log/app/event/" + exappid()
			      + "/32453/" + type + "/FirstPost")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
	return response;
    }

    @Test
    @PerfTest(invocations = 10000, threads = 1000)
	public void appPost() {
	
	int r;
	r = addAppLogPost("AppStart").getStatus();
	
	assertOK(r);
	
	r = addAppLogPostServerTime("AppStart").getStatus();

	assertOK(r);

	r = addAppLogPost("AppBogus").getStatus();
	
	assertBadRequest(r);
    }


    @Test 
    public void addLog() {


    String response = addAppLogs(1, "AppLaunch");

    assertEquals("\"logged\"", response);
    }

    @Test 
	public void addLog2() {


	addAppLogs2(1, "AppLaunch");
    }


    
    @Test
    // Test to see that an event is logged
	public void appLogFullTest() throws IOException {

		String response;
		int tries = 1;

		// Put a handful of logs in the database
		addAppLogs(tries, "AppLaunch");

		// Test getting the total count of logs
		response = target("log/app/getcountlogs").request().get(String.class);

		//App Query Test
		List<AppLog> logs;
		logs = target("log/app/getquery") 
		    .queryParam("app", exappid())
		    .queryParam("type", "AppLaunch")
		    .request()
		    .get(new GenericType<List<AppLog>>(){});
	
		assertTrue(logs.size()>tries);

		String num = target("log/app/getnumber") 
		    .queryParam("app", exappid())
		    .queryParam("type", "AppLaunch")
		    .request()
		    .get(String.class);
		//System.out.println("Number is: " + num);
		
		assertEquals(num, "[" + logs.size() + "]");

		// Get Apps
		List<String> apps;
		apps = target("log/app/getapps")
		    .request()
		    .get(new GenericType<List<String>>(){});
		
		assertTrue(apps.size() > 0); //Must be at least one app in DB
	}

	@Test
	// Test to see that checks that should fail, do
	public void appLogFails() throws IOException {


	    int r;

	    r = target(
		       "log/app/stamp/" + now() + "/" + exappid()
		       + "/32453/AppRandom/TestPost").request()
		.get(Response.class).getStatus();
	    
	    assertBadRequest(r);

	    r = target(
		       "log/app/stamp/" + "3423" + "/" + exappid()
		       + "/32453/AppRandom/TestPost").request()
		.get(Response.class).getStatus();

 	    assertBadRequest(r);
	
	    // Try a timestamp that is too big to test that branch
	    r = target("log/app/stamp/" + "5102444800" + "/" + exappid() 
			      + "/32453/AppRandom/TestPost")
		.request().get(Response.class).getStatus();
	    assertBadRequest(r);
	}


    @Test
	public void appQueryTestDateRange() throws IOException {
	
	List<AppLog> logs;
	logs = target("log/app/getquery")
	    .queryParam("app", exappid())
	    .queryParam("type", "AppLaunch")
	    .queryParam("from", "01/1/2014 1:30 PM, GMT")
	    .queryParam("to", "01/10/2014 1:30 PM, GMT")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});

	// Bad date range
	int res = target("log/app/getquery")
	    .queryParam("app", exappid())
	    .queryParam("type", "AppStart")
	    .queryParam("from", "jumble of date-looking numbers that doesn't parse")
	    .queryParam("to", "01/10/2014 1:30 PM, GMT")
	    .request()
	    .get(Response.class)
	    .getStatus();

	assertBadRequest(res);

	// Picky coverage test...
	logs = target("log/app/getquery")
	    .queryParam("app", exappid())
	    .queryParam("type", "AppStart")
	    .queryParam("from", "01/1/2014 1:30 PM, GMT")
	    .queryParam("to", "none")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
    }

    @Test
	public void allcustom() throws IOException {

	List<AppLog> logs;
	
	logs = target("log/app/getallcustom")
	    .request(MediaType.APPLICATION_JSON)
	    .get(new GenericType<List<AppLog>>(){});

	//System.out.println("Number of custom logs:  " + logs.size()); 
    }

    @Test
	public void allStartsLaunches() throws IOException {

	String json;
	
	json = target("log/app/getalllaunches")
	    .request(MediaType.APPLICATION_JSON)
	    .get(String.class);
	
	//System.out.println("All Launches:  " + json); 

	json = target("log/app/getallstarts")
	    .request(MediaType.APPLICATION_JSON)
	    .get(String.class);

	//System.out.println("All Starts:  " + json); 
    }

    @Test
	public void appBadQueryTest() throws IOException {
	
	int r = target("log/app/getquery")
	    .queryParam("app", exappid())
	    .queryParam("type", "AppBogus")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class).getStatus();

	assertBadRequest(r);
    }

    public void appAggTest(String app, String event, String period, int size) {
	
	long start, timed;

	start = System.currentTimeMillis();
	
	List<TimeStat> l = target("log/app/getagg/" + period + "/" + app + "/" + event)
	    .request(MediaType.APPLICATION_JSON)
	    .get(new GenericType<List<TimeStat>>(){});
	
	assertTrue(l.size() > 0); 
	
	timed = System.currentTimeMillis() - start;
	
	//Post performance stats to the database
	Response response = target("log/app/perf/" + now() + "/" + "TestPerf"
				   + "/32453/" + "AppCustom/" + timed + "/AggQuery" + size + period)
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
	assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    public void appAggQuery(int size) {
	
	long start, timed;
	int s;
	
	start = System.currentTimeMillis();
	
	genRandomLogs(size, "AggTest", "AppLaunch");
	genRandomLogs(size, "AggTest", "AppStart");
	genRandomLogs(size/2, "AggTest", "AppConsume");
	genRandomLogs(size/4, "AggTest", "AppCollaborate");
	genRandomLogs(size/8, "AggTest", "AppProsume");

	genRandomLogs(size, "AggTest2", "AppLaunch");
	



	timed = System.currentTimeMillis() - start;
	
	//Post performance stats to the database, use TestPerf to persist these logs
	s = target("log/app/perf/" + now() + "/" + "TestPerf"
				   + "/32453/" + "AppCustom/" + timed + "/"+ size + "Logs")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class).getStatus();
	
	assertOK(s);

	
	
	String launches = target("log/app/getlaunches/AggTest")
	    .request()
	    .get(String.class);
	
	//System.out.println("Agg launches = " + launches);
	
	appAggTest("AggTest", "AppLaunch", "monthly", size);
	appAggTest("AggTest", "AppLaunch", "weekly", size);
	appAggTest("AggTest", "AppLaunch", "daily", size);


	//Look at all AppCustom logs
	List<TimeStat> l = target("log/app/getallcustom")
	    .request(MediaType.APPLICATION_JSON)
	    .get(new GenericType<List<TimeStat>>(){});

	// There must be some
	assertTrue(l.size() > 1); 

	// Remove test - we don't assert result
	String total = target("log/app/getagg/devices/AggTest")
	    .request(MediaType.APPLICATION_JSON)
	    .get(String.class);

	//System.out.println("Unique device test: " + total);

	// Bad event type
	s = target("log/app/getagg/daily/" + exappid() + "/AppBogus")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class).getStatus();

	assertBadRequest(s);

	// Bad period
	s = target("log/app/getagg/hourly/" + exappid() + "/AppLaunch")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class).getStatus();

	assertBadRequest(s);


	// Bad date
	s = target("log/app/getagg/daily/" + exappid() + "/AppLaunch")
	    .queryParam("from", "jumble of date-looking numbers that doesn't parse")
	    .queryParam("to", "01/10/2014 1:30 PM, GMT")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class).getStatus();

	assertBadRequest(s);


	// Bad period
	s = target("log/app/getagg/hourly/" + exappid() + "/AppLaunch")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class).getStatus();

	assertBadRequest(s);

	//Check session aggregates not working 
	
	//l = target("log/app/getagg/dailysessions/" + exappid())
	//  .request(MediaType.APPLICATION_JSON)
	//  .get(new GenericType<List<TimeStat>>(){});
	
	//System.out.println("Daily Sessions " + l.size()); 

	//assertTrue(l.size() > -1); 
	

	// Bad event type
	s = target("log/app/getagg/weekly/" + exappid() + "/AppBogus")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class)
	    .getStatus();

	assertBadRequest(s);

	// Check histogram is not generated if there are no logs
	l = target("log/app/getagg/weekly/CandyCrush/AppLaunch")
	    .request(MediaType.APPLICATION_JSON)
	    .get(new GenericType<List<TimeStat>>(){});

 	// Check dodgy event type
	s = target("log/app/getalltotals/AppBogus")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class)
	    .getStatus();

	assertBadRequest(s);

	// Check bad date
	s = target("log/app/getalltotals/AppLaunch")
	    .queryParam("from", "jumble of date-looking numbers that doesn't parse")
	    .queryParam("to", "01/10/2014 1:30 PM, GMT")
	    .request(MediaType.APPLICATION_JSON)
	    .get(Response.class)
	    .getStatus();

	assertBadRequest(s);



	//Might want to benchmark this
	List<AppStat> as = target("log/app/getalltotals/AppLaunch")
	    .request(MediaType.APPLICATION_JSON)
	    .get(new GenericType<List<AppStat>>(){});
	

	//System.out.println("AppStats of size " + as.size());
	

	// Test the AppAcivity call
	//AppActivity ac = target("log/app/getactivity/AggTest")
	//    .request(MediaType.APPLICATION_JSON)
	//    .get(AppActivity.class);

	/*
	System.out.println("AppActivity for "
			   + ac.getappid() + ", " 
			   + ac.gettotal() + ", " 
			   + ac.getstart() + ", " 
			   + ac.getconsume() + ", " 
			   + ac.getcollaborate() + ", " 
			   + ac.getprosume());
	*/

	//for (AppStat st : as) {
	
	//    System.out.println("App: " + st.getapp() + ", count: " + st.getcount());
	//}

	//assertEquals(l.size(), 0); 
	cleanAppLogs("AggTest");
	cleanAppLogs("AggTest2");
    }

    @Test
	public void scaleAppAggQuery() {

	appAggQuery(5);
	//appAggQuery(50);
 	//appAggQuery(500);
	//appAggQuery(5000);
    }


    @Test
    // note: since appAggQuery(int) always operates on the same 2 appids (and
    //       tries to cleanup at the end), we cannot have concurrent invocations!
    @PerfTest(invocations = 10, threads = 1)
	public void perfscaleAggQuery() {

	appAggQuery(500);
    }


    @Test
    // Do a log then check to see that it has actually been logged
    public void appFullTest() throws IOException {

	String response;
	List<AppLog> logs;	
	int appid;
	String call;
	String launches;
	// Generate a new random app key
	appid = gen.nextInt(100000);

	// Log with that as an appid
	call = "log/app/stamp/" + now() + "/"+ appid + "/14545/AppLaunch/TestPost";
	response = target(call).request().get(String.class);

	assertEquals("\"logged\"", response);

	call = "log/app/stamp/" + now() + "/"+ appid + "/14545/AppLaunch/SecondPost";
	response = target(call).request().get(String.class);

	assertEquals("\"logged\"", response);

	// Now log a start as well
	call = "log/app/stamp/" + now() + "/"+ appid + "/14545/AppStart/TestPost";
	response = target(call).request().get(String.class);

	assertEquals("\"logged\"", response);

	// Now see if that log is present in the database
	logs = target("log/app/getquery")
	    .queryParam("app", appid)
	    .queryParam("type", "AppLaunch")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	   
	// Should be two logs in the database
	assertEquals(2, logs.size());


	// Now check pattern query
	logs = target("log/app/getquery")
	    .queryParam("app", appid)
	    .queryParam("type", "AppLaunch")
	    .queryParam("pattern", "Post")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});

	// Should be two logs in the database
	assertEquals(2, logs.size());

	// Now check pattern query
	logs = target("log/app/getquery")
	    .queryParam("app", appid)
	    .queryParam("type", "AppLaunch")
	    .queryParam("pattern", "SecondPost")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});

	// Should be one log in the database
	assertEquals(1, logs.size());


	// Now the count direct from the count call
	launches = target("log/app/getlaunches/" + appid)
	    //.queryParam("app", appid)
	    .request()
	    .get(String.class);
	
	// Should still be two
	assertEquals("[2]", launches);

	launches = target("log/app/getstarts/" + appid)
	    //.queryParam("app", appid)
	    .request()
	    .get(String.class);
	
	// Should still be only one
	assertEquals("[1]", launches);

	// Now do a multiple event types query
	logs = target("log/app/getquery")
	    .queryParam("app", appid)

	    //.queryParam("type", "AppLaunch")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	
	// Should hit both events
	assertEquals(3, logs.size());

	//Now get rid of these logs
	cleanAppLogs(String.valueOf(appid));

	// Should be nothing left now
	logs = target("log/app/getquery")
	    .queryParam("app", appid)
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	
	assertEquals(0, logs.size());
    }

    // Crude injection attack
    @Test 
	public void inject() throws IOException {
	
	int fail;

	fail = target("log/app/getquery")
	    .queryParam("app", "nothing' || true || appid == 'nothng")
	    .request().get(Response.class).getStatus();
	
	assertBadRequest(fail);
    }

    // Odd log
    @Test 
	public void oddlog() throws IOException {
	
	int res;

	res = target("log/app/getquery")
	    .queryParam("this:. should be OK ,-now")
	    .request().get(Response.class).getStatus();
	
	assertOK(res);
    }


    // This is the failed log for the MyKW app
    // Need to find out what is wrong with it
    @Test 
	public void abiLog() throws IOException {
	
	int res;
	
	// Original call
 	res = target("log/app/event/1417432204/MyKWTest//AppConsume/FeedSelect HTTP/1.0")
	
	// Remove the final slash - gives 404
 	//res = target("log/app/event/1417432204/MyKWTest//AppConsume/FeedSelect HTTP1.0")

	// Add the dev id - works fine
 	//res = target("log/app/event/1417432204/MyKWTest/-5573/AppConsume/FeedSelect HTTP1.0")
	    
	    
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class).getStatus();
	
	assertEquals(res, 404);
	cleanAppLogs("MyKWTest");

    }


    @Test 
	public void checknotimestamp() throws IOException {
	
	int res;
	
	
 	res = target("log/app/event/MyKWTest/2345234/AppConsume/FeedSelect")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class).getStatus();
	
	assertOK(res);
	cleanAppLogs("MyKWTest");

    }

    @Test
    // Some apps have spaces and ampersands in the name - need to make sure these work
	public void appAmpersandTest() throws IOException {


	String call = "log/app/stamp/" + now() 
	    + "/Complaints & Suggestions/14545/AppLaunch/TestPost";
	
	String response = target(call).request().get(String.class);
	
	assertEquals("\"logged\"", response);

	// Now see if that log is present in the database
	List<AppLog> logs = target("log/app/getquery")
	    .queryParam("app", "Complaints & Suggestions")
	    .queryParam("type", "AppLaunch")
	    .request()
	    .get(new GenericType<List<AppLog>>(){});
	
	// Should be at least one in the database
	assertTrue(logs.size()>0);
	cleanAppLogs("Complaints & Suggestions");
    }

    

    
    // ==================
    // Dataset Tests
    // ==================

    @Test
	public void datasetFailAccess() throws IOException {
	
	int fail;
	
	
	// Wrong city
	/* Remove for now as we don't check the city ID, add later if we check IDs
	failResponse = target("log/dataset/stamp/DatasetRead/" 
			      + goodDSID() + "/" + badCity() + "/none")
	    .request().get(Response.class);

	assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), 
		     failResponse.getStatus());
	*/

	// Wrong type
	fail = target("log/dataset/stamp/DatasetBogus/" 
			      + goodDSID() +  "/" + goodCity1()+ "/none")
	    .request().get(Response.class).getStatus();
	
	assertBadRequest(fail);
    }
    
    
    @Test
	public void datasetAccessTests() throws IOException {

	String writeResponse, readResponse, failResponse;

	// Test the GET calls
	writeResponse = target("log/dataset/stamp/DatasetWrite/" 
			       + goodDSID() + "/" + goodCity1()+ "/none")
	    .request().get(String.class);

	readResponse = target("log/dataset/stamp/DatasetRead/"
			      + goodDSID() + "/" + goodCity1()+ "/none")
	    .request().get(String.class);
	assertEquals("\"logged\"", writeResponse);
	assertEquals("\"logged\"", readResponse);

	// Test the POST calls
	Response r = target("log/dataset/stampp/DatasetWrite/" 
			    + goodDSID() + "/" + goodCity1()+ "/none")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);

	assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());

	r = target("log/dataset/stampp/DatasetRead/"
		   + goodDSID() + "/" + goodCity1()+ "/none")
	    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
	
	assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
    }

	@Test
	    public void datasetQueryFullTest() throws IOException {

	    // Log a write under a fake unique DSID, and check to see that it is there
	    //int dsid = gen.nextInt(100000);
	    
	    // Have to use the real one for now
	    int dsid = DSID;

	    String response, call;	 
	    List<DatasetLog> logs;
   
	    call = "log/dataset/stamp/DatasetWrite/" + dsid +  "/"  + goodCity1() + "/none";

	    response = target(call).request().get(String.class);
	    
	    assertEquals("\"logged\"", response);
		
	    // Query for specific city and dataset
	    logs = target("log/dataset/getquery").queryParam("dsid", dsid)
		.queryParam("city", goodCity1()).request()
		.get(new GenericType<List<DatasetLog>>() {
		    });
	    
	    // Should be at least one
	    assertTrue(logs.size()>1);

	    // Query all logs for a specific city
	    logs = target("log/dataset/getquery")
		.queryParam("city", goodCity1()).request()
		.get(new GenericType<List<DatasetLog>>() {
		    });
	    
	    // Should be many of these 
	    assertTrue(logs.size()>1);


	    // Query logs of specific type
	    logs = target("log/dataset/getquery")
		.queryParam("type", "DatasetWrite").request()
		.get(new GenericType<List<DatasetLog>>() {
		    });
	    

	    // Should be many of these 
	    assertTrue(logs.size()>1);

	    // Query for bogus event type
	    int r = target("log/dataset/getquery")
		.queryParam("type", "DatasetBogus").request()
		.get(Response.class).getStatus();

	    assertBadRequest(r);


	    // Query using good date constraints
	    logs = target("log/dataset/getquery")
		.queryParam("type", "DatasetWrite")
		.queryParam("from", "01/1/2010 1:30 PM, GMT")
		.queryParam("to", "01/10/2024 1:30 PM, GMT")
		.request()
		.get(new GenericType<List<DatasetLog>>() {
		    });
	    
	    // Should be many of these 
	    assertTrue(logs.size()>1);

	    // Query including a pattern
	    logs = target("log/dataset/getquery")
		.queryParam("type", "DatasetWrite")
		.queryParam("pattern", "Unlikely String")
		.request()
		.get(new GenericType<List<DatasetLog>>() {
		    });
	    
	    // Should not be any of these 
	    assertTrue(logs.size()==0);

	    // Query using bad date constraints
	    int status = target("log/dataset/getquery")
		.queryParam("type", "DatasetWrite")
		.queryParam("from", "01/1/2010 1:30 PM, GMT")
		.queryParam("to", "no such date")
		.request()		
		.get(Response.class).getStatus();

	    assertBadRequest(status);

 	    logs = target("log/dataset/getquery").queryParam("dsid", dsid)
		.queryParam("city", goodCity2()).request()
		.get(new GenericType<List<DatasetLog>>() {
		    });

	    // Repeat the test but without asking for the logs
	    assertEquals(0, logs.size());
	    String empty = target("log/dataset/getquerytotal").queryParam("dsid", dsid)
		.queryParam("city", goodCity2()).request()
		.get(String.class);

	    // Should be no such log in the database
	    assertEquals("[0]", empty);
	}
    
	// ==================
	// Rating Tests
	// ==================

    /*
    @Test
	public void ratingHereTest() throws IOException {

		String response;

		response = target("log/rating/here").request().get(String.class);
		assertEquals("\"yes\"", response);
	}
    */


	@Test
	// Test to see that an event is logged using legacy get mechanism
	public void ratingTest() throws IOException {

		String response;
		response = target("log/rating/stamp/" + exappid() + "/1/1/TestPost")
				.request().get(String.class);

		assertEquals("\"rated\"", response);
		cleanRatings(exappid());
	}



	/*
	@Test
	// Test to see that an event is posted
	    public void ratingpostTest() throws IOException {
	    
	    Response response;
	    response = target("log/rating/response/" + exappid() + "/1/1/TestPost")
		.request(MediaType.APPLICATION_JSON).post(null, Response.class);
	    
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	*/


	@Test
	// Do a log then check to see that it has actually been logged
	public void ratingFullTestUntracked() throws IOException {

		String response;
		List<Rating> ratings;
		int appid;
		String call;
		// Generate a new random app key
		appid = gen.nextInt(100000);

		// Log with that as an appid
		call = "log/rating/stamp/" + appid + "/1/2/TestPost";
		// System.out.println("App call " + call);
		response = target(call).request().get(String.class);

		// Now see if that log is present in the database
		ratings = target("log/rating/getquery/" + appid)
		// .queryParam("app", appid)
				.request().get(new GenericType<List<Rating>>() {
				});
		// Should be one and only one such log in the database
		assertEquals(1, ratings.size());

		//Need to remove the log
		cleanRatings(String.valueOf(appid));

	}
    
    // Generate a set of random ratings
    public void randomRatings (String app, int num) {

	int i,q,a;
	Response response;
	
	for (i=0; i<num; i++) {

	    q = gen.nextInt(12) + 1;
	    a = gen.nextInt(5) + 1;
	    

	    response = target("log/rating/trackedresponse/" + app + "/-1234/" +
			      q + "/" + a + "/TestPost")
		.request(MediaType.APPLICATION_JSON).post(null, Response.class);
	    
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
    }
    
    @Test
	public void ratingTestStats() throws IOException {
	
	List<ResponseStat> rs;
	
	// Generate some responses
	randomRatings(exappid(), 50);
	
	rs = target("log/rating/getstats/"+exappid())
	    .request()
	    .get(new GenericType<List<ResponseStat>>(){});
	
	// Should be several questions
	assertTrue(rs.size() > 1);

	//Remove logs

	cleanRatings(exappid());
    }


	@Test
	// Do a log then check to see that it has actually been logged
	public void ratingFullTestTracked() throws IOException {

		List<Rating> ratings;
		int appid;
		String call;
		// Generate a new random app key
		appid = gen.nextInt(100000);
			    
		Response response;
		response = target("log/rating/trackedresponse/" + appid + "/-1234/1/1/TesterPost")
		    .request(MediaType.APPLICATION_JSON).post(null, Response.class);
	    
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


		// Now see if that log is present in the database
		ratings = target("log/rating/getquery/" + appid)
		// .queryParam("app", appid)
				.request().get(new GenericType<List<Rating>>() {
				});

		// Should be one and only one such log in the database
		assertEquals(1, ratings.size());

		// Get Apps
		List<String> apps;
		apps = target("log/rating/getapps")
		    .request()
		    .get(new GenericType<List<String>>(){});
		
		//System.out.println("Rated apps: " + apps);

		assertTrue(apps.size() > 0); //Must be at least one app in DB

		//Need to remove the ratings
		cleanRatings(String.valueOf(appid));
		

	}
    
}

