


JDO Logging
-----------


To set this up you need to create an iescities database under postgres.


As root create user iescities, and set password citiesies 


	> adduser iescities
	... citiesies
	

Now log in as the postgres admin account
	
	> su - postgres
	> psql template1
	
Now create the user and database:
	
	CREATE USER iescities WITH PASSWORD 'citiesies';
	CREATE DATABASE iescities;
	GRANT ALL PRIVILEGES ON DATABASE iescities to iescities;
	\q
	
Test

	> psql -d iescities -U iescities
	
Now you need to create the appropriate tables using datanucleus:

	> mvn datanucleus:schema-create

Use this sequence if you change any classes that are persisted:
	
	> mvn clean test datanucleus:schema-delete
	> mvn datanucleus:schema-create
	  
	  
You can check the database for fresh entries:

	> su - iescities
	> psql -h 127.0.0.1 -d iescities -U iescities -W
	> select * from "APPLOG";

(Note that the quotes are essential for postgres as names are case sensitive)

You may need to forcibly remove a table that needs updating:

	> drop table "APPLOG";

To list tables:

	> \dt;
	
To exit:
	
	> \q;
	

To update the timestamp field, run this on tables APPLOG, RATING, DATASETLOG


	ALTER TABLE "APPLOG"
		ALTER COLUMN "DATE" SET DATA TYPE timestamp with time zone
		USING
			timestamp with time zone 'epoch' + "TIMESTAMP" * interval '1 second';

	ALTER TABLE "DATASETLOG"
		ALTER COLUMN "DATE" SET DATA TYPE timestamp with time zone
		USING
			timestamp with time zone 'epoch' + "TIMESTAMP" * interval '1 second';

	ALTER TABLE "RATING"
		ALTER COLUMN "DATE" SET DATA TYPE timestamp with time zone
		USING
			timestamp with time zone 'epoch' + "TIMESTAMP" * interval '1 second';
			




To add the new session id column for the rating table;

	ALTER TABLE "RATING" ADD COLUMN "SESSIONID" integer;



SLF4j Logging
-------------

The actual logging is done via
[SLF4j](http://slf4j.org/faq.html). This framework allows us to change
the actual backend logging mechanism depending on how we want to
implement this. Currently this is set up and tested using the simplest
`stdio` file logging mechanism. This is set and can be changed in the
pom.xml of the project, by updating this fragment:


```xml
	<dependency>
	  <groupId>org.slf4j</groupId>
	  <artifactId>slf4j-simple</artifactId>
	  <version>${slf4jVersion}</version>
	</dependency>
```

Running
=======

To get this running you should just need:

```sh
mvn compile exec:java
```


Configuration
=============


You can drop a /etc/restlogger.conf on the machine of which you test this to change some
settings, for example

-- /etc/restlogger.conf --
```
server = 127.0.0.1
port = 8080
basedir = iescities
```
