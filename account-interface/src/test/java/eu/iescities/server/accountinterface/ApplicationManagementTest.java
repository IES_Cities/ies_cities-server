package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Unit tests for {@link ApplicationManagement}.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class ApplicationManagementTest {
	private static final String tempAppPackage = ApplicationManagementTest.class.getName() + ".TempApp";
	private static final String tempAppPackage2 = tempAppPackage + "2";
	protected static final String tempTagName = "tempTag";
	protected static final String tempTagName2 = "temporary";

	private PersistenceManager pm;

	private String adminSession;
	private String sessionId;
	private String unauthorizedSessionId;
	private String bobSessionId;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		pm = PMF.getPM();

		adminSession = TestUtils.loginAdminUser();
		sessionId = UserManagement.login("mike", "secret");
		unauthorizedSessionId = UserManagement.login("john", "secret");
		bobSessionId = UserManagement.login("bob", "secret");
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		// TODO: use package name!
		final Query query = pm.newQuery(ApplicationManagement.queryApplicationByPackageName);
		query.deletePersistentAll(tempAppPackage);
		query.deletePersistentAll(tempAppPackage2);
		try {
			UserManagement.logout(sessionId);
		} catch (final Exception e) {
		}
		pm.close();
	}

	private Application addDefaultTempApp() throws SecurityException,
			IllegalArgumentException {
		return addApp("tempApp", "descr", "http://server.iescities.eu",
				"http://server.iescities.eu/testApp.jpg",
				TestUtils.honululuCouncil.getCouncilId(), "0.01", "tos",
				"permissionA", tempAppPackage,
				new HashSet<String>(Arrays.asList(tempTagName, tempTagName2)),
				TestUtils.hawaiiCouncil.getGeographicalScope());
	}

	private Application addApp(final String appName, final String description,
			final String url, final String image, final Integer councilId,
			final String version, final String termsOfService,
			final String permissions, final String packageName,
			final HashSet<String> tags, final GeoScope geoScope)
			throws SecurityException, IllegalArgumentException {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName(appName);
		tempApp.setDescription(description);
		tempApp.setUrl(url);
		tempApp.setImage(image);
		tempApp.setRelatedCouncilId(councilId);
		tempApp.setVersion(version);
		tempApp.setTermsOfService(termsOfService);
		tempApp.setPermissions(permissions);
		tempApp.setPackageName(packageName);
		tempApp.setTags(tags);
		tempApp.setGeographicalScope(geoScope);
		
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		return app;
	}

	/**
	 * Tests {@link ApplicationTranslation#toBean()} and verifies all getters.
	 */
	@Test
	public void testToBean() {
		final ApplicationTranslation app1EN = ApplicationManagement
				.getAppTranslationByLang(pm, TestUtils.app1.getAppId(), Language.EN, null);
		compareApp(TestUtils.app1, app1EN, true);
		
		final eu.iescities.server.accountinterface.Application app1 = ApplicationManagement
				.getAppById(pm, TestUtils.app1.getAppId());
		app1EN.setApp(app1);
		compareApp(TestUtils.app1, app1EN, true);
		// now test with all getters of the two objects:
		assertEquals(TestUtils.app1.getAppId(), app1.getAppId());
		assertEquals(TestUtils.app1.getName(), app1EN.getName());
		assertEquals(TestUtils.app1.getPermissions(), app1.getPermissions());
		assertEquals(TestUtils.app1.getDescription(), app1EN.getDescription());
		assertEquals(TestUtils.app1.getUrl(), app1.getUrl());
		assertEquals(TestUtils.app1.getImage(), app1EN.getImage());
		assertEquals(TestUtils.app1.getPackageName(), app1.getPackageName());
		GeoScopeManagementTest.compareGeoScope(TestUtils.app1.getGeographicalScope(),
				app1.getGeographicalScope(), true);
		if (TestUtils.app1.getRelatedCouncilId() == null) {
			assertNull(app1.getRelatedCouncil());
		} else {
			assertEquals((int) TestUtils.app1.getRelatedCouncilId(), app1
					.getRelatedCouncil().getCouncilId());
		}
		assertEquals(TestUtils.app1.getGooglePlayRating(), app1.getGooglePlayRating());
		assertEquals(TestUtils.app1.getVersion(), app1.getVersion());
		assertEquals(TestUtils.app1.getTermsOfService(), app1EN.getTos());
		assertEquals(TestUtils.app1.getAccessNumber(), Integer.valueOf(app1.getAccessNumber()));
		assertEquals(TestUtils.app1.getDownloadNumber(), Integer.valueOf(app1.getDownloadNumber()));
		assertArrayEquals(TestUtils.app1.getTags().toArray(), app1EN.getTags().toArray());
	}

	/**
	 * Tests {@link ApplicationManagement#verifyAppId(int)}.
	 */
	@Test
	public void testVerifyAppId() {
		assertTrue(ApplicationManagement.verifyAppId(TestUtils.app1.getAppId()));
		assertFalse(ApplicationManagement.verifyAppId(-1));
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with an English locale.
	 */
	@Test
	public void testRegisterApp1EN() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		tempApp.setAccessNumber(10);
		tempApp.setDownloadNumber(2);
		tempApp.setGooglePlayRating(1.1);
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		assertTrue(ApplicationManagement.verifyAppId(app.getAppId()));
		compareApp(tempApp, app, false);
		compareApp(app, ApplicationManagement.getAppById(app.getAppId(),
				Language.EN, null), true);
		final List<Application> apps = UserManagement
				.getDevelopedApps(sessionId, Language.EN, null);
		assertEquals(1, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a Spanish locale.
	 */
	@Test
	public void testRegisterApp1ES1() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.ES);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		tempApp.setAccessNumber(10);
		tempApp.setDownloadNumber(2);
		tempApp.setGooglePlayRating(1.1);
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		assertTrue(ApplicationManagement.verifyAppId(app.getAppId()));
		compareApp(tempApp, app, false);
		compareApp(app, ApplicationManagement.getAppById(app.getAppId(),
				Language.ES, null), true);
		final List<Application> apps = UserManagement.getDevelopedApps(
				sessionId, Language.ES, null);
		assertEquals(1, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a Spanish locale retrieving English with fallback Spanish.
	 */
	@Test
	public void testRegisterApp1ES2() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.ES);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		tempApp.setAccessNumber(10);
		tempApp.setDownloadNumber(2);
		tempApp.setGooglePlayRating(1.1);
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		assertTrue(ApplicationManagement.verifyAppId(app.getAppId()));
		compareApp(tempApp, app, false);
		compareApp(app, ApplicationManagement.getAppById(app.getAppId(),
				Language.EN, Language.ES), true);
		final List<Application> apps = UserManagement.getDevelopedApps(
				sessionId, Language.EN, Language.ES);
		assertEquals(1, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a Spanish locale retrieving English with no fallback.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterApp1ES3() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.ES);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		tempApp.setAccessNumber(10);
		tempApp.setDownloadNumber(2);
		tempApp.setGooglePlayRating(1.1);
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		assertTrue(ApplicationManagement.verifyAppId(app.getAppId()));
		compareApp(tempApp, app, false);
		final List<Application> apps = UserManagement.getDevelopedApps(
				sessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(app, ApplicationManagement.getAppById(app.getAppId(),
				Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with minimal provided info.
	 */
	@Test
	public void testRegisterApp2() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setPackageName(tempAppPackage);
		final Application app = ApplicationManagement.registerApp(sessionId,
				tempApp);
		assertTrue(ApplicationManagement.verifyAppId(app.getAppId()));
		compareApp(tempApp, app, false);
		compareApp(app, ApplicationManagement.getAppById(app.getAppId(),
				Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * trying to register the app twice with the same package name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppTwice1() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setPackageName(tempAppPackage);
		
		final Application registeredApp = ApplicationManagement
				.registerApp(sessionId, tempApp);
		assertTrue(ApplicationManagement.verifyAppId(registeredApp.getAppId()));
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * trying to register the app twice with different names but same package
	 * name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppTwice2() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp1");
		tempApp.setPackageName(tempAppPackage);
		
		final Application registeredApp = ApplicationManagement
				.registerApp(sessionId, tempApp);
		assertTrue(ApplicationManagement.verifyAppId(registeredApp.getAppId()));
		tempApp.setName("tempApp2");
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * trying to register the app twice with the same package name but different
	 * languages.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppTwice3() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp1");
		tempApp.setPackageName(tempAppPackage);
		
		final Application registeredApp = ApplicationManagement
				.registerApp(sessionId, tempApp);
		assertTrue(ApplicationManagement.verifyAppId(registeredApp.getAppId()));
		tempApp.setLang(Language.ES);
		tempApp.setName("tempApp2");
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * trying to register the app twice with the same name but different
	 * languages.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppTwice4() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp1");
		tempApp.setPackageName(tempAppPackage);
		
		final Application registeredApp = ApplicationManagement
				.registerApp(sessionId, tempApp);
		assertTrue(ApplicationManagement.verifyAppId(registeredApp.getAppId()));
		tempApp.setLang(Language.ES);
		tempApp.setPackageName(tempAppPackage2);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRegisterAppUnauthorized() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		
		ApplicationManagement.registerApp(UUID.randomUUID().toString(),
				tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with an empty app name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidParameter1() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("");
		tempApp.setPackageName(tempAppPackage);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a <tt>null</tt> app name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidParameter1a() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName(null);
		tempApp.setPackageName(tempAppPackage);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with an empty package name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidParameter2() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setPackageName("");
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a <tt>null</tt> package name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidParameter2a() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setPackageName(null);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with a <tt>null</tt> language string.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidParameter3() {
		final Application tempApp = new Application();
		tempApp.setLang(null);
		tempApp.setName("tempApp");
		tempApp.setPackageName(tempAppPackage);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#registerApp(String, Application)}
	 * with an invalid geo scope.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterAppInvalidScope() {
		final Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setPackageName(tempAppPackage);
		final GeoScope invalidScope = new GeoScope("invalid", 0.0, 0.0, 0.0,
				0.0);
		tempApp.setGeographicalScope(invalidScope);
		ApplicationManagement.registerApp(sessionId, tempApp);
	}

	/**
	 * Tests {@link ApplicationManagement#getAppById(int, Language, Language)}.
	 */
	@Test
	public void testGetAppById1a() {
		final Application app = ApplicationManagement.getAppById(TestUtils.app1
				.getAppId(), Language.EN, null);
		compareApp(TestUtils.app1, app, true);
	}

	/**
	 * Tests {@link ApplicationManagement#getAppById(int, Language, Language)}
	 * with a non-existing translation and a valid fallback.
	 */
	@Test
	public void testGetAppById1b() {
		final Application app = ApplicationManagement.getAppById(TestUtils.app1
				.getAppId(), Language.ES, Language.EN);
		compareApp(TestUtils.app1, app, true);
	}

	/**
	 * Tests {@link ApplicationManagement#getAppById(int, Language, Language)}
	 * with a non-existing translation and no fall-back.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppById2() {
		ApplicationManagement.getAppById(TestUtils.app1.getAppId(),
				Language.ES, null);
	}

	/**
	 * Tests {@link ApplicationManagement#getAppById(int, Language, Language)}
	 * with a non-existing translation and a non-existing fall-back.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppById3() {
		ApplicationManagement.getAppById(TestUtils.app1.getAppId(),
				Language.ES, Language.IT);
	}

	/**
	 * Tests {@link ApplicationManagement#getAppById(int, Language, Language)}
	 * with an invalid app ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppByIdNonExisting1() {
		ApplicationManagement.getAppById(-1, Language.EN, null);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAppByPackageName(String, Language, Language)}
	 * .
	 */
	@Test
	public void testGetAppByPackageName1a() {
		final Application app = ApplicationManagement.getAppByPackageName(TestUtils.app1
				.getPackageName(), Language.EN, null);
		compareApp(TestUtils.app1, app, true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAppByPackageName(String, Language, Language)}
	 * with a non-existing translation and a valid fallback..
	 */
	@Test
	public void testGetAppByPackageName1b() {
		final Application app = ApplicationManagement.getAppByPackageName(TestUtils.app1
				.getPackageName(), Language.ES, Language.EN);
		compareApp(TestUtils.app1, app, true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAppByPackageName(String, Language, Language)}
	 * with an invalid package name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppByPackageName2() {
		ApplicationManagement.getAppByPackageName("foo", Language.EN, null);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAppByPackageName(String, Language, Language)}
	 * with a non-existing translation and no fall-back.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppByPackageName3() {
		ApplicationManagement.getAppByPackageName(
				TestUtils.app1.getPackageName(), Language.ES, null);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAppByPackageName(String, Language, Language)}
	 * with a non-existing translation and a non-existing fall-back.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppByPackageName4() {
		ApplicationManagement.getAppByPackageName(
				TestUtils.app1.getPackageName(), Language.ES, Language.IT);
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} from the app
	 * developer session.
	 */
	@Test
	public void testRemoveApp1() {
		final int appId = addDefaultTempApp().getAppId();

		assertEquals(1, ApplicationManagement.removeApp(sessionId, appId));
		final Query query = pm
				.newQuery(ApplicationManagement.queryApplicationByPackageName);
		assertNull(query.execute(tempAppPackage));
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} from an admin
	 * session.
	 */
	@Test
	public void testRemoveApp2() {
		final int appId = addDefaultTempApp().getAppId();

		assertEquals(1, ApplicationManagement.removeApp(adminSession, appId));
		final Query query = pm
				.newQuery(ApplicationManagement.queryApplicationByPackageName);
		assertNull(query.execute(tempAppPackage));
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} from an
	 * unauthorised session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveApp3() {
		final int appId = addDefaultTempApp().getAppId();

		ApplicationManagement.removeApp(unauthorizedSessionId, appId);
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} from a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveApp4() {
		final int appId = addDefaultTempApp().getAppId();

		ApplicationManagement.removeApp(UUID.randomUUID().toString(), appId);
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} from an admin
	 * session using a non-existing application ID.
	 */
	@Test
	public void testRemoveApp5() {
		assertEquals(0, ApplicationManagement.removeApp(adminSession, -1));
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} with an app
	 * which is installed by a user.
	 */
	@Test
	public void testRemoveAppWithUser() {
		final int appId = addDefaultTempApp().getAppId();

		UserManagement.addInstalledApp(sessionId, appId);

		assertEquals(1, ApplicationManagement.removeApp(sessionId, appId));
		final Query query = pm
				.newQuery(ApplicationManagement.queryApplicationByPackageName);
		assertNull(query.execute(tempAppPackage));
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} with an app
	 * which is installed by a user.
	 */
	@Test
	public void testRemoveAppWithTwoDevelopers() {
		final Application tempApp = addDefaultTempApp();

		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDeveloper(sessionId, tempApp.getAppId(),
				TestUtils.bobUser.getUserId());
		List<Application> apps = UserManagement.getDevelopedApps(sessionId,
				Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);
		apps = UserManagement.getDevelopedApps(bobSessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);

		assertEquals(1,
				ApplicationManagement.removeApp(sessionId, tempApp.getAppId()));
		final Query query = pm
				.newQuery(ApplicationManagement.queryApplicationByPackageName);
		assertNull(query.execute(tempAppPackage));

		apps = UserManagement.getDevelopedApps(sessionId, Language.EN, null);
		assertEquals(0, apps.size());
		apps = UserManagement.getDevelopedApps(bobSessionId, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests {@link ApplicationManagement#removeApp(String, int)} with an app
	 * which is installed by a user and has no tags.
	 */
	@Test
	public void testRemoveAppWithoutTags() {
		final int appId = addApp("tempApp", "descr",
				"http://server.iescities.eu",
				"http://server.iescities.eu/testApp.jpg",
				TestUtils.honululuCouncil.getCouncilId(), "0.01", "tos",
				"permissionA", tempAppPackage, new HashSet<String>(),
				TestUtils.hawaiiCouncil.getGeographicalScope()).getAppId();

		UserManagement.addInstalledApp(sessionId, appId);

		assertEquals(1, ApplicationManagement.removeApp(sessionId, appId));
		final Query query = pm
				.newQuery(ApplicationManagement.queryApplicationByPackageName);
		assertNull(query.execute(tempAppPackage));
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * changing the app's description and permissions.
	 */
	@Test
	public void testUpdateApp1() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setDescription("bar");
		updateBean.setPermissions("permissionB");
		ApplicationManagement.updateApp(sessionId, updateBean);

		tempApp.setDescription(updateBean.getDescription());
		tempApp.setPermissions(updateBean.getPermissions());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * changing the app's name and description.
	 */
	@Test
	public void testUpdateApp2() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName("tempApp2");
		updateBean.setDescription("bar");
		ApplicationManagement.updateApp(sessionId, updateBean);

		tempApp.setName(updateBean.getName());
		tempApp.setDescription(updateBean.getDescription());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * changing the app's name and description.
	 */
	@Test
	public void testUpdateApp2a() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName("tempApp1");
		updateBean.setDescription("bar");
		ApplicationManagement.updateApp(sessionId, updateBean);

		tempApp.setName(updateBean.getName());
		tempApp.setDescription(updateBean.getDescription());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * adding a new translation with a different name.
	 */
	@Test
	public void testUpdateApp2b() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.ES);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName("tempApp1 (ES)");
		updateBean.setDescription("bar (ES)");
		ApplicationManagement.updateApp(sessionId, updateBean);

		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);

		tempApp.setLang(Language.ES);
		tempApp.setName(updateBean.getName());
		tempApp.setDescription(updateBean.getDescription());
		tempApp.setImage(null);
		tempApp.setTermsOfService("");
		tempApp.setTags(new HashSet<String>());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.ES, null), true);
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.ES, Language.EN), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * adding a new translation with the same name.
	 */
	@Test
	public void testUpdateApp2c() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.ES);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName(tempApp.getName());
		updateBean.setDescription("bar (ES)");
		ApplicationManagement.updateApp(sessionId, updateBean);

		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);

		tempApp.setLang(Language.ES);
		tempApp.setName(updateBean.getName());
		tempApp.setDescription(updateBean.getDescription());
		tempApp.setImage(null);
		tempApp.setTermsOfService("");
		tempApp.setTags(new HashSet<String>());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.ES, null), true);
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.ES, Language.EN), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * changing the app's description and package name.
	 */
	@Test
	public void testUpdateApp3() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setDescription("bar");
		updateBean.setPackageName(tempAppPackage2);
		ApplicationManagement.updateApp(sessionId, updateBean);

		tempApp.setDescription(updateBean.getDescription());
		tempApp.setPackageName(updateBean.getPackageName());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * changing the app's description and package name (to the same value).
	 */
	@Test
	public void testUpdateApp3a() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setDescription("bar");
		updateBean.setPackageName(tempApp.getPackageName());
		ApplicationManagement.updateApp(sessionId, updateBean);

		tempApp.setDescription(updateBean.getDescription());
		tempApp.setPackageName(updateBean.getPackageName());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)} as
	 * an IES Cities admin.
	 */
	@Test
	public void testUpdateAppAsAdmin() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setDescription("bar");
		ApplicationManagement.updateApp(adminSession, updateBean);

		tempApp.setDescription(updateBean.getDescription());
		compareApp(tempApp, ApplicationManagement.getAppById(
				tempApp.getAppId(), Language.EN, null), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an invalid application ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppInvalidId() {
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(-1);
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a name longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongName() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName(TestUtils.randomString(260));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an empty name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppEmptyName() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setName("");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an non-unique name in the same language.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppNonUniqueName1() {
		final Application tempApp1 = addDefaultTempApp();
		final Application tempApp2 = new Application();
		tempApp2.setLang(Language.EN);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		ApplicationManagement.registerApp(sessionId, tempApp2);
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp1.getAppId());
		updateBean.setName("tempApp2");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an non-unique name in another language.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppNonUniqueName2() {
		final Application tempApp1 = addDefaultTempApp();
		final Application tempApp2 = new Application();
		tempApp2.setLang(Language.EN);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		ApplicationManagement.registerApp(sessionId, tempApp2);
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.ES);
		updateBean.setAppId(tempApp1.getAppId());
		updateBean.setName("tempApp2");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a description longer than 65535 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongDescription() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setDescription(TestUtils.randomString(66000));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a version longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongVersion() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setVersion(TestUtils.randomString(260));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a TOS longer than 65535 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongTos() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setTermsOfService(TestUtils.randomString(66000));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a permissions string longer than 65535 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongPermissions() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setPermissions(TestUtils.randomString(66000));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a package name longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongPackageName() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setPackageName(TestUtils.randomString(260));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an empty package name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppEmptyPackageName() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setPackageName("");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an URL longer than 10000 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongUrl() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setUrl(TestUtils.randomString(10001));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an invalid/unsupported URL.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppInvalidUrl1() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setUrl("ftp://foo.example.com");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an invalid/unsupported URL.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppInvalidUrl2() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setUrl("http://foo.example");
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an image (path) longer than 10000 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongImage() {
		final Application tempApp = addDefaultTempApp();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(tempApp.getAppId());
		updateBean.setImage(TestUtils.randomString(10001));
		ApplicationManagement.updateApp(sessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with an (unauthorised) user session.
	 */
	@Test(expected = SecurityException.class)
	public void testUpdateAppUnauthorized1() {
		final int appId = addDefaultTempApp().getAppId();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(appId);
		updateBean.setDescription("bar");
		ApplicationManagement.updateApp(unauthorizedSessionId, updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#updateApp(String, Application)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testUpdateAppUnauthorized2() {
		final int appId = addDefaultTempApp().getAppId();
		
		final Application updateBean = new Application();
		updateBean.setLang(Language.EN);
		updateBean.setAppId(appId);
		updateBean.setDescription("bar");
		ApplicationManagement.updateApp(UUID.randomUUID().toString(),
				updateBean);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllDatasetsOfApp(int, int, int, Language, Language)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllDatasetsOfApp1() {
		final List<Dataset> datasets = ApplicationManagement
				.getAllDatasetsOfApp(TestUtils.app1.getAppId(), 0, 1,
						Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllDatasetsOfApp(int, int, int, Language, Language)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetAllDatasetsOfApp2() {
		final List<Dataset> datasets = ApplicationManagement
				.getAllDatasetsOfApp(TestUtils.app1.getAppId(), 0, 2,
						Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllDatasetsOfApp(int, int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetAllDatasetsOfApp3() {
		final List<Dataset> datasets = ApplicationManagement
				.getAllDatasetsOfApp(TestUtils.app1.getAppId(), 1, 10,
						Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllDatasetsOfApp(int, int, int, Language, Language)}
	 * with a limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllDatasetsOfAppTooMany() {
		ApplicationManagement.getAllDatasetsOfApp(TestUtils.app1.getAppId(), 0,
				10000, Language.EN, null);
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session.
	 */
	@Test
	public void testAddDeveloperAdmin1() {
		final Application tempApp = addDefaultTempApp();
		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		final List<Application> apps = UserManagement.getDevelopedApps(
				sessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session adding the same developer twice.
	 */
	@Test
	public void testAddDeveloperAdmin2() {
		final Application tempApp = addDefaultTempApp();
		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		List<Application> apps = UserManagement.getDevelopedApps(sessionId,
				Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);

		// add again:
		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		apps = UserManagement.getDevelopedApps(sessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDeveloperAdmin3() {
		addDefaultTempApp();
		ApplicationManagement.addDeveloper(adminSession, -1,
				TestUtils.mikeUser.getUserId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDeveloperAdmin4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with a
	 * developer session.
	 */
	@Test
	public void testAddDeveloperDevel1() {
		final Application tempApp = addDefaultTempApp();
		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDeveloper(sessionId, tempApp.getAppId(),
				TestUtils.bobUser.getUserId());
		final List<Application> apps = UserManagement.getDevelopedApps(
				bobSessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with a
	 * developer session adding the same developer twice.
	 */
	@Test
	public void testAddDeveloperDevel2() {
		final Application tempApp = addDefaultTempApp();
		ApplicationManagement.addDeveloper(adminSession, tempApp.getAppId(),
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDeveloper(sessionId, tempApp.getAppId(),
				TestUtils.bobUser.getUserId());
		List<Application> apps = UserManagement.getDevelopedApps(bobSessionId,
				Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);

		// add again:
		ApplicationManagement.addDeveloper(sessionId, tempApp.getAppId(),
				TestUtils.bobUser.getUserId());
		apps = UserManagement.getDevelopedApps(bobSessionId, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(tempApp, apps.iterator().next(), true);
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with a
	 * developer session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDeveloperDevel3() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDeveloper(sessionId, -1,
				TestUtils.bobUser.getUserId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with a
	 * developer session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDeveloperDevel4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDeveloper(sessionId, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with
	 * an (unauthorised) user session.
	 */
	@Test(expected = SecurityException.class)
	public void testAddDeveloperUnauthorized1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(unauthorizedSessionId, appId,
				TestUtils.mikeUser.getUserId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDeveloper(String, int, int)} with a
	 * random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddDeveloperUnauthorized2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(UUID.randomUUID().toString(), appId,
				TestUtils.mikeUser.getUserId());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllDevelopersOfApp(int, int, int)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllDevelopersOfApp1() {
		final List<User> users = ApplicationManagement
				.getAllDevelopersOfApp(TestUtils.app1.getAppId(), 0, 1);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0), TestUtils.johnUser, true);
	}

	/**
	 * Tests {@link ApplicationManagement#getAllDevelopersOfApp(int, int, int)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetAllDevelopersOfApp2() {
		final List<User> users = ApplicationManagement
				.getAllDevelopersOfApp(TestUtils.app1.getAppId(), 0, 2);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0), TestUtils.johnUser, true);
	}

	/**
	 * Tests {@link ApplicationManagement#getAllDevelopersOfApp(int, int, int)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetAllDevelopersOfApp3() {
		final List<User> users = ApplicationManagement
				.getAllDevelopersOfApp(TestUtils.app1.getAppId(), 1, 10);
		assertEquals(0, users.size());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllDevelopersOfApp(int, int, int)}
	 * with a limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllDevelopersOfAppTooMany() {
		ApplicationManagement.getAllDevelopersOfApp(TestUtils.app1.getAppId(),
				0, 10000);
	}

	/**
	 * Tests {@link ApplicationManagement#getAllDevelopersOfApp(int, int, int)}
	 * with a non-existing app ID and a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllDevelopersOfAppNonExisting1() {
		final List<User> ratings = ApplicationManagement.getAllDevelopersOfApp(
				-1, 0, 1);
		assertEquals(0, ratings.size());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session.
	 */
	@Test
	public void testAddDatasetAdmin1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		final List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session adding the same dataset twice.
	 */
	@Test
	public void testAddDatasetAdmin2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// add again:
		ApplicationManagement.addDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDatasetAdmin3() {
		addDefaultTempApp();
		ApplicationManagement.addDataset(adminSession, -1,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * admin session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDatasetAdmin4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDataset(adminSession, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with a
	 * developer session.
	 */
	@Test
	public void testAddDatasetDevel1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDataset(sessionId, appId,
				TestUtils.dataset1.getDatasetId());
		final List<Dataset> datasets = ApplicationManagement
				.getAllDatasetsOfApp(appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with a
	 * developer session adding the same dataset twice.
	 */
	@Test
	public void testAddDatasetDevel2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDataset(sessionId, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// add again:
		ApplicationManagement.addDataset(sessionId, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with a
	 * developer session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDatasetDevel3() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDataset(sessionId, -1,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with a
	 * developer session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddDatasetDevel4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.addDataset(sessionId, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with an
	 * (unauthorised) user session.
	 */
	@Test(expected = SecurityException.class)
	public void testAddDatasetUnauthorized1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDataset(unauthorizedSessionId, appId,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#addDataset(String, int, int)} with a
	 * random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddDatasetUnauthorized2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDataset(UUID.randomUUID().toString(), appId,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * an admin session.
	 */
	@Test
	public void testRemoveDatasetAdmin1() {
		final int appId = addDefaultTempApp().getAppId();
		// first add the dataset:
		ApplicationManagement.addDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// now remove the dataset:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * an admin session removing the same dataset twice.
	 */
	@Test
	public void testRemoveDatasetAdmin2() {
		final int appId = addDefaultTempApp().getAppId();
		// first add the dataset:
		ApplicationManagement.addDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// now remove the dataset:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// remove the dataset again:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * an admin session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveDatasetAdmin3() {
		addDefaultTempApp();
		ApplicationManagement.removeDataset(adminSession, -1,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * an admin session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveDatasetAdmin4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.removeDataset(adminSession, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * a developer session.
	 */
	@Test
	public void testRemoveDatasetDevel1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		// first add the dataset:
		ApplicationManagement.addDataset(sessionId, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// now remove the dataset:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * a developer session removing the same dataset twice.
	 */
	@Test
	public void testRemoveDatasetDevel2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		// first add the dataset:
		ApplicationManagement.addDataset(sessionId, appId,
				TestUtils.dataset1.getDatasetId());
		List<Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
				appId, 0, 10, Language.EN, null);
		assertEquals(1, datasets.size());
		final Dataset ds = TestUtils.dataset1.clone();
		ds.setNumberAppsUsingIt(ds.getNumberAppsUsingIt() + 1);
		DatasetManagementTest.compareDataset(ds, datasets.get(0), true);
		assertEquals(
				ds.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// now remove the dataset:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());

		// remove the dataset again:
		ApplicationManagement.removeDataset(adminSession, appId,
				TestUtils.dataset1.getDatasetId());
		datasets = ApplicationManagement.getAllDatasetsOfApp(appId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
		assertEquals(
				TestUtils.dataset1.getNumberAppsUsingIt(),
				DatasetManagement.getAppsUsingDataSet(
						TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN,
						null).size());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * a developer session and an invalid app id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveDatasetDevel3() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.removeDataset(sessionId, -1,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * a developer session and an invalid dataset id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveDatasetDevel4() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.addDeveloper(adminSession, appId,
				TestUtils.mikeUser.getUserId());
		ApplicationManagement.removeDataset(sessionId, appId, -1);
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * an (unauthorised) user session.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveDatasetUnauthorized1() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.removeDataset(unauthorizedSessionId, appId,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests {@link ApplicationManagement#removeDataset(String, int, int)} with
	 * a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveDatasetUnauthorized2() {
		final int appId = addDefaultTempApp().getAppId();
		ApplicationManagement.removeDataset(UUID.randomUUID().toString(), appId,
				TestUtils.dataset1.getDatasetId());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllApps1() {
		final List<Application> apps = ApplicationManagement.getAllApps(0, 1,
				Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetAllApps2() {
		final List<Application> apps = ApplicationManagement.getAllApps(0, 2,
				Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetAllApps3() {
		final List<Application> apps = ApplicationManagement.getAllApps(1, 10,
				Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllAppsTooMany() {
		ApplicationManagement.getAllApps(2, 10000, Language.EN, null);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 10, offset 0 and non-translated apps.
	 */
	@Test
	public void testGetAllAppsL10n1() {
		final List<Application> apps = ApplicationManagement.getAllApps(0, 10,
				Language.ES, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 10, offset 0 and apps in two languages, retrieving all of
	 * them (first lang EN, then ES).
	 */
	@Test
	public void testGetAllAppsL10n2() {
		Application tempApp2 = new Application();
		tempApp2.setLang(Language.ES);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		tempApp2 = ApplicationManagement.registerApp(sessionId, tempApp2);
		
		final List<Application> apps = ApplicationManagement.getAllApps(0, 10,
				Language.EN, Language.ES);
		assertEquals(2, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
		compareApp(tempApp2, apps.get(1), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#getAllApps(int, int, Language, Language)}
	 * with a limit of 10, offset 0 and apps in two languages, retrieving all of
	 * them (first lang ES, then EN).
	 */
	@Test
	public void testGetAllAppsL10n3() {
		Application tempApp2 = new Application();
		tempApp2.setLang(Language.ES);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		tempApp2 = ApplicationManagement.registerApp(sessionId, tempApp2);
		
		final List<Application> apps = ApplicationManagement.getAllApps(0, 10,
				Language.ES, Language.EN);
		assertEquals(2, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
		compareApp(tempApp2, apps.get(1), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one existing search word with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith1Word1() {
		final List<Application> apps = ApplicationManagement.findApps("lorem",
				0, 1, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one non-existing search word with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith1Word2() {
		final List<Application> apps = ApplicationManagement.findApps("user",
				0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using an existing search word with a limit of 2, offset 0 with two
	 * apps of different languages finding both.
	 */
	@Test
	public void testFindAppsWith1WordL10n1() {
		Application tempApp2 = new Application();
		tempApp2.setLang(Language.ES);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		tempApp2.setDescription("Spanish lorem ipsum");
		tempApp2 = ApplicationManagement.registerApp(sessionId, tempApp2);

		final List<Application> apps = ApplicationManagement.findApps("lorem",
				0, 2, Language.EN, Language.ES);
		assertEquals(2, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
		compareApp(tempApp2, apps.get(1), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using an existing search word with a limit of 2, offset 0 with two
	 * apps of different languages finding one (fallback) language only.
	 */
	@Test
	public void testFindAppsWith1WordL10n2() {
		Application tempApp2 = new Application();
		tempApp2.setLang(Language.ES);
		tempApp2.setName("tempApp2");
		tempApp2.setPackageName(tempAppPackage2);
		tempApp2.setDescription("Spanish lorem ipsum");
		tempApp2 = ApplicationManagement.registerApp(sessionId, tempApp2);

		final List<Application> apps = ApplicationManagement.findApps("Spanish",
				0, 2, Language.EN, Language.ES);
		assertEquals(1, apps.size());
		compareApp(tempApp2, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using an existing search word with a limit of 2, offset 0 with one app in
	 * two languages finding both versions (the fallback version should be
	 * ignored).
	 */
	@Test
	public void testFindAppsWith1WordL10n3() {
		Application tempApp1 = new Application();
		tempApp1.setLang(Language.EN);
		tempApp1.setName("tempApp1");
		tempApp1.setPackageName(tempAppPackage);
		tempApp1.setDescription("English OL32Hf99 eqx3D8yQ JoZ8PP0y MJh3LyyA ");
		tempApp1 = ApplicationManagement.registerApp(adminSession, tempApp1);
		
		Application tempApp2 = new Application();
		tempApp2.setAppId(tempApp1.getAppId());
		tempApp2.setLang(Language.ES);
		tempApp2.setName("tempApp2");
		tempApp2.setDescription("Spanish OL32Hf99 eqx3D8yQ JoZ8PP0y MJh3LyyA ");
		tempApp2 = ApplicationManagement.updateApp(adminSession, tempApp2);

		List<Application> apps = ApplicationManagement.findApps("eqx3D8yQ", 0,
				2, Language.EN, Language.ES);
		assertEquals(1, apps.size());
		compareApp(tempApp1, apps.get(0), true);

		apps = ApplicationManagement.findApps("eqx3D8yQ", 0, 2, Language.ES,
				Language.EN);
		assertEquals(1, apps.size());
		compareApp(tempApp2, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using two existing search words with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith2Words1() {
		final List<Application> apps = ApplicationManagement.findApps(
				"consectetur amet", 0, 1, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using two non-existing search words with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith2Words2() {
		final List<Application> apps = ApplicationManagement.findApps(
				"user council", 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one existing tag with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith1Tag1() {
		final List<Application> apps = ApplicationManagement.findApps(
				"distributed", 0, 1, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one non-existing tag with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith1Tag2() {
		final List<Application> apps = ApplicationManagement.findApps(
				"Distributed", 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one existing and one non-existing tag with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith2Tags1() {
		final List<Application> apps = ApplicationManagement.findApps(
				"council fixit", 0, 1, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using two non-existing tags with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith2Tags2() {
		final List<Application> apps = ApplicationManagement.findApps(
				"user council", 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one existing app name with a limit of 2, offset 0.
	 */
	@Test
	public void testFindAppsWith1AppName1() {
		final List<Application> apps = ApplicationManagement.findApps("app1",
				0, 2, Language.EN, null);
		assertEquals(1, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using one non-existing app name with a limit of 2, offset 0.
	 */
	@Test
	public void testFindAppsWith1AppName2() {
		final List<Application> apps = ApplicationManagement.findApps("app2",
				0, 2, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using two existing app names with a limit of 10, offset 0.
	 */
	@Test
	public void testFindAppsWith2AppNames1() {
		Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("tempApp");
		tempApp.setDescription("descr");
		tempApp.setUrl("http://server.iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempApp.setVersion("0.01");
		tempApp.setTermsOfService("tos");
		tempApp.setPermissions("permissionA");
		tempApp.setPackageName(tempAppPackage);
		tempApp.setTags(new HashSet<String>());
		tempApp.setGeographicalScope(TestUtils.hawaiiCouncil
				.getGeographicalScope());
		
		tempApp = ApplicationManagement.registerApp(sessionId, tempApp);
		final List<Application> apps = ApplicationManagement.findApps(
				"app1 tempApp", 0, 10, Language.EN, null);
		assertEquals(2, apps.size());
		compareApp(TestUtils.app1, apps.get(0), true);
		compareApp(tempApp, apps.get(1), true);
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * using two non-existing app names with a limit of 1, offset 0.
	 */
	@Test
	public void testFindAppsWith2AppName2() {
		final List<Application> apps = ApplicationManagement.findApps(
				"app2 app3", 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link ApplicationManagement#findApps(String, int, int, Language, Language)}
	 * with a limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFindAppsTooMany() {
		ApplicationManagement.findApps("app1", 0, 10000, Language.EN, null);
	}

	/**
	 * Tests {@link ApplicationManagement#getAllRatingsOfApp(int, int, int)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllRatingsOfApp1() {
		final List<AppRating> ratings = ApplicationManagement
				.getAllRatingsOfApp(TestUtils.app1.getAppId(), 0, 1);
		assertEquals(1, ratings.size());
		assertEquals(TestUtils.rating1.getRatingId(), ratings.get(0)
				.getRatingId());
		assertEquals(TestUtils.rating1.getUserName(), ratings.get(0)
				.getUserName());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllRatingsOfApp(int, int, int)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetAllRatingsOfApp2() {
		final List<AppRating> ratings = ApplicationManagement
				.getAllRatingsOfApp(TestUtils.app1.getAppId(), 0, 2);
		assertEquals(2, ratings.size());
		assertEquals(TestUtils.rating1.getRatingId(), ratings.get(0)
				.getRatingId());
		assertEquals(TestUtils.rating1.getUserName(), ratings.get(0)
				.getUserName());
		assertEquals(TestUtils.rating2.getRatingId(), ratings.get(1)
				.getRatingId());
		assertEquals(TestUtils.rating2.getUserName(), ratings.get(1)
				.getUserName());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllRatingsOfApp(int, int, int)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetAllRatingsOfApp3() {
		final List<AppRating> ratings = ApplicationManagement
				.getAllRatingsOfApp(TestUtils.app1.getAppId(), 1, 10);
		assertEquals(1, ratings.size());
		assertEquals(TestUtils.rating2.getRatingId(), ratings.get(0)
				.getRatingId());
		assertEquals(TestUtils.rating2.getUserName(), ratings.get(0)
				.getUserName());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllRatingsOfApp(int, int, int)}
	 * with a non-existing app ID and a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllRatingsOfAppNonExisting1() {
		final List<AppRating> ratings = ApplicationManagement
				.getAllRatingsOfApp(-1, 0, 1);
		assertEquals(0, ratings.size());
	}

	/**
	 * Tests {@link ApplicationManagement#getAllRatingsOfApp(int, int, int)}
	 * with a limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllRatingsOfAppTooMany() {
		ApplicationManagement.getAllRatingsOfApp(TestUtils.app1.getAppId(),
				0, 10000);
	}

	/**
	 * Compares the given two applications and verifies that all their
	 * properties are equal.
	 * 
	 * @param expected
	 *            the expected app
	 * @param actual
	 *            the actual app
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareApp(final Application expected,
			final ApplicationTranslation actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		compareApp(expected, actual.toBean(), completeExpBean);
	}

	/**
	 * Compares the given two applications and verifies that all their
	 * properties are equal.
	 * 
	 * @param expected
	 *            the expected app
	 * @param actual
	 *            the actual app
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareApp(final Application expected,
			final Application actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getAppId() != -1) {
			assertEquals("getAppId()", expected.getAppId(), actual.getAppId());
		}
		if (completeExpBean || expected.getLang() != null) {
			assertEquals("getLang()", expected.getLang(), actual.getLang());
		}
		assertEquals("getName()", expected.getName(), actual.getName());
		if (completeExpBean || expected.getPermissions() != null) {
			assertEquals("getPermissions()", expected.getPermissions(),
					actual.getPermissions());
		} else {
			assertNotNull("getPermissions()", actual.getPermissions());
		}
		if (completeExpBean || expected.getDescription() != null) {
			assertEquals("getDescription()", expected.getDescription(),
					actual.getDescription());
		} else {
			assertNotNull("getDescription()", actual.getDescription());
		}
		assertEquals("getUrl()", expected.getUrl(), actual.getUrl());
		assertEquals("getImage()", expected.getImage(), actual.getImage());
		assertEquals("getPackageName()", expected.getPackageName(),
				actual.getPackageName());
		GeoScopeManagementTest.compareGeoScope(expected.getGeographicalScope(),
				actual.getGeographicalScope(), completeExpBean);
		assertEquals(expected.getRelatedCouncilId(),
				actual.getRelatedCouncilId());
		assertEquals("getGooglePlayRating()", expected.getGooglePlayRating(),
				actual.getGooglePlayRating());
		if (completeExpBean || expected.getVersion() != null) {
			assertEquals("getVersion()", expected.getVersion(),
					actual.getVersion());
		} else {
			assertNotNull("getVersion()", actual.getVersion());
		}
		if (completeExpBean || expected.getTermsOfService() != null) {
			assertEquals("getTermsOfService()", expected.getTermsOfService(),
					actual.getTermsOfService());
		} else {
			assertNotNull("getTermsOfService()", actual.getTermsOfService());
		}
		if (completeExpBean || expected.getAccessNumber() != null) {
			assertEquals("getAccessNumber()", expected.getAccessNumber(),
					actual.getAccessNumber());
		} else {
			assertEquals("getAccessNumber()", actual.getAccessNumber(),
					Integer.valueOf(0));
		}
		if (completeExpBean || expected.getDownloadNumber() != null) {
			assertEquals("getDownloadNumber()", expected.getDownloadNumber(),
					actual.getDownloadNumber());
		} else {
			assertEquals("getDownloadNumber()", actual.getDownloadNumber(),
					Integer.valueOf(0));
		}
		if (completeExpBean || expected.getTags() != null) {
			assertArrayEquals("getTags()", expected.getTags().toArray(), actual
					.getTags().toArray());
		} else {
			assertNotNull("getTags()", actual.getTags());
		}
	}

}
