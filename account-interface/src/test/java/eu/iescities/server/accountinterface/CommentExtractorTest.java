package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import eu.iescities.server.accountinterface.CommentExtractor;
import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Language;

public class CommentExtractorTest {

	private final static GregorianCalendar gc = new GregorianCalendar(2014, 01, 15);
	private static String adminSession;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 */
	@Before
	public void setUp() {
		adminSession = TestUtils.loginAdminUser();
	}
	
	@Test
	public void testGenerateDateObject() {
		try {
			final Date date = CommentExtractor.generateDateObject("February 15, 2014", null);
			// Months numbers start in 0, so February is indeed month #1
			assertEquals(date, new Date());
		} 
		catch (final ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateDateObjectEnglish() {
		try {
			final Date date = CommentExtractor.generateDateObject("February 15, 2014", Language.EN);
			// Months numbers start in 0, so February is indeed month #1
			assertEquals(date, gc.getTime());
		} 
		catch (final ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateDateObjectSpanish() {
		try {
			final Date date = CommentExtractor.generateDateObject("15 de febrero de 2014", Language.ES);
			// Months numbers start in 0, so February is indeed month #1
			assertEquals(date, gc.getTime());
		} 
		catch (final ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateDateObjectItalian() {
		try {
			final Date date = CommentExtractor.generateDateObject("15 febbraio 2014", Language.IT);
			// Months numbers start in 0, so February is indeed month #1
			assertEquals(date, gc.getTime());
		} 
		catch (final ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testTrimFullReviewText() {
		final String origComment = "test comment";
		final String comment = CommentExtractor.trimFullReviewText(origComment, null);
		assertEquals(comment, "");
	}

	@Test
	public void testTrimFullReviewTextEnglish() {
		final String origComment = "test comment Full Review";
		final String comment = CommentExtractor.trimFullReviewText(origComment, Language.EN);
		assertEquals(false, comment.contains("Full Review"));
	}
	
	@Test
	public void testTrimFullReviewTextEnglishElse() {
		final String origComment = " test comment  ";
		final String comment = CommentExtractor.trimFullReviewText(origComment, Language.EN);
		assertEquals(comment, "test comment");
	}

	@Test
	public void testTrimFullReviewTextSpanish() {
		final String origComment = "test comment Opinión completa";
		final String comment = CommentExtractor.trimFullReviewText(origComment, Language.ES);
		assertEquals(false, comment.contains("Opinión completa"));
	}

	@Test
	public void testTrimFullReviewTextItalian() {
		final String origComment = "test comment Recensione completa";
		final String comment = CommentExtractor.trimFullReviewText(origComment, Language.IT);
		assertEquals(false, comment.contains("Recensione completa"));
	}
	
	@Test
	@Ignore
	public void testExtractCommentsFromGooglePlay_VLC() {
		final String packageName = "org.videolan.vlc.betav7neon";
		
		Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("VLC for Android Beta");
		tempApp.setDescription("VLC media player is a free and open source cross-platform multimedia player that plays most multimedia files as well as discs, devices, and network streaming protocols."
				+ "This is a BETA version of the port of VLC media player to the Android™ platform. It is intended for power users and hackers. This version is not perfectly stable and is slower than the final version."
				+ "It is still a beta, so it might kill your kitten or destroy your house, but it should be fine now."
				+ "Use it at your own risk. Have fun! :)");
		tempApp.setUrl("https://videolan.org/");
		tempApp.setImage("https://images.videolan.org/images/largeVLC.png");
		tempApp.setVersion("0.1.4");
		tempApp.setTermsOfService("");
		tempApp.setPermissions("• It needs hardware control, in order to change the audio volume;"
				+ "• It needs phone calls control, in order to pause the music when someone is calling;"
				+ "• It needs full Internet access to open network streams;"
				+ "• It needs to be able to read logs, during the beta version, to help understanding the crashes and issues;"
				+ "• It needs to get access to the SD card storage, in order to allow deletion of files."
				+ "• It needs to get access to the settings, in order to change your audio ringtone.");
		tempApp.setPackageName(packageName);
		tempApp.setTags(new HashSet<String>(Arrays.asList("VLC", "VideoLAN", "MediaPlayer")));
		tempApp = ApplicationManagement.registerApp(adminSession, tempApp);

		// each call should retrieve 20 comments (if this changes, the following needs to be adapted)
		final int nrExpectedComments = 40;
		CommentExtractor.extractCommentsFromGooglePlay(packageName, Language.EN);

		List<AppRating> allRatings = ApplicationManagement.getAllRatingsOfApp(
				tempApp.getAppId(), 0, 100);
		assertEquals(nrExpectedComments, allRatings.size());
		
		// retrieve a second time - the number of comments should not change
		CommentExtractor.extractCommentsFromGooglePlay(packageName, Language.EN);

		allRatings = ApplicationManagement.getAllRatingsOfApp(
				tempApp.getAppId(), 0, 100);
		assertEquals(nrExpectedComments, allRatings.size());
	}
	
	@Test
	@Ignore
	public void testCommentRetrieval() {
		final String packageName = "com.facebook.katana";
		
		Application tempApp = new Application();
		tempApp.setLang(Language.EN);
		tempApp.setName("Facebook");
		tempApp.setPackageName(packageName);
		tempApp = ApplicationManagement.registerApp(adminSession, tempApp);
		
		CommentExtractor.extractCommentsFromGooglePlay(packageName, Language.EN);
		CommentExtractor.extractCommentsFromGooglePlay(packageName, Language.ES);
		CommentExtractor.extractCommentsFromGooglePlay(packageName, Language.IT);
		
		final List<AppRating> allRatings = ApplicationManagement.getAllRatingsOfApp(tempApp.getAppId(), 0, 100);
		
		int counterEN = 0;
		int counterES = 0;
		int counterIT = 0;
		
		for(final AppRating appRating : allRatings) {
			if (appRating.getLang() == Language.EN) {
				counterEN++;
			}
			else if (appRating.getLang() == Language.ES) {
				counterES++;
			}
			else if (appRating.getLang() == Language.IT) {
				counterIT++;
			}
		}
		
		assertTrue("No english comments for Facebook application", counterEN > 1);
		assertTrue("No spanish comments for Facebook application", counterES > 1);
		assertTrue("No italian comments for Facebook application", counterIT > 1);
	}
	
	@Test
	public void testExtractOverallRatingFromGooglePlay_ViaggiaRovereto() {
		final String packageName = "eu.trentorise.smartcampus.viaggiarovereto";		
		
		Application tempApp = new Application();
		tempApp.setLang(Language.IT);
		tempApp.setName("ViaggiaRovereto");
		tempApp.setDescription("ViaggiaRovereto è l'applicazione che soddisfa le tue esigenze"
				+ "di mobilita' sostenibile a Rovereto e dintorni!");
		tempApp.setUrl("https://iescities.eu/");
		tempApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-rovereto-viaggia-logo-42432892-0_avatar.png");
		tempApp.setVersion("0.9.12");
		tempApp.setTermsOfService("");
		tempApp.setPermissions("");
		tempApp.setPackageName(packageName);
		tempApp.setTags(new HashSet<String>(Arrays.asList("Rovereto", "IESCities")));
		tempApp = ApplicationManagement.registerApp(adminSession, tempApp);
		
		CommentExtractor.extractRatingFromGooglePlay(packageName);

		// retrieve app drom DB again, verify rating:
		tempApp = ApplicationManagement.getAppById(tempApp.getAppId(), Language.IT, null);
		assertTrue("Rating value is below 0", tempApp.getGooglePlayRating() >= 0);
		assertTrue("Rating value is above 5", tempApp.getGooglePlayRating() <= 5);
	}
	
	@Test(expected=NullPointerException.class)
	public void testExtractCommentsFromGooglePlayUnknown() {
		CommentExtractor.extractCommentsFromGooglePlay("ñ");
	}
	
	@Test(expected=NullPointerException.class)
	public void testExtractRatingsFromGooglePlayUnknown() {
		CommentExtractor.extractRatingFromGooglePlay("ñ");
	}
}
