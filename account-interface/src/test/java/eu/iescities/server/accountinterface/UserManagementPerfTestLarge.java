package eu.iescities.server.accountinterface;

import org.databene.contiperf.PerfTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Unit tests for {@link UserManagement}.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class UserManagementPerfTestLarge extends UserManagementPerfTest {
	final private static int tempUserCount = 10000;
	final private static int validSessionCount = 5000;
	final private static int invocations = 10000;
	final private static int threads = 1000;
	
	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		UserManagementPerfTest.setupDB(tempUserCount, validSessionCount, 10);
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testUpdateUser1() {
		super.testUpdateUser1();
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testUpdateUser1a() {
		super.testUpdateUser1a();
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(String)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetUserBySession1() {
		super.testGetUserBySession1();
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetUserById() {
		super.testGetUserById();
	}

	/**
	 * Tests {@link UserManagement#getInstalledApps(String, Language, Language)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetInstalledApps1() {
		super.testGetInstalledApps1();
	}

	/**
	 * Tests {@link UserManagement#getDevelopedApps(String, Language, Language)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetDevelopedApps() {
		super.testGetDevelopedApps();
	}

	/**
	 * Tests {@link UserManagement#getUsedDataSets(String, int, int, Language, Language)} with a
	 * limit of 10, offset 0.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetUsedDataSets1() {
		super.testGetUsedDataSets1();
	}

	/**
	 * Tests
	 * {@link UserManagement#getPublishedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 0.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetPublishedDataSets1() {
		super.testGetPublishedDataSets1();
	}

	/**
	 * Tests {@link UserManagement#validateUserSession(String)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testValidateUserSession1() {
		super.testValidateUserSession1();
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)}.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testLogin1() {
		super.testLogin1();
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 10, offset 1.
	 */
	@Test
	@PerfTest(invocations = invocations, threads = threads)
	@Override
	public void testGetAllUsers3() {
		super.testGetAllUsers3();
	}

}
