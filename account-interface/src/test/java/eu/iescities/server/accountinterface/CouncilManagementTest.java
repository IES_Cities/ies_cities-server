package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Unit tests for {@link CouncilManagement}.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
public class CouncilManagementTest {
	protected static final String tempCouncilName = "tempCouncil";

	private PersistenceManager pm;

	private String adminSessionId;
	private String sessionId;
	private String unauthorizedSessionId;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		pm = PMF.getPM();

		adminSessionId = TestUtils.loginAdminUser();
		sessionId = UserManagement.login("mike", "secret");
		unauthorizedSessionId = UserManagement.login("john", "secret");
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		final Query query = pm.newQuery(CouncilManagement.queryCouncilByName);
		query.deletePersistentAll(tempCouncilName);
		final Query usrQuery = pm.newQuery(UserManagement.queryUserByName);
		usrQuery.deletePersistentAll(UserManagementTest.tempUserName);
		usrQuery.deletePersistentAll(UserManagementTest.tempUserName2);
		try {
			UserManagement.logout(adminSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(sessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(unauthorizedSessionId);
		} catch (final Exception e) {
		}
		pm.close();
	}

	private Council addDefaultTempCouncil() throws SecurityException,
			IllegalArgumentException {
		final int userId = UserManagement.registerUser(
				UserManagementTest.getDefaultTempUser()).getUserId();
		return addCouncil(tempCouncilName, "temp council description",
				TestUtils.honululuCouncil.getGeographicalScope(), userId);
	}

	private Council addCouncil(final String name, final String description,
			final GeoScope geoScope, final int councilAdminId)
			throws SecurityException, IllegalArgumentException {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(name);
		tempCouncil.setDescription(description);
		tempCouncil.setGeographicalScope(geoScope);

		final Council council = CouncilManagement.createCouncil(adminSessionId,
				tempCouncil);
		CouncilManagement.addCouncilAdmin(adminSessionId,
				council.getCouncilId(), councilAdminId);
		return council;
	}

	/**
	 * Tests {@link CouncilTranslation#toBean()} and verifies all getters.
	 */
	@Test
	public void testToBean() {
		final CouncilTranslation council1EN = CouncilManagement
				.getCouncilTranslationByLang(pm,
						TestUtils.hawaiiCouncil.getCouncilId(), Language.EN,
						null);
		compareCouncil(TestUtils.hawaiiCouncil, council1EN, true);
		
		final eu.iescities.server.accountinterface.Council council1 = CouncilManagement
				.getCouncilById(pm, TestUtils.hawaiiCouncil.getCouncilId());
		compareCouncil(TestUtils.hawaiiCouncil, council1, true);
		// now test with all getters of the two objects:
		assertEquals(TestUtils.hawaiiCouncil.getCouncilId(), council1.getCouncilId());
		assertEquals(TestUtils.hawaiiCouncil.getName(), council1EN.getName());
		assertEquals(TestUtils.hawaiiCouncil.getDescription(), council1EN.getDescription());
		GeoScopeManagementTest.compareGeoScope(TestUtils.hawaiiCouncil.getGeographicalScope(),
				council1.getGeographicalScope(), true);
	}

	/**
	 * Tests {@link CouncilManagement#verifyCouncilId(int)}.
	 */
	@Test
	public void testVerifyCouncilId() {
		assertTrue(CouncilManagement.verifyCouncilId(TestUtils.hawaiiCouncil.getCouncilId()));
		assertTrue(CouncilManagement.verifyCouncilId(TestUtils.honululuCouncil.getCouncilId()));
		assertFalse(CouncilManagement.verifyCouncilId(-1));
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)}.
	 */
	@Test
	public void testCreateCouncil1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		final Council council = CouncilManagement.createCouncil(adminSessionId,
				tempCouncil);
		assertTrue(CouncilManagement.verifyCouncilId(council.getCouncilId()));
		compareCouncil(tempCouncil, council, false);
		compareCouncil(council, CouncilManagement.getCouncilById(
				council.getCouncilId(), Language.EN, null), true);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} using a
	 * geoscope with only the ID set.
	 */
	@Test
	public void testCreateCouncil2() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		final GeoScope scope = new GeoScope();
		scope.setId(TestUtils.honululuCouncil.getGeographicalScope().getId());
		tempCouncil.setGeographicalScope(scope);
		final Council council = CouncilManagement.createCouncil(adminSessionId,
				tempCouncil);
		assertTrue(CouncilManagement.verifyCouncilId(council.getCouncilId()));
		compareCouncil(tempCouncil, council, false);
		compareCouncil(council, CouncilManagement.getCouncilById(
				council.getCouncilId(), Language.EN, null), true);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} without a
	 * geoscope.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilNoGeoScope1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} without a
	 * description.
	 */
	@Test
	public void testCreateCouncilNoDescription1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} creating
	 * the same council twice.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilTwice() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		final Council registeredCouncil = CouncilManagement
				.createCouncil(adminSessionId, tempCouncil);
		assertTrue(CouncilManagement
				.verifyCouncilId(registeredCouncil.getCouncilId()));
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} without a
	 * name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilNoName1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with an
	 * empty name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilEmptyName1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName("");
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with a
	 * too-long name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilInvalid1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(TestUtils.randomString(1000));
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with a
	 * too-long description string.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilInvalid2() {
		final char[] descr = new char[100000];
		Arrays.fill(descr, 0, descr.length, 'c');
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription(new String(descr));
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with a
	 * too-long description string.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCouncilInvalid3() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(null);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(adminSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with an
	 * unauthorised session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testCreateCouncilUnauthorised1() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(unauthorizedSessionId, tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#createCouncil(String, Council)} with a
	 * random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testCreateCouncilUnauthorised2() {
		final Council tempCouncil = new Council();
		tempCouncil.setLang(Language.EN);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		CouncilManagement.createCouncil(UUID.randomUUID().toString(),
				tempCouncil);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilById(int, Language, Language)}.
	 */
	@Test
	public void testGetCouncilById1a() {
		Council council = CouncilManagement.getCouncilById(
				TestUtils.honululuCouncil.getCouncilId(), Language.EN, null);
		compareCouncil(TestUtils.honululuCouncil, council, true);

		council = CouncilManagement.getCouncilById(
				TestUtils.hawaiiCouncil.getCouncilId(), Language.EN, null);
		compareCouncil(TestUtils.hawaiiCouncil, council, true);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilById(int, Language, Language)}
	 * with a non-existing translation and a valid fallback.
	 */
	@Test
	public void testGetCouncilById1b() {
		Council council = CouncilManagement.getCouncilById(
				TestUtils.honululuCouncil.getCouncilId(), Language.ES,
				Language.EN);
		compareCouncil(TestUtils.honululuCouncil, council, true);

		council = CouncilManagement.getCouncilById(
				TestUtils.hawaiiCouncil.getCouncilId(), Language.ES,
				Language.EN);
		compareCouncil(TestUtils.hawaiiCouncil, council, true);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilById(int, Language, Language)}
	 * with a non-existing translation and no fallback.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilById2() {
		CouncilManagement.getCouncilById(
				TestUtils.honululuCouncil.getCouncilId(), Language.ES, null);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilById(int, Language, Language)}
	 * with a non-existing translation and a non-existing fall-back.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilById4() {
		CouncilManagement.getCouncilById(
				TestUtils.honululuCouncil.getCouncilId(), Language.ES,
				Language.IT);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilById(int, Language, Language)}
	 * with a non-existing council ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilByIdNonExisting1() {
		CouncilManagement.getCouncilById(-1, Language.EN, null);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncil(String, int)} from an admin
	 * session.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveCouncil1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();

		assertEquals(1, CouncilManagement.removeCouncil(adminSessionId, councilId));
		final Query query = pm.newQuery(CouncilManagement.queryCouncilByName);
		assertArrayEquals(
				new eu.iescities.server.accountinterface.Council[] {},
				((List<eu.iescities.server.accountinterface.Council>) query
						.execute(tempCouncilName)).toArray());
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncil(String, int)} from the
	 * council admins session.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveCouncil2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.removeCouncil(tempUserSession, councilId);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncil(String, int)} from an
	 * unauthorised session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveCouncil3() {
		final int councilId = addDefaultTempCouncil().getCouncilId();

		CouncilManagement.removeCouncil(unauthorizedSessionId, councilId);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncil(String, int)} from a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveCouncil4() {
		final int councilId = addDefaultTempCouncil().getCouncilId();

		CouncilManagement.removeCouncil(UUID.randomUUID().toString(), councilId);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncil(String, int)} from an admin
	 * session using a non-existing application ID.
	 */
	@Test
	public void testRemoveCouncil5() {
		assertEquals(0, CouncilManagement.removeCouncil(adminSessionId, -1));
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilDataSets(int, int, int, Language, Language)} with a
	 * limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilDataSets1a() {
		List<Dataset> datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.honululuCouncil.getCouncilId(), 0, 1, Language.EN,
				null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);

		datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.hawaiiCouncil.getCouncilId(), 0, 1, Language.EN,
				null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilDataSets(int, int, int, Language, Language)} with a
	 * limit of 10, offset 0.
	 */
	@Test
	public void testGetCouncilDataSets1b() {
		List<Dataset> datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.honululuCouncil.getCouncilId(), 0, 10, Language.EN,
				null);
		assertEquals(2, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);
		DatasetManagementTest.compareDataset(TestUtils.dataset2,
				datasets.get(1), true);

		datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.hawaiiCouncil.getCouncilId(), 0, 10, Language.EN,
				null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilDataSets(int, int, int, Language, Language)} with a
	 * limit of 10, offset 1.
	 */
	@Test
	public void testGetCouncilDataSets1c() {
		List<Dataset> datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.honululuCouncil.getCouncilId(), 1, 10, Language.EN,
				null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset2,
				datasets.get(0), true);

		datasets = CouncilManagement.getCouncilDataSets(
				TestUtils.hawaiiCouncil.getCouncilId(), 1, 10, Language.EN,
				null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilDataSets(int, int, int, Language, Language)} with a
	 * non-existing council ID and a limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilDataSetsNonExisting() {
		final List<Dataset> datasets = CouncilManagement.getCouncilDataSets(-1,
				0, 1, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilDataSets(int, int, int, Language, Language)} with a limit
	 * of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilDataSetsTooMany() {
		CouncilManagement.getCouncilDataSets(
				TestUtils.honululuCouncil.getCouncilId(), 2, 10000, Language.EN,
				null);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilAdmins(int, int, int)} with a
	 * limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilAdmins1() {
		final List<User> users = CouncilManagement.getCouncilAdmins(
				TestUtils.honululuCouncil.getCouncilId(), 0, 1);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.mikeUser, TestUtils.mikeUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilAdmins(int, int, int)} with a
	 * limit of 2, offset 0.
	 */
	@Test
	public void testGetCouncilAdmins2() {
		final List<User> users = CouncilManagement.getCouncilAdmins(
				TestUtils.honululuCouncil.getCouncilId(), 0, 2);
		assertEquals(2, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.mikeUser, TestUtils.mikeUser.isDeveloper());
		UserManagementTest.isPrivateUser(users.get(1),
				TestUtils.mike2User, TestUtils.mike2User.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilAdmins(int, int, int)} with a
	 * limit of 10, offset 2.
	 */
	@Test
	public void testGetCouncilAdmins3() {
		final List<User> users = CouncilManagement.getCouncilAdmins(
				TestUtils.honululuCouncil.getCouncilId(), 2, 10);
		assertEquals(0, users.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilAdmins(int, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilAdmins1TooMany() {
		CouncilManagement.getCouncilAdmins(
				TestUtils.honululuCouncil.getCouncilId(), 0, 10000);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilUsersCouncilAdmin1() {
		final List<User> users = CouncilManagement.getCouncilUsers(sessionId,
				0, 1);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.bobUser, TestUtils.bobUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 2, offset 0.
	 */
	@Test
	public void testGetCouncilUsersCouncilAdmin2() {
		final List<User> users = CouncilManagement.getCouncilUsers(sessionId,
				0, 2);
		assertEquals(2, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.bobUser, TestUtils.bobUser.isDeveloper());
		UserManagementTest.isPrivateUser(users.get(1),
				TestUtils.johnUser, TestUtils.johnUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 10, offset 1.
	 */
	@Test
	public void testGetCouncilUsersCouncilAdmin3() {
		final List<User> users = CouncilManagement.getCouncilUsers(sessionId,
				1, 10);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.johnUser, TestUtils.johnUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilUsersCouncilAdmin1TooMany() {
		CouncilManagement.getCouncilUsers(sessionId, 0, 10000);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersCouncilAdmin1Unauthorized() {
		CouncilManagement.getCouncilUsers(unauthorizedSessionId, 0, 10);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersCouncilAdmin2Unauthorized() {
		CouncilManagement.getCouncilUsers(adminSessionId, 0, 10);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersCouncilAdmin1UnauthorizedTooMany() {
		CouncilManagement.getCouncilUsers(unauthorizedSessionId, 0, 10000);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilUsersAdmin1() {
		final List<User> users = CouncilManagement
				.getCouncilUsers(adminSessionId,
						TestUtils.honululuCouncil.getCouncilId(), 0, 1);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.bobUser, TestUtils.bobUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 2, offset 0.
	 */
	@Test
	public void testGetCouncilUsersAdmin2() {
		final List<User> users = CouncilManagement
				.getCouncilUsers(adminSessionId,
						TestUtils.honululuCouncil.getCouncilId(), 0, 2);
		assertEquals(2, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.bobUser, TestUtils.bobUser.isDeveloper());
		UserManagementTest.isPrivateUser(users.get(1),
				TestUtils.johnUser, TestUtils.johnUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10, offset 1.
	 */
	@Test
	public void testGetCouncilUsersAdmin3() {
		final List<User> users = CouncilManagement
				.getCouncilUsers(adminSessionId,
						TestUtils.honululuCouncil.getCouncilId(), 1, 10);
		assertEquals(1, users.size());
		UserManagementTest.isPrivateUser(users.get(0),
				TestUtils.johnUser, TestUtils.johnUser.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilUsersAdmin1TooMany() {
		CouncilManagement.getCouncilUsers(adminSessionId,
				TestUtils.honululuCouncil.getCouncilId(), 0, 10000);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10, offset 0 from a user session.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersAdmin1Unauthorized() {
		CouncilManagement.getCouncilUsers(unauthorizedSessionId,
				TestUtils.honululuCouncil.getCouncilId(), 0, 10);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10, offset 0 from a council session to its administrated council.
	 */
	@Test
	public void testGetCouncilUsersAdmin2Unauthorized() {
		CouncilManagement.getCouncilUsers(sessionId,
				TestUtils.honululuCouncil.getCouncilId(), 0, 10);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10, offset 0 from a council session to another council.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersAdmin3Unauthorized() {
		CouncilManagement.getCouncilUsers(sessionId,
				TestUtils.hawaiiCouncil.getCouncilId(), 0, 10);
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilUsers(String, int, int, int)} with a
	 * limit of 10000, offset 0.
	 */
	@Test(expected = SecurityException.class)
	public void testGetCouncilUsersAdmin1UnauthorizedTooMany() {
		CouncilManagement.getCouncilUsers(sessionId,
				TestUtils.hawaiiCouncil.getCouncilId(), 0, 10000);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getCouncilApps(int, int, int, Language, Language)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetCouncilApps1() {
		List<Application> apps = CouncilManagement.getCouncilApps(
				TestUtils.honululuCouncil.getCouncilId(), 0, 1, Language.EN,
				null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.get(0), true);

		apps = CouncilManagement
				.getCouncilApps(TestUtils.hawaiiCouncil.getCouncilId(), 0, 1,
						Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests {@link CouncilManagement#getCouncilApps(int, int, int, Language, Language)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetCouncilApps2() {
		List<Application> apps = CouncilManagement.getCouncilApps(
				TestUtils.honululuCouncil.getCouncilId(), 0, 2, Language.EN,
				null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.get(0), true);

		apps = CouncilManagement
				.getCouncilApps(TestUtils.hawaiiCouncil.getCouncilId(), 0, 1,
						Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getCouncilApps(int, int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetCouncilApps3() {
		final List<Application> apps = CouncilManagement.getCouncilApps(
				TestUtils.app1.getAppId(), 1, 10, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getCouncilApps(int, int, int, Language, Language)}
	 * with a non-existing council ID and a limit of 10, offset 1.
	 */
	@Test
	public void testGetCouncilAppsNonExisting1() {
		final List<Application> apps = CouncilManagement.getCouncilApps(-1, 1,
				10, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getCouncilApps(int, int, int, Language, Language)}
	 * with a limit of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCouncilAppsTooMany() {
		CouncilManagement.getCouncilApps(TestUtils.app1.getAppId(), 2, 10000,
				Language.EN, null);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAuthorizedCouncil(PersistenceManager, String)}
	 * .
	 */
	@Test
	public void testGetAuthorizedCouncil1() {
		final eu.iescities.server.accountinterface.Council council = CouncilManagement
				.getAuthorizedCouncil(pm, sessionId);
		compareCouncil(TestUtils.honululuCouncil, council, true);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAuthorizedCouncil(PersistenceManager, String)}
	 * with an unauthorised session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetAuthorizedCouncil2() {
		CouncilManagement.getAuthorizedCouncil(pm, unauthorizedSessionId);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAuthorizedCouncil(PersistenceManager, String)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetAuthorizedCouncil3() {
		CouncilManagement
				.getAuthorizedCouncil(pm, UUID.randomUUID().toString());
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 1, offset 0.
	 */
	@Test
	public void testGetAllCouncils1() {
		final List<Council> councils = CouncilManagement.getAllCouncils(0, 1,
				Language.EN, null);
		assertEquals(1, councils.size());
		compareCouncil(TestUtils.honululuCouncil, councils.get(0), true);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 2, offset 0.
	 */
	@Test
	public void testGetAllCouncils2() {
		final List<Council> councils = CouncilManagement.getAllCouncils(0, 2,
				Language.EN, null);
		assertEquals(2, councils.size());
		compareCouncil(TestUtils.honululuCouncil, councils.get(0), true);
		compareCouncil(TestUtils.hawaiiCouncil, councils.get(1), true);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetAllCouncils3() {
		final List<Council> councils = CouncilManagement.getAllCouncils(1, 10,
				Language.EN, null);
		assertEquals(1, councils.size());
		compareCouncil(TestUtils.hawaiiCouncil, councils.get(0), true);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllCouncilsTooMany() {
		CouncilManagement.getAllCouncils(2, 10000, Language.EN, null);
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 10, offset 0 and non-translated councils.
	 */
	@Test
	public void testGetAllCouncilsL10n1() {
		final List<Council> councils = CouncilManagement.getAllCouncils(0, 10,
				Language.ES, null);
		assertEquals(0, councils.size());
	}

	/**
	 * Tests
	 * {@link CouncilManagement#getAllCouncils(int, int, Language, Language)}
	 * with a limit of 10, offset 0 and councils in two languages, retrieving
	 * all of them.
	 */
	@Test
	public void testGetAllCouncilsL10n2() {
		Council tempCouncil = new Council();
		tempCouncil.setLang(Language.ES);
		tempCouncil.setName(tempCouncilName);
		tempCouncil.setDescription("temp council description");
		tempCouncil.setGeographicalScope(TestUtils.honululuCouncil
				.getGeographicalScope());
		tempCouncil = CouncilManagement.createCouncil(adminSessionId,
				tempCouncil);
		
		final List<Council> councils = CouncilManagement.getAllCouncils(0, 10,
				Language.EN, Language.ES);
		assertEquals(3, councils.size());
		compareCouncil(TestUtils.honululuCouncil, councils.get(0), true);
		compareCouncil(TestUtils.hawaiiCouncil, councils.get(1), true);
		compareCouncil(tempCouncil, councils.get(2), true);
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * a council admin session.
	 */
	@Test
	public void testAddCouncilAdmin1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		User tmpUser2 = UserManagementTest.getDefaultTempUser();
		tmpUser2.setUsername(UserManagementTest.tempUserName2);
		tmpUser2 = UserManagement.registerUser(tmpUser2);
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final User tmpUser = UserManagement.getUserBySession(tempUserSession);

		CouncilManagement.addCouncilAdmin(tempUserSession, councilId, tmpUser2.getUserId());
		final List<User> councilAdmins = CouncilManagement.getCouncilAdmins(councilId, 0, 10);
		
		assertEquals(2, councilAdmins.size());
		UserManagementTest.isPrivateUser(councilAdmins.get(0),
				tmpUser, tmpUser.isDeveloper());
		UserManagementTest.isPrivateUser(councilAdmins.get(1),
				tmpUser2, tmpUser2.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * an IES Cities admin session.
	 */
	@Test
	public void testAddCouncilAdmin2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		User tmpUser2 = UserManagementTest.getDefaultTempUser();
		tmpUser2.setUsername(UserManagementTest.tempUserName2);
		tmpUser2 = UserManagement.registerUser(tmpUser2);
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final User tmpUser = UserManagement.getUserBySession(tempUserSession);

		CouncilManagement.addCouncilAdmin(adminSessionId, councilId, tmpUser2.getUserId());
		final List<User> councilAdmins = CouncilManagement.getCouncilAdmins(councilId, 0, 10);
		
		assertEquals(2, councilAdmins.size());
		UserManagementTest.isPrivateUser(councilAdmins.get(0),
				tmpUser, tmpUser.isDeveloper());
		UserManagementTest.isPrivateUser(councilAdmins.get(1),
				tmpUser2, tmpUser2.isDeveloper());
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * an unauthorised session.
	 */
	@Test(expected = SecurityException.class)
	public void testAddCouncilAdminUnauthorized1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		User tmpUser2 = UserManagementTest.getDefaultTempUser();
		tmpUser2.setUsername(UserManagementTest.tempUserName2);
		tmpUser2 = UserManagement.registerUser(tmpUser2);

		CouncilManagement.addCouncilAdmin(sessionId, councilId, tmpUser2.getUserId());
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddCouncilAdminUnauthorized2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		User tmpUser2 = UserManagementTest.getDefaultTempUser();
		tmpUser2.setUsername(UserManagementTest.tempUserName2);
		tmpUser2 = UserManagement.registerUser(tmpUser2);

		CouncilManagement.addCouncilAdmin(UUID.randomUUID().toString(),
				councilId, tmpUser2.getUserId());
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * a council admin session with a non-existing user.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddCouncilAdminNonExisting() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.addCouncilAdmin(tempUserSession, councilId, -1);
	}

	/**
	 * Tests {@link CouncilManagement#addCouncilAdmin(String, int, int)} from
	 * a council admin session with a user who already manages a council.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddCouncilAdminInvalid1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.addCouncilAdmin(tempUserSession, councilId,
				TestUtils.mikeUser.getUserId());
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * a council admin session.
	 */
	@Test
	public void testRemoveCouncilAdmin1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final int councilAdminId = UserManagement.getUserBySession(
				tempUserSession).getUserId();

		CouncilManagement.removeCouncilAdmin(tempUserSession, councilId,
				councilAdminId);
		assertEquals(0, CouncilManagement.getCouncilAdmins(councilId, 0, 10)
				.size());
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * an IES Cities admin session.
	 */
	@Test
	public void testRemoveCouncilAdmin2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final int councilAdminId = UserManagement.getUserBySession(
				tempUserSession).getUserId();

		CouncilManagement.removeCouncilAdmin(adminSessionId, councilId,
				councilAdminId);
		assertArrayEquals(new User[0], CouncilManagement.getCouncilAdmins(councilId, 0, 10)
				.toArray());
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * an unauthorised session.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveCouncilAdminUnauthorized1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final int councilAdminId = UserManagement.getUserBySession(
				tempUserSession).getUserId();

		CouncilManagement.removeCouncilAdmin(sessionId, councilId,
				councilAdminId);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveCouncilAdminUnauthorized2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);
		final int councilAdminId = UserManagement.getUserBySession(
				tempUserSession).getUserId();

		CouncilManagement.removeCouncilAdmin(UUID.randomUUID().toString(),
				councilId, councilAdminId);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * a council admin's session with a non-existing user.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveCouncilAdminNonExisting() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.removeCouncilAdmin(tempUserSession, councilId,
				-1);
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * a council admin session with a user who manages another council.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveCouncilAdminInvalid1() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.removeCouncilAdmin(tempUserSession, councilId,
				TestUtils.mikeUser.getUserId());
	}

	/**
	 * Tests {@link CouncilManagement#removeCouncilAdmin(String, int, int)} from
	 * a council admin session with a user who manages no council.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveCouncilAdminInvalid2() {
		final int councilId = addDefaultTempCouncil().getCouncilId();
		final String tempUserSession = UserManagement.login(
				UserManagementTest.tempUserName,
				UserManagementTest.tempUserPassword);

		CouncilManagement.removeCouncilAdmin(tempUserSession, councilId,
				TestUtils.bobUser.getUserId());
	}

	/**
	 * Compares the given two councils and verifies that all their properties
	 * are equal.
	 * 
	 * @param expected
	 *            the expected council
	 * @param actual
	 *            the actual council
	 * @param completeExpBean
	 *            whether the expected council bean is complete or not
	 */
	protected static void compareCouncil(final Council expected,
			final CouncilTranslation actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		compareCouncil(expected, actual.toBean(), completeExpBean);
	}

	/**
	 * Compares the given two councils and verifies that all their properties
	 * are equal (only useful for testing and verifying internal, i.e.
	 * protected, methods).
	 * 
	 * @param expected
	 *            the expected council
	 * @param actual
	 *            the actual council
	 * @param completeExpBean
	 *            whether the expected council bean is complete or not
	 */
	protected static void compareCouncil(final Council expected,
			final eu.iescities.server.accountinterface.Council actual,
			final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getCouncilId() != -1) {
			assertEquals("getCouncilId()", expected.getCouncilId(),
					actual.getCouncilId());
		}
		GeoScopeManagementTest.compareGeoScope(expected.getGeographicalScope(),
				actual.getGeographicalScope(), completeExpBean);
	}

	/**
	 * Compares the given two councils and verifies that all their properties
	 * are equal.
	 * 
	 * @param expected
	 *            the expected council
	 * @param actual
	 *            the actual council
	 * @param completeExpBean
	 *            whether the expected council bean is complete or not
	 */
	protected static void compareCouncil(final Council expected,
			final Council actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getCouncilId() != -1) {
			assertEquals("getCouncilId()", expected.getCouncilId(),
					actual.getCouncilId());
		}
		assertEquals("getName()", expected.getName(), actual.getName());
		if (completeExpBean || expected.getDescription() != null) {
			assertEquals("getDescription()", expected.getDescription(),
					actual.getDescription());
		} else {
			assertNotNull("getDescription()", actual.getDescription());
		}
		GeoScopeManagementTest.compareGeoScope(expected.getGeographicalScope(),
				actual.getGeographicalScope(), completeExpBean);
	}

}
