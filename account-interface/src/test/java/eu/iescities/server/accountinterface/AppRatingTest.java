package eu.iescities.server.accountinterface;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppRatingTest {
	private static String longerThan255CharsString;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
		longerThan255CharsString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
				+ "Aliquam malesuada urna nunc, a pharetra lacus molestie ut. Nunc libero tellus, "
				+ "condimentum eu nisi dapibus, fringilla maximus ante. Proin tempor, elit sit amet "
				+ "pulvinar placerat, diam turpis ultrices felis, a fermentum ligula quam ac volutpat.";
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSetLowRatingValue() {
		AppRating appRating = new AppRating();
		appRating.setRating(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testHighLowRatingValue() {
		AppRating appRating = new AppRating();
		appRating.setRating(6);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testLongRatingTitleValue() {
		AppRating appRating = new AppRating();
		appRating.setTitle(longerThan255CharsString);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testLongRatingUsernameValue() {
		AppRating appRating = new AppRating();
		appRating.setUserName(longerThan255CharsString);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyRatingUsernameValue() {
		AppRating appRating = new AppRating();
		appRating.setUserName("");
	}
}
