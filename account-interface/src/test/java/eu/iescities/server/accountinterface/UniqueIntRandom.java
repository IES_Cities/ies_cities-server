package eu.iescities.server.accountinterface;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Helper class for to generate random Integers 
 * without duplicates.
 * 
 * @author Jan Skrzypczak, skrzypczak@zib.de
 */
public class UniqueIntRandom {

	private static final Random rand = new Random();

    // Dummy value to associate with an Object in the backing Map
    private static final Object PRESENT = new Object();
    
	/**
	 * Contains all 'locked' values which will not be returned
	 * when {@link #nextInt(int)} is called.
	 */
	private ConcurrentHashMap<Integer, Object> usedInteger = new ConcurrentHashMap<Integer, Object>();
	
	/**
	 * Behaves like java.util.Random's nextInt(limit) method, except
	 * that a number is not returned a second time until {@link #release(int)}
	 * is called for that number.
	 * Note: Calling this method repeatedly with a small limit and without
	 * calling release may lock this thread if no numbers are left to return.
	 *
	 * @param limit
	 * 		Upper limit (exclusive) of value generated.
	 * @return An integer from 0 (inclusive) to limit (exclusive).
	 */
	public int nextInt(final int limit) {
		int i;
		do {
			i = rand.nextInt(limit);
		} while (usedInteger.putIfAbsent(i, PRESENT) != null);
		
		return i;
	}
	
	/**
	 * Release a value. That means this value can be
	 * returned once more from {@link #nextInt(int)}.
	 * If this value was not locked, false is returned
	 * and nothing happens.
	 * 
	 * @param n
	 * 		The number to release.
	 * @return true if n was released, false if it
	 * 		was not locked when calling this method
	 */
	public boolean release(final int n) {
		return usedInteger.remove(n) == PRESENT;
	}
	
	/**
	 * Release all locked values.
	 */
	public void clear() {
		usedInteger.clear();
	}
}
