package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.List;
import java.util.UUID;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;

/**
 * Unit tests for {@link DatasetManagement}.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
public class DatasetManagementTest {
	protected static final String tempDatasetName = "testDataset";

	PersistenceManager pm;

	static String adminSessionId;
	static String councilSessionId;
	static String developerSessionId;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		pm = PMF.getPM();

		adminSessionId = TestUtils.loginAdminUser();
		councilSessionId = UserManagement.login("mike", "secret");
		developerSessionId = UserManagement.login("john", "secret");
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		final Query query = pm.newQuery(DatasetManagement.queryDatasetByName);
		query.deletePersistentAll(tempDatasetName);
		try {
			UserManagement.logout(adminSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(councilSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(developerSessionId);
		} catch (final Exception e) {
		}
		pm.close();
	}

	private Dataset addDefaultTempDataset() throws SecurityException,
			IllegalArgumentException {
		final Dataset tempDs = new Dataset();
		tempDs.setLang(Language.EN);
		tempDs.setName(tempDatasetName);
		tempDs.setDescription("descr");
		final Dataset ds = DatasetManagement.registerDataset(councilSessionId,
				tempDs);
		return ds;
	}

	/**
	 * Tests {@link DatasetTranslation#toBean()}.
	 */
	@Test
	public void testToBean() {
		final DatasetTranslation dataset1EN = DatasetManagement
				.getDatasetTranslationById(pm,
						TestUtils.dataset1.getDatasetId(), Language.EN,
						null);
		compareDataset(TestUtils.dataset1, dataset1EN, true);
		
		final eu.iescities.server.accountinterface.Dataset ds1 = DatasetManagement
				.getDatasetById(pm, TestUtils.dataset1.getDatasetId());
		compareDataset(TestUtils.dataset1, ds1, true);
		// now test with all getters of the two objects:
		assertEquals(TestUtils.dataset1.getDatasetId(), ds1.getDatasetId());
		assertEquals(TestUtils.dataset1.getCouncilId(), ds1.getPublishingCouncil().getCouncilId());
		assertEquals(TestUtils.dataset1.getReadAccessNumber(), ds1.getReadAccessNumber());
		assertEquals(TestUtils.dataset1.getWriteAccessNumber(), ds1.getWriteAccessNumber());
		assertEquals(TestUtils.dataset1.getUserId(), ds1.getPublishingUser().getUserId());
		assertEquals(TestUtils.dataset1.getPublishingTimestamp(), ds1.getPublishingDate());
		assertEquals(TestUtils.dataset1.getName(), dataset1EN.getName());
		assertEquals(TestUtils.dataset1.getDescription(), dataset1EN.getDescription());
		assertEquals(TestUtils.dataset1.getJsonMapping(), ds1.getJsonMapping());
		assertEquals(TestUtils.dataset1.getQuality(), ds1.getQuality());
		assertEquals(TestUtils.dataset1.getQualityVerifiedBy(),
				ds1.getQualityVerifiedBy() == null ? -1 : ds1
						.getQualityVerifiedBy().getUserId());
		assertEquals(TestUtils.dataset1.getNumberAppsUsingIt(), ds1.getAppsUsingThisSet().size());
	}

	/**
	 * Tests {@link DatasetManagement#verifyDatasetId(int)}.
	 */
	@Test
	public void testVerifyDatasetId() {
		assertTrue(DatasetManagement.verifyDatasetId(TestUtils.dataset1.getDatasetId()));
		assertTrue(DatasetManagement.verifyDatasetId(TestUtils.dataset2.getDatasetId()));
		assertFalse(DatasetManagement.verifyDatasetId(-1));
	}

	/**
	 * Tests {@link DatasetManagement#registerDataset(String, Dataset)}.
	 */
	@Test
	public void testRegisterDataSet1a() {
		final Dataset tempDs = new Dataset();
		tempDs.setLang(Language.EN);
		tempDs.setName(tempDatasetName);
		tempDs.setDescription("descr");
		final Dataset ds = DatasetManagement.registerDataset(councilSessionId,
				tempDs);
		assertTrue(DatasetManagement.verifyDatasetId(ds.getDatasetId()));
		compareDataset(tempDs, ds, false);
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#registerDataset(String, Dataset)}.
	 */
	@Test
	public void testRegisterDataSet1b() {
		final Dataset tempDs = new Dataset();
		tempDs.setLang(Language.EN);
		tempDs.setName(tempDatasetName);
		tempDs.setDescription("descr");
		tempDs.setJsonMapping("test");
		final Dataset ds = DatasetManagement.registerDataset(councilSessionId,
				tempDs);
		assertTrue(DatasetManagement.verifyDatasetId(ds.getDatasetId()));
		compareDataset(tempDs, ds, false);
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#registerDataset(String, Dataset)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRegisterDataSet3() {
		final Dataset tempDs = new Dataset();
		tempDs.setLang(Language.EN);
		tempDs.setName("testDataset2");
		tempDs.setDescription("shouldn't work");
		DatasetManagement.registerDataset(UUID.randomUUID().toString(),
				tempDs);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)}.
	 */
	@Test
	public void testUpdateDataset1() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setDescription("bar");
		DatasetManagement.updateDataset(councilSessionId, updateBean);

		ds.setDescription(updateBean.getDescription());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * dataset quality and verification user in one go (by an IES Cities admin).
	 */
	@Test
	public void testUpdateDatasetQuality1() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		updateBean.setQualityVerifiedBy(TestUtils.adminUser.getUserId());
		DatasetManagement.updateDataset(adminSessionId, updateBean);

		ds.setQuality(updateBean.getQuality());
		ds.setQualityVerifiedBy(updateBean.getQualityVerifiedBy());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * dataset quality and verification user in one go (by a council admin).
	 */
	@Test
	public void testUpdateDatasetQuality2() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		updateBean.setQualityVerifiedBy(TestUtils.mikeUser.getUserId());
		DatasetManagement.updateDataset(councilSessionId, updateBean);

		ds.setQuality(updateBean.getQuality());
		ds.setQualityVerifiedBy(updateBean.getQualityVerifiedBy());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * dataset quality by a council admin and then verifying this by an IES
	 * Cities admin.
	 */
	@Test
	public void testUpdateDatasetQuality3() {
		final Dataset ds = addDefaultTempDataset();

		// set quality by council admin
		Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		DatasetManagement.updateDataset(councilSessionId, updateBean);
		ds.setQuality(updateBean.getQuality());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
		
		// verify quality by IES Cities admin:
		updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQualityVerifiedBy(TestUtils.adminUser.getUserId());
		DatasetManagement.updateDataset(adminSessionId, updateBean);
		ds.setQualityVerifiedBy(updateBean.getQualityVerifiedBy());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * dataset quality by a council admin and then verifying this by another
	 * council admin.
	 */
	@Test
	public void testUpdateDatasetQuality4() {
		final Dataset ds = addDefaultTempDataset();

		// set quality by council admin
		Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		DatasetManagement.updateDataset(councilSessionId, updateBean);
		ds.setQuality(updateBean.getQuality());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
		
		// verify quality by another council admin:
		final String council2SessionId = UserManagement.login("mike2", "secret");
		updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQualityVerifiedBy(TestUtils.mike2User.getUserId());
		DatasetManagement.updateDataset(council2SessionId, updateBean);
		ds.setQualityVerifiedBy(updateBean.getQualityVerifiedBy());
		compareDataset(ds, DatasetManagement.getDatasetById(ds.getDatasetId(),
				Language.EN, null), true);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * an invalid dataset quality.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetQualityInvalid1() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(-2);
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} setting
	 * an invalid dataset quality.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetQualityInvalid2() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(6);
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} verifying
	 * a quality rating by an IES Cities admin but setting the wrong user ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetQualityVerificationWrongId1() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		updateBean.setQualityVerifiedBy(TestUtils.mikeUser.getUserId());
		DatasetManagement.updateDataset(adminSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} verifying
	 * a quality rating by a council admin but setting the wrong user ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetQualityVerificationWrongId2() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		updateBean.setQualityVerifiedBy(TestUtils.mike2User.getUserId());
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} verifying
	 * a quality rating by a developer.
	 */
	@Test(expected = SecurityException.class)
	public void testUpdateDatasetQualityVerificationUnauthorized() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setQuality(1);
		updateBean.setQualityVerifiedBy(TestUtils.mikeUser.getUserId());
		DatasetManagement.updateDataset(developerSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} with a
	 * name longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetTooLongName() {
		final Dataset ds = addDefaultTempDataset();
		
		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setName(TestUtils.randomString(260));
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} with
	 * an empty name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateDatasetEmptyName() {
		final Dataset ds = addDefaultTempDataset();

		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setName("");
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} with a
	 * description longer than 65535 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongDescription() {
		final Dataset ds = addDefaultTempDataset();

		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setDescription(TestUtils.randomString(66000));
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#updateDataset(String, Dataset)} with a
	 * JSON mapping longer than 131072 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateAppTooLongJsonMapping() {
		final Dataset ds = addDefaultTempDataset();

		final Dataset updateBean = new Dataset();
		updateBean.setLang(Language.EN);
		updateBean.setDatasetId(ds.getDatasetId());
		updateBean.setJsonMapping(TestUtils.randomString(131100));
		DatasetManagement.updateDataset(councilSessionId, updateBean);
	}

	/**
	 * Tests {@link DatasetManagement#removeDataSet(String, int)} with a council
	 * session.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveDataSet1a() {
		final Dataset ds = addDefaultTempDataset();

		assertEquals(
				1,
				DatasetManagement.removeDataSet(councilSessionId,
						ds.getDatasetId()));
		final Query query = pm.newQuery(DatasetManagement.queryDatasetByName);
		assertArrayEquals(new eu.iescities.server.accountinterface.Dataset[] {},
				((List<eu.iescities.server.accountinterface.Dataset>) query
						.execute(tempDatasetName)).toArray());
	}

	/**
	 * Tests {@link DatasetManagement#removeDataSet(String, int)} with an admin
	 * session.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testRemoveDataSet1b() {
		final Dataset ds = addDefaultTempDataset();

		assertEquals(
				1,
				DatasetManagement.removeDataSet(adminSessionId,
						ds.getDatasetId()));
		final Query query = pm.newQuery(DatasetManagement.queryDatasetByName);
		assertArrayEquals(new eu.iescities.server.accountinterface.Dataset[] {},
				((List<eu.iescities.server.accountinterface.Dataset>) query
						.execute(tempDatasetName)).toArray());
	}

	/**
	 * Tests {@link DatasetManagement#removeDataSet(String, int)} with an
	 * unauthorised session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveDataSet2() {
		final Dataset ds = addDefaultTempDataset();

		DatasetManagement.removeDataSet(developerSessionId, ds.getDatasetId());
	}

	/**
	 * Tests {@link DatasetManagement#removeDataSet(String, int)} with a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveDataSet3() {
		final Dataset ds = addDefaultTempDataset();

		DatasetManagement.removeDataSet(UUID.randomUUID().toString(),
				ds.getDatasetId());
	}

	/**
	 * Tests {@link DatasetManagement#removeDataSet(String, int)} with a
	 * non-existing dataset ID.
	 */
	@Test
	public void testRemoveDataSetNonExisting1() {
		assertEquals(0, DatasetManagement.removeDataSet(councilSessionId, -1));
	}

	/**
	 * Tests {@link DatasetManagement#getDatasetById(int, Language, Language)}.
	 */
	@Test
	public void testGetDatasetById1() {
		Dataset ds = DatasetManagement.getDatasetById(
				TestUtils.dataset1.getDatasetId(), Language.EN, null);
		compareDataset(TestUtils.dataset1, ds, true);

		ds = DatasetManagement.getDatasetById(TestUtils.dataset2.getDatasetId(),
				Language.EN, null);
		compareDataset(TestUtils.dataset2, ds, true);
	}

	/**
	 * Tests {@link DatasetManagement#getDatasetById(int, Language, Language)} with a non-existing
	 * dataset ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetDatasetByIdNotExisting1() {
		DatasetManagement.getDatasetById(-1, Language.EN, null);
	}

	/**
	 * Tests {@link DatasetManagement#getDatasetByName(String, Language, Language)}.
	 */
	@Test
	public void testGetDatasetByName1() {
		Dataset ds = DatasetManagement.getDatasetByName(
				TestUtils.dataset1.getName(), Language.EN, null);
		compareDataset(TestUtils.dataset1, ds, true);

		ds = DatasetManagement.getDatasetByName(TestUtils.dataset2.getName(),
				Language.EN, null);
		compareDataset(TestUtils.dataset2, ds, true);
	}

	/**
	 * Tests {@link DatasetManagement#getDatasetByName(String, Language, Language)} with a
	 * non-existing dataset name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetDatasetByNameNotExisting1() {
		DatasetManagement.getDatasetByName("non-existing-dataset", Language.EN,
				null);
	}

	/**
	 * Tests {@link DatasetManagement#getAllDataSets(int, int, Language, Language)} with a limit of
	 * 1, offset 0.
	 */
	@Test
	public void testGetAllDataSets1() {
		final List<Dataset> datasets = DatasetManagement.getAllDataSets(0, 1,
				Language.EN, null);
		assertEquals(1, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
	}

	/**
	 * Tests {@link DatasetManagement#getAllDataSets(int, int, Language, Language)} with a limit of
	 * 2, offset 0.
	 */
	@Test
	public void testGetAllDataSets2() {
		final List<Dataset> datasets = DatasetManagement.getAllDataSets(0, 2,
				Language.EN, null);
		assertEquals(2, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
		compareDataset(TestUtils.dataset2, datasets.get(1), true);
	}

	/**
	 * Tests {@link DatasetManagement#getAllDataSets(int, int, Language, Language)} with a limit of
	 * 10, offset 1.
	 */
	@Test
	public void testGetAllDataSets3() {
		final List<Dataset> datasets = DatasetManagement.getAllDataSets(1, 10,
				Language.EN, null);
		assertEquals(1, datasets.size());
		compareDataset(TestUtils.dataset2, datasets.get(0), true);
	}

	/**
	 * Tests {@link DatasetManagement#getAllDataSets(int, int, Language, Language)} with a limit of
	 * 10, offset 2.
	 */
	@Test
	public void testGetAllDataSets4() {
		final List<Dataset> datasets = DatasetManagement.getAllDataSets(2, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link DatasetManagement#getAllDataSets(int, int, Language, Language)} with a limit of
	 * 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllDataSetsTooMany() {
		DatasetManagement.getAllDataSets(2, 10000, Language.EN, null);
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using one
	 * existing search word with a limit of 1, offset 0.
	 */
	@Test
	public void testFindDataSetsWith1Word1() {
		final List<Dataset> datasets = DatasetManagement
				.findDataSets("description1", 0, 1, Language.EN, null);
		assertEquals(1, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using one
	 * non-existing search word with a limit of 1, offset 0.
	 */
	@Test
	public void testFindDataSetsWith1Word2() {
		final List<Dataset> datasets = DatasetManagement.findDataSets("user", 0,
				1, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using two
	 * existing search words with a limit of 10, offset 0.
	 */
	@Test
	public void testFindDataSetsWith2Words1() {
		final List<Dataset> datasets = DatasetManagement.findDataSets(
				"description1 description2", 0, 10, Language.EN, null);
		assertEquals(2, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
		compareDataset(TestUtils.dataset2, datasets.get(1), true);
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using two
	 * non-existing search words with a limit of 1, offset 0.
	 */
	@Test
	public void testFindDataSetsWith2Words2() {
		final List<Dataset> datasets = DatasetManagement
				.findDataSets("user council", 0, 1, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using one
	 * existing dataset name with a limit of 2, offset 0.
	 */
	@Test
	public void testFindDataSetsWith1DataSetName1() {
		final List<Dataset> datasets = DatasetManagement.findDataSets("ds1", 0,
				2, Language.EN, null);
		assertEquals(1, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using one
	 * non-existing dataset name with a limit of 1, offset 0.
	 */
	@Test
	public void testFindDataSetsWith1DataSetName2() {
		final List<Dataset> datasets = DatasetManagement
				.findDataSets("dataset3", 0, 1, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using two
	 * existing dataset names with a limit of 10, offset 0.
	 */
	@Test
	public void testFindDataSetsWith2DataSetNames1() {
		final List<Dataset> datasets = DatasetManagement.findDataSets("ds1 ds2",
				0, 10, Language.EN, null);
		assertEquals(2, datasets.size());
		compareDataset(TestUtils.dataset1, datasets.get(0), true);
		compareDataset(TestUtils.dataset2, datasets.get(1), true);
	}

	/**
	 * Tests {@link DatasetManagement#findDataSets(String, int, int, Language, Language)} using two
	 * non-existing dataset names with a limit of 1, offset 0.
	 */
	@Test
	public void testFindDataSetsWith2DataSetName2() {
		final List<Dataset> datasets = DatasetManagement
				.findDataSets("dataset3 dataset4", 0, 1, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests
	 * {@link DatasetManagement#getAppsUsingDataSet(int, int, int, Language, Language)}
	 * and a limit of 1, offset 0.
	 */
	@Test
	public void testGetAppsUsingDataSet1a() {
		List<Application> apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset1.getDatasetId(), 0, 1, Language.EN, null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.get(0), true);

		apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset2.getDatasetId(), 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link DatasetManagement#getAppsUsingDataSet(int, int, int, Language, Language)}
	 * and a limit of 10, offset 0.
	 */
	@Test
	public void testGetAppsUsingDataSet1b() {
		List<Application> apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset1.getDatasetId(), 0, 10, Language.EN, null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.get(0), true);

		apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset2.getDatasetId(), 0, 10, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link DatasetManagement#getAppsUsingDataSet(int, int, int, Language, Language)}
	 * and a limit of 10, offset 1.
	 */
	@Test
	public void testGetAppsUsingDataSet1c() {
		List<Application> apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset1.getDatasetId(), 1, 10, Language.EN, null);
		assertEquals(0, apps.size());

		apps = DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset2.getDatasetId(), 1, 10, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link DatasetManagement#getAppsUsingDataSet(int, int, int, Language, Language)}
	 * with a non-existing dataset ID and a limit of 1, offset 0.
	 */
	public void testGetAppsUsingDataSet2() {
		final List<Application> apps = DatasetManagement.getAppsUsingDataSet(
				-1, 0, 1, Language.EN, null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests
	 * {@link DatasetManagement#getAppsUsingDataSet(int, int, int, Language, Language)}
	 * with a limit of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAppsUsingDataSetTooMany() {
		DatasetManagement.getAppsUsingDataSet(
				TestUtils.dataset1.getDatasetId(), 2, 10000, Language.EN, null);
	}

	/**
	 * Compares the given two datasets and verifies that all their properties
	 * are equal.
	 * 
	 * @param expected
	 *            the expected dataset
	 * @param actual
	 *            the actual dataset
	 * @param completeExpBean
	 *            whether the expected dataset bean is complete or not
	 */
	protected static void compareDataset(final Dataset expected,
			final DatasetTranslation actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		compareDataset(expected, actual.toBean(), completeExpBean);
	}

	/**
	 * Compares the given two datasets and verifies that all their properties
	 * are equal.
	 * 
	 * @param expected
	 *            the expected dataset
	 * @param actual
	 *            the actual dataset
	 * @param completeExpBean
	 *            whether the expected dataset bean is complete or not
	 */
	protected static void compareDataset(final Dataset expected,
			final eu.iescities.server.accountinterface.Dataset actual,
			final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getDatasetId() != -1) {
			assertEquals("getDatasetId()", expected.getDatasetId(),
					actual.getDatasetId());
		}
		if (completeExpBean || expected.getUserId() != -1) {
			assertNotNull(actual.getPublishingUser());
			assertEquals("getUserId()", expected.getUserId(),
					actual.getPublishingUser().getUserId());
		}
		if (completeExpBean || expected.getCouncilId() != -1) {
			assertNotNull(actual.getPublishingCouncil());
			assertEquals("getCouncilId()", expected.getCouncilId(),
					actual.getPublishingCouncil().getCouncilId());
		}
		if (completeExpBean || expected.getJsonMapping() != null) {
			assertEquals("getJsonMapping()", expected.getJsonMapping(),
					actual.getJsonMapping());
		} else {
			assertNotNull("getJsonMapping()", actual.getJsonMapping());
		}
		assertEquals("getReadAccessNumber()",
				expected.getReadAccessNumber(),
				actual.getReadAccessNumber());
		assertEquals("getWriteAccessNumber()",
				expected.getWriteAccessNumber(),
				actual.getWriteAccessNumber());
		if (completeExpBean || expected.getQuality() != -1) {
			assertEquals("getQuality()",
					expected.getQuality(),
					actual.getQuality());
		} else {
			assertTrue("getQuality() >= 0", actual.getQuality() >= 0);
			assertTrue("getQuality() <= 5", actual.getQuality() <= 5);
		}
		if (completeExpBean || expected.getQualityVerifiedBy() != -1) {
			if (expected.getQualityVerifiedBy() == -1) {
				assertNull(actual.getQualityVerifiedBy());
			} else {
				assertNotNull(actual.getQualityVerifiedBy());
				assertEquals("getQualityVerifiedBy()",
						expected.getQualityVerifiedBy(),
						actual.getQualityVerifiedBy().getUserId());
			}
		}
		assertEquals("getNumberAppsUsingIt()", expected.getNumberAppsUsingIt(),
				actual.getAppsUsingThisSet().size());
		if (completeExpBean || expected.getPublishingTimestamp() != null) {
			assertEquals("getPublishingTimestamp()",
					expected.getPublishingTimestamp(),
					actual.getPublishingDate());
		} else {
			assertNotNull("getPublishingTimestamp()",
					actual.getPublishingDate());
		}
	}

	/**
	 * Compares the given two datasets and verifies that all their properties
	 * are equal.
	 * 
	 * @param expected
	 *            the expected dataset
	 * @param actual
	 *            the actual dataset
	 * @param completeExpBean
	 *            whether the expected dataset bean is complete or not
	 */
	protected static void compareDataset(final Dataset expected,
			final Dataset actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getDatasetId() != -1) {
			assertEquals("getDatasetId()", expected.getDatasetId(),
					actual.getDatasetId());
		}
		if (completeExpBean || expected.getUserId() != -1) {
			assertEquals("getUserId()", expected.getUserId(),
					actual.getUserId());
		}
		if (completeExpBean || expected.getCouncilId() != -1) {
			assertEquals("getCouncilId()", expected.getCouncilId(),
					actual.getCouncilId());
		}
		if (completeExpBean || expected.getDescription() != null) {
			assertEquals("getDescription()", expected.getDescription(),
					actual.getDescription());
		} else {
			assertNotNull("getDescription()", actual.getDescription());
		}
		if (completeExpBean || expected.getJsonMapping() != null) {
			assertEquals("getJsonMapping()", expected.getJsonMapping(),
					actual.getJsonMapping());
		} else {
			assertNotNull("getJsonMapping()", actual.getJsonMapping());
		}
		if (completeExpBean || expected.getName() != null) {
			assertEquals("getName()", expected.getName(), actual.getName());
		} else {
			assertNotNull("getName()", actual.getName());
		}
		assertEquals("getReadAccessNumber()",
				expected.getReadAccessNumber(),
				actual.getReadAccessNumber());
		assertEquals("getWriteAccessNumber()",
				expected.getWriteAccessNumber(),
				actual.getWriteAccessNumber());
		if (completeExpBean || expected.getQuality() != -1) {
			assertEquals("getQuality()",
					expected.getQuality(),
					actual.getQuality());
		} else {
			assertTrue("getQuality() >= 0", actual.getQuality() >= 0);
			assertTrue("getQuality() <= 5", actual.getQuality() <= 5);
		}
		if (completeExpBean || expected.getQualityVerifiedBy() != -1) {
			assertEquals("getQualityVerifiedBy()",
					expected.getQualityVerifiedBy(),
					actual.getQualityVerifiedBy());
		}
		assertEquals("getNumberAppsUsingIt()", expected.getNumberAppsUsingIt(),
				actual.getNumberAppsUsingIt());
		if (completeExpBean || expected.getPublishingTimestamp() != null) {
			assertEquals("getPublishingTimestamp()",
					expected.getPublishingTimestamp(),
					actual.getPublishingTimestamp());
		} else {
			assertNotNull("getPublishingTimestamp()",
					actual.getPublishingTimestamp());
		}
	}

}
