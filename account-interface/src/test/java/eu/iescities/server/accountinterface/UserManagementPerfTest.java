package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.Required;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Unit tests for {@link UserManagement}.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class UserManagementPerfTest {
	
	@SuppressWarnings("javadoc")
	@Rule
	final public ContiPerfRule i = new ContiPerfRule();
	
	final private static Random rand = new Random();
	/**
	 * Random generator used when test requires that no object is accessed
	 * by multiple threads at the same time (e.g. {@link #testUpdateUser1()}
	 */
	final private static UniqueIntRandom uniqueRand = new UniqueIntRandom();
	
	/**
	 * Temporary already registered users for the duration of the class' test
	 * suite.
	 * 
	 * Note: properties will be set in {@link #setUpBeforeClass()}.
	 */
	private static User[] tempUsers;
	/**
	 * The first half of {@link #tempUsers} will be logged in in
	 * {@link #setUpBeforeClass()} and their session IDs are stored here.
	 */
	private static String[] validSessions;

	PersistenceManager pm;

	String adminSessionId;
	String councilSessionId;
	String developerSessionId;
	String userSessionId;

	/**
	 * Gets the default temporary user where anything could be changed.
	 * 
	 * @return a user with the same properties as {@link #tempUser}
	 * @see #setUpBeforeClass()
	 */
	protected static User getDefaultTempUser() {
		final User tempUser = new User();
		tempUser.setUsername(UserManagementTest.tempUserName);
		tempUser.setPassword(UserManagementTest.tempUserPassword);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
		return tempUser;
	}
	
	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		setupDB(500, 250, 1);
	}

	/**
	 * Setup the DB for the whole unit test.
	 * 
	 * @param userCount
	 *            number of temporary (already registered) users for the
	 *            duration of the class' test suite (stored in
	 *            {@link #tempUsers})
	 * @param sessionCount
	 *            number of logged in user session IDs (stored in
	 *            {@link #validSessions})
	 * @param timeOutMinutes
	 *            timeout in minutes
	 *
	 * @throws InterruptedException
	 * @throws TimeoutException
	 *             if the user setup cannot be completed in time
	 * @throws ExecutionException
	 *             if the user setup somehow failed
	 */
	protected static void setupDB(final int userCount, final int sessionCount,
			final long timeOutMinutes) throws InterruptedException,
					TimeoutException, ExecutionException {
		tempUsers = new User[userCount];
		validSessions = new String[sessionCount];
		TestUtils.initDatastore();
		final String adminSessionId = TestUtils.loginAdminUser();
		
		// register tempUsers.length users in parallel:
		final ExecutorService exec = Executors.newFixedThreadPool(20);
		final Future<?>[] futures = new Future<?>[userCount];
		try {
			for (int i = 0; i < tempUsers.length; ++i) {
				final Integer j = Integer.valueOf(i);
				futures[i] = exec.submit(new Runnable() {
					@Override
					public void run() {
						final User tempUser = new User();
						final String append = String.format("nr_%03d", j);
						tempUser.setUsername(UserManagementTest.tempUserName + append);
						tempUser.setPassword(UserManagementTest.tempUserPassword);
						tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
								.getCouncilId());
						tempUsers[j] = UserManagement.registerUser(tempUser);
						// first validSessions.length are logged in
						if (j < validSessions.length) {
							validSessions[j] = UserManagement.login(tempUser.getUsername(),
									UserManagementTest.tempUserPassword);
							// every second logged in user has app1 installed
							if ((j % 2) == 0) {
								UserManagement.addInstalledApp(
										validSessions[j],
										TestUtils.app1.getAppId());
							}
							// every 10th logged in user develops app1
							if ((j % 10) == 0) {
								ApplicationManagement.addDeveloper(adminSessionId,
										TestUtils.app1.getAppId(),
										tempUsers[j].getUserId());
							}
							if ((j % 15) == 0) {
								CouncilManagement.addCouncilAdmin(
										adminSessionId,
										TestUtils.hawaiiCouncil.getCouncilId(),
										tempUsers[j].getUserId());
								final Dataset tempDs = new Dataset();
								tempDs.setLang(Language.EN);
								tempDs.setName(DatasetManagementTest.tempDatasetName
										+ append);
								tempDs.setDescription("descr");
								DatasetManagement.registerDataset(
										validSessions[j], tempDs);
							}
						}
					}
				});
			}
		} finally {
			exec.shutdown();
			if (!exec.awaitTermination(timeOutMinutes, TimeUnit.MINUTES)) {
				throw new TimeoutException("Could not create users in time.");
			}
			for (final Future<?> future : futures) {
				// check successful completion
				assertNull(future.get());
			}
		}
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.pm = PMF.getPM();

		adminSessionId = TestUtils.loginAdminUser();
		councilSessionId = UserManagement.login("mike", "secret");
		developerSessionId = UserManagement.login("john", "secret");
		userSessionId = UserManagement.login("bob", "secret");

		// ensure there is no existing temp user
		final PersistenceManager pm = PMF.getPM();
		final Query query = pm.newQuery(UserManagement.queryUserByName);
		assertNull(query.execute(UserManagementTest.tempUserName));
		assertNull(query.execute(UserManagementTest.tempUserName2));
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		final Query dsQuery = pm.newQuery(DatasetManagement.queryDatasetByName);
		dsQuery.deletePersistentAll(DatasetManagementTest.tempDatasetName);
		
		final Query query = pm.newQuery(UserManagement.queryUserByName);
		query.deletePersistentAll(UserManagementTest.tempUserName);
		query.deletePersistentAll(UserManagementTest.tempUserName2);

		try {
			UserManagement.logout(councilSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(developerSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(userSessionId);
		} catch (final Exception e) {
		}
		pm.close();
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 200)
	public void testUpdateUser1() {
		final int i = uniqueRand.nextInt(validSessions.length);
		
		try {
			final User updateBean = new User();
			updateBean.setUserId(tempUsers[i].getUserId());
			updateBean.setProfile(TestUtils.randomString(300));
			updateBean.setName("test2");
			updateBean.setSurname("user2");
			updateBean.setEmail(String.format("user_%03d@test2.org", i));
			UserManagement.updateUser(validSessions[i], updateBean, null);
		} finally {
			uniqueRand.release(i);
		}
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 200)
	public void testUpdateUser1a() {
		final int i = uniqueRand.nextInt(validSessions.length);

		try {
			final User updateBean = new User();
			updateBean.setUserId(tempUsers[i].getUserId());
			updateBean.setProfile(TestUtils.randomString(300));
			updateBean.setUsername(tempUsers[i].getUsername());
			updateBean.setName("test2");
			updateBean.setSurname("user2");
			updateBean.setEmail(String.format("user_%03d@test2.org", i));
			UserManagement.updateUser(validSessions[i], updateBean, null);
		} finally {
			uniqueRand.release(i);
		}
	}

//	/**
//	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
//	 * the user's profile, username, name, surname, email.
//	 */
//	@Test
//	public void testUpdateUser2() {
//		final User user = UserManagement.registerUser(tempUser);
//		final String sessionId = UserManagement.login(tempUserName,
//				tempUserPassword);
//		
//		final User updateBean = new User();
//		updateBean.setUserId(user.getUserId());
//		updateBean.setProfile(TestUtils.randomString(300));
//		updateBean.setUsername(tempUserName2);
//		updateBean.setName("test2");
//		updateBean.setSurname("user2");
//		updateBean.setEmail("user2@test2.org");
//		UserManagement.updateUser(sessionId, updateBean, null);
//	}
//
//	/**
//	 * Test method for {@link UserManagement#updateUser(String, User, String)}
//	 * changing the password.
//	 */
//	@Test
//	public void testUpdateUserPassword1() {
//		UserManagement.registerUser(tempUser);
//
//		String sessionId = UserManagement.login(tempUserName, tempUserPassword);
//		final User changeBean = new User();
//		changeBean.setPassword(tempUserPassword + "X");
//		UserManagement.updateUser(sessionId, changeBean, tempUserPassword);
//		UserManagement.logout(sessionId);
//		sessionId = UserManagement.login(tempUserName, tempUserPassword + "X");
//		assertNotNull(sessionId);
//		assertTrue(sessionId.length() > 0);
//
//		try {
//			UserManagement.login(tempUserName, tempUserPassword);
//			fail();
//		} catch (final IllegalArgumentException e) {
//			// ok - we should not be able to login with the old password!
//		}
//	}

	/**
	 * Tests {@link UserManagement#getUserBySession(String)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 300)
	public void testGetUserBySession1() {
		final int i = rand.nextInt(validSessions.length);
		final User user = UserManagement.getUserBySession(validSessions[i]);
		assertEquals(tempUsers[i].getUserId(), user.getUserId());
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 600)
	public void testGetUserById() {
		final int i = rand.nextInt(tempUsers.length);
		final User user = UserManagement.getUserById(adminSessionId,
				tempUsers[i].getUserId());
		assertEquals(tempUsers[i].getUserId(), user.getUserId());
	}

	/**
	 * Tests {@link UserManagement#getInstalledApps(String, Language, Language)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 350)
	public void testGetInstalledApps1() {
		final int i = rand.nextInt(validSessions.length);
		final List<Application> apps = UserManagement
				.getInstalledApps(validSessions[i], Language.EN, null);
		if ((i % 2) == 0) {
			assertEquals(1, apps.size());
		} else {
			assertEquals(0, apps.size());
		}
	}

//	/**
//	 * Tests {@link UserManagement#addInstalledApp(String, int)}.
//	 */
//	@Test
//	public void testAddInstalledApps1() {
//		UserManagement.registerUser(tempUser);
//		final String tempUserSession = UserManagement.login(tempUserName,
//				tempUserPassword);
//
//		UserManagement.addInstalledApp(tempUserSession,
//				TestUtils.app1.getAppId());
//
//		final Set<Application> apps = UserManagement
//				.getInstalledApps(tempUserSession);
//		assertEquals(1, apps.size());
//		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
//				.next(), true);
//	}
//
//	/**
//	 * Tests {@link UserManagement#removeInstalledApp(String, int)}.
//	 */
//	@Test
//	public void testRemoveInstalledApps1() {
//		UserManagement.registerUser(tempUser);
//		final String tempUserSession = UserManagement.login(tempUserName,
//				tempUserPassword);
//
//		UserManagement.addInstalledApp(tempUserSession,
//				TestUtils.app1.getAppId());
//
//		Set<Application> apps = UserManagement
//				.getInstalledApps(tempUserSession);
//		assertEquals(1, apps.size());
//		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
//				.next(), true);
//
//		UserManagement.removeInstalledApp(tempUserSession,
//				TestUtils.app1.getAppId());
//		apps = UserManagement.getInstalledApps(tempUserSession);
//		assertEquals(0, apps.size());
//	}

	/**
	 * Tests {@link UserManagement#getDevelopedApps(String, Language, Language)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 400)
	public void testGetDevelopedApps() {
		final int i = rand.nextInt(validSessions.length);
		final List<Application> apps = UserManagement
				.getDevelopedApps(validSessions[i], Language.EN, null);
		if ((i % 10) == 0) {
			assertEquals(1, apps.size());
		} else {
			assertEquals(0, apps.size());
		}
	}

	/**
	 * Tests {@link UserManagement#getUsedDataSets(String, int, int, Language, Language)} with a
	 * limit of 10, offset 0.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 200)
	public void testGetUsedDataSets1() {
		final int i = rand.nextInt(validSessions.length);
		final List<Dataset> datasets = UserManagement
				.getUsedDataSets(validSessions[i], 0, 10, Language.EN, null);
		if ((i % 10) == 0) {
			assertEquals(1, datasets.size());
		} else {
			assertEquals(0, datasets.size());
		}
	}

	/**
	 * Tests
	 * {@link UserManagement#getPublishedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 0.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 250)
	public void testGetPublishedDataSets1() {
		final int i = rand.nextInt(validSessions.length);
		final List<Dataset> datasets = UserManagement.getPublishedDataSets(
				validSessions[i], 0, 10, Language.EN, null);
		if ((i % 15) == 0) {
			assertEquals(1, datasets.size());
		} else {
			assertEquals(0, datasets.size());
		}
	}

	/**
	 * Tests {@link UserManagement#validateUserSession(String)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 800)
	public void testValidateUserSession1() {
		final int i = rand.nextInt(validSessions.length);
		UserManagement.validateUserSession(validSessions[i]);
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)}.
	 */
	@Test
	@PerfTest(invocations = 1000, threads = 20)
	@Required(throughput = 200)
	public void testLogin1() {
		final int i = rand.nextInt(tempUsers.length);
		final String sessionId = UserManagement
				.login(tempUsers[i].getUsername(),
						UserManagementTest.tempUserPassword);
		assertNotNull(sessionId);
		assertTrue(sessionId.length() > 0);
	}

//	/**
//	 * Test method for {@link UserManagement#login(String, String)} with an
//	 * valid session time extending the sessionExpires attribute during access.
//	 */
//	@Test
//	public void testLogin6() {
//		final int defaultSessionTime = eu.iescities.server.accountinterface.User.defaultSessionTime;
//		try {
//			// register user
//			UserManagement.registerUser(tempUser);
//
//			// log in
//			eu.iescities.server.accountinterface.User.defaultSessionTime = 1;
//			final String returnVal = UserManagement.login(tempUserName,
//					tempUserPassword);
//			assertNotNull(returnVal);
//			assertTrue(returnVal.length() > 0);
//			final User user1 = UserManagement.getUserBySession(returnVal);
//			Thread.sleep(400);
//			final User user2 = UserManagement.getUserBySession(returnVal);
//			assertTrue("exp old = " + user1.getSessionExpires() + ", exp new= "
//					+ user2.getSessionExpires(), user2.getSessionExpires()
//					.after(user1.getSessionExpires()));
//		} catch (final InterruptedException e) {
//		} finally {
//			eu.iescities.server.accountinterface.User.defaultSessionTime = defaultSessionTime;
//		}
//	}
//
//	/**
//	 * Test method for {@link UserManagement#registerUser(User)} and
//	 * {@link UserManagement#loginWithGoogle(String)} with a Google profile ID.
//	 */
//	@Test
//	@Ignore(value = "need a manually generated access token for now")
//	public void testLoginWithGoogle1() {
//		// add token and profile ID retrieved from e.g.
//		// https://developers.google.com/oauthplayground
//		// Step 1: select "openid profile" as scope -> authorize APIs
//		// Step 2: "exchange authorization code for tokens"
//		// Step 3a: copy the "id_token" field from the response into the authToken variable
//		// Step 3b: request https://www.googleapis.com/oauth2/v1/userinfo to retrieve the profileId ("id" in the response)
//		final String authToken = "<access token>";
//		final String profileId = "<profile id>";
//		User tempUser = new User();
//		tempUser.setUsername(tempUserName);
//		tempUser.setGoogleProfile(profileId);
//		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
//		tempUser = UserManagement.registerUser(tempUser);
//		final String sessionId = UserManagement.loginWithGoogle(authToken);
//		// verify session ID is working:
//		final User user = UserManagement.getUserBySession(sessionId);
//		UserManagementTest.compareUser(tempUser, user, true);
//	}
//
//	/**
//	 * Test method for {@link UserManagement#logout(String)}.
//	 */
//	@Test
//	public void testLogout() {
//		// verify that the session is valid:
//		UserManagement.getUserBySession(userSessionId);
//		// log out
//		UserManagement.logout(userSessionId);
//		try {
//			UserManagement.getUserBySession(userSessionId);
//			fail("user session should be invalid after registration");
//		} catch (final SecurityException e) {
//		}
//	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 10, offset 1.
	 */
	@Test
	@PerfTest(invocations = 500, threads = 20)
	@Required(throughput = 50)
	public void testGetAllUsers3() {
		final List<User> users = UserManagement.getAllUsers(adminSessionId, 1,
				10);
		assertEquals(Math.min(10, 4 + tempUsers.length), users.size());
	}

}
