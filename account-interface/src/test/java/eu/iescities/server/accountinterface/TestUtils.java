package eu.iescities.server.accountinterface;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.apache.commons.lang3.RandomStringUtils;

import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Utility functions useful for unit tests.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class TestUtils {
	private final static Logger LOGGER = Logger.getLogger(TestUtils.class.getName());

	/**
	 * Password of the default admin user created by {@link #addAdminUser()}.
	 */
	public static final String ADMIN_PASSWORD = "secret";
	/**
	 * Username of the default admin user created by {@link #addAdminUser()}.
	 */
	public static final String ADMIN_NAME = "admin";

	/**
	 * Council covering the Honululu island.
	 */
	public static Council honululuCouncil;
	/**
	 * Council covering the Hawaii island.
	 */
	public static Council hawaiiCouncil;
	/**
	 * IES Cities administrator.
	 */
	public static User adminUser;
	/**
	 * User of the {@link #honululuCouncil} council with {@link #app1}
	 * installed.
	 */
	public static User bobUser;
	/**
	 * User of the {@link #honululuCouncil} council developing {@link #app1}.
	 */
	public static User johnUser;
	/**
	 * User of the {@link #hawaiiCouncil} council managing the
	 * {@link #honululuCouncil} council who published {@link #dataset1} and
	 * {@link #dataset2}.
	 */
	public static User mikeUser;
	/**
	 * User of the {@link #hawaiiCouncil} council managing the
	 * {@link #honululuCouncil} council who published {@link #dataset1} and
	 * {@link #dataset2}.
	 */
	public static User mike2User;
	/**
	 * Dataset published by {@link #mikeUser} and {@link #honululuCouncil} used
	 * by {@link #app1}.
	 */
	public static Dataset dataset1;
	/**
	 * Dataset published by {@link #mikeUser} and {@link #honululuCouncil}.
	 */
	public static Dataset dataset2;
	/**
	 * Application developed by {@link #johnUser} using {@link #dataset1} with 2
	 * ratings: {@link #rating1} and {@link #rating2}.
	 */
	public static Application app1;
	/**
	 * 5-star rating for {@link #app1} by "jonny".
	 */
	public static AppRating rating1;
	/**
	 * 3-star rating for {@link #app1} by "bobby".
	 */
	public static AppRating rating2;

	/**
	 * Deletes all {@link GeoScope} instances from the store.
	 */
	public static void cleanupGeoScopes() {
		final PersistenceManager pm = PMF.getPM();
		try {
			cleanupGeoScopes(pm);
		} finally {
			pm.close();
		}
	}

	/**
	 * Deletes all {@link GeoScope} instances from the store.
	 * 
	 * @param pm
	 *            persistence manager to use
	 */
	private static void cleanupGeoScopes(final PersistenceManager pm) {
		final Query query = pm
				.newQuery(eu.iescities.server.accountinterface.GeoScope.class);
		@SuppressWarnings("unchecked")
		final List<eu.iescities.server.accountinterface.GeoScope> geoScopes = (List<eu.iescities.server.accountinterface.GeoScope>) query
				.execute();
		if (geoScopes.size() > 0) {
			LOGGER.info("Removing " + geoScopes.size()
					+ " geographical scopes");
			pm.deletePersistentAll(geoScopes);
		}
	}

	/**
	 * Removes every entry from the datastore leaving only blank tables.
	 */
	@SuppressWarnings("unchecked")
	public static void cleanUpDatastore() {
		final PersistenceManager pm = PMF.getPM();
		Query query;

		try {
			// remove password reset requests
			query = pm
					.newQuery(eu.iescities.server.accountinterface.UserPasswordReset.class);
			final List<eu.iescities.server.accountinterface.UserPasswordReset> uprs = (List<eu.iescities.server.accountinterface.UserPasswordReset>) query
					.execute();
			if (uprs.size() > 0) {
				LOGGER.info("Removing " + uprs.size() + " password reset requests");
				pm.deletePersistentAll(uprs);
			}
			
			// remove apps
			query = pm
					.newQuery(eu.iescities.server.accountinterface.Application.class);
			final List<eu.iescities.server.accountinterface.Application> apps = (List<eu.iescities.server.accountinterface.Application>) query
					.execute();
			if (apps.size() > 0) {
				LOGGER.info("Removing " + apps.size() + " apps");
				pm.deletePersistentAll(apps);
			}

			// remove datasets
			query = pm
					.newQuery(eu.iescities.server.accountinterface.Dataset.class);
			final List<eu.iescities.server.accountinterface.Dataset> datasets = (List<eu.iescities.server.accountinterface.Dataset>) query
					.execute();
			if (datasets.size() > 0) {
				LOGGER.info("Removing " + datasets.size() + " datasets");
				pm.deletePersistentAll(datasets);
			}

			// remove users
			query = pm
					.newQuery(eu.iescities.server.accountinterface.User.class);
			final List<eu.iescities.server.accountinterface.User> users = (List<eu.iescities.server.accountinterface.User>) query
					.execute();
			if (users.size() > 0) {
				LOGGER.info("Removing " + users.size() + " users");
				pm.deletePersistentAll(users);
			}

			// remove councils
			query = pm
					.newQuery(eu.iescities.server.accountinterface.Council.class);
			final List<eu.iescities.server.accountinterface.Council> councils = (List<eu.iescities.server.accountinterface.Council>) query
					.execute();
			if (councils.size() > 0) {
				LOGGER.info("Removing " + councils.size() + " councils");
				pm.deletePersistentAll(councils);
			}

			// remove geographical scopes
			cleanupGeoScopes(pm);
		} finally {
			pm.close();
		}
	}

	/**
	 * Adds an administrator to the DB.
	 * 
	 * @return the created user object
	 */
	public static User addAdminUser() {
		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = new eu.iescities.server.accountinterface.User();
			user.setUsername(ADMIN_NAME);
			user.setPassword(PasswordHash.createHash(ADMIN_PASSWORD));
			user.setIesAdmin(true);
			pm.makePersistent(user);
			return user.toBean();
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final InvalidKeySpecException e) {
			throw new RuntimeException(e);
		} finally {
			pm.close();
		}
	}

	/**
	 * Logs in the admin user of {@link #addAdminUser()}.
	 * 
	 * @return the session ID
	 */
	public static String loginAdminUser() {
		return UserManagement.login(TestUtils.ADMIN_NAME,
				TestUtils.ADMIN_PASSWORD);
	}
	
	/**
	 * Creates a user with the given username and password.
	 * 
	 * @param name
	 *            a user name
	 * @param password
	 *            a selected password
	 *
	 * @return the created user object
	 *
	 * @throws IllegalArgumentException
	 *             if the user name or password is invalid
	 * @throws NoSuchAlgorithmException
	 *             if no provider supports the password hash algorithm
	 * @throws InvalidKeySpecException
	 *             if the created key specification is inappropriate for the
	 *             secret-key factory to produce a secret key
	 */
	public static eu.iescities.server.accountinterface.User getSimpleUser(
			final String name, final String password) throws IllegalArgumentException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		final eu.iescities.server.accountinterface.User simpleUser = new eu.iescities.server.accountinterface.User();
		simpleUser.setUsername(name);
		simpleUser.setPassword(PasswordHash.createHash(password));
		return simpleUser;
	}

	/**
	 * Initialises the DB by adding some default councils, users, datasets, apps
	 * and app ratings.
	 */
	public static void initDatastore() {
		cleanUpDatastore();
		final PersistenceManager pm = PMF.getPM();

		try {
			adminUser = addAdminUser();

			final eu.iescities.server.accountinterface.Council honululuCouncil = new eu.iescities.server.accountinterface.Council();
			final CouncilTranslation honululuCouncilEN = new CouncilTranslation(Language.EN);
			honululuCouncilEN.setCouncil(honululuCouncil);
			honululuCouncilEN.setName("Honululu");
			honululuCouncilEN.setDescription("Just a fake council");
			honululuCouncilEN.setImage("http://server.iescities.eu/honululu.jpg");
			honululuCouncil.setGeographicalScope(new eu.iescities.server.accountinterface.GeoScope(
					"Honululu", 21.754398, -157.642822, 21.253542, -158.274536));
			pm.makePersistent(honululuCouncilEN);

			final eu.iescities.server.accountinterface.Council hawaiiCouncil = new eu.iescities.server.accountinterface.Council();
			final CouncilTranslation hawaiiCouncilEN = new CouncilTranslation(Language.EN);
			hawaiiCouncilEN.setCouncil(hawaiiCouncil);
			hawaiiCouncilEN.setName("Hawaii");
			hawaiiCouncilEN.setDescription("Another fake council");
			honululuCouncilEN.setImage("http://server.iescities.eu/hawaii.jpg");
			hawaiiCouncil.setGeographicalScope(new eu.iescities.server.accountinterface.GeoScope(
					"Hawaii", 20.262197, -154.830322, 18.885498, -156.082764));
			pm.makePersistent(hawaiiCouncilEN);

			final eu.iescities.server.accountinterface.User bobUser = new eu.iescities.server.accountinterface.User();
			bobUser.setUsername("bob");
			bobUser.setPassword(PasswordHash.createHash("secret"));
			bobUser.setPreferredLocation(honululuCouncil);
			pm.makePersistent(bobUser);

			final eu.iescities.server.accountinterface.User johnUser = new eu.iescities.server.accountinterface.User();
			johnUser.setUsername("john");
			johnUser.setPassword(PasswordHash.createHash("secret"));
			johnUser.setPreferredLocation(honululuCouncil);
			pm.makePersistent(johnUser);

			final eu.iescities.server.accountinterface.User mikeUser = new eu.iescities.server.accountinterface.User();
			mikeUser.setUsername("mike");
			mikeUser.setPassword(PasswordHash.createHash("secret"));
			mikeUser.setPreferredLocation(hawaiiCouncil);
			mikeUser.setManagedCouncil(honululuCouncil);
			honululuCouncil.getCouncilAdmins().add(mikeUser);
			pm.makePersistent(mikeUser);

			final eu.iescities.server.accountinterface.User mike2User = new eu.iescities.server.accountinterface.User();
			mike2User.setUsername("mike2");
			mike2User.setPassword(PasswordHash.createHash("secret"));
			mike2User.setPreferredLocation(hawaiiCouncil);
			mike2User.setManagedCouncil(honululuCouncil);
			honululuCouncil.getCouncilAdmins().add(mike2User);
			pm.makePersistent(mike2User);

			final eu.iescities.server.accountinterface.Dataset dataset1 = new eu.iescities.server.accountinterface.Dataset();
			final DatasetTranslation dataset1EN = new DatasetTranslation(Language.EN);
			dataset1EN.setDataset(dataset1);
			dataset1EN.setName("ds1");
			dataset1EN.setDescription("dataset description1");
			dataset1.setPublishingDate(new Date());
			dataset1.setPublishingUser(mikeUser);
			dataset1.setPublishingCouncil(honululuCouncil);
			pm.makePersistent(dataset1EN);

			final eu.iescities.server.accountinterface.Dataset dataset2 = new eu.iescities.server.accountinterface.Dataset();
			final DatasetTranslation dataset2EN = new DatasetTranslation(Language.EN);
			dataset2EN.setDataset(dataset2);
			dataset2EN.setName("ds2");
			dataset2EN.setDescription("dataset description2");
			dataset2.setPublishingDate(new Date());
			dataset2.setPublishingUser(mikeUser);
			dataset2.setPublishingCouncil(honululuCouncil);
			pm.makePersistent(dataset2EN);

			final eu.iescities.server.accountinterface.Application app1 = new eu.iescities.server.accountinterface.Application();
			final ApplicationTranslation app1EN = new ApplicationTranslation(Language.EN);
			app1EN.setApp(app1);
			app1EN.setName("app1");
			app1EN.setDescription("Lorem ipsum dolor sit amet, consectetur "
					+ "adipisicing elit, sed do eiusmod tempor incididunt ut "
					+ "labore et dolore magna aliqua. Ut enim ad minim veniam, "
					+ "quis nostrud exercitation ullamco laboris nisi ut aliquip "
					+ "ex ea commodo consequat. Duis aute irure dolor in "
					+ "reprehenderit in voluptate velit esse cillum dolore eu "
					+ "fugiat nulla pariatur. Excepteur sint occaecat cupidatat "
					+ "non proident, sunt in culpa qui officia deserunt mollit "
					+ "anim id est laborum.");
			app1.setUrl("http://server.iescities.eu");
			app1EN.setImage("http://server.iescities.eu/testApp.jpg");
			app1.setRelatedCouncil(honululuCouncil);
			app1.setVersion("1");
			app1EN.setTos("Terms of Service");
			app1.setPermissions("app permissions");
			app1.addDeveloper(johnUser);
			app1.addDataset(dataset1);
			app1EN.addTag("distributed");
			app1EN.addTag("fixit");
			app1.setPackageName(TestUtils.class.getName());
			johnUser.getDevelopedApps().add(app1);
			dataset1.addAppUsingThisSet(app1);
			pm.makePersistent(app1EN);
			bobUser.getInstalledApps().add(app1);

			final eu.iescities.server.accountinterface.AppRating rating1 = new eu.iescities.server.accountinterface.AppRating();
			rating1.setApp(app1);
			rating1.setUserName("jonny");
			rating1.setLang(eu.iescities.server.accountinterface.datainterface.Language.EN);
			rating1.setTitle("great app!");
			rating1.setRating(5);
			rating1.setDate(new Date());
			pm.makePersistent(rating1);

			final eu.iescities.server.accountinterface.AppRating rating2 = new eu.iescities.server.accountinterface.AppRating();
			rating2.setApp(app1);
			rating2.setUserName("bobby");
			rating2.setLang(eu.iescities.server.accountinterface.datainterface.Language.EN);
			rating2.setTitle("works");
			rating2.setRating(3);
			rating2.setDate(new Date());
			pm.makePersistent(rating2);
			
			TestUtils.honululuCouncil = honululuCouncilEN.toBean();
			TestUtils.hawaiiCouncil = hawaiiCouncilEN.toBean();
			TestUtils.bobUser = bobUser.toBean();
			TestUtils.johnUser = johnUser.toBean();
			TestUtils.mikeUser = mikeUser.toBean();
			TestUtils.mike2User = mike2User.toBean();
			TestUtils.dataset1 = dataset1EN.toBean();
			TestUtils.dataset2 = dataset2EN.toBean();
			TestUtils.app1 = app1EN.toBean();
			TestUtils.rating1 = rating1.toBean();
			TestUtils.rating2 = rating2.toBean();
		} catch (final IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final InvalidKeySpecException e) {
			throw new RuntimeException(e);
		} finally {
			pm.close();
		}
	}

	/**
	 * Creates a random string with the given length.
	 *
	 * Characters will be chosen from the set of all characters replacing \u0000
	 * with '_' since it is not valid for PostgreSQL UTF8 columns.
	 *
	 * @param count
	 *            the length of random string to create
	 * @return the random string
	 * 
	 * @see <a
	 *      href="http://www.postgresql.org/message-id/1171970019.3101.328.camel@coppola.muc.ecircle.de">
	 *      Invalid byte sequence for encoding "UTF8": 0x00</a>
	 */
    public static String randomString(final int count) {
    	return RandomStringUtils.random(count).replace('\u0000', '_');
    }
}
