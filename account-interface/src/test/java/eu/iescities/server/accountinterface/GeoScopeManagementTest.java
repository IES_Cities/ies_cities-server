package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.List;

import javax.jdo.PersistenceManager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.PMF;

/**
 * Test suite for {@link GeoScopeManagement}.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class GeoScopeManagementTest {
	static final double PRECISION = 0.001;
	
	static String adminSessionId;

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();
		TestUtils.addAdminUser();
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		adminSessionId = TestUtils.loginAdminUser();
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		UserManagement.logout(adminSessionId);
		TestUtils.cleanupGeoScopes();
	}

	private GeoScope createScope(final String name, final double firstLat,
			final double firstLong, final double secondLat,
			final double secondLong) throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final GeoPoint nw = new GeoPoint(firstLat, firstLong);
			final GeoPoint se = new GeoPoint(secondLat, secondLong);
			final eu.iescities.server.accountinterface.GeoScope scope = new eu.iescities.server.accountinterface.GeoScope(
					name, nw, se);
			pm.makePersistent(scope);
			final GeoScope result = new GeoScope(name, nw, se);
			result.setId(scope.getScopeId());
			return result;
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests some {@link GeoPoint} constructors and
	 * {@link GeoPoint#distanceInKm(GeoPoint)}.
	 */
	@Test
	public void testGeoPointEquals() {
		assertTrue(new GeoPoint(0.0, 0.0).distanceInKm(new GeoPoint()) <= PRECISION);
		assertTrue(new GeoPoint().distanceInKm(new GeoPoint(0.0, 0.0)) <= PRECISION);
		assertTrue(new GeoPoint(0.1, 0.2).distanceInKm(new GeoPoint(0.1, 0.2)) <= PRECISION);
		assertTrue(new GeoPoint(0.1, 0.2).distanceInKm(new GeoPoint("0.1, 0.2")) <= PRECISION);
		assertTrue(new GeoPoint("0.1, 0.2").distanceInKm(new GeoPoint(0.1, 0.2)) <= PRECISION);
		final GeoPoint point = new GeoPoint();
		point.setLatitude(0.1);
		point.setLongitude(0.2);
		assertTrue(new GeoPoint(0.1, 0.2).distanceInKm(point) <= PRECISION);
		assertTrue(point.distanceInKm(new GeoPoint(0.1, 0.2)) <= PRECISION);
		assertFalse(new GeoPoint().distanceInKm(new GeoPoint(0.1, 0.2)) <= PRECISION);
		assertFalse(new GeoPoint(0.1, 0.2).distanceInKm(new GeoPoint()) <= PRECISION);
		assertFalse(new GeoPoint().distanceInKm(new GeoPoint(0.1, 0.1)) <= PRECISION);
		assertFalse(new GeoPoint(0.1, 0.1).distanceInKm(new GeoPoint()) <= PRECISION);
	}

	/**
	 * Tests
	 * {@link eu.iescities.server.accountinterface.GeoScope#GeoScope(String, double, double, double, double)}
	 * with an empty name.
	 */
	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void testGeoScopeEmptyName() {
		new eu.iescities.server.accountinterface.GeoScope("", 0.1, 0.2, 0.3,
				0.4);
	}

	/**
	 * Tests
	 * {@link eu.iescities.server.accountinterface.GeoScope#GeoScope(String, double, double, double, double)}
	 * with a too long name.
	 */
	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void testGeoScopeTooLongName() {
		new eu.iescities.server.accountinterface.GeoScope(
				TestUtils.randomString(260), 0.1, 0.2, 0.3, 0.4);
	}

	/**
	 * Tests
	 * {@link eu.iescities.server.accountinterface.GeoScope#GeoScope(GeoScope)}.
	 */
	@Test
	public void testGeoScopeCopy() {
		final GeoScope scope1 = new GeoScope("test", 0.1, 0.2, 0.3, 0.4);
		final GeoScope scope2 = new eu.iescities.server.accountinterface.GeoScope(
				scope1).toBean();
		compareGeoScope(scope1, scope2, true);
	}

	/**
	 * Tests {@link GeoScope#getCentralGeoPoint()}.
	 */
	@Test
	public void testGeoScopeCentralPoint() {
		final GeoScope scope1 = new GeoScope("test", 0.1, 0.2, 0.3, 0.4);
		assertTrue(scope1.getCentralGeoPoint().distanceInKm(
				new GeoPoint(0.2, 0.3)) <= PRECISION);
	}

	/**
	 * Tests {@link eu.iescities.server.accountinterface.GeoScope#toBean()}.
	 */
	@Test
	public void testToBean() {
		final eu.iescities.server.accountinterface.GeoScope scope = new eu.iescities.server.accountinterface.GeoScope(
				"Honululu", new GeoPoint(21.754398, -157.642822), new GeoPoint(
						21.253542, -158.274536));
		final GeoScope scopeBean = scope.toBean();
		compareGeoScope(scopeBean, scope, true);
		// now test with all getters of the two objects:
		assertEquals(scopeBean.getId(), scope.getScopeId());
		assertEquals(scopeBean.getName(), scope.getScopeName());
		assertEquals(scopeBean.getNorthEastLatitude(), scope.getNorthEast().getLatitude(), 0.1);
		assertEquals(scopeBean.getNorthEastLongitude(), scope.getNorthEast().getLongitude(), 0.1);
		assertEquals(scopeBean.getSouthWestLatitude(), scope.getSouthWest().getLatitude(), 0.1);
		assertEquals(scopeBean.getSouthWestLongitude(), scope.getSouthWest().getLongitude(), 0.1);
		assertTrue(scopeBean.getNorthEastGeoPoint().distanceInKm(scope.getNorthEast()) <= PRECISION);
		assertTrue(scopeBean.getSouthWestGeoPoint().distanceInKm(scope.getSouthWest()) <= PRECISION);
	}

	/**
	 * Tests {@link GeoScopeManagement#verifyScopeId(int)}.
	 */
	@Test
	public void testVerifyScopeId() {
		assertFalse(GeoScopeManagement.verifyScopeId(-1));
	}

	/**
	 * Tests
	 * {@link GeoScopeManagement#createScope(String, String, GeoPoint, GeoPoint)}.
	 */
	@Test
	public void testCreateScope1() {
		final GeoScope honululuScopeExp = GeoScopeManagement.createScope(
				adminSessionId, "Honululu",
				new GeoPoint(21.754398, -157.642822), new GeoPoint(21.253542,
						-158.274536));
		assertTrue(GeoScopeManagement.verifyScopeId(honululuScopeExp.getId()));
		final GeoScope hawaiiScopeExp = GeoScopeManagement.createScope(
				adminSessionId, "Hawaii", new GeoPoint(20.262197, -154.830322),
				new GeoPoint(18.885498, -156.082764));
		assertTrue(GeoScopeManagement.verifyScopeId(hawaiiScopeExp.getId()));

		// now retrieve the scopes
		compareGeoScope(honululuScopeExp,
				GeoScopeManagement.getScopeById(honululuScopeExp.getId()), true);
		compareGeoScope(hawaiiScopeExp,
				GeoScopeManagement.getScopeById(hawaiiScopeExp.getId()), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#removeScope(String, int)} with valid
	 * scope ids.
	 */
	@Test
	public void testRemoveScope1() {
		final GeoScope honululuScopeExp = createScope("Honululu", 21.754398,
				-157.642822, 21.253542, -158.274536);

		assertEquals(
				1,
				GeoScopeManagement.removeScope(adminSessionId,
						honululuScopeExp.getId()));
	}

	/**
	 * Tests {@link GeoScopeManagement#removeScope(String, int)} with an
	 * invalid scope id.
	 */
	@Test
	public void testRemoveScope2() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);

		assertEquals(0,
				GeoScopeManagement.removeScope(adminSessionId, -1));
	}

	/**
	 * Tests {@link GeoScopeManagement#getScopeById(int)} with a non-existing
	 * scope.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetScope0() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		GeoScopeManagement.getScopeById(-1);
	}

	/**
	 * Tests {@link GeoScopeManagement#getScopeById(int)} with existing scopes.
	 */
	@Test
	public void testGetScope1() {
		final GeoScope honululuScopeExp = createScope("Honululu", 21.754398,
				-157.642822, 21.253542, -158.274536);
		final GeoScope hawaiiScopeExp = createScope("Hawaii", 20.262197,
				-154.830322, 18.885498, -156.082764);

		// now retrieve the scopes by name in the region
		compareGeoScope(honululuScopeExp,
				GeoScopeManagement.getScopeById(honululuScopeExp.getId()), true);
		compareGeoScope(hawaiiScopeExp,
				GeoScopeManagement.getScopeById(hawaiiScopeExp.getId()), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#getScopesCoveringPoint(GeoPoint)}
	 */
	@Test
	public void testGetScopesCoveringPoint1() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		final GeoScope hawaiiScopeExp = createScope("Hawaii", 20.262197,
				-154.830322, 18.885498, -156.082764);

		// now retrieve the scopes by geo points in the region
		final List<GeoScope> scopeList = GeoScopeManagement
				.getScopesCoveringPoint(new GeoPoint(19.542915, -155.665857));
		assertEquals(1, scopeList.size());
		compareGeoScope(hawaiiScopeExp, scopeList.get(0), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#getScopesCoveringPoint(GeoPoint)}
	 */
	@Test
	public void testGetScopesCoveringPoint2() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		final GeoScope hawaiiScopeExp = createScope("Hawaii", 20.262197,
				-154.830322, 18.885498, -156.082764);
		final GeoScope bothScopeExp = createScope("Honululu+Hawaii", 21.754398,
				-154.830322, 18.885498, -158.274536);

		// now retrieve the scopes by geo points in the region
		final List<GeoScope> scopeList = GeoScopeManagement
				.getScopesCoveringPoint(new GeoPoint(19.542915, -155.665857));
		assertEquals(2, scopeList.size());
		compareGeoScope(hawaiiScopeExp, scopeList.get(0), true);
		compareGeoScope(bothScopeExp, scopeList.get(1), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopes1() {
		final GeoScope honululuScopeExp = createScope("Honululu", 21.754398,
				-157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("Honululu",
				0, 10);
		assertEquals(1, scopes.size());
		compareGeoScope(honululuScopeExp, scopes.get(0), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, one containing a sub-string of another in its name,
	 * offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopes2a() {
		final GeoScope honululuScopeExp = createScope("Honululu", 21.754398,
				-157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		final GeoScope bothScopesExp = createScope("Honululu+Hawaii",
				21.754398, -154.830322, 18.885498, -158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("Honululu",
				0, 10);
		assertEquals(2, scopes.size());
		compareGeoScope(honululuScopeExp, scopes.get(0), true);
		compareGeoScope(bothScopesExp, scopes.get(1), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, one containing a sub-string of another in its name,
	 * offset 1, limit 10.
	 */
	@Test
	public void testFindGeoScopes2b() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		final GeoScope bothScopesExp = createScope("Honululu+Hawaii",
				21.754398, -154.830322, 18.885498, -158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("Honululu",
				1, 10);
		assertEquals(1, scopes.size());
		compareGeoScope(bothScopesExp, scopes.get(0), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, one containing a sub-string of another in its name,
	 * offset 2, limit 10.
	 */
	@Test
	public void testFindGeoScopes2c() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		createScope("Honululu+Hawaii", 21.754398, -154.830322, 18.885498,
				-158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("Honululu",
				2, 10);
		assertEquals(0, scopes.size());
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, searching for a non-existing scope, offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopes3() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("Bristol",
				0, 10);
		assertEquals(0, scopes.size());
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes and two existing search words, offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopes4() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		final GeoScope bothScopesExp = createScope("Honululu+Hawaii",
				21.754398, -154.830322, 18.885498, -158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes(
				"Honululu Hawaii", 0, 10);
		assertEquals(1, scopes.size());
		compareGeoScope(bothScopesExp, scopes.get(0), true);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes and one existing, one non-existing search word, offset 0,
	 * limit 10.
	 */
	@Test
	public void testFindGeoScopes5() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		createScope("Honululu+Hawaii", 21.754398, -154.830322, 18.885498,
				-158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes(
				"Honululu Bristol", 0, 10);
		assertEquals(0, scopes.size());
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes and two non-existing search words, offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopes6() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		createScope("Hawaii", 20.262197, -154.830322, 18.885498, -156.082764);
		createScope("Honululu+Hawaii", 21.754398, -154.830322, 18.885498,
				-158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes(
				"Zaragoza Bristol", 0, 10);
		assertEquals(0, scopes.size());
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, one containing a sub-string of another in its name,
	 * offset 0, limit 10000.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFindGeoScopesTooMany() {
		createScope("Honululu", 21.754398, -157.642822, 21.253542, -158.274536);
		GeoScopeManagement.findScopes("Honululu", 0, 10000);
	}

	/**
	 * Tests {@link GeoScopeManagement#findScopes(String, int, int)} with
	 * existing scopes, searching for a non-existing scope, offset 0, limit 10.
	 */
	@Test
	public void testFindGeoScopesEmpty() {
		final GeoScope honululuScopeExp = createScope("Honululu", 21.754398,
				-157.642822, 21.253542, -158.274536);
		final GeoScope hawaiiScopeExp = createScope("Hawaii", 20.262197,
				-154.830322, 18.885498, -156.082764);
		createScope("Honululu+Hawaii", 21.754398, -154.830322, 18.885498,
				-158.274536);

		final List<GeoScope> scopes = GeoScopeManagement.findScopes("", 0, 2);
		assertEquals(2, scopes.size());
		compareGeoScope(honululuScopeExp, scopes.get(0), true);
		compareGeoScope(hawaiiScopeExp, scopes.get(1), true);
	}

	/**
	 * Compares the given two geographical scopes and verifies that all their
	 * properties are equal.
	 * 
	 * @param expected
	 *            the expected geographical scope
	 * @param actual
	 *            the actual geographical scope
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareGeoScope(final GeoScope expected,
			final eu.iescities.server.accountinterface.GeoScope actual,
			final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		compareGeoScope(expected, actual.toBean(), completeExpBean);
	}

	/**
	 * Compares the given two geographical scopes and verifies that all their
	 * properties are equal.
	 * 
	 * @param expected
	 *            the expected geographical scope
	 * @param actual
	 *            the actual geographical scope
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareGeoScope(final GeoScope expected,
			final GeoScope actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getId() != -1) {
			assertEquals("getId()", expected.getId(), actual.getId());
		}
		if (completeExpBean || expected.getName() != null) {
			assertEquals("getName()", expected.getName(), actual.getName());
		} else {
			assertNotNull("getName()", actual.getName());
		}
		compareGeoPoint(expected.getNorthEastGeoPoint(),
				actual.getNorthEastGeoPoint(), completeExpBean);
		compareGeoPoint(expected.getSouthWestGeoPoint(),
				actual.getSouthWestGeoPoint(), completeExpBean);
	}

	/**
	 * Compares the given two geographical points and verifies that all their
	 * properties are equal.
	 * 
	 * @param expected
	 *            the expected geographical point
	 * @param actual
	 *            the actual geographical point
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareGeoPoint(final GeoPoint expected,
			final GeoPoint actual, final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getLatitude() != 0.0
				|| expected.getLongitude() != 0.0) {
			assertEquals("getLatitude()", expected.getLatitude(),
					actual.getLatitude(), 0.0);
			assertEquals("getLongitude()", expected.getLongitude(),
					actual.getLongitude(), 0.0);
		}
	}
}
