package eu.iescities.server.accountinterface;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Unit tests for {@link UserManagement}.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
public class UserManagementTest {
	protected static final String tempUserName = "tempUser";
	protected static final String tempUserName2 = "tempUser2";
	protected static final String tempUserPassword = "secret";
	
	/**
	 * For testing Google OAuth 2.0 methods, set this variable with the
	 * <tt>id_token</tt> contents retrieved from e.g. <a
	 * href="https://developers.google.com/oauthplayground">the Google OAuth 2.0
	 * Playground</a>
	 * <ol>
	 * <li>select "openid profile" as scope -> authorize APIs</li>
	 * <li>hit "exchange authorization code for tokens"</li>
	 * <li>copy the "id_token" field from the response into the googleIdToken variable
	 * </li>
	 * </ol>
	 */
	final String googleIdToken = "<id_token>";
	
	/**
	 * Temporary user bean object for registering a user that will be deleted
	 * after each test case.
	 * 
	 * Note: properties will be set in {@link #setUpBeforeClass()}.
	 */
	private static final User tempUser = new User();

	PersistenceManager pm;

	String adminSessionId;
	String councilSessionId;
	String developerSessionId;
	String userSessionId;

	/**
	 * Gets the default temporary user where anything could be changed.
	 * 
	 * @return a user with the same properties as {@link #tempUser}
	 * @see #setUpBeforeClass()
	 */
	protected static User getDefaultTempUser() {
		final User tempUser = new User();
		tempUser.setUsername(tempUserName);
		tempUser.setPassword(tempUserPassword);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
		return tempUser;
	}
	
	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.initDatastore();
		tempUser.setUsername(tempUserName);
		tempUser.setPassword(tempUserPassword);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	/**
	 * Executed before each test case of this class.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.pm = PMF.getPM();

		adminSessionId = TestUtils.loginAdminUser();
		councilSessionId = UserManagement.login("mike", "secret");
		developerSessionId = UserManagement.login("john", "secret");
		userSessionId = UserManagement.login("bob", "secret");

		// ensure there is no existing temp user
		final PersistenceManager pm = PMF.getPM();
		final Query query = pm.newQuery(UserManagement.queryUserByName);
		assertNull(query.execute(tempUserName));
		assertNull(query.execute(tempUserName2));
	}

	/**
	 * Executed after each test case of this class.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		final Query dsQuery = pm.newQuery(DatasetManagement.queryDatasetByName);
		dsQuery.deletePersistentAll(DatasetManagementTest.tempDatasetName);
		
		final Query query = pm.newQuery(UserManagement.queryUserByName);
		query.deletePersistentAll(tempUserName);
		query.deletePersistentAll(tempUserName2);

		try {
			UserManagement.logout(councilSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(developerSessionId);
		} catch (final Exception e) {
		}
		try {
			UserManagement.logout(userSessionId);
		} catch (final Exception e) {
		}
		pm.close();
	}

	/**
	 * Tests {@link eu.iescities.server.accountinterface.User#toBean()}.
	 */
	@Test
	public void testToBean() {
		final eu.iescities.server.accountinterface.User user1 = UserManagement
				.getUserById(pm, TestUtils.mikeUser.getUserId());
		compareUser(TestUtils.mikeUser, user1, true);
		// now test with all getters of the two objects:
		assertEquals(TestUtils.mikeUser.getUserId(), user1.getUserId());
		assertEquals(TestUtils.mikeUser.getUsername(), user1.getUsername());
//		assertEquals(TestUtils.mikeUser.getPassword(), user1.getPassword());
		assertEquals(TestUtils.mikeUser.getGoogleProfile(), user1.getGoogleProfile());
		assertEquals(TestUtils.mikeUser.getName(), user1.getName());
		assertEquals(TestUtils.mikeUser.getSurname(), user1.getSurname());
		assertEquals(TestUtils.mikeUser.getEmail(), user1.getEmail());
		assertEquals(TestUtils.mikeUser.getPreferredCouncilId(), user1.getPreferredLocation().getCouncilId());
		assertEquals(TestUtils.mikeUser.getProfile(), user1.getProfile());
//		assertEquals(TestUtils.mikeUser.getSessionId(), user1.getSessionId());
//		assertEquals(TestUtils.mikeUser.getSessionExpires(), user1.get...());
		assertEquals(TestUtils.mikeUser.isAdmin(), user1.isIesAdmin());
		assertEquals(TestUtils.mikeUser.getManagedCouncilId(), user1.getManagedCouncil().getCouncilId());
	}

	/**
	 * Tests {@link UserManagement#verifyUserId(int)}.
	 */
	@Test
	public void testVerifyUserId() {
		assertTrue(UserManagement.verifyUserId(TestUtils.adminUser.getUserId()));
		assertTrue(UserManagement.verifyUserId(TestUtils.bobUser.getUserId()));
		assertTrue(UserManagement.verifyUserId(TestUtils.johnUser.getUserId()));
		assertTrue(UserManagement.verifyUserId(TestUtils.mikeUser.getUserId()));
		assertTrue(UserManagement.verifyUserId(TestUtils.mike2User.getUserId()));
		assertFalse(UserManagement.verifyUserId(-1));
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)}.
	 */
	@Test
	public void testRegisterUser1() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			// register user
			final User registeredUser = UserManagement.registerUser(tempUser);
			// verify
			assertTrue(UserManagement.verifyUserId(registeredUser.getUserId()));
			compareUser(tempUser, registeredUser, false);
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserById(pm, registeredUser.getUserId());
			compareUser(registeredUser, user, true);
		} finally {
			pm.close();
		}
	}

	/**
	 * Test method for {@link UserManagement#registerUser(User)} with a Google
	 * profile ID.
	 */
	@Test
	@Ignore(value = "need a manually generated access token for now")
	public void testRegisterUserWithGoogle1() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			// register user, set arbitrary profile ID
			final User tempUser = new User();
			tempUser.setUsername(tempUserName);
			tempUser.setGoogleProfile(googleIdToken);
			tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
					.getCouncilId());
			final User registeredUser = UserManagement.registerUser(tempUser);
			// verify
			assertTrue(UserManagement.verifyUserId(registeredUser.getUserId()));
			tempUser.setGoogleProfile(UserManagement.verifyGoogleToken(tempUser
					.getGoogleProfile())); // the account id from the token is returned
			compareUser(tempUser, registeredUser, false);
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserById(pm, registeredUser.getUserId());
			compareUser(registeredUser, user, true);
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with an empty password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter1() {
		final User tempUser = new User();
		tempUser.setUsername(tempUserName);
		tempUser.setPassword("");
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with a too short
	 * password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter1b() {
		final User tempUser = new User();
		tempUser.setUsername(tempUserName);
		tempUser.setPassword("abc");
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with an empty username
	 * and password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter2() {
		final User tempUser = new User();
		tempUser.setUsername("");
		tempUser.setPassword("");
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with an empty username.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter3() {
		final User tempUser = new User();
		tempUser.setUsername("");
		tempUser.setPassword(tempUserPassword);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with a <tt>null</tt>
	 * username.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter3a() {
		final User tempUser = new User();
		tempUser.setUsername(null);
		tempUser.setPassword(tempUserPassword);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with a <tt>null</tt>
	 * username and password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter4() {
		final User tempUser = new User();
		tempUser.setUsername(null);
		tempUser.setPassword(null);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil
				.getCouncilId());
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} with an invalid council
	 * id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidParameter5() {
		final User tempUser = new User();
		tempUser.setUsername(tempUserName);
		tempUser.setPassword(tempUserPassword);
		// need an ID which is not set yet but generally valid, i.e. >= 0:
		tempUser.setPreferredCouncilId(Integer.MAX_VALUE);
		UserManagement.registerUser(tempUser);
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} trying to
	 * register the same username twice.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserTwice() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			// register user
			final User registeredUser = UserManagement.registerUser(tempUser);
			// verify
			assertTrue(UserManagement.verifyUserId(registeredUser.getUserId()));
			compareUser(tempUser, registeredUser, false);
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserById(pm, registeredUser.getUserId());
			compareUser(registeredUser, user, true);
			// try adding another user
			UserManagement.registerUser(tempUser);
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} trying to
	 * register the same email twice.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserNonUniqueEmail() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final User tmpUser1 = new User();
			tmpUser1.setUsername(tempUserName);
			tmpUser1.setPassword(tempUserPassword);
			tmpUser1.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
			tmpUser1.setEmail("test@test.com");
			// register user
			final User registeredUser = UserManagement.registerUser(tmpUser1);
			// verify
			assertTrue(UserManagement.verifyUserId(registeredUser.getUserId()));
			compareUser(tmpUser1, registeredUser, false);
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserById(pm, registeredUser.getUserId());
			compareUser(registeredUser, user, true);
			// try adding another user
			final User tmpUser2 = new User();
			tmpUser2.setUsername(tempUserName2);
			tmpUser2.setPassword(tempUserPassword);
			tmpUser2.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
			tmpUser2.setEmail("test@test.com");
			UserManagement.registerUser(tmpUser2);
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} trying to
	 * register with an invalid google profile <tt>id_token</tt>.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRegisterUserInvalidGoogleProfile() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final User tmpUser1 = new User();
			tmpUser1.setUsername(tempUserName);
			tmpUser1.setPassword(tempUserPassword);
			tmpUser1.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
			tmpUser1.setGoogleProfile("12345678");
			// register user
			UserManagement.registerUser(tmpUser1);
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests {@link UserManagement#registerUser(User)} trying to
	 * register the same google profile twice.
	 */
	@Test(expected = IllegalArgumentException.class)
	@Ignore(value = "need a manually generated access token for now")
	public void testRegisterUserNonUniqueGoogleProfile() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final User tmpUser1 = new User();
			tmpUser1.setUsername(tempUserName);
			tmpUser1.setPassword(tempUserPassword);
			tmpUser1.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
			tmpUser1.setGoogleProfile(googleIdToken);
			// register user
			final User registeredUser = UserManagement.registerUser(tmpUser1);
			// verify
			assertTrue(UserManagement.verifyUserId(registeredUser.getUserId()));
			compareUser(tmpUser1, registeredUser, false);
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserById(pm, registeredUser.getUserId());
			compareUser(registeredUser, user, true);
			// try adding another user
			final User tmpUser2 = new User();
			tmpUser2.setUsername(tempUserName2);
			tmpUser2.setPassword(tempUserPassword);
			tmpUser2.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
			tmpUser2.setGoogleProfile(googleIdToken);
			UserManagement.registerUser(tmpUser2);
		} finally {
			pm.close();
		}
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	public void testUpdateUser1() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setProfile(TestUtils.randomString(300));
		updateBean.setName("test2");
		updateBean.setSurname("user2");
		updateBean.setEmail("user2@test2.org");
		UserManagement.updateUser(sessionId, updateBean, null);

		user.setProfile(updateBean.getProfile());
		user.setUsername(tempUser.getUsername());
		user.setName(updateBean.getName());
		user.setSurname(updateBean.getSurname());
		user.setEmail(updateBean.getEmail());
		compareUser(user,
				UserManagement.getUserById(pm, user.getUserId()), true);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, name, surname, email.
	 */
	@Test
	public void testUpdateUser1a() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setProfile(TestUtils.randomString(300));
		updateBean.setUsername(tempUser.getUsername());
		updateBean.setName("test2");
		updateBean.setSurname("user2");
		updateBean.setEmail("user2@test2.org");
		UserManagement.updateUser(sessionId, updateBean, null);

		user.setProfile(updateBean.getProfile());
		user.setUsername(updateBean.getUsername());
		user.setName(updateBean.getName());
		user.setSurname(updateBean.getSurname());
		user.setEmail(updateBean.getEmail());
		compareUser(user,
				UserManagement.getUserById(pm, user.getUserId()), true);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} changing
	 * the user's profile, username, name, surname, email.
	 */
	@Test
	public void testUpdateUser2() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setProfile(TestUtils.randomString(300));
		updateBean.setUsername(tempUserName2);
		updateBean.setName("test2");
		updateBean.setSurname("user2");
		updateBean.setEmail("user2@test2.org");
		UserManagement.updateUser(sessionId, updateBean, null);

		user.setProfile(updateBean.getProfile());
		user.setUsername(updateBean.getUsername());
		user.setName(updateBean.getName());
		user.setSurname(updateBean.getSurname());
		user.setEmail(updateBean.getEmail());
		compareUser(user,
				UserManagement.getUserById(pm, user.getUserId()), true);
	}

	/**
	 * Test method for {@link UserManagement#updateUser(String, User, String)}
	 * changing the password.
	 */
	@Test
	public void testUpdateUserPassword1() {
		UserManagement.registerUser(tempUser);

		String sessionId = UserManagement.login(tempUserName, tempUserPassword);
		final User changeBean = new User();
		changeBean.setPassword(tempUserPassword + "X");
		UserManagement.updateUser(sessionId, changeBean, tempUserPassword);
		UserManagement.logout(sessionId);
		sessionId = UserManagement.login(tempUserName, tempUserPassword + "X");
		assertNotNull(sessionId);
		assertTrue(sessionId.length() > 0);

		try {
			UserManagement.login(tempUserName, tempUserPassword);
			fail();
		} catch (final IllegalArgumentException e) {
			// ok - we should not be able to login with the old password!
		}
	}

	/**
	 * Test method for {@link UserManagement#updateUser(String, User, String)}
	 * changing the password but using a wrong old password.
	 */
	@Test
	public void testUpdateUserPassword2() {
		UserManagement.registerUser(tempUser);

		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		try {
			final User changeBean = new User();
			changeBean.setPassword(tempUserPassword + "X");
			UserManagement.updateUser(sessionId, changeBean, tempUserPassword + "Y");
			fail();
		} catch (final IllegalArgumentException e) {
			// verify the session is still valid:
			UserManagement.getUserBySession(sessionId);
		}
	}

	/**
	 * Test method for {@link UserManagement#updateUser(String, User, String)}
	 * changing the password but using a wrong old password.
	 */
	@Test
	public void testUpdateUserLongPassword() {
		UserManagement.registerUser(tempUser);

		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		final User changeBean = new User();
		changeBean.setPassword(TestUtils.randomString(260));
		UserManagement.updateUser(sessionId, changeBean, tempUserPassword);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with a
	 * username longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserTooLongUsername() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setUsername(TestUtils.randomString(260));
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with
	 * an empty username.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserEmptyUsername() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);

		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setUsername("");
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with a
	 * name longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserTooLongName() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setName(TestUtils.randomString(260));
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with a
	 * surname longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserTooLongSurname() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setSurname(TestUtils.randomString(260));
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with an
	 * email longer than 255 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserTooLongEmail() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setEmail(TestUtils.randomString(260));
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with an
	 * invalid email.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserInvalidEmail() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setEmail("foo@example");
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#updateUser(String, User, String)} with a
	 * profile longer than 65535 characters.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUpdateUserTooLongProfile() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		
		final User updateBean = new User();
		updateBean.setUserId(user.getUserId());
		updateBean.setProfile(TestUtils.randomString(66000));
		UserManagement.updateUser(sessionId, updateBean, null);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(String)}.
	 */
	@Test
	public void testGetUserBySession1() {
		final User johnUser = UserManagement
				.getUserBySession(developerSessionId);
		compareUser(TestUtils.johnUser, johnUser, true);

		final User mikeUser = UserManagement.getUserBySession(councilSessionId);
		compareUser(TestUtils.mikeUser, mikeUser, true);

		final User bobUser = UserManagement.getUserBySession(userSessionId);
		compareUser(TestUtils.bobUser, bobUser, true);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(String)} after logout.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserBySession2() {
		UserManagement.logout(developerSessionId);
		UserManagement.getUserBySession(developerSessionId);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(String)} with a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserBySession3() {
		UserManagement.getUserBySession(UUID.randomUUID().toString());
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)}.
	 */
	@Test
	public void testGetUserById() {
		final User johnUser = UserManagement.getUserById(adminSessionId,
				TestUtils.johnUser.getUserId());
		compareUser(TestUtils.johnUser, johnUser, true);

		final User mikeUser = UserManagement.getUserById(adminSessionId,
				TestUtils.mikeUser.getUserId());
		compareUser(TestUtils.mikeUser, mikeUser, true);

		final User bobUser = UserManagement.getUserById(adminSessionId,
				TestUtils.bobUser.getUserId());
		compareUser(TestUtils.bobUser, bobUser, true);
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)} with a non-existing
	 * user id.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUserByIdNonExisting1() {
		UserManagement.getUserById(adminSessionId, -1);
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)} with a non-admin
	 * user session.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserByIdUnauthorised1() {
		UserManagement.getUserById(userSessionId,
				TestUtils.johnUser.getUserId());
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)} with a non-admin
	 * developer session.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserByIdUnauthorised2() {
		UserManagement.getUserById(developerSessionId,
				TestUtils.johnUser.getUserId());
	}

	/**
	 * Tests {@link UserManagement#getUserById(String, int)} with a non-admin
	 * council session.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserByIdUnauthorised3() {
		UserManagement.getUserById(councilSessionId,
				TestUtils.johnUser.getUserId());
	}

	/**
	 * Tests {@link UserManagement#getInstalledApps(String, Language, Language)}.
	 */
	@Test
	public void testGetInstalledApps1() {
		final List<Application> apps = UserManagement
				.getInstalledApps(userSessionId, Language.EN, null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
				.next(), true);
	}

	/**
	 * Tests {@link UserManagement#getInstalledApps(String, Language, Language)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetInstalledApps2() {
		UserManagement.getInstalledApps(UUID.randomUUID().toString(),
				Language.EN, null);
	}

	/**
	 * Tests {@link UserManagement#addInstalledApp(String, int)}.
	 */
	@Test
	public void testAddInstalledApps1() {
		UserManagement.registerUser(tempUser);
		final String tempUserSession = UserManagement.login(tempUserName,
				tempUserPassword);

		UserManagement.addInstalledApp(tempUserSession,
				TestUtils.app1.getAppId());

		final List<Application> apps = UserManagement
				.getInstalledApps(tempUserSession, Language.EN, null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
				.next(), true);
	}

	/**
	 * Tests {@link UserManagement#addInstalledApp(String, int)} with a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddInstalledApps2() {
		UserManagement.addInstalledApp(UUID.randomUUID().toString(),
				TestUtils.app1.getAppId());
	}

	/**
	 * Tests {@link UserManagement#addInstalledApp(String, int)} with a
	 * non-existing app ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddInstalledAppsNonExisting1() {
		UserManagement.registerUser(tempUser);
		final String tempUserSession = UserManagement.login(tempUserName,
				tempUserPassword);

		UserManagement.addInstalledApp(tempUserSession, -1);
	}

	/**
	 * Tests {@link UserManagement#removeInstalledApp(String, int)}.
	 */
	@Test
	public void testRemoveInstalledApps1() {
		UserManagement.registerUser(tempUser);
		final String tempUserSession = UserManagement.login(tempUserName,
				tempUserPassword);

		UserManagement.addInstalledApp(tempUserSession,
				TestUtils.app1.getAppId());

		List<Application> apps = UserManagement
				.getInstalledApps(tempUserSession, Language.EN, null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
				.next(), true);

		UserManagement.removeInstalledApp(tempUserSession,
				TestUtils.app1.getAppId());
		apps = UserManagement.getInstalledApps(tempUserSession, Language.EN,
				null);
		assertEquals(0, apps.size());
	}

	/**
	 * Tests {@link UserManagement#removeInstalledApp(String, int)} with a
	 * random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveInstalledApps2() {
		UserManagement.removeInstalledApp(UUID.randomUUID().toString(),
				TestUtils.app1.getAppId());
	}

	/**
	 * Tests {@link UserManagement#removeInstalledApp(String, int)} with a
	 * non-existing app ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveInstalledAppsNonExisting1() {
		UserManagement.registerUser(tempUser);
		final String tempUserSession = UserManagement.login(tempUserName,
				tempUserPassword);

		UserManagement.removeInstalledApp(tempUserSession, -1);
	}

	/**
	 * Tests {@link UserManagement#getDevelopedApps(String, Language, Language)}.
	 */
	@Test
	public void testGetDevelopedApps() {
		List<Application> apps;
		apps = UserManagement.getDevelopedApps(councilSessionId, Language.EN,
				null);
		assertEquals(0, apps.size());

		apps = UserManagement.getDevelopedApps(developerSessionId, Language.EN,
				null);
		assertEquals(1, apps.size());
		ApplicationManagementTest.compareApp(TestUtils.app1, apps.iterator()
				.next(), true);

		// no translation available but still the app exists:
		apps = UserManagement.getDevelopedApps(developerSessionId, Language.ES,
				null);
		assertEquals(1, apps.size());
		final Application appNoLang = new Application();
		appNoLang.setAppId(TestUtils.app1.getAppId());
		appNoLang.setDownloadNumber(TestUtils.app1.getDownloadNumber());
		appNoLang.setAccessNumber(TestUtils.app1.getAccessNumber());
		appNoLang.setUrl(TestUtils.app1.getUrl());
		appNoLang.setRelatedCouncilId(TestUtils.app1.getRelatedCouncilId());
		appNoLang.setGeographicalScope(TestUtils.app1.getGeographicalScope());
		appNoLang.setGooglePlayRating(TestUtils.app1.getGooglePlayRating());
		appNoLang.setVersion(TestUtils.app1.getVersion());
		appNoLang.setPermissions(TestUtils.app1.getPermissions());
		appNoLang.setPackageName(TestUtils.app1.getPackageName());
		// no translation for these fields:
		appNoLang.setName("");
		appNoLang.setDescription("");
		appNoLang.setTermsOfService("");
		appNoLang.setTags(new HashSet<String>());
		ApplicationManagementTest.compareApp(appNoLang, apps.iterator().next(),
				true);
	}

	/**
	 * Tests
	 * {@link UserManagement#getUsedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 0.
	 */
	@Test
	public void testGetUsedDataSets1() {
		List<Dataset> datasets;

		datasets = UserManagement.getUsedDataSets(councilSessionId, 0, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());

		datasets = UserManagement.getUsedDataSets(developerSessionId, 0, 10,
				Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);
	}

	/**
	 * Tests
	 * {@link UserManagement#getUsedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetUsedDataSets2() {
		List<Dataset> datasets;

		datasets = UserManagement.getUsedDataSets(councilSessionId, 1, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());

		datasets = UserManagement.getUsedDataSets(developerSessionId, 1, 10,
				Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests
	 * {@link UserManagement#getUsedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10000, offset 1.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUsedDataSetsTooMany() {
		UserManagement.getUsedDataSets(councilSessionId, 0, 10000, Language.EN,
				null);
	}

	/**
	 * Tests
	 * {@link UserManagement#getPublishedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 0.
	 */
	@Test
	public void testGetPublishedDataSets1() {
		List<Dataset> datasets;

		datasets = UserManagement.getPublishedDataSets(councilSessionId, 0, 10,
				Language.EN, null);
		assertEquals(2, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset1,
				datasets.get(0), true);
		DatasetManagementTest.compareDataset(TestUtils.dataset2,
				datasets.get(1), true);

		datasets = UserManagement.getPublishedDataSets(developerSessionId, 0,
				10, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests
	 * {@link UserManagement#getPublishedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10, offset 1.
	 */
	@Test
	public void testGetPublishedDataSets2() {
		List<Dataset> datasets;

		datasets = UserManagement.getPublishedDataSets(councilSessionId, 1, 10,
				Language.EN, null);
		assertEquals(1, datasets.size());
		DatasetManagementTest.compareDataset(TestUtils.dataset2,
				datasets.get(0), true);

		datasets = UserManagement.getPublishedDataSets(developerSessionId, 1,
				10, Language.EN, null);
		assertEquals(0, datasets.size());
	}

	/**
	 * Tests
	 * {@link UserManagement#getPublishedDataSets(String, int, int, Language, Language)}
	 * with a limit of 10000, offset 1.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetPublishedDataSetsTooMany() {
		UserManagement.getPublishedDataSets(councilSessionId, 0, 10000,
				Language.EN, null);
	}

	/**
	 * Tests {@link UserManagement#addIesAdmin(String, int)} with a valid user
	 * ID.
	 */
	@Test
	public void testAddIesAdmin1() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(adminSessionId, userId);
		assertTrue(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		final String userSession = UserManagement.login(tempUserName,
				tempUserPassword);
		assertTrue(UserManagement.getUserBySession(userSession).isAdmin());
	}

	/**
	 * Tests {@link UserManagement#addIesAdmin(String, int)} with a valid user
	 * ID setting the property twice.
	 */
	@Test
	public void testAddIesAdmin2() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(adminSessionId, userId);
		assertTrue(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		final String userSession = UserManagement.login(tempUserName,
				tempUserPassword);
		assertTrue(UserManagement.getUserBySession(userSession).isAdmin());

		UserManagement.addIesAdmin(adminSessionId, userId);
		assertTrue(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		assertTrue(UserManagement.getUserBySession(userSession).isAdmin());
	}

	/**
	 * Tests {@link UserManagement#addIesAdmin(String, int)} with an invalid
	 * user ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddIesAdminInvalidId() {
		UserManagement.addIesAdmin(adminSessionId, -1);
	}

	/**
	 * Tests {@link UserManagement#addIesAdmin(String, int)} with an
	 * unauthorised admin session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddIesAdminUnauthorised1() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(userSessionId, userId);
	}

	/**
	 * Tests {@link UserManagement#addIesAdmin(String, int)} with an
	 * unauthorised admin session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testAddIesAdminUnauthorised2() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(councilSessionId, userId);
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with a valid
	 * admin user ID.
	 */
	@Test
	public void testRemoveIesAdmin1() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(adminSessionId, userId);
		UserManagement.removeIesAdmin(adminSessionId, userId);
		assertFalse(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		final String userSession = UserManagement.login(tempUserName,
				tempUserPassword);
		assertFalse(UserManagement.getUserBySession(userSession).isAdmin());
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with a valid
	 * non-admin user ID.
	 */
	@Test
	public void testRemoveIesAdmin2() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.removeIesAdmin(adminSessionId, userId);
		assertFalse(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		final String userSession = UserManagement.login(tempUserName,
				tempUserPassword);
		assertFalse(UserManagement.getUserBySession(userSession).isAdmin());
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with a valid
	 * admin user ID removing the property twice.
	 */
	@Test
	public void testRemoveIesAdmin3() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.addIesAdmin(adminSessionId, userId);
		UserManagement.removeIesAdmin(adminSessionId, userId);
		assertFalse(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		final String userSession = UserManagement.login(tempUserName,
				tempUserPassword);
		assertFalse(UserManagement.getUserBySession(userSession).isAdmin());

		UserManagement.removeIesAdmin(adminSessionId, userId);
		assertFalse(UserManagement.getUserById(adminSessionId, userId)
				.isAdmin());
		assertFalse(UserManagement.getUserBySession(userSession).isAdmin());
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with an invalid
	 * user ID.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveIesAdminInvalidId() {
		UserManagement.removeIesAdmin(adminSessionId, -1);
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with an
	 * unauthorised admin session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveIesAdminUnauthorised1() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.removeIesAdmin(userSessionId, userId);
	}

	/**
	 * Tests {@link UserManagement#removeIesAdmin(String, int)} with an
	 * unauthorised admin session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testRemoveIesAdminUnauthorised2() {
		final int userId = UserManagement.registerUser(tempUser).getUserId();

		UserManagement.removeIesAdmin(councilSessionId, userId);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(PersistenceManager, String)}
	 * .
	 */
	@Test
	public void testGetUserBySessionPersistenceManagerString() {
		final eu.iescities.server.accountinterface.User user = UserManagement
				.getUserBySession(pm, developerSessionId);
		compareUser(TestUtils.johnUser, user, true);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(PersistenceManager, String)}
	 * with an invalid session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserBySessionPersistenceManagerStringInvalidSession1() {
		UserManagement.logout(developerSessionId);
		UserManagement.getUserBySession(pm, developerSessionId);
	}

	/**
	 * Tests {@link UserManagement#getUserBySession(PersistenceManager, String)}
	 * with a random session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testGetUserBySessionPersistenceManagerStringInvalidSession2() {
		UserManagement.getUserBySession(pm, UUID.randomUUID().toString());
	}

	/**
	 * Tests {@link UserManagement#validateUserSession(String)}.
	 */
	@Test
	public void testValidateUserSession1() {
		UserManagement.validateUserSession(developerSessionId);
	}

	/**
	 * Tests {@link UserManagement#validateUserSession(String)} after user
	 * logout.
	 */
	@Test(expected = SecurityException.class)
	public void testValidateUserSession2() {
		UserManagement.logout(developerSessionId);
		UserManagement.validateUserSession(developerSessionId);
	}

	/**
	 * Tests {@link UserManagement#validateUserSession(String)} with a random
	 * session ID.
	 */
	@Test(expected = SecurityException.class)
	public void testValidateUserSession3() {
		UserManagement.validateUserSession(UUID.randomUUID().toString());
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)}.
	 */
	@Test
	public void testLogin1() {
		// register user
		final User registeredUser = UserManagement.registerUser(tempUser);

		// log in
		final String returnVal = UserManagement.login(tempUserName,
				tempUserPassword);
		assertNotNull(returnVal);
		assertTrue(returnVal.length() > 0);
		// verify session ID is working:
		final User user = UserManagement.getUserBySession(returnVal);
		compareUser(registeredUser, user, true);
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with an
	 * empty password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testLogin2() {
		// register user
		UserManagement.registerUser(tempUser);

		// try erroneous parameters
		UserManagement.login(tempUserName, "");
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with a wrong
	 * password.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testLogin3() {
		// register user
		UserManagement.registerUser(tempUser);

		// try erroneous parameters
		UserManagement.login(tempUserName,
				tempUserPassword.substring(0, tempUserPassword.length() - 1));
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with a wrong
	 * password trying to login after the failed login attempt.
	 */
	@Test
	public void testLogin4() {
		// register user
		UserManagement.registerUser(tempUser);

		// try erroneous parameters
		try {
			final String returnVal = UserManagement.login(tempUserName,
					"'' || 1");
			assertTrue(returnVal == null || returnVal.length() == 0);
		} catch (final IllegalArgumentException e) {
		}

		// log in and verify it is still working
		final String returnVal = UserManagement.login(tempUserName,
				tempUserPassword);
		assertTrue(returnVal != null && returnVal.length() > 0);
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with an
	 * expired session time.
	 */
	@Test
	public void testLogin5() {
		final long defaultSessionTime = eu.iescities.server.accountinterface.User
				.getDefaultSessionTime();
		try {
			// register user
			UserManagement.registerUser(tempUser);

			// log in
			eu.iescities.server.accountinterface.User.setDefaultSessionTime(0);
			String returnVal = UserManagement.login(tempUserName,
					tempUserPassword);
			assertNotNull(returnVal);
			assertTrue(returnVal.length() > 0);
			Thread.sleep(10);
			// verify session ID is NOT working:
			try {
				UserManagement.getUserBySession(returnVal);
				fail("Session ID should not be valid any more");
			} catch (final SecurityException e) {
			}

			// log in and verify it is still working
			returnVal = UserManagement.login(tempUserName, tempUserPassword);
			assertTrue(returnVal != null && returnVal.length() > 0);
		} catch (final InterruptedException e) {
		} finally {
			eu.iescities.server.accountinterface.User
					.setDefaultSessionTime(defaultSessionTime);
		}
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with a
	 * valid session time extending the sessionExpires attribute during access.
	 */
	@Test
	public void testLogin6() {
		final long defaultSessionTime = eu.iescities.server.accountinterface.User
				.getDefaultSessionTime();
		try {
			// register user
			UserManagement.registerUser(tempUser);

			// log in
			eu.iescities.server.accountinterface.User.setDefaultSessionTime(1000);
			final String returnVal = UserManagement.login(tempUserName,
					tempUserPassword);
			assertNotNull(returnVal);
			assertTrue(returnVal.length() > 0);
			final User user1 = UserManagement.getUserBySession(returnVal);
			Thread.sleep(400);
			final User user2 = UserManagement.getUserBySession(returnVal);
			assertTrue("exp old = " + user1.getSessionExpires() + ", exp new= "
					+ user2.getSessionExpires(), user2.getSessionExpires()
					.after(user1.getSessionExpires()));
		} catch (final InterruptedException e) {
		} finally {
			eu.iescities.server.accountinterface.User
					.setDefaultSessionTime(defaultSessionTime);
		}
	}

	/**
	 * Test method for {@link UserManagement#login(String, String)} with a
	 * <tt>null</tt> user name.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testLoginNonExisting1() {
		// try erroneous parameters
		UserManagement.login("NonExistingUser",
				tempUserPassword.substring(0, tempUserPassword.length() - 1));
	}

	/**
	 * Test method for {@link UserManagement#registerUser(User)} and
	 * {@link UserManagement#loginWithGoogle(String)} with a Google profile ID.
	 */
	@Test
	@Ignore(value = "need a manually generated access token for now")
	public void testLoginWithGoogle1() {
		User tempUser = new User();
		tempUser.setUsername(tempUserName);
		tempUser.setGoogleProfile(googleIdToken);
		tempUser.setPreferredCouncilId(TestUtils.honululuCouncil.getCouncilId());
		tempUser = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.loginWithGoogle(googleIdToken);
		// verify session ID is working:
		final User user = UserManagement.getUserBySession(sessionId);
		compareUser(tempUser, user, true);
	}

	/**
	 * Test method for {@link UserManagement#loginWithGoogle(String)} with an
	 * invalid access token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testLoginWithGoogleUnauthorised1() {
		final String authToken = "foo";
		UserManagement.loginWithGoogle(authToken);
	}

	/**
	 * Test method for {@link UserManagement#logout(String)}.
	 */
	@Test
	public void testLogout() {
		// verify that the session is valid:
		UserManagement.getUserBySession(userSessionId);
		// log out
		UserManagement.logout(userSessionId);
		try {
			UserManagement.getUserBySession(userSessionId);
			fail("user session should be invalid after registration");
		} catch (final SecurityException e) {
		}
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)},
	 * {@link UserManagement#getUserByPasswordResetToken(String)} and
	 * {@link UserManagement#resetPasswordWithResetToken(String, String)}.
	 */
	@Test
	public void testCreatePasswordResetRequest1() {
		User user = new User();
		user.setUsername(tempUserName);
		user.setPassword(tempUserPassword);
		user.setEmail("temp@user.me");
		user = UserManagement.registerUser(user);
		
		final String token = UserManagement.createPasswordResetRequest("temp@user.me");

		// creating a token should not influence the user logging in
		String sessionId = UserManagement.login(tempUserName, tempUserPassword);
		User sessionUser = UserManagement.getUserBySession(sessionId);
		compareUser(user, sessionUser, true);

		// retrieve the user by the token and try to reset the password
		final User tokenUser = UserManagement.getUserByPasswordResetToken(token);
		compareUser(user, tokenUser, true);
		UserManagement.resetPasswordWithResetToken(token, tempUserPassword + 'X');
		sessionId = UserManagement.login(tempUserName, tempUserPassword + 'X');
		sessionUser = UserManagement.getUserBySession(sessionId);
		compareUser(user, sessionUser, true);
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} removing a
	 * user after creating a password reset token with
	 * {@link UserManagement#createPasswordResetRequest(String)}.
	 */
	@Test
	public void testCreatePasswordResetRequest2() {
		
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final Query query = pm.newQuery(UserManagement.queryUserByName);

			User user = new User();
			user.setUsername(tempUserName);
			user.setPassword(tempUserPassword);
			user.setEmail("temp@user.me");
			user = UserManagement.registerUser(user);

			UserManagement.createPasswordResetRequest("temp@user.me");
			assertEquals(1, UserManagement.removeUser(adminSessionId, user.getUserId()));
			assertNull(query.execute(tempUserName));
		} finally {
			pm.close();
		}
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * with an empty email address.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreatePasswordResetRequestEmptyEmail() {
		UserManagement.createPasswordResetRequest("");
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * with a non-existing email address.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreatePasswordResetRequestNonExistingEmail() {
		UserManagement.createPasswordResetRequest("non_existing@user.me");
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * with an invalid email address.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreatePasswordResetRequestInvalidEmail() {
		UserManagement.createPasswordResetRequest("invalid@user");
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * with a too long email address.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCreatePasswordResetRequestTooLongEmail() {
		UserManagement.createPasswordResetRequest(TestUtils.randomString(260));
	}

	/**
	 * Test method for {@link UserManagement#getUserByPasswordResetToken(String)}
	 * with an empty token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUserByPasswordResetTokenEmpty() {
		UserManagement.getUserByPasswordResetToken("");
	}

	/**
	 * Test method for {@link UserManagement#getUserByPasswordResetToken(String)}
	 * with a too long token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUserByPasswordResetTokenTooLong() {
		UserManagement.getUserByPasswordResetToken(TestUtils.randomString(33));
	}

	/**
	 * Test method for {@link UserManagement#getUserByPasswordResetToken(String)}
	 * with a non-existing token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUserByPasswordResetTokenNonExisting() {
		UserManagement.getUserByPasswordResetToken("non-existing");
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * , {@link UserManagement#getUserByPasswordResetToken(String)} with an
	 * expired token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetUserByPasswordResetTokenExpired() {
		User user = new User();
		user.setUsername(tempUserName);
		user.setPassword(tempUserPassword);
		user.setEmail("temp@user.me");
		user = UserManagement.registerUser(user);
		final long defaultTimeout = UserManagement.getPasswordResetTimeout();
		
		try {
			UserManagement.setPasswordResetTimeout(0);
			
			final String token = UserManagement.createPasswordResetRequest("temp@user.me");
			Thread.sleep(10);
			UserManagement.getUserByPasswordResetToken(token);
		} catch (final InterruptedException e) {
		} finally {
			UserManagement.setPasswordResetTimeout(defaultTimeout);
		}
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * and {@link UserManagement#resetPasswordWithResetToken(String, String)}
	 * with an empty token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testResetPasswordWithResetTokenEmpty() {
		UserManagement.resetPasswordWithResetToken("", tempUserPassword + 'X');
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * and {@link UserManagement#resetPasswordWithResetToken(String, String)}
	 * with a too long token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testResetPasswordWithResetTokenTooLong() {
		UserManagement.resetPasswordWithResetToken(TestUtils.randomString(33),
				tempUserPassword + 'X');
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * and {@link UserManagement#resetPasswordWithResetToken(String, String)}
	 * with a non-existing token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testResetPasswordWithResetTokenNonExisting() {
		UserManagement.resetPasswordWithResetToken("non-existing",
				tempUserPassword + 'X');
	}

	/**
	 * Test method for {@link UserManagement#createPasswordResetRequest(String)}
	 * and {@link UserManagement#resetPasswordWithResetToken(String, String)}
	 * with an expired token.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testResetPasswordWithResetTokenExpired() {
		User user = new User();
		user.setUsername(tempUserName);
		user.setPassword(tempUserPassword);
		user.setEmail("temp@user.me");
		user = UserManagement.registerUser(user);
		final long defaultTimeout = UserManagement.getPasswordResetTimeout();
		
		try {
			UserManagement.setPasswordResetTimeout(0);
			
			final String token = UserManagement.createPasswordResetRequest("temp@user.me");
			Thread.sleep(10);
			UserManagement.resetPasswordWithResetToken(token, tempUserPassword + 'X');
		} catch (final InterruptedException e) {
		} finally {
			UserManagement.setPasswordResetTimeout(defaultTimeout);
			// login should still work with the old password:
			final String sessionId = UserManagement.login(tempUserName, tempUserPassword);
			final User sessionUser = UserManagement.getUserBySession(sessionId);
			compareUser(user, sessionUser, true);
		}
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 1, offset 0.
	 */
	@Test
	public void testGetAllUsers1() {
		final List<User> users = UserManagement.getAllUsers(adminSessionId, 0,
				1);
		assertEquals(1, users.size());
		compareUser(TestUtils.adminUser, users.get(0), true);
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 2, offset 0.
	 */
	@Test
	public void testGetAllUsers2() {
		final List<User> users = UserManagement.getAllUsers(adminSessionId, 0,
				2);
		assertEquals(2, users.size());
		compareUser(TestUtils.adminUser, users.get(0), true);
		compareUser(TestUtils.bobUser, users.get(1), true);
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 10, offset 1.
	 */
	@Test
	public void testGetAllUsers3() {
		final List<User> users = UserManagement.getAllUsers(adminSessionId, 1,
				10);
		assertEquals(4, users.size());
		compareUser(TestUtils.bobUser, users.get(0), true);
		compareUser(TestUtils.johnUser, users.get(1), true);
		compareUser(TestUtils.mikeUser, users.get(2), true);
		compareUser(TestUtils.mike2User, users.get(3), true);
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with an
	 * unauthorised user session.
	 */
	@Test(expected = SecurityException.class)
	public void testGetAllUsersInvalid() {
		UserManagement.getAllUsers(userSessionId, 0, 2);
	}

	/**
	 * Tests {@link UserManagement#getAllUsers(String, int, int)} with a limit
	 * of 10000, offset 2.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetAllUsersTooMany() {
		UserManagement.getAllUsers(userSessionId, 2, 10000);
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)}.
	 */
	@Test
	public void testRemoveUser1() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final Query query = pm.newQuery(UserManagement.queryUserByName);
			final int userId = UserManagement.registerUser(tempUser)
					.getUserId();

			assertEquals(1, UserManagement.removeUser(adminSessionId, userId));

			assertNull(query.execute(tempUserName));
		} finally {
			pm.close();
		}
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with a
	 * user who added a dataset.
	 */
	@Test
	public void testRemoveUser1b() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final Query query = pm.newQuery(UserManagement.queryUserByName);
			final int userId = UserManagement.registerUser(tempUser)
					.getUserId();
			CouncilManagement.addCouncilAdmin(adminSessionId,
					TestUtils.hawaiiCouncil.getCouncilId(), userId);
			final String sessionId = UserManagement.login(tempUserName,
					tempUserPassword);
			final Dataset tempDs = new Dataset();
			tempDs.setLang(Language.EN);
			tempDs.setName(DatasetManagementTest.tempDatasetName);
			tempDs.setDescription("descr");
			DatasetManagement.registerDataset(sessionId, tempDs);

			assertEquals(1, UserManagement.removeUser(adminSessionId, userId));

			assertNull(query.execute(tempUserName));
		} finally {
			pm.close();
		}
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with an
	 * invalid user ID.
	 */
	@Test
	public void testRemoveUser2() {
		assertEquals(0, UserManagement.removeUser(adminSessionId, -1));
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with an
	 * unauthorised session ID (the user himself cannot call the admin-function
	 * even to delete his account!).
	 */
	@Test
	public void testRemoveUserUnauthorised1() {
		final User user = UserManagement.registerUser(tempUser);
		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		testRemoveUserUnauthorised(sessionId, user,
				"removing own user with admin function must fail");
	}

	private void testRemoveUserUnauthorised(final String sessionId,
			final User user, final String failMsg)
			throws IllegalArgumentException, SecurityException {
		try {
			UserManagement.removeUser(sessionId, user.getUserId());
			fail(failMsg);
		} catch (final SecurityException e) {
			// user must still exist
			compareUser(user,
					UserManagement.getUserById(pm, user.getUserId()), true);
		}
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with an
	 * unauthorised session ID (another user).
	 */
	@Test
	public void testRemoveUserUnauthorised2() {
		final User user = UserManagement.registerUser(tempUser);
		testRemoveUserUnauthorised(userSessionId, user,
				"removing other user with admin function must fail if not admin");
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with an
	 * unauthorised session ID (a council admin).
	 */
	@Test
	public void testRemoveUserUnauthorised3() {
		final User user = UserManagement.registerUser(tempUser);
		testRemoveUserUnauthorised(councilSessionId, user,
				"removing other user with admin function must fail if not admin");
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, int)} with an
	 * invalid admin session ID (logged out).
	 */
	@Test
	public void testRemoveUserInvalidSession1() {
		final User user = UserManagement.registerUser(tempUser);
		UserManagement.logout(adminSessionId);
		testRemoveUserUnauthorised(councilSessionId, user,
				"removing other user with admin function must fail if not logged in (as admin)");
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, String)}.
	 */
	@Test
	public void testRemoveSelfUser1() {
		final PersistenceManager pm = PMF.getPM();
		pm.setIgnoreCache(true);
		try {
			final Query query = pm.newQuery(UserManagement.queryUserByName);
			UserManagement.registerUser(tempUser);

			final String sessionId = UserManagement.login(tempUserName,
					tempUserPassword);
			assertEquals(1, UserManagement.removeUser(sessionId, tempUserPassword));

			assertNull(query.execute(tempUserName));
		} finally {
			pm.close();
		}
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, String)} with a
	 * wrong password.
	 */
	@Test
	public void testRemoveSelfUserUnauthorised1() {
		final User user = UserManagement.registerUser(tempUser);

		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		try {
			UserManagement.removeUser(sessionId, tempUserPassword + "x");
			fail("deleting own user must fail with wrong password");
		} catch (final IllegalArgumentException e) {
			// user must still exist
			compareUser(user,
					UserManagement.getUserById(pm, user.getUserId()), true);
		}
	}

	/**
	 * Test method for {@link UserManagement#removeUser(String, String)} with an
	 * invalid session ID (logged out).
	 */
	@Test
	public void testRemoveSelfUserInvalidSession1() {
		final User user = UserManagement.registerUser(tempUser);

		final String sessionId = UserManagement.login(tempUserName,
				tempUserPassword);
		UserManagement.logout(sessionId);
		try {
			UserManagement.removeUser(sessionId, tempUserPassword);
			fail("deleting own user must fail if logged out");
		} catch (final SecurityException e) {
			// user must still exist
			compareUser(user,
					UserManagement.getUserById(pm, user.getUserId()), true);
		}
	}

	/**
	 * Compares the given two users and verifies that all their properties
	 * (except session ID and session expiry date) are equal.
	 * 
	 * @param expected
	 *            the expected user
	 * @param actual
	 *            the actual user
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareUser(final User expected,
			final eu.iescities.server.accountinterface.User actual,
			final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		compareUser(expected, actual.toBean(), completeExpBean);
	}

	/**
	 * Compares the given two users and verifies that all their properties
	 * (except session ID and session expiry date) are equal.
	 * 
	 * @param expected
	 *            the expected user
	 * @param actual
	 *            the actual user
	 * @param completeExpBean
	 *            whether the expected user bean is complete or not
	 */
	protected static void compareUser(final User expected, final User actual,
			final boolean completeExpBean) {
		if (expected == null) {
			assertNull(actual);
			return;
		}
		assertNotNull(actual);
		if (completeExpBean || expected.getUserId() != -1) {
			assertEquals("getUserId()", expected.getUserId(), actual.getUserId());
		} else {
			assertNotEquals("getUserId()", actual.getUserId(), -1);
		}
//		assertEquals("getSessionId()", expected.getSessionId(),
//				actual.getSessionId());
//		assertEquals("getSessionExpires()", expected.getSessionExpires(),
//				actual.getSessionExpires());
		assertEquals("getUsername()", expected.getUsername(),
				actual.getUsername());
		assertNull("getPassword()", actual.getPassword()); // never returned
		if (completeExpBean || expected.getGoogleProfile() != null) {
			assertEquals("getGoogleProfile()", expected.getGoogleProfile(),
					actual.getGoogleProfile());
		}
		if (completeExpBean || expected.getName() != null) {
			assertEquals("getName()", expected.getName(), actual.getName());
		} else {
			assertNotNull("getName()", actual.getName());
		}
		if (completeExpBean || expected.getSurname() != null) {
			assertEquals("getSurname()", expected.getSurname(),
					actual.getSurname());
		} else {
			assertNotNull("getSurname()", actual.getSurname());
		}
		if (completeExpBean || expected.getEmail() != null) {
			assertEquals("getEmail()", expected.getEmail(), actual.getEmail());
		} else {
			assertNotNull("getEmail()", actual.getEmail());
		}
		if (completeExpBean || expected.getProfile() != null) {
			assertEquals("getProfile()", expected.getProfile(),
					actual.getProfile());
		} else {
			assertNotNull("getProfile()", actual.getProfile());
		}
		assertEquals("getPreferredCouncilId()",
				expected.getPreferredCouncilId(),
				actual.getPreferredCouncilId());
		assertEquals("getManagedCouncilId()",
				expected.getManagedCouncilId(),
				actual.getManagedCouncilId());
		assertEquals("isAdmin()", expected.isAdmin(), actual.isAdmin());
		assertEquals("isDeveloper()", expected.isDeveloper(),
				actual.isDeveloper());
	}

	/**
	 * Checks that only the user id, name, surname, isAdmin and isDeveloper
	 * properties are set the the given user and the rest is null.
	 * 
	 * @param privateUser
	 *            the user to verify his privacy
	 * @param dbUser
	 *            the DB user to verify
	 * @param isDeveloper
	 *            whether the user now is a developer (may have changed since
	 *            the DB user object creation)
	 */
	protected static void isPrivateUser(final User privateUser,
			final User dbUser, final boolean isDeveloper) {
		// public data:
		assertEquals("getUserId()", dbUser.getUserId(),
				privateUser.getUserId());
		assertEquals("getName()", dbUser.getName(), privateUser.getName());
		assertEquals("getSurname()", dbUser.getSurname(),
				privateUser.getSurname());
		assertEquals("isAdmin()", dbUser.isAdmin(), privateUser.isAdmin());
		assertEquals("isDeveloper()", isDeveloper, privateUser.isDeveloper());
		assertNotNull("getProfile()", privateUser.getProfile());
		// private data:
		assertEquals("getPreferredCouncilId()", -1,
				privateUser.getPreferredCouncilId());
		assertEquals("getManagedCouncilId()", -1,
				privateUser.getManagedCouncilId());
		assertNull("getSessionExpires()", privateUser.getSessionExpires());
		assertNull("getUsername()", privateUser.getUsername());
		assertNull("getPassword()", privateUser.getPassword());
		assertNull("getGoogleProfile()", privateUser.getGoogleProfile());
		assertNull("getEmail()", privateUser.getEmail());
	}

}
