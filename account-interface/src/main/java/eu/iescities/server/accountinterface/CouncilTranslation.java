/**
 *  Copyright 2013-2015 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.apache.commons.validator.routines.UrlValidator;

import eu.iescities.server.accountinterface.datainterface.Language;

/**
 * Translated strings of an IES Cities council.
 *
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = CouncilTranslation.PK.class)
class CouncilTranslation {
	@PrimaryKey
	@ForeignKey(deleteAction = ForeignKeyAction.CASCADE)
	private Council council;

	/**
	 * Translation language.
	 */
	@PrimaryKey
	@Column(length = 35, allowsNull = "false")
	private Language lang = null;

	/**
	 * Inner class representing Primary Key
	 */
	public static class PK implements Serializable {
		private static final long serialVersionUID = -6189982793030814464L;
		public Council.PK council; // same name as the real field above
		public Language lang; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			final StringTokenizer token = new StringTokenizer(s, "::");
			this.council = new Council.PK(token.nextToken());
			this.lang = Language.getEnumFromString(token.nextToken());
		}

		@Override
		public String toString() {
			return council.toString() + "::" + lang.toString();
		}

		@Override
		public int hashCode() {
			return council.hashCode() ^ lang.hashCode();
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return this.council.equals(otherPK.council)
						&& otherPK.lang == this.lang;
			}
			return false;
		}
	}

	/**
	 * Council name.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String name = "";

	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String description = "";

	/**
	 * URL to a council logo.
	 */
	@Persistent
	@Column(length = 10000, allowsNull = "true")
	private @Nullable String image;
	
	/**
	 * Default constructor (no valid {@link #lang} set!).
	 */
	public CouncilTranslation() {
	}
	
	/**
	 * Creates a new council translation for the given language.
	 * 
	 * @param lang
	 */
	public CouncilTranslation(@Nonnull final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the council behind this translation object.
	 * 
	 * @return council
	 */
	public Council getCouncil() {
		return council;
	}

	/**
	 * Sets the council behind this translation object.
	 * 
	 * @param council
	 *            the council to set
	 */
	protected void setCouncil(final Council council) {
		this.council = council;
	}

	/**
	 * Gets the council translations's language.
	 * 
	 * @return the lang
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Gets the council's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the council's name.
	 * 
	 * @param name
	 *            council name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the name is invalid
	 */
	public void setName(@Nonnull final String name) throws IllegalArgumentException {
		if (name.length() == 0 || name.length() > 255) {
			throw new IllegalArgumentException(
					"Council name empty or longer than 255 characters!");
		}
		this.name = name;
	}

	/**
	 * Gets the council's description.
	 * 
	 * @return description
	 */
	public @Nonnull String getDescription() {
		return description;
	}

	/**
	 * Sets the council's description.
	 * 
	 * @param description
	 *            description to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the description is invalid
	 */
	public void setDescription(@Nonnull final String description)
			throws IllegalArgumentException {
		if (description.length() > 65535) {
			throw new IllegalArgumentException(
					"Council description longer than 65535 characters!");
		}
		this.description = description;
	}

	/**
	 * Gets the image URL of this council.
	 * 
	 * @return URL to an image
	 */
	public @Nullable String getImage() {
		return image;
	}

	/**
	 * Sets the image URL of this council.
	 * 
	 * @param image
	 *            the image URL to set (up to 10000 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the image path is invalid
	 */
	public void setImage(@Nullable final String image) throws IllegalArgumentException {
		if (image != null) {
			if (image.length() > 10000) {
				throw new IllegalArgumentException(
						"Council image URL longer than 10000 characters!");
			}
			// DEFAULT schemes = "http", "https", "ftp"
			final String[] schemes = { "http", "https" };
			final UrlValidator urlValidator = new UrlValidator(schemes);
			if (!urlValidator.isValid(image)) {
				throw new IllegalArgumentException(
						"Council image URL is invalid!");
			}
		}
		this.image = image;
	}

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Council}
	 * bean.
	 *
	 * @return a council object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Council toBean() {
		return council.toBean(this);
	}
}
