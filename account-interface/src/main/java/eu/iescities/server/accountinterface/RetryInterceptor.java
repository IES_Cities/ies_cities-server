/**
 *  Copyright 2014 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

/**
 * Helper class which can be used to decide
 * whether or not to retry a failed transaction.
 *
 * @author Jan Skrzypczak, skrzypcj@zib.de
 */
public class RetryInterceptor {

	/**
	 * Default time in milliseconds until timeout.
	 */
	public static final int DEFAULT_TIMEOUT = 1000 * 30;

	/**
	 * Maximum time in ms {@link #waitToRetry(Throwable)} will
	 * wait.
	 */
	public static final int MAX_WAIT_TIME = 200;

	/**
	 * Time in ms until timeout. See {@link #waitToRetry(Throwable)}.
	 */
	private int timeout;

	/**
	 * Time at which time measurement was started. 
	 */
	private long startTime;

	/**
	 * Default constructor using a default timeout {@link #DEFAULT_TIMEOUT}.
	 */
	public RetryInterceptor() {
		this(DEFAULT_TIMEOUT);
	}

	/**
	 * New instance using custom timeout.
	 * A timeout of 0 means retry indefinitely
	 * @param timeoutInMs
	 */
	public RetryInterceptor(final int timeoutInMs) {
		timeout = timeoutInMs;
		startTime = System.currentTimeMillis();
	}
	
	/**
	 * Calling this method will block the current thread for a random
	 * time between 0 to {@link #MAX_WAIT_TIME} ms.
	 * If waiting would exceed the set timeout, the passed Throwable
	 * is thrown immediately. Nothing will be thrown otherwise.
	 * 
	 * @param throwing
	 * 		The Throwable to throw.
	 * @throws T
	 * 		The same as passed as parameter.
	 */
	public <T extends Throwable> void waitToRetry(final T throwing) throws T {
		if (startTime < 0) {
			throw throwing;
		}

		final long currentTimeMillis = System.currentTimeMillis();
		final int timeSinceStart = (int) (currentTimeMillis - startTime);
		// exponential back-off, but always add a random time between 0 and
		// (MAX_WAIT_TIME / 10) ms
		final int waitTime = Math.max(
				timeSinceStart + (int) (Math.random() * MAX_WAIT_TIME / 10.0),
				MAX_WAIT_TIME);
		if (timeout != 0 && timeSinceStart + waitTime > timeout) {
			// after sleeping timeout would have been exceeded
			throw throwing;
		}
		try {
			Thread.sleep(waitTime);
		} catch (final InterruptedException e) {
			throw throwing;
		}
	}
}
