package eu.iescities.server.accountinterface.datainterface;

/**
 * Enumeration for supported languages.
 * 
 * The string representations of the constants must match <a
 * href="https://en.wikipedia.org/wiki/IETF_language_tag">IETF language
 * tags</a> (replace hyphens with underscore) and must not be longer than 35
 * characters.
 * 
 * @author Nico Kruber, kruber@zib.de
 */
public enum Language {
	/**
	 * English.
	 */
	EN,
	/**
	 * Spanish.
	 */
	ES,
	/**
	 * Italian.
	 */
	IT;

	/**
	 * Gets the language enum for the given string.
	 * 
	 * @param value
	 *            an enum's string representation (case insensitive)
	 * 
	 * @return a language enum
	 *
	 * @throws IllegalArgumentException
	 *             if the given string does not reflect a valid language
	 */
	public static Language getEnumFromString(final String value)
			throws IllegalArgumentException {
		if (value != null) {
			for (final Language lang : Language.values()) {
				if (value.equalsIgnoreCase(lang.toString())) {
					return lang;
				}
			}
		}

		throw new IllegalArgumentException("Unknown language tag: " + value);
	}
}
