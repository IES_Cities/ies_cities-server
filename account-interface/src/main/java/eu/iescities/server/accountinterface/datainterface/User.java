package eu.iescities.server.accountinterface.datainterface;

import java.util.Date;

/**
 * Everything about a user of the IES Cities platform.
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 * @author Nico Kruber, kruber@zib.de
 */
public class User {
	private int userId = -1;
	/**
	 * The user's session expiry date.
	 */
	private Date sessionExpires;

	/**
	 * User name for login (non-empty, max 255 characters).
	 */
	private String username;
	/**
	 * The user's clear-text password (only used to receive password changes).
	 */
	private String password;
	/**
	 * OAuth 2.0 profile ID as issued by Google.
	 */
	private String googleProfile;
	/**
	 * First name, visible by all users, e.g. for comments, lists of app
	 * developers etc.
	 */
	private String name;
	/**
	 * Surname, visible by all users, e.g. for comments, lists of app
	 * developers etc.
	 */
	private String surname;
	private String email;
	private String profile;
	private int preferredCouncilId = -1;
	private int managedCouncilId = -1;
	private boolean admin = false;
	private boolean developer = false;

	/**
	 * Gets the user's ID.
	 * 
	 * @return user ID
	 */
	public int getUserId() {
		return userId;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user's username (for login).
	 * 
	 * @return name
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * Gets the clear-text password of the user (only for submitting password
	 * changes).
	 * 
	 * @return clear-text password
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Gets the OAuth 2.0 profile identification string as returned by Google.
	 * 
	 * @return profile identification string
	 */
	public String getGoogleProfile() {
		return googleProfile;
	}

	/**
	 * Sets the OAuth 2.0 profile identification string as returned by Google.
	 * 
	 * @param googleProfile
	 *            the profile identification to set
	 */
	public void setGoogleProfile(final String googleProfile) {
		this.googleProfile = googleProfile;
	}

	/**
	 * Gets the user's first firstname.
	 * 
	 * @return first firstname
	 */
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the user's surname.
	 * 
	 * @return surname
	 */
	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	/**
	 * Gets the user's email address.
	 * 
	 * @return email address
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}


	/**
	 * Gets the user's profile.
	 * 
	 * @return user profile
	 */
	public String getProfile() {
		return profile;
	}

	public void setProfile(final String profile) {
		this.profile = profile;
	}

	/**
	 * Gets the date the user's current session expires.
	 * 
	 * @return the session expiry date or <tt>null</tt> if there is no session
	 */
	public Date getSessionExpires() {
		if (sessionExpires == null) {
			return null;
		}
		// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
		return new Date(sessionExpires.getTime());
	}

	/**
	 * Sets the date the user's current session expires.
	 * 
	 * @param sessionExpires
	 *            the session expiry date (or <tt>null</tt> if there is no
	 *            session) to set
	 */
	public void setSessionExpires(final Date sessionExpires) {
		if (sessionExpires == null) {
			this.sessionExpires = null;
		} else {
			// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
			this.sessionExpires = new Date(sessionExpires.getTime());
		}
	}

	/**
	 * Gets the user's current IES Cities admin status.
	 * 
	 * @return <tt>true</tt> if the user is an IES Cities admin, <tt>false</tt>
	 *         otherwise
	 */
	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(final boolean admin) {
		this.admin = admin;
	}

	public boolean isDeveloper() {
		return developer;
	}

	public void setDeveloper(final boolean developer) {
		this.developer = developer;
	}

	/**
	 * Gets the user's preferred location/council.
	 * 
	 * @return a council id or <tt>-1</tt> if not set
	 */
	public int getPreferredCouncilId() {
		return preferredCouncilId;
	}

	public void setPreferredCouncilId(final int preferredCouncilId) {
		this.preferredCouncilId = preferredCouncilId;
	}

	/**
	 * Gets the council a user manages, i.e. is admin for.
	 * 
	 * @return the managed council id or <tt>-1</tt> if the user does not manage
	 *         a council
	 */
	public int getManagedCouncilId() {
		return managedCouncilId;
	}

	/**
	 * Sets the council a user manages, i.e. is admin for.
	 * 
	 * @param managedCouncilId
	 *            the managed council id or <tt>-1</tt> (if the user does not
	 *            manage a council)
	 */
	public void setManagedCouncilId(final int managedCouncilId) {
		this.managedCouncilId = managedCouncilId;
	}
}
