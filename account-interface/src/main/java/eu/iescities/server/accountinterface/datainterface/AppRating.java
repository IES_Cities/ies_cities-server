/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface.datainterface;

import java.util.Date;

/**
 * Rating and comment for an application by a user.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
public class AppRating {
	private int ratingId;

	/**
	 * Time and date the rating was created.
	 */
	private Date date;

	/**
	 * Rating (1 to 5), the higher, the better.
	 */
	private int rating;

	/**
	 * Rating title (brief, up to 255 characters).
	 */
	private String title;

	/**
	 * More detailed comment for the rating (up to 65535 characters).
	 */
	private String comment = "";

	/**
	 * Rating language.
	 */
	private Language lang;

	/**
	 * Name of the user (any string is ok for privacy issues).
	 */
	private String userName;

	/**
	 * The ID of the rated application.
	 */
	private int appId;

	/**
	 * Gets the rating ID.
	 * 
	 * @return rating ID
	 */
	public int getRatingId() {
		return ratingId;
	}

	/**
	 * Sets the rating ID.
	 * 
	 * @param ratingId
	 *            the ratingId to set
	 */
	public void setRatingId(final int ratingId) {
		this.ratingId = ratingId;
	}

	/**
	 * Gets the time and date the rating was created.
	 * 
	 * @return the date
	 */
	public Date getDate() {
		if (date == null) {
			return null;
		}
		// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
		return new Date(date.getTime());
	}

	/**
	 * Sets the time and date the rating was created.
	 * 
	 * @param date
	 *            the date to set
	 */
	public void setDate(final Date date) {
		if (date == null) {
			this.date = null;
		} else {
			// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
			this.date = new Date(date.getTime());
		}
	}

	/**
	 * Gets the rating value.
	 * 
	 * @return the rating value
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * Sets the rating value.
	 * 
	 * @param rating
	 *            the rating value to set (must be between 0 and 5 (inclusive))
	 */
	public void setRating(final int rating) {
		this.rating = rating;
	}

	/**
	 * Gets the rating title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the rating title.
	 * 
	 * @param title
	 *            the title to set (up to 255 characters)
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Gets the comment of this rating.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment of this rating.
	 * 
	 * @param comment
	 *            the comment to set (up to 65535 characters)
	 */
	public void setComment(final String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the language of this rating.
	 * 
	 * @return the language
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Sets the language of this rating.
	 * 
	 * @param lang
	 *            the language to set
	 */
	public void setLang(final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the name of the user who created this rating.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the name of the user who created this rating.
	 * 
	 * @param userName
	 *            the user name to set (up to 255 characters)
	 */
	public void setUserName(final String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the ID of the app this rating was created for.
	 * 
	 * @return the appId
	 */
	public int getAppId() {
		return appId;
	}

	/**
	 * Sets the ID of the app this rating was created for.
	 * 
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(final int appId) {
		this.appId = appId;
	}
}
