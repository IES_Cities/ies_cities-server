package eu.iescities.server.accountinterface.datainterface;

import java.util.Set;

/**
 * An app is a mobile/web application within IES Cities' framework
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 * @author Nico Kruber, kruber@zib.de
 */
public class Application {
    /**
     * Unique identifier of an app (invalid if < 0).
     */
	private int appId = -1;
	
	/**
	 * Language of localisable fields, e.g. name and description.
	 */
	private Language lang;

	/**
	 * Name of an app.
	 */
	private String name;

	/**
	 * Permissions of an app.
	 */
	private String permissions;

	/**
	 * Description of an app.
	 */
	private String description;

	/**
	 * Url of an app.
	 */
	private String url;

	/**
	 * Image of an app.
	 */
	private String image;

	/**
	 * Android package name.
	 */
	private String packageName;

	/**
	 * Geographical scope of an app.
	 */
	private GeoScope geographicalScope;

	/**
	 * ID of the Council the app belongs to.
	 */
	private Integer relatedCouncilId;
	
	/**
	 * Overall rating extracted from Google Play, not only from people's
	 * comments (may be <tt>null</tt> if not set yet).
	 */
	private Double googlePlayRating;

	/**
	 * Version of an app.
	 */
	private String version;
	/**
	 * Terms of Service of an app.
	 */
	private String termsOfService;

	/**
	 * Number of accesses of an app.
	 */
	private Integer accessNumber;
	/**
	 * Number of downloads of an app.
	 */
	private Integer downloadNumber;
	/**
	 * List of tags assigned to an app.
	 */
	private Set<String> tags;

	/**
	 * Default constructor.
	 */
	public Application() {
		super();
	}

	// Getters & Setters

	/**
	 * Gets the application's ID.
	 * 
	 * @return application ID
	 */
	public int getAppId() {
		return appId;
	}

	/**
	 * Sets the application's ID.
	 * 
	 * @param appId
	 *            application ID
	 */
	public void setAppId(final int appId) {
		this.appId = appId;
	}

	/**
	 * Gets the language of localisable fields, e.g. name and description.
	 * 
	 * @return the language used
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Sets the language of localisable fields, e.g. name and description.
	 * 
	 * @param lang
	 *            the language to set
	 */
	public void setLang(final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the application's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the application's name.
	 * 
	 * @param name
	 *            application name to set (up to 255 characters)
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the app permissions needed by this application.
	 * 
	 * @return permissions and their description
	 */
	public String getPermissions() {
		return permissions;
	}

	/**
	 * Sets the app permissions needed by this application.
	 * 
	 * @param permissions
	 *            new permissions and their description (up to 65535 characters)
	 */
	public void setPermissions(final String permissions) {
		this.permissions = permissions;
	}

	/**
	 * Gets the application's description.
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the application's description.
	 * 
	 * @param description
	 *            the description to set (up to 65535 characters)
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the URL for this application.
	 * 
	 * @return URL
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the URL for this application.
	 * 
	 * @param url
	 *            the URL to set (up to 10000 characters)
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Gets the image of this application.
	 * 
	 * @return path to an image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the image of this application.
	 * 
	 * @param image
	 *            the image path to set (up to 10000 characters)
	 */
	public void setImage(final String image) {
		this.image = image;
	}

	/**
	 * Gets the geographical scope of the application.
	 * 
	 * @return the geographical scope
	 */
	public GeoScope getGeographicalScope() {
		return geographicalScope;
	}

	/**
	 * Sets the geographical scope of the application.
	 * 
	 * @param geographicalScope
	 *            the geographical scope to set
	 */
	public void setGeographicalScope(final GeoScope geographicalScope) {
		this.geographicalScope = geographicalScope;
	}

	/**
	 * Gets the ID of the council this application is developed for.
	 * 
	 * @return council
	 */
	public Integer getRelatedCouncilId() {
		return relatedCouncilId;
	}

	/**
	 * Sets the ID of the council this application is developed for.
	 * 
	 * @param relatedCouncilId
	 *            the council ID to set
	 */
	public void setRelatedCouncilId(final Integer relatedCouncilId) {
		this.relatedCouncilId = relatedCouncilId;
	}
	
	/**
	 * Gets the application googlePlayRating.
	 * 
	 * @return googlePlayRating score
	 */
	public Double getGooglePlayRating() {
		return this.googlePlayRating;
	}

	/**
	 * Sets the application googlePlayRating.
	 * 
	 * @param score
	 *            the google play rating score to set (1 integer, 1 decimal or
	 *            <tt>null</tt> if unset)
	 */
	public void setGooglePlayRating(final Double score) {
		this.googlePlayRating = score;
	}

	/**
	 * Gets the application version.
	 * 
	 * @return version number
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the application version.
	 * 
	 * @param version
	 *            the version number to set (up to 255 characters)
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	/**
	 * Gets the terms of service for this application.
	 * 
	 * @return terms of service
	 */
	public String getTermsOfService() {
		return termsOfService;
	}

	/**
	 * Sets the terms of service for this application.
	 * 
	 * @param termsOfService
	 *            the terms of service to set (up to 65535 characters)
	 */
	public void setTermsOfService(final String termsOfService) {
		this.termsOfService = termsOfService;
	}

	/**
	 * Gets the number of accesses for this application.
	 * 
	 * @return number of accesses
	 */
	public Integer getAccessNumber() {
		return accessNumber;
	}

	/**
	 * Gets the number of accesses for this application.
	 * 
	 * @param accessNumber
	 *            number of accesses to set
	 */
	public void setAccessNumber(final Integer accessNumber) {
		this.accessNumber = accessNumber;
	}

	/**
	 * Gets the number of downloads of this application.
	 * 
	 * @return number of downloads
	 */
	public Integer getDownloadNumber() {
		return downloadNumber;
	}

	/**
	 * Sets the number of downloads of this application.
	 * 
	 * @param downloadNumber
	 *            number of downloads to set
	 */
	public void setDownloadNumber(final Integer downloadNumber) {
		this.downloadNumber = downloadNumber;
	}

	/**
	 * Gets all the tags of this application.
	 * 
	 * @return application tags
	 */
	public Set<String> getTags() {
		return tags;
	}

	/**
	 * Sets all the tags of this application.
	 * 
	 * @param tags
	 *            a set of tags (each up to 255 characters)
	 */
	public void setTags(final Set<String> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the package name of the application.
	 * 
	 * @return the package name
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Sets the package name of the application.
	 * 
	 * @param packageName
	 *            the package name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the package name is invalid
	 */
	public void setPackageName(final String packageName) {
		this.packageName = packageName;
	}
}
