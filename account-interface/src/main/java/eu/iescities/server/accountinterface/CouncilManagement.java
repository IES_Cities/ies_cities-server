/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.JDODataStoreException;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import org.datanucleus.util.StringUtils;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Methods for retrieving and changing council information.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
@PersistenceAware
public class CouncilManagement {
	
	/**
	 * Maximum number of allowed elements to retrieve when querying for all
	 * users.
	 */
	protected static final int MAX_LIST_ELEMENTS_LIMIT = 1000;

	/**
	 * Creates a new council using an existing geographical scope.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the admin's session ID
	 * @param councilBean
	 *            the council bean object containing at least a council name, a
	 *            valid geographical scope and a language field
	 * 
	 * @return the new council bean object filled with the data written to the
	 *         DB
	 * @throws SecurityException
	 *             if the sessionId is invalid or the user is no admin
	 * @throws IllegalArgumentException
	 *             if the name of the council is already used or a parameter is
	 *             invalid
	 */
	public static Council createCouncil(final String sessionId,
			final Council councilBean) throws SecurityException,
			IllegalArgumentException {
		if (councilBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
	
			final CouncilTranslation councilL10n = new CouncilTranslation(
					councilBean.getLang());
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final Council result = storeCouncil(pm, user, councilL10n, councilBean)
						.toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Updates council data with new information.
	 * 
	 * NOTE: Only allowed by council or IES Cities admins!
	 * 
	 * @param sessionId
	 *            the admin's session ID
	 * @param councilBean
	 *            the council bean object containing the information to
	 *            change (must contain at least the council id and a
	 *            language field)
	 * 
	 * @return the new council bean object filled with the data written to the
	 *         DB
	 * @throws SecurityException
	 *             if the sessionId is invalid or the user is no admin
	 * @throws IllegalArgumentException
	 *             if the name of the council is already used or a parameter is
	 *             invalid
	 */
	public static Council updateCouncil(final String sessionId,
			final Council councilBean) throws SecurityException,
			IllegalArgumentException {
		if (councilBean.getCouncilId() < 0) {
			throw new IllegalArgumentException("No council ID supplied.");
		}
		if (councilBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				CouncilTranslation councilL10n;
				try {
					councilL10n = getCouncilTranslationByLang(pm, councilBean.getCouncilId(),
							councilBean.getLang(), null);
				} catch (final IllegalArgumentException e) {
					// no translation for this language yet
					councilL10n = new CouncilTranslation(councilBean.getLang());
					councilL10n.setCouncil(getCouncilById(pm, councilBean.getCouncilId()));
				}
				final Council result = storeCouncil(pm, user, councilL10n, councilBean)
						.toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Stores changes to a new or existing council by converting the
	 * {@link Council} bean properties.
	 * 
	 * Wrap around {@link Transaction#begin()} and {@link Transaction#commit()}
	 * calls to execute all statements in a transaction.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param user
	 *            the user trying to store the application
	 * @param councilL10n
	 *            a council's translation object from the database (or a
	 *            newly created one with no properties set)
	 * @param councilBean
	 *            the council bean object containing at least an application ID
	 *            and the information to change (the included {@link GeoScope}
	 *            object only needs its ID to be set)
	 * 
	 * @return a new council bean object filled with the data written to the DB
	 * 
	 * @throws SecurityException
	 *             if the user is not authorised to change the council, i.e. not
	 *             a council admin an IES Cities admin
	 * @throws IllegalArgumentException
	 *             is thrown if the council ID is missing in the bean or on
	 *             invalid parameters
	 */
	private static CouncilTranslation storeCouncil(
			final PersistenceManager pm,
			final eu.iescities.server.accountinterface.User user,
			final CouncilTranslation councilL10n,
			final Council councilBean) throws IllegalArgumentException,
			SecurityException {
		if (councilL10n.getCouncil() != null) {
			// existing council
			if (!(user.isIesAdmin() || (user.getManagedCouncil() != null && user
					.getManagedCouncil().getCouncilId() == councilL10n
					.getCouncil().getCouncilId()))) {
				throw new SecurityException(
						"User not authorized to modify council.");
			}
		} else {
			// create new council (only admins are allowed to)
			if (!user.isIesAdmin()) {
				throw new SecurityException(
						"User not authorized to create council.");
			}
			if (councilBean.getGeographicalScope() == null) {
				throw new IllegalArgumentException(
						"Missing geographical scope for council.");
			}
			councilL10n.setCouncil(new eu.iescities.server.accountinterface.Council());
		}
		final eu.iescities.server.accountinterface.Council council = councilL10n.getCouncil();

		// check unique council name:
		if (council.getCouncilId() < 0
				&& StringUtils.isEmpty(councilBean.getName())) {
			throw new IllegalArgumentException("Council name null or empty.");
		}
		if (council.getCouncilId() < 0
				|| (councilBean.getName() != null && !councilL10n.getName()
						.equals(councilBean.getName()))) {
			if (pm.newQuery(
					"SELECT UNIQUE council.councilId FROM "
							+ CouncilTranslation.class.getName()
							+ " WHERE council.councilId != :councilId && name == :name")
					.execute(council.getCouncilId(), councilBean.getName()) != null) {
				throw new IllegalArgumentException(
						"Council name already exists.");
			}

			councilL10n.setName(councilBean.getName());
		}

		if (councilBean.getDescription() != null) {
			councilL10n.setDescription(councilBean.getDescription());
		}
		if (councilBean.getImage() != null) {
			councilL10n.setImage(councilBean.getImage());
		}

		if (councilBean.getGeographicalScope() != null) {
			final eu.iescities.server.accountinterface.GeoScope geoScope = GeoScopeManagement
					.getScopeById(pm, councilBean.getGeographicalScope()
							.getId());
			council.setGeographicalScope(geoScope);
		}

		pm.makePersistent(councilL10n);
		return councilL10n;
	}

	/**
	 * Gets a council by its ID.
	 * 
	 * @param councilId
	 *            the ID of the council to retrieve
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return the council
	 * @throws IllegalArgumentException
	 *             is thrown if there is no council with the given ID
	 */
	public static Council getCouncilById(final int councilId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getCouncilTranslationByLang(pm, councilId, lang,
					fallbackLang).toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Removes an existing council and all its datasets.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the admin's session ID
	 * @param councilId
	 *            the ID of the council to remove
	 * 
	 * @return the number of removed councils
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static long removeCouncil(final String sessionId, final int councilId)
			throws SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				UserManagement.validateAdminSession(pm, sessionId);
				tx.begin();
				// delete all datasets published by this council
				Query query = pm.newQuery("SELECT FROM "
						+ eu.iescities.server.accountinterface.Dataset.class
								.getName()
						+ " WHERE publishingCouncil != null"
						+ " && publishingCouncil.councilId == :param");
				query.deletePersistentAll(councilId);
				query = pm.newQuery("SELECT UNIQUE FROM "
						+ eu.iescities.server.accountinterface.Council.class
								.getName() + " WHERE councilId == :param");
				final long deletedCouncils = query.deletePersistentAll(councilId);
				tx.commit();
				return deletedCouncils;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
			}
		}
	}

	/**
	 * Gets a subset of the list of all councils.
	 * 
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of councils to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of councils
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static List<Council> getAllCouncils(final int offset,
			final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
			throws IllegalArgumentException, SecurityException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ CouncilTranslation.class.getName()
					+ " ORDER BY council.councilId ASC, lang ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			final StringBuilder filter = new StringBuilder();
			filter.append("(lang == '");
			filter.append(lang);
			if (fallbackLang != null) {
				filter.append("' || lang == '");
				filter.append(fallbackLang);
			}
			filter.append("')");
			query.setFilter(filter.toString());
			@SuppressWarnings("unchecked")
			final List<CouncilTranslation> councils = (List<CouncilTranslation>) query
					.execute();
			final ArrayList<Council> result = new ArrayList<Council>(
					councils.size());
			// filter duplicate councils with lang and fallbackLang
			Council previousCouncil = null;
			for (final CouncilTranslation councilL10n : councils) {
				if (previousCouncil != null
						&& previousCouncil.getCouncilId() == councilL10n
								.getCouncil().getCouncilId()) {
					if (previousCouncil.getLang() == fallbackLang) {
						assert councilL10n.getLang() == lang;
						previousCouncil = councilL10n.toBean();
						result.set(result.size() - 1, previousCouncil);
					}
				} else {
					previousCouncil = councilL10n.toBean();
					result.add(previousCouncil);
				}
			}
			return result;
		} finally {
			pm.close();
		}
	}

	/**
	 * Find all datasets for the given council.
	 * 
	 * @param councilId
	 *            the council's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of applications to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return list of dataset objects ordered (ascending) by their IDs (empty
	 *         if <tt>councilId</tt> does not exist)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Dataset> getCouncilDataSets(final int councilId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
					throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException(
					"limit too high (> " + MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Dataset.class
							.getName()
					+ " WHERE publishingCouncil != null"
					+ " && publishingCouncil.councilId == :param"
					+ " ORDER BY datasetId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return DatasetManagement.getDatasetTranslations(pm,
					(List<eu.iescities.server.accountinterface.Dataset>) query
							.execute(councilId),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a subset of the list of all applications related to a given council.
	 * 
	 * @param councilId
	 *            the council's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of applications to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for apps to retrieve
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of applications sorted by their IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Application> getCouncilApps(final int councilId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Application.class.getName()
					+ " WHERE relatedCouncil.councilId == :councilid"
					+ " ORDER BY appId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return ApplicationManagement.getAppTranslations(pm,
					(List<eu.iescities.server.accountinterface.Application>) query
							.execute(councilId),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a list of all council admins.
	 * 
	 * @param councilId
	 *            the council's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users managing the given countil (in ascending user ID
	 *         order), filled with only the user id, name, surname, isAdmin and
	 *         isDeveloper
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<User> getCouncilAdmins(final int councilId,
			final int offset, final int limit) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT"
					+ " userId AS userId,"
					+ " firstname AS name,"
					+ " surname AS surname,"
					+ " isIesAdmin AS admin,"
					+ " profile AS profile,"
					+ " !developedApps.isEmpty() AS developer"
					+ " INTO " + User.class.getName()
					+ " FROM " + eu.iescities.server.accountinterface.User.class.getName()
					+ " WHERE managedCouncil != null"
					+ " && managedCouncil.councilId == :param"
					+ " ORDER BY userId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return (List<User>) query.execute(councilId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a list of all council users, i.e. users with preferred location set
	 * to the council of a council admins council.
	 * 
	 * NOTE: Only allowed by council admins!
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users managing the given countil (in ascending user ID
	 *         order), filled with only the user id, name, surname, isAdmin and
	 *         isDeveloper
	 * @throws SecurityException
	 *             is thrown if the session ID is not a valid council admin user's
	 *             session
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	public static List<User> getCouncilUsers(final String sessionId,
			final int offset, final int limit) throws SecurityException,
			IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.Council council = getAuthorizedCouncil(
					pm, sessionId);
			return getCouncilUsers(council.getCouncilId(), offset, limit);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a list of all council users, i.e. users with preferred location set
	 * to the given council.
	 * 
	 * NOTE: Only allowed by IES Cities admin users or by council admins for
	 * their council only!
	 * 
	 * @param sessionId
	 *            the admin's session ID
	 * @param councilId
	 *            the council's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users managing the given countil (in ascending user ID
	 *         order), filled with only the user id, name, surname, isAdmin and
	 *         isDeveloper
	 * @throws SecurityException
	 *             is thrown if the session ID is not a valid admin user's
	 *             session
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	public static List<User> getCouncilUsers(final String sessionId,
			final int councilId, final int offset, final int limit)
			throws SecurityException, IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = UserManagement
					.getUserBySession(pm, sessionId);
			if (!(user.isIesAdmin() || (user.getManagedCouncil() != null && user
					.getManagedCouncil().getCouncilId() == councilId))) {
				throw new SecurityException(
						"User not authorized to retrieve the list of council users.");
			}
			return getCouncilUsers(councilId, offset, limit);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a list of all council users, i.e. users with preferred location set
	 * to the given council.
	 * 
	 * @param councilId
	 *            the council's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users managing the given countil (in ascending user ID
	 *         order), filled with only the user id, name, surname, isAdmin and
	 *         isDeveloper
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	protected static List<User> getCouncilUsers(final int councilId,
			final int offset, final int limit) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT"
					+ " userId AS userId,"
					+ " firstname AS name,"
					+ " surname AS surname,"
					+ " isIesAdmin AS admin,"
					+ " profile AS profile,"
					+ " !developedApps.isEmpty() AS developer"
					+ " INTO " + User.class.getName()
					+ " FROM " + eu.iescities.server.accountinterface.User.class.getName()
					+ " WHERE preferredLocation != null"
					+ " && preferredLocation.councilId == :param"
					+ " ORDER BY userId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return (List<User>) query.execute(councilId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Adds an administrator for a given council.
	 * 
	 * NOTE: Only allowed by IES Cities admin users or another council admin!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param councilId
	 *            the ID of the council to add the admin to
	 * @param userId
	 *            the user ID of the user to remove
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if the user or council IDs do not exist or the user
	 *             to add already manages a council
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static void addCouncilAdmin(final String sessionId,
			final int councilId, final int userId)
			throws IllegalArgumentException, SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				final eu.iescities.server.accountinterface.User admin = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Council council = getCouncilById(
						pm, councilId);
				if (!(admin.isIesAdmin() || council.getCouncilAdmins().contains(
						admin))) {
					throw new SecurityException("User is not an admin of  the IES "
							+ "cities platform or the council.");
				}
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserById(pm, userId);
				if (user.getManagedCouncil() != null) {
					throw new IllegalArgumentException(
							"User to add already manages the council.");
				}
				council.getCouncilAdmins().add(user);
				user.setManagedCouncil(council);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Removes an administrator from a given council.
	 * 
	 * NOTE: Only allowed by IES Cities admin users or another council admin!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param councilId
	 *            the ID of the council to add the admin to
	 * @param userId
	 *            the user ID of the user to remove
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if the user or council IDs do not exist or the user
	 *             to remove is not an admin of this council
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static void removeCouncilAdmin(final String sessionId,
			final int councilId, final int userId)
			throws IllegalArgumentException, SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User admin = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Council council = getCouncilById(
						pm, councilId);
				if (!(admin.isIesAdmin() || council.getCouncilAdmins().contains(
						admin))) {
					throw new SecurityException("User is not an admin of the IES "
							+ "cities platform or the council.");
				}
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserById(pm, userId);
				if (user.getManagedCouncil() == null
						|| !user.getManagedCouncil().equals(council)) {
					throw new IllegalArgumentException(
							"User to remove is not an admin of the council.");
				}
				council.getCouncilAdmins().remove(user);
				user.setManagedCouncil(null);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Returns the council object the user (identified by her session ID) is
	 * managing.
	 * 
	 * @param pm
	 *            a PersistenceManager object
	 * @param sessionId
	 *            the user's session ID
	 * 
	 * @return the (attached) council, the user is in charge of
	 * @throws SecurityException
	 *             is thrown if the user is not a council admin
	 */
	protected static eu.iescities.server.accountinterface.Council getAuthorizedCouncil(
			final PersistenceManager pm, final String sessionId)
			throws SecurityException {
		final eu.iescities.server.accountinterface.User user = UserManagement
				.getUserBySession(pm, sessionId);
		final eu.iescities.server.accountinterface.Council council = user
				.getManagedCouncil();
		if (council == null) {
			throw new SecurityException("User " + user.getUsername() + " is "
					+ "not a council admin.");
		}
		return council;
	}

	/**
	 * Verifies that a given ID points to a valid council.
	 *
	 * @param councilId
	 *            the ID to check
	 * 
	 * @return whether the council id exists or not
	 */
	public static @Nonnull boolean verifyCouncilId(final int councilId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT UNIQUE"
					+ " councilId"
					+ " FROM " + eu.iescities.server.accountinterface.Council.class.getName()
					+ " WHERE councilId == :param");
			return null != query.execute(councilId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a council by its ID.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param councilId
	 *            the ID of the council to retrieve
	 * 
	 * @return a council attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if there is no council with the given ID
	 */
	protected static @Nonnull eu.iescities.server.accountinterface.Council getCouncilById(
			final PersistenceManager pm,
			final int councilId) throws IllegalArgumentException {
		try {
			return pm.getObjectById(
					eu.iescities.server.accountinterface.Council.class,
					"" + councilId);
		} catch (final JDOObjectNotFoundException e) {
			throw new IllegalArgumentException("Unknown council id: "
					+ councilId + ".", e);
		}
	}

	/**
	 * Finds a translation for a council by it's id and language code.
	 * 
	 * @param pm
	 *            an instance of the PersistenceManager
	 * @param appId
	 *            the ID of the council in question
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a council translation attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             if the councilId cannot be resolved
	 */
	protected static @Nonnull CouncilTranslation getCouncilTranslationByLang(
			@Nonnull final PersistenceManager pm, final int councilId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final Query query = pm.newQuery("SELECT UNIQUE FROM "
				+ CouncilTranslation.class.getName()
				+ " WHERE council.councilId == :councilId && lang == :lang");
		CouncilTranslation councilL10n = (CouncilTranslation) query
				.execute(councilId, lang);
		if (councilL10n == null && fallbackLang != null) {
			councilL10n = (CouncilTranslation) query.execute(councilId,
					fallbackLang);
		}
		if (councilL10n == null) {
			throw new IllegalArgumentException(
					"No council translation for councilId " + councilId
							+ " into language " + lang + ".");
		}
		return councilL10n;
	}

	protected static final String queryCouncilByName = "SELECT FROM "
			+ eu.iescities.server.accountinterface.Council.class.getName()
			+ " WHERE (SELECT council.councilId FROM "
			+ CouncilTranslation.class.getName()
			+ " WHERE name == :name GROUP BY council.councilId)"
			+ ".contains(councilId)";
}
