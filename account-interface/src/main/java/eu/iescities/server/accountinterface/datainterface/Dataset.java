package eu.iescities.server.accountinterface.datainterface;

import java.util.Date;

/**
 * Everything about a dataset of the IES Cities platform.
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 * @author Nico Kruber, kruber@zib.de
 */
public class Dataset implements Cloneable {
	/**
	 * Unique identifier of a dataset (invalid if < 0).
	 */
	private int datasetId = -1;
	
	/**
	 * Language of localisable fields, e.g. name and description.
	 */
	private Language lang;

	/**
	 * Reference to the user who published this dataset (no user if < 0).
	 */
	private int userId = -1;

	/**
	 * Reference to the publishing council of this dataset (no council dataset if < 0).
	 */
	private int councilId = -1;
	
	/**
	 * Detailed dataset description (up to 65535 characters).
	 */
	private String description;
	
	/**
	 * JSON description (up to 131072 characters).
	 */
	private String jsonMapping;
	
	/**
	 * Name of the dataset (up to 255 characters).
	 */
	private String name;
	
	/**
	 * Number of read accesses.
	 */
	private int readAccessNumber;
	
	/**
	 * Number of write accesses.
	 */
	private int writeAccessNumber;
	
	/**
	 * Number of apps using this dataset.
	 */
	private int numberAppsUsingIt;
	
	/**
	 * Publication date.
	 */
	private Date publishingTimestamp;
	
	/**
	 * Quality rating: <tt>0</tt> (not rated), <tt>1</tt> to 5</tt> (rated - the
	 * higher the better), <tt>-1</tt> (invalid / do not change).
	 */
	private int quality = -1;
	
	/**
	 * Id of the user who set the quality rating (or <tt>-1</tt> if unverified).
	 */
	private int qualityVerifiedBy = -1;

	/**
	 * Default constructor
	 */
	public Dataset() {
		super();
	}

	/**
	 * Gets the dataset's ID.
	 * 
	 * @return dataset ID
	 */
	public int getDatasetId() {
		return datasetId;
	}

	/**
	 * Sets the dataset's ID.
	 * 
	 * @param datasetId
	 *            the dataset ID to set
	 */
	public void setDatasetId(final int datasetId) {
		this.datasetId = datasetId;
	}

	/**
	 * Gets the language of localisable fields, e.g. name and description.
	 * 
	 * @return the language used
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Sets the language of localisable fields, e.g. name and description.
	 * 
	 * @param lang
	 *            the language to set
	 */
	public void setLang(final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the ID of the user who published this dataset.
	 * 
	 * @return userid or <tt>-1</tt> if the user has been deleted
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the ID of the user who published this dataset.
	 * 
	 * @param userId
	 *            the user to set
	 */
	public void setUserId(final int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the ID of the council which published this dataset.
	 * 
	 * @return council
	 */
	public int getCouncilId() {
		return councilId;
	}

	/**
	 * Sets the ID of the council which published this dataset.
	 * 
	 * @param councilId
	 *            the ID of the council to set
	 */
	public void setCouncilId(final int councilId) {
		this.councilId = councilId;
	}

	/**
	 * Gets the dataset's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the dataset's name.
	 * 
	 * @param name
	 *            the name to set (up to 255 characters)
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the dataset's description.
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the dataset's description.
	 * 
	 * @param description
	 *            the description to set (up to 65535 characters)
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the JSON mapping.
	 * 
	 * @return a string containing a JSON mapping object
	 */
	public String getJsonMapping() {
		return jsonMapping;
	}

	/**
	 * Sets the JSON mapping.
	 * 
	 * @param jsonMapping
	 *            a string containing a JSON mapping object (up to 131072
	 *            characters)
	 */
	public void setJsonMapping(final String jsonMapping) {
		this.jsonMapping = jsonMapping;
	}

	/**
	 * Gets the number of read accesses for this dataset.
	 * 
	 * @return number of read accesses
	 */
	public int getReadAccessNumber() {
		return readAccessNumber;
	}

	/**
	 * Sets the number of read accesses for this dataset.
	 * 
	 * @param readAccessNumber
	 *            the number of read accesses to set
	 */
	public void setReadAccessNumber(final int readAccessNumber) {
		this.readAccessNumber = readAccessNumber;
	}

	/**
	 * Gets the number of write accesses for this dataset.
	 * 
	 * @return number of write accesses
	 */
	public int getWriteAccessNumber() {
		return writeAccessNumber;
	}

	/**
	 * Sets the number of write accesses for this dataset.
	 * 
	 * @param writeAccessNumber
	 *            the number of write accesses to set
	 */
	public void setWriteAccessNumber(final int writeAccessNumber) {
		this.writeAccessNumber = writeAccessNumber;
	}

	/**
	 * Gets the number of apps using this dataset.
	 * 
	 * @return number of apps
	 */
	public int getNumberAppsUsingIt() {
		return numberAppsUsingIt;
	}

	/**
	 * Sets the number of apps using this dataset.
	 * 
	 * @param numberAppsUsingIt
	 *            number of apps to set
	 */
	public void setNumberAppsUsingIt(final int numberAppsUsingIt) {
		this.numberAppsUsingIt = numberAppsUsingIt;
	}

	/**
	 * Gets the date this dataset was published.
	 * 
	 * @return publish date
	 */
	public Date getPublishingTimestamp() {
		if (publishingTimestamp == null) {
			return null;
		}
		// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
		return new Date(publishingTimestamp.getTime());
	}

	/**
	 * Sets the date this dataset was published.
	 * 
	 * @param publishingTimestamp
	 *            the date to set
	 */
	public void setPublishingTimestamp(final Date publishingTimestamp) {
		if (publishingTimestamp == null) {
			this.publishingTimestamp = null;
		} else {
			// see http://www.informit.com/articles/article.aspx?p=31551&seqNum=2
			this.publishingTimestamp = new Date(publishingTimestamp.getTime());
		}
	}

	/**
	 * Gets the dataset's quality rating.
	 * 
	 * @return the quality: <tt>0</tt> (not rated), <tt>1</tt> to 5</tt> (rated
	 *         - the higher the better), <tt>-1</tt> (invalid / do not change)
	 */
	public int getQuality() {
		return quality;
	}

	/**
	 * Sets the dataset's quality rating.
	 * 
	 * @param quality
	 *            <tt>0</tt> (not rated), <tt>1</tt> to <tt>5</tt> (rated - the
	 *            higher the better), <tt>-1</tt> (invalid / do not change)
	 */
	public void setQuality(final int quality) {
		this.quality = quality;
	}

	/**
	 * Gets the id of the user who set the quality rating.
	 * 
	 * @return a user id
	 */
	public int getQualityVerifiedBy() {
		return qualityVerifiedBy;
	}

	/**
	 * Sets the user who set the quality rating.
	 * 
	 * @param qualityVerifiedBy
	 *            a user id
	 */
	public void setQualityVerifiedBy(final int qualityVerifiedBy) {
		this.qualityVerifiedBy = qualityVerifiedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Dataset clone() {
		try {
			final Dataset clone = (Dataset) super.clone();
			clone.datasetId = this.datasetId;
			clone.userId = this.userId;
			clone.councilId = this.councilId;
			clone.description = this.description;
			clone.jsonMapping = this.jsonMapping;
			clone.name = this.name;
			clone.readAccessNumber = this.readAccessNumber;
			clone.writeAccessNumber = this.writeAccessNumber;
			clone.numberAppsUsingIt = this.numberAppsUsingIt;
			clone.publishingTimestamp = this.publishingTimestamp;
			clone.quality = this.quality;
			clone.qualityVerifiedBy = this.qualityVerifiedBy;
			return clone;
		} catch (final CloneNotSupportedException e) {
			throw new RuntimeException(e); // should not happen!
		}
	}
}
