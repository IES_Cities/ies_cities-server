/**
 *  Copyright 2014 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nonnull;
import javax.jdo.JDODataStoreException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import org.datanucleus.util.StringUtils;

import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.PMF;

/**
 * Manages geographical scopes.
 * 
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceAware
public class GeoScopeManagement {
	/**
	 * Maximum number of allowed elements to retrieve when querying for a
	 * possibly big list.
	 */
	protected static final int MAX_LIST_ELEMENTS_LIMIT = 1000;
	
	/**
	 * Creates a new named rectangular geographical scope.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param name
	 *            the name of the scope
	 * @param northWest
	 *            the north-west point of the scope
	 * @param southEast
	 *            the south-east point of the scope
	 * 
	 * @return the created geographical scope
	 * @throws SecurityException
	 *             is thrown if the user session is invalid
	 */
	public static GeoScope createScope(final String sessionId,
			final String name, final GeoPoint northWest,
			final GeoPoint southEast) {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				UserManagement.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.GeoScope scope = new eu.iescities.server.accountinterface.GeoScope(
						name, northWest, southEast);
				pm.makePersistent(scope);
				tx.commit();
				return scope.toBean();
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Removes a named rectangular geographical scope.
	 * 
	 * NOTE: A geoscope can only be removed by an IES cities platform admin!
	 * 
	 * @param sessionId
	 *            the admin's session ID
	 * @param scopeId
	 *            the ID of the scope
	 * 
	 * @return the number of instances that were deleted
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static long removeScope(final String sessionId, final int scopeId)
			throws SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				UserManagement.validateAdminSession(pm, sessionId);
				tx.begin();
				final Query query = pm.newQuery(queryScopeById);
				final long deletedScopes = query.deletePersistentAll(scopeId);
				tx.commit();
				return deletedScopes;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets a geographical scope by name.
	 * 
	 * @param scopeId
	 *            the ID of the scope
	 * 
	 * @return the scope
	 * @throws IllegalArgumentException
	 *             is thrown if the scope is unknown
	 */
	public static GeoScope getScopeById(final int scopeId)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery(queryScopeByIdIntoBean);
			final GeoScope scope = (GeoScope) query.execute(scopeId);
			if (scope == null) {
				throw new IllegalArgumentException("Unknown scope id: "
						+ scopeId + ".");
			}
			return scope;
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a list of geographical scopes covering the given point.
	 * 
	 * @param point
	 *            a geographical point to cover
	 * 
	 * @return the list of scopes sorted by their name (ascending)
	 */
	@SuppressWarnings("unchecked")
	public static List<GeoScope> getScopesCoveringPoint(
			final GeoPoint point) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery(queryScopeByPointIntoBean);
			return (List<GeoScope>) query
					.execute(point.getLatitude(), point.getLongitude());
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a subset of the list of all geographical scopes containing the given
	 * search string.
	 * 
	 * The search string is split into words and each is matched against
	 * geoscope names (AND-connected, i.e. all words must be in the name). Note:
	 * The search is case-insensitive.
	 * 
	 * @param searchString
	 *            the string to search for (space-separated words)
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of geoscopes to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of geoscopes including all of the search words and sorted
	 *         by the geoscope IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<GeoScope> findScopes(final String searchString,
			final int offset, final int limit) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm
					.newQuery(eu.iescities.server.accountinterface.GeoScope.class);
			query.setResult("scopeId AS id,"
					+ " scopeName AS name,"
					+ " northEastLat AS northEastLatitude,"
					+ " northEastLong AS northEastLongitude,"
					+ " southWestLat AS southWestLatitude,"
					+ " southWestLong AS southWestLongitude");
			query.setResultClass(GeoScope.class);
			query.setOrdering("scopeId ASC"); // should be default, just in case
			query.setRange(offset, offset + limit);

			if (searchString.length() > 0) {
				final String[] searchWords = StringUtils.split(
						StringUtils.removeSpecialTagsFromString(searchString),
						" ");
				// note: define the words as real parameters to prevent SQL
				// injections (datanucleus must filter these)
				final HashMap<String, String> parameters = new HashMap<String, String>(
						searchWords.length);
				final StringBuilder parameterDecl = new StringBuilder();
				final StringBuilder filter = new StringBuilder();
				for (int i = 0; i < searchWords.length; ++i) {
					final String pword = "word" + i;
					final String pexpr = "wexpr" + i;
					parameters.put(pexpr, ".*" + searchWords[i].toLowerCase()
							+ ".*");
					parameters.put(pword, searchWords[i]);
					parameterDecl.append("String ");
					parameterDecl.append(pexpr);
					parameterDecl.append(", String ");
					parameterDecl.append(pword);
					parameterDecl.append(", ");
					filter.append("scopeName.toLowerCase().matches(");
					filter.append(pexpr);
					filter.append(") && ");
				}
				assert (parameterDecl.length() > 2);
				query.declareParameters(parameterDecl.substring(0,
						parameterDecl.length() - 2));
				assert (filter.length() > 4);
				query.setFilter(filter.substring(0, filter.length() - 4));
				return (List<GeoScope>) query.executeWithMap(parameters);
			} else {
				return (List<GeoScope>) query.execute();
			}
		} finally {
			pm.close();
		}
	}

	/**
	 * Verifies that a given ID points to a valid geographical scope.
	 *
	 * @param scopeId
	 *            the ID to check
	 * 
	 * @return whether the geographical scope id exists or not
	 */
	public static @Nonnull boolean verifyScopeId(final int scopeId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT UNIQUE"
					+ " scopeId"
					+ " FROM " + eu.iescities.server.accountinterface.GeoScope.class.getName()
					+ " WHERE scopeId == :param");
			return null != query.execute(scopeId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a geographical scope by name.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param scopeId
	 *            the ID of the scope
	 * 
	 * @return the scope object attached to the {@link PersistenceManager}
	 *         (always non-<tt>null</tt>)
	 * @throws IllegalArgumentException
	 *             is thrown if the scope is unknown
	 */
	protected static eu.iescities.server.accountinterface.GeoScope getScopeById(
			final PersistenceManager pm, final int scopeId)
			throws IllegalArgumentException {
		final Query query = pm.newQuery(queryScopeById);
		final eu.iescities.server.accountinterface.GeoScope scope = (eu.iescities.server.accountinterface.GeoScope) query
				.execute(scopeId);
		if (scope == null) {
			throw new IllegalArgumentException("Unknown scope id: " + scopeId
					+ ".");
		}
		return scope;
	}

	private static final String queryScopeById = "SELECT UNIQUE FROM "
			+ eu.iescities.server.accountinterface.GeoScope.class.getName()
			+ " WHERE scopeId == :param1";

	private static final String queryScopeByIdIntoBean = "SELECT UNIQUE"
			+ " scopeId AS id,"
			+ " scopeName AS name,"
			+ " northEastLat AS northEastLatitude,"
			+ " northEastLong AS northEastLongitude,"
			+ " southWestLat AS southWestLatitude,"
			+ " southWestLong AS southWestLongitude"
			+ " INTO " + GeoScope.class.getName()
			+ " FROM " + eu.iescities.server.accountinterface.GeoScope.class.getName()
			+ " WHERE scopeId == :param1";

	private static final String queryScopeByPointIntoBean = "SELECT"
			+ " scopeId AS id,"
			+ " scopeName AS name,"
			+ " northEastLat AS northEastLatitude,"
			+ " northEastLong AS northEastLongitude,"
			+ " southWestLat AS southWestLatitude,"
			+ " southWestLong AS southWestLongitude"
			+ " INTO " + GeoScope.class.getName()
			+ " FROM " + eu.iescities.server.accountinterface.GeoScope.class.getName()
			+ " WHERE northEastLat >= :param1 && southWestLat <= :param1"
			+ " && northEastLong >= :param2 && southWestLong <= :param2"
			+ " ORDER BY scopeName ASC";

}
