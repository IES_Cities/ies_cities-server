/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import org.apache.commons.validator.routines.UrlValidator;

/**
 * An IES Cities application.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = Application.PK.class)
class Application {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int appId = -1;
	
    /**
     * Inner class representing Primary Key
     */
	public static class PK implements Serializable {
		private static final long serialVersionUID = 6593424468946295027L;
		public int appId; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			this.appId = Integer.valueOf(s).intValue();
		}

		@Override
		public String toString() {
			return "" + appId;
		}

		@Override
		public int hashCode() {
			return appId;
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return otherPK.appId == this.appId;
			}
			return false;
		}
	}

	@Persistent
	@Column(allowsNull = "false")
	private int downloadNumber = 0;

	@Persistent
	@Column(allowsNull = "false")
	private int accessNumber = 0;

	@Persistent
	@Column(length = 10000, allowsNull = "true")
	private @Nullable String url;

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	@Index(name = "APPLICATION_N49")
	private @Nullable Council relatedCouncil;

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	@Index(name = "APPLICATION_N50")
	private @Nullable GeoScope geographicalScope;
	
	@Persistent
	@Column(allowsNull = "true")
	private Double googlePlayRating;

	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String version = "0.0";

	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String permissions = "";

	@Persistent
	@Unique(name = "APPLICATION_U2")
	@Column(allowsNull = "false")
	private String packageName;

	/**
	 * Bi-directional M-N mapping between an application and its datasets.
	 */
	@Persistent
	@Join(column = "APPID", deleteAction = ForeignKeyAction.CASCADE)
	@Element(column = "DATASETID", deleteAction = ForeignKeyAction.CASCADE)
	private @Nonnull Set<Dataset> datasets = new HashSet<Dataset>();

	/**
	 * Bi-directional M-N mapping between an application and its developers.
	 */
	@Persistent(defaultFetchGroup = "false")
	@Join(column = "APPID", deleteAction = ForeignKeyAction.CASCADE)
	@Element(column = "USERID", deleteAction = ForeignKeyAction.CASCADE)
	private @Nonnull Set<User> developers = new HashSet<User>();

	/**
	 * Gets the application's ID.
	 * 
	 * @return application ID
	 */
	public int getAppId() {
		return appId;
	}

	/**
	 * Gets the number of downloads of this application.
	 * 
	 * @return number of downloads
	 */
	public int getDownloadNumber() {
		return downloadNumber;
	}

	/**
	 * Sets the number of downloads of this application.
	 * 
	 * @param downloadNumber
	 *            number of downloads to set
	 */
	public void setDownloadNumber(final int downloadNumber) {
		this.downloadNumber = downloadNumber;
	}

	/**
	 * Gets the number of accesses for this application.
	 * 
	 * @return number of accesses
	 */
	public int getAccessNumber() {
		return accessNumber;
	}

	/**
	 * Gets the number of accesses for this application.
	 * 
	 * @param accessNumber
	 *            number of accesses to set
	 */
	public void setAccessNumber(final int accessNumber) {
		this.accessNumber = accessNumber;
	}

	/**
	 * Gets the URL for this application.
	 * 
	 * @return URL
	 */
	public @Nullable String getUrl() {
		return url;
	}

	/**
	 * Sets the URL for this application.
	 * 
	 * @param url
	 *            the URL to set (up to 10000 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the URL is invalid
	 */
	public void setUrl(@Nullable final String url) throws IllegalArgumentException {
		if (url != null) { 
			if (url.length() > 10000) {
				throw new IllegalArgumentException(
						"Application URL longer than 10000 characters!");
			}
			// DEFAULT schemes = "http", "https", "ftp"
			final String[] schemes = { "http", "https" };
			final UrlValidator urlValidator = new UrlValidator(schemes);
			if (!urlValidator.isValid(url)) {
				throw new IllegalArgumentException("Application URL is invalid!");
			}
		}
		this.url = url;
	}

	/**
	 * Gets the council for which this application is developed for.
	 * 
	 * @return council
	 */
	public @Nullable Council getRelatedCouncil() {
		return relatedCouncil;
	}

	/**
	 * Sets the council for which this application is developed for.
	 * 
	 * @param relatedCouncil
	 *            the council to set
	 */
	public void setRelatedCouncil(@Nullable final Council relatedCouncil) {
		this.relatedCouncil = relatedCouncil;
	}

	/**
	 * Gets the geographical scope of the application.
	 * 
	 * @return the geographical scope
	 */
	public @Nullable GeoScope getGeographicalScope() {
		return geographicalScope;
	}

	/**
	 * Sets the geographical scope of the application.
	 * 
	 * @param geographicalScope
	 *            the geographical scope to set
	 */
	public void setGeographicalScope(@Nullable final GeoScope geographicalScope) {
		this.geographicalScope = geographicalScope;
	}
	
	/**
	 * Gets the application googlePlayRating.
	 * 
	 * @return googlePlayRating score
	 */
	public Double getGooglePlayRating() {
		return this.googlePlayRating;
	}

	/**
	 * Sets the application googlePlayRating.
	 * 
	 * @param score
	 *            the google play rating score to set (1 integer, 1 decimal or
	 *            <tt>null</tt> if unset)
	 */
	public void setGooglePlayRating(final Double score) {
		this.googlePlayRating = score;
	}

	/**
	 * Gets the application version.
	 * 
	 * @return version number
	 */
	public @Nonnull String getVersion() {
		return version;
	}

	/**
	 * Sets the application version.
	 * 
	 * @param version
	 *            the version number to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the version is invalid
	 */
	public void setVersion(@Nonnull final String version)
			throws IllegalArgumentException {
		if (version.length() > 255) {
			throw new IllegalArgumentException(
					"Application version longer than 255 characters!");
		}
		this.version = version;
	}

	/**
	 * Gets the app permissions needed by this application.
	 * 
	 * @return permissions and their description
	 */
	public @Nonnull String getPermissions() {
		return permissions;
	}

	/**
	 * Sets the app permissions needed by this application.
	 * 
	 * @param permissions
	 *            new permissions and their description (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the permissions are invalid
	 */
	public void setPermissions(@Nonnull final String permissions)
			throws IllegalArgumentException {
		if (permissions.length() > 65535) {
			throw new IllegalArgumentException(
					"Application permissions longer than 65535 characters!");
		}
		this.permissions = permissions;
	}

	/**
	 * Gets the package name of the application.
	 * 
	 * @return the package name
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Sets the package name of the application.
	 * 
	 * @param packageName
	 *            the package name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the package name is invalid
	 */
	public void setPackageName(@Nonnull final String packageName)
			throws IllegalArgumentException {
		if (packageName.length() == 0 || packageName.length() > 255) {
			throw new IllegalArgumentException(
					"Application package name empty or longer than 255 characters!");
		}
		this.packageName = packageName;
	}

	// public void setAppRatings(Set<AppRating> appRatings) {
	// this.appRatings = appRatings;
	// }

	/**
	 * Gets the datasets an application uses.
	 * 
	 * @return set of datasets
	 */
	public @Nonnull Set<Dataset> getDatasets() {
		return datasets;
	}

	public void addDataset(@Nonnull final Dataset dataset) {
		this.datasets.add(dataset);
	}

	public void removeDataset(@Nonnull final Dataset dataset) {
		this.datasets.remove(dataset);
	}

	// public void setDatasets(Set<Dataset> datasets) {
	// this.datasets = datasets;
	// }

	/**
	 * Gets the developers of this application.
	 * 
	 * @return application developers
	 */
	public @Nonnull Set<User> getDevelopers() {
		return developers;
	}

	/**
	 * Adds a developer of this application.
	 * 
	 * @param user
	 *            the user to add as an application developer for this
	 *            application
	 */
	public void addDeveloper(@Nonnull final User user) {
		this.developers.add(user);
	}

	/**
	 * Removes a developer of this application.
	 * 
	 * @param user
	 *            the user to remove from the list of application developer for
	 *            this application
	 */
	public void removeDeveloper(@Nonnull final User user) {
		this.developers.remove(user);
	}

	// public void setDevelopers(@Nonnull Set<User> developers) {
	// this.developers = developers;
	// }

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Application}
	 * bean.
	 *
	 * @param appL10n
	 *            localised application fields
	 * 
	 * @return an application object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Application toBean(
			@Nonnull final ApplicationTranslation appL10n) {
		final eu.iescities.server.accountinterface.datainterface.Application result = new eu.iescities.server.accountinterface.datainterface.Application();
		result.setAppId(appId);
		result.setLang(appL10n.getLang());
		result.setName(appL10n.getName());
		result.setDescription(appL10n.getDescription());
		result.setDownloadNumber(downloadNumber);
		result.setAccessNumber(accessNumber);
		result.setGooglePlayRating(googlePlayRating);
		result.setUrl(url);
		result.setImage(appL10n.getImage());
		if (relatedCouncil != null) {
			result.setRelatedCouncilId(relatedCouncil.getCouncilId());
		}
		if (geographicalScope != null) {
			result.setGeographicalScope(geographicalScope.toBean());
		}
		result.setVersion(version);
		result.setTermsOfService(appL10n.getTos());
		result.setPermissions(permissions);
		result.setPackageName(packageName);
		result.setTags(appL10n.getTags());
		return result;
	}
}
