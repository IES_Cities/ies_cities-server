package eu.iescities.server.accountinterface.datainterface;

/**
 * Named rectangular geographical region.
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 * @author Nico Kruber, kruber@zib.de
 */
public class GeoScope {

	private int id = -1;
	private String name;

	private double northEastLatitude;
	private double northEastLongitude;
	private double southWestLatitude;
	private double southWestLongitude;

	public GeoScope() {
		super();
	}

	/**
	 * Creates a named rectangular geographical region.
	 * 
	 * @param name
	 *            the name (up to 255 characters)
	 * @param northEast
	 *            the North-East point
	 * @param southWest
	 *            the South-West point
	 */
	public GeoScope(final String name, final GeoPoint northEast,
			final GeoPoint southWest) {
		super();
		this.name = name;
		this.setNorthEastGeoPoint(northEast);
		this.setSouthWestGeoPoint(southWest);
	}

	/**
	 * Creates a named rectangular geographical region.
	 * 
	 * @param name
	 *            the name (up to 255 characters)
	 * @param northEastLatitude
	 *            the North-East latitude
	 * @param northEastLongitude
	 *            the North-East longitude
	 * @param southWestLatitude
	 *            the South-West latitude
	 * @param southWestLongitude
	 *            the South-West longitude
	 */
	public GeoScope(final String name, final double northEastLatitude,
			final double northEastLongitude, final double southWestLatitude,
			final double southWestLongitude) {
		super();
		this.name = name;
		this.northEastLatitude = northEastLatitude;
		this.northEastLongitude = northEastLongitude;
		this.southWestLatitude = southWestLatitude;
		this.southWestLongitude = southWestLongitude;
	}

	// Getters & Setters

	/**
	 * Gets the ID of the geographical scope.
	 * 
	 * @return the scope ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the ID of the geographical scope.
	 * 
	 * @param id
	 *            the scope ID to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Gets the name of the geographical scope.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public double getNorthEastLatitude() {
		return northEastLatitude;
	}

	public void setNorthEastLatitude(final double northEastLatitude) {
		this.northEastLatitude = northEastLatitude;
	}

	public double getNorthEastLongitude() {
		return northEastLongitude;
	}

	public void setNorthEastLongitude(final double northEastLongitude) {
		this.northEastLongitude = northEastLongitude;
	}

	public double getSouthWestLatitude() {
		return southWestLatitude;
	}

	public void setSouthWestLatitude(final double southWestLatitude) {
		this.southWestLatitude = southWestLatitude;
	}

	public double getSouthWestLongitude() {
		return southWestLongitude;
	}

	public void setSouthWestLongitude(final double southWestLongitude) {
		this.southWestLongitude = southWestLongitude;
	}

	// Other methods

	public void setNorthEastGeoPoint(final GeoPoint geopoint) {
		this.northEastLatitude = geopoint.getLatitude();
		this.northEastLongitude = geopoint.getLongitude();
	}

	/**
	 * Gets the North-East point of the region.
	 * 
	 * @return geographical point
	 */
	public GeoPoint getNorthEastGeoPoint() {
		return new GeoPoint(this.northEastLatitude, this.northEastLongitude);
	}

	public void setSouthWestGeoPoint(final GeoPoint geopoint) {
		this.southWestLatitude = geopoint.getLatitude();
		this.southWestLongitude = geopoint.getLongitude();
	}

	/**
	 * Gets the South-West point of the region.
	 * 
	 * @return geographical point
	 */
	public GeoPoint getSouthWestGeoPoint() {
		return new GeoPoint(this.southWestLatitude, this.southWestLongitude);
	}

	public GeoPoint getCentralGeoPoint() {
		final double latitude = (this.southWestLatitude + this.northEastLatitude) / 2;
		final double longitude = (this.southWestLongitude + this.northEastLongitude) / 2;

		return new GeoPoint(latitude, longitude);
	}
}
