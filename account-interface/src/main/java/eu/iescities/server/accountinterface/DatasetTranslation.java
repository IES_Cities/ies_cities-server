/**
 *  Copyright 2013-2015 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.annotation.Nonnull;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import eu.iescities.server.accountinterface.datainterface.Language;

/**
 * Translated strings of an IES Cities dataset.
 *
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = DatasetTranslation.PK.class)
class DatasetTranslation {
	@PrimaryKey
	@ForeignKey(deleteAction = ForeignKeyAction.CASCADE)
	private Dataset dataset;

	/**
	 * Translation language.
	 */
	@PrimaryKey
	@Column(length = 35, allowsNull = "false")
	private Language lang = null;

	/**
	 * Inner class representing Primary Key
	 */
	public static class PK implements Serializable {
		private static final long serialVersionUID = -6189982793030814464L;
		public Dataset.PK dataset; // same name as the real field above
		public Language lang; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			final StringTokenizer token = new StringTokenizer(s, "::");
			this.dataset = new Dataset.PK(token.nextToken());
			this.lang = Language.getEnumFromString(token.nextToken());
		}

		@Override
		public String toString() {
			return dataset.toString() + "::" + lang.toString();
		}

		@Override
		public int hashCode() {
			return dataset.hashCode() ^ lang.hashCode();
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return this.dataset.equals(otherPK.dataset)
						&& otherPK.lang == this.lang;
			}
			return false;
		}
	}

	/**
	 * Dataset name.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String name = "";

	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String description = "";
	
	/**
	 * Default constructor (no valid {@link #lang} set!).
	 */
	public DatasetTranslation() {
	}
	
	/**
	 * Creates a new dataset translation for the given language.
	 * 
	 * @param lang
	 */
	public DatasetTranslation(@Nonnull final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the dataset behind this translation object.
	 * 
	 * @return dataset
	 */
	public Dataset getDataset() {
		return dataset;
	}

	/**
	 * Sets the dataset behind this translation object.
	 * 
	 * @param dataset
	 *            the dataset to set
	 */
	protected void setDataset(final Dataset dataset) {
		this.dataset = dataset;
	}

	/**
	 * Gets the dataset translations's language.
	 * 
	 * @return the lang
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Gets the dataset's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the dataset's name.
	 * 
	 * @param name
	 *            dataset name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the name is invalid
	 */
	public void setName(@Nonnull final String name) throws IllegalArgumentException {
		if (name.length() == 0 || name.length() > 255) {
			throw new IllegalArgumentException(
					"Dataset name empty or longer than 255 characters!");
		}
		this.name = name;
	}

	/**
	 * Gets the dataset's description.
	 * 
	 * @return description
	 */
	public @Nonnull String getDescription() {
		return description;
	}

	/**
	 * Sets the dataset's description.
	 * 
	 * @param description
	 *            description to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the description is invalid
	 */
	public void setDescription(@Nonnull final String description)
			throws IllegalArgumentException {
		if (description.length() > 65535) {
			throw new IllegalArgumentException(
					"Dataset description longer than 65535 characters!");
		}
		this.description = description;
	}

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Dataset}
	 * bean.
	 *
	 * @return a dataset object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Dataset toBean() {
		return dataset.toBean(this);
	}
}
