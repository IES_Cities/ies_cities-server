/**
 *  Copyright 2014 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import javax.annotation.Nonnull;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import eu.iescities.server.accountinterface.datainterface.GeoPoint;

/**
 * Named rectangular geographical region.
 * 
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false")
class GeoScope {
	/**
	 * ID of the geographic scope.
	 */
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int scopeId = -1;
	/**
	 * Name of the geographic scope (up to 255 characters).
	 */
	@Persistent
	@Unique
	@Column(allowsNull = "false")
	private String scopeName;

	@Persistent
	@Column(allowsNull = "false")
	private double northEastLat;

	@Persistent
	@Column(allowsNull = "false")
	private double northEastLong;

	@Persistent
	@Column(allowsNull = "false")
	private double southWestLat;

	@Persistent
	@Column(allowsNull = "false")
	private double southWestLong;

	/**
	 * Creates a named rectangular geographical region.
	 * 
	 * @param scopeName
	 *            the name (up to 255 characters)
	 * @param northEast
	 *            the North-East point
	 * @param southWest
	 *            the South-West point
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if a parameter is invalid
	 */
	public GeoScope(final @Nonnull String scopeName,
			final @Nonnull GeoPoint northEast, final @Nonnull GeoPoint southWest)
			throws IllegalArgumentException {
		this(scopeName, northEast.getLatitude(), northEast.getLongitude(),
				southWest.getLatitude(), southWest.getLongitude());
	}

	/**
	 * Creates a named rectangular geographical region.
	 * 
	 * @param scopeName
	 *            the name (up to 255 characters)
	 * @param northEastLat
	 *            the North-East latitude
	 * @param northEastLong
	 *            the North-East longitude
	 * @param southWestLat
	 *            the South-West latitude
	 * @param southWestLong
	 *            the South-West longitude
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if a parameter is invalid
	 */
	public GeoScope(final @Nonnull String scopeName, final double northEastLat,
			final double northEastLong, final double southWestLat,
			final double southWestLong) throws IllegalArgumentException {
		if (scopeName.length() == 0 || scopeName.length() > 255) {
			throw new IllegalArgumentException(
					"Geographical scope name empty or longer than 255 characters");
		}
		this.scopeName = scopeName;
		this.northEastLat = northEastLat;
		this.northEastLong = northEastLong;
		this.southWestLat = southWestLat;
		this.southWestLong = southWestLong;
	}

	/**
	 * Creates a named rectangular geographical region from the given
	 * {@link eu.iescities.server.accountinterface.datainterface.GeoScope} bean.
	 * 
	 * @param scope
	 *            the scope bean
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if a parameter is invalid
	 */
	public GeoScope(
			final @Nonnull eu.iescities.server.accountinterface.datainterface.GeoScope scope) {
		this(scope.getName(), scope.getNorthEastGeoPoint(), scope
				.getSouthWestGeoPoint());
	}

	/**
	 * Gets the ID of the geographical scope.
	 * 
	 * @return the scope ID
	 */
	public int getScopeId() {
		return scopeId;
	}

	/**
	 * Gets the name of the geographical scope.
	 * 
	 * @return the name
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * Gets the North-East point of the region.
	 * 
	 * @return geographical point
	 */
	public @Nonnull GeoPoint getNorthEast() {
		return new GeoPoint(northEastLat, northEastLong);
	}

	/**
	 * Gets the South-West point of the region.
	 * 
	 * @return geographical point
	 */
	public @Nonnull GeoPoint getSouthWest() {
		return new GeoPoint(southWestLat, southWestLong);
	}

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.GeoScope} bean.
	 * 
	 * @return a geo point bean object
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.GeoScope toBean() {
		final eu.iescities.server.accountinterface.datainterface.GeoScope geoScope = new eu.iescities.server.accountinterface.datainterface.GeoScope(
				scopeName, northEastLat, northEastLong, southWestLat,
				southWestLong);
		geoScope.setId(scopeId);
		return geoScope;
	}
}
