/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * A dataset published by a council and used by applications.
 * 
 * TODO a dataset requires a URI(?)
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = Dataset.PK.class)
class Dataset {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int datasetId = -1;
	
    /**
     * Inner class representing Primary Key
     */
	public static class PK implements Serializable {
		private static final long serialVersionUID = 6593424468946295027L;
		public int datasetId; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			this.datasetId = Integer.valueOf(s).intValue();
		}

		@Override
		public String toString() {
			return "" + datasetId;
		}

		@Override
		public int hashCode() {
			return datasetId;
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return otherPK.datasetId == this.datasetId;
			}
			return false;
		}
	}

	@Persistent
	@Column(allowsNull = "false")
	private Date publishingDate;

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	@ForeignKey(deleteAction = ForeignKeyAction.NULL)
	@Index(name = "DATASET_N49")
	private @Nullable User publishingUser;

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	@Index(name = "DATASET_N50")
	private Council publishingCouncil;

	/**
	 * Quality rating: 0 (not rated), 1 to 5 (rated - the higher the better).
	 */
	@Persistent
	@Column(jdbcType = "SMALLINT", allowsNull = "false", defaultValue = "0")
	private int quality = 0;

	/**
	 * The user who set the quality rating.
	 */
	@Persistent
	@Column(allowsNull = "true", defaultValue = "null")
	@ForeignKey(deleteAction = ForeignKeyAction.NULL)
	@Index(name = "DATASET_N51")
	private @Nullable User qualityVerifiedBy = null;

	@Persistent
	@Column(allowsNull = "false")
	private int readAccessNumber = 0;

	@Persistent
	@Column(allowsNull = "false")
	private int writeAccessNumber = 0;

	/**
	 * Detailed dataset description in JSON format (up to 131072 characters).
	 */
	@Persistent
	@Column(length = 131072, allowsNull = "false")
	private @Nonnull String jsonMapping = "";

	/**
	 * Bi-directional M-N mapping between a dataset and its applications.
	 */
	@Persistent(mappedBy = "datasets")
	@Join(deleteAction = ForeignKeyAction.CASCADE)
	@Element(deleteAction = ForeignKeyAction.CASCADE)
	private @Nonnull Set<Application> appsUsingThisSet = new HashSet<Application>();
	
	/**
	 * Gets the dataset's ID.
	 * 
	 * @return dataset ID
	 */
	public int getDatasetId() {
		return datasetId;
	}

	/**
	 * Gets the date this dataset was published.
	 * 
	 * @return publish date
	 */
	public Date getPublishingDate() {
		return publishingDate;
	}

	/**
	 * Sets the date this dataset was published.
	 * 
	 * @param publishingDate
	 *            the date to set
	 */
	public void setPublishingDate(final Date publishingDate) {
		this.publishingDate = publishingDate;
	}

	/**
	 * Gets the user who published this dataset.
	 * 
	 * @return user or <tt>null</tt> if the user has been deleted
	 */
	public User getPublishingUser() {
		return publishingUser;
	}

	/**
	 * Sets the user who published this dataset.
	 * 
	 * @param publishingUser
	 *            the user to set
	 */
	public void setPublishingUser(@Nullable final User publishingUser) {
		this.publishingUser = publishingUser;
	}

	/**
	 * Gets the council which published this dataset.
	 * 
	 * @return council
	 */
	public Council getPublishingCouncil() {
		return publishingCouncil;
	}

	/**
	 * Sets the council which published this dataset.
	 * 
	 * @param publishingCouncil
	 *            the council to set
	 */
	public void setPublishingCouncil(@Nonnull final Council publishingCouncil) {
		this.publishingCouncil = publishingCouncil;
	}

	/**
	 * Gets the dataset's quality rating.
	 * 
	 * @return the quality: 0 (not rated), 1 to 5 (rated - the higher the
	 *         better)
	 */
	public int getQuality() {
		return quality;
	}

	/**
	 * Sets the dataset's quality rating.
	 * 
	 * @param quality
	 *            0 (not rated), 1 to 5 (rated - the higher the better)
	 */
	public void setQuality(final int quality) {
		if (quality > 5 || quality < 0) {
			throw new IllegalArgumentException(
					"Invalid dataset quality rating (0-5).");
		}
		this.quality = quality;
	}

	/**
	 * Gets the user who set the quality rating.
	 * 
	 * @return a user
	 */
	public @Nullable User getQualityVerifiedBy() {
		return qualityVerifiedBy;
	}

	/**
	 * Sets the user who set the quality rating.
	 * 
	 * @param qualityVerifiedBy
	 *            a user
	 */
	public void setQualityVerifiedBy(final @Nullable User qualityVerifiedBy) {
		this.qualityVerifiedBy = qualityVerifiedBy;
	}

	/**
	 * Gets the number of read accesses for this dataset.
	 * 
	 * @return number of read accesses
	 */
	public int getReadAccessNumber() {
		return readAccessNumber;
	}

	/**
	 * Sets the number of read accesses for this dataset.
	 * 
	 * @param readAccessNumber
	 *            the number of read accesses to set
	 */
	public void setReadAccessNumber(final int readAccessNumber) {
		this.readAccessNumber = readAccessNumber;
	}

	/**
	 * Gets the number of write accesses for this dataset.
	 * 
	 * @return number of write accesses
	 */
	public int getWriteAccessNumber() {
		return writeAccessNumber;
	}

	/**
	 * Sets the number of write accesses for this dataset.
	 * 
	 * @param writeAccessNumber
	 *            the number of write accesses to set
	 */
	public void setWriteAccessNumber(final int writeAccessNumber) {
		this.writeAccessNumber = writeAccessNumber;
	}

	/**
	 * Gets the JSON mapping.
	 * 
	 * @return a string containing a JSON mapping object
	 */
	public @Nonnull String getJsonMapping() {
		return jsonMapping;
	}

	/**
	 * Sets the JSON mapping.
	 * 
	 * @param jsonMapping
	 *            a string containing a JSON mapping object (up to 131072
	 *            characters)
	 */
	public void setJsonMapping(@Nonnull final String jsonMapping) {
		if (jsonMapping.length() > 131072) {
			throw new IllegalArgumentException(
					"Dataset JSON description longer than 131072 characters!");
		}
		this.jsonMapping = jsonMapping;
	}

	/**
	 * Gets the apps which use this dataset.
	 * 
	 * @return set of apps
	 */
	public @Nonnull Set<Application> getAppsUsingThisSet() {
		return appsUsingThisSet;
	}

	/**
	 * Adds an application using this dataset.
	 * 
	 * @param app
	 *            the application to add
	 */
	public void addAppUsingThisSet(@Nonnull final Application app) {
		appsUsingThisSet.add(app);
	}

	/**
	 * Removes an application using this dataset.
	 * 
	 * @param app
	 *            the application to remove
	 */
	public void removeAppUsingThisSet(@Nonnull final Application app) {
		appsUsingThisSet.remove(app);
	}

	// public void setAppsUsingThisSet(Set<Application> appsUsingThisSet) {
	// this.appsUsingThisSet = appsUsingThisSet;
	// }

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Dataset} bean.
	 *
	 * @param datasetL10n
	 *            localised dataset fields
	 * 
	 * @return a dataset object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Dataset toBean(
			@Nonnull final DatasetTranslation datasetL10n) {
		final eu.iescities.server.accountinterface.datainterface.Dataset result = new eu.iescities.server.accountinterface.datainterface.Dataset();
		result.setDatasetId(datasetId);
		if (publishingCouncil != null) {
			result.setCouncilId(publishingCouncil.getCouncilId());
		}
		result.setReadAccessNumber(readAccessNumber);
		result.setWriteAccessNumber(writeAccessNumber);
		if (publishingUser != null) {
			result.setUserId(publishingUser.getUserId());
		}
		result.setPublishingTimestamp(publishingDate);
		result.setName(datasetL10n.getName());
		result.setDescription(datasetL10n.getDescription());
		result.setJsonMapping(jsonMapping);
		result.setNumberAppsUsingIt(appsUsingThisSet.size());
		result.setQuality(quality);
		if (qualityVerifiedBy != null) {
			result.setQualityVerifiedBy(qualityVerifiedBy.getUserId());
		}
		return result;
	}
}
