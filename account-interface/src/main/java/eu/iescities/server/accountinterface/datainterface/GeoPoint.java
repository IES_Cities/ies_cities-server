package eu.iescities.server.accountinterface.datainterface;

import javax.annotation.Nonnull;

/**
 * Geographical point as defined by latitude and longitude.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Oscar Peña, oscar.pena@deusto.es
 */
public class GeoPoint {
	/**
	 * Latitude
	 */
	private double latitude = 0.0;

	/**
	 * Longitude
	 */
	private double longitude = 0.0;

	/**
	 * Default constructor - use the setters to make something useful of this
	 * object.
	 */
	public GeoPoint() {
		super();
	}

	/**
	 * Creates a new geographical point.
	 * 
	 * @param latitude
	 *            latitude
	 * @param longitude
	 *            longitude
	 */
	public GeoPoint(final double latitude, final double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * Creates a new geographical point from a single string representation.
	 * 
	 * @param latitudeLongitude
	 *            latitude and longitude in the format "
	 *            <tt>latitude,longitude</tt>"
	 * 
	 * @throws NullPointerException
	 *             if the string is null
	 * @throws NumberFormatException
	 *             if the string does not contain parsable doubles
	 * 
	 */
	public GeoPoint(final String latitudeLongitude)
			throws NullPointerException, NumberFormatException {
		super();
		final String[] latLong = latitudeLongitude.split(",");
		this.latitude = Double.parseDouble(latLong[0]);
		this.longitude = Double.parseDouble(latLong[1]);
	}

	/**
	 * Gets the latitude.
	 * 
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 * 
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the longitude.
	 * 
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 * 
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(final double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Converts decimal degrees to radians.
	 * 
	 * @param deg
	 *            number in decimal degrees
	 */
	protected static double deg2rad(final double deg) {
		return (deg * Math.PI / 180.0);
	}

	/**
	 * Converts radians to decimal degrees.
	 * 
	 * @param rad
	 *            number in radians
	 */
	protected static double rad2deg(final double rad) {
		return (rad * 180.0 / Math.PI);
	}

	/**
	 * Calculates the distance from this point to the given point in kilometres.
	 * 
	 * @param target
	 *            target point
	 * 
	 * @return distance in kilometres
	 */
	public double distanceInKm(final @Nonnull GeoPoint target) {
		final double sourceLatitudeRad = deg2rad(this.latitude);
		final double sourceLongitude = this.longitude;

		final double targetLatitudeRad = deg2rad(target.getLatitude());
		final double targetLongitude = target.getLongitude();

		final double thetaRad = deg2rad(sourceLongitude - targetLongitude);
		double dist = Math.sin(sourceLatitudeRad) * Math.sin(targetLatitudeRad)
				+ Math.cos(sourceLatitudeRad) * Math.cos(targetLatitudeRad)
				* Math.cos(thetaRad);

		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		// dist is expressed in miles, so a conversion to km is needed
		return (dist * 1.609344);
	}

	@Override
	public String toString() {
		return "[" + latitude + "°, " + longitude + "°]";
	}
}
