/**
 *  Copyright 2014 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * Persistable user password-reset object with all its relations.
 * 
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false")
class UserPasswordReset {

	/**
	 * Token that will be send to the user's email address and thus validates
	 * his identity.
	 */
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.UUIDHEX)
	@Column(length = 32)
	private String token;

	/**
	 * The user for this password reset request.
	 */
	@Persistent
	@Column(allowsNull = "false")
	@ForeignKey(deleteAction = ForeignKeyAction.CASCADE)
	private User user;

	/**
	 * The password reset request will be valid until this date.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private Date expires;

	/**
	 * Gets the request's validation token.
	 * 
	 * @return the token that was send to the user's email address and validates
	 *         his identity
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the request's validation token.
	 * 
	 * @param token
	 *            the token that was send to the user's email address and
	 *            validates his identity
	 */
	public void setToken(final @Nonnull String token) {
		this.token = token;
	}

	/**
	 * Gets the user the password should be reset for.
	 * 
	 * @return the target user of the request
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user the password should be reset for.
	 * 
	 * @param user
	 *            the target user of the request
	 */
	public void setUser(final @Nonnull User user) {
		this.user = user;
	}

	/**
	 * Gets the date this password reset request expires.
	 * 
	 * @return the date this password reset request expires
	 */
	public Date getExpires() {
		return expires;
	}

	/**
	 * Sets the date this password reset request expires.
	 * 
	 * @param expires
	 *            the date this password reset request expires
	 */
	public void setExpires(final Date expires) {
		this.expires = expires;
	}

}
