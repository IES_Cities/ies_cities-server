/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import eu.iescities.server.accountinterface.datainterface.Language;

/**
 * Rating and comment for an application by a user.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false")
@Unique(members = {"app", "userName", "date"})
class AppRating {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int ratingId;

	/**
	 * Time and date the rating was created.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private Date date;

	/**
	 * Rating (1 to 5), the higher, the better.
	 */
	@Persistent
	@Column(jdbcType = "SMALLINT", allowsNull = "false")
	private int rating;

	/**
	 * Rating title (brief, up to 255 characters).
	 */
	@Persistent
	@Column(allowsNull = "false")
	private String title;

	/**
	 * More detailed comment for the rating (up to 65535 characters).
	 */
	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String comment = "";

	/**
	 * Rating language.
	 */
	@Persistent
	@Column(length = 35, allowsNull = "false")
	private Language lang;
	
	/**
	 * Name of the user (any string is ok for privacy issues).
	 */
	@Persistent
	@Column(allowsNull = "false")
	private String userName;

	/**
	 * Uni-directional 1-N mapping between a rating and its application.
	 */
	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "false")
	@ForeignKey(deleteAction = ForeignKeyAction.CASCADE)
	private Application app;

	/**
	 * Gets the rating ID.
	 * 
	 * @return rating ID
	 */
	public int getRatingId() {
		return ratingId;
	}

	/**
	 * Gets the time and date the rating was created.
	 * 
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the time and date the rating was created.
	 * 
	 * @param date
	 *            the date to set
	 */
	public void setDate(@Nonnull final Date date) {
		this.date = date;
	}

	/**
	 * Gets the rating value.
	 * 
	 * @return the rating value
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * Sets the rating value.
	 * 
	 * @param rating
	 *            the rating value to set (must be between 0 and 5 (inclusive))
	 * 
	 * @throws IllegalArgumentException
	 *             if the rating is invalid
	 */
	public void setRating(final int rating) throws IllegalArgumentException {
		if (rating < 0 || rating > 5) {
			throw new IllegalArgumentException(
					"Rating must be between 0 and 5 (inclusive)!");
		}
		this.rating = rating;
	}

	/**
	 * Gets the rating title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the rating title.
	 * 
	 * @param title
	 *            the title to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the title is invalid
	 */
	public void setTitle(@Nonnull final String title) throws IllegalArgumentException {
		if (title.length() > 255) {
			throw new IllegalArgumentException(
					"App rating title longer than 255 characters!");
		}
		this.title = title;
	}

	/**
	 * Gets the comment of this rating.
	 * 
	 * @return the comment
	 */
	public @Nonnull String getComment() {
		return comment;
	}

	/**
	 * Sets the comment of this rating.
	 * 
	 * @param comment
	 *            the comment to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the comment is invalid
	 */
	public void setComment(@Nonnull final String comment)
			throws IllegalArgumentException {
		if (comment.length() > 65535) {
			throw new IllegalArgumentException(
					"App rating comment longer than 65535 characters!");
		}
		this.comment = comment;
	}

	/**
	 * Gets the language of this rating.
	 * 
	 * @return the language
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Sets the language of this rating.
	 * 
	 * @param lang
	 *            the language to set
	 */
	public void setLang(@Nonnull final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the name of the user who created this rating.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the name of the user who created this rating.
	 * 
	 * @param userName
	 *            the user name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the user name is invalid
	 */
	public void setUserName(@Nonnull final String userName)
			throws IllegalArgumentException {
		if (userName.length() == 0 || userName.length() > 255) {
			throw new IllegalArgumentException(
					"App rating username empty or longer than 255 characters!");
		}
		this.userName = userName;
	}

	/**
	 * Gets the app this rating was created for.
	 * 
	 * @return the user
	 */
	public Application getApp() {
		return app;
	}

	/**
	 * Sets the app this rating was created for.
	 * 
	 * @param app
	 *            the application to set
	 */
	public void setApp(@Nonnull final Application app) {
		this.app = app;
	}

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.AppRating} bean.
	 * 
	 * @return an application rating object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.AppRating toBean() {
		final eu.iescities.server.accountinterface.datainterface.AppRating result = new eu.iescities.server.accountinterface.datainterface.AppRating();
		result.setRatingId(ratingId);
		result.setDate(date);
		result.setRating(rating);
		result.setTitle(title);
		result.setComment(comment);
		result.setLang(lang);
		result.setUserName(userName);
		result.setAppId(app.getAppId());
		return result;
	}
	
	@Override
	public @Nonnull String toString() {
		final StringBuilder appRating = new StringBuilder();
		
		appRating.append("\"").append(this.title).append("\" - ");
		appRating.append(this.comment).append("\n");
		appRating.append("Made by: ").append(this.userName);
		appRating.append(" - in: ").append(this.lang);
		appRating.append(" - on: ").append(this.date.toString());
		
		return appRating.toString();
	}
}
