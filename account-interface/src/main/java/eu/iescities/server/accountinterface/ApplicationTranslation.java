/**
 *  Copyright 2013-2015 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.apache.commons.validator.routines.UrlValidator;

import eu.iescities.server.accountinterface.datainterface.Language;

/**
 * Translated strings of an IES Cities application.
 *
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = ApplicationTranslation.PK.class)
class ApplicationTranslation {
	@PrimaryKey
	@ForeignKey(deleteAction = ForeignKeyAction.CASCADE)
	private Application app;

	/**
	 * Translation language.
	 */
	@PrimaryKey
	@Column(length = 35, allowsNull = "false")
	private Language lang = null;

	/**
	 * Inner class representing Primary Key
	 */
	public static class PK implements Serializable {
		private static final long serialVersionUID = -6189982793030814464L;
		public Application.PK app; // same name as the real field above
		public Language lang; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			final StringTokenizer token = new StringTokenizer(s, "::");
			this.app = new Application.PK(token.nextToken());
			this.lang = Language.getEnumFromString(token.nextToken());
		}

		@Override
		public String toString() {
			return app.toString() + "::" + lang.toString();
		}

		@Override
		public int hashCode() {
			return app.hashCode() ^ lang.hashCode();
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return this.app.equals(otherPK.app)
						&& otherPK.lang == this.lang;
			}
			return false;
		}
	}

	/**
	 * Application name.
	 *
	 * Must be unique except for translations of the same app!
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String name = "";

	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String description = "";

	/**
	 * URL to an application logo.
	 */
	@Persistent
	@Column(length = 10000, allowsNull = "true")
	private @Nullable String image;

	/**
	 * Terms of Service.
	 */
	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String tos = "";

	/**
	 * Uni-directional M-N mapping of an application and its tags.
	 */
	@Persistent(defaultFetchGroup = "true")
	@Join(columns = { @Column(name = "APPID"), @Column(name = "LANG") },
	      deleteAction = ForeignKeyAction.CASCADE)
	@Element(column = "TAG")
	@Index
	private @Nonnull Set<String> tags = new HashSet<String>();
	
	/**
	 * Default constructor (no valid {@link #lang} set!).
	 */
	public ApplicationTranslation() {
	}
	
	/**
	 * Creates a new app translation for the given language.
	 * 
	 * @param lang
	 */
	public ApplicationTranslation(@Nonnull final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the application behind this translation object.
	 * 
	 * @return application
	 */
	public Application getApp() {
		return app;
	}

	/**
	 * Sets the application behind this translation object.
	 * 
	 * @param app
	 *            the application to set
	 */
	protected void setApp(final Application app) {
		this.app = app;
	}

	/**
	 * Gets the application translations's language.
	 * 
	 * @return the lang
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Gets the application's name.
	 * 
	 * @return name
	 */
	public @Nonnull String getName() {
		return name;
	}

	/**
	 * Sets the application's name.
	 * 
	 * @param name
	 *            application name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the name is invalid
	 */
	public void setName(@Nonnull final String name) throws IllegalArgumentException {
		if (name.length() == 0 || name.length() > 255) {
			throw new IllegalArgumentException(
					"Application name empty or longer than 255 characters!");
		}
		this.name = name;
	}

	/**
	 * Gets the application's description.
	 * 
	 * @return description
	 */
	public @Nonnull String getDescription() {
		return description;
	}

	/**
	 * Sets the application's description.
	 * 
	 * @param description
	 *            the description to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the description is invalid
	 */
	public void setDescription(@Nonnull final String description)
			throws IllegalArgumentException {
		if (description.length() > 65535) {
			throw new IllegalArgumentException(
					"Application description longer than 65535 characters!");
		}
		this.description = description;
	}

	/**
	 * Gets the image URL of this application.
	 * 
	 * @return URL to an image
	 */
	public @Nullable String getImage() {
		return image;
	}

	/**
	 * Sets the image URL of this application.
	 * 
	 * @param image
	 *            the image URL to set (up to 10000 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the image path is invalid
	 */
	public void setImage(@Nullable final String image) throws IllegalArgumentException {
		if (image != null) {
			if (image.length() > 10000) {
				throw new IllegalArgumentException(
						"Application image URL longer than 10000 characters!");
			}
			// DEFAULT schemes = "http", "https", "ftp"
			final String[] schemes = { "http", "https" };
			final UrlValidator urlValidator = new UrlValidator(schemes);
			if (!urlValidator.isValid(image)) {
				throw new IllegalArgumentException(
						"Application image URL is invalid!");
			}
		}
		this.image = image;
	}

	/**
	 * Gets the terms of service for this application.
	 * 
	 * @return terms of service
	 */
	public @Nonnull String getTos() {
		return tos;
	}

	/**
	 * Sets the terms of service for this application.
	 * 
	 * @param tos
	 *            the terms of service to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the terms of service are invalid
	 */
	public void setTos(@Nonnull final String tos) throws IllegalArgumentException {
		if (tos.length() > 65535) {
			throw new IllegalArgumentException(
					"Application ToS longer than 65535 characters!");
		}
		this.tos = tos;
	}

	/**
	 * Gets all the tags of this application.
	 * 
	 * @return application tags
	 */
	public @Nonnull Set<String> getTags() {
		return tags;
	}

	/**
	 * Adds the given application tag.
	 * 
	 * @param tag
	 *            a tag (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the tag is invalid
	 */
	public void addTag(@Nonnull final String tag) throws IllegalArgumentException {
		if (tag.length() > 255) {
			throw new IllegalArgumentException(
					"Application tag longer than 255 characters!");
		}
		this.tags.add(tag);
	}

	/**
	 * Removes the given application tag (if it exists).
	 * 
	 * @param tag
	 *            a tag (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the tag is invalid
	 */
	public void removeTag(@Nonnull final String tag) {
		if (tag.length() > 255) {
			throw new IllegalArgumentException(
					"Application tag longer than 255 characters!");
		}
		this.tags.remove(tag);
	}

	// public void setTags(@Nonnull Set<AppTag> tags) {
	// this.tags = tags;
	// }

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Application}
	 * bean.
	 *
	 * @return an application object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Application toBean() {
		return app.toBean(this);
	}
}
