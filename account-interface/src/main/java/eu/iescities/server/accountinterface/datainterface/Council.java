package eu.iescities.server.accountinterface.datainterface;

/**
 * Everything about a council of the IES Cities platform.
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 * @author Nico Kruber, kruber@zib.de
 */
public class Council {
    /**
     * Unique identifier of a council (invalid if < 0).
     */
	private int councilId = -1;
	
	/**
	 * Language of localisable fields, e.g. name and description.
	 */
	private Language lang;
	
	/**
	 * Name of a council.
	 */
	private String name;
	
	/**
	 * Description of a council.
	 */
	private String description;
	
	/**
	 * Logo or image of a council.
	 */
	private String image;
	private GeoScope geographicalScope;

	/**
	 * Default constructor
	 */
	public Council() {
		super();
	}

	/**
	 * Gets the council's ID.
	 * 
	 * @return council ID
	 */
	public int getCouncilId() {
		return councilId;
	}


	/**
	 * Sets the council's ID.
	 * 
	 * @param councilId
	 *            council ID to set
	 */
	public void setCouncilId(final int councilId) {
		this.councilId = councilId;
	}

	/**
	 * Gets the language of localisable fields, e.g. name and description.
	 * 
	 * @return the language used
	 */
	public Language getLang() {
		return lang;
	}

	/**
	 * Sets the language of localisable fields, e.g. name and description.
	 * 
	 * @param lang
	 *            the language to set
	 */
	public void setLang(final Language lang) {
		this.lang = lang;
	}

	/**
	 * Gets the council's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the council's name.
	 * 
	 * @param name
	 *            name to set (up to 255 characters)
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the council's description.
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the council's description.
	 * 
	 * @param description
	 *            description to set (up to 65535 characters)
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the image of this council.
	 * 
	 * @return path to an image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the image of this council.
	 * 
	 * @param image
	 *            the image path to set (up to 10000 characters)
	 */
	public void setImage(final String image) {
		this.image = image;
	}

	/**
	 * Gets the geographical scope of the council.
	 * 
	 * @return the geographical scope
	 */
	public GeoScope getGeographicalScope() {
		return geographicalScope;
	}

	/**
	 * Sets the geographical scope of the council.
	 * 
	 * @param geographicalScope
	 *            the geographical scope to set
	 */
	public void setGeographicalScope(final GeoScope geographicalScope) {
		this.geographicalScope = geographicalScope;
	}
}
