/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.JDODataStoreException;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import org.datanucleus.util.StringUtils;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;

/**
 * Methods for retrieving and changing dataset information.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceAware
public class DatasetManagement {
	/**
	 * Maximum number of allowed elements to retrieve when querying for a
	 * possibly big list.
	 */
	protected static final int MAX_LIST_ELEMENTS_LIMIT = 1000;

	/**
	 * Registers a new dataset.
	 * 
	 * NOTE: Only allowed by council admin users!
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param datasetBean
	 *            the dataset bean object containing at least a dataset name and
	 *            a language field
	 * 
	 * @return the new dataset object filled with the data written to the
	 *         DB
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid or the user is not a
	 *             council admin
	 * @throws IllegalArgumentException
	 *             if the name of the dataset is already used or a parameter is
	 *             invalid
	 */
	public static Dataset registerDataset(final String sessionId,
			final Dataset datasetBean) throws SecurityException,
			IllegalArgumentException {
		if (datasetBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			final DatasetTranslation datasetL10n = new DatasetTranslation(
					datasetBean.getLang());
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final Dataset result = storeDataset(pm, user, datasetL10n, datasetBean)
						.toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Updates dataset data with new information.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param datasetBean
	 *            the dataset bean object containing the information to
	 *            change (must contain at least the dataset id and a
	 *            language field)
	 * 
	 * @return a new dataset bean object filled with the data written to the
	 *         DB
	 * @throws SecurityException
	 *             if the user is not authorised to change the dataset, i.e.
	 *             not the dataset's council admin or an IES Cities admin
	 * @throws IllegalArgumentException
	 *             is thrown if the dataset ID is missing in the bean or on
	 *             invalid parameters
	 */
	public static Dataset updateDataset(final String sessionId,
			final Dataset datasetBean) throws SecurityException,
			IllegalArgumentException {
		if (datasetBean.getDatasetId() < 0) {
			throw new IllegalArgumentException("No dataset ID supplied.");
		}
		if (datasetBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
	
				DatasetTranslation datasetL10n;
				try {
					datasetL10n = getDatasetTranslationById(pm, datasetBean.getDatasetId(),
							datasetBean.getLang(), null);
				} catch (final IllegalArgumentException e) {
					// no translation for this language yet
					datasetL10n = new DatasetTranslation(datasetBean.getLang());
					datasetL10n.setDataset(getDatasetById(pm, datasetBean.getDatasetId()));
				}
				final Dataset result = storeDataset(pm, user, datasetL10n,
						datasetBean).toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Stores changes to a new or existing dataset by converting the
	 * {@link Dataset} bean properties.
	 * 
	 * Note: Only the dataset name, description and quality rating (only with a
	 * valid verifying user id) can be changed.
	 * 
	 * Wrap around {@link Transaction#begin()} and {@link Transaction#commit()}
	 * calls to execute all statements in a transaction.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param user
	 *            the user trying to store the dataset
	 * @param datasetL10n
	 *            a dataset's translation object from the database (or a
	 *            newly created one with no properties set)
	 * @param datasetBean
	 *            the dataset bean object containing at least a dataset ID and
	 *            the information to change
	 * 
	 * @return a new dataset bean object filled with the data written to the DB
	 * 
	 * @throws SecurityException
	 *             if the user is not authorised to change the dataset, i.e. not
	 *             the dataset's council admin or an IES Cities admin
	 * @throws IllegalArgumentException
	 *             is thrown if the dataset ID is missing in the bean or on
	 *             invalid parameters
	 */
	private static DatasetTranslation storeDataset(
			final PersistenceManager pm,
			final eu.iescities.server.accountinterface.User user,
			final DatasetTranslation datasetL10n,
			final Dataset datasetBean) throws SecurityException,
			IllegalArgumentException {
		if (datasetL10n.getDataset() != null) {
			// existing dataset
			if (!isDatasetAdmin(datasetL10n.getDataset(), user)) {
				throw new SecurityException(
						"User not authorized to modify dataset.");
			}
		} else {
			// create new dataset
			datasetL10n.setDataset(new eu.iescities.server.accountinterface.Dataset());
		}
		final eu.iescities.server.accountinterface.Dataset dataset = datasetL10n.getDataset();

		// check unique dataset name:
		if (dataset.getDatasetId() < 0
				&& StringUtils.isEmpty(datasetBean.getName())) {
			throw new IllegalArgumentException("Dataset name null or empty.");
		}
		if (dataset.getDatasetId() < 0
				|| (datasetBean.getName() != null && !datasetL10n.getName()
						.equals(datasetBean.getName()))) {
			if (pm.newQuery("SELECT UNIQUE dataset.datasetId FROM "
					+ DatasetTranslation.class.getName()
					+ " WHERE dataset.datasetId != :datasetId && name == :name")
					.execute(dataset.getDatasetId(), datasetBean.getName()) != null) {
				throw new IllegalArgumentException(
						"Dataset name already exists.");
			}

			datasetL10n.setName(datasetBean.getName());
		}

		if (datasetBean.getDescription() != null) {
			datasetL10n.setDescription(datasetBean.getDescription());
		}
		if (datasetBean.getJsonMapping() != null) {
			dataset.setJsonMapping(datasetBean.getJsonMapping());
		}

		if (dataset.getDatasetId() < 0) {
			// new dataset
			dataset.setPublishingDate(new Date());
			if (user.getManagedCouncil() != null) {
				// council dataset
				dataset.setPublishingCouncil(user.getManagedCouncil());
			}
			dataset.setPublishingUser(user);
		}

		if (datasetBean.getQuality() != -1) {
			if (dataset.getPublishingCouncil() != null) {
				// council dataset
				dataset.setQuality(datasetBean.getQuality());
				dataset.setQualityVerifiedBy(null);
			} else {
				throw new IllegalArgumentException(
						"User datasets do not support the quality field.");
			}
		}
		
		if (datasetBean.getQualityVerifiedBy() != -1) {
			if (dataset.getPublishingCouncil() != null) {
				if (datasetBean.getQualityVerifiedBy() != user.getUserId()) {
					throw new IllegalArgumentException(
							"Wrong quality verification field (must be the own user id).");
				}
				dataset.setQualityVerifiedBy(user);
			} else {
				throw new IllegalArgumentException(
						"User datasets do not support the verifiedBy field.");
			}
		}
		
		pm.makePersistent(datasetL10n);
		return datasetL10n;
	}

	/**
	 * Checks whether the user is a dataset admin of the dataset, i.e. either a
	 * council admin of a council dataset or the publishing user of a user
	 * dataset.
	 * 
	 * @param dataset
	 *            the dataset to check
	 * @param user
	 *            the user to verify
	 * 
	 * @return whether the user is allowed to change this dataset or not
	 */
	public static boolean isDatasetAdmin(
			final eu.iescities.server.accountinterface.Dataset dataset,
			final eu.iescities.server.accountinterface.User user) {
		if (user.isIesAdmin()) {
			return true;
		}
		if (dataset.getPublishingCouncil() != null) {
			// council dataset
			return dataset.getPublishingCouncil().getCouncilAdmins()
					.contains(user);
		} else if (dataset.getPublishingUser() != null) {
			// user dataset
			return dataset.getPublishingUser().getUserId() == user.getUserId();
		}
		return false;
	}

	/**
	 * Removes a dataset from the IES cities platorm.
	 * 
	 * NOTE: A dataset can only be removed by an admin of the council which
	 * published the dataset or an IES cities platform admin!
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param datasetId
	 *            the data set's ID
	 * 
	 * @return the number of instances that were deleted
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid or the user is not in
	 *             the corresponding list of council admins
	 */
	public static long removeDataSet(final String sessionId, final int datasetId)
			throws SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final User user = UserManagement.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Dataset dataset = getDatasetById(
						pm, datasetId);
				if (!isDatasetAdmin(dataset, user)) {
					throw new SecurityException(
							"User not authorized to remove given dataset"
									+ " (no admin of associated council).");
				}
				pm.deletePersistent(dataset);
				tx.commit();
				return 1;
			} catch (final IllegalArgumentException e) {
				return 0;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets a dataset by its ID.
	 * 
	 * @param datasetId
	 *            the ID of the dataset to retrieve
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return the dataset
	 * @throws IllegalArgumentException
	 *             is thrown if there is no dataset with the given ID
	 */
	public static Dataset getDatasetById(final int datasetId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getDatasetTranslationById(pm, datasetId, lang,
					fallbackLang).toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a dataset by its name.
	 * 
	 * @param datasetName
	 *            the name of the dataset to retrieve
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return the dataset
	 * @throws IllegalArgumentException
	 *             is thrown if there is no dataset with the given name
	 */
	public static Dataset getDatasetByName(final String datasetName,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getDatasetTranslationByName(pm, datasetName, lang,
					fallbackLang).toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a subset of the list of all datasets.
	 * 
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of datasets sorted by the dataset IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	public static List<Dataset> getAllDataSets(final int offset,
			final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
					throws IllegalArgumentException {
		return findDataSets("", offset, limit, lang, fallbackLang);
	}

	/**
	 * Gets a subset of the list of all datasets containing the given search
	 * string.
	 * 
	 * The search string is split into words and each is matched against dataset
	 * names and descriptions (OR-connected), case-insensitively.
	 * 
	 * @param searchString
	 *            the string to search for (space-separated words)
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of datasets including at least one of the search words and
	 *         sorted by the dataset IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Dataset> findDataSets(final String searchString,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ DatasetTranslation.class.getName()
					+ " ORDER BY dataset.datasetId ASC, lang ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			final StringBuilder filter = new StringBuilder();
			filter.append("(lang == '");
			filter.append(lang);
			if (fallbackLang != null) {
				filter.append("' || lang == '");
				filter.append(fallbackLang);
			}
			filter.append("') && ");
			List<DatasetTranslation> datasets;
			if (searchString.length() > 0) {
				final String[] searchWords = StringUtils.split(
						StringUtils.removeSpecialTagsFromString(searchString),
						" ");
				// note: define the words as real parameters to prevent SQL
				// injections (datanucleus must filter these)
				final HashMap<String, String> parameters = new HashMap<String, String>(
						searchWords.length);
				final StringBuilder parameterDecl = new StringBuilder();
				for (int i = 0; i < searchWords.length; ++i) {
					final String pexpr = "wexpr" + i;
					parameters.put(pexpr, ".*" + searchWords[i].toLowerCase()
							+ ".*");
					parameterDecl.append("String ");
					parameterDecl.append(pexpr);
					parameterDecl.append(", ");
					filter.append("name.toLowerCase().matches(");
					filter.append(pexpr);
					filter.append(") || description.toLowerCase().matches(");
					filter.append(pexpr);
					filter.append(") || ");
				}
				assert (parameterDecl.length() > 2);
				query.declareParameters(parameterDecl.substring(0,
						parameterDecl.length() - 2));
				assert (filter.length() > 4);
				query.setFilter(filter.substring(0, filter.length() - 4));
				datasets = (List<DatasetTranslation>) query.executeWithMap(parameters);
			} else {
				query.setFilter(filter.substring(0, filter.length() - 4));
				datasets = (List<DatasetTranslation>) query.execute();
			}
			return filterDuplicateDatasetTranslations(datasets, lang,
					fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Filter duplicate translations of a dataset list in favour of the (first)
	 * <tt>lang</tt>.
	 * 
	 * @param datasets
	 *            a list of datasets with possible duplicates of the two
	 *            available languages neighbouring each other
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 *
	 * @return a list of datasets with duplicate translations removed
	 */
	protected static ArrayList<Dataset> filterDuplicateDatasetTranslations(
			final Collection<DatasetTranslation> datasets, final Language lang,
			final Language fallbackLang) {
		final ArrayList<Dataset> result = new ArrayList<Dataset>(
				datasets.size());
		// filter duplicate datasets with lang and fallbackLang
		Dataset previousDataset = null;
		for (final DatasetTranslation datasetL10n : datasets) {
			if (previousDataset != null
					&& previousDataset.getDatasetId() == datasetL10n
							.getDataset().getDatasetId()) {
				if (previousDataset.getLang() == fallbackLang) {
					assert datasetL10n.getLang() == lang;
					previousDataset = datasetL10n.toBean();
					result.set(result.size() - 1, previousDataset);
				}
			} else {
				previousDataset = datasetL10n.toBean();
				result.add(previousDataset);
			}
		}
		return result;
	}

	/**
	 * Finds all applications that are using a given dataset.
	 * 
	 * @param datasetId
	 *            the ID of the dataset in question
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return list of application objects ordered (ascending) by their IDs
	 *         (empty if <tt>datasetId</tt> does not exist)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Application> getAppsUsingDataSet(final int datasetId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Application.class.getName()
					+ " WHERE datasets.contains(dataset)"
					+ " && dataset.datasetId == :param"
					+ " ORDER BY appId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return ApplicationManagement.getAppTranslations(pm,
					(List<eu.iescities.server.accountinterface.Application>) query
							.execute(datasetId),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Verifies that a given ID points to a valid dataset.
	 *
	 * @param datasetId
	 *            the ID to check
	 * 
	 * @return whether the dataset id exists or not
	 */
	public static @Nonnull boolean verifyDatasetId(final int datasetId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT UNIQUE"
					+ " datasetId"
					+ " FROM " + eu.iescities.server.accountinterface.Dataset.class.getName()
					+ " WHERE datasetId == :param");
			return null != query.execute(datasetId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a dataset by its ID.
	 * 
	 * @param pm
	 *            a PersistenceManager object
	 * @param datasetId
	 *            the ID of the dataset to retrieve
	 * 
	 * @return a dataset attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if there is no dataset with the given ID
	 */
	protected static @Nonnull eu.iescities.server.accountinterface.Dataset getDatasetById(
			final PersistenceManager pm, final int datasetId)
			throws IllegalArgumentException {
		try {
			return pm.getObjectById(
					eu.iescities.server.accountinterface.Dataset.class,
					"" + datasetId);
		} catch (final JDOObjectNotFoundException e) {
			throw new IllegalArgumentException("Unknown dataset id: "
					+ datasetId + ".", e);
		}
	}

	/**
	 * Finds a translation for a dataset by it's id and language code.
	 * 
	 * @param pm
	 *            a PersistenceManager object
	 * @param datasetId
	 *            the ID of the dataset to retrieve
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a dataset translation attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if there is no dataset with the given ID
	 */
	protected static @Nonnull DatasetTranslation getDatasetTranslationById(
			final PersistenceManager pm, final int datasetId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
					throws IllegalArgumentException {
		final Query query = pm.newQuery("SELECT UNIQUE FROM "
				+ DatasetTranslation.class.getName()
				+ " WHERE dataset.datasetId == :datasetId && lang == :lang");
		DatasetTranslation datasetL10n = (DatasetTranslation) query
				.execute(datasetId, lang);
		if (datasetL10n == null && fallbackLang != null) {
			datasetL10n = (DatasetTranslation) query.execute(datasetId,
					fallbackLang);
		}
		if (datasetL10n == null) {
			throw new IllegalArgumentException(
					"No dataset translation for dataset id " + datasetId
							+ " into language " + lang + ".");
		}
		return datasetL10n;
	}

	/**
	 * Finds a translation for a dataset by it's name and language code.
	 * 
	 * @param pm
	 *            a PersistenceManager object
	 * @param datasetName
	 *            the name of the dataset to retrieve
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a dataset translation attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if there is no dataset with the given ID
	 */
	protected static @Nonnull DatasetTranslation getDatasetTranslationByName(
			final PersistenceManager pm, final String datasetName,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
					throws IllegalArgumentException {
		final Query query = pm.newQuery("SELECT UNIQUE FROM "
				+ DatasetTranslation.class.getName()
				+ " WHERE name == :datasetName && lang == :lang");
		DatasetTranslation datasetL10n = (DatasetTranslation) query
				.execute(datasetName, lang);
		if (datasetL10n == null && fallbackLang != null) {
			datasetL10n = (DatasetTranslation) query.execute(datasetName,
					fallbackLang);
		}
		if (datasetL10n == null) {
			throw new IllegalArgumentException(
					"No dataset translation for dataset name " + datasetName
							+ " into language " + lang + ".");
		}
		return datasetL10n;
	}

	/**
	 * Finds translations for dataset objects with a fallback of using empty
	 * fields.
	 * 
	 * @param pm
	 *            an instance of the PersistenceManager
	 * @param datasets
	 *            the datasets
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 *
	 * @return an object bean with either the appropriate translation or empty
	 *         translation fields
	 */
	protected static List<Dataset> getDatasetTranslations(
			final PersistenceManager pm,
			final Collection<eu.iescities.server.accountinterface.Dataset> datasets,
			final Language lang, final Language fallbackLang) {
		final ArrayList<Dataset> result = new ArrayList<Dataset>(
				datasets.size());
		for (final eu.iescities.server.accountinterface.Dataset dataset : datasets) {
			Dataset bean;
			try {
				final DatasetTranslation appL10n = getDatasetTranslationById(pm,
						dataset.getDatasetId(), lang, fallbackLang);
				bean = dataset.toBean(appL10n);
			} catch (final IllegalArgumentException e) {
				// no translation available: use empty fields:
				bean = dataset.toBean(new DatasetTranslation());
			}
			result.add(bean);
		}
		return result;
	}

	protected static final String queryDatasetByName = "SELECT FROM "
			+ eu.iescities.server.accountinterface.Dataset.class.getName()
			+ " WHERE (SELECT dataset.datasetId FROM "
			+ DatasetTranslation.class.getName()
			+ " WHERE name == :name GROUP BY dataset.datasetId)"
			+ ".contains(datasetId)";

}
