/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * An IES Cities council definition.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false", identityType = IdentityType.APPLICATION,
                    objectIdClass = Council.PK.class)
class Council {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int councilId = -1;
	
    /**
     * Inner class representing Primary Key
     */
	public static class PK implements Serializable {
		private static final long serialVersionUID = 6593424468946295027L;
		public int councilId; // same name as the real field above

		public PK() {
		}

		public PK(final String s) {
			this.councilId = Integer.valueOf(s).intValue();
		}

		@Override
		public String toString() {
			return "" + councilId;
		}

		@Override
		public int hashCode() {
			return councilId;
		}

		@Override
		public boolean equals(final Object other) {
			if (other != null && (other instanceof PK)) {
				final PK otherPK = (PK) other;
				return otherPK.councilId == this.councilId;
			}
			return false;
		}
	}

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "false")
	private GeoScope geographicalScope;

	/**
	 * Bi-directional 1-N mapping between a council and its admins.
	 */
	@Persistent(mappedBy = "managedCouncil", defaultFetchGroup = "false")
	@Nonnull private Set<User> councilAdmins = new HashSet<User>();

	/**
	 * Gets the council's ID.
	 * 
	 * @return council ID
	 */
	public int getCouncilId() {
		return councilId;
	}

	/**
	 * @return the geographicalScope
	 */
	public GeoScope getGeographicalScope() {
		return geographicalScope;
	}

	/**
	 * @param geographicalScope
	 *            the geographicalScope to set
	 */
	public void setGeographicalScope(@Nonnull final GeoScope geographicalScope) {
		this.geographicalScope = geographicalScope;
	}

	/**
	 * Gets the admins for the council.
	 * 
	 * @return set of council admins
	 */
	public @Nonnull Set<User> getCouncilAdmins() {
		return councilAdmins;
	}

	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.Council} bean.
	 *
	 * @param councilL10n
	 *            localised council fields
	 * 
	 * @return a council object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.Council toBean(
			@Nonnull final CouncilTranslation councilL10n) {
		final eu.iescities.server.accountinterface.datainterface.Council result = new eu.iescities.server.accountinterface.datainterface.Council();
		result.setCouncilId(councilId);
		result.setLang(councilL10n.getLang());
		result.setName(councilL10n.getName());
		result.setDescription(councilL10n.getDescription());
		result.setImage(councilL10n.getImage());
		result.setGeographicalScope(geographicalScope.toBean());
		return result;
	}
}
