/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface.datainterface;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

/**
 * Persistence manager helper.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
public final class PMF {
	private static final PersistenceManagerFactory pmfInstance = JDOHelper
			.getPersistenceManagerFactory("datanucleus.properties");

	/**
	 * Gets an instance of PersistenceManager from the factory
	 * 
	 * @return a PersistenceManager instance with default options
	 */
	public static PersistenceManager getPM() {
		return pmfInstance.getPersistenceManager();
	}
}
