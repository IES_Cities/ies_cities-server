package eu.iescities.server.accountinterface;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;

/**
 * Extracts comments from Google Play.
 * 
 * @author Oscar Peña, oscar.pena@deusto.es
 */
public class CommentExtractor {
	
	private static final String FULL_REVIEW_TEXT_ENGLISH = "Full Review";
	private static final String FULL_REVIEW_TEXT_SPANISH = "Opinión completa";
	private static final String FULL_REVIEW_TEXT_ITALIAN = "Recensione completa";

	private static final String playStoreURL = "https://play.google.com/store/apps/details?id=";

	static int convertToFiveStarRatingSystem(final int rating) {
		return rating / 20;
	}

	static Date generateDateObject(final String date, final Language language) throws ParseException {
		if (language != null) {
			switch (language) {
				case ES:
					return new SimpleDateFormat("dd' de 'MMMM' de 'yyyy", new Locale("es", "ES")).parse(date);
		
				case EN:
					return new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH).parse(date);
		
				case IT:
					return new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN).parse(date);
			}
		}
		return new Date();
	}

	static String trimFullReviewText(final String comment, final Language language) {
		String fullReviewText = null;
		Integer index = null;

		if (language!=null) {
			switch (language) {
				case ES:
					fullReviewText = FULL_REVIEW_TEXT_SPANISH;
					break;
		
				case EN:
					fullReviewText = FULL_REVIEW_TEXT_ENGLISH;
					break;
		
				case IT:
					fullReviewText = FULL_REVIEW_TEXT_ITALIAN;
					break;
		
				default:
					fullReviewText = "";
					break;
			}
		}
		else {
			fullReviewText = "";			
		}

		index = comment.indexOf(fullReviewText);
		
		if (index != -1) {
			return comment.substring(0, index).trim();
		} 
		else {
			return comment.trim();
		}
	}
	
	public static Language checkCommentLanguage(final String comment) {
		if (comment.endsWith(FULL_REVIEW_TEXT_ITALIAN)) {
			return Language.IT;
		}
		else if (comment.endsWith(FULL_REVIEW_TEXT_SPANISH)) {
			return Language.ES;
		}
		else {
			return Language.EN;
		}
	}

	public static void extractCommentsFromGooglePlay(final String packageName, final Language language) {
		
		final PersistenceManager pm = PMF.getPM();
		final StringBuilder url = new StringBuilder(playStoreURL).append(packageName);

		if (language!=null) {
			switch (language) {
				case ES:
					url.append("&hl=es");
					break;
		
				case EN:
					url.append("&hl=en");
					break;
		
				case IT:
					url.append("&hl=it");
					break;
			}
		}

		try {
			final Query appQuery = pm.newQuery("SELECT UNIQUE FROM "
					+ Application.class.getName()
					+ " WHERE packageName == :param");
			final Application application = (Application) appQuery
					.execute(packageName);
			if (application == null) {
				throw new NullPointerException();
			}
			
			final Document doc = Jsoup.connect(url.toString()).get();

			final Elements reviews = doc.select(".single-review");

			for (final Element review : reviews) {
				final String author = review.select(".author-name").text()
						.trim();

				final Elements reviewBody = review.select(".review-body");

				final String title = reviewBody.select(".review-title").text();
				
				final String comment_text = reviewBody.text().substring(title.length());

				final Language comment_language = checkCommentLanguage(comment_text);
				
				final Date date = generateDateObject(
						review.select(".review-date").text(), comment_language);
				
				final String comment = trimFullReviewText(comment_text, comment_language);
				
				String rating = review.select(".current-rating").attr("style");
				final Integer stars;
								
				if (rating.equalsIgnoreCase("")) {
					stars = 0;
				}
				else {
					rating = rating.replaceAll("[^0-9]", "");
					stars = convertToFiveStarRatingSystem(Integer.parseInt(rating));
				}

				/*
				 * Generate comment object with the following information: -
				 * author - date - stars - title - comment - language
				 */
				final AppRating appRating = new AppRating();
				appRating.setComment(comment);
				appRating.setDate(date);
				appRating.setLang(comment_language);							
				appRating.setRating(stars);
				appRating.setTitle(title.substring(0, title.length() > 255?255:title.length()));
				appRating.setUserName(author);
				appRating.setApp(application);
				
				// check for previously imported ratings:
				final Query appRatingQuery = pm.newQuery("SELECT UNIQUE FROM "
						+ AppRating.class
								.getName()
						+ " WHERE app.appId == :appid"
						+ " && userName == :username"
						+ " && date == :date");
				
				final eu.iescities.server.accountinterface.AppRating prevRating = (eu.iescities.server.accountinterface.AppRating) appRatingQuery
						.execute(application.getAppId(), author, date);
				
				if (prevRating == null) {
					pm.makePersistent(appRating);
				}
			}
		} 
		catch (final IOException e) {
			e.printStackTrace();
		} 
		catch (final ParseException e) {
			e.printStackTrace();
		} 
		finally {
			pm.close();
		}
	}
	
	public static void extractCommentsFromGooglePlay(final String packageName) {
		// TODO: extract Google Play comments for all application languages in the DB?
		extractCommentsFromGooglePlay(packageName, null);
	}
	
	public static void extractRatingFromGooglePlay(final String packageName) {
		final PersistenceManager pm = PMF.getPM();
		final StringBuilder url = new StringBuilder(playStoreURL).append(packageName);
		
		try {
			final Query appQuery = pm.newQuery("SELECT UNIQUE FROM " + Application.class.getName() + " WHERE packageName == :param");
			final Application application = (Application) appQuery.execute(packageName);
			
			if (application == null) {
				throw new NullPointerException();
			}
			
			final Document doc = Jsoup.connect(url.toString()).get();
			
			// update overall Google Play Score (includes scores without comments):
			final String scoreString = doc.select(".score").text();
			final double score = Double.parseDouble(scoreString.replace(",", "."));
			
			application.setGooglePlayRating(score);
			pm.makePersistent(application);			
		}
		catch (final IOException e) {
			e.printStackTrace();
		}
		finally {
			pm.close();
		}
	}
	
	
	public static void main(final String args[]) {		
		System.out.println("Comments & Ratings extractor");

		for (final Language lang : Language.values()) {
			try {
				final List<eu.iescities.server.accountinterface.datainterface.Application> allApps = ApplicationManagement
						.getAllApps(0, 1000, lang, null);

				for (final eu.iescities.server.accountinterface.datainterface.Application app : allApps) {
					final String packageName = app.getPackageName();

					System.out.println("Extracting data from " + packageName);

					extractRatingFromGooglePlay(packageName);

					extractCommentsFromGooglePlay(packageName, lang);
				}
				
			} catch (final IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Extractor process finished");
	}
}
