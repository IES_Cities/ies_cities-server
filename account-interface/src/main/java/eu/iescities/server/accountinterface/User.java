/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;
import javax.jdo.listener.StoreCallback;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * Persistable user object with all its relations.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceCapable(detachable = "false")
class User implements StoreCallback {
	/**
	 * Default session lifetime in Milliseconds.
	 * 
	 * This is the maximum time span after the last action the session is still
	 * valid. The actual time span may however be 1/3 shorter as the session
	 * lifetime is only updated after 1/3 of the time span has passed (as an
	 * optimisation).
	 */
	private static long DEFAULT_SESSION_TIMEOUT = 60 * 60 * 1000;
	private static long DEFAULT_SESSION_TIMEOUT_23 = (2 * DEFAULT_SESSION_TIMEOUT) / 3;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int userId;

	/**
	 * User name for login (non-empty, max 255 characters).
	 */
	@Persistent
	@Unique
	@Column(allowsNull = "false")
	private String username;

	/**
	 * User password hash, may be <tt>null</tt> if authenticated via OAuth 2.0
	 * only!
	 */
	@Persistent
	@Column(allowsNull = "true")
	private @Nullable String password;

	/**
	 * OAuth 2.0 profile ID as issued by Google.
	 */
	@Persistent
	@Unique
	@Column(allowsNull = "true")
	private @Nullable String googleProfile;

	/**
	 * First name, visible by all users, e.g. for comments, lists of app
	 * developers etc.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String firstname = "";

	/**
	 * Surname, visible by all users, e.g. for comments, lists of app
	 * developers etc.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull String surname = "";

	@Persistent
	@Column(allowsNull = "true")
	private @Nullable String email;

	@Persistent
	@Unique(name = "LOWERCASE_EMAIL")
	@Column(allowsNull = "true")
	private @Nullable String lowercaseEmail;

	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	private @Nullable Council preferredLocation;

	/**
	 * User self-description (up to 65535 characters).
	 */
	@Persistent
	@Column(length = 65535, allowsNull = "false")
	private @Nonnull String profile = "";

	@Persistent
	@Unique
	@Column(allowsNull = "true")
	private @Nullable String sessionId;

	/**
	 * With a valid {@link #sessionId} this is the expiry date; if
	 * {@link #sessionId} is <tt>null</tt> this is the date of the last logout
	 * or the time the user was created.
	 */
	@Persistent
	@Column(allowsNull = "false")
	private @Nonnull Date sessionExpires = new Date();

	@Persistent
	@Column(allowsNull = "false")
	private boolean isIesAdmin = false;

	/**
	 * Bi-directional 1-N mapping between a user and the council he is admin
	 * for.
	 */
	@Persistent(defaultFetchGroup = "true")
	@Column(allowsNull = "true")
	private @Nullable Council managedCouncil;

	/**
	 * Uni-directional M-N mapping of a user and his installed applications.
	 */
	@Persistent(defaultFetchGroup = "false")
	@Join(column = "USERID", deleteAction = ForeignKeyAction.CASCADE)
	@Element(column = "APPID", deleteAction = ForeignKeyAction.CASCADE)
	private @Nonnull Set<Application> installedApps = new HashSet<Application>();

	/**
	 * Bi-directional M-N mapping between a user and his developed applications.
	 */
	@Persistent(mappedBy = "developers", defaultFetchGroup = "false")
	@Join(deleteAction = ForeignKeyAction.CASCADE)
	@Element(deleteAction = ForeignKeyAction.CASCADE)
	private @Nonnull Set<Application> developedApps = new HashSet<Application>();

	/**
	 * Gets the user's ID.
	 * 
	 * @return user ID
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Gets the user's username (for login).
	 * 
	 * @return name
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the user's username (for login).
	 * 
	 * @param username
	 *            to user name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the name is invalid
	 */
	public void setUsername(@Nonnull final String username)
			throws IllegalArgumentException {
		if (username.length() == 0 || username.length() > 255) {
			throw new IllegalArgumentException(
					"User name empty or longer than 255 characters!");
		}
		this.username = username;
	}

	/**
	 * Gets the hash of the user password.
	 * 
	 * @return salted hash
	 */
	public @Nullable String getPassword() {
		return password;
	}

	/**
	 * Sets the hash of the user (for login).
	 * 
	 * @param password
	 *            salted password hash to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the password is invalid
	 */
	public void setPassword(@Nonnull final String password)
			throws IllegalArgumentException {
		if (password.length() > 255) {
			throw new IllegalArgumentException(
					"User password longer than 255 characters!");
		}
		this.password = password;
	}

	/**
	 * Gets the OAuth 2.0 profile identification string as returned by Google.
	 * 
	 * @return profile identification string
	 */
	public @Nullable String getGoogleProfile() {
		return googleProfile;
	}

	/**
	 * Sets the OAuth 2.0 profile identification string as returned by Google.
	 * 
	 * @param googleProfile
	 *            the profile identification to set
	 */
	public void setGoogleProfile(@Nullable final String googleProfile) {
		this.googleProfile = googleProfile;
	}

	/**
	 * Gets the user's first firstname.
	 * 
	 * @return first firstname
	 */
	public @Nonnull String getName() {
		return firstname;
	}

	/**
	 * Sets the user's first name.
	 * 
	 * @param firstname
	 *            first name to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the name is invalid
	 */
	public void setName(@Nonnull final String firstname) throws IllegalArgumentException {
		if (firstname.length() > 255) {
			throw new IllegalArgumentException(
					"User first name longer than 255 characters!");
		}
		this.firstname = firstname;
	}

	/**
	 * Gets the user's surname.
	 * 
	 * @return surname
	 */
	public @Nonnull String getSurname() {
		return surname;
	}

	/**
	 * Sets the user's surname.
	 * 
	 * @param surname
	 *            surname to set (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the surname is invalid
	 */
	public void setSurname(@Nonnull final String surname)
			throws IllegalArgumentException {
		if (surname.length() > 255) {
			throw new IllegalArgumentException(
					"User surname longer than 255 characters!");
		}
		this.surname = surname;
	}

	/**
	 * Gets the user's email address.
	 * 
	 * @return email address or the empty string if no email address is stored
	 */
	public @Nonnull String getEmail() {
		if (email == null) {
			return "";
		}
		return email;
	}

	/**
	 * Sets the user's email address.
	 * 
	 * @param email
	 *            email to set (up to 255 characters) or the empty string if no
	 *            email address should be stored
	 * 
	 * @throws IllegalArgumentException
	 *             if the email is invalid
	 */
	public void setEmail(@Nonnull final String email)
			throws IllegalArgumentException {
		if (email.length() == 0) {
			this.email = null;
		} else if (email.length() > 255) {
			throw new IllegalArgumentException(
					"User email address longer than 255 characters!");
		} else {
			final EmailValidator emailValidator = EmailValidator.getInstance();
			if (!emailValidator.isValid(email)) {
				throw new IllegalArgumentException(
						"User email address is invalid!");
			}
			this.email = email;
		}
	}
	
	/**
	 * Makes sure the {@link #lowercaseEmail} field is filled accordingly.
	 */
	@Override
	public void jdoPreStore() {
		if (email != null) {
			lowercaseEmail = email.toLowerCase();
		} else {
			lowercaseEmail = null;
		}
	}

	/**
	 * Gets the user's preferred location.
	 * 
	 * @return a council
	 */
	public @Nullable Council getPreferredLocation() {
		return preferredLocation;
	}

	/**
	 * Sets the user's preferred location.
	 * 
	 * @param preferredLocation
	 *            council to set
	 */
	public void setPreferredLocation(@Nullable final Council preferredLocation) {
		this.preferredLocation = preferredLocation;
	}

	/**
	 * Gets the user's profile.
	 * 
	 * @return user profile
	 */
	public @Nonnull String getProfile() {
		return profile;
	}

	/**
	 * Sets the user's profile.
	 * 
	 * @param profile
	 *            user profile to set (up to 65535 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the user profile is invalid
	 */
	public void setProfile(@Nonnull final String profile)
			throws IllegalArgumentException {
		if (profile.length() > 65535) {
			throw new IllegalArgumentException(
					"User profile longer than 65535 characters!");
		}
		this.profile = profile;
	}

	/**
	 * Gets the user's session ID.
	 * 
	 * @return the session ID
	 */
	@Nullable String getSessionId() {
		return sessionId;
	}

	/**
	 * Starts a new user session with the given session ID and sets the default
	 * session lifetime from {@link #DEFAULT_SESSION_TIMEOUT}.
	 * 
	 * @param sessionId
	 *            the (unique!) session ID to use (up to 255 characters)
	 * 
	 * @throws IllegalArgumentException
	 *             if the session ID is invalid
	 */
	public void startSession(@Nonnull final String sessionId)
			throws IllegalArgumentException {
		if (sessionId.length() > 255) {
			throw new IllegalArgumentException(
					"User session ID longer than 255 characters!");
		}
		this.sessionId = sessionId;
		final Date date = new Date();
		date.setTime(date.getTime() + DEFAULT_SESSION_TIMEOUT);
		this.sessionExpires = date;
	}

	/**
	 * Updates the user's current session lifetime by adding
	 * {@link #DEFAULT_SESSION_TIMEOUT} Milliseconds to the current time if 1/3 of
	 * the time span has already passed.
	 * 
	 * This is the maximum time span after the last action the session is still
	 * valid. The actual time span may however be 1/3 shorter as the session
	 * lifetime is only updated after 1/3 of the time span has passed (as an
	 * optimisation).
	 */
	public void updateSession() {
		final Date date = new Date(System.currentTimeMillis()
				+ DEFAULT_SESSION_TIMEOUT_23);
		if (date.after(this.sessionExpires)) {
			this.sessionExpires = date;
		}
	}

	/**
	 * Gets the default session time in Milliseconds.
	 * 
	 * @return the session time in Milliseconds
	 */
	protected static long getDefaultSessionTime() {
		return DEFAULT_SESSION_TIMEOUT;
	}

	/**
	 * Changes the default session time, e.g. for tests.
	 * 
	 * @param defaultSessionTime
	 *            the default session time in Milliseconds to set
	 */
	protected static void setDefaultSessionTime(final long defaultSessionTime) {
		User.DEFAULT_SESSION_TIMEOUT = defaultSessionTime;
		User.DEFAULT_SESSION_TIMEOUT_23 = (2 * defaultSessionTime) / 3;
	}

	/**
	 * Stops a user's session by setting the session ID to <tt>null</tt> and
	 * updating the session lifetime with the current date (= logout date).
	 */
	public void stopSession() {
		this.sessionExpires = new Date();
		this.sessionId = null;
	}

//	/**
//	 * Gets the user's current session ID.
//	 * 
//	 * @return session ID (may be <tt>null</tt>)
//	 */
//	public @Nullable String getSessionId() {
//		return this.sessionId;
//	}

	/**
	 * Checks whether the user's session is valid, i.e. not expired yet.
	 * 
	 * @return <tt>true</tt> if the session is still valid, <tt>false</tt>
	 *         otherwise
	 * @see #sessionExpires
	 */
	public boolean sessionIsValid() {
		return this.sessionExpires.after(new Date());
	}

	/**
	 * Gets the user's current IES Cities admin status.
	 * 
	 * @return <tt>true</tt> if the user is an IES Cities admin, <tt>false</tt>
	 *         otherwise
	 */
	public boolean isIesAdmin() {
		return isIesAdmin;
	}

	/**
	 * Sets the user's current IES Cities admin status.
	 * 
	 * @param isIesAdmin
	 *            whether the user should be admin or not
	 */
	public void setIesAdmin(final boolean isIesAdmin) {
		this.isIesAdmin = isIesAdmin;
	}

	/**
	 * Gets the council the user is a council admin for.
	 * 
	 * @return the council
	 */
	public @Nullable Council getManagedCouncil() {
		return managedCouncil;
	}

	/**
	 * Sets the user to manage this council, i.e. the user is an admin for the
	 * council.
	 * 
	 * @param managedCouncil
	 *            the managedCouncil to set
	 */
	public void setManagedCouncil(@Nullable final Council managedCouncil) {
		this.managedCouncil = managedCouncil;
	}

	/**
	 * Gets the apps the user has installed.
	 * 
	 * @return set of apps
	 */
	public @Nonnull Set<Application> getInstalledApps() {
		return installedApps;
	}

	/**
	 * Gets the apps the user develops.
	 * 
	 * @return set of apps
	 */
	public @Nonnull Set<Application> getDevelopedApps() {
		return developedApps;
	}
	
	/**
	 * Converts this object to a
	 * {@link eu.iescities.server.accountinterface.datainterface.User} bean.
	 * 
	 * Not every member is converted, e.g. the session ID and password are not
	 * copied to the bean for security reasons.
	 * 
	 * @return a user object bean
	 */
	public @Nonnull eu.iescities.server.accountinterface.datainterface.User toBean() {
		final eu.iescities.server.accountinterface.datainterface.User result = new eu.iescities.server.accountinterface.datainterface.User();
		result.setUserId(userId);
		result.setSessionExpires(sessionExpires);
		result.setUsername(username);
		result.setGoogleProfile(googleProfile);
		result.setName(firstname);
		result.setSurname(surname);
		result.setEmail(getEmail());
		result.setProfile(profile);
		if (preferredLocation != null) {
			result.setPreferredCouncilId(preferredLocation.getCouncilId());
		}
		if (managedCouncil != null) {
			result.setManagedCouncilId(managedCouncil.getCouncilId());
		}
		result.setAdmin(isIesAdmin);
		result.setDeveloper(!developedApps.isEmpty());
		return result;
	}
}
