/**
 *  Copyright 2013 Zuse Institute Berlin
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package eu.iescities.server.accountinterface;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.JDODataStoreException;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import org.apache.commons.validator.routines.EmailValidator;
import org.datanucleus.util.StringUtils;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Methods for retrieving and changing user information.
 * 
 * @author Nico Kruber, kruber@zib.de
 * @author Robert Döbbelin, doebbelin@zib.de
 */
@PersistenceAware
public class UserManagement {
	/**
	 * Maximum number of allowed elements to retrieve when querying for a
	 * possibly big list.
	 */
	protected static final int MAX_LIST_ELEMENTS_LIMIT = 1000;
	
	/**
	 * Lifetime of password reset requests in Milliseconds.
	 */
	private static long PASSWORD_RESET_TIMEOUT = 60 * 60 * 24 * 1000;

	
	/**
	 * Verifies Google OAuth 2.0 access tokens.
	 */
	final static GoogleIdTokenVerifier googleVerifier = new GoogleIdTokenVerifier(
			new NetHttpTransport(), new GsonFactory());

	/**
	 * Registers a user with username and password.
	 * 
	 * For registration with Google OAuth 2.0, set the <tt>id_token</tt> from
	 * Google as the <tt>googleProfile</tt> with
	 * {@link User#setGoogleProfile(String)}. The Google signature will be
	 * verified and we will extract the profile id to use internally and return
	 * as <tt>googleProfile</tt> in the returned {@link User} bean.
	 * 
	 * It is upon the caller to check for bad user names and password security.
	 * 
	 * @param userBean
	 *            the user bean object containing at least a user name and
	 *            password or user name and Google profile ID
	 * 
	 * @return a new user bean object filled with the data written to the DB
	 * @throws IllegalArgumentException
	 *             is thrown on invalid username, password or council ID are
	 *             invalid or if the username already exists
	 * @throws RuntimeException
	 *             if the user's password hash cannot be set due to a service
	 *             misconfiguration
	 */
	public static User registerUser(final User userBean)
			throws IllegalArgumentException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final eu.iescities.server.accountinterface.User user = new eu.iescities.server.accountinterface.User();
			
			try {
				return storeUser(pm, user, userBean, null);
			} catch (final JDODataStoreException e) {
				// likely to be an abort due to conflicting transactions
				intercept.waitToRetry(e);
			} finally {
				pm.close();
			}
		}
	}

	/**
	 * Updates a user profile with new information.
	 * 
	 * For updating registrations with Google OAuth 2.0, set the
	 * <tt>id_token</tt> from Google as the <tt>googleProfile</tt> with
	 * {@link User#setGoogleProfile(String)}. The Google signature will be
	 * verified and we will extract the profile id to use internally and return
	 * as <tt>googleProfile</tt> in the returned {@link User} bean.
	 * 
	 * It is upon the caller to check for bad user names and password security.
	 * 
	 * Note: Username changes are possible as long as the new name is not taken
	 * yet. Password changes additionally require the according parameter to be
	 * set. A password change is indicated by a non-<tt>null</tt> and non-empty
	 * password in the user bean).
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param userBean
	 *            the user bean object containing the information to change (the
	 *            user id is ignored)
	 * @param currentPassword
	 *            current password (only needed for password changes, otherwise
	 *            <tt>null</tt>)
	 * 
	 * @return a new user bean object filled with the data written to the DB
	 * @throws IllegalArgumentException
	 *             is thrown on invalid username, password or council ID are
	 *             invalid or if a new username already exists
	 * @throws RuntimeException
	 *             if the user's password hash cannot be set due to a service
	 *             misconfiguration
	 */
	public static User updateUser(final String sessionId, final User userBean,
			final String currentPassword) throws IllegalArgumentException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			
			try {
				final eu.iescities.server.accountinterface.User user = getUserBySession(
						pm, sessionId);
				return storeUser(pm, user, userBean, currentPassword); 
			} catch (final JDODataStoreException e) {
				// likely to be an abort due to conflicting transactions
				intercept.waitToRetry(e);
			} finally {
				pm.close();
			}
		}
	}

	/**
	 * Stores changes to a new or existing user by converting the {@link User}
	 * bean properties.
	 * 
	 * Wrap around {@link Transaction#begin()} and {@link Transaction#commit()}
	 * calls to execute all statements in a transaction.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param user
	 *            the user object from the database (or a newly created one with
	 *            no properties set)
	 * @param userBean
	 *            the user bean object containing at least a user name and
	 *            password or user name and Google profile ID
	 * @param currentPassword
	 *            current password (only needed for password changes, can be any
	 *            value (including <tt>null</tt>) otherwise)
	 * 
	 * @return a new user bean object filled with the data written to the DB
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown on invalid username, password or council ID are
	 *             invalid or if the username already exists
	 * @throws RuntimeException
	 *             if the user's password hash cannot be set due to a service
	 *             misconfiguration
	 */
	private static User storeUser(
			final PersistenceManager pm,
			final eu.iescities.server.accountinterface.User user,
			final User userBean, final String currentPassword)
			throws IllegalArgumentException, RuntimeException {
		final boolean changeUserName = userBean.getUsername() != null
				&& !userBean.getUsername().equals(user.getUsername());
		final boolean changeEmail = userBean.getEmail() != null
				 && !userBean.getEmail().equals(user.getEmail());
		final boolean hasPassword = !StringUtils
				.isEmpty(userBean.getPassword());
		final String googleProfile = StringUtils.isEmpty(userBean
				.getGoogleProfile()) ? null : verifyGoogleToken(userBean
				.getGoogleProfile());
		final boolean changeGoogleProfile = googleProfile != null
				 && !googleProfile.equals(user.getGoogleProfile());
		
		final Transaction tx = pm.currentTransaction();
		tx.begin();
		
		try {
			// password supplied or new user without google profile information
			// (here, a valid password is required!)
			if (hasPassword || (googleProfile == null && user.getUsername() == null)) {
				if (!hasPassword) {
					throw new IllegalArgumentException("No password provided. Please provide a password with at least 5 characters.");
				}
				if (userBean.getPassword().length() < 5) {
					throw new IllegalArgumentException("The password is too short (at least 5 characters required).");
				}

				try {
					// only store password if there is no old one yet or the
					// correct old password was provided
					if (user.getPassword() != null
							&& !PasswordHash.validatePassword(currentPassword,
									user.getPassword())) {
						throw new IllegalArgumentException("Incorrect old password.");
					}

					user.setPassword(PasswordHash.createHash(userBean.getPassword()));
				} catch (final NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				} catch (final InvalidKeySpecException e) {
					throw new RuntimeException(e);
				}
			}
			if (changeGoogleProfile) {
				user.setGoogleProfile(googleProfile);
			}
			if (userBean.getName() != null) {
				user.setName(userBean.getName());
			}
			if (userBean.getSurname() != null) {
				user.setSurname(userBean.getSurname());
			}
			if (changeEmail) {
				user.setEmail(userBean.getEmail());
			}
			if (userBean.getProfile() != null) {
				user.setProfile(userBean.getProfile());
			}
			// do not set admin property!
			// do not set managed council property
			// do not set installed/developed apps property

			// if new user or if existing user changing username,
			// check that the new name does not exist yet
			if (user.getUsername() == null
					&& StringUtils.isEmpty(userBean.getUsername())) {
				throw new IllegalArgumentException("Username null or empty.");
			}
			if (changeUserName) {
				user.setUsername(userBean.getUsername());
			}

			if (userBean.getPreferredCouncilId() >= 0) {
				final Council preferredCouncil = CouncilManagement.getCouncilById(
						pm, userBean.getPreferredCouncilId());
				user.setPreferredLocation(preferredCouncil);
			}
			pm.makePersistent(user);
			tx.commit();
			return user.toBean();
		} catch (final JDODataStoreException e) {
			if (tx.isActive()) {
				tx.rollback();
			}
			if (changeUserName) {
				if (pm.newQuery(
						"SELECT UNIQUE userId FROM "
								+ eu.iescities.server.accountinterface.User.class
								.getName() + " WHERE username == :param1")
								.execute(userBean.getUsername()) != null) {
					throw new IllegalArgumentException("Username already used by another account.");
				}
			}
			if (changeGoogleProfile) {
				if (pm.newQuery(
						"SELECT UNIQUE googleProfile FROM "
								+ eu.iescities.server.accountinterface.User.class
								.getName() + " WHERE googleProfile == :param1")
								.execute(googleProfile) != null) {
					throw new IllegalArgumentException("Google profile ID already used by another account.");
				}
			}
			if (changeEmail) {
				if (pm.newQuery(
						"SELECT UNIQUE lowercaseEmail FROM "
								+ eu.iescities.server.accountinterface.User.class
								.getName() + " WHERE lowercaseEmail == :param1")
								.execute(userBean.getEmail().toLowerCase()) != null) {
					throw new IllegalArgumentException("Email already used by another account.");
				}
			}
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
	}

	/**
	 * Logs a user in with username and password.
	 * 
	 * @param username
	 *            the username to login with
	 * @param password
	 *            the (clear-text) password
	 * 
	 * @return a string containing the ID of the current session
	 * @throws IllegalArgumentException
	 *             is thrown if username and password do not match with an
	 *             existing user
	 */
	public static String login(final String username, final String password)
			throws IllegalArgumentException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			final Query query = pm.newQuery(queryUserByName);
			final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
					.execute(username);
			
			if (user != null) {
				try {
					tx.begin();
					if (PasswordHash.validatePassword(password, user.getPassword())) {
						final String sessionId = user.getSessionId() != null ? user
								.getSessionId() : UUID.randomUUID().toString();
						user.startSession(sessionId);
						tx.commit();
						return sessionId;
					}
				} catch (final JDODataStoreException e) {
					intercept.waitToRetry(e);
					continue;
				} catch (final NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				} catch (final InvalidKeySpecException e) {
					throw new RuntimeException(e);
				} finally {
					if (tx.isActive()) {
						tx.rollback();
					}
					pm.close();
				}
			} 
			throw new IllegalArgumentException("Invalid username or password.");
		} 
	}

	/**
	 * Verifies a user by a Google OAuth 2.0 <tt>id_token</tt>.
	 * 
	 * An application requests the <tt>id_token</tt> e.g. via Google Play
	 * Services using at least the scope "<tt>openid profile</tt>" and submits
	 * this token to this function for verification.
	 * 
	 * @param tokenString
	 *            a user's OAuth 2.0 <tt>id_token</tt> issued by Google
	 * 
	 * @return sessionId the ID of the current session
	 * @throws IllegalArgumentException
	 *             is thrown if the <tt>id_token</tt> is invalid or there is no
	 *             user with this token's profile ID
	 * @see <a
	 *      href="https://developer.android.com/google/play-services/auth.html">Google
	 *      Play Services Authorization</a>
	 */
	public static String loginWithGoogle(final String tokenString)
			throws IllegalArgumentException {
		final String oauthProfile = verifyGoogleToken(tokenString);

		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			final Query query = pm.newQuery("SELECT UNIQUE FROM "
					+ eu.iescities.server.accountinterface.User.class.getName()
					+ " WHERE googleProfile == :param1");
			final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
					.execute(oauthProfile);
			
			if (user != null) {
				try {
					tx.begin();
					final String sessionId = user.getSessionId() != null ? user
							.getSessionId() : UUID.randomUUID().toString();
					user.startSession(sessionId);
					tx.commit();
					return sessionId;
				} catch (final JDODataStoreException e) {
					intercept.waitToRetry(e);
					continue;
				} finally {
					if (tx.isActive()) {
						tx.rollback();
					}
					pm.close();
				}
			}
			throw new IllegalArgumentException("Invalid username or password.");
		}
	}

	/**
	 * Verifies that a given <tt>id_token</tt> was issued by Google and returns
	 * the Google profile ID encoded within it.
	 * 
	 * @param tokenString
	 *            a user's OAuth 2.0 <tt>id_token</tt>
	 * 
	 * @return Google profile ID
	 * @throws IllegalArgumentException
	 */
	protected static String verifyGoogleToken(final String tokenString)
			throws IllegalArgumentException {
		try {
			// check cryptographic signature
			final GoogleIdToken token = googleVerifier.verify(tokenString);
			if (token == null) {
				throw new IllegalArgumentException("Invalid token.");
			}
			return token.getPayload().getSubject();
		} catch (final GeneralSecurityException e) {
			throw new IllegalArgumentException("Security issue: "
					+ e.getMessage());
		} catch (final IOException e) {
			throw new IllegalArgumentException("Network problem: "
					+ e.getMessage());
		}
	}
	

	/**
	 * Logs a user out identified by a session ID.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * 
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	public static void logout(final String sessionId) {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				tx.begin();
				// similar to getUserBySession(pm, sessionId):
				final Query query = pm.newQuery(queryUserByValidSessionId);
				final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
						.execute(sessionId, new Date());
				if (user == null) {
					throw new SecurityException("Invalid session id.");
				}
				user.stopSession();
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}
	
	/**
	 * Creates a password reset token for the user with the given email address.
	 * 
	 * @param email
	 *            a user's email address
	 * 
	 * @return password reset token
	 * 
	 * @throws IllegalArgumentException
	 *             if the email address is invalid or there is no user with this
	 *             address
	 */
	public static String createPasswordResetRequest(final @Nonnull String email)
			throws IllegalArgumentException {
		if (StringUtils.isEmpty(email)) {
			throw new IllegalArgumentException("Empty user email address!");
		}
		if (email.length() > 255) {
			throw new IllegalArgumentException(
					"User email address longer than 255 characters!");
		}
		final EmailValidator emailValidator = EmailValidator.getInstance();
		if (!emailValidator.isValid(email)) {
			throw new IllegalArgumentException("User email address is invalid!");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			try {
				final Query query = pm.newQuery("SELECT UNIQUE FROM "
						+ eu.iescities.server.accountinterface.User.class.getName()
						+ " WHERE lowercaseEmail == :param1 ORDER BY userId ASC");
				final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
						.execute(email);
				if (user == null) {
					throw new IllegalArgumentException(
							"There is no user with the given email address.");
				}
				final UserPasswordReset pwdResetReq = new UserPasswordReset();
				pwdResetReq.setUser(user);
				pwdResetReq.setExpires(new Date(System.currentTimeMillis()
						+ PASSWORD_RESET_TIMEOUT));
				pm.makePersistent(pwdResetReq);
				return pwdResetReq.getToken();
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				pm.close();
			}
		}
	}

	/**
	 * Gets the default lifetime of password reset requests in Milliseconds.
	 * 
	 * @return the lifetime of password reset requests in Milliseconds
	 */
	protected static long getPasswordResetTimeout() {
		return PASSWORD_RESET_TIMEOUT;
	}

	/**
	 * Changes the default lifetime of password reset requests, e.g. for tests.
	 * 
	 * @param passwordResetTimeout
	 *            the lifetime of password reset requests in Milliseconds to set
	 */
	protected static void setPasswordResetTimeout(final long passwordResetTimeout) {
		UserManagement.PASSWORD_RESET_TIMEOUT = passwordResetTimeout;
	}
	
	/**
	 * Returns the user associated with the given password reset request token.
	 * 
	 * @param token
	 *            a password reset request token
	 * 
	 * @return a user object
	 * 
	 * @throws IllegalArgumentException
	 *             if the token is invalid, expired or does not exist
	 */
	public static User getUserByPasswordResetToken(final @Nonnull String token)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getUserPasswordResetByToken(pm, token).getUser().toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Resets a user's password to the given string.
	 * 
	 * @param token
	 *            a password reset request token
	 * @param newPassword
	 *            the new password in plain-text
	 * 
	 * @throws IllegalArgumentException
	 *             if the token is invalid, expired or does not exist
	 * @throws RuntimeException
	 *             if the user's password hash cannot be set due to a service
	 *             misconfiguration
	 */
	public static void resetPasswordWithResetToken(final @Nonnull String token,
			final @Nonnull String newPassword) throws IllegalArgumentException,
			RuntimeException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final UserPasswordReset upr = getUserPasswordResetByToken(pm, token);
			final eu.iescities.server.accountinterface.User user = upr.getUser();
			user.setPassword(PasswordHash.createHash(newPassword));
			pm.deletePersistent(upr);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final InvalidKeySpecException e) {
			throw new RuntimeException(e);
		} finally {
			pm.close();
		}
	}
	
	/**
	 * Gets the {@link UserPasswordReset} object for a given password reset
	 * request token.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param token
	 *            a password reset request token
	 * 
	 * @return a user password reset object attached to the passed
	 *         {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if the token is invalid, expired or does not exist
	 */
	protected static @Nonnull UserPasswordReset getUserPasswordResetByToken(
			final PersistenceManager pm, final @Nonnull String token)
			throws IllegalArgumentException {
		if (StringUtils.isEmpty(token)) {
			throw new IllegalArgumentException(
					"Empty password reset request token!");
		}
		if (token.length() > 32) {
			throw new IllegalArgumentException(
					"Password reset request token longer than 32 characters!");
		}

		try {
			final eu.iescities.server.accountinterface.UserPasswordReset upr = pm
					.getObjectById(
							eu.iescities.server.accountinterface.UserPasswordReset.class,
							token);
			// check time validity!
			if (upr.getExpires().before(new Date())) {
				pm.deletePersistent(upr); // remove token
				throw new IllegalArgumentException(
						"Expired password reset token: " + token + ".");
			}
			return upr;
		} catch (final JDOObjectNotFoundException e) {
			throw new IllegalArgumentException("Unknown password reset token: "
					+ token + ".", e);
		}
	}

	/**
	 * Retrieves the {@link User} object from the datastore and validates the
	 * passed sessionId.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * 
	 * @return a (detached) {@link User} object
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	public static User getUserBySession(final String sessionId)
			throws SecurityException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getUserBySession(pm, sessionId).toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Retrieves a {@link User} object from the datastore based on his ID.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param userId
	 *            the user ID of the user to retrieve
	 * 
	 * @return a (detached) {@link User} object
	 * @throws IllegalArgumentException
	 *             is thrown if the user ID is unknown
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static User getUserById(final String sessionId, final int userId)
			throws IllegalArgumentException, SecurityException {
		final PersistenceManager pm = PMF.getPM();
		try {
			validateAdminSession(pm, sessionId);
			return getUserById(pm, userId).toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Finds all applications installed by the user.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param lang
	 *            language code for apps to retrieve
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return set of {@link Application}s
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	public static List<Application> getInstalledApps(final String sessionId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws SecurityException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = getUserBySession(
					pm, sessionId);
			return ApplicationManagement.getAppTranslations(pm,
					user.getInstalledApps(), lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Adds an Application to the user's list of installed ones
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param appId
	 *            the ID of the application in question
	 * 
	 * @throws SecurityException
	 *             is thrown if the sessionId is invalid
	 * @throws IllegalArgumentException
	 *             is thrown if appId cannot be resolved
	 */
	public static void addInstalledApp(final String sessionId, final int appId)
			throws SecurityException, IllegalArgumentException {		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				final eu.iescities.server.accountinterface.User user = getUserBySession(
						pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = ApplicationManagement
						.getAppById(pm, appId);
				user.getInstalledApps().add(app);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Removes an Application from the user's list of installed ones. No
	 * exception is thrown, if the given application is not present in the set
	 * of installed apps.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param appId
	 *            the ID of the application in question
	 * 
	 * @throws SecurityException
	 *             is thrown if the sessionId is invalid
	 * @throws IllegalArgumentException
	 *             is thrown if appId cannot be resolved
	 */
	public static void removeInstalledApp(final String sessionId,
			final int appId) throws IllegalArgumentException, SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = getUserBySession(
						pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = ApplicationManagement
						.getAppById(pm, appId);
				// Note: we kindly accept the situation when the user tries to un-
				// install an app which is not in the list of installed apps.
				user.getInstalledApps().remove(app);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Finds all applications developed by the user.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param lang
	 *            language code for apps to retrieve
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return set of {@link Application}s
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	public static List<Application> getDevelopedApps(final String sessionId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = getUserBySession(
					pm, sessionId);
			return ApplicationManagement.getAppTranslations(pm,
					user.getDevelopedApps(), lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Finds all datasets used by the applications the user develops.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return list of {@link Dataset}s (in ascending dataset ID order)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	@SuppressWarnings("unchecked")
	public static List<Dataset> getUsedDataSets(final String sessionId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
					throws IllegalArgumentException, SecurityException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException(
					"limit too high (> " + MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = getUserBySession(
					pm, sessionId);
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Dataset.class
							.getName()
					+ " WHERE appsUsingThisSet.contains(app)"
					+ " && app.developers.contains(:user)"
					+ " ORDER BY datasetId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return DatasetManagement.getDatasetTranslations(pm,
					(List<eu.iescities.server.accountinterface.Dataset>) query
							.execute(user),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Finds all datasets published by the user.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return list of {@link Dataset}s (in ascending dataset ID order)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	@SuppressWarnings("unchecked")
	public static List<Dataset> getPublishedDataSets(final String sessionId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang) throws IllegalArgumentException,
			SecurityException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final eu.iescities.server.accountinterface.User user = getUserBySession(
					pm, sessionId);
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Dataset.class
							.getName()
					+ " WHERE publishingUser == :user"
					+ " ORDER BY datasetId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return DatasetManagement.getDatasetTranslations(pm,
					(List<eu.iescities.server.accountinterface.Dataset>) query
							.execute(user),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Sets the given user to be an IES cities platform admin.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param userId
	 *            the user ID to set the property at
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if the user ID is unknown
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static void addIesAdmin(final String sessionId, final int userId)
			throws IllegalArgumentException, SecurityException {
		setIesAdmin(sessionId, userId, true);
	}

	/**
	 * Removes the given user from the list of IES cities platform admins.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param userId
	 *            the user ID to set the property at
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if the user ID is unknown
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static void removeIesAdmin(final String sessionId, final int userId)
			throws IllegalArgumentException, SecurityException {
		setIesAdmin(sessionId, userId, false);
	}

	/**
	 * Sets the {@link User#isIesAdmin()} property.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param userId
	 *            the user ID to set the property at
	 * @param isIesAdmin
	 *            whether the user should be an IES Cities admin or not
	 * 
	 * @throws IllegalArgumentException
	 *             is thrown if the user ID is unknown
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	protected static void setIesAdmin(final String sessionId, final int userId,
			final boolean isIesAdmin) throws IllegalArgumentException,
			SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				UserManagement.validateAdminSession(pm, sessionId);
				tx.begin();
				getUserById(pm, userId).setIesAdmin(isIesAdmin);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets a subset of the list of all users.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users sorted by the user IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	@SuppressWarnings("unchecked")
	public static List<User> getAllUsers(final String sessionId,
			final int offset, final int limit) throws IllegalArgumentException,
			SecurityException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			validateAdminSession(pm, sessionId);
			final Query query = pm.newQuery("SELECT"
					+ " userId AS userId,"
					+ " sessionExpires AS sessionExpires,"
					+ " username AS username,"
					+ " googleProfile AS googleProfile,"
					+ " firstname AS name,"
					+ " surname AS surname,"
					+ " IF (email == null) '' ELSE email AS email,"
					+ " profile AS profile,"
					// ugly workaround to get an integer literal "-1":
					+ " IF (preferredLocation == null) (userId - userId - 1) ELSE preferredLocation.councilId AS preferredCouncilId,"
					+ " IF (managedCouncil == null) (userId - userId - 1) ELSE managedCouncil.councilId AS managedCouncilId,"
					+ " isIesAdmin AS admin,"
					+ " !developedApps.isEmpty() AS developer"
					+ " INTO " + User.class.getName()
					+ " FROM " + eu.iescities.server.accountinterface.User.class.getName()
					+ " ORDER BY userId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return (List<User>) query.execute();
		} finally {
			pm.close();
		}
	}

	/**
	 * Removes the current user from the IES cities platform.
	 * 
	 * The user must provide the current password for additional authentication.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param password
	 *            the user's password
	 * 
	 * @return the number of instances that were deleted
	 * @throws IllegalArgumentException
	 *             is thrown if the password is wrong
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	public static long removeUser(final String sessionId,
			final String password) throws IllegalArgumentException,
			SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
	
			try {
				tx.begin();
				// similar to getUserBySession(pm, sessionId):
				final Query query = pm.newQuery(queryUserByValidSessionId);
				final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
						.execute(sessionId, new Date());
				if (user == null) {
					throw new SecurityException("Invalid session id.");
				}
				if (!PasswordHash.validatePassword(password, user.getPassword())) {
					// update user session lifetime
					user.updateSession();
					tx.commit();
					throw new IllegalArgumentException("Invalid password.");
				}
				pm.deletePersistent(user);
				tx.commit();
				return 1;
			} catch (final NoSuchAlgorithmException e) {
				System.err.println("ERROR: Something bad happened:");
				e.printStackTrace();
				return 0;
			} catch (final InvalidKeySpecException e) {
				System.err.println("ERROR: Something bad happened:");
				e.printStackTrace();
				return 0;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Removes a user from the DB.
	 * 
	 * NOTE: Only allowed by IES Cities admin users!
	 * 
	 * @param sessionId
	 *            an admin's session ID
	 * @param userId
	 *            the user ID of the user to remove
	 * 
	 * @return the number of instances that were deleted
	 * @throws SecurityException
	 *             is thrown if the admin session is invalid
	 */
	public static long removeUser(final String sessionId, final int userId)
			throws SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				validateAdminSession(pm, sessionId);
				tx.begin();
				final Query query = pm.newQuery("SELECT UNIQUE FROM "
						+ eu.iescities.server.accountinterface.User.class.getName()
						+ " WHERE userId == :param1");
				final long deletedUsers = query.deletePersistentAll(userId);
				tx.commit();
				return deletedUsers;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Retrieves the {@link User} object from the datastore and validates the
	 * passed sessionId. The users session expire date is updated if the user is
	 * validated successfully.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param sessionId
	 *            the user's session ID
	 * 
	 * @return the {@link User} object attached to the
	 *         {@link PersistenceManager} (always non-<tt>null</tt>)
	 * @throws SecurityException
	 *             is thrown if the session ID is invalid
	 */
	protected static eu.iescities.server.accountinterface.User getUserBySession(
			final PersistenceManager pm, final String sessionId)
			throws SecurityException {
		final Query query = pm.newQuery(queryUserByValidSessionId);
		final eu.iescities.server.accountinterface.User user = (eu.iescities.server.accountinterface.User) query
				.execute(sessionId, new Date());
		if (user == null) {
			throw new SecurityException("Invalid session id.");
		}
		// the session expire date is incremented.
		user.updateSession();
		return user;
	}

	/**
	 * Validates a user session.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @throws SecurityException
	 *             is thrown if the session ID is not valid.
	 */
	protected static void validateUserSession(final String sessionId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			getUserBySession(pm, sessionId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Validates an admin session.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param sessionId
	 *            the user's session ID
	 * 
	 * @throws SecurityException
	 *             is thrown if the session ID is not a valid admin user's
	 *             session
	 */
	protected static void validateAdminSession(final PersistenceManager pm,
			final String sessionId) throws SecurityException {
		if (!getUserBySession(pm, sessionId).isIesAdmin()) {
			throw new SecurityException("User is not an admin of the IES "
					+ "cities platform.");
		}
	}

	/**
	 * Verifies that a given ID points to a valid user.
	 *
	 * @param userId
	 *            the ID to check
	 * 
	 * @return whether the user id exists or not
	 */
	public static @Nonnull boolean verifyUserId(final int userId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT UNIQUE"
					+ " userId"
					+ " FROM " + eu.iescities.server.accountinterface.User.class.getName()
					+ " WHERE userId == :param");
			return null != query.execute(userId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Retrieves the {@link User} object from the datastore and validates the
	 * passed sessionId. The users session expire date is updated if the user is
	 * validated successfully.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param userId
	 *            the user's ID
	 * 
	 * @return the {@link User} object attached to the
	 *         {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             is thrown if the user ID is unknown
	 */
	protected static @Nonnull eu.iescities.server.accountinterface.User getUserById(
			final PersistenceManager pm, final int userId)
			throws IllegalArgumentException {
		try {
			return pm.getObjectById(
					eu.iescities.server.accountinterface.User.class, userId);
		} catch (final JDOObjectNotFoundException e) {
			throw new IllegalArgumentException("Unknown user id: " + userId
					+ ".", e);
		}
	}

	protected static final String queryUserByValidSessionId = "SELECT UNIQUE FROM "
			+ eu.iescities.server.accountinterface.User.class.getName()
			+ " WHERE sessionId == :id && sessionExpires >= :date";

	protected static final String queryUserByName = "SELECT UNIQUE FROM "
			+ eu.iescities.server.accountinterface.User.class.getName()
			+ " WHERE username == :param1";

}
