package eu.iescities.server.accountinterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.jdo.JDODataStoreException;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import org.datanucleus.util.StringUtils;

import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;

/**
 * Interface for handling applications
 * 
 * TODO: add possibility to create an {@link AppRating}.
 * 
 * @author Robert Döbbelin, doebbelin@zib.de
 * @author Nico Kruber, kruber@zib.de
 */
@PersistenceAware
public class ApplicationManagement {
	/**
	 * Maximum number of allowed elements to retrieve when querying for a
	 * possibly big list.
	 */
	protected static final int MAX_LIST_ELEMENTS_LIMIT = 1000;

	/**
	 * Registers a new application.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param appBean
	 *            the application bean object containing at least an application
	 *            name, package name and language field
	 * 
	 * @return the new application object
	 * @throws SecurityException
	 *             if the sessionId is invalid
	 * @throws IllegalArgumentException
	 *             if the name of the app is already used or a parameter is
	 *             invalid
	 */
	public static Application registerApp(final String sessionId,
			final Application appBean) throws SecurityException,
			IllegalArgumentException {
		if (appBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
	
			final ApplicationTranslation appL10n = new ApplicationTranslation(
					appBean.getLang());
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final Application result = storeApp(pm, user, appL10n, appBean)
						.toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Updates application data with new information.
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param appBean
	 *            the application bean object containing the information to
	 *            change (must contain at least the application id and a
	 *            language field)
	 * 
	 * @return a new application bean object filled with the data written to the
	 *         DB
	 * @throws SecurityException
	 *             if the user is not authorised to change the application, i.e.
	 *             not the app developer or an IES Cities admin
	 * @throws IllegalArgumentException
	 *             is thrown if the application ID is missing in the bean or on
	 *             invalid parameters
	 */
	public static Application updateApp(final String sessionId,
			@Nonnull final Application appBean) throws SecurityException,
			IllegalArgumentException {
		if (appBean.getAppId() < 0) {
			throw new IllegalArgumentException("No application ID supplied.");
		}
		if (appBean.getLang() == null) {
			throw new IllegalArgumentException("No language supplied.");
		}
		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				ApplicationTranslation appL10n;
				try {
					appL10n = getAppTranslationByLang(pm, appBean.getAppId(),
							appBean.getLang(), null);
				} catch (final IllegalArgumentException e) {
					// no translation for this language yet
					appL10n = new ApplicationTranslation(appBean.getLang());
					appL10n.setApp(getAppById(pm, appBean.getAppId()));
				}
				final Application result = storeApp(pm, user, appL10n, appBean)
						.toBean();
				tx.commit();
				return result;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Stores changes to a new or existing application by converting the
	 * {@link Application} bean properties.
	 * 
	 * Wrap around {@link Transaction#begin()} and {@link Transaction#commit()}
	 * calls to execute all statements in a transaction.
	 * 
	 * @param pm
	 *            an instance of the {@link PersistenceManager}
	 * @param user
	 *            the user trying to store the application
	 * @param appL10n
	 *            an application's translation object from the database (or a
	 *            newly created one with no properties set)
	 * @param appBean
	 *            the application bean object containing at least an application
	 *            ID and the information to change
	 * 
	 * @return a new application bean object filled with the data written to the
	 *         DB
	 * 
	 * @throws SecurityException
	 *             if the user is not authorised to change the application, i.e.
	 *             not the app developer or an IES Cities admin
	 * @throws IllegalArgumentException
	 *             is thrown if the application ID is missing in the bean or on
	 *             invalid parameters
	 */
	private static ApplicationTranslation storeApp(
			final PersistenceManager pm,
			final eu.iescities.server.accountinterface.User user,
			final ApplicationTranslation appL10n,
			final Application appBean) throws SecurityException,
			IllegalArgumentException {
		if (appL10n.getApp() != null) {
			// existing app
			if (!(user.isIesAdmin() || user.getDevelopedApps().contains(appL10n.getApp()))) {
				throw new SecurityException("User not authorized to modify "
						+ "application.");
			}
		} else {
			// new app
			appL10n.setApp(new eu.iescities.server.accountinterface.Application());
		}
		final eu.iescities.server.accountinterface.Application app = appL10n.getApp();

		// check unique app name:
		if (app.getAppId() < 0 && StringUtils.isEmpty(appBean.getName())) {
			throw new IllegalArgumentException(
					"Application name null or empty.");
		}
		if (app.getAppId() < 0
				|| (appBean.getName() != null && !appL10n.getName().equals(
						appBean.getName()))) {
			// app names must be unique except for translations of the same app!
			if (pm.newQuery(
					"SELECT UNIQUE app.appId FROM "
							+ ApplicationTranslation.class.getName()
							+ " WHERE app.appId != :appId && name == :name")
					.execute(app.getAppId(), appBean.getName()) != null) {
				throw new IllegalArgumentException(
						"Application name already used by another app.");
			}

			appL10n.setName(appBean.getName());
		}

		// check unique package name:
		if (app.getPackageName() == null
				&& StringUtils.isEmpty(appBean.getPackageName())) {
			throw new IllegalArgumentException(
					"Application package name null or empty.");
		}
		if (app.getPackageName() == null
				|| (appBean.getPackageName() != null && !app.getPackageName()
						.equals(appBean.getPackageName()))) {
			if (pm.newQuery(
					"SELECT UNIQUE appId FROM "
							+ eu.iescities.server.accountinterface.Application.class
									.getName()
							+ " WHERE packageName == :param1").execute(
					appBean.getPackageName()) != null) {
				throw new IllegalArgumentException(
						"Application package name already exists.");
			}

			app.setPackageName(appBean.getPackageName());
		}

		if (appBean.getDescription() != null) {
			appL10n.setDescription(appBean.getDescription());
		}
		if (appBean.getDownloadNumber() != null) {
			app.setDownloadNumber(appBean.getDownloadNumber());
		}
		if (appBean.getAccessNumber() != null) {
			app.setAccessNumber(appBean.getAccessNumber());
		}
		if (appBean.getUrl() != null) {
			app.setUrl(appBean.getUrl());
		}
		if (appBean.getImage() != null) {
			appL10n.setImage(appBean.getImage());
		}
		if (appBean.getRelatedCouncilId() != null) {
			final Council council = CouncilManagement.getCouncilById(pm,
					appBean.getRelatedCouncilId());
			app.setRelatedCouncil(council);
		}
		if (appBean.getGeographicalScope() != null) {
			final GeoScope geoScope = GeoScopeManagement.getScopeById(pm,
					appBean.getGeographicalScope().getId());
			app.setGeographicalScope(geoScope);
		}
		if (appBean.getGooglePlayRating() != null) {
			app.setGooglePlayRating(appBean.getGooglePlayRating());
		}
		if (appBean.getVersion() != null) {
			app.setVersion(appBean.getVersion());
		}
		if (appBean.getTermsOfService() != null) {
			appL10n.setTos(appBean.getTermsOfService());
		}
		if (appBean.getPermissions() != null) {
			app.setPermissions(appBean.getPermissions());
		}
		if (appBean.getTags() != null) {
			appL10n.getTags().clear();
			appL10n.getTags().addAll(appBean.getTags());
		}

		if (app.getAppId() < 0) {
			app.addDeveloper(user);
			// be sure that everything of the app is set,
			// adding the app to the user will make it persistent, too
			user.getDevelopedApps().add(app);
		}
		pm.makePersistent(appL10n);
		return appL10n;
	}

	/**
	 * Retrieves an {@link Application} object from the datastore based on its
	 * ID.
	 * 
	 * @param appId
	 *            the ID of the application to retrieve
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a (detached) {@link Application} object
	 * @throws IllegalArgumentException
	 *             is thrown if <tt>appId</tt> is unknown or no translation is
	 *             found
	 */
	public static Application getAppById(final int appId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			return getAppTranslationByLang(pm, appId, lang, fallbackLang)
					.toBean();
		} finally {
			pm.close();
		}
	}

	/**
	 * Retrieves an {@link Application} object from the datastore based on its
	 * package name.
	 *
	 * @param packageName
	 *            the package name of the application to retrieve
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 *
	 * @return a (detached) {@link Application} object
	 * @throws IllegalArgumentException
	 *             is thrown if <tt>packageName</tt> is unknown or no
	 *             translation is found
	 */
	public static Application getAppByPackageName(final String packageName,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery(queryApplicationByPackageName);
			final eu.iescities.server.accountinterface.Application app = (eu.iescities.server.accountinterface.Application) query
					.execute(packageName);
			if (app == null) {
				throw new IllegalArgumentException(
						"There is no application with the given packagename.");
			}
			return app.toBean(getAppTranslationByLang(pm, app.getAppId(), lang,
					fallbackLang));
		} finally {
			pm.close();
		}
	}

	/**
	 * Removes a given application from the IES cities platform.
	 * 
	 * NOTE: Only allowed by application developers or IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the user's session ID
	 * @param appId
	 *            the ID of the application in question
	 * 
	 * @return number of deleted applications
	 * @throws SecurityException
	 *             if sessionId is invalid or the user is not a developer of
	 *             the application
	 */
	public static long removeApp(final String sessionId, final int appId)
			throws SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = getAppById(
						pm, appId);
	
				if (!(user.isIesAdmin() || user.getDevelopedApps().contains(app))) {
					throw new SecurityException("User not authorized to remove "
							+ "application.");
				}
	
				pm.deletePersistent(app);
				tx.commit();
				return 1;
			} catch (final IllegalArgumentException e) {
				return 0;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets a subset of the list of all applications.
	 * 
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of applications to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of applications sorted by the application IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	public static List<Application> getAllApps(final int offset,
			final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		return findApps("", offset, limit, lang, fallbackLang);
	}

	/**
	 * Gets a subset of the list of all applications containing the given search
	 * string.
	 * 
	 * The search string is split into words and each is matched against
	 * application names, descriptions and tags (OR-connected). Note: The search
	 * is case-insensitive in all fields but the tags.
	 * 
	 * @param searchString
	 *            the string to search for (space-separated words)
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of applications to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            second language code to search in
	 * 
	 * @return a list of applications including at least one of the search words
	 *         and sorted by the application IDs (ascending)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Application> findApps(
			@Nonnull final String searchString, final int offset,
			final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			List<ApplicationTranslation> apps;
			final Query query = pm.newQuery(ApplicationTranslation.class);
			query.setOrdering("app.appId ASC, lang ASC"); // should be default, just in case
			query.setRange(offset, offset + limit);
			final StringBuilder filter = new StringBuilder();
			filter.append("(lang == '");
			filter.append(lang);
			if (fallbackLang != null) {
				filter.append("' || lang == '");
				filter.append(fallbackLang);
			}
			filter.append("') && (");
			if (searchString.length() > 0) {
				final String[] searchWords = StringUtils.split(
						StringUtils.removeSpecialTagsFromString(searchString),
						" ");
				// note: define the words as real parameters to prevent SQL
				// injections (datanucleus must filter these)
				final HashMap<String, String> parameters = new HashMap<String, String>(
						searchWords.length);
				final StringBuilder parameterDecl = new StringBuilder();
				for (int i = 0; i < searchWords.length; ++i) {
					final String pword = "word" + i;
					final String pexpr = "wexpr" + i;
					parameters.put(pexpr, ".*" + searchWords[i].toLowerCase()
							+ ".*");
					parameters.put(pword, searchWords[i]);
					parameterDecl.append("String ");
					parameterDecl.append(pexpr);
					parameterDecl.append(", String ");
					parameterDecl.append(pword);
					parameterDecl.append(", ");
					filter.append("name.toLowerCase().matches(");
					filter.append(pexpr);
					filter.append(") || description.toLowerCase().matches(");
					filter.append(pexpr);
					filter.append(") || tags.contains(");
					filter.append(pword);
					filter.append(") || ");
				}
				assert (parameterDecl.length() > 2);
				query.declareParameters(parameterDecl.substring(0,
						parameterDecl.length() - 2));
				assert (filter.length() > 4);
				query.setFilter(filter.substring(0, filter.length() - 4) + ")");
				apps = (List<ApplicationTranslation>) query
						.executeWithMap(parameters);
			} else {
				query.setFilter(filter.substring(0, filter.length() - 5));
				apps = (List<ApplicationTranslation>) query.execute();
			}
			return filterDuplicateAppTranslations(apps, lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Filter duplicate translations of an app list in favour of the (first)
	 * <tt>lang</tt>.
	 * 
	 * @param apps
	 *            a list of apps with possible duplicates of the two
	 *            available languages neighbouring each other
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 *
	 * @return a list of apps with duplicate translations removed
	 */
	protected static List<Application> filterDuplicateAppTranslations(
			final Collection<ApplicationTranslation> apps, final Language lang,
			final Language fallbackLang) {
		final ArrayList<Application> result = new ArrayList<Application>(
				apps.size());
		// filter duplicate applications with lang and fallbackLang
		Application previousApp = null;
		for (final ApplicationTranslation appL10n : apps) {
			if (previousApp != null
					&& previousApp.getAppId() == appL10n.getApp()
							.getAppId()) {
				if (previousApp.getLang() == fallbackLang) {
					assert appL10n.getLang() == lang;
					previousApp = appL10n.toBean();
					result.set(result.size() - 1, previousApp);
				}
			} else {
				previousApp = appL10n.toBean();
				result.add(previousApp);
			}
		}
		return result;
	}

	/**
	 * Adds a developer to an application.
	 * 
	 * NOTE: Only allowed by app developers and IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the developer's or admin's session ID
	 * @param appId
	 *            the application's ID
	 * @param developerID
	 *            the ID of the user to add as a developer
	 * 
	 * @throws IllegalArgumentException
	 *             if the appId cannot be resolved
	 * @throws SecurityException
	 *             is thrown if the developer or admin session is invalid
	 */
	public static void addDeveloper(final String sessionId, final int appId,
			final int developerID) throws IllegalArgumentException,
			SecurityException {		
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = getAppById(
						pm, appId);
				if (!(user.isIesAdmin() || user.getDevelopedApps().contains(app))) {
					throw new SecurityException(
							"User not authorized to add developers.");
				}
			
				final eu.iescities.server.accountinterface.User developer = UserManagement
						.getUserById(pm, developerID);
				developer.getDevelopedApps().add(app);
				app.getDevelopers().add(developer);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets a list of all application developers.
	 * 
	 * @param appId
	 *            the application's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of users to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of users developing the given application (in ascending
	 *         user ID order), filled with only the user id, name, surname,
	 *         isAdmin and isDeveloper
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<User> getAllDevelopersOfApp(final int appId,
			final int offset, final int limit) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT"
					+ " userId AS userId,"
					+ " firstname AS name,"
					+ " surname AS surname,"
					+ " isIesAdmin AS admin,"
					+ " profile AS profile,"
					+ " true AS developer"
					+ " INTO " + User.class.getName()
					+ " FROM " + eu.iescities.server.accountinterface.User.class.getName()
					+ " WHERE developedApps.contains(app)"
					+ " && app.appId == :param"
					+ " ORDER BY userId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return (List<User>) query.execute(appId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Adds a dataset to an application.
	 * 
	 * NOTE: Only allowed by app developers and IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the developer's or admin's session ID
	 * @param appId
	 *            the application's ID
	 * @param datasetId
	 *            the dataset's ID
	 * 
	 * @throws IllegalArgumentException
	 *             if the appId or datasetId cannot be resolved
	 * @throws SecurityException
	 *             is thrown if the developer or admin session is invalid
	 */
	public static void addDataset(final String sessionId, final int appId,
			final int datasetId) throws IllegalArgumentException,
			SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = getAppById(
						pm, appId);
				if (!(user.isIesAdmin() || user.getDevelopedApps().contains(app))) {
					throw new SecurityException(
							"User not authorized to add datasets.");
				}
				
				final eu.iescities.server.accountinterface.Dataset dataset = DatasetManagement
						.getDatasetById(pm, datasetId);
				app.addDataset(dataset);
				dataset.addAppUsingThisSet(app);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Removes a dataset from an application.
	 * 
	 * NOTE: Only allowed by app developers and IES Cities admin users!
	 * 
	 * @param sessionId
	 *            the developer's or admin's session ID
	 * @param appId
	 *            the application's ID
	 * @param datasetId
	 *            the dataset's ID
	 * 
	 * @throws IllegalArgumentException
	 *             if the appId or datasetId cannot be resolved
	 * @throws SecurityException
	 *             is thrown if the developer or admin session is invalid
	 */
	public static void removeDataset(final String sessionId, final int appId,
			final int datasetId) throws IllegalArgumentException,
			SecurityException {
		final RetryInterceptor intercept = new RetryInterceptor();
		while (true) {
			final PersistenceManager pm = PMF.getPM();
			final Transaction tx = pm.currentTransaction();
			try {
				final eu.iescities.server.accountinterface.User user = UserManagement
						.getUserBySession(pm, sessionId);
				tx.begin();
				final eu.iescities.server.accountinterface.Application app = getAppById(
						pm, appId);
				if (!(user.isIesAdmin() || user.getDevelopedApps().contains(app))) {
					throw new SecurityException(
							"User not authorized to remove datasets.");
				}
				
				final eu.iescities.server.accountinterface.Dataset dataset = DatasetManagement
						.getDatasetById(pm, datasetId);
				app.removeDataset(dataset);
				dataset.removeAppUsingThisSet(app);
				tx.commit();
				return;
			} catch (final JDODataStoreException e) {
				intercept.waitToRetry(e);
			} finally {
				if (tx.isActive()) {
					tx.rollback();
				}
				pm.close();
			}
		}
	}

	/**
	 * Gets all datasets an application uses.
	 * 
	 * @param appId
	 *            the application's ID
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of datasets to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * @param lang
	 *            language code for localised fields
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return a list of datasets used by the application (in ascending dataset
	 *         ID order)
	 * @throws IllegalArgumentException
	 *             if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<Dataset> getAllDatasetsOfApp(final int appId,
			final int offset, final int limit, @Nonnull final Language lang,
			@Nullable final Language fallbackLang)
					throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT FROM "
					+ eu.iescities.server.accountinterface.Dataset.class
							.getName()
					+ " WHERE appsUsingThisSet.contains(app)"
					+ " && app.appId == :param"
					+ " ORDER BY datasetId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return DatasetManagement.getDatasetTranslations(pm,
					(List<eu.iescities.server.accountinterface.Dataset>) query
							.execute(appId),
					lang, fallbackLang);
		} finally {
			pm.close();
		}
	}

	/**
	 * Gets a subset of the list of all ratings of an application.
	 * 
	 * @param appId
	 *            the ID of the application to retrieve ratings from
	 * @param offset
	 *            offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit
	 *            maximum number of ratings to retrieve (max
	 *            {@link #MAX_LIST_ELEMENTS_LIMIT})
	 * 
	 * @return a list of ratings sorted (ascending) by their IDs (empty if the
	 *         <tt>appId</tt> is unknown)
	 * @throws IllegalArgumentException
	 *             is thrown if the limit is too high
	 */
	@SuppressWarnings("unchecked")
	public static List<AppRating> getAllRatingsOfApp(final int appId,
			final int offset, final int limit) throws IllegalArgumentException {
		if (limit > MAX_LIST_ELEMENTS_LIMIT) {
			throw new IllegalArgumentException("limit too high (> "
					+ MAX_LIST_ELEMENTS_LIMIT + ")");
		}

		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT"
					+ " ratingId AS ratingId,"
					+ " date AS date,"
					+ " rating AS rating,"
					+ " title AS title,"
					+ " comment AS comment,"
					+ " lang AS lang,"
					+ " userName AS userName,"
					+ " app.appId AS appId"
					+ " INTO " + AppRating.class.getName()
					+ " FROM " + eu.iescities.server.accountinterface.AppRating.class.getName()
					+ " WHERE app.appId == :appid"
					+ " ORDER BY ratingId ASC");
			// set range this way (otherwise it may sometimes be ignored)
			query.setRange(offset, offset + limit);
			return (List<AppRating>) query.execute(appId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Verifies that a given ID points to a valid application.
	 *
	 * @param appId
	 *            the ID to check
	 * 
	 * @return whether the app id exists or not
	 */
	public static @Nonnull boolean verifyAppId(final int appId) {
		final PersistenceManager pm = PMF.getPM();
		try {
			final Query query = pm.newQuery("SELECT UNIQUE"
					+ " appId"
					+ " FROM " + eu.iescities.server.accountinterface.Application.class.getName()
					+ " WHERE appId == :param");
			return null != query.execute(appId);
		} finally {
			pm.close();
		}
	}

	/**
	 * Finds an application by it's id.
	 * 
	 * @param pm
	 *            an instance of the PersistenceManager
	 * @param appId
	 *            the ID of the application in question
	 * 
	 * @return an application attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             if the appId cannot be resolved
	 */
	protected static @Nonnull eu.iescities.server.accountinterface.Application getAppById(
			@Nonnull final PersistenceManager pm, final int appId)
			throws IllegalArgumentException {
		try {
			return pm.getObjectById(
					eu.iescities.server.accountinterface.Application.class,
					"" + appId);
		} catch (final JDOObjectNotFoundException e) {
			throw new IllegalArgumentException("Unknown application id: "
					+ appId + ".", e);
		}
	}

	/**
	 * Finds a translation for an application by it's id and language code.
	 * 
	 * @param pm
	 *            an instance of the PersistenceManager
	 * @param appId
	 *            the ID of the application in question
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 * 
	 * @return an application attached to the passed {@link PersistenceManager}
	 * @throws IllegalArgumentException
	 *             if the appId cannot be resolved
	 */
	protected static @Nonnull ApplicationTranslation getAppTranslationByLang(
			@Nonnull final PersistenceManager pm, final int appId,
			@Nonnull final Language lang, @Nullable final Language fallbackLang)
			throws IllegalArgumentException {
		final Query query = pm.newQuery("SELECT UNIQUE FROM "
				+ ApplicationTranslation.class.getName()
				+ " WHERE app.appId == :appId && lang == :lang");
		ApplicationTranslation appL10n = (ApplicationTranslation) query
				.execute(appId, lang);
		if (appL10n == null && fallbackLang != null) {
			appL10n = (ApplicationTranslation) query.execute(appId,
					fallbackLang);
		}
		if (appL10n == null) {
			throw new IllegalArgumentException(
					"No application translation for appId " + appId
							+ " into language " + lang + ".");
		}
		return appL10n;
	}

	/**
	 * Finds translations for application objects with a fallback of using empty
	 * fields.
	 * 
	 * @param pm
	 *            an instance of the PersistenceManager
	 * @param apps
	 *            the applications
	 * @param lang
	 *            language code
	 * @param fallbackLang
	 *            fallback language code if a translation for <tt>lang</tt> does
	 *            not exist (may be <tt>null</tt>)
	 *
	 * @return an object bean with either the appropriate translation or empty
	 *         translation fields
	 */
	protected static List<Application> getAppTranslations(
			final PersistenceManager pm,
			final Collection<eu.iescities.server.accountinterface.Application> apps,
			final Language lang, final Language fallbackLang) {
		final ArrayList<Application> result = new ArrayList<Application>(
				apps.size());
		for (final eu.iescities.server.accountinterface.Application app : apps) {
			Application bean;
			try {
				final ApplicationTranslation appL10n = getAppTranslationByLang(pm,
						app.getAppId(), lang, fallbackLang);
				bean = app.toBean(appL10n);
			} catch (final IllegalArgumentException e) {
				// no translation available: use empty fields:
				bean = app.toBean(new ApplicationTranslation());
			}
			result.add(bean);
		}
		return result;
	}

	protected static final String queryApplicationByPackageName = "SELECT UNIQUE FROM "
			+ eu.iescities.server.accountinterface.Application.class.getName()
			+ " WHERE packageName == :param";
}
