First implementation of the service for the collection of social data from Facebook and Twitter.

Configuration
=============

To be able to query Facebook and Twitter, some parameters must be set in
src\main\resources\iescities.properties

For Facebook:
fb.clientId
fb.clientSecret
are obtained from a Facebook app
https://developers.facebook.com/apps

For Twitter
tw.consumerKey
tw.consumerSecret
tw.accessToken
tw.accessTokenSecret
are obtained in a similar way from a Twitter application
https://dev.twitter.com/apps

In iescities.properties other two parameters are found that are used to define the default value for some query parameters
default.facebookSources
default.maxPostsNumber
default.radius
For their meaning, see "Usage"

Usage
=====

The web application exposes two GET method to get information about share/like for a URL and three GET methods to search for social data.

In "usage" we have "likes" (Facebook "likes", Twitter "favorite"), "shares" (Facebook "shares", Twitter "retweets") 
and "replies" (Facebook reply messages, no Twitter equivalent since its API doens't allow to get replies to a tweet)

- /social/usage
That requires one parameter
- url: the url to get information about (note: twitter doesn't seem to support '?' in URLs, so remove anything after '?' included if possible or get no results)

Examples

http://localhost:8080/IESCities/api/social/usage?url=http://www.repubblica.it/tecnologia/2015/03/11/news/itunes_store_e_altri_servizi_apple_giu_-109279444/?ref=HREC1-7

{
    "Facebook": {
        "likes": 5,
        "shares": 386,
        "replies": 0
    },
    "Twitter": {
        "likes": 0,
        "shares": 0
    }
}

Removing '?'
http://localhost:8080/IESCities/api/social/usage?url=http://www.repubblica.it/tecnologia/2015/03/11/news/itunes_store_e_altri_servizi_apple_giu_-109279444/
{
    "Facebook": {
        "likes": 5,
        "shares": 386,
        "replies": 0
    },
    "Twitter": {
        "likes": 3,
        "shares": 40
    }
}

- social/friendsUsage
That requires the following parameters
- url: the url to get information about
- facebookToken: a Facebook user token
- twitterAccessToken: a Twitter user access token
- twitterAccessTokenSecret: a Twitter user access token

http://localhost:8080/IESCities/api/social/friendsUsage?url=http://www.repubblica.it/tecnologia/2015/03/11/news/itunes_store_e_altri_servizi_apple_giu_-109279444?ref=HREC1-7&facebookToken=...&twitterAccessToken=...&twitterAccessTokenSecret=...
{
    "Facebook": {
        "likes": 2
    },
    "Twitter": {
        "shares": 0
    }
}

- /social/posts
- /social/media
- /social/events
The former two methods returns objects of type Post, while the latter one returns objects of type Event.

All of them require request parameters for the query (in JSON format):
- keyword: one or more words to search for.
- coordinates: a pair latitude/longitude to restrict the search to an area.
- radius: the radius the searches are limited to (in meters)
- facebookSources: the number of Facebook Places to get posts from. Note that some Place could have no posts.
- maxPostsNumber: the maximum number of posts per source (Facebook/Twitter). Note that for Facebook, each Place will have associated at most maxPostsNumber / facebookSources

So for example we could have a request for posts
/social/posts?keyword=Trento&coordinates=46.07466,11.11914&radius=10&facebookSources=5&maxPostsNumber=10

with the corresponding response (see the class eu.iescities.server.datawrapper.model.Post and eu.iescities.server.datawrapper.model.Event for the various fields meanings):

{
    "Facebook": [
        {
            "parent": "Trento",
            "message": "Amici, siete mai stati ai MERCATINI DI NATALE a Trento ?\r\n\r\n\r\nLa nostra proposta Mercatini di natale: http://bit.ly/offerta-mercatini",
            "source": "Facebook",
            "media": [
                "http://www.facebook.com/photo.php?fbid=562218923866151&set=a.309900649097981.74018.244690192285694&type=1"
            ],
            "comments": [],
            "createdAt": 1384935942000,
            "authorId": 244690192285694
        },
        {
            "parent": "Cinema Astra Multisala e Osteria",
            "message": "-------------  IN ANTEPRIMA  --------------\n\" A PROPOSITO DI DAVIS \" di Ethan Coen, Joel Coen\nMartedì 4 Febbraio 2014 - Ore: 18.45 e 21.00\nhttp://trovacinema.repubblica.it/film/a-proposito-di-davis/429436\nhttps://www.youtube.com/watch?v=aEfTIekU49M",
            "source": "Facebook",
            "media": [
                "http://www.facebook.com/photo.php?fbid=587121598036698&set=a.179834138765448.46478.159533037462225&type=1&relevant_count=1"
            ],
            "comments": [
                "Renato Meo <3 yes yes yes"
            ],
            "createdAt": 1391201502000,
            "authorId": 159533037462225
        },
        {
            "parent": "Festival dell'Arte di Trento",
            "message": "per tutti gli artisti... sabato a bolzano, in palio 600 euro!",
            "source": "Facebook",
            "media": [
                "http://www.facebook.com/events/428741273903581/"
            ],
            "comments": [],
            "createdAt": 1379002310000,
            "authorId": 100000366002396
        },
        ...
    ],
    "Twitter": [
        {
            "parent": null,
            "message": "#Trento #today Ancora chiusa la SS42 del Tonale - Rimane chiusa la straav del passo Tonale dopo Vermiglio, a caus... http://t.co/RPH7TnDWZW",
            "source": "Twitter",
            "media": [],
            "comments": null,
            "createdAt": 1391614216000,
            "authorId": 452061176
        },
        {
            "parent": null,
            "message": "RT @trentofestival: Il Trento Film Festival cerca giovani volontari per la 62° edizione! Se hai fra i 18 e i 30 anni e ti piacerebbe... htt…",
            "source": "Twitter",
            "media": [],
            "comments": null,
            "createdAt": 1391612526000,
            "authorId": 1353799848
        },
        {
            "parent": null,
            "message": "Firmato l'accordo fra Deutsche Bank e Confindustria di Trento per agevolare le imprese ad... http://t.co/RYuhD1Psea",
            "source": "Twitter",
            "media": [],
            "comments": null,
            "createdAt": 1391607456000,
            "authorId": 358937939
        },
        ...
    ]
}

Testing
=======

The JUnit test shows the usage of the class used to make the searches.
Before running the test, the variables regarding Facebook and Twitter security must be set (see "Configuration").
