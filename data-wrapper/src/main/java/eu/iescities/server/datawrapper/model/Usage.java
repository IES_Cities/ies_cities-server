package eu.iescities.server.datawrapper.model;

public class Usage {

	/**
	 * Facebook likes or Twitter favorites
	 */
	private Integer likes = null;

	/**
	 * Facebook shares or Twitter retweet
	 */
	private Integer shares = null;

	/**
	 * Facebook replies
	 */
	private Integer replies = null;

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public Integer getShares() {
		return shares;
	}

	public void setShares(Integer shares) {
		this.shares = shares;
	}

	public Integer getReplies() {
		return replies;
	}

	public void setReplies(Integer replies) {
		this.replies = replies;
	}
	
}
