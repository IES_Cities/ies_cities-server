package eu.iescities.server.datawrapper.search;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import eu.iescities.server.datawrapper.model.Event;
import eu.iescities.server.datawrapper.model.Place;
import eu.iescities.server.datawrapper.model.Post;
import eu.iescities.server.datawrapper.model.SearchParameters;
import eu.iescities.server.datawrapper.model.Usage;

/**
 * Class implementing searches on Facebook and Twitter.
 */
public class SocialSearcher {
	
	private FacebookSearcher facebookSearcher;
	public TwitterSearcher twitterSearcher;
	

	/**
	 * Constructor. For Facebook queries, two parameters are needed. For Twitter,
	 * four.
	 * 
	 * @param fbClientId
	 * @param fbClientSecret
	 * @param twConsumerKey
	 * @param twConsumerSecret
	 * @param twAccessToken
	 * @param twAccessTokenSecret
	 */
	public SocialSearcher(String fbClientId, String fbClientSecret, String twConsumerKey, String twConsumerSecret, String twAccessToken, String twAccessTokenSecret) {
		super();
		facebookSearcher = new FacebookSearcher(fbClientId, fbClientSecret);
		
		twitterSearcher = new TwitterSearcher(twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		
	}

	/**
	 * Search for posts on Facebook and Twitter
	 * 
	 * @param sp
	 *          The search parameters
	 * @return A map whose keys ("Facebook" and "Twitter") contains the retrieved
	 *         posts.
	 * @throws Exception
	 */
	public Map<String, List<Post>> posts(SearchParameters sp) throws Exception {
		Map<String, List<Post>> result = new TreeMap<String, List<Post>>();
		result.put(Post.FACEBOOK, facebookSearcher.search(sp));
		result.put(Post.TWITTER, twitterSearcher.search(sp, false));
		return result;
	}

	/**
	 * Search for posts with media content on Facebook and Twitter
	 * 
	 * @param sp
	 *          The search parameters
	 * @return A map whose keys ("Facebook" and "Twitter") contains the retrieved
	 *         posts.
	 * @throws Exception
	 */
	public Map<String, List<Post>> media(SearchParameters sp) throws Exception {
		Map<String, List<Post>> result = new TreeMap<String, List<Post>>();
		result.put(Post.FACEBOOK, facebookSearcher.searchPhotos(sp));
		result.put(Post.TWITTER, twitterSearcher.search(sp, true));
		return result;
	}

	

	/**
	 * Return informations (likes/retweet...) from Facebook and Twitter about a URL 
	 * @param address a URL
	 * @return social information about the URL from Facebook and Twitter
	 * @throws Exception
	 */
	public Map<String, Usage> usage(String address) throws Exception{
		Map<String, Usage> result = new TreeMap<String, Usage>();
		result.put(Post.FACEBOOK, facebookSearcher.likes(address));
		result.put(Post.TWITTER, twitterSearcher.retweets(address));
		return result;
	}
	
	/**
	 * Search for events on Facebook
	 * @param sp  The search parameters
	 * @return a list of Events
	 * @throws Exception
	 */
	public List<Event> events(SearchParameters sp) throws Exception {
		return facebookSearcher.events(sp);
	}	
	
	/**
	 * Search for events on Facebook
	 * @param id  the Facebook Id for a Place
	 * @return a list of Events
	 * @throws Exception
	 */
	public List<Event> facebookEvents(String id) throws Exception {
		return facebookSearcher.events(id);
	}		
	
	
	/**
	 * Search for places on Facebook
	 * @return a list of Events
	 * @throws Exception
	 */
	public List<Place> facebookPlaces(SearchParameters sp) throws Exception {
		return facebookSearcher.places(sp);
	}		
	
	
	/**
	 * Return informations (likes/retweet...) from Facebook and Twitter made by friends of a user about a URL
	 * @param address a URL
	 * @param facebookToken a user Facebook token
	 * @param twitterAccessToken a user Twitter access token
	 * @param twitterAccessTokenSecret a user Twitter access token secret
	 * @return
	 * @throws Exception
	 */
	public Map<String, Usage> friendsShare(String address, String facebookToken, String twitterAccessToken, String twitterAccessTokenSecret) throws Exception	{
		Map<String, Usage> result = new TreeMap<String, Usage>();
		if (facebookToken != null) {
			result.put(Post.FACEBOOK,facebookSearcher.friendsLikes(address, facebookToken));
		}
		if (twitterAccessToken != null && twitterAccessTokenSecret != null) {
			result.put(Post.TWITTER,twitterSearcher.friendsRetweets(address, twitterAccessToken, twitterAccessTokenSecret));
		}
		return result;
	}
	
	
	

	
	
	

	
	
	
}
