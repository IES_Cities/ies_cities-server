package eu.iescities.server.datawrapper;

import java.io.IOException;
import java.util.Properties;

import org.glassfish.jersey.server.ResourceConfig;

import eu.iescities.server.datawrapper.config.Config;

public class TestApplication extends ResourceConfig {

	public TestApplication() {
		packages("eu.iescities.server.datawrapper");

		try {
			Properties props = new Properties();
			props.load(getClass().getResourceAsStream("/iescities.properties"));
	
			Config config = Config.getInstance(); 
			config.setProperties(props);
		}
		catch (IOException e) {
			System.out.println("Could not load properties file from resource folder");
		}
	}
}