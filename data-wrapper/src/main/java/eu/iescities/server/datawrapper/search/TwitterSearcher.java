package eu.iescities.server.datawrapper.search;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import twitter4j.GeoLocation;
import twitter4j.MediaEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.CharStreams;
import com.google.common.primitives.Longs;

import eu.iescities.server.datawrapper.model.Post;
import eu.iescities.server.datawrapper.model.SearchParameters;
import eu.iescities.server.datawrapper.model.Usage;

public class TwitterSearcher {

	private static final int MAX_RADIUS = 50;

	private Twitter twitter;

	private String twConsumerKey;
	private String twConsumerSecret;

	public TwitterSearcher(String twConsumerKey, String twConsumerSecret, String twAccessToken, String twAccessTokenSecret) {
		super();
		this.twConsumerKey = twConsumerKey;
		this.twConsumerSecret = twConsumerSecret;
		twitter = buildTwitter(twAccessToken, twAccessTokenSecret);
	}

	
	private Twitter buildTwitter(String accessToken, String accessTokenSecret) {
		twitter4j.conf.ConfigurationBuilder tcb = new twitter4j.conf.ConfigurationBuilder();
		tcb.setDebugEnabled(true).setOAuthConsumerKey(twConsumerKey).setOAuthConsumerSecret(twConsumerSecret).setOAuthAccessToken(accessToken).setOAuthAccessTokenSecret(accessTokenSecret);
		TwitterFactory tf = new TwitterFactory(tcb.build());
		return tf.getInstance();
	}

	/**
	 * Search on Twitter for posts related to the query
	 * 
	 * @param sp
	 *            The search parameters
	 * @param mediaOnly
	 *            Return only posts containing some kind of media content
	 * @return A list of Posts
	 * @throws Exception
	 */
	protected List<Post> search(SearchParameters sp, boolean mediaOnly) throws Exception {
		Set<Post> result = Sets.newHashSet();

		Set<String> ts = new HashSet<String>();
		List<Status> tweets = extendedQueries(sp, twitter);

		for (Status status : tweets) {
			String text = status.getText();
			if (ts.contains(text)) {
				continue;
			}

			List<String> media = Lists.newArrayList();

			MediaEntity mes[] = status.getMediaEntities();
			for (MediaEntity me : mes) {
				String url = me.getURL();
				media.add(url);
			}

			URLEntity ues[] = status.getURLEntities();
			for (URLEntity ue : ues) {
				String url = ue.getURL();
				String extUrl = ue.getExpandedURL();
				// add images from Instagram
				if (extUrl.contains("instagram")) {
					media.add(url);
				}
			}

			if (mediaOnly && media.isEmpty()) {
				continue;
			}

			Post post = new Post();
			post.setSource(Post.TWITTER);
			post.setMessage(text);
			post.setMedia(media);
			post.setAuthorId(status.getUser().getId());
			post.setCreatedAt(status.getCreatedAt().getTime());

			result.add(post);

		}

		return new ArrayList<Post>(result);
	}

	/**
	 * Execute from one to four queries on Twitter based on the original keyword(s):
	 * <ul>
	 * <li>The original keyword(s)
	 * <li>The original keyword(s) transformed into hashtags
	 * <li>The original keyword(s) within a radius if coordinates are present
	 * <li>The original keyword(s) transformed into hashtags within a radius if coordinates are present
	 * </ul>
	 * 
	 * @param sp
	 *            The search parameters
	 * @param twitter
	 * @return a list of Twitter posts
	 * @throws TwitterException
	 */
	private List<Status> extendedQueries(SearchParameters sp, Twitter twitter) throws TwitterException {
		List<Status> result = Lists.newArrayList();

		Query query;
		QueryResult tweets;

		int posts = sp.getMaxPostsNumber();
		List<String> keywords = Lists.newArrayList();
		keywords.add(sp.getKeyword());
		if (!sp.getKeyword().startsWith("#") && !sp.getKeyword().isEmpty()) {
			keywords.add("#" + sp.getKeyword().replace(" ", " #"));
			posts /= 2;
		}
		if (sp.getCoordinates() != null) {
			posts /= 2;
		}

		for (String keyword : keywords) {
			if (keyword.isEmpty()) {
				query = new Query();
			} else {
				query = new Query(keyword);
			}
			query.setResultType(Query.RECENT);
			query.setCount(posts);

			if (!keyword.isEmpty()) {
				tweets = twitter.search(query);
				result.addAll(tweets.getTweets());
			}

			if (sp.getCoordinates() != null) {
				query.setGeoCode(new GeoLocation(sp.getCoordinates()[0], sp.getCoordinates()[1]), Math.min(MAX_RADIUS, sp.getRadius()), Query.KILOMETERS);
			}
			tweets = twitter.search(query);
			result.addAll(tweets.getTweets());

		}

		return result;
	}

	/**
	 * Return retweets statistics about a URL
	 * @param address a URL
	 * @return  information about number of retweets of the URL
	 * @throws Exception
	 */
	public Usage retweets(String address) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		URL url = new URL("http://cdn.syndication.twitter.com/widgets/tweetbutton/count.json?url=" + URLEncoder.encode(address, "UTF-8"));

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);

		int count = rootNode.path("count").intValue();
		
		Usage usage = new Usage();
		usage.setShares(count);

		return usage;
	}	
	

	// TODO one week
	/**
	 * Return retweets statistics made by friends about a URL
	 * @param address a url
	 * @param token a twitter access token
	 * @param secret the twitter access token secret
	 * @return information about number of retweets of the URL
	 * @throws Exception
	 */
	public Usage friendsRetweets(String address, String token, String secret) throws Exception {
		List<Long> result = Lists.newArrayList();

		List<Long> friends = friends(token, secret);
		List<Status> tw = Lists.newArrayList();

		Query query = new Query(address);
		QueryResult tweets;
		tweets = twitter.search(query);
		tw.addAll(tweets.getTweets());
		while (tweets.hasNext()) {
			query = tweets.nextQuery();
			query.setCount(1000);
			tweets = twitter.search(query);
			tw.addAll(tweets.getTweets());
		}

		for (Status tweet : tw) {
			if (friends.contains(tweet.getUser().getId())) {
				result.add(tweet.getUser().getId());
			}
		}

		Usage usage = new Usage();
		usage.setShares(result.size());

		return usage;
	}

	/**
	 * Return the ids of a user friends
	 * @param token a twitter access token
	 * @param secret the twitter access token secret
	 * @return a list of user ids
	 * @throws Exception
	 */
	private List<Long> friends(String token, String secret) throws Exception {
		Twitter userTwitter = buildTwitter(token, secret);

		long[] friends = userTwitter.getFollowersIDs(-1).getIDs();

		return Longs.asList(friends);
	}

}
