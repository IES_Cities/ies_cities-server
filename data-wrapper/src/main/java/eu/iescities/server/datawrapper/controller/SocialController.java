package eu.iescities.server.datawrapper.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import eu.iescities.server.datawrapper.config.Config;
import eu.iescities.server.datawrapper.model.Post;
import eu.iescities.server.datawrapper.model.SearchParameters;
import eu.iescities.server.datawrapper.search.SocialSearcher;

/**
 * Handler of remote requests.
 */
@Path("/social")
@Api(value = "/social", description = "Operations about social data")
public class SocialController {

	private int facebookSources;
	private int maxPostsNumber;
	private int radius;	
	private String fbClientId;
	private String fbClientSecret;	
	private String twConsumerKey;	
	private String twConsumerSecret;	
	private String twAccessToken;	
	private String twAccessTokenSecret;		
	
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public SocialController() throws IOException {
		super();
		
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		
		Config config = Config.getInstance();
		facebookSources = config.getFacebookSources();
		maxPostsNumber = config.getMaxPostsNumber();
		radius = config.getRadius();
		fbClientId = config.getFbClientId();
		fbClientSecret = config.getFbClientSecret();
		twConsumerKey = config.getTwConsumerKey();
		twConsumerSecret = config.getTwConsumerSecret();
		twAccessToken = config.getTwAccessToken();
		twAccessTokenSecret = config.getTwAccessTokenSecret();
	}

	@GET @Path("/ping")
	@ApiOperation(value = "Test presence of the Social Wrapper API")
	public String ping() {
		return "pong";
	}
	
	/**
	 * Search for posts
	 * @param request
	 * @param response
	 * @param sp The search parameters
	 * @return A map whose keys ("Facebook" and "Twitter") contains the retrieved posts.
	 * @throws Exception 
	 */
	@GET @Path("/posts")
	  @ApiOperation(value = "Search posts in the social networks about the specified topics"
//	  , notes = 
//		  "{keyword}: search keyword\n" + 
//		  "{coordinates}: reference point (latitude,longitude)\n" + 
//		  "{maxPostsNumber}: max number of posts to return\n"
//		  + "{facebookSources}: max number of Facebook pages to extract info from\n"
//		  + "{radius}: radius of the area to consider (in kilometers)"
	)
	  @Produces(MediaType.APPLICATION_JSON)
	public String posts(@QueryParam("keyword") String keyword, 
			@QueryParam("coordinates") String coords,
			@QueryParam("facebookSources") int facebookSources,
			@QueryParam("maxPostsNumber") int maxPostsNumber,
			@QueryParam("radius") int radius,
			@QueryParam("categories") String categories) throws Exception {
		Map<String, List<Post>> result = new TreeMap<String, List<Post>>();
		SearchParameters sp = new SearchParameters();
		sp.setKeyword(keyword);
		if (coords != null) {
			String[] coordsStr = coords.split(",");
			sp.setCoordinates(new double[]{Double.parseDouble(coordsStr[0]),Double.parseDouble(coordsStr[1])});
		}
		if (facebookSources == 0) {
			sp.setFacebookSources(this.facebookSources);
		} else {
			sp.setFacebookSources(facebookSources);
		}
		if (maxPostsNumber == 0) {
			sp.setMaxPostsNumber(this.maxPostsNumber);
		} else {
			sp.setMaxPostsNumber(maxPostsNumber);
		}
		if (radius == 0) {
			sp.setRadius(this.radius);
		} else {
			sp.setRadius(radius);
		}
		if (categories != null) {
			String[] cats = categories.split(",");
			sp.setCategories(Arrays.asList(cats));
		} else {
			sp.setCategories(new ArrayList<String>());
		}
		
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		result = searcher.posts(sp);

		return objectMapper.writeValueAsString(result);
	}
	
	/**
	 * Search for media
	 * @param request
	 * @param response
	 * @param sp The search parameters
	 * @return A map whose keys ("Facebook" and "Twitter") contains the retrieved posts.
	 * @throws Exception 
	 */	
	@GET @Path("/media")
	  @ApiOperation(value = "Search media in the social networks about the specified topics"
//	  , notes = 
//	  "{keyword}: search keyword\n" + 
//	  "{coordinates}: reference point (latitude,longitude)\n" + 
//	  "{maxPostsNumber}: max number of media URLs to return\n"
//	  + "{facebookSources}: max number of Facebook pages to extract info from\n"
//	  + "{radius}: radius of the area to consider (in kilometers)"
	  )
	  @Produces(MediaType.APPLICATION_JSON)
	public String media(@QueryParam("keyword") String keyword, 
			@QueryParam("coordinates") String coords,
			@QueryParam("facebookSources") int facebookSources,
			@QueryParam("maxPostsNumber") int maxPostsNumber,
			@QueryParam("radius") int radius,
			@QueryParam("categories") String categories) throws Exception {
		Map<String, List<Post>> result = new TreeMap<String, List<Post>>();
		SearchParameters sp = new SearchParameters();
		sp.setKeyword(keyword);
		if (coords != null) {
			String[] coordsStr = coords.split(",");
			sp.setCoordinates(new double[]{Double.parseDouble(coordsStr[0]),Double.parseDouble(coordsStr[1])});
		}
		if (facebookSources == 0) {
			sp.setFacebookSources(this.facebookSources);
		} else {
			sp.setFacebookSources(facebookSources);
		}
		if (maxPostsNumber == 0) {
			sp.setMaxPostsNumber(this.maxPostsNumber);
		} else {
			sp.setMaxPostsNumber(maxPostsNumber);
		}
		if (radius == 0) {
			sp.setRadius(this.radius);
		} else {
			sp.setRadius(radius);
		}		
		if (categories != null) {
			String[] cats = categories.split(",");
			sp.setCategories(Arrays.asList(cats));
		} else {
			sp.setCategories(new ArrayList<String>());
		}		
		
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		result = searcher.media(sp);
		return objectMapper.writeValueAsString(result);
	}	
	
	/**
	 * Search for events
	 * @param request
	 * @param response
	 * @param sp The search parameters
	 * @return A list of events.
	 * @throws Exception 
	 */	
	@GET @Path("/events")
	  @ApiOperation(value = "Search events in the social networks about the specified topics"
//	  , notes = 
//	  "{keyword}: search keyword\n" + 
//	  "{coordinates}: reference point (latitude,longitude)\n" + 
//	  "{maxPostsNumber}: max number of events to return\n"
//	  + "{facebookSources}: max number of Facebook pages to extract info from\n"
//	  + "{radius}: radius of the area to consider (in kilometers)"
	  )
	  @Produces(MediaType.APPLICATION_JSON)
	public String events(@QueryParam("keyword") String keyword, 
			@QueryParam("coordinates") String coords,
			@QueryParam("facebookSources") int facebookSources,
			@QueryParam("maxPostsNumber") int maxPostsNumber,
			@QueryParam("radius") int radius,
			@QueryParam("categories") String categories) throws Exception {
		SearchParameters sp = new SearchParameters();
		sp.setKeyword(keyword);
		if (coords != null) {
			String[] coordsStr = coords.split(",");
			sp.setCoordinates(new double[]{Double.parseDouble(coordsStr[0]),Double.parseDouble(coordsStr[1])});
		}
		if (facebookSources == 0) {
			sp.setFacebookSources(this.facebookSources);
		} else {
			sp.setFacebookSources(facebookSources);
		}
		if (maxPostsNumber == 0) {
			sp.setMaxPostsNumber(this.maxPostsNumber);
		} else {
			sp.setMaxPostsNumber(maxPostsNumber);
		}
		if (radius == 0) {
			sp.setRadius(this.radius);
		} else {
			sp.setRadius(radius);
		}	
		if (categories != null) {
			String[] cats = categories.split(",");
			sp.setCategories(Arrays.asList(cats));
		} else {
			sp.setCategories(new ArrayList<String>());
		}		
		
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		return objectMapper.writeValueAsString(searcher.events(sp));
	}	
	
	
	/**
	 * Search for events
	 * @param request
	 * @param response
	 * @param sp The search parameters
	 * @return A list of events.
	 * @throws Exception 
	 */	
	@GET @Path("/places")
	  @ApiOperation(value = "Search places on facebook about the specified topics")
	  @Produces(MediaType.APPLICATION_JSON)
	public String places(@QueryParam("keyword") String keyword, 
			@QueryParam("coordinates") String coords,
			@QueryParam("facebookSources") int facebookSources,
			@QueryParam("maxPostsNumber") int maxPostsNumber,
			@QueryParam("radius") int radius,
			@QueryParam("categories") String categories) throws Exception {
		SearchParameters sp = new SearchParameters();
		sp.setKeyword(keyword);
		if (coords != null) {
			String[] coordsStr = coords.split(",");
			sp.setCoordinates(new double[]{Double.parseDouble(coordsStr[0]),Double.parseDouble(coordsStr[1])});
		}
		if (facebookSources == 0) {
			sp.setFacebookSources(this.facebookSources);
		} else {
			sp.setFacebookSources(facebookSources);
		}
		if (maxPostsNumber == 0) {
			sp.setMaxPostsNumber(this.maxPostsNumber);
		} else {
			sp.setMaxPostsNumber(maxPostsNumber);
		}
		if (radius == 0) {
			sp.setRadius(this.radius);
		} else {
			sp.setRadius(radius);
		}	
		if (categories != null) {
			String[] cats = categories.split(",");
			sp.setCategories(Arrays.asList(cats));
		} else {
			sp.setCategories(new ArrayList<String>());
		}		
		
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		return objectMapper.writeValueAsString(searcher.facebookPlaces(sp));
	}	
	
	
	/**
	 * Search for events
	 * @param request
	 * @param response
	 * @param placeId facebook Id of the place posting the events
	 * @return A list of events.
	 * @throws Exception 
	 */	
	@GET @Path("/facebookPlaceEvents")
	  @ApiOperation(value = "Search events for a given facebook Place page"
//	  , notes = 
//	  "{placeId}: facebook Id of the place posting the events\n"
	  )
	  @Produces(MediaType.APPLICATION_JSON)
	public String facebookPlaceEvents(@QueryParam("placeId") String placeId) throws Exception {
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		return objectMapper.writeValueAsString(searcher.facebookEvents(placeId));
	}		
	
	
	@GET @Path("/usage")
	  @ApiOperation(value = "Search shares/likes in the social networks about the specified url"
//	  , notes = 
//	  "{url}: url to obtain social info about"
	  )
	  @Produces(MediaType.APPLICATION_JSON)
	public String usage(@QueryParam("url") String url) throws Exception {

		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		return objectMapper.writeValueAsString(searcher.usage(url));
	}	
	
	@GET @Path("/friendsUsage")
	  @ApiOperation(value = "Search shares/likes about the specified url made by friends of a user in the social networks"
//			  , notes = 
//			  "{url}: url to obtain social info about\n" + 
//			  "{facebookToken}: a facebook token\n" + 
//			  "{twitterAccessToken}: a twitter access token\n" +
//			  "{twitterAccessTokenSecret}: the twitter access token secret"
			  )
	  @Produces(MediaType.APPLICATION_JSON)
	public String friendsUsage(@QueryParam("url") String url,
			@QueryParam("facebookToken") String facebookToken,
			@QueryParam("twitterAccessToken") String twitterAccessToken,
			@QueryParam("twitterAccessTokenSecret") String twitterAccessTokenSecret) throws Exception {

		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);
		
		return objectMapper.writeValueAsString(searcher.friendsShare(url, facebookToken ,twitterAccessToken, twitterAccessTokenSecret));
	}		
	
	
}
