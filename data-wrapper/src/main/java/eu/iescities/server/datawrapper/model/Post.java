package eu.iescities.server.datawrapper.model;

import java.util.List;

/**
 * Data about a (Facebook or Twitter) post.
 */
public class Post {

	public static final String FACEBOOK = "Facebook";
	public static final String TWITTER = "Twitter";
	
	/**
	 * Message of the post
	 */
	private String message;
	
	/**
	 * List of links to media attached to the post
	 */
	private List<String> media;
	
	/**
	 * Comments to the original message
	 */
	private List<String> comments;
	
	/**
	 * Parent page (for FACEBOOK posts)
	 */
	private String parent;
	
	/**
	 * Parent page id (for FACEBOOK posts)
	 */
	private String parentId;	
	
	
	/**
	 * Source ("Facebook" or "Twitter")
	 */
	
	private String source;
	
	/**
	 * Id of the author of the post
	 */
	private long authorId;
	
	/**
	 * Post creation time
	 */
	private long createdAt;
	
	public Post() {
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getMedia() {
		return media;
	}

	public void setMedia(List<String> media) {
		this.media = media;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long author) {
		this.authorId = author;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + (int) (createdAt ^ (createdAt >>> 32));
		result = prime * result + ((media == null) ? 0 : media.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (authorId != other.authorId)
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (createdAt != other.createdAt)
			return false;
		if (media == null) {
			if (other.media != null)
				return false;
		} else if (!media.equals(other.media))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

}
