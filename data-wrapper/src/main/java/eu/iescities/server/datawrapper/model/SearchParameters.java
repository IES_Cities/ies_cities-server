package eu.iescities.server.datawrapper.model;

import java.util.List;

/**
 * Parameters used for queries.
 */
public class SearchParameters {

	/**
	 * Keyword(s) to search for
	 */
	private String keyword;
	
	/**
	 * Coordinates around which to search: latitude and longitude
	 */
	private double coordinates[];
	
	/**
	 * Number of Facebook places to search posts from
	 */
	private int facebookSources = 1;
	
	/**
	 * Maximum number of post per source (FACEBOOK/Twitter)
	 */
	private int maxPostsNumber;

	/**
	 * Radius (in meters) for geolocalized searches
	 */
	private int radius;
	
	/**
	 * List of categories a place category must belong to
	 */
	private List<String> categories;
	
	public SearchParameters() {
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public int getFacebookSources() {
		return facebookSources;
	}

	public void setFacebookSources(int facebookSources) {
		this.facebookSources = facebookSources;
	}

	public int getMaxPostsNumber() {
		return maxPostsNumber;
	}

	public void setMaxPostsNumber(int postsNumber) {
		this.maxPostsNumber = postsNumber;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

}
