package eu.iescities.server.datawrapper.config;

import java.util.Properties;

public class Config {
	
	private static Config instance = null;
	
	private int facebookSources;
	private int maxPostsNumber;
	private int radius;	
	private String fbClientId;
	private String fbClientSecret;	
	private String twConsumerKey;	
	private String twConsumerSecret;	
	private String twAccessToken;	
	private String twAccessTokenSecret;		

	public Config() {
		// TODO Auto-generated constructor stub
	}

	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}
	
	public void setProperties(Properties props) {
		twConsumerKey = props.getProperty("tw.consumerKey");
		twConsumerSecret = props.getProperty("tw.consumerSecret");
		twAccessToken = props.getProperty("tw.accessToken");
		twAccessTokenSecret = props.getProperty("tw.accessTokenSecret");
		
		fbClientId = props.getProperty("fb.clientId");
		fbClientSecret = props.getProperty("fb.clientSecret");

		facebookSources = Integer.parseInt(props.getProperty("default.facebookSources"));
		maxPostsNumber = Integer.parseInt(props.getProperty("default.maxPostsNumber"));
		radius = Integer.parseInt(props.getProperty("default.radius"));
	}

	public int getFacebookSources() {
		return facebookSources;
	}

	public int getMaxPostsNumber() {
		return maxPostsNumber;
	}

	public int getRadius() {
		return radius;
	}

	public String getFbClientId() {
		return fbClientId;
	}

	public String getFbClientSecret() {
		return fbClientSecret;
	}

	public String getTwConsumerKey() {
		return twConsumerKey;
	}

	public String getTwConsumerSecret() {
		return twConsumerSecret;
	}

	public String getTwAccessToken() {
		return twAccessToken;
	}

	public String getTwAccessTokenSecret() {
		return twAccessTokenSecret;
	}
}
