package eu.iescities.server.datawrapper.search;

import java.net.URL;

import eu.iescities.server.datawrapper.model.Place;

/**
 * Container used to store informations for multithreaded queries 
 */
public class PlaceRequest {

	/**
	 * place
	 */
	private Place place;
	
	/**
	 * query URL
	 */
	private URL url;
	
	/**
	 * query result
	 */
	private String result;

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
