package eu.iescities.server.datawrapper.search;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.net.ssl.HttpsURLConnection;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.CharStreams;

import eu.iescities.server.datawrapper.model.Event;
import eu.iescities.server.datawrapper.model.Place;
import eu.iescities.server.datawrapper.model.Post;
import eu.iescities.server.datawrapper.model.SearchParameters;
import eu.iescities.server.datawrapper.model.Usage;


public class FacebookSearcher {

	private static final int MIN_POSTS = 10;
	
	private String fbClientId;
	private String fbClientSecret;	
	private static ExecutorService executorService;
	
	public FacebookSearcher(String fbClientId, String fbClientSecret) {
		super();
		this.fbClientId = fbClientId;
		this.fbClientSecret = fbClientSecret;
		if (executorService == null) {
			executorService = Executors.newCachedThreadPool();
		}
	}
	
	/**
	 * Search for events on Facebook
	 * @param sp  The search parameters
	 * @return a list of Events
	 * @throws Exception
	 */
	public List<Event> events(SearchParameters sp) throws Exception {
		List<Event> result = searchEvents(sp);
		return result;
	}
	
	/**
	 * Search for events for a Facebook Place page
	 * @param id  the Facebook Id for a Place
	 * @return a list of Events
	 * @throws Exception
	 */	
	public List<Event> events(String id) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);
		List<Place> places = getPlace(id);
		return findEvents(places, 0, token);
	}	
	
	protected List<Place> places(SearchParameters sp) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);
		List<Place> places = findPlaces(sp, token);
		return places;
	}

	/**
	 * Search first for Facebook Places related to the query and then for the
	 * posts published on them.
	 * 
	 * @param sp
	 *          The search parameters
	 * @return A list of Posts
	 * @throws Exception
	 */
	protected List<Post> search(SearchParameters sp) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);
		List<Place> places = findPlaces(sp, token);
//		places.addAll(findPages(sp, token));
		List<Post> result = findPosts(places, Math.max(MIN_POSTS, sp.getMaxPostsNumber() / sp.getFacebookSources()), false, token);
		return result;
	}

	/**
	 * Search first for Facebook Places related to the query and then for the
	 * Photos published on them.
	 * 
	 * @param sp
	 *          The search parameters
	 * @return A list of Posts
	 * @throws Exception
	 */
	protected List<Post> searchPhotos(SearchParameters sp) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);
		List<Place> places = findPlaces(sp, token);
//		places.addAll(findPages(sp, token));		
		List<Post> result = findPhotos(places,  Math.max(MIN_POSTS, sp.getMaxPostsNumber() / sp.getFacebookSources()), token);
		return result;
	}

	/**
	 * Search for Facebook events
	 * @param sp The search parameters
	 * @return A list of Events
	 * @throws Exception
	 */
	private List<Event> searchEvents(SearchParameters sp) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);
		List<Place> places = findPlaces(sp, token);
//		places.addAll(findPages(sp, token));
		List<Event> result = findEvents(places,  Math.max(MIN_POSTS, sp.getMaxPostsNumber() / sp.getFacebookSources()), token);
		return result;
	}
	
	/**
	 * Get the access token needed for Facebook queries
	 * 
	 * @param clientId
	 * @param clientSecret
	 * @return An access token
	 * @throws Exception
	 */
	public String getToken(String clientId, String clientSecret) throws Exception {
		URL url = new URL("https://graph.facebook.com/oauth/access_token?client_id=" + clientId + "&client_secret=" + clientSecret + "&grant_type=client_credentials");
		InputStream is = url.openStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		String token = res.split("=")[1];
		token = URLEncoder.encode(token, "UTF-8");

		return token;
	}
	
	/**
	 * Search for Facebook places
	 * 
	 * @param sp The search parameters
	 * @param token
	 *          The access token
	 * @return A list of Places
	 * @throws Exception
	 */
	private static List<Place> findPlaces(SearchParameters sp, String token) throws Exception {
		if (sp.getCoordinates() == null) {
			return Lists.newArrayList();
		}
		return findRoot(sp, token, "place");
	}

	
	/**
	 * Search for Facebook pages
	 * 
	 * @param sp The search parameters
	 * @param token
	 *          The access token
	 * @return A list of Places
	 * @throws Exception
	 */
	private static List<Place> findPages(SearchParameters sp, String token) throws Exception {
		return findRoot(sp, token, "page");
	}	
	
	/**
	 * Search for a specified type of Facebook page
	 * @param sp The search parameters
	 * @param token
	 *          The access token
	 * @return A list of Places
	 * @return
	 * @throws Exception
	 */
	private static List<Place> findRoot(SearchParameters sp, String token, String type) throws Exception {
		List<Place> result = Lists.newArrayList();
		HttpsURLConnection connection;

		String area;
		if (sp.getCoordinates() != null) {
			area = "&center=" + sp.getCoordinates()[0] + "," + sp.getCoordinates()[1] + "&distance=" + sp.getRadius();
		} else {
			area = "";
		}		

		URL url = new URL(("https://graph.facebook.com/v2.2/search?" + ((sp.getKeyword() != null) ?("q=" + sp.getKeyword()):"") + "&type=" + type + area + "&limit=" + sp.getFacebookSources() + "&access_token=" + token).replace(" ", "%20"));
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);
		Iterator<JsonNode> it = rootNode.path("data").elements();

		while (it.hasNext()) {
			JsonNode obj = it.next();
			
			Place place = mapper.readValue(obj.toString(), Place.class);

			if (sp.getCategories() != null && !sp.getCategories().isEmpty()) {
				if (!sp.getCategories().contains(place.getCategory())) {
					continue;
				}
			}
			
			result.add(place);
		}

		return result;
	}	
	
	
	/**
	 * Currently not used. Given a list of Place, return another list containing
	 * Places strictly related to them
	 * 
	 * @param places
	 *          the original Places
	 * @param token
	 *          The access token
	 * @return A list of Places
	 * @throws Exception
	 */
	private List<Place> findBestPlaces(List<Place> places, String token) throws Exception {
		List<Place> result = Lists.newArrayList();
		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();

		for (Place place : places) {
			URL url = new URL(("https://graph.facebook.com/fql?q=SELECT name,page_id,best_page_id FROM page WHERE page_id=" + place.getId() + "&access_token=" + token).replace(" ", "%20"));
			connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.connect();
			
			InputStream is = connection.getInputStream();

			String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));

			Map map = mapper.readValue(res, Map.class);

			List list = (List) (map.get("data"));
			for (Object obj : list) {
				String bestId = (String) ((Map) obj).get("best_page_id");

				if (bestId != null && !place.getId().equals(bestId)) {
					URL url2 = new URL(("https://graph.facebook.com/fql?q=SELECT name,page_id FROM page WHERE page_id=" + bestId + "&access_token=" + token).replace(" ", "%20"));

					HttpsURLConnection connection2 = (HttpsURLConnection) url2.openConnection();
					connection2.setRequestMethod("GET");
					connection2.setDoOutput(true);
					connection2.connect();

					InputStream is2 = connection2.getInputStream();

					String res2 = CharStreams.toString(new InputStreamReader(is2, Charsets.UTF_8));

					JsonNode rootNode = mapper.readValue(res2, JsonNode.class);
					Iterator<JsonNode> it = rootNode.path("data").elements();

					while (it.hasNext()) {
						JsonNode obj2 = it.next();
						Long id = obj2.path("page_id").longValue();
						String name = obj2.path("name").textValue();
						Place bestPlace = new Place(name, id.toString());
						result.add(bestPlace);
					}
					
				}
			}
		}

		result.addAll(places);

		return result;
	}

	/**
	 * Search for posts made by a particular user
	 * @param userId the user id
	 * @param posts number of posts
	 * @param mediaOnly return only media content
	 * @return a list of Post
	 * @throws Exception
	 */
	public List<Post> findUserPosts(String userId, int posts, boolean mediaOnly) throws Exception {	
		List<Post> result = Lists.newArrayList();
		
		String token = getToken(fbClientId, fbClientSecret);
		
		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();		
		
		URL url = new URL(("https://graph.facebook.com/v2.2/" + userId + "?fields=posts&format=json&method=get&pretty=1&access_token=" + token).replace(" ", "%20"));
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		connection.disconnect();

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);

		Iterator<JsonNode> it = rootNode.path("posts").path("data").elements();
		
		while (it.hasNext()) {
			JsonNode obj = it.next();
			Post post = new Post();
			String message = obj.path("message").textValue();
			if (message == null) {
				message = "";
			}
			Long author = Long.parseLong(obj.path("from").path("id").textValue());
			DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
			Long createdAt = parser.parseDateTime(obj.path("created_time").textValue()).getMillis();
			List<String> media = extractMedia(obj);
			if (message.isEmpty() && media.isEmpty()) {
				continue;
			}

			if (mediaOnly && media.isEmpty()) {
				continue;
			}

			List<String> comments = extractComments(obj);

			post.setMessage(message);
			post.setMedia(media);
			post.setComments(comments);
			post.setSource(Post.FACEBOOK);
			post.setParentId(userId);
			post.setAuthorId(author);
			post.setCreatedAt(createdAt);
			result.add(post);
		}

		return result;
	}
	
	/**
	 * Return posts made by friends of a user
	 * @param token a user token
	 * @param keyword keyword to look for
	 * @param maxPosts maximum number of posts
	 * @return a list of Post
	 * @throws Exception
	 */
	public List<Post> findFriendsPosts(String token, String keyword, int maxPosts) throws Exception {	
		List<Post> result = Lists.newArrayList();		
		
		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();		
		
		URL url = new URL(("https://graph.facebook.com/v2.2/me/friends?fields=posts.limit(" + maxPosts + ")&format=json&method=get&pretty=1&access_token=" + token).replace(" ", "%20"));
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		System.out.println(res);
		connection.disconnect();

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);

		Iterator<JsonNode> it = rootNode.path("data").elements();
		
		while (it.hasNext()) {
			JsonNode obj = it.next();
			String id = obj.path("id").textValue();

			Iterator<JsonNode> it2 = obj.path("posts").path("data").elements();

			while (it2.hasNext()) {

				JsonNode obj2 = it2.next();
				
				String message = obj2.path("message").textValue();

				 if (message == null || !message.toLowerCase().contains(keyword.toLowerCase())) {
				 continue;
				 }

				Long author = Long.parseLong(obj2.path("from").path("id").textValue());
				DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
				Long createdAt = parser.parseDateTime(obj2.path("created_time").textValue()).getMillis();
				List<String> media = extractMedia(obj2);
				if (message.isEmpty() && media.isEmpty()) {
					continue;
				}

				List<String> comments = extractComments(obj2);

				Post post = new Post();
				post.setMessage(message);
				post.setMedia(media);
				post.setComments(comments);
				post.setSource(Post.FACEBOOK);
				post.setParentId(id);
				post.setAuthorId(author);
				post.setCreatedAt(createdAt);
				result.add(post);
			}
		}

		return result;
	}	
	
	/**
	 * Return a list of friends ids
	 * @param token a user token
	 * @return a list of user ids
	 * @throws Exception
	 */
	public List<String> findFriendsIds(String token) throws Exception {	
		List<String> result = Lists.newArrayList();
		
		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();		
		
		URL url = new URL(("https://graph.facebook.com/v2.2/me?fields=friends{id}&format=json&method=get&pretty=1&access_token=" + token).replace(" ", "%20"));
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		connection.disconnect();

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);

		Iterator<JsonNode> it = rootNode.path("friends").path("data").elements();
		
		while (it.hasNext()) {
			JsonNode obj = it.next();
			String id = obj.path("id").textValue();

			result.add(id);
		}

		return result;
	}		
	
	/**
	 * Return information about Facebook likes/shares/messages for a specific URL
	 * @param address a URL
	 * @return information about likes/shares/messages of the URL
	 * @throws Exception
	 */
	public Usage likes(String address) throws Exception {
		String token = getToken(fbClientId, fbClientSecret);

		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();
		
		URL url = new URL(("https://graph.facebook.com/v2.2/" + URLEncoder.encode(address,"UTF-8") +  "?access_token=" + token));
		
			
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		connection.disconnect();

		JsonNode rootNode = mapper.readValue(res, JsonNode.class);		
		
		JsonNode share = rootNode.path("share");
		int shares = share.path("share_count").intValue();
		int comments = share.path("comment_count").intValue();
		
		JsonNode obj = rootNode.path("og_object");
		String id = obj.path("id").textValue();
		
		url = new URL(("https://graph.facebook.com/v2.2/" + id + "/likes?summary=1&access_token=" + token).replace(" ", "%20"));
		
		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		is = connection.getInputStream();

		res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		connection.disconnect();
		
		rootNode = mapper.readValue(res, JsonNode.class);

		JsonNode summary = rootNode.path("summary");
		int likes = summary.path("total_count").intValue();		
		
		Usage usage = new Usage();
		usage.setLikes(likes);
		usage.setShares(shares);
		usage.setReplies(comments);
		
		return usage;
	}	
	
	/**
	 * Return information about Facebook likes made by friends of a user for a specific URL
	 * @param address a URL
	 * @param token a user token
	 * @return
	 * @throws Exception
	 */
	public Usage friendsLikes(String address, String token) throws Exception {
		List<String> likes = Lists.newArrayList();
		
		ObjectMapper mapper = new ObjectMapper();
		
		String url = ("https://graph.facebook.com/v2.2/" + URLEncoder.encode(address,"UTF-8") +  "?access_token=" + token);
		String res = pagedQuery(url, false).get(0);
		
		JsonNode rootNode = mapper.readValue(res, JsonNode.class);				
		JsonNode obj = rootNode.path("og_object");
		String id = obj.path("id").textValue();
		
		url = ("https://graph.facebook.com/v2.2/" + id + "?fields=likes.limit(1000){id}&access_token=" + token).replace(" ", "%20");
		
		List<String> pages = pagedQuery(url,  true);
		
		for (String page: pages) {
			rootNode = mapper.readValue(page, JsonNode.class);
			
			Iterator<JsonNode> it = rootNode.path("likes").path("data").elements();
			while (it.hasNext()) {
				JsonNode next = it.next();
				id = next.path("id").textValue();

				likes.add(id);
			}
		}
		
		List<String> friends = findFriendsIds(token);
		
		int count = Sets.intersection(new HashSet<String>(likes), new HashSet<String>(friends)).size();
		
		Usage usage = new Usage();
		usage.setLikes(count);

		return usage;
	}	
	
	
	/**
	 * Find Posts from the Facebook Streams for a list of Places
	 * 
	 * @param places
	 *          The list of Places
	 * @param posts
	 *          Maximum number of posts
	 * @param mediaOnly
	 *          Return only posts containing some kind of media content. Should
	 *          use {@link #findPhotos(List, int, String)} instead.
	 * @param token
	 *          The access token
	 * @return a list of Posts
	 * @throws Exception
	 */
	private List<Post> findPosts(List<Place> places, int posts, boolean mediaOnly, String token) throws Exception {
		Set<Post> result = Sets.newHashSet();

		ObjectMapper mapper = new ObjectMapper();

		List<PlaceRequest> placeRequests = Lists.newArrayList();
		for (Place place : places) {
			URL url = new URL(("https://graph.facebook.com/v2.2/" + place.getId() + "?fields=posts&format=json&method=get&pretty=1&access_token=" + token).replace(" ", "%20"));
			PlaceRequest pr = new PlaceRequest();
			pr.setUrl(url);
			pr.setPlace(place);
			placeRequests.add(pr);
		}

		parallelRequests(placeRequests);

		for (PlaceRequest pr : placeRequests) {		
			JsonNode rootNode = mapper.readValue(pr.getResult(), JsonNode.class);

			Iterator<JsonNode> it = rootNode.path("posts").path("data").elements();
			
			rootNode.path("posts").path("data");

			while (it.hasNext()) {
				JsonNode obj = it.next();
				Post post = new Post();
				String message = obj.path("message").textValue();
				if (message == null) {
					message = "";
				}
				Long author = Long.parseLong(obj.path("from").path("id").textValue());
				DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
				Long createdAt = parser.parseDateTime(obj.path("created_time").textValue()).getMillis();
				List<String> media = extractMedia(obj);
				if (message.isEmpty() && media.isEmpty()) {
					continue;
				}

				if (mediaOnly && media.isEmpty()) {
					continue;
				}

				List<String> comments = extractComments(obj);

				post.setMessage(message);
				post.setMedia(media);
				post.setComments(comments);
				post.setSource(Post.FACEBOOK);
				post.setParent(pr.getPlace().getName());
				post.setParentId(pr.getPlace().getId());
				post.setAuthorId(author);
				post.setCreatedAt(createdAt);
				result.add(post);
			}

		}
		return new ArrayList<Post>(result);
	}

	/**
	 * Find Photos associated to a list of Places
	 * 
	 * @param places
	 *          The list of Places
	 * @param posts
	 *          Maximum number of posts
	 * @param token
	 *          The access token
	 * @return A list of Posts
	 * @throws Exception
	 */
	private List<Post> findPhotos(List<Place> places, int posts, String token) throws Exception {
		Set<Post> result = Sets.newHashSet();

		ObjectMapper mapper = new ObjectMapper();
		
		List<PlaceRequest> placeRequests = Lists.newArrayList();
		for (Place place : places) {
			URL url = new URL(("https://graph.facebook.com/v2.2/"  + place.getId() + "/photos?limit=" + posts + "&access_token=" + token).replace(" ", "%20"));
			PlaceRequest pr = new PlaceRequest();
			pr.setUrl(url);
			pr.setPlace(place);
			placeRequests.add(pr);
		}

		parallelRequests(placeRequests);

		for (PlaceRequest pr : placeRequests) {		
			JsonNode rootNode = mapper.readValue(pr.getResult(), JsonNode.class);

			Iterator<JsonNode> it = rootNode.path("data").elements();
			while (it.hasNext()) {
				JsonNode obj = it.next();
				Post post = new Post();
				Long author = Long.parseLong(obj.path("from").path("id").textValue());
				String message = obj.path("name").textValue();
				DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
				Long createdAt = parser.parseDateTime(obj.path("created_time").textValue()).getMillis();
				List<String> media = Lists.newArrayList();
				String link = obj.path("link").textValue().replace("graph.facebook", "facebook");
				media.add(link);
				List<String> comments = extractComments(obj);

				post.setAuthorId(author);
				post.setMessage(message);
				post.setMedia(media);
				post.setComments(comments);
				post.setSource(Post.FACEBOOK);
				post.setParent(pr.getPlace().getName());
				post.setParentId(pr.getPlace().getId());
				post.setCreatedAt((long) createdAt * 1000);
				result.add(post);
			}

		}
		return new ArrayList<Post>(result);
	}

	/**
	 * Find the (not expired) Events associated to a list of Places
	 * 
	 * @param places
	 *          The list of Places
	 * @param posts
	 *          Maximum number of posts
	 * @param token
	 *          The access token
	 * @return A list of Events
	 * @throws Exception
	 */
	private List<Event> findEvents(List<Place> places, int posts, String token) throws Exception {
		Set<Event> result = Sets.newHashSet();

		ObjectMapper mapper = new ObjectMapper();

		List<PlaceRequest> placeRequests = Lists.newArrayList();
		for (Place place : places) {
			URL url = new URL(("https://graph.facebook.com/v2.2/" + place.getId() + "/events?since=" + (int) (System.currentTimeMillis() / 1000) + "&format=json&method=get&pretty=1&access_token=" + token).replace(" ", "%20"));
			PlaceRequest pr = new PlaceRequest();
			pr.setUrl(url);
			pr.setPlace(place);
			placeRequests.add(pr);
		}

		parallelRequests(placeRequests);

		for (PlaceRequest pr : placeRequests) {

			JsonNode rootNode = mapper.readValue(pr.getResult(), JsonNode.class);

			Iterator<JsonNode> it = rootNode.path("data").elements();
			while (it.hasNext()) {
				JsonNode obj = it.next();
				Event event = new Event();
				String name = obj.path("name").textValue();
				String description = obj.path("description").textValue();
				String location = obj.path("location").textValue();
				DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
				String time = obj.path("start_time").textValue();
				long startTime = parser.parseDateTime(time).getMillis();
				time = obj.path("end_time").textValue();
				Long endTime = null;
				if (time != null && !time.isEmpty()) {
					endTime = parser.parseDateTime(time).getMillis();
				} else {}

				String image = obj.path("pic_cover").path("source").textValue();

				event.setName(name);
				event.setDescription(description);
				event.setSource(Post.FACEBOOK);
				event.setParent(pr.getPlace().getName());
				event.setParentId(pr.getPlace().getId());
				event.setStartTime(startTime);
				event.setEndTime(endTime);
				event.setImage(image);
				event.setLocation(location);

				result.add(event);
			}

		}
		return new ArrayList<Event>(result);
	}

	/**
	 * Extracts the links to media contents attached to a post
	 * 
	 * @param node
	 * @return
	 */
	private List<String> extractMedia(JsonNode node) {
		List<String> result = Lists.newArrayList();

		if (node.has("attachment") && node.path("attachment").has("media")) {
			Iterator<JsonNode> it = node.path("attachment").path("media").elements();

			while (it.hasNext()) {
				JsonNode obj = it.next();
				if (obj.has("href")) {
					result.add(obj.path("href").textValue());
				}
			}
		}
		
		if (node.has("picture")) {
			result.add(node.path("picture").textValue());
		}
		return result;
	}

	/**
	 * Extract the comments to a post
	 * 
	 * @param node
	 * @return
	 */
	private List<String> extractComments(JsonNode node) {
		List<String> result = Lists.newArrayList();
		Iterator<JsonNode> it = node.path("comments").path("data").elements();
		while (it.hasNext()) {
			JsonNode obj = it.next();
			String text = obj.path("message").textValue();
			result.add(text);
		}
		return result;
	}
	
	/**
	 * Get a Facebook Place given its id
	 * @param id the Place id
	 * @return
	 */
	private static List<Place> getPlace(String id) {
		List<Place> result = Lists.newArrayList(new Place("", id));
		return result;
	}	
	
	/**
	 * Execute a query whose paged result are navigated to collect results
	 * @param query the initial Facebook api query URL
	 * @param pagination navigate results?
	 * @return a list of results
	 * @throws Exception
	 */
	public List<String> pagedQuery(String query, boolean pagination) throws Exception {
		List<String> result = Lists.newArrayList();

		HttpsURLConnection connection;

		ObjectMapper mapper = new ObjectMapper();

		URL url = new URL(query);

		connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		InputStream is = connection.getInputStream();

		String res = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
		connection.disconnect();
		result.add(res);

		if (pagination) {
			JsonNode rootNode = mapper.readValue(res, JsonNode.class);

			JsonNode paging = rootNode.path("paging");
			String next = paging.path("next").textValue();

			if (next != null) {
				result.addAll(pagedQuery(next, pagination));
			}
		}

		return result;
	}	
	
	/**
	 * Make many queries (Place related) in multiple threads.
	 * @param placeRequests
	 * @throws Exception
	 */
	private void parallelRequests(List<PlaceRequest> placeRequests) throws Exception {
		List<Future<PlaceRequest>> results = Lists.newArrayList();
		
		for (PlaceRequest pr : placeRequests) {
			CallableRequest callableReq = new CallableRequest();
			callableReq.setPlaceRequest(pr);
			Future<PlaceRequest> future = executorService.submit(callableReq);
			results.add(future);
		}
		for (Future<PlaceRequest> plan: results) {
			plan.get();
		}
	}
	
	private class CallableRequest implements Callable<PlaceRequest> {
		
		private PlaceRequest placeRequest;
		
		public void setPlaceRequest(PlaceRequest placeRequest) {
			this.placeRequest = placeRequest;
		}
		
		@Override
		public PlaceRequest call() throws Exception {
			try {
				HttpsURLConnection connection;

				connection = (HttpsURLConnection) placeRequest.getUrl().openConnection();
				connection.setRequestMethod("GET");
				connection.setDoOutput(true);
				connection.connect();

				InputStream is = connection.getInputStream();

				String r = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
				
				connection.disconnect();

				placeRequest.setResult(r);
				
			} catch (Exception e) {
				
			}
			return placeRequest;
		}

	}	
	
	
	
}
