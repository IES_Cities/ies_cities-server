package eu.iescities.server.datawrapper.model;

import java.util.List;
import java.util.Map;

/**
 * Minimal data about a Facebook Place.
 */
public class Place {

	/**
	 * Place name
	 */
	private String name;
	
	/**
	 * Place id
	 */
	private String id;
	
	private String category;
	
	private List<Map<String, String>> category_list;
	
	private Location location; 
	
	public Place() {
	}

	public Place(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Map<String, String>> getCategory_list() {
		return category_list;
	}

	public void setCategory_list(List<Map<String, String>> category_list) {
		this.category_list = category_list;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return name;
	}

}
