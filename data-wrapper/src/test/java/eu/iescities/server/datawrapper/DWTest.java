package eu.iescities.server.datawrapper;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

public class DWTest extends JerseyTest {

	@Override
	protected Application configure() {
		return new TestApplication();
	}

	@Test
	public void pingTest() throws IOException {

		String writeResponse;

		writeResponse = target("social/ping")
				.request().get(String.class);
		assertEquals("pong", writeResponse);
	}

	@Test
	public void postsTest() {
		String writeResponse = target("social/posts")
				.queryParam("keyword","Trento")
				.queryParam("coordinates", "46.07466,11.11914")
				.request()
				.get(String.class);
		
		System.err.println(writeResponse);
	}

	@Test
	public void eventsTest() {
		String writeResponse = target("social/events")
				.queryParam("keyword","Trento")
				.queryParam("coordinates", "46.07466,11.11914")
				.request()
				.get(String.class);
		
		System.err.println(writeResponse);
	}

	@Test
	public void mediaTest() {
		String writeResponse = target("social/media")
				.queryParam("keyword","Trento")
				.queryParam("coordinates", "46.07466,11.11914")
				.request()
				.get(String.class);
		
		System.err.println(writeResponse);
	}

}

