package eu.iescities.server.datawrapper.test;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.junit.Test;

import eu.iescities.server.datawrapper.model.Post;
import eu.iescities.server.datawrapper.model.SearchParameters;
import eu.iescities.server.datawrapper.search.SocialSearcher;

public class SocialWrapperTest {

//parameters needed to query Facebook and Twitter
	private String fbClientId = "";
	private String fbClientSecret = "";
	private String twConsumerKey = "";
	private String twConsumerSecret = "";
	private String twAccessToken = "";
	private String twAccessTokenSecret = "";

	@Test
	public void searchTest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.INDENT_OUTPUT, true);
		
		SocialSearcher searcher = new SocialSearcher(fbClientId, fbClientSecret, twConsumerKey, twConsumerSecret, twAccessToken, twAccessTokenSecret);

		Map<String, List<Post>> result = new TreeMap<String, List<Post>>();
		SearchParameters sp = new SearchParameters();
		sp.setKeyword("Trento");
		sp.setCoordinates(new double[] {46.07466,11.11914});
		sp.setFacebookSources(5);
		sp.setMaxPostsNumber(50);
		
		result = searcher.posts(sp);
		
		System.out.println(mapper.writeValueAsString(result));
		System.out.println("____________________________");
		
		result = searcher.media(sp);
		
		System.out.println(mapper.writeValueAsString(result));
		System.out.println("____________________________");		
	}

}
