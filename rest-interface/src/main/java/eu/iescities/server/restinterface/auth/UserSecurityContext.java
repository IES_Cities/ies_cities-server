package eu.iescities.server.restinterface.auth;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.User;

public class UserSecurityContext implements SecurityContext {

	public static final UserInfo anonymousUserInfo = new UserInfo();

	private UserInfo userInfo;

	public UserSecurityContext(String auth) {
		if (auth != null) {
			String[] authInfo = BasicAuth.decode(auth);

			try {
				String sessionId = UserManagement.login(authInfo[0],
						authInfo[1]);
				User user = UserManagement.getUserBySession(sessionId);
				userInfo = new UserInfo(user, authInfo[1], sessionId);
			} catch (IllegalArgumentException e) {

			}
		}
	}
	
	@Override
	public Principal getUserPrincipal() {
		if (userInfo != null) {
			return userInfo;
		}
		return anonymousUserInfo;
	}

	@Override
	public boolean isUserInRole(final String role) {
		if (userInfo != null) {
			return userInfo.getRoles().contains(role);
		}

		return false;
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public String getAuthenticationScheme() {
		return BASIC_AUTH;
	}
}
