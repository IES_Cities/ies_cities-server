package eu.iescities.server.restinterface.serialization;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
	
@Provider
@Produces("application/json")
public class DatasetListBodyWriter implements MessageBodyWriter<List<Dataset>> {
 
		@Override
		public boolean isWriteable(Class<?> type, Type genericType,
				Annotation[] annotations, MediaType mediaType) {
			return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE) 
	                && List.class.isAssignableFrom(type) 
	                && (((ParameterizedType)genericType).getActualTypeArguments()[0]).equals(Dataset.class);
		}
	
		@Override
		public long getSize(List<Dataset> datasetList, Class<?> type,
				Type genericType, Annotation[] annotations, MediaType mediaType) {
			// deprecated by JAX-RS 2.0 and ignored by Jersey runtime
			return 0;
		}
	
		@Override
		public void writeTo(List<Dataset> datasetList, Class<?> type, Type genericType,
				Annotation[] annotations, MediaType mediaType,
				MultivaluedMap<String, Object> httpHeaders,
				OutputStream entityStream) throws IOException, WebApplicationException {
	
			final Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.setDateFormat("dd/MM/yyyy HH:mm:ss")
					.setPrettyPrinting()
					.create();
			entityStream.write(gson.toJson(datasetList).getBytes());
		}
}
