package eu.iescities.server.restinterface.serialization;

import java.util.List;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;

public class SchemaInfo {

	private ConnectorType type = ConnectorType.not_available;
	private List<String> tables;
	
	public SchemaInfo() {
		
	}
	
	public SchemaInfo(ConnectorType type) {
		this.type = type;
	}
	
	public SchemaInfo(ConnectorType type, List<String> tables) {
		this.type = type;
		this.tables = tables;
	}

	public ConnectorType getType() {
		return type;
	}

	public void setType(ConnectorType type) {
		this.type = type;
	}

	public void setTables(List<String> tables) {
		this.tables = tables;
	}
	
	public List<String> getTables() {
		return tables;
	}
}
