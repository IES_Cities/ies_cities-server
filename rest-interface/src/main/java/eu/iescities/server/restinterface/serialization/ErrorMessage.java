package eu.iescities.server.restinterface.serialization;

import eu.iescities.server.restinterface.exception.IESCitiesException;

public class ErrorMessage {

	private boolean error;
	private int status;
	private String message;
	
	public ErrorMessage() {
		this.error = true;
	}
	
	public ErrorMessage(IESCitiesException e) {
		this.status = e.getStatus().getStatusCode();
		this.message = e.getMessage();
		this.error = true;
	}
	
	public ErrorMessage(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
}
