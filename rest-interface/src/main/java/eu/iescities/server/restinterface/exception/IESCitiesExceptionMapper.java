package eu.iescities.server.restinterface.exception;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import eu.iescities.server.restinterface.serialization.ErrorMessage;

@Provider
@Singleton
public class IESCitiesExceptionMapper implements ExceptionMapper<IESCitiesException> {

	public Response toResponse(IESCitiesException e) {
		return Response.status(e.getStatus()).entity(new ErrorMessage(e))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
