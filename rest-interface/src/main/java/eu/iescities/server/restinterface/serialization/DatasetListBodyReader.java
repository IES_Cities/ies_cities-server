package eu.iescities.server.restinterface.serialization;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@Provider
@Consumes("application/json")
public class DatasetListBodyReader implements MessageBodyReader<List<Dataset>> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE) 
                && List.class.isAssignableFrom(type) 
                && (((ParameterizedType)genericType).getActualTypeArguments()[0]).equals(Dataset.class);
	}

	@Override
	public List<Dataset> readFrom(Class<List<Dataset>> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		
		final BufferedReader reader = new BufferedReader(new InputStreamReader(entityStream));
        final StringBuilder strBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            strBuilder.append(line);
        }
		
		final Gson gson = new GsonBuilder()
			.setDateFormat("dd/MM/yyyy HH:mm:ss")
			.create();
		final Type collectionType = new TypeToken<ArrayList<Dataset>>(){}.getType();
		return gson.fromJson(strBuilder.toString(), collectionType);
	}
}
