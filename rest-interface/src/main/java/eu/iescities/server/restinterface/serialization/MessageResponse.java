package eu.iescities.server.restinterface.serialization;

public class MessageResponse {
	private String message;

	public void setResponse(String msg) {
		this.message = msg;
	}

	public String getResponse() {
		return message;
	}
}