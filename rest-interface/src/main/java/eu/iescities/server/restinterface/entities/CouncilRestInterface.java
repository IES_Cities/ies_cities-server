package eu.iescities.server.restinterface.entities;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.serialization.Dataset;

/**
 * 
 * @author Alex Rodriguez, Unai Aguilera
 * 
 */
@Path("/entities/councils")
@Api(value = "/entities", description = "Operations about basic database entities")
public class CouncilRestInterface {

	@GET
	@Path("/all")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all councils", notes = "Returns a list with all the registered councils", response = Council.class)
	public List<Council> getAllCouncils(
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false)
			@DefaultValue("1000") @QueryParam("limit") int limit,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return CouncilManagement.getAllCouncils(offset, limit, lang, Language.EN);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/{councilid}/admins")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets a list of all council admins", notes = "User objects are only filled with the user id, name, surname, isAdmin and isDeveloper")
	public List<User> getAllCouncilAdmins(
			@ApiParam(value = "Council ID", required = true) @PathParam("councilid") int councilId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
			try {
				final Language lang	= Language.valueOf(langStr.toUpperCase());
				Council council = CouncilManagement.getCouncilById(councilId, lang, Language.EN);
				return CouncilManagement.getCouncilAdmins(
						council.getCouncilId(), offset, limit);
			} catch (IllegalArgumentException e) {
				throw new EntityNotFoundException(e.getMessage());
			}
	}

	@GET
	@Path("/{councilid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets a council", response = Council.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Council not found") })
	public Council getCouncil(
			@ApiParam(value = "Council ID to get", required = true) @PathParam("councilid") int councilid,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return CouncilManagement.getCouncilById(councilid, lang, Language.EN);
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@POST
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Registers a new council", notes = "Only accesible for IES-Cities admin users", response = Council.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid data supplied") })
	public Council createCouncil(@Context SecurityContext sc,
			@ApiParam(value = "Council data", required = true) Council council) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			GeoScope geoScope = checkGeoscope(council, userInfo);
			council.setGeographicalScope(geoScope);
			return CouncilManagement.createCouncil(userInfo.getSessionId(), council);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	private GeoScope checkGeoscope(Council council, UserInfo userInfo) {
		GeoScope geoScope = council.getGeographicalScope();

		try {
			geoScope = GeoScopeManagement.getScopeById(geoScope.getId());
		} catch (IllegalArgumentException e) {
			geoScope = GeoScopeManagement.createScope(userInfo.getSessionId(),
					geoScope.getName(), geoScope.getNorthEastGeoPoint(),
					geoScope.getSouthWestGeoPoint());
		}
		return geoScope;
	}

	@PUT
	@RolesAllowed({ UserRoles.IESCITIES_ADMIN, UserRoles.COUNCIL_ADMIN })
	@Path("/{councilid}/admins")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds a user as a council administrator", notes = "Only accesible for global or other council's admin users")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "User not found") })
	public Response addAdmin(
			@Context SecurityContext sc,
			@ApiParam(value = "Council ID", required = true) @PathParam("councilid") int councilid,
			@ApiParam(value = "User", required = true) User user) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			// TODO How can be detected council or user not found?
			CouncilManagement.addCouncilAdmin(userInfo.getSessionId(),
					councilid, user.getUserId());
			return Response.ok().build();
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage()); 
		}
	}

	@DELETE
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@Path("/{councilid}/admins/{userid}")
	@ApiOperation(value = "Removes an admin from the council", notes = "Only available to IES Cities global admins")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid data supplied") })
	public Response removeAdmin(
			@Context SecurityContext sc,
			@ApiParam(value = "Council ID to get", required = true) @PathParam("councilid") int councilid,
			@ApiParam(value = "User ID", required = true) @PathParam("userid") int userid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			CouncilManagement.removeCouncilAdmin(userInfo.getSessionId(),
					councilid, userid);
			return Response.ok().build();
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@PUT
	@RolesAllowed({ UserRoles.IESCITIES_ADMIN, UserRoles.COUNCIL_ADMIN })
	@Path("/{councilid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates a council", notes = "Only accesible for council admin or IES-Cities admins", response = Council.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid data supplied") })
	public Council updateCouncil(
			@Context SecurityContext sc,
			@ApiParam(value = "Council ID", required = true) @PathParam("councilid") int councilId,
			@ApiParam(value = "Council data", required = true) Council council) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			GeoScope geoScope = checkGeoscope(council, userInfo);
			council.setGeographicalScope(geoScope);
			council.setCouncilId(councilId);
			return CouncilManagement.updateCouncil(userInfo.getSessionId(),
					council);
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@DELETE
	@RolesAllowed({ UserRoles.IESCITIES_ADMIN })
	@Path("/{councilid}")
	@ApiOperation(value = "Removes a council", notes = "Removes a council. Only accesible for admin users")
	public Response removeCouncil(
			@Context SecurityContext sc,
			@ApiParam(value = "Council ID", required = true) @PathParam("councilid") int councilId) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			long deletedCouncils = CouncilManagement.removeCouncil(
					userInfo.getSessionId(), councilId);
			if (deletedCouncils == 0) {
				throw new EntityNotFoundException("Unknown council id: " + councilId+ ".");
			}
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/{councilid}/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the applications registered within a council")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Application> getCouncilApps(
			@ApiParam(value = "Council ID to search", required = true) @PathParam("councilid") int councilId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallbacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final Council council = CouncilManagement.getCouncilById(councilId, lang, Language.EN);
			try {
				return CouncilManagement.getCouncilApps(council.getCouncilId(),
						offset, limit, lang, Language.EN);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@RolesAllowed({ UserRoles.COUNCIL_ADMIN, UserRoles.IESCITIES_ADMIN })
	@Path("/{councilid}/users")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the users registered within council", notes = "Only accesible for council or global admins")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<User> getCouncilUsers(
			@Context SecurityContext sc,
			@ApiParam(value = "Council ID to search", required = true) @PathParam("councilid") int councilId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallbacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		
		final Language lang	= Language.valueOf(langStr.toUpperCase());
		final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
		try {
			final Council council = CouncilManagement.getCouncilById(councilId, lang, Language.EN);
			try {
				return CouncilManagement.getCouncilUsers(
						userInfo.getSessionId(), council.getCouncilId(),
						offset, limit);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/{councilid}/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the Datasets of a council", notes = "Only accesible for council admin or IES-Cities admins. Mapping information is only available for dataset owners or global administators.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Dataset> getCouncilDatasets(@Context SecurityContext sc,
			@ApiParam(value = "Council ID to search", required = true) @PathParam("councilid") int councilId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallbacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final Council council = CouncilManagement.getCouncilById(councilId, lang, Language.EN);
			try {
				List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets = CouncilManagement.getCouncilDataSets(
						council.getCouncilId(), offset, limit, lang, Language.EN);
				
				datasets = DatasetRestInterface.checkSensibleInfo(datasets, (UserInfo) sc.getUserPrincipal());
				return DatasetRestInterface.createDatasets(datasets);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());			
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
}
