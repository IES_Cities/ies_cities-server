package eu.iescities.server.restinterface.util.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import eu.iescities.server.querymapper.BasicConfig;

public class Config extends BasicConfig {
	
	private static Config instance = null;
	
	private String swaggerURL = "";
	private String dataDir = "";
	
	private String email = "";
	private String pass = "";
	private String host = "";
	
	private int schedulerThreadCount = 10;
	private int schedulerCheckInterval = 10;
	
	private Properties p;
	
	private Config() throws IOException {
		load("iescities.properties");
	}
	
	public static Config getInstance() throws IOException {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}

	public void load(String file) throws IOException {
		p = new Properties();
		try (FileInputStream is = new FileInputStream(file)) {
			p.load(is);
			
			System.out.println("IES Cities configuration properties");
			System.out.println("===================================");
			
			dataDir = getProperty(p, "datadir", "ies_data");
			swaggerURL = getProperty(p, "swaggerurl", "http://localhost:8080/IESCities/api");
			
			email = getProperty(p, "email", "");
			pass = getProperty(p, "password", "");
			host = getProperty(p, "host", "");
			
			schedulerThreadCount = getProperty(p, "schedulerthreadcount", 10);
			schedulerCheckInterval = getProperty(p, "schedulercheckinterval", 10);
		} 
	}
	
	public Properties getProperties() {
		return p;
	}

	public String getSwaggerURL() {
		return swaggerURL;
	}

	public String getDataDir() {
		return dataDir;
	}

	public String getEmail() {
		return email;
	}

	public String getPass() {
		return pass;
	}

	public String getHost() {
		return host;
	}

	public int getSchedulerThreadCount() {
		return schedulerThreadCount;
	}

	public void setSchedulerThreadCount(int threadCount) {
		this.schedulerThreadCount = threadCount;
	}

	public int getSchedulerCheckInterval() {
		return schedulerCheckInterval;
	}

	public void setSchedulerCheckInterval(int checkInterval) {
		this.schedulerCheckInterval = checkInterval;
	}
}
