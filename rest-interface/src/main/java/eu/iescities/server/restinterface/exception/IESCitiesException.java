package eu.iescities.server.restinterface.exception;

import javax.ws.rs.core.Response.Status;

@SuppressWarnings("serial")
public class IESCitiesException extends Exception {

	private final Status status;
	private final String message;
	
	public IESCitiesException(Status status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
}
