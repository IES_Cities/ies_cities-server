package eu.iescities.server.restinterface.exception;

import javax.ws.rs.core.Response;

@SuppressWarnings("serial")
public class UnexpectedErrorException extends IESCitiesException {

	public UnexpectedErrorException(String message) {
		super(Response.Status.INTERNAL_SERVER_ERROR, message);
	}
}
