package eu.iescities.server.restinterface.serialization;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;

public class DataSourceConnectorAdapter implements JsonDeserializer<DataSourceConnector>{

	@Override
	public DataSourceConnector deserialize(JsonElement element, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		try {
			final DataSourceConnector connector = ConnectorFactory.load(element.toString());
			return connector;
		} catch (DataSourceConnectorException e) {
			throw new JsonParseException("Invalid connector mapping. " + e.getMessage());
		}
	}
}
