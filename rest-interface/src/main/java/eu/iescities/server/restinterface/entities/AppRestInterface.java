package eu.iescities.server.restinterface.entities;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.serialization.ApplicationInfo;
import eu.iescities.server.restinterface.serialization.Dataset;

@Path("/entities/apps")
@Api(value = "/entities", description = "Operations about database entities")
@PermitAll
public class AppRestInterface {
		
	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates an application", notes = "Only accesible for registered users", response = Application.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Invalid app data supplied") })
	// TODO Application requires to pass Council information (not a council id)
	public Application createApplication(
			@Context SecurityContext sc,
			@ApiParam(value = "Application data", required = true) ApplicationInfo appInfo) throws IESCitiesException {
		try {
			Application application = new Application();
			application.setName(appInfo.getName());
			application.setDescription(appInfo.getDescription());
			application.setUrl(appInfo.getUrl());
			application.setImage(appInfo.getImage());

			application.setRelatedCouncilId(appInfo.getCouncilid());

			application.setVersion(appInfo.getVersion());
			application.setTermsOfService(appInfo.getTos());
			application.setPermissions(appInfo.getPermissions());
			application.setPackageName(appInfo.getPackageName());

			application.setTags(appInfo.getTags());

			GeoScope geoScope = GeoScopeManagement.getScopeById(appInfo
					.getScopeid());
			application.setGeographicalScope(geoScope);
			
			application.setLang(appInfo.getLang());

			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			return ApplicationManagement.registerApp(userInfo.getSessionId(),
					application);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage()); 
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@DELETE
	@RolesAllowed({ UserRoles.APP_DEVELOPER,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{appid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Removes the application", notes = "The application must already exist. Allowed only for application developers or council admins")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "App not found") })
	public Response deleteApplication(
			@Context SecurityContext sc,
			@ApiParam(value = "Application id", required = true) @PathParam("appid") int appId) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			long deletedApps = ApplicationManagement.removeApp(
					userInfo.getSessionId(), appId);
			
			//TODO This should be checked by the removeApp method
			if (deletedApps == 0) {
				throw new EntityNotFoundException("Unknown application id: " + appId + ".");
			}

			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@PUT
	@RolesAllowed({ UserRoles.APP_DEVELOPER,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{appid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the application data", notes = "The application id must already exist. Allowed only for application developers or council admins", response = Application.class)
	public Application updateApplication(
			@Context SecurityContext sc,
			@ApiParam(value = "Application id", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "Updated data for app", required = true) Application app) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			try {
				app.setAppId(appId);
				return ApplicationManagement.updateApp(userInfo.getSessionId(),	app);
			} catch (IllegalArgumentException e) {
				throw new EntityNotFoundException(e.getMessage());
			}
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());

		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());			
		}
	}

	@GET
	@Path("/{appId}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find app by ID", notes = "Returns an app based on ID", response = Application.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "App not found") })
	public Application getApplicationInfo(
			@ApiParam(value = "ID of app to fetch", required = true) @PathParam("appId") int appId,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false) @DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return ApplicationManagement.getAppById(appId, lang, Language.EN);
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get all apps", notes = "Retrieves all the apps registered within IES Cities")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Application> getAllApplications(
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false) 
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return ApplicationManagement.getAllApps(offset, limit, lang, Language.EN);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/near/{geopoint}{radius : (/\\S+)?}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find apps near a central geopoint", notes = "Optionally, a 'radius' parameter can be given to circle an area within the central geopoint")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid coordinates or radius") })
	public List<Application> getAllApplicationsNearGeopoint(
			@ApiParam(value = "Coordinates of central geopoint {lat},{long}", required = true) @PathParam("geopoint") String coordinates,
			@ApiParam(value = "Radius in km from the central geopoint", required = false) @PathParam("radius") String radius,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		ArrayList<Application> appList = new ArrayList<Application>();

		try {
			final GeoPoint nearGeopoint = new GeoPoint(coordinates);

			final Language lang	= Language.valueOf(langStr.toUpperCase());
			for (Application app : ApplicationManagement.getAllApps(0, 1000, lang, Language.EN)) {
				final Council council = CouncilManagement.getCouncilById(app.getRelatedCouncilId(), lang, Language.EN);
				if (council != null) {
					GeoScope geoScope = council.getGeographicalScope();
					if (geoScope != null) {
						GeoPoint geopoint = geoScope.getCentralGeoPoint();
						if (geopoint != null) {
							if (!radius.isEmpty()) {
								radius = radius.replaceFirst("/", "");
								try {
									double r = Double.parseDouble(radius);
			
									double distance = nearGeopoint
											.distanceInKm(geopoint);
			
									if (distance <= r) {
										appList.add(app);
									}
								} catch (NumberFormatException e) {
									throw new InvalidRequestException(e.getMessage());
								}
							} else if (nearGeopoint.equals(geopoint)
									&& !appList.contains(app)) {
								appList.add(app);
							}
						}
					}
				}
			}

			return appList;
		} catch (NumberFormatException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/search/")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find apps that match certain keyword(s)", notes = "The search string is split into words and each is matched against app names, descriptions and tags (OR-connected), case-insensitively.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Application> getAllApplicationsMatching(
			@ApiParam(value = "keywords", required = true) @QueryParam("keywords") String keywords,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return ApplicationManagement.findApps(keywords, offset, limit, lang, Language.EN);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/{appid}/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all the datasets related to an app", notes = "Gets all the datasets related to the specified app. Mapping information is only available for dataset owners or global administators.")
	public List<Dataset> getAppDatasets(@Context SecurityContext sc,
			@ApiParam(value = "Application id", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final Application app = ApplicationManagement.getAppById(appId, lang, Language.EN);
			try {
				List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets = ApplicationManagement.getAllDatasetsOfApp(
						app.getAppId(), offset, limit, lang, Language.EN);
				datasets = DatasetRestInterface.checkSensibleInfo(datasets, (UserInfo) sc.getUserPrincipal());
				return DatasetRestInterface.createDatasets(datasets);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/{appid}/developers")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all the developers related to an app", notes = "Gets all the developers related to the specified app")
	public List<User> getAppDevelopers(
			@ApiParam(value = "Application id", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false) @DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final Application app = ApplicationManagement.getAppById(appId, lang, Language.EN);
			try {
				return ApplicationManagement.getAllDevelopersOfApp(
						app.getAppId(), offset, limit);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());				
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/{appid}/ratings")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all the ratings of this app", notes = "Gets all the ratings related to the specified app")
	public List<AppRating> getAppRatings(
			@ApiParam(value = "Application id", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false) @DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final Application app = ApplicationManagement.getAppById(appId, lang, Language.EN);
			try {
				return ApplicationManagement.getAllRatingsOfApp(app.getAppId(),
						offset, limit);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());			
		}
	}

	@PUT
	@RolesAllowed({ UserRoles.APP_DEVELOPER,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{appid}/developers/")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds an existing user as a developer of the app", notes = "The app id and the user must exist. Allowed only for application developers or IES-Cities admins")
	public Response addDeveloper(
			@Context SecurityContext sc,
			@ApiParam(value = "Application ID", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "App to add as installed. Only the app id is required", required = true) User user) throws IESCitiesException {
		UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
		try {
			// TODO Howto validate that the user already exists?
			ApplicationManagement.addDeveloper(userInfo.getSessionId(), appId,
					user.getUserId());
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@PUT
	@RolesAllowed({ UserRoles.APP_DEVELOPER,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{appid}/datasets/")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds an existing dataset as related to the app", notes = "Adds an existing dataset as related to the app")
	public Response addDataset(
			@Context SecurityContext sc,
			@ApiParam(value = "Application ID", required = true) @PathParam("appid") int appId,
			@ApiParam(value = "Dataset id to add as related. Only the dataset id is required", required = true) Dataset dataset) throws IESCitiesException {
		
		final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
		try {
			ApplicationManagement.addDataset(userInfo.getSessionId(), appId,
					dataset.getDatasetId());
			return Response.ok().build();
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}
}
