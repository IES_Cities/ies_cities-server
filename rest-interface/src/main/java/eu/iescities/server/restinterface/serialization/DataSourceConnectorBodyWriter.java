package eu.iescities.server.restinterface.serialization;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
	
@Provider
@Produces("application/json")
public class DataSourceConnectorBodyWriter implements MessageBodyWriter<DataSourceConnector> {
 
		@Override
		public boolean isWriteable(Class<?> type, Type genericType,
				Annotation[] annotations, MediaType mediaType) {
			return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)
					&& genericType == DataSourceConnector.class;
		}
	
		@Override
		public long getSize(DataSourceConnector connector, Class<?> type,
				Type genericType, Annotation[] annotations, MediaType mediaType) {
			// deprecated by JAX-RS 2.0 and ignored by Jersey runtime
			return 0;
		}
	
		@Override
		public void writeTo(DataSourceConnector connector, Class<?> type, Type genericType,
				Annotation[] annotations, MediaType mediaType,
				MultivaluedMap<String, Object> httpHeaders,
				OutputStream entityStream) throws IOException, WebApplicationException {
	
			entityStream.write(connector.toString().getBytes());
		}
}
