package eu.iescities.server.restinterface.util.swagger;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.wordnik.swagger.jaxrs.config.BeanConfig;

import eu.iescities.server.restinterface.util.config.Config;

@WebListener
public class SwaggerInitializer implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
        	final Config config = Config.getInstance();
        	eu.iescities.server.querymapper.config.Config.getInstance().setDataDir(config.getDataDir());
        	
        	BeanConfig beanConfig = new BeanConfig();
    		beanConfig.setVersion("1.0.0");
    		beanConfig.setResourcePackage("eu.iescities.server");
    		beanConfig.setBasePath(Config.getInstance().getSwaggerURL());

    		beanConfig.setScan(true);
        	
        } catch (IOException e) {
        	System.out.println(e.getMessage());
        }
	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {
	}

}
