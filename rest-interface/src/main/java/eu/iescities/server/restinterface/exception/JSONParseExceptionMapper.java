package eu.iescities.server.restinterface.exception;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.google.gson.JsonParseException;

import eu.iescities.server.restinterface.serialization.ErrorMessage;

@Provider
@Singleton
public class JSONParseExceptionMapper implements ExceptionMapper<JsonParseException> {

	public Response toResponse(JsonParseException e) {
		final ErrorMessage error = new ErrorMessage(Response.Status.BAD_REQUEST.getStatusCode(), e.getMessage());
		return Response.status(Response.Status.BAD_REQUEST).entity(error)
				.type(MediaType.APPLICATION_JSON).build();
	}
}
