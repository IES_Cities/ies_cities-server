package eu.iescities.server.restinterface.exception;

import javax.ws.rs.core.Response;

@SuppressWarnings("serial")
public class InvalidRequestException extends IESCitiesException {

	public InvalidRequestException(String message) {
		super(Response.Status.BAD_REQUEST, message);
	}
}
