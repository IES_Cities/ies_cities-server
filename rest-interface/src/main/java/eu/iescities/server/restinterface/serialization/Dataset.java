package eu.iescities.server.restinterface.serialization;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;

public class Dataset {
	
	@Expose
	private int datasetId = -1;
	
	@Expose
	private int userId = -1;
	
	@Expose
	private int councilId = -1;
	
	@Expose
	private String description;
	
	@Expose
	private DataSourceConnector jsonMapping;
	
	@Expose
	private String name;
	
	@Expose
	private int readAccessNumber;
	
	@Expose
	private int writeAccessNumber;
	
	@Expose
	private int numberAppsUsingIt;
	
	@Expose
	private Date publishingTimestamp;
	
	@Expose
	private int quality = -1;
	
	@Expose
	private int qualityVerifiedBy = -1;
	
	@Expose
	private Language lang;

	public Dataset() {
		
	}

	public Dataset(eu.iescities.server.accountinterface.datainterface.Dataset dataset) {
		this.datasetId = dataset.getDatasetId();
		this.userId = dataset.getUserId();
		this.councilId = dataset.getCouncilId();
		this.description = dataset.getDescription();
		this.name = dataset.getName();
		this.readAccessNumber = dataset.getReadAccessNumber();
		this.writeAccessNumber = dataset.getWriteAccessNumber();
		this.numberAppsUsingIt = dataset.getNumberAppsUsingIt();
		this.publishingTimestamp = dataset.getPublishingTimestamp();
		this.quality = dataset.getQuality();
		this.qualityVerifiedBy = dataset.getQualityVerifiedBy();
		this.lang = dataset.getLang();
		
		try {
			this.jsonMapping = ConnectorFactory.load(dataset.getJsonMapping());
		} catch (DataSourceConnectorException e) {
			this.jsonMapping = new DataSourceConnector();
		}
	}

	public int getDatasetId() {
		return datasetId;
	}


	public void setDatasetId(int datasetId) {
		this.datasetId = datasetId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public int getCouncilId() {
		return councilId;
	}


	public void setCouncilId(int councilId) {
		this.councilId = councilId;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public DataSourceConnector getJsonMapping() {
		return jsonMapping;
	}


	public void setJsonMapping(DataSourceConnector jsonMapping) {
		this.jsonMapping = jsonMapping;
	}
	
	public void setMapping(String json) throws DataSourceConnectorException {
		this.jsonMapping = ConnectorFactory.load(json);
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getReadAccessNumber() {
		return readAccessNumber;
	}


	public void setReadAccessNumber(int readAccessNumber) {
		this.readAccessNumber = readAccessNumber;
	}


	public int getWriteAccessNumber() {
		return writeAccessNumber;
	}


	public void setWriteAccessNumber(int writeAccessNumber) {
		this.writeAccessNumber = writeAccessNumber;
	}


	public int getNumberAppsUsingIt() {
		return numberAppsUsingIt;
	}


	public void setNumberAppsUsingIt(int numberAppsUsingIt) {
		this.numberAppsUsingIt = numberAppsUsingIt;
	}


	public Date getPublishingTimestamp() {
		return publishingTimestamp;
	}


	public void setPublishingTimestamp(Date publishingTimestamp) {
		this.publishingTimestamp = publishingTimestamp;
	}


	public int getQuality() {
		return quality;
	}


	public void setQuality(int quality) {
		this.quality = quality;
	}


	public int getQualityVerifiedBy() {
		return qualityVerifiedBy;
	}


	public void setQualityVerifiedBy(int qualityVerifiedBy) {
		this.qualityVerifiedBy = qualityVerifiedBy;
	}
	
	public Language getLang() {
		return lang;
	}
	
	public void setLang(Language lang) {
		this.lang = lang;
	}

	public eu.iescities.server.accountinterface.datainterface.Dataset getDBDataset() {
		final eu.iescities.server.accountinterface.datainterface.Dataset dataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		dataset.setDatasetId(this.datasetId);
		dataset.setUserId(this.userId);
		dataset.setCouncilId(this.councilId);
		dataset.setDescription(this.description);
		dataset.setJsonMapping(this.jsonMapping.toString());
		dataset.setName(this.name);
		dataset.setReadAccessNumber(this.readAccessNumber);
		dataset.setWriteAccessNumber(this.writeAccessNumber);
		dataset.setNumberAppsUsingIt(this.numberAppsUsingIt);
		dataset.setPublishingTimestamp(this.publishingTimestamp);
		dataset.setQuality(this.quality);
		dataset.setQualityVerifiedBy(this.qualityVerifiedBy);
		dataset.setLang(this.lang);
		
		return dataset;
	}
	
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.setDateFormat("dd/MM/yyyy HH:mm:ss").setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}
}
