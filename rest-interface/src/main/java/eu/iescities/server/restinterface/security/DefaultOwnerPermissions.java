/**
 *  Copyright 2013, 2014, 2015 University of Deusto
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * 
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.security;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.security.Permissions;
import eu.iescities.server.querymapper.datasource.security.TablePermission;

public class DefaultOwnerPermissions extends Permissions {

	public DefaultOwnerPermissions(DataSourceInfo datasetInfo) {
		for (TableInfo tableInfo : datasetInfo.getTables()) {
			addSelectPermission(new TablePermission(tableInfo.getName(), AccessType.ALL));
			addInsertPermission(new TablePermission(tableInfo.getName(), AccessType.ALL));
			addDeletePermission(new TablePermission(tableInfo.getName(), AccessType.OWNER));
			addUpdatePermission(new TablePermission(tableInfo.getName(), AccessType.OWNER));
		}
	}
}
