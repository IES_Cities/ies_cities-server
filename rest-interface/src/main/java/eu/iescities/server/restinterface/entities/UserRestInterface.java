package eu.iescities.server.restinterface.entities;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;

import javax.annotation.security.RolesAllowed;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.exception.UnexpectedErrorException;
import eu.iescities.server.restinterface.serialization.Dataset;
import eu.iescities.server.restinterface.util.config.Config;

@Path("entities/users")
@Api(value = "/entities", description = "Operations about basic database entities")
public class UserRestInterface {

	@GET
	@Path("/me")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the current user information", notes = "Authentication is required to access this method", response = User.class)
	public User getCurrentUser(@Context SecurityContext sc) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			return UserManagement.getUserBySession(userInfo.getSessionId());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/{userid}")
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the  user information of the required user", notes = "Authentication is required to access this method", response = User.class)
	public User getUser(
			@Context SecurityContext sc,
			@ApiParam(value = "ID of the user's ", required = true) @PathParam("userid") int userid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			return UserManagement.getUserById(userInfo.getSessionId(), userid);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/login")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the current user information", notes = "Authentication is required to access this method", response = User.class)
	public User loginUser(@Context SecurityContext sc) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			return UserManagement.getUserBySession(userInfo.getSessionId());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a user", notes = "Creates a user. ", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid user data supplied") })
	public User createUser(
			@ApiParam(value = "User data", required = true) User user) throws IESCitiesException {
		try {
			return UserManagement.registerUser(user);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		} catch (RuntimeException e) {
			throw new UnexpectedErrorException(e.getMessage());
		}
	}

	@PUT
	@Path("/{userid}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the user information", notes = "Updates the user information, by the moment only the password and user profile. Only accesible to registered users", response = User.class)
	public User updateUser(@Context SecurityContext sc,
			@ApiParam(value = "User ID", required = true) @PathParam("userid") int userid,
			@ApiParam(value = "Updated user data", required = true) User user) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			user.setUserId(userid);
			try {
				return UserManagement.updateUser(userInfo.getSessionId(), user,
					userInfo.getPass());
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (RuntimeException e) {
			throw new UnexpectedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/googlelogin")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the current user information", notes = "Authentication is required to access this method", response = User.class)
	public User loginWithGoogle(
			@ApiParam(value = "A user's OAuth 2.0 access token issued by Google", required = true)
			@QueryParam("tokenstring") String tokenstring) throws IESCitiesException {
		try {
			String user = UserManagement.loginWithGoogle(tokenstring);
			return UserManagement.getUserBySession(user);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = "Logs a user out identified by a session ID.", notes = "Authentication is required to access this method")
	public Response logout(@Context SecurityContext sc) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.logout(userInfo.getSessionId());
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/me/installed")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = "Gets the specified user's installed applications.")
	public List<Application> getInstalledApps(@Context SecurityContext sc,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return UserManagement.getInstalledApps(userInfo.getSessionId(), lang, Language.EN);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@PUT
	@Path("/me/installed")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = "Adds an app to the user's installed list", notes = "The app id must exist. Only accesible for registered users")
	public Response addInstalledApp(@Context SecurityContext sc,
			@ApiParam(value = "Application ID", required = true) Application app) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.addInstalledApp(userInfo.getSessionId(),
					app.getAppId());
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@DELETE
	@Path("/me/installed/{appid}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = "Removes an app from the user's list of installed ones", notes = "The app id must exist. Only accesible for registered users")
	public Response removeInstalledApp(
			@Context SecurityContext sc,
			@ApiParam(value = "Application ID", required = true) @PathParam("appid") int appid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.removeInstalledApp(userInfo.getSessionId(), appid);
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/me/developer")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.APP_DEVELOPER)
	@ApiOperation(value = "Gets all applications developed by the user", notes = "Only accesible for registered users")
	public List<Application> getCurrentUserDevelopedApps(@Context SecurityContext sc,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false)
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException { 
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			return UserManagement.getDevelopedApps(userInfo.getSessionId(), lang, Language.EN);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/me/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.APP_DEVELOPER)
	@ApiOperation(value = " Gets all datasets used by the applications the user develops.", notes = "Only accesible for registered users")
	public List<Dataset> getCurrentUserDatasets(
			@Context SecurityContext sc,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000")
			@QueryParam("limit") int limit,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets = UserManagement.getUsedDataSets(userInfo.getSessionId(), offset, limit, lang, Language.EN);
			datasets = DatasetRestInterface.checkSensibleInfo(datasets, userInfo);
			return DatasetRestInterface.createDatasets(datasets);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@POST
	@Path("/{userid}/admin/true")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@ApiOperation(value = " Sets the given user to be an IES cities platform admin.", notes = "Only accesible for IES-Cities admin users")
	public Response addAdmin(
			@Context SecurityContext sc,
			@ApiParam(value = "User ID", required = true) @PathParam("userid") int userid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.addIesAdmin(userInfo.getSessionId(), userid);
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@POST
	@Path("/{userid}/admin/false")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@ApiOperation(value = "Removes the given user from the list of IES cities platform admins.", notes = "Only accesible for IES-Cities admin users")
	public Response removeAdmin(
			@Context SecurityContext sc,
			@ApiParam(value = "User ID", required = true) @PathParam("userid") int userid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.removeIesAdmin(userInfo.getSessionId(), userid);
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@ApiOperation(value = " Returns all users from the system.", notes = "Authentication is required to access this method")
	public List<User> getAllUsers(
			@Context SecurityContext sc,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit)
			throws IESCitiesException { 
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			return UserManagement.getAllUsers(userInfo.getSessionId(), offset,
					limit);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@DELETE
	@Path("/me")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = " Removes the current user from the IES cities platform", notes = "The user must provide its current password for additional authentication")
	public Response removeCurrentUser(@Context SecurityContext sc) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			UserManagement.removeUser(userInfo.getSessionId(),
					userInfo.getPass());
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@DELETE
	@Path("/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@ApiOperation(value = " Removes a user", notes = "The user must exist. Only accesible for IES-Cities admin users")
	public Response removeUser(
			@Context SecurityContext sc,
			@ApiParam(value = "User ID", required = true) @PathParam("userid") int userid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			try {
				UserManagement.getUserById(userInfo.getSessionId(), userid);
				UserManagement.removeUser(userInfo.getSessionId(), userid);
				return Response.ok().build();
			} catch (IllegalArgumentException e) {
				throw new EntityNotFoundException(e.getMessage());
			}
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}
	@GET
	@Path("/reset/password/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Generate a token and send it by email to the user.", notes = "Authentication is required to access this method")
	public Response resetPassword(@Context HttpServletRequest req,
			@ApiParam(value = "Email", required = true) @PathParam("email") String email) throws UnknownHostException, IESCitiesException {
		try {
			String token = UserManagement.createPasswordResetRequest(email);
			Properties mailServerProperties;
			Session getMailSession;
			MimeMessage generateMailMessage;
			//Step1     
	        System.out.println("\n 1st ===> setup Mail Server Properties..");
	        mailServerProperties = System.getProperties();
	        mailServerProperties.put("mail.smtp.port", "587"); // TLS Port
	        mailServerProperties.put("mail.smtp.auth", "true"); // Enable Authentication
	        mailServerProperties.put("mail.smtp.starttls.enable", "true"); // Enable StartTLS
	        System.out.println("Mail Server Properties have been setup successfully..");
	 
	//Step2     
	        System.out.println("\n\n 2nd ===> get Mail Session..");
	        InetAddress ip= InetAddress.getLocalHost();
	         System.out.println("Your current IP address : " + ip.getHostAddress()+":"+req.getLocalPort()+" -->"+req.getLocalAddr());
	        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
	        generateMailMessage = new MimeMessage(getMailSession);
	        generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
	        generateMailMessage.setSubject("Password Reset requeriment from IESCities");
	        //String emailBody = "http://iescities.com:8080/IESCities/api/entities/users/reset/password/phase2/"+token+"/"+newpassword;
	        String emailBody = "https://iescities.com:8443/IESCities/verifyPass.html?token="+token;
	        generateMailMessage.setContent(emailBody, "text/html");
	        System.out.println("Mail Session has been created successfully..");
	 
	        //Step3     
	        System.out.println("\n\n 3rd ===> Get Session and Send mail");
	        Transport transport = getMailSession.getTransport("smtp");
	        // Enter your correct gmail UserID and Password
	        final Config config = Config.getInstance();
	        transport.connect(config.getHost(), config.getEmail(), config.getPass());
	        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
	        transport.close();			
			return Response.ok().build();
		} catch (MessagingException | IllegalArgumentException | IOException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}
	@GET
	@Path("/reset/password/phase2/{token}/{newpassword}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "The password is regenerated using the token.", notes = "Authentication is required to access this method")
	public Response resetPasswordPhase2(@Context HttpServletRequest req,
			@ApiParam(value = "token", required = true) @PathParam("token") String token,
			@ApiParam(value = "New password", required = true) @PathParam("newpassword") String newpassword) throws IESCitiesException {
		try {
			UserManagement.resetPasswordWithResetToken(token, newpassword);
			return Response.ok().build();
		}
		catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}	
}
