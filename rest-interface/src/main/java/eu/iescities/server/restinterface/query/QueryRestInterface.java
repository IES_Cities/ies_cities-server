package eu.iescities.server.restinterface.query;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.PMF;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.loggingrest.DatasetLogging;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSource;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.query.QueriableFactory;
import eu.iescities.server.querymapper.datasource.query.QueriableFactoryException;
import eu.iescities.server.querymapper.datasource.query.SPARQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;
import eu.iescities.server.querymapper.sql.schema.serialization.JSONLDResult;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.exception.UnexpectedErrorException;
import eu.iescities.server.restinterface.security.DefaultOwnerPermissions;
import eu.iescities.server.restinterface.serialization.MessageResponse;

@Path("/data/query")
@Api(value="/data", description = "Operations to query datasets")
public class QueryRestInterface {
	
	public static final int DEFAULT_COUNCIL_ID = Integer.MIN_VALUE;

	public static int getAccesorCouncilID(UserInfo userInfo) {
		User user = userInfo.getUser();
		if (user != null) {
			return user.getPreferredCouncilId();
		}
		return DEFAULT_COUNCIL_ID;
	}
	
	public static int MAX_LOGGED_QUERY = 4096;
	
	public static String trimQuery(String query) {
		return query.substring(0, query.length() > MAX_LOGGED_QUERY?MAX_LOGGED_QUERY:query.length());
	}
	
	private void logSQLQuery(Dataset dataset, UserInfo userInfo, String sqlQuery, int rows) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + trimQuery(sqlQuery) + "\"," +  
									"\"type\": \"SQL\"," + 
									"\"rows\": " + rows +
								"}";
		
		datasetLogging.poststamp("DatasetQuery", dataset.getDatasetId(), getAccesorCouncilID(userInfo), loggedData);
	}
	
	private void logSQLQueryError(Dataset dataset, UserInfo userInfo, String sqlQuery, String cause) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + trimQuery(sqlQuery) + "\"," + 
									"\"type\": \"SQL\"," + 
									"\"error\": \"" + cause + "\"" +
								"}";
		
		datasetLogging.poststamp("DatasetQuery", dataset.getDatasetId(), getAccesorCouncilID(userInfo), loggedData);
	}
	
	private void logSPARQLQuery(Dataset dataset, UserInfo userInfo, String sparqlQuery, int rows) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + trimQuery(sparqlQuery) + "\"," + 
									"\"type\": \"SPARQL\"," + 
									"\"rows\": " + rows +
								"}";
		
		datasetLogging.poststamp("DatasetQuery", dataset.getDatasetId(), getAccesorCouncilID(userInfo), loggedData);
	}
	
	private void logSPARQLQueryError(Dataset dataset, UserInfo userInfo, String sparqlQuery, String cause) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + trimQuery(sparqlQuery) + "\"," + 
									"\"type\": \"SPARQL\"," + 
									"\"error\": \"" + cause + "\"" +
								"}";
		
		datasetLogging.poststamp("DatasetQuery", dataset.getDatasetId(), getAccesorCouncilID(userInfo), loggedData);
	}
	
	@POST
	@Path ("/{datasetid}/sql")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a SQL query on the specified dataset", 
		notes = "Returns a serialized table containing query results. The dataset must be compatible with SQL queries.",
		response = ResultSet.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL query"),
		@ApiResponse(code = 404, message = "Dataset not found") 
	})
	public ResultSet executeSQLQuery(@Context SecurityContext sc,
			@ApiParam(value = "ID of dataset to query", required = true) 
			@PathParam("datasetid") int datasetid,
			@ApiParam(value = "Query in SQL format", required = true)
			String sqlQuery,
			@ApiParam(value = "Data origin selection: any, original, user", required = false)
			@QueryParam("origin") @DefaultValue("original") String origin) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo)sc.getUserPrincipal(); 
			final String userId = userInfo.getUser() != null ? userInfo.getUser().getUsername() : "";
			
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
				final SQLQueriable queriable = QueriableFactory.createQueriable(connector, new Integer(datasetid).toString(), false);
				checkDefaultPermissions(queriable, queriable.getInfo(), userInfo, dataset.getUserId(), connector.getType());
				
				DataOrigin dataOrigin = DataOrigin.ANY;
				if (origin.toUpperCase().equals("ORIGINAL")) {
					dataOrigin = DataOrigin.ORIGIN;
				} else if (origin.toUpperCase().equals("USER")) {
					dataOrigin = DataOrigin.USER;
				}
				
				final ResultSet results = queriable.executeSQLSelect(sqlQuery, userId, dataOrigin);
				
				logSQLQuery(dataset, userInfo, sqlQuery, results.getCount());
				
				return results;
			} catch (DataSourceSecurityManagerException e) {
				logSQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, e.getMessage());
				throw new UnauthorizedErrorException(e.getMessage());
			} catch (DatabaseCreationException | QueriableFactoryException e) {
				throw new UnexpectedErrorException(e.getMessage());
			} catch (DataSourceManagementException | TranslationException e) {
				String cause = "Error processing SQL query. " + e.getMessage();
				logSQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, cause);
				throw new InvalidRequestException(cause);
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage()); 
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@Path ("/{datasetid}/sql/jsonld")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a SQL query on the specified dataset", 
		notes = "Returns a JSON-LD containing query results. The dataset must be compatible with SQL queries.",
		response = JSONLDResult.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL query"),
		@ApiResponse(code = 404, message = "Dataset not found") 
	})
	public String executeSQLQueryJSONLDFormat(@Context SecurityContext sc,
			@ApiParam(value = "ID of dataset to query", required = true) 
			@PathParam("datasetid") int datasetid,
			@ApiParam(value = "Query in SQL format", required = true)
			String sqlQuery) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo)sc.getUserPrincipal();
			
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
				
				if (connector.getType() == ConnectorType.sparql) {
					final SPARQLConnector schemaConnector = (SPARQLConnector) connector;
					
					final SPARQLQueriable query = new SPARQLDataSource(schemaConnector);
					final ResultSet results = query.executeSQLSelect(sqlQuery, userInfo.getName(), DataOrigin.ANY);
					
					final JSONLDResult rs = new JSONLDResult(results, query.getJSONLDContext());
					
					logSQLQuery(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, rs.getCount());
					
					return rs.toJSON();
				} else {
					String cause = "Error processing SQL query. Cannot obtain JSON LD result from a pure SQL database";
					logSQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, cause);
					throw new InvalidRequestException(cause);
				}
			} catch (DataSourceSecurityManagerException e) {
				logSQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, e.getMessage());
				throw new UnauthorizedErrorException(e.getMessage());
			} catch (DataSourceManagementException | TranslationException e) {
				String cause = "Error processing SQL query.";
				logSQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sqlQuery, cause);
				throw new InvalidRequestException(cause);
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@Path ("/{datasetid}/transform/sql")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(value = "Transforms a SQL query on the specified dataset", 
		notes = "Returns an SPARQL version of the query",
		response = MessageResponse.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL query"),
		@ApiResponse(code = 404, message = "Dataset not found") 
	})
	public String transformSQLQuery(
			@ApiParam(value = "ID of dataset to query", required = true) 
			@PathParam("datasetid") int datasetid,
			@ApiParam(value = "Query in SQL format", required = true)
			String sqlQuery) throws IESCitiesException {
		try {
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);

			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
	
				if (connector.getType() == ConnectorType.sparql) {
					final SPARQLConnector schema = (SPARQLConnector) connector;
								
					final SPARQLDataSource queriable = new SPARQLDataSource(schema);
					queriable.executeSQLSelect(sqlQuery, "", DataOrigin.ANY);
					
					return queriable.getTranslatedQuery(); 
				} else {
					throw new InvalidRequestException("Error in dataset description for dataset " + datasetid);
				}
			} catch (Exception e) {
				String cause = "Error processing SQL query. " + e.getMessage();
				throw new InvalidRequestException(cause);
			}  
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@Path ("/{datasetid}/sparql")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a SPARQL query on the specified dataset", 
		notes = "Returns a serialized table containing query results. The dataset must be compatible with SPARQL queries.",
		response = ResultSet.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL query"),
		@ApiResponse(code = 404, message = "Dataset not found") 
	})
	public ResultSet executeSPARLQuery(@Context SecurityContext sc,
			@ApiParam(value = "ID of dataset to query", required = true)
			@PathParam("datasetid") int datasetid, 
			@ApiParam(value = "Query in SPARQL format", required = true)
			String sparqlQuery) throws IESCitiesException {
		
		try {
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
						
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
				
				if (connector.getType() == ConnectorType.sparql) {
					final SPARQLConnector schemaConnector = (SPARQLConnector) connector;
					final SPARQLQueriable queriable = new SPARQLDataSource(schemaConnector);
	
					final ResultSet results = queriable.executeSPARQL(sparqlQuery);
					
					logSPARQLQuery(dataset, (UserInfo)sc.getUserPrincipal(), sparqlQuery, results.getCount());
					
					return results;
				} else {
					String cause = "Invalid mapping description for dataset " + datasetid;
					logSPARQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sparqlQuery, cause);
					throw new InvalidRequestException(cause);
				}
			} catch (DataSourceManagementException e) {
				String cause = "Error processing SPARQL query. " + e.getMessage();
				logSPARQLQueryError(dataset, (UserInfo)sc.getUserPrincipal(), sparqlQuery, cause);
				throw new InvalidRequestException(e.getMessage());
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@RolesAllowed({ UserRoles.COUNCIL_ADMIN,
		UserRoles.IESCITIES_ADMIN })
	@ApiOperation(value = "Executes queries returning a single integer value (count) on the IES Cities database", notes = "Only accesible to admin users")
	@Path("/kpi")
	public String query(@Context SecurityContext sc,
			String sql) throws IESCitiesException {
		final PersistenceManager pm = PMF.getPM();
		
		try {
			final Query query = pm.newQuery("javax.jdo.query.SQL", sql);
			List<?> results = (List<?>) query.execute();
			Long count = (Long) results.iterator().next();
			return count.toString();
		} catch (Exception e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}
	
	public static void checkDefaultPermissions(DataSource dataSource, DataSourceInfo datasetInfo, UserInfo userInfo, int userId, ConnectorType type) throws DataSourceManagementException, DataSourceSecurityManagerException {
		if (dataSource.getPermissions().isEmpty()) {
			switch (type) {
				case json_schema:	final User user = userInfo.getUser();
									if (user == null || user.getUserId() != userId) {
										throw new DataSourceSecurityManagerException(String.format("User %s not allowed to access or update dataset", userInfo.getUser().getUsername()));
									}
									break;
									
				case json:
				case csv:			dataSource.setPermissions(new DefaultOwnerPermissions(datasetInfo));
									break;
									
				default:			break;
			}
		}
	}
}