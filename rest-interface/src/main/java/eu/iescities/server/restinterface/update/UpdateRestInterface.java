package eu.iescities.server.restinterface.update;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.loggingrest.DatasetLogging;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLDataSource;
import eu.iescities.server.querymapper.datasource.update.JSONLDUpdateable;
import eu.iescities.server.querymapper.datasource.update.JSONUpdateable;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.exception.UnexpectedErrorException;
import eu.iescities.server.restinterface.query.QueryRestInterface;
import eu.iescities.server.restinterface.serialization.query.UpdateResult;

@Path("/data/update")
@Api(value="/data", description = "Operations to query & update data")
public class UpdateRestInterface {
	
	private void logSQLUpdate(Dataset dataset, UserInfo userInfo, String sqlUpdate, int rows) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + QueryRestInterface.trimQuery(sqlUpdate) + "\"," + 
									"\"type\": \"SQL\"," + 
									"\"rows\": " + rows +
								"}";
		
		datasetLogging.poststamp("DatasetWrite", dataset.getDatasetId(), QueryRestInterface.getAccesorCouncilID(userInfo), loggedData);
	}
	
	private void logSQLUpdateError(Dataset dataset, UserInfo userInfo, String sqlUpdate, String cause) {
		DatasetLogging datasetLogging = new DatasetLogging();
		
		String loggedData = 	"{" +
									"\"query\": " + "\"" + QueryRestInterface.trimQuery(sqlUpdate) + "\"," + 
									"\"type\": \"SQL\"," + 
									"\"error\": \"" + cause + "\"" +
								"}";
		
		datasetLogging.poststamp("DatasetWrite", dataset.getDatasetId(), QueryRestInterface.getAccesorCouncilID(userInfo), loggedData);
	}

	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{datasetid}/sql")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a SQL update on the specified dataset",
		notes = "The dataset must be compatible with SQL update execution. Only accesible for authorized users.",
		response = UpdateResult.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL update"),
      	@ApiResponse(code = 404, message = "Dataset not found") 
    })
	public UpdateResult executeSQLUpdate(@Context SecurityContext sc,
		@ApiParam(value = "ID of dataset to update", required = true)
		@PathParam("datasetid") int datasetid, 
		@ApiParam(value = "SQL update statement or statements if transaction mode enabled", required = true)
		String sqlUpdate,
		@ApiParam(value = "Enable transaction mode for multiple update execution", required = false)
		@DefaultValue("false") @QueryParam("transaction") boolean transaction) throws IESCitiesException {

		try {
			final UserInfo userInfo = (UserInfo)sc.getUserPrincipal();
			
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
		
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());						
				final SQLUpdateable sqlUpdateable = UpdateableFactory.createUpdateable(connector, new Integer(datasetid).toString(), false);
				QueryRestInterface.checkDefaultPermissions(sqlUpdateable, sqlUpdateable.getInfo(), userInfo, dataset.getUserId(), connector.getType());
				
				int modifiedRows = 0;
				if (!transaction) {
					modifiedRows = sqlUpdateable.executeSQLUpdate(sqlUpdate, userInfo.getUser().getUsername());
				}
				else {
					modifiedRows = sqlUpdateable.executeSQLTransactionUpdate(sqlUpdate, userInfo.getUser().getUsername());
				}
				
				logSQLUpdate(dataset, (UserInfo)sc.getUserPrincipal(), sqlUpdate, modifiedRows);
				
				return new UpdateResult(modifiedRows); 
			} catch (DataSourceSecurityManagerException e) {
				throw new UnauthorizedErrorException(e.getMessage());
			} catch (DatabaseCreationException |UpdateableFactoryException e) {
				throw new UnexpectedErrorException(e.getMessage());
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
			} catch (DataSourceManagementException e) {
				String cause = "Error processing SQL update. " + e.getMessage();
				logSQLUpdateError(dataset, (UserInfo)sc.getUserPrincipal(), sqlUpdate, cause);
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{datasetid}/jsonld")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes an insert on the specified dataset",
		notes = "The dataset must be backed by a SPARQL endpoint. Only accesible for authorized users.",
		response = UpdateResult.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL update"),
      	@ApiResponse(code = 404, message = "Dataset not found") 
    })
	public UpdateResult executeJSONLDUpdate(@Context SecurityContext sc,
		@ApiParam(value = "ID of dataset to update", required = true)
		@PathParam("datasetid") int datasetid, 
		@ApiParam(value = "JSON-LD containing the data to insert", required = true)
		String jsonld) throws IESCitiesException {

		try {
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
		
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
	
				if (connector.getType() == ConnectorType.sparql) {
					final SPARQLConnector schemaConnector = (SPARQLConnector) connector;
												
					final JSONLDUpdateable updateJSONLD = new SPARQLDataSource(schemaConnector, false);						
					final int rows = updateJSONLD.executeJSONLDUpdate(jsonld);
					
					logSQLUpdate(dataset, (UserInfo)sc.getUserPrincipal(), jsonld, rows);
					
					return new UpdateResult(rows); 
				} else {
					String cause = "Expected SPARQL compatible dataset";
					logSQLUpdateError(dataset, (UserInfo)sc.getUserPrincipal(), jsonld, cause);
					throw new InvalidRequestException(cause);
				}
			} catch (Exception e) {
				String cause = "Error processing JSON-LD update. " + e.getMessage();
				logSQLUpdateError(dataset, (UserInfo)sc.getUserPrincipal(), jsonld, cause);
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{datasetid}/json")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes an insert on the specified dataset",
		notes = "The dataset must be backed by a SPARQL endpoint. Only accesible for authorized users.",
		response = UpdateResult.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL update"),
      	@ApiResponse(code = 404, message = "Dataset not found") 
    })
	public UpdateResult executeJSONUpdate(@Context SecurityContext sc,
		@ApiParam(value = "ID of dataset to update", required = true)
		@PathParam("datasetid") int datasetid, 
		@ApiParam(value = "JSON-LD containing the data to insert", required = true)
		String json) throws IESCitiesException {

		try {
			final UserInfo userInfo = (UserInfo)sc.getUserPrincipal();
			
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
		
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());				
				final JSONUpdateable jsonUpdateable = UpdateableFactory.createJSONUpdateable(connector, new Integer(datasetid).toString(), false);
				QueryRestInterface.checkDefaultPermissions(jsonUpdateable, jsonUpdateable.getInfo(), userInfo, dataset.getUserId(), connector.getType());
				
				final int updatedRows = jsonUpdateable.executeJSONUpdate(json, userInfo.getUser().getUsername());
		
				logSQLUpdate(dataset, (UserInfo)sc.getUserPrincipal(), json, updatedRows);
				
				return new UpdateResult(updatedRows); 
			} catch (DataSourceSecurityManagerException e) {
				throw new UnauthorizedErrorException(e.getMessage());
			} catch (DatabaseCreationException | UpdateableFactoryException e) {
				throw new UnexpectedErrorException(e.getMessage());
			} catch (DataSourceManagementException | TranslationException e) {
				String cause = "Error processing SQL update. " + e.getMessage();
				logSQLUpdateError(dataset, (UserInfo)sc.getUserPrincipal(), json, cause);
				throw new InvalidRequestException(e.getMessage());
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
	
	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{datasetId}/sparul")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes an SPARUL on the specified dataset",
		notes = "The dataset must be backed by a SPARQL endpoint. Only accesible for authorized users.",
		response = UpdateResult.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 400, message = "Error processing SQL update"),
      	@ApiResponse(code = 404, message = "Dataset not found") 
    })
	public UpdateResult executeSPARQLUpdate(@Context SecurityContext sc,
		@ApiParam(value = "ID of dataset to update", required = true)
		@PathParam("datasetid") int datasetid, 
		@ApiParam(value = "SPARUL statement", required = true)
		String sparul) throws IESCitiesException {

		try {
			final Dataset dataset = DatasetManagement.getDatasetById(datasetid, Language.EN, null);
		
			try {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
	
				if (connector.getType() == ConnectorType.sparql) {
					final SPARQLConnector schema = (SPARQLConnector) connector;
												
					final UpdateRequest request = new UpdateRequest();
					request.add(sparul);
					
					final UpdateProcessor p = UpdateExecutionFactory.createRemote(request, schema.getUpdateEndpoint());
					p.execute();
					
					return new UpdateResult(0); 
				} else {
					String cause = "Expected SPARUL compatible dataset";
					throw new InvalidRequestException(cause);
				}
			} catch (DataSourceConnectorException e) {
				throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
}