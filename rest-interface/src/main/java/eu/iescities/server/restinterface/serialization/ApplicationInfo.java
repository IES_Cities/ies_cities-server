package eu.iescities.server.restinterface.serialization;

import java.util.Set;

import eu.iescities.server.accountinterface.datainterface.Language;

public class ApplicationInfo {

	private String name;
	private String description;
	private String url;
	private String image;
	private String version;
	private String tos;
	private String permissions;
	private String packageName;
	private Set<String> tags;
	private int councilid;
	private int scopeid;
	private Language lang;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTos() {
		return tos;
	}
	public void setTos(String tos) {
		this.tos = tos;
	}
	public String getPermissions() {
		return permissions;
	}
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public int getCouncilid() {
		return councilid;
	}
	public void setCouncilid(int councilid) {
		this.councilid = councilid;
	}
	public int getScopeid() {
		return scopeid;
	}
	public void setScopeid(int scopeid) {
		this.scopeid = scopeid;
	}
	public Language getLang() {
		return lang;
	}
	public void setLang(Language lang) {
		this.lang = lang;
	}
}
