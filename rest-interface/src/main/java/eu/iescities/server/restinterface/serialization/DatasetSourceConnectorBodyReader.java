package eu.iescities.server.restinterface.serialization;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;

@Provider
@Consumes("application/json")
public class DatasetSourceConnectorBodyReader implements MessageBodyReader<DataSourceConnector> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)
				&& genericType == DataSourceConnector.class;
	}

	@Override
	public DataSourceConnector readFrom(Class<DataSourceConnector> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		
		final BufferedReader reader = new BufferedReader(new InputStreamReader(entityStream));
        final StringBuilder strBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            strBuilder.append(line);
        }
		
        try {
        	final DataSourceConnector connector = ConnectorFactory.load(strBuilder.toString());
        	return connector;
        } catch (DataSourceConnectorException e) {
        	throw new WebApplicationException("Invalid connector mapping. " + e.getMessage());
        }
	}
}
