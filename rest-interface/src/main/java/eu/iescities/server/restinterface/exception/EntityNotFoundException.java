package eu.iescities.server.restinterface.exception;

import javax.ws.rs.core.Response;

@SuppressWarnings("serial")
public class EntityNotFoundException extends IESCitiesException {

	public EntityNotFoundException(String message) {
		super(Response.Status.NOT_FOUND, message);
	}
}
