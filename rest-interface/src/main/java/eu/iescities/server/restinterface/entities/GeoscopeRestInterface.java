package eu.iescities.server.restinterface.entities;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;

@Path("entities/geoscopes")
@Api(value = "/entities", description = "Operations about basic database entities")
public class GeoscopeRestInterface {

	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a new geoscope", notes = "Only accesible for registered users", response = GeoScope.class)
	public GeoScope createGeoscope(@Context SecurityContext sc,
			@ApiParam(value = "Geographical scope", required = true) GeoScope geoscope) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			return GeoScopeManagement.createScope(userInfo.getSessionId(),
					geoscope.getName(), geoscope.getNorthEastGeoPoint(),
					geoscope.getSouthWestGeoPoint());
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}

	}

	// TODO Geoscopes can be created by anyone but only removed by admins???
	@DELETE
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@Path("/{geoscopeid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes the current user", notes = "Only accesible for IES-Cities admins")
	public Response deleteGeoscope(
			@Context SecurityContext sc,
			@ApiParam(value = "Geoscope id", required = true) @PathParam("geoscopeid") int geoscopeid) throws IESCitiesException {
		try {
			UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			long deletedGeoscopes = GeoScopeManagement.removeScope(
					userInfo.getSessionId(), geoscopeid);

			if (deletedGeoscopes == 0) {
				throw new EntityNotFoundException("Unknown geoscopeid id: " + geoscopeid + ".");
			}

			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/{geoscopeid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes the current user", notes = "The geoscope id must already exist")
	public GeoScope getGeoScope(
			@ApiParam(value = "Geoscope id", required = true) @PathParam("geoscopeid") int geoscopeid) throws IESCitiesException {
		try {
			return GeoScopeManagement.getScopeById(geoscopeid);
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	@GET
	@Path("/search/")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find geoscopes that match certain keyword(s)", notes = "The search string is split into words and each is matched against dataset names and descriptions (OR-connected), case-insensitively.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<GeoScope> getGeoscopesMathing(
			@ApiParam(value = "keywords", required = true) @QueryParam("keywords") String keywords,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false)
			@DefaultValue("1000") @QueryParam("limit") int limit) throws IESCitiesException {
		try {
			// TODO Maybe it should be changed to comma separated values
			return GeoScopeManagement.findScopes(keywords, offset, limit);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@Path("/covering/{geopoint}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find geoscopes covering the passed geopoint")
	public List<GeoScope> getGeoscopesCovering(
			@ApiParam(value = "Coordinates of central geopoint {lat},{long}", required = true)
			@PathParam("geopoint") String coordinates) throws IESCitiesException {
		try {
			GeoPoint geopoint = new GeoPoint(coordinates);
			return GeoScopeManagement.getScopesCoveringPoint(geopoint);
		} catch (NumberFormatException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}
}
