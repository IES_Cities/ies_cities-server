package eu.iescities.server.restinterface.cors;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

public class CrossOriginResourceSharingFilter implements ContainerResponseFilter {
	
	@Override
	public void filter(ContainerRequestContext creq, ContainerResponseContext cresp) throws IOException {
        cresp.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
        cresp.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS");
        cresp.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");
	}
}
