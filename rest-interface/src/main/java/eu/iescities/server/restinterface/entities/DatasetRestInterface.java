package eu.iescities.server.restinterface.entities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.quartz.SchedulerException;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaDataSource;
import eu.iescities.server.querymapper.datasource.query.QueriableFactory;
import eu.iescities.server.querymapper.datasource.query.QueriableFactoryException;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import eu.iescities.server.restinterface.auth.UserInfo;
import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.exception.EntityNotFoundException;
import eu.iescities.server.restinterface.exception.IESCitiesException;
import eu.iescities.server.restinterface.exception.InvalidRequestException;
import eu.iescities.server.restinterface.exception.UnauthorizedErrorException;
import eu.iescities.server.restinterface.exception.UnexpectedErrorException;
import eu.iescities.server.restinterface.serialization.Dataset;
import eu.iescities.server.restinterface.serialization.SchemaInfo;
import eu.iescities.server.restinterface.util.scheduler.IESCitiesMainJob;

@Path("entities/datasets")
@Api(value = "/entities", description = "Operations about basic database entities")
public class DatasetRestInterface {

	@POST
	@RolesAllowed({ UserRoles.REGISTERED_USER,
			UserRoles.COUNCIL_ADMIN,
			UserRoles.IESCITIES_ADMIN })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a dataset", notes = "Only accesible form council admins or IES-Cities admin users", response = Dataset.class)
	public Dataset createDataset(@Context SecurityContext sc,
			@ApiParam(value = "New dataset creation info") Dataset dataset) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			final eu.iescities.server.accountinterface.datainterface.Dataset registeredDataset =  DatasetManagement.registerDataset(userInfo.getSessionId(), dataset.getDBDataset());
			final DataSourceConnector connector = dataset.getJsonMapping();

			if (connector.getType() == ConnectorType.json_schema) {
				new JSONSchemaDataSource((JSONSchemaConnector) connector, new Integer(registeredDataset.getDatasetId()).toString());
			} else {
				try {
					final IESCitiesMainJob mainJob = new IESCitiesMainJob();
					final String datasetID = "" + registeredDataset.getDatasetId();
					mainJob.createDatasourceJob(datasetID, connector, QuartzScheduler.getInstance().getScheduler());
				} catch (IOException e) {
					throw new UnexpectedErrorException(e.getMessage());
				} catch (SchedulerException e) {
					throw new UnexpectedErrorException(e.getMessage());
				}
			}

			return new Dataset(registeredDataset);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		} catch (DatabaseCreationException | DataSourceManagementException e) {
			throw new UnexpectedErrorException(e.getMessage());
		}
	}

	@PUT
	@RolesAllowed({ UserRoles.COUNCIL_ADMIN,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{datasetid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the dataset information", notes = "The dataset must already exist. Only accesible form council admins or IES-Cities admin users", response = Dataset.class)
	public Dataset updateDataset(
			@Context SecurityContext sc,
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetid,
			@ApiParam(value = "Dataset info", required = true) Dataset dataset,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			
			final Language lang	= Language.valueOf(langStr.toUpperCase());

			try {
				DatasetManagement.getDatasetById(datasetid, lang, Language.EN);
			} catch (IllegalArgumentException e) {
				throw new EntityNotFoundException(e.getMessage());
			}

			final eu.iescities.server.accountinterface.datainterface.Dataset updatedDataset = DatasetManagement.updateDataset(userInfo.getSessionId(),
					dataset.getDBDataset());
			return new Dataset(updatedDataset);
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@DELETE
	@RolesAllowed({ UserRoles.REGISTERED_USER,
			UserRoles.COUNCIL_ADMIN,
			UserRoles.IESCITIES_ADMIN })
	@Path("/{datasetid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes a dataset", notes = "Only accesible form council admins or IES-Cities admin users")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Dataset not found") })
	public Response deleteDataset(
			@Context SecurityContext sc,
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetid) throws IESCitiesException {
		try {
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();

			final long deletedDatasets = DatasetManagement.removeDataSet(userInfo.getSessionId(), datasetid);
			
			if (deletedDatasets == 0) { 
				throw new EntityNotFoundException("Unknown dataset id: " + datasetid + ".");
			}
			
			return Response.ok().build();
		} catch (SecurityException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		}
	}

	@GET
	@Path("/{datasetid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets a dataset", notes = "Returns the dataset information. Mapping information is only available for dataset owners or global administators.",
		response = Dataset.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Dataset not found") })
	public Dataset getDataset(@Context SecurityContext sc,
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetId,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			eu.iescities.server.accountinterface.datainterface.Dataset dataset = DatasetManagement.getDatasetById(datasetId, lang, Language.EN);
			checkSensibleInfo(dataset, (UserInfo) sc.getUserPrincipal());
			return new Dataset(dataset);
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}

	private static eu.iescities.server.accountinterface.datainterface.Dataset checkSensibleInfo(eu.iescities.server.accountinterface.datainterface.Dataset dataset, UserInfo userInfo) {
		if (userInfo.getRoles().contains(UserRoles.IESCITIES_ADMIN) || userInfo.getRoles().contains(UserRoles.COUNCIL_ADMIN)) {
			return dataset;
		} else {
			if (userInfo.getUser() != null) {
				final List<User> councilAdmins = CouncilManagement.getCouncilAdmins(dataset.getCouncilId(), 0, 1000);
				for (User user : councilAdmins) {
					if (user.getUserId() == userInfo.getUser().getUserId()) {
						return dataset;
					}
				}
			}
			dataset.setJsonMapping(null);
			return dataset;
		}
	}

	public static List<eu.iescities.server.accountinterface.datainterface.Dataset> checkSensibleInfo(List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets, UserInfo userInfo) {
		//Remove dataset sensible information (i.e json mapping)
		for (eu.iescities.server.accountinterface.datainterface.Dataset dataset : datasets) {
			checkSensibleInfo(dataset, userInfo);
		}
		return datasets;
	}

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Find some matching dataset",
		notes = "The search string is split into words and each is matched against dataset names and descriptions (OR-connected), case-insensitively. Mapping information is only available for dataset owners or global administators.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Dataset> searchDataset(@Context SecurityContext sc,
			@ApiParam(value = "keywords", required = true) @QueryParam("keywords") String keywords,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false)
			@DefaultValue("1000") @QueryParam("limit") int limit,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets = DatasetManagement.findDataSets(keywords, offset, limit, lang, Language.EN);
			datasets = checkSensibleInfo(datasets, (UserInfo) sc.getUserPrincipal());
			return createDatasets(datasets);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	public static List<Dataset> createDatasets(List<eu.iescities.server.accountinterface.datainterface.Dataset> dbDatasets) {
		final List<Dataset> datasets = new ArrayList<Dataset>();
		for (eu.iescities.server.accountinterface.datainterface.Dataset ds : dbDatasets) {
			datasets.add(new Dataset(ds));
		}
		return datasets;
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all datasets", notes = "Retrieves all the registered datasets. Mapping information is only available for dataset owners or global administators.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Limit value is invalid (MAX = 1000)") })
	public List<Dataset> getAllDatasets(@Context SecurityContext sc,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@DefaultValue("EN") @QueryParam("lang") String langStr)
			throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			List<eu.iescities.server.accountinterface.datainterface.Dataset> datasets = DatasetManagement.findDataSets("", offset, limit, lang, Language.EN);
			datasets = checkSensibleInfo(datasets, (UserInfo) sc.getUserPrincipal());
			return createDatasets(datasets);
		} catch (IllegalArgumentException e) {
			throw new InvalidRequestException(e.getMessage());
		}
	}

	@GET
	@RolesAllowed({ UserRoles.COUNCIL_ADMIN,
		UserRoles.IESCITIES_ADMIN })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{datasetid}/mapping")
	@ApiOperation(value = "Get dataset mapping",
		notes = "Only accesible form council admins or IES-Cities admin users")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Dataset not found") })
	public DataSourceConnector getJsonMapping(@Context SecurityContext sc,
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetID) throws IESCitiesException {
		try {
			final eu.iescities.server.accountinterface.datainterface.Dataset dataset = DatasetManagement.getDatasetById(datasetID, Language.EN, null);
			final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
			if (userInfo.getRoles().contains(UserRoles.IESCITIES_ADMIN)
				|| (userInfo.getRoles().contains(UserRoles.COUNCIL_ADMIN)
						&& userInfo.getUser().getManagedCouncilId() == dataset.getCouncilId())) {
				final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
				return connector;
			} else {
				throw new UnauthorizedErrorException("User not authorized to perform operation");
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (DataSourceConnectorException e) {
			throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{datasetid}/info")
	@ApiOperation(value = "Gets the dataset type")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Dataset not found") })
	public SchemaInfo getSchemaInfo(
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetID) throws IESCitiesException {
		try {
			final eu.iescities.server.accountinterface.datainterface.Dataset dataset = DatasetManagement.getDatasetById(datasetID, Language.EN, null);
			final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
			
			final SQLQueriable queriable = QueriableFactory.createQueriable(connector, new Integer(datasetID).toString(), false);
			
			final List<String> tables = new ArrayList<String>();
			final ResultSet rs = queriable.getTables();
			for (final Row row : rs.getRows()) {
				tables.add(row.getValue("Tables").toString());
			}

			final SchemaInfo schemaInfo = new SchemaInfo(connector.getType(), tables);

			return schemaInfo;
		}  catch (DataSourceSecurityManagerException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (DatabaseCreationException | DataSourceManagementException | QueriableFactoryException e) {
			throw new UnexpectedErrorException(e.getMessage());
		} catch (DataSourceConnectorException e) {
			throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage());
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{datasetid}/schema")
	@ApiOperation(value = "Gets the dataset's schema information")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Dataset not found") })
	public DataSourceInfo getDatasetInfo(
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetID,
			@DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final eu.iescities.server.accountinterface.datainterface.Dataset dataset = DatasetManagement.getDatasetById(datasetID,  Language.EN,  Language.EN);
			final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
			final SQLQueriable queriable = QueriableFactory.createQueriable(connector, new Integer(datasetID).toString(), false);

			final DataSourceInfo datasetInfo = queriable.getInfo();
			return datasetInfo;
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		} catch (DatabaseCreationException | DataSourceManagementException e) {
			throw new UnexpectedErrorException(e.getMessage());
		} catch (DataSourceSecurityManagerException | QueriableFactoryException e) {
			throw new UnauthorizedErrorException(e.getMessage());
		} catch (DataSourceConnectorException e) {
			throw new UnexpectedErrorException("Invalid connector mapping. " + e.getMessage()); 
		}
	}

	@GET
	@Path("/{datasetid}/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the applications related to a dataset", notes = "Gets the applications related to a dataset")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Invalid data supplied"),
			@ApiResponse(code = 404, message = "Dataset not found") })
	public List<Application> getDatasetApps(
			@ApiParam(value = "Dataset id", required = true) @PathParam("datasetid") int datasetId,
			@ApiParam(value = "Offset for pagination purposes", required = false) @DefaultValue("0") @QueryParam("offset") int offset,
			@ApiParam(value = "Limit for pagination purposes", required = false) @DefaultValue("1000") @QueryParam("limit") int limit,
			@ApiParam(value = "Retrieved information lenguage. Fallacks to English if not available.", required = false) @DefaultValue("EN") @QueryParam("lang") String langStr) throws IESCitiesException {
		try {
			final Language lang	= Language.valueOf(langStr.toUpperCase());
			final eu.iescities.server.accountinterface.datainterface.Dataset dataset = DatasetManagement.getDatasetById(datasetId, lang, Language.EN);
			try {
				return DatasetManagement.getAppsUsingDataSet(
						dataset.getDatasetId(), offset, limit, lang, Language.EN);
			} catch (IllegalArgumentException e) {
				throw new InvalidRequestException(e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			throw new EntityNotFoundException(e.getMessage());
		}
	}
}
