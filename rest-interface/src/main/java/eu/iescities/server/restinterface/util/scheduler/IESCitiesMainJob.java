package eu.iescities.server.restinterface.util.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.util.scheduler.MainJob;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;

public class IESCitiesMainJob extends MainJob {

	@Override
	public void checkJobs(Scheduler sch) {
		final List<String> availableMappings = new ArrayList<String>();

		try {
			try {
				final List<Dataset> datasets = DatasetManagement.getAllDataSets(0, 1000, Language.EN, null);
				for (final Dataset dataset : datasets) {
					if (!dataset.getJsonMapping().isEmpty()) {
						final String dataSourceID = "" + dataset.getDatasetId();
						try {
							final DataSourceConnector connector = ConnectorFactory.load(dataset.getJsonMapping());
							if (isValidConnector(connector)) {
								availableMappings.add(dataSourceID);
								try {
									if (!isRunning(dataSourceID, sch)) {
										try {
											createDatasourceJob(dataSourceID, connector, sch);
										} catch (SchedulerException | IOException e) {
											DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.ERROR, "Problem creating job. " + e.getMessage());
										}
									}
								} catch (SchedulerException e) {
									DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.ERROR, "Problem checking running job for datasource. " + e.getMessage());
								}
							}
						} catch (DataSourceConnectorException e) {
							DatasetStatusManager.getInstance().updateStatus(dataSourceID, Status.ERROR, "Problem loading connector. " + e.getMessage());
						}
					}
				}
			} catch (DatasetStatusManagerException e) {
				System.out.println("Could not store database status. " + e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Problem obtaining registered datasets from IES Cities database");
		}
		
		try {
			stopJobs(availableMappings, sch);
		} catch (SchedulerException e) {
			System.out.println("Unexpected problem stopping running jobs.");
		}
	}
}
