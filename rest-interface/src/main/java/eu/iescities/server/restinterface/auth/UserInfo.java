package eu.iescities.server.restinterface.auth;

import java.util.ArrayList;
import java.util.List;

import javax.management.remote.JMXPrincipal;

import eu.iescities.server.accountinterface.datainterface.User;

@SuppressWarnings("serial")
public class UserInfo extends JMXPrincipal {
 
	private String sessionId;
	private User user;
	private String pass;
	
	private List<String> userRoles;
	
	public UserInfo() {
		super("Anonymous");
		this.user = null;
		this.pass = "";
		this.sessionId = "";
		this.userRoles = new ArrayList<String>();
	}
	
	public UserInfo(User user, String pass, String sessionId) {
		super(user.getName());
		this.user = user;
		this.pass = pass;
		this.sessionId = sessionId;
		this.userRoles = createRoles();
	}
	
	public User getUser() {
		return user;
	}
	
	public String getPass() {
		return pass;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	private List<String> createRoles() {
		final List<String> userRoles = new ArrayList<String>();
		
		userRoles.add(UserRoles.REGISTERED_USER);
		
		if (getUser().isAdmin()) {
			userRoles.add(UserRoles.IESCITIES_ADMIN);
		}
		
		if (getUser().getManagedCouncilId() != -1) {
			userRoles.add(UserRoles.COUNCIL_ADMIN);
		}
		
		if (getUser().isDeveloper()) {
			userRoles.add(UserRoles.APP_DEVELOPER);
		}
		
		return userRoles;
	}
	
	public List<String> getRoles() {
		return userRoles;
	}

}
