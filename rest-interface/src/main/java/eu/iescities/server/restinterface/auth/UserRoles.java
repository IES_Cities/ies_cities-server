package eu.iescities.server.restinterface.auth;

public class UserRoles {

	public static final String IESCITIES_ADMIN = "ADMIN";
	public static final String COUNCIL_ADMIN = "COUNCIL_ADMIN";
	public static final String APP_DEVELOPER = "APPLICATION_DEVELOPER";
	public static final String REGISTERED_USER = "REGISTERED_USER";
}
