package eu.iescities.server.restinterface.exception;

import javax.ws.rs.core.Response.Status;

@SuppressWarnings("serial")
public class UnauthorizedErrorException extends IESCitiesException {

	public UnauthorizedErrorException(String message) {
		super(Status.FORBIDDEN, message); 
	}
}
