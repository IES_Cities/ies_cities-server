package eu.iescities.server.restinterface.auth;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;

@PreMatching
public class SecurityRequestFilter implements ContainerRequestFilter {

	@Override
    public void filter(final ContainerRequestContext requestContext) throws IOException {
		String auth = requestContext.getHeaderString("Authorization");
		
		UserSecurityContext securityContext = new UserSecurityContext(auth);
		
        requestContext.setSecurityContext(securityContext);
    }
}
