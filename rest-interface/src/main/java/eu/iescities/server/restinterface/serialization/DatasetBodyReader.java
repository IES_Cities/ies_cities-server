package eu.iescities.server.restinterface.serialization;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;

@Provider
@Consumes("application/json")
public class DatasetBodyReader implements MessageBodyReader<Dataset> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)
				&& type == Dataset.class;
	}

	@Override
	public Dataset readFrom(Class<Dataset> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		
		final BufferedReader reader = new BufferedReader(new InputStreamReader(entityStream));
        final StringBuilder strBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            strBuilder.append(line);
        }
        
		final Gson gson = new GsonBuilder()
			.registerTypeAdapter(DataSourceConnector.class, new DataSourceConnectorAdapter())
			.setDateFormat("dd/MM/yyyy HH:mm:ss")
			.create();
		
		final Dataset dataset = gson.fromJson(strBuilder.toString(), Dataset.class);
		return dataset;
	}
}
