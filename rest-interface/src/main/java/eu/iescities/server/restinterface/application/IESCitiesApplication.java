package eu.iescities.server.restinterface.application;

import java.io.IOException;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.quartz.SchedulerException;

import eu.iescities.server.datawrapper.controller.SocialController;
import eu.iescities.server.loggingrest.AppLogging;
import eu.iescities.server.loggingrest.DatasetLogging;
import eu.iescities.server.loggingrest.RatingLogging;
import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import eu.iescities.server.restinterface.auth.SecurityRequestFilter;
import eu.iescities.server.restinterface.cors.CrossOriginResourceSharingFilter;
import eu.iescities.server.restinterface.util.config.Config;
import eu.iescities.server.restinterface.util.scheduler.IESCitiesMainJob;
import eu.iescities.server.restinterface.util.swagger.SwaggerInitializer;

@ApplicationPath("/api")
public class IESCitiesApplication extends ResourceConfig {

	public IESCitiesApplication() {
		System.out.println("Starting IES Cities server");
		
		packages("eu.iescities.server.restinterface");
		
		register(CrossOriginResourceSharingFilter.class);
        register(RolesAllowedDynamicFeature.class);
        register(SecurityRequestFilter.class);
		
		//TODO: This should be done by automatic package scanning. But it is not working.
		register(AppLogging.class); 
		register(DatasetLogging.class);
		register(RatingLogging.class);
		
		register(SocialController.class);
		
		//Swagger
		register(com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);        
        register(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
        register(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
        register(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.class);
        
        register(SwaggerInitializer.class);
        
        try {
        	final Config config = Config.getInstance();
        	
        	eu.iescities.server.querymapper.config.Config.getInstance().setDataDir(config.getDataDir());
        	eu.iescities.server.datawrapper.config.Config.getInstance().setProperties(config.getProperties());
        	
        	if (eu.iescities.server.querymapper.config.Config.getInstance().checkDataDir()) {
    	        try {
    				QuartzScheduler.getInstance().start(IESCitiesMainJob.class);
    			} catch (SchedulerException e) {
    				e.printStackTrace();
    			}
            } else {
            	System.out.println("Could not start scheduler. Data dir '" + config.getDataDir() + "' not found.");
            }
        } catch (IOException e) {
        	System.out.println(e.getMessage());
        }
    }
}