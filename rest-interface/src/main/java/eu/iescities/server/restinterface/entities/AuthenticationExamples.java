package eu.iescities.server.restinterface.entities;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import eu.iescities.server.restinterface.auth.UserRoles;
import eu.iescities.server.restinterface.serialization.MessageResponse;

@Path("/auth")
@Api(value = "/auth", description = "Operations for authentication testing")
@PermitAll
public class AuthenticationExamples {

	@GET
	@Path("/test/any/hello")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Says hello", notes = "Used for authentication demostration purposes. Anonymous users are authorized")
	public MessageResponse sayHello(@Context SecurityContext sc) {
		MessageResponse response = new MessageResponse();
		response.setResponse("hello, anonymous user");
		return response;
	}

	@GET
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/test/user/hello")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Says hello", notes = "Used for authentication demostration purposes. Only registered users are authorized")
	public MessageResponse sayHelloUser(@Context SecurityContext sc) {
		MessageResponse response = new MessageResponse();
		response.setResponse("hello, registered user");
		return response;
	}

	@GET
	@RolesAllowed(UserRoles.IESCITIES_ADMIN)
	@Path("/test/admin/hello")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Says hello", notes = "Used for authentication demostration purposes. Only IES-Cities admin users are authorized")
	public MessageResponse sayHelloAdmin(@Context SecurityContext sc) {
		MessageResponse response = new MessageResponse();
		response.setResponse("hello, you are an IESCities admin");
		return response;
	}

	@GET
	@RolesAllowed(UserRoles.COUNCIL_ADMIN)
	@Path("/test/council/hello")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Says hello", notes = "Used for authentication demostration purposes. Only council admin users are authorized")
	public MessageResponse sayHelloCouncil(@Context SecurityContext sc) {
		MessageResponse response = new MessageResponse();
		response.setResponse("hello, you are a council admin");
		return response;
	}

	@GET
	@RolesAllowed(UserRoles.APP_DEVELOPER)
	@Path("/test/developer/hello")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Says hello", notes = "Used for authentication demostration purposes. Only application developeres are authorized")
	public MessageResponse sayHelloDeveloper(@Context SecurityContext sc) {
		MessageResponse response = new MessageResponse();
		response.setResponse("hello, you are an application developer");
		return response;
	}
}
