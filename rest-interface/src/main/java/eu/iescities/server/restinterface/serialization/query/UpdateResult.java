package eu.iescities.server.restinterface.serialization.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="UpdateResult")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={
		"rows"
	})
public class UpdateResult {

	private int rows;
	
	public UpdateResult() {
		
	}
	
	public UpdateResult(int rows) {
		this.rows = rows;
	}
	
	public int getRows() {
		return rows;
	}
}
