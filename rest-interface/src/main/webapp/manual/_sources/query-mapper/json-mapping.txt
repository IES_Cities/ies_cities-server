JSON datasource mapping
========================

This mapping type is used to connect with datasources provided in JSON format. The
platform scans the JSON data and creates a relational view of the same data which can be
queried using SQL. 

The following code shows the general format of a mapping which connects with a JSON
datasource.

.. code-block:: json

   {
      "mapping": "json",
      "uri": "someuri",
      "root": "someattr",
      "key": "someattr",
      "refresh": 1000
   }

mapping
   This attribute must contain the value ``json``.

uri
   The uri of the JSON file to be used as the datasource. Files can be referenced
   using local system routes.

root
   Defines the attribute where the mapping starts. Some JSON files contain headers which
   do not required to be mapped to the relational view. The mapper expects the specified
   attribute to contain a list of objects with the same structure.

key
   Selects the attribute from the set of mapped objects to be used as the primary key for
   the main table. If the objects do not contain an attribute which can be used as the
   primary key, it is possible to ask the mapper to generate an ad hoc identifier using
   the "_id" value for this attribute.

refresh
   Some JSON datasources can change periodically. This attribute defines the interval to
   retrieve the new data and update the internal structures.


.. _json_mapping_example:

Mapping example
---------------

This example shows how to map the following JSON datasource using the query mapper.

.. code-block:: json

   {
      "header": "somevalue",
      "results": [
         {
            "key1": "value1",
            "key2": "value2",
            "key3": [
                  {
                     "key1": "value3",
                     "key2": "value4",
                     "key3": [
                              "value5",
                              "value6"
                           ]
                  },
                  {
                     "key1": "value7",
                     "key2": "value8",
                     "key3": [
                              "value9",
                              "value10"
                           ]
                  }
            ]
         }
      ],
      "date": "10-1-2015"
   }


The previous JSON datasource can be connected using the following mapping description.
We suppose that the file is located in the route specified by the ``uri`` attribute.

.. code-block:: json

   {
      "mapping": "json",
      "uri": "http://someuri/data.json",
      "root": "results",
      "key": "_id",
      "refresh": 86400
   }

The ``root`` attribute specifies that the mapping from JSON objects to relational tables
must start from the *results* attribute, which contains the real data to be mapped,
meaning that the other attributes are ignored. 

As can be seen in the datasource used in this example, the *results* attribute contains a
list of objects defining the same group of attributes. This attributes are scanned and
a relational table is created to represent the data their contain. In this case, the
mapping description specifies that this table must contain an ad hoc identifier. 

The table will be named as the root element, in this case ``results``, and it will contain
the following attributes:

=======  ======   =======
_id      key1     key2
=======  ======   =======
1        value1   value2
=======  ======   =======

The ``_id`` column is added by the mapper and their ids are automatically generated. The
``key1`` and ``key2`` columns are obtained from the JSON datasource structure and their
values are copied from each object to a new inserted row.

The attribute ``key3`` contains a list of objects that is mapped to a new table
that is connected to the previous one using primary key the ``results`` table.

=========   ======   =======  ======
parent_id   _id      key1     key2
=========   ======   =======  ======
1           1        value3   value4
1           2        value7   value8
=========   ======   =======  ======

The ``parent_id`` columns contains the identifier of the object from the connected table
that the entries belong to, while the ``_id`` column contains an ad hoc identifier for
each entry inserted in this table. The name of this new table is generated automatically
using the parent table name and the connecting property name. In this case, the this table
will be named ``results_key3``.

The process is applied again to any other attribute containing a list of entries. In the
case of this example, it will generate a third table named ``results_key3_key3`` with the
following structure and data.

=========   ======   =======
parent_id   _id      key3   
=========   ======   =======
1           1        value5 
1           2        value6 
2           3        value9
2           4        value10 
=========   ======   =======
