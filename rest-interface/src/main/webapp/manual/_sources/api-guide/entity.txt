Entity API
==========

This section of the API contains the methods related with the management of the basic
entities including: users, councils, datasets, geographical scopes and applications.

.. note::

    For more information about how to use the following examples see :ref:`using_jquery`.
    For examples of how to access general methods see the :doc:`intro` section.

Councils
--------

Councils can be managed using the methods provided by the platform for each entity type.
Councils are described using the following JSON format, containing the council's generic
data and its associated geographical information.
The following code provides an example of a JSON serialized council object.


.. code-block:: json

      {
        "councilId": 2273,
        "name": "Zaragoza",
        "description": "Ayuntamiento de Zaragoza",
        "geographicalScope": {
          "id": 3075,
          "name": "Zaragoza Region",
          "northEastGeoPoint": {
            "latitude": 41.6894079,
            "longitude": -0.8427317999999999
          },
          "southWestGeoPoint": {
            "latitude": 41.6139746,
            "longitude": -0.9472301000000001
          }
        }
      }

The API contains ``GET`` methods to obtain data related with the passed council
identifier.

    /entities/councils/{councilid}/apps
        Obtains the list of all applications registered within a council.

    /entities/councils/{councilid}/datasets
        Returns the list of datasets associated with the passed council identifier.

    /entities/councils/{councilid}/users
        Gets the list of users who have the council as their preferred option.


Registering councils
^^^^^^^^^^^^^^^^^^^^

Councils can be registered into the platform using the following method that is
only accesible to registered ``admin`` users.

/entities/councils
     This method receives a JSON object as the ``POST`` data containing the description of
     the new registered dataset.

    .. code-block:: javascript

            function make_basic_auth(user, password) {
              var tok = user + ':' + password;
              var hash = window.btoa(tok);
              return "Basic " + hash;
            }

            API_BASE = 'http://localhost:8080/IESCities/api';

            var council = {
                "name": "new council's name",
                "description": "a more detailed description",
                "geographicalScope": {
                    "name": "Some new region",
                    "northEastGeoPoint": {
                      "latitude": 41.6894079,
                      "longitude": -0.8427317999999999
                    },
                    "southWestGeoPoint": {
                      "latitude": 41.6139746,
                      "longitude": -0.9472301000000001
                    }
                  }
            }

            var request = $.ajax ({
              url: API_BASE + '/entities/councils',
              type: "POST",
              data: JSON.stringify(council),
              // adding authentication header
              beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', make_basic_auth('admin', 'secret'));
              },
              contentType: 'application/json',
              });

            request.done(function(json) {
                console.log(JSON.stringify(json));
            });

            request.fail(function( jqXHR, textStatus ) {
              console.log('Council name already registered');
            });


.. _council_admins:

Council admins
^^^^^^^^^^^^^^

Council administrators can be managed by global admins the others council's administrators
using the following method.

/entities/councils/{councilid}/admins
    This method accessed using ``GET`` returns the list of users authorised as administrators
    of the selected councilid. The method JSON list containing user objects filled with
    their *id*, *name*, surname and other options such as *isAdmin* and *isDeveloper*. The
    following code zzshows an response sample

    .. code-block:: json

        [
          {
            "admin": false,
            "developer": false,
            "managedCouncilId": -1,
            "name": "John",
            "preferredCouncilId": -1,
            "profile": "",
            "surname": "Smith",
            "userId": 23996
          }
        ]


    The previous URL can also be used to add new administrators to the council. In this
    case, the method must be invoked as a ``PUT`` method that receives the description of
    the user to add. The user and the council must be previously exist. The following code
    shows an example of how to perform this query.

    .. code-block:: javascript

        function make_basic_auth(user, password) {
          var tok = user + ':' + password;
          var hash = window.btoa(tok);
          return "Basic " + hash;
        }

        API_BASE = 'http://localhost:8080/IESCities/api';

        var user = {
            "userId": 23995
        }

        var request = $.ajax ({
          url: API_BASE + '/entities/councils/2387/admins',
          type: "PUT",
          data: JSON.stringify(user),
          // adding authentication header
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', make_basic_auth('bob', 'secret'));
          },
          contentType: 'application/json',
          });

        request.done(function(json) {
            console.log(JSON.stringify(json));
        });

        request.fail(function( jqXHR, textStatus ) {
          console.log('Council name already registered');
        });

Applications
------------

This section describes actions that can be applied to applications registered into the
platform. The API contains methods to add new applications and to update or delete
existing ones, following the structure for other entities that is explained in the
:doc:`intro`.

The following code shows an example of a JSON object describing an application.

.. code-block:: json

    {
    "accessNumber": 0,
    "appId": 4030,
    "belongsTo": {
      "councilId": 2986,
      "name": "Zaragoza",
      "description": "Ayuntamiento de Zaragoza",
      "geographicalScope": {
        "id": 3999,
        "name": "Zaragoza Region",
        "northEastGeoPoint": {
          "latitude": 41.6894079,
          "longitude": -0.8427317999999999
        },
        "southWestGeoPoint": {
          "latitude": 41.6139746,
          "longitude": -0.9472301000000001
        }
      }
    },
    "description": "The application will allow citizens to send complaints and suggestions about public services, as well as report problems detected in the public space. This will be done via web, or mobile, and it will be possible to include the geographical coordinates of the incident and a photograph. The city council, in return, will inform the citizens about the status of the incident (fixed, on going or pending).",
    "downloadNumber": 0,
    "geographicalScope": {
      "id": 3999,
      "name": "Zaragoza Region",
      "northEastGeoPoint": {
        "latitude": 41.6894079,
        "longitude": -0.8427317999999999
      },
      "southWestGeoPoint": {
        "latitude": 41.6139746,
        "longitude": -0.9472301000000001
      }
    },
    "googlePlayRating": 0,
    "image": "https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-zaragoza-complaints-logo-1172407584-4_avatar.png",
    "name": "Your opinion matters (Complaints & Suggestions)",
    "packageName": "es.zgz.apps.complaints",
    "permissions": "INTERNET, WRITE_EXTERNAL_STORAGE, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, CAMERA, VIBRATE",
    "tags": [
      "zaragoza",
      "opinion",
      "suggestions",
      "complaints",
      "photo",
      "smart city",
      "311"
    ],
    "termsOfService": "Terms of Service for Complaints & Suggestions",
    "url": "https://play.google.com/store/apps/details?id=es.zgz.apps.complaints",
    "lang": "EN",
    "version": "1.0.6"
  }

Searching apps
^^^^^^^^^^^^^^

The API provides two different methods to search for applications: based on keywords and
based on location.

When adding new applications only the following fields must be provided in the posted
JSON object.

.. code-block:: json

    {
      "accessNumber": 0,
      "appId": 4030,
      "belongsTo": {
        "councilId": 2986,
      },
      "description": "some description",
      "geographicalScope": {
        "id": 3999,
      },
      "image": "http://some.image.com/url",
      "name": "Application name",
      "packageName": "google.package.name",
      "permissions": "ANDROID PERMISSION LIST",
      "tags": [
        "tag1",
        "tag2",
        "tag3"
      ],
      "termsOfService": "Terms of Service for the app",
      "url": "https://play.google.com/store/apps/details?id=app.pkg",
      "version": "1.0.0"
    }


/entities/apps/search/
    This method performs a search in the set of all registered applications and returns
    a list containing those ones matching the specified string. The string is compared
    with all the text based fields of each application object: name, description, etc.

    The method is a ``GET`` method and requires to pass the searched keywords as an URL
    parameter ``/entities/apps/search/?keywords=string``.


/entities/apps/near/{lat},{long}/{radius}
    This method enables to search using a central point specified by its lat,long
    coordinates and a radius in kms from this central point. The method returns a list
    of applications that are located inside the specified area.

App developers
^^^^^^^^^^^^^^

The application API contains specific methods to manage the list of developers associated
with each registered application.

/entities/apps{appid}/developers
    This URL can be used as a ``GET`` method to obtain the list of the users currently
    registered as developers of the specified application.

    The same URL can also be used as a ``PUT`` method to add existing users as developers
    of an application. The method receives a JSON object containing the id of an existing
    user to be added as a developer. For a similar example that adds an existing user as
    a council administrator see section :ref:`council_admins`.

Managing datasets
^^^^^^^^^^^^^^^^^

Each application can have a list of associated datasets that are used or related to it.
The following URL provides the functionality to manage an application's datasets.

/entities/apps/{appid}/datasets
    This URL can be used as a ``GET`` method to obtain the list of the datasets registered
    as related with the current application.

    The previous URL can also be used as a ``PUT`` method that enables to add new datasets
    to the

    .. code-block:: javascript

        function make_basic_auth(user, password) {
          var tok = user + ':' + password;
          var hash = window.btoa(tok);
          return "Basic " + hash;
        }

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = {
            "datasetId": 23995
        }

        var request = $.ajax ({
          url: API_BASE + '/entities/apps/4343/datasets',
          type: "PUT",
          data: JSON.stringify(dataset),
          // adding authentication header
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', make_basic_auth('bob', 'secret'));
          },
          contentType: 'application/json',
          });

        request.done(function(json) {
            console.log("Dataset added");
        });

Geographical scopes
-------------------

A geographical scopes enables to specify zones related to councils or applications. A

.. code-block:: json

        {
          "id": 3075,
          "name": "Region name",
          "northEastGeoPoint": {
            "latitude": 41.6894079,
            "longitude": -0.8427317999999999
          },
          "southWestGeoPoint": {
            "latitude": 41.6139746,
            "longitude": -0.9472301000000001
          }
        }

There are methods to add, delete and modify geoscope information. These methods follow
the structure for other entities that is explained in the :doc:`intro`.

/entities/geoscopes/search/
  This method performs a search in the set of all registered geocopes and returns
  a list containing those ones matching the specified string. The string is compared
  with all the text based fields of each application object: name, description, etc.

  The method is a ``GET`` method and requires to pass the searched keywords as an URL
  parameter ``/entities/geoscopes/search/?keywords=string``.

/entities/geoscopes/covering/{lat},{long}
  This ``GET`` method returns those geoscopes that cover the specified geopoint coordinates.
  The geopoint coordinates must be provided in the URL using a string with the {lat},{long}
  format.


Users
-----

The API provides methods to manage the registered users. The methods provide the basic
functionality to add, remove and update users. See :doc:`intro` for examples of the
conventions applied for these methods.

The following code shows an example of a user entity serialized to JSON.

.. code-block:: json

  {
    "admin": false,
    "developer": false,
    "email": "",
    "managedCouncilId": -1,
    "name": "",
    "preferredCouncilId": -1,
    "profile": "",
    "sessionExpires": "2015-02-17T14:46:25.906+01:00",
    "surname": "",
    "userId": 29187,
    "username": "user"
  }

Installed apps
^^^^^^^^^^^^^^

There exist methods that enable to register which applications have been installed by the
current user in order to track the usage of the applications.

/entities/users/me/installed
  This method can be used as a ``GET`` method in order to obtain the list of the installed
  applications by the user which is invoking the method. The method can only be invoked if
  registered users and the information passed in the invocation header is used to obtain
  the associated applications.

  In addition, this method can be used as a ``PUT`` method allowing to associate already
  registered applications with the current user. For a similar example see the code
  provided in :ref:`council_admins`.

.. _datasets:

Datasets
--------

The API provides methods to manage the registered datasets, including basic functionality
to add, remove and update datasets. See :doc:`intro` for examples of the conventions
applied used for these methods.

Registering datasets
^^^^^^^^^^^^^^^^^^^^

/entities/datasets
     This method receives a JSON object as the ``POST`` data containing the description of
     the new registered dataset.

    The `jsonMapping` field contains the description of the mapping file which relates the
    registered dataset with the external data source. A description of the available mappings
    formats and usage is provided in :ref:`mappings`.

    In addition, datasets support different permissions that can specified in the mapping
    section and which can be updated through the provided procedure. See :ref:`permissions`
    for more information about the available permissions and how to specify them for each
    dataset.

    .. code-block:: javascript

            function make_basic_auth(user, password) {
              var tok = user + ':' + password;
              var hash = window.btoa(tok);
              return "Basic " + hash;
            }

            API_BASE = 'http://localhost:8080/IESCities/api';

            var dataset = {
               "name": "Monumentos",
               "description": "Catálogo de monumentos como iglesias, ermitas, estatuas, plazas, museos, palacios y otros edificios históricos, etc. de gran valor artístico y/o histórico situados en la ciudad de Zaragoza y sus alrededores. http://www.zaragoza.es/ciudad/risp/detalle_Risp?id=86",
               "jsonMapping": {
                  "mapping": "json",
                  "refresh": 86400,
                  "uri": "http://www.zaragoza.es/georref/json/hilo/ver_Monumento",
                  "root": "features",
                  "key": "_id"
               }
            }

            var request = $.ajax ({
              url: API_BASE + '/entities/datasets',
              type: "POST",
              data: JSON.stringify(dataset),
              // adding authentication header
              beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', make_basic_auth('zaragoza', 'secret'));
              },
              contentType: 'application/json',
              });

            request.done(function(json) {
                console.log(JSON.stringify(json));
            });

            request.fail(function( jqXHR, textStatus ) {
              console.log('Dataset name already registered');
            });

Getting dataset schema
^^^^^^^^^^^^^^^^^^^^^^

The API provides a method to retrieve the information that describes the schema of the
mapped datasource.

/entities/datasets/{datasetid}/schema
    This method returns the schema information of the mapped datasource in a relational
    view containing one entry for each queriable table. The table description contains
    the name of the table and a list of column descriptions, which contain the name of
    the column and its type. In addition, the returned object also contains the type
    of the mapping registered within this dataset. See :ref:`mappings` for more info about
    supported mappings.

    .. code-block:: json

        {
        "tables": [
          {
            "columns": [
              {
                "name": "name",
                "type": "varchar(20)"
              },
              {
                "name": "id",
                "type": "integer"
              }
            ],
            "name": "country"
          },
          {
            "columns": [
              {
                "name": "name",
                "type": "varchar(10)"
              },
              {
                "name": "occupation",
                "type": "varchar(10)"
              },
              {
                "name": "id",
                "type": "integer"
              },
              {
                "name": "born",
                "type": "integer"
              }
            ],
            "name": "people"
          }
        ],
        "type": "database"
      }


Obtaining dataset mapping
^^^^^^^^^^^^^^^^^^^^^^^^^

The `/entities/dataset/{datasetid}` method does not return the json mapping associated
with a dataset, as this method is public and the mapping can contain sensitive information
used to connect with the external datasource.

/entities/datasets/{datasetid}/mapping
    This GET method obtains the mapping information associated with some dataset. It can
    only be called by global admins or council admins which registered the accessed
    dataset.

    The method will return the mapping connecting the dataset with the datasource.
    See :ref:`mappings` for more info about supported mappings. For example, the following
    retrieve mapping connects the registered dataset with an external JSON datasource.

    .. code-block:: json

        {
            "mapping": "json",
            "refresh": 86400, "uri": "http://www.zaragoza.es/buscador/select?q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:(%22Agenda%20Juvenil%22)&rows=1000000&wt=json",
            "root": "response/docs",
            "key": "id"
        }
