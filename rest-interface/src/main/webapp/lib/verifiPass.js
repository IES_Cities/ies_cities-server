function resetPhase2(token,password){
	///reset/password/phase2/{token}/{newpassword}
	url = '/IESCities/api/entities/users/reset/password/phase2/'+token+'/'+password;
	xml = getHTTPObject();
	xml.onreadystatechange = procesarReset;
	
	xml.open('GET',url)	;
	xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//xml.setRequestHeader("Content-type", "application/json");
	
	
	xml.send();	
	return true;	
}

function procesarReset(){
	if (xml.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		if (xml.status == "200"){
			alert ("Password changed!");
			document.location='index.html';
		}
		else {
			alert ("Error: "+xml.responseText);
		}
	}
	
}
