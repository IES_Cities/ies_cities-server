
/*------------------------------------*\
    #DATOS
\*------------------------------------*/ 

var selectCity = sessionStorage.getItem("councilid");
console.log(sessionStorage.getItem("councilid"));
/* Declaration of this variable has been added */
var citySelected;

/* What is this?!? */
function getCity() {

    var getCity = localStorage.getItem("citySelected");

    if (citySelected != true) {
        citySelected = false;
    } else {
        citySelected == true;
    }

}

window.onload = getCity;

var latLong;

/*------------------------------------*\
    GRAFICA
\*------------------------------------
function showGrafica() {

    $("#selectGrafica").show();
    $("#grafica1").show();
    $("#grafica2").show();
    $("#grafica3").show();
    $("#grafica4").show();
    $("#content").hide();
}

*/ 



 var councilid = councilid;
/*------------------------------------*\
    SELECT CITY
\*-----------------------------------*/
function changeCityNew(councilid) {

   

    if (councilid == "217") {
     
        $("#bg_canvas").css({"background-image": "url(./img/backgroundZaragoza.jpg)"});
      
        //city related text
		if (lang == "ES")
			$("#content").load("zaragoza.html");
		else if (lang == "EN")
			$("#content").load("zaragoza_en.html");
		else if (lang == "IT")
			$("#content").load("zaragoza_it.html");
		else
			$("#content").load("zaragoza.html");
        
        /* responsiveReturnButton has been added to the same click listener */
        $("#returnButton, #responsiveReturnButton").click(function(){
			if (lang == "ES")
				$("#content").load("zaragoza.html");
			else if (lang == "EN")
				$("#content").load("zaragoza_en.html");
			else if (lang == "IT")
				$("#content").load("zaragoza_it.html");
			else
				$("#content").load("zaragoza_en.html");
					
		});

    } else if (councilid == "218") {
       
        $("#bg_canvas").css({"background-image": "url(./img/backgroundBristol.jpg)"});
        
		if (lang == "ES")
			$("#content").load("bristol.html");
		else if (lang == "EN")
			$("#content").load("bristol_en.html");
		else if (lang == "IT")
			$("#content").load("bristol_it.html");
        else
			$("#content").load("bristol_en.html");
        //city related text

     
        /* responsiveReturnButton has been added to the same click listener */
        $("#returnButton, #responsiveReturnButton").click(function(){
			if (lang == "ES")
				$("#content").load("bristol.html");
			else if (lang == "EN")
				$("#content").load("bristol_en.html");
			else if (lang == "IT")
				$("#content").load("bristol_it.html");
		});


    } else if (councilid == "219") {
        //alert("Rovereto"); 
        $("#bg_canvas").css({"background-image": "url(./img/backgroundRovereto.jpg)"});
       
        //city related text
        //city related text
		if (lang == "ES")
			$("#content").load("rovereto.html");
		else if (lang == "EN")
			$("#content").load("rovereto_en.html");
		else if (lang == "IT")
			$("#content").load("rovereto_it.html");
        else
			$("#content").load("rovereto_en.html");
        
        /* responsiveReturnButton has been added to the same click listener */
        $("#returnButton, #responsiveReturnButton").click(function(){
			if (lang == "ES")
				$("#content").load("rovereto.html");
			else if (lang == "EN")
				$("#content").load("rovereto_en.html");
			else if (lang == "IT")
				$("#content").load("rovereto_it.html");
		   else
				$("#content").load("rovereto_en.html");
 				
 		});
    } else if (councilid == "220") {
        // alert("Majadahonda");
        $("#bg_canvas").css({"background-image": "url(./img/backgroundMajadahonda.jpg)"});
        
        
        
        
        //city related text
		if (lang == "ES")
			$("#content").load("majadahonda.html");
		else if (lang == "EN")
			$("#content").load("majadahonda_en.html");
		else if (lang == "IT")
			$("#content").load("majadahonda_it.html");
       else
			$("#content").load("majadahonda_en.html");
 
        
        /* responsiveReturnButton has been added to the same click listener */
        $("#returnButton, #responsiveReturnButton").click(function(){
			if (lang == "ES")
				$("#content").load("majadahonda.html");
			else if (lang == "EN")
				$("#content").load("majadahonda_en.html");
			else if (lang == "IT")
				$("#content").load("majadahonda_it.html");
		   else
				$("#content").load("majadahonda_en.html");
	 
		});
 }


    citySelected = true;
    console.log(councilid);

    if (citySelected == true) {

        //$("#bg_canvas").hide();
        $("#header").show();
        //$("#logoCiudad").css({"padding":"3em","margin":"0 auto","margin-top":"3em"});
        $("#banner").hide();
        $("#nav").hide();
        $(".egmenu").hide();
        $("#header a").show();
        $("#returnMenu").show();
        $("#returnMenu").css({"margin-top": "10em"});
//        $("#logoiEsCities").hide();
        $("#userInfo").hide();
        $("#iesCititesTitle").hide();
        $("#userLoggedInfo").hide();
        
        /* Hides or show responsive elements depending on device's resolution */
        if($(window).width() <= 700) {
          $("#responsiveSelectCity").show();
          $("#responsiveReturnButton").show();
          $("#responsiveCity").show();
        } else {
          $("#selectCity").show();
          $("#logoCiudadUser").show();
          $("#returnButton").show();
        }
        
        
    }
    localStorage.setItem("citySelected", citySelected);
    console.log(localStorage.getItem("citySelected"));
    

}






$(document).ready(function() {

  /* if browser's resolution changes adapts responsive elements */
  $(window).resize(function() {
      if(citySelected && $(window).width() <= 700) {
        $("#selectCity").hide();
        $("#returnButton").hide();
        $("#logoCiudadUser").hide();
        $("#responsiveSelectCity").show();
        $("#responsiveReturnButton").show();
        $("#responsiveCity").show();
      } else if(citySelected && $(window).width() > 700) {
        $("#selectCity").show();
        $("#logoCiudadUser").show();
        $("#returnButton").show();
        $("#responsiveSelectCity").hide();
        $("#responsiveReturnButton").hide();
        $("#responsiveCity").hide();
      }
    });
/*------------------------------------*\
    #MAIN MENU
\*------------------------------------*/ 
 

//$("#bg_canvas").hide();
//$("#banner").hide();
//$("#nav").hide();
    $("#selectGrafica").hide();
    $("#modal").hide();
    $("#returnMenu").hide();
//    $("#header a").hide();
//    $("#header").hide();
    $("#grafica1").hide();
    $("#grafica2").hide();
    $("#grafica3").hide();
    $("#grafica4").hide();
    $("#tableList").hide();
//    $("#formulario").hide();
    $("#selectCity").hide();
    /* Hide responsive elements by default */
    $("#responsiveSelectCity").hide();
    $("#responsiveReturnButton").hide();
    $("#responsiveCity").hide();
    $("#logoCiudadUser").hide();
    $("#returnButton").hide();
    $("#userLoggedInfo").hide();
    

    


/*------------------------------------*\
    #MENU
\*------------------------------------*/ 
    menu = $('nav ul');

    $('#menuMobile').on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function() {
        var w = $(window).width();
        if (w > 480 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
    
    
    $('#main-menu > li:has(ul.sub-menu)').addClass('parent');
    $('ul.sub-menu > li:has(ul.sub-menu) > a').addClass('parent');
//
    $('#menu-toggle').click(function() {
      $('#main-menu').slideToggle(300);
      return false;
    });
    
//    $('.sub-menu a, #selectCity').click(function(){
//      $('#main-menu').slideToggle(300);
//      return true;
//    });

     $('#frorum_icon').click(function(){
     window.open("https://www.facebook.com/IESCities", "_blank");
  });

    $(window).resize(function() {
        if ($(window).width() > 700) {
            $('#main-menu').removeAttr('style');
        }
    });
    
  $('.sub-menu .aboutIesCities').click(function(){
      document.getElementById('light').style.display='block';
      document.getElementById('fade').style.display='block'
      $(".modal").load("iesCitites.html");
      });
      
  $('.overlay').click(function(){
      document.getElementById('light').style.display='none';
      document.getElementById('fade').style.display='none'
      });


  $('.termUse').click(function(){
      $('a[href$=".pdf"]').each(function(){
         window.open = $(this).prop('target','_blank'); 
      });
  });

  $('#twitter_icon').click(function(){
     window.open("https://twitter.com/IESCities", "_blank");
  });
  
  $('#facebook_icon').click(function(){
     window.open("https://www.facebook.com/IESCities", "_blank");
  });
   $('#iesCities_icon').click(function(){
     window.open("http://www.iescities.eu/", "_blank");
  });
  

   $('.feeedbackInfo').click(function(){
     window.open("http://www.iescities.eu/", "_blank");
  });


 $('#feedbackInfo a').click(function(){
     window.open("https://docs.google.com/forms/d/1229icOGv947KR59lhYxLgxSsvI0f4SE4yZdxQqIfDTE/viewform ", "_blank");
  });
  
   $('.userGpInfo a').click(function(){
     window.open("https://play.google.com/store/apps/details?id=eu.iescities.player", "_blank");
     alert("click");
  });

 
/*------------------------------------*\
    #GRAFICA CONTENEDOR
\*------------------------------------*/ 
    $('.has-sub').click(function(e) {
        e.preventDefault();
        $(this).parent().toggleClass('tap');
    });

    $('.btn').click(function() {
        $('#chart').load('chart.html');
    });

/*****CLOSE GRAFICA*****/

$('.closeBtn').click(function(){
     $("#selectGrafica").hide();
     $("#content").show();});

/*------------------------------------*\
    #SLIDER MENU
\*------------------------------------*/     

    $("#slideMenuClose").hide();

    $("#signIn").click(function() {
        $("#formulario").toggle({"margin-right": "0"});
    });

    $("#slideMenuUserOpen").click(function() {
       
       $('#dataApp').toggleClass('dataAppActivo');
       $('#formulario').toggleClass('dataLoginActivo');
       return false;
    });
    
/*------------------------------------*\
    RESPONSIVE MENU FUNCTIONALITY
\*------------------------------------*/ 
    
    $("#dataApp #left").click(function() {
       
       $('#dataApp').toggleClass('dataAppActivo');
       $('#formulario').toggleClass('dataLoginActivo');
       return false;
    });
    
/*------------------------------------*\
    #USER
\*------------------------------------*/      

    console.log(sessionStorage.getItem("user"));
    console.log(sessionStorage.getItem("scopes"));
    var user = sessionStorage.getItem("user");
    if (user != null && user != "") {
        var user = JSON.parse(user);
        console.log(user.admin);
       
            
    }

//Ver que ciudad esta seleccionada 	o volver a seleccionar ciudad
    /*   	    
     if (user == null || user ==""){
     $("#nav .button.grafica").show({right:'250px'});
     }else{
     $('.egmenu').hide();
     //$("#nav .button.grafica").hide({right:'250px'});
     //$("#map_canvas").hide();
     $("#banner").hide();
     $("#nav").hide();
     $("#returnMenu").show();
     $("#returnMenu").css({"margin-top":"10em"});
     }
     */

    /* externalized selectCity logic into a function */
    function backToIndex() {
      $("#cityselector").show();
      $("#userInfo").show();
      $("#iesCititesTitle").show();
      $("#userInfo").css({"margin-top": "8em"});
      $("#returnMenu").hide();
      $("#selectCity").hide();
      $("#responsiveSelectCity").hide();
      $("#logoCiudadUser").hide();
      $("#returnButton").hide();
      $("#responsiveReturnButton").hide();
//    $("#header").hide();
//    $("#logoiEsCities").show();
      $("#bg_canvas").css({"background-image": "url(./img/selectCityBackground.jpg)"});
      $('#dataApp').removeClass('dataAppActivo');
      $('#formulario').removeClass('dataLoginActivo');
      /***logged user info***/
      if(user.username != null) {
        $("#userLoggedInfo").show();
        $("#userLogged").html(user.username);
      }
    }

    $("#selectCity").click(function() {
      backToIndex();   
    });

    /* Added responsiveSelectCity click listener */
    $("#responsiveSelectCity").click(function() {
      /* Hides the navication's toggle menu after clicking */
      $("#main-menu").hide();
      /* executes selecCity's button's same code */
      backToIndex();
    });

/*------------------------------------*\
    #PRUEBA GRAFICA
\*------------------------------------*/

    $("#grafica1").click(function() {
        sessionStorage.setItem("grafica", "GRPSUP1");
        // viewUserFlot();
        $("#modal").show();
    });
    $("#grafica2").click(function() {
        sessionStorage.setItem("grafica", "GRPSUP4");
        $("#modal").show();
    });
    $("#grafica3").click(function() {
        sessionStorage.setItem("grafica", "GRPSUP5");
        $("#modal").show();
    });
    $("#grafica4").click(function() {
        $("#modal").show();
    });
    $("#modal .btn").click(function() {
        $("#modal").hide();
        $("#selectGrafica").hide();
        $("#content").show();
    });
    
/*------------------------------------*\
    #GRAFICA CONTENEDOR
\*------------------------------------
    function viewUserFlot() {
        $('#modal').load("chart.html");
        $('#flot-placeholder').show({right: '250px'});
        $('#flot-placeholder').css('width', 250);
        $('#flot-placeholder').css('height', 250);
    }
*/




/*------------------------------------*\
    #LOGIN FORM
\*------------------------------------*/
    $("#signIn_icon").click(function() {
        $("#formulario").show();
    });


//    $("#signUp_icon").click(function() {
//        $("#returnMenu").load("form.html");
//    });


    $("#closeForm").click(function() {
        location.reload();
    });
    

//        console.log(user);

});

$("button #tableList").click(function () {
    
});

/*------------------------------------*\
    #RESET PASSWORD (disable for Alex file fuction)
\*------------------------------------*/
//function  paintPasswordResetForm(){      
//      document.getElementById('light').style.display='block';
//      document.getElementById('fade').style.display='block'
//      $(".modal").load("verifyPass.html");
//     }



  /**dfsdfdf***/
  
 /*
function loadMap(){
if ($('.dvAppGSContent').lenght){
function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(-34.397, 150.644)
  };

  var map = new google.maps.Map(document.getElementById('dvAppGSContent'),
      mapOptions);
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;
}
}
*/
