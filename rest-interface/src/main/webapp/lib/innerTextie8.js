if (Object.defineProperty && Object.getOwnPropertyDescriptor &&
     Object.getOwnPropertyDescriptor(Element.prototype, "text") )
  (function() {
    var text = Object.getOwnPropertyDescriptor(Element.prototype, "text");
    Object.defineProperty(Element.prototype, "innerText",
      { // It won't work if you just drop in innerText.get
        // and innerText.set or the whole descriptor.
        get : function() {
          return text.get.call(this)
        },
        set : function(x) {
          return text.set.call(this, x)
        }
      }
    );
  })();