var languaje ="";
var isadmin = false;
var userconnected= false;
var DEFAULT_SEARCH_VALUE ='a';
var goauth = null;

/**
 * Idioma, primero miramos si tenemos variable en la url.
 * si hay la metemos en session.
 * si no hay consultamos la variable de sesion, si hay la usamos
 * si no hay parametro en la url ni variable de sesion usamos ingles, y lo mentemos en sesion.
 */
var lang = getQueryVariable("lang");
if (lang == "none"){
	lang  = sessionStorage.getItem("lang");
	if (lang == null || lang == ""){
		lang = "EN";		
	}	
}
lang = lang.toUpperCase();
//For fix old values in session storage.
if (lang == 'ENG') lang ='EN';

sessionStorage.setItem("lang",lang);

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  } 
  //alert('lang ' + variable + ' not found');
  return "none";
}
if (lang == "ES")
	languaje = lang_es;
else if (lang == "EN")
	languaje = lang_en;
else if (lang == "IT")
	languaje = lang_it;
else 
	languaje = lang_en;
	
//alert (languaje.greeting);
	
	
/**
 * Load initial data of web, if there are some error, this is printed.
 * 
 * If there are user in session and is iesadmin cities and user management anchors are shown.
 * If is a city manager datasets and app management link are shown, restrinted to the 
 * data of user management city.
 * 
 * @returns {Boolean}
 */
function loadinitialdata(){
	
	//Returns a list with all the registered councils
	loadCities();
	
	loadFeatures();
	//loadCityDatasets();
		//if (document.location.pathname.indexOf('application.html') == -1) 
	//loadCityApps();
	loadGeoScopes();
	//Get the object user of current sessionid, and show the data.
	loadUser();
	
}
/**
 * Gets the userid store it in session and reload the data of the web
 */
function changeCity(councilid){
	//var councilid =  document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
	log("WebCitySelect",""+councilid);
	sessionStorage.removeItem("councilid");
	sessionStorage.setItem("councilid",councilid);
	
	var city = getCouncil(councilid);
	var name  = city.name;
	if (name == "Zaragoza"){
		$("#logoCiudadUser").css({"background-image":"url(./img/logoZaragoza.png)"});
		/* Adds the icon and the name of the city after selectLanguage spam for movile devices */
		$("#responsiveCity").empty().append('<img class="responsiveCityIcon" src="./img/logoZaragoza.png" height="25px">');
		$("#responsiveCity").append('<p class="responsiveCityName">' + city.name + '</p>');
	}
	else if (name == "Bristol"){
		$("#logoCiudadUser").css({"background-image":"url(./img/logoBristol.png)"});
		/* Adds the icon and the name of the city after selectLanguage spam for movile devices */
		$("#responsiveCity").empty().append('<img class="responsiveCityIcon" src="./img/logoBristol.png" height="25px">');
		$("#responsiveCity").append('<p class="responsiveCityName">' + city.name + '</p>')
	}
	else if (name == "Rovereto"){
		$("#logoCiudadUser").css({"background-image":"url(./img/logoRovereto.png)"});
		/* Adds the icon and the name of the city after selectLanguage spam for movile devices */
		$("#responsiveCity").empty().append('<img class="responsiveCityIcon" src="./img/logoRovereto.png" height="25px">');
		$("#responsiveCity").append('<p class="responsiveCityName">' + city.name + '</p>')
	}
	else if (name == "Majadahonda"){
		$("#logoCiudadUser").css({"background-image":"url(./img/logoMajadahonda.png)"});
		/* Adds the icon and the name of the city after selectLanguage spam for movile devices */
		$("#responsiveCity").empty().append('<img class="responsiveCityIcon" src="./img/logoMajadahonda.png" height="25px">');
		$("#responsiveCity").append('<p class="responsiveCityName">' + city.name + '</p>')
	}

	
	loadFeatures();
	loadCityDatasets();
		//if (document.location.pathname.indexOf('application.html') == -1) 
	loadCityApps();
	changeCityNew(councilid);
	//loadUser();
	
}

 

/**
 * Use as callback procesarCities function.
 * @returns Returns a list with all the registered councils
 */
function loadCities(){
	//url = protocol+urlservicio+'api/entities/councils/all';
	url = '/IESCities/api/entities/councils/all';
	xml = getHTTPObject();
	xml.onreadystatechange = procesarCities;
	xml.open('GET',url,false)	;
	xml.send();	
	return true;
}
/**
 * Callback of LoadCities function.
 * @returns {Boolean}
 */
function procesarCities(){
	var result = document.getElementById("cityselector");
	if (xml.readyState == 4){
		if (xml.status == "200"){
			sessionStorage.setItem("allcities",xml.responseText);
			var selectedcity = sessionStorage.getItem("councilid");
			//if xml.responseText ==null means that there are not cities in the system.
			if (xml.responseText == "null"){
				if (document.location.pathname ==path+'index.html' || document.location.pathname == path) loadUser();
				return true;
			}
			var cities= eval('(' + xml.responseText + ')');
			var selected ="";
					
			result.innerText ="";
			//result.innerText = "<form name='frmcity'>";
			
			//result.innerText = result.innerText+" <select name='city' onchange='javascript:changeCity()'>";
         	//<li id="mapZaragoza" value="82"><h2>Zaragoza</h2><a href="#" id="logoZaragoza"   onclick="changeCity()"></a></li>
            //<li id="mapBristol"  value="83"><h2>Bristol</h2><a href="#" id="logoBristol"  onclick="changeCity()"></a></li>
            //<li id="mapRovereto" value="84"><h2>Rovereto</h2><a href="#" id="logoRovereto"  onclick="changeCity()"></a></li>     
            //<li id="mapMajadahonda" value="85"><h2>Majadahonda</h2><a href="#" id="logoMajadahonda"  onclick="changeCity()"></a></li>     

			if (cities == null) result.innerText+"";
			else if (cities.length != null){
				for (data=0;data<cities.length;data++){
					//if (cities[data].councilId == selectedcity) selected ="selected";
					//else selected = "";
					result.innerText = result.innerText+" <li value ='"+cities[data].councilId+"'><h2>"+cities[data].name+"</h2><a href='#'id='"+cities[data].name+"' onclick='javascript:changeCity("+cities[data].councilId+")'></a></li>";
				}
			}
			
			//result.innerText = result.innerText+" </select></form>";
			result.innerHTML =  result.innerText;
		
		//if (document.location.pathname.indexOf('dataset.html') == -1) 
		}

	}
	else{
		result.innerHTML = "Loading...";
		
	}
}
/**
 * Show aditional features of council admin.
 * @returns {Boolean}
 */
function loadFeatures(){
	var responsetext=sessionStorage.getItem("user");
	var selectedcity = sessionStorage.getItem("councilid");
	if (responsetext == "null" || responsetext==null || responsetext == ""){
		return true;
	}
	else{
		user = eval('(' + responsetext + ')');
		userconnected = true;
		var result = document.getElementById("left");
		result.innerText ="";

		if (user.admin){

			result.innerText = "<h1>"+languaje.council_management+"</h1><ul>"+				
				" <li><a href='#' onclick=showCouncilDashBoard()>"+languaje.council_dashboard+"</a></li>"+
				" <li><a href='#' onclick=showEngagemnetCampaings()>"+languaje.engagement_campaigns+"</a></li>"+
				" <li><a href='#' onclick=OpenInNewTab('http://iescities.com:3838/sample-apps/questionnaires/') >In-App Questionnaires Results</a></li>"+
				" <li><a href='#' onclick=OpenInNewTab('http://iescities.com:3838/sample-apps/dashboard/') >Council Apps Dashboard</a></li>"+
				"</ul>"+
				"</br></br>";		
			
			result.innerText = result.innerText+"<h1>"+languaje.admin_management_features+"</h1><ul>"+
			" <li><a href='#' onclick=getIESUsers()>"+languaje.manage_ies_cities_users+"</a></li>"+							
			" <li><a href='#' onclick=showGrafica()>"+languaje.admin_dashboard+"</a></li>"+
				"</ul>"+
				"</br></br>";
		}		
		else if (user.managedCouncilId == selectedcity){
			
			result.innerText = "<h1>"+languaje.council_management+"</h1><ul>"+
				" <li><a href='#' onclick=getCityUsers()>"+languaje.manage_councils_users+"</a></li>"+	
				" <li><a href='#' onclick=showCouncilDashBoard()>"+languaje.council_dashboard+"</a></li>"+
				" <li><a href='#' onclick=showEngagemnetCampaings()>"+languaje.engagement_campaigns+"</a></li>"+
				" <li><a href='#' onclick=OpenInNewTab('http://iescities.com:3838/sample-apps/questionnaires/') >In-App Questionnaires Results</a></li>"+
				" <li><a href='#' onclick=OpenInNewTab('http://iescities.com:3838/sample-apps/dashboard/') >Council Apps Dashboard</a></li>"+
				
				"</ul>"+
				"</br></br>";		
			
		}
		
		
		result.innerText =result.innerText+"<div id='datasets'></div></br></br><div id='apps'></div>";
		result.innerHTML =result.innerText ;
		
	}	
}
function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}
function showCouncilDashBoard(){	
	var selectedcity = sessionStorage.getItem("councilid");	
	$("#content").load("graficasc"+selectedcity+".html?_="+(new Date()).getTime());
}
function showEngagemnetCampaings(){	
	var selectedcity = sessionStorage.getItem("councilid");	
	$("#content").load("graficasenc"+selectedcity+".html?_="+(new Date()).getTime());
}

function showGrafica(){		
	$("#content").load("graficas.html?_="+(new Date()).getTime());
}
/**
 * Tag is a string ',' separated, the problem is that without black spaces the tables 
 * are painted in bad way, so we add blanks.
 * @param tags
 * @returns {String}
 */
function paintTags(tags){
	if (tags == undefined || tags == null) return "";
	var aux="";
	if (tags.length == 1){
		aux= replaceAll(tags[0],',',' ');
	}
	else{
		for (tag=0;tag<tags.length;tag++){
			aux = aux+" "+tags[tag];
		}
	}
	return aux;
}
/**
 * Replace in text, all the ocurrences of 'busca' for 'reemplaza'
 * @param text
 * @param busca
 * @param reemplaza
 * @returns
 */
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}


/**
 * Gets the HttpObject that depends of the browser
 * @returns
 */
function getHTTPObject(){
	if (typeof XMLHttpRequest != 'undefined'){
		return new XMLHttpRequest();
	}
	try{
		return new ActiveXObject("Msxml2.XMLHTTP");
		
	}
	catch (e){
		try{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
			
		}
	}
}
/**
 * Returns the BasicAuth header.
 */
function make_basic_auth(user, password){
	var tok = user + ':' + password;
	var hash = Base64.encode(tok);
	return "Basic " + hash;
}
function getCouncil(councilid){
	
	var councils= eval('(' + sessionStorage.getItem("allcities") + ')');	
	if (councils == null) return new Object();
	else if (councils.length != null){
		for (data=0;data<councils.length;data++){
			if (councils[data].councilId == councilid) 
				return councils[data];
		}
	}
	return new Object();
	
}
function CustomAlert(){
    this.render = function(dialog){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
        dialogbox.style.left = (winW/2) - (550 * .5)+"px";
        dialogbox.style.top = "100px";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxhead').innerHTML = "Alert";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Alert.ok()">OK</button>';
    };
	this.ok = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	};
}
var Alert = new CustomAlert();
