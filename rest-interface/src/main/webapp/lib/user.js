/**
For google singing
*/
 var islogin="";
function toJsonObject(authResult){
	var token = new Object();
	token.id_token = authResult['id_token'];
	token.access_token = authResult['access_token'];
	token.state =authResult['state'];

	token.error = authResult['error'];
	token.error_description = authResult['error_description'];
	token.expires_in =authResult['expires_in'];

	token.authUser = authResult['authUser'];
	token.error_description = authResult['error_description'];
	token.expires_in =authResult['expires_in'];
	
	return token;

}

var loginFinished = function(authResult) {
	if (authResult) {
		console.log(authResult);
		if (authResult['status']['signed_in']) {
			if (!islogin)
				document.getElementById("errorregister").innerText = 'User granted access';
		
			gapi.auth.setToken(authResult);
		} 
		else {
			document.getElementById("errorregister").innerText = 'Access denied: ' + authResult['error'];
		}
		goauth = toJsonObject(authResult);
  

	} 
	else {
		document.getElementById('errorregister').innerHTML = 'Empty authResult';
	}
	if (islogin){
		iesGooglelogin();
	}
	else{
		getGoogleData();
	}
};

 
  var options = {
    'callback' : loginFinished,
    'approvalprompt' : 'force',
    'clientid' : '796687980545-dui5jmcjt5kmt5rb7pkjda5l0splq37s.apps.googleusercontent.com',
    'cookiepolicy' : 'single_host_origin',
    'scope': 'openid profile'
  };
 
  function auth(isauth){   
	islogin = isauth;
	if (!islogin && !document.getElementById('enabledgogle').checked){
		goauth = null;
		return;
	}
	
	gapi.auth.signIn(options);

  }
  /**
   * Login of user using basic auth.
   * Gets the current user information.
   * Use as callback procesarResultado function.
   */
  function iesGooglelogin(){
  	url = '/IESCities/api/entities/users/googlelogin';
  	xml = getHTTPObject();
  	xml.onreadystatechange = procesarResultado;
  	var params = 'tokenstring='+goauth.id_token;
  	xml.open('GET',url+'?'+params);
  	
  	
  	xml.setRequestHeader("Content-type", "application/json");
  	
  	xml.send();
  	return true;	

  }

 
function getGoogleData(){
	url = 'https://www.googleapis.com/oauth2/v1/userinfo'
	xml = getHTTPObject();
	xml.onreadystatechange = procesarGetGoogleData;
	
	var params = 'Bearer '+goauth.access_token;
	xml.open('GET',url);
	xml.setRequestHeader('Authorization', params);
	
	//xml.setRequestHeader("Content-type", "application/json");
	
	xml.send();
	return true;	

}
/**
{
    "id": "106106592086670705712",
    "name": "Alex Rodriguez",
    "given_name": "Alex",
    "family_name": "Rodriguez",
    "link": "https://plus.google.com/106106592086670705712",
    "picture": "https://lh5.googleusercontent.com/-PKIzjK8h2IA/AAAAAAAAAAI/AAAAAAAAAVY/tjFsSmO9jpo/photo.jpg",
    "gender": "male",
    "locale": "es"
}

{
    "error": {
        "errors": [
            {
                "domain": "global",
                "reason": "authError",
                "message": "Invalid Credentials",
                "locationType": "header",
                "location": "Authorization"
            }
        ],
        "code": 401,
        "message": "Invalid Credentials"
    }
}

*/
function procesarGetGoogleData(){
	if (xml.readyState == 4){
		if (xml.status == "200" && xml.responseText!=""){
			var resultGoogle= eval('(' + xml.responseText + ')');
			//goauth.idgoogle = resultGoogle.id;
			document.register.name.value =  resultGoogle.given_name;
			document.register.surname.value =  resultGoogle.family_name;
		}
		else {
			var resultGoogle= eval('(' + xml.responseText + ')');
			document.getElementById("errorregister").innerText = "Error al conectar con google!"//resultGoogle.error.message;
		}
		
	}	
}

/**
 * Login of user using basic auth.
 * Gets the current user information.
 * Use as callback procesarResultado function.
 */
function ieslogin(){
	var auth = make_basic_auth(document.login.username.value,document.login.password.value);
	if (validarLogin()){
		url = '/IESCities/api/entities/users/login';
		xml = getHTTPObject();
		xml.onreadystatechange = procesarResultado;
		xml.open('GET',url)	;
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.setRequestHeader('Authorization', auth);	
		xml.send();
		return true;	
	}
}
/**
 * Validates User and password fields.
 * @returns {Boolean}
 */
function validarLogin(){
	var result = document.getElementById("error");
	if (document.login.username.value == "" ){
		result.innerHTML=languaje.forms_user_required_message;
		Alert.render(languaje.forms_user_required_message);
		return false;
	}
	else if (document.login.password.value== "" || document.login.password.length < 5){
		result.innerHTML=languaje.forms_user_passshort_message;
		Alert.render(languaje.forms_user_passshort_message);
		return false;
	}
	else {
	
		return true;
	}
}
/**
 * Callback of ieslogin function.
 */
function procesarResultado(){
	
	if (xml.readyState == 4){
		//var user= eval('(' + xml.responseText + ')');
		if (xml.status == "200" && xml.responseText!=""){
		//if (xml.status!="500" && xml.status!="403" && xml.responseText!= null && xml.responseText!=""){
			sessionStorage.setItem("user",xml.responseText);
			sessionStorage.setItem("password",document.login.password.value);
			//sessionStorage.removeItem("errorlogin");
			document.location='index.html';
		}
		else {
			var result = document.getElementById("error");
			if (xml.status == 403){
				result.innerHTML=xml.statusText+".";
				Alert.render("Login error.");
			}
			else{
				var respuesta = eval('(' + xml.responseText + ')');
				if (respuesta.hasOwnProperty("message")){
					result.innerHTML=xml.statusText+".";
					Alert.render(message);
				}
				else{
					Alert.render("Login error.");
				}
			}			
			
			//result.innerHTML=languaje.forms_invalid_user_passshort_message+".";
			//sessionStorage.setItem("errorlogin","Invalid user or password.");
		}
		
	}

}


/**
 * Get the object user of current sessionid, and show the data.
 * @returns {Boolean}
 */
function loadUser(){
	return procesarUser();

}
/**
 * si no hay usuario pintamos el formulario de registro.
 * @returns {Boolean}
 */
function procesarUser(){
	var result = document.getElementById("formulario");
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	if (user == null || user == ""){
		paintFrmLogin();
		return true;
	}
	else {// (xml.readyState == 4){
		//var user= eval('(' + xml.responseText + ')');
		
		result.innerText = "<h1>"+languaje.form_user+"</h1>";
		result.innerText = result.innerText+" <ul>";
		if (user == null ) result.innerText+"";

		else {
			var prefereedcouncil = -1;
			var managedcouncil  = -1;
			result.innerText = result.innerText+" <li>"+languaje.forms_name+": "+user.name+"</li>";
			result.innerText = result.innerText+" <li>"+languaje.forms_username+": "+user.username+"</li>";
			result.innerText = result.innerText+" <li>"+languaje.forms_surname+": "+user.surname+"</li>";
			result.innerText = result.innerText+" <li>"+languaje.forms_email+": "+user.email+"</li>";
			result.innerText = result.innerText+" <li>"+languaje.form_admin+": "+user.admin+"</li>";
			//result.innerText = result.innerText+" <li>Has applications: "+user.developer+"</li>";
			result.innerText = result.innerText+" <li>"+languaje.forms_profile+": "+user.profile+"</li>";
			if (user.preferredCouncilId != -1 ){
				prefereedcouncil= getSelectedCouncil(user.preferredCouncilId);
				result.innerText = result.innerText+" <li>"+languaje.frm_admin_pref_council+": "+prefereedcouncil+"</li>";
				changeSelectedCouncil(user.preferredCouncilId);
			}
			if (user.managedCouncilId != -1 ){
				managedcouncil = getSelectedCouncil(user.managedCouncilId);
				result.innerText = result.innerText+" <li>"+languaje.frm_admin_man_council+": "+managedcouncil+"</li>";
				changeSelectedCouncil(user.managedCouncilId);
			}
		}
		result.innerText = result.innerText+" </ul>";
		result.innerText = result.innerText+"<form name='"+languaje.forms_button_login+"' accept-charset='utf-8'>";
		
		if (user!=null){
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_myapps+"' onclick='javascript:getMyApps()'>";
			//result.innerText = result.innerText+"<input type='button' value='My Logs' onclick='javascript:viewLogsUser()'>";
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_change_profile+"' onclick='javascript:paintFrmUpdate()'>";
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_reset_password+"' onclick='javascript:paintPasswordResetForm()'>";
			
		}
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_logout+"' onclick='javascript:logout()'>";
		
 
		result.innerText = result.innerText+"</form>";
		result.innerHTML =result.innerText ;
		
		
		return true;
	}
	//else
		//result.innerHTML = "Loading...";
}
function paintPasswordResetForm(){
	var result = document.getElementById("formulario");
	result.innerText = "<h1>"+languaje.forms_button_reset_password+"</h1>";

	result.innerText = result.innerText+"<form name='reset' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='email' >"+languaje.forms_email+"</label>";
	result.innerText = result.innerText+"<input type='email' name='email' placeholder='email' required>";
	result.innerText = result.innerText+"</p>";	
	/*result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='password'  >Password</label>";
	result.innerText = result.innerText+"<input type='password' name='password' placeholder='password, 5 characters minimum' pattern='.{5,}'  required>";	
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='password'  >Repeat Password</label>";
	result.innerText = result.innerText+"<input type='password' name='rpassword' placeholder='password, 5 characters minimum' pattern='.{5,}'  required>";
	
	result.innerText = result.innerText+"</p>";
	*/
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_reset_password+"' onclick='javascript:resetPasswordPhase1()'>";
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_back+"' onclick='javascript:procesarUser()'>";
	result.innerText = result.innerText+"</form>";
	result.innerHTML =result.innerText ;
	
	
	return true;

}
function resetPasswordPhase1(){
	var email=document.reset.email.value;
	/*var password = document.reset.password.value;
	var rpassword = document.reset.rpassword.value;
	*/
	if (email == null || email ==""){
		Alert.render(languaje.frm_user_error_email);	
		procesarUser();
		return false;
	}
	
	url = '/IESCities/api/entities/users/reset/password/'+email;
	xml = getHTTPObject();
	xml.onreadystatechange = procesarReset;
	
	xml.open('GET',url)	;
	xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//xml.setRequestHeader("Content-type", "application/json");
	
	
	xml.send();	
	return true;	
}
function procesarReset(){
	if (xml.readyState == 4){		
		//var sessionid = user.sessionId;
		if (xml.status == "200"){
			Alert.render (languaje.forms_mail_sent_message);				
			procesarUser();
			
		}
		else {
			var error = eval('(' + xml.responseText + ')');
			if (error == null || error == ""){
				Alert.render(xml.responseText);
				sessionStorage.setItem("errorlogin",xml.responseText);
			}
			else{
				var message = error.message;
				Alert.render(message);
				sessionStorage.setItem("errorlogin",message);
			}
			procesarUser();
			
		}
		
		
	}
	
}
/**
 * The parameter is the id of the city, returns the asigned value.
 */
function  getSelectedCouncil(value){
	var allcities= eval('(' + sessionStorage.getItem("allcities") + ')');
	for(var i=0; i < allcities.length; i++)
    {
      if(allcities[i].councilId == value)
        return allcities[i].name;
    }
}
/**
 * The parameter is the id of a city, (the city of the loged user) we change the city selector in 
 * order to show the data of the preferred city of the user.
 */
function  changeSelectedCouncil(value){
  /*for(var i=0; i < document.frmcity.city.options.length; i++)
    {
      if(document.frmcity.city.options[i].value == value)
        document.frmcity.city.selectedIndex = i;
		changeCity();
		
    }*/
	changeCity(value);
}
/**
 * Logout current user. Use as callback function procesarLogout.
 * @returns {Boolean}
 */
function logout(){
	url = '/IESCities/api/entities/users/logout';
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	if (user == null || user == "") return true;
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	xml = getHTTPObject();
	xml.onreadystatechange = procesarLogout;
	xml.open('GET',url)	;
	xml.setRequestHeader('Authorization', auth);
	xml.send();	
	return true;	
}
/**
 * Get and paint a table with the user developed apps
 * @returns {Boolean}
 */
function getMyApps(){
	url = '/IESCities/api/entities/users/me/developer';
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	if (user == null || user == "") return true;
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	xml = getHTTPObject();
	xml.onreadystatechange = paintMyApps;
	xml.open('GET',url)	;
	xml.setRequestHeader('Authorization', auth);
	xml.send();	
	return true;	
}
/**
 * Paint the table with the applications of an user.
 */
function paintMyApps(){
	var result = document.getElementById("content");
	if (xml.readyState == 4){		
		//var sessionid = user.sessionId;
		if (xml.status!="403" && xml.responseText!= null && xml.responseText!=""){
			sessionStorage.setItem("userapps", xml.responseText);
			var applications= eval('(' + xml.responseText + ')');
			result.innerText = "<h1>"+languaje.frm_user_apps+"</h1>";
			result.innerText = result.innerText+"<table>";
			result.innerText = result.innerText+"<thead>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<th>";
			result.innerText = result.innerText+languaje.forms_name+"</th>";
			result.innerText = result.innerText+"<th>";
			result.innerText = result.innerText+languaje.forms_tags+"</th>";
			result.innerText = result.innerText+"<th>";
			result.innerText = result.innerText+languaje.form_actions+"          </th>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"</thead>";
			result.innerText = result.innerText+"<tbody>";
			if (applications == null ) result.innerText = result.innerText+"";
			else if (applications.length != null){
				for (data=0;data<applications.length;data++){
					result.innerText = result.innerText+"<tr>";
					result.innerText = result.innerText+"<td >"+applications[data].name+"</td>";
					result.innerText = result.innerText+"<td>"+paintTags(applications[data].tags)+"</td>";
					result.innerText = result.innerText+"<td>";
					result.innerText = result.innerText+"<a href='#' onclick=getApps('"+applications[data].appId+"')>"+languaje.forms_button_view+" </a> <a href='#' onclick=paintFrmUpdateApp('"+data+"')>Edit</a> <a href='#' onclick=deleteApplication('"+data+"')>Delete</a>";
					result.innerText = result.innerText+"<a href='#' onclick=viewLogsApp('"+data+"')> "+languaje.forms_button_logs+" </a> ";
					result.innerText = result.innerText+"<a href='#' onclick=showAppGrafica("+applications[data].appId+");> "+languaje.forms_button_graph+" </a> </td>";
					result.innerText = result.innerText+"</tr>";
					
				}
			}
			result.innerText = result.innerText+"</tbody>";
			result.innerText = result.innerText+"</table>";
			result.innerHTML =result.innerText ;			
		}
		else {
			result.innerText = "<h1>"+languaje.frm_user_apps+"</h1>";
			result.innerText = result.innerText+" "+languaje.forms_applications_not_found_message;
			result.innerHTML =result.innerText ;
		}
	}

	
}
function showAppGrafica(appid){		
	$("#content").load("graficas"+appid+".html");
}

function paintFrmUpdate(){
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	paintFrmRegister(user);
}
/**
 * Paint the register form in the content div.
 */
function paintFrmRegister(selecteduser){
	var result = document.getElementById("content");
	goauth = null;
	if (selecteduser ==undefined || selecteduser == ''){

		result.innerText = "<h1>"+languaje.forms_register+"</h1>";
	}
	else{
		result.innerText = "<h1>"+languaje.frm_user_update_user+"</h1>";
		result.innerText = result.innerText+"<p>"+languaje.forms_user_update_message+"</p>";
	}
	var errorregister =sessionStorage.getItem("errorregister");
	
	if (errorregister!=null && errorregister!=""){
		result.innerText = result.innerText+"<div id='errorregister'>"+errorregister+"</div>";
		sessionStorage.removeItem("errorregister");
	}	
	else{
		result.innerText = result.innerText+"<div id='errorregister'></div>";
	}
	result.innerText = result.innerText+"<form name='register' accept-charset='utf-8' >";	
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='username'>"+languaje.forms_username+"</label>";
	result.innerText = result.innerText+"<input type='text' name='username' placeholder='yourname' required>";
	result.innerText = result.innerText+"</p>";
	if (selecteduser == undefined || selecteduser == ''){

		result.innerText = result.innerText+"<p>";
		result.innerText = result.innerText+"<label class='fieldregister' for='password'  >"+languaje.forms_password+"</label>";
		result.innerText = result.innerText+"<input type='password' name='password' placeholder='password' pattern='.{5,}'  required>";
		result.innerText = result.innerText+"<small>5 characters minimum</small>";	
		result.innerText = result.innerText+"</p>";
	}
	else{
		result.innerText = result.innerText+"<p>";
		result.innerText = result.innerText+"<label class='fieldregister' for='password'  >"+languaje.forms_current_password+"</label>";
		result.innerText = result.innerText+"<input type='password' name='currentpassword' placeholder='currentpassword' pattern='.{5,}'>";
		result.innerText = result.innerText+"<small>5 characters minimum</small>";	
		result.innerText = result.innerText+"</p>";
		result.innerText = result.innerText+"<p>";
		result.innerText = result.innerText+"<label class='fieldregister' for='password'  >"+languaje.forms_new_password+"</label>";
		result.innerText = result.innerText+"<input type='password' name='password' placeholder='password' pattern='.{5,}'>";
		result.innerText = result.innerText+"<small>5 characters minimum</small>";	
		result.innerText = result.innerText+"</p>";
		
		
	}
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='name'>"+languaje.forms_name+"</label>";
	result.innerText = result.innerText+"<input type='text' name='name' placeholder='name' required>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='surname'>"+languaje.forms_surname+"</label>";
	result.innerText = result.innerText+"<input type='text' name='surname' placeholder='surname' required>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='email' >"+languaje.forms_email+"</label>";
	result.innerText = result.innerText+"<input type='email' name='email' placeholder='email' required>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldregister' for='profile'>"+languaje.forms_city+"</label>";
	var councilid = sessionStorage.getItem("councilid");
	result.innerText = result.innerText+"<input type='city' readonly = 'readonly' placeholder='email' value ='"+getCouncil(councilid).name+"' >";
	result.innerText = result.innerText+"</p> ";  

	if (selecteduser == undefined || selecteduser == ''){
		result.innerText = result.innerText+"<p><input type='checkbox' name='terms' id='terms' />" +
		" "+languaje.form_message_read+" ";
		
		
		result.innerText = result.innerText+"<a href='files/IES CITIES-Terms and Conditions-ENG-v10.pdf' target='_blank'/>"+languaje.term_of_use+"</a>";

		result.innerText = result.innerText+"</p><input type='checkbox' id='enabledgogle' onclick='auth(false);'/> Enable Access with Google";
		
		result.innerText = result.innerText+"<input type='button' id='register' value='"+languaje.forms_register+"' onclick='javascript:iesregister();' >";
		

		
	}
	else{
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_update+"' onclick='javascript:updateUser();' >";
		
	}
	result.innerText = result.innerText+"</form>";
	result.innerHTML =result.innerText ;
	//if (selecteduser == undefined || selecteduser == '')
		//document.getElementById("register").disabled = true;
	if (selecteduser !=undefined && selecteduser != ''){
		document.register.username.value = selecteduser.username;
		document.register.name.value = selecteduser.name;
		document.register.surname.value = selecteduser.surname;	
		
		document.register.email.value = selecteduser.email;
	
	
		
	}	
}
/*function activateButton(element) {

    if(element.checked) {
      document.getElementById("register").disabled = false;
     }
     else  {
      document.getElementById("register").disabled = true;
    }

}*/
function updateUser(){
	if (validarUpdate()){
		var user= eval('(' + sessionStorage.getItem("user") + ')');
		url = '/IESCities/api/entities/users/'+user.userId;
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		
		var name=document.register.name.value;
		var password=document.register.password.value;
		var surname=document.register.surname.value;
		var username=document.register.username.value;
		var email=document.register.email.value;
		var councilid = sessionStorage.getItem("councilid");

		var council = councilid;//document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
		var user = new Object();
		user.name = name;
		user.password = password;
		user.surname =surname;
		user.username = username;
		user.email = email;
		user.preferredCouncilId = council;
		user.profile="User";
		if (goauth != null ){
			user.googleProfile = goauth.id_token;
		}
		var params = JSON.stringify(user);
		xml = getHTTPObject();
		xml.onreadystatechange = procesarResultadoUpdate;
		xml.open('PUT',url,false)	;
		xml.setRequestHeader("Content-type", "application/json");
		xml.setRequestHeader('Authorization', auth);
		xml.send(params);	
		return true;
	}	
	
}
/**
 * 
 * Register new user. Callbaback function procesarResultadoUser
 */
function iesregister(){
	if (validarRegister()){
		url = '/IESCities/api/entities/users';
		var name=document.register.name.value;
		var password=document.register.password.value;
		var surname=document.register.surname.value;
		var username=document.register.username.value;
		var email=document.register.email.value;
		
		var councilid = sessionStorage.getItem("councilid");

		var council = councilid;//document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
		var user = new Object();
		user.name = name;
		user.password = password;
		user.surname =surname;
		user.username = username;
		user.email = email;
		user.preferredCouncilId = council;
		user.profile="User";
		if (goauth != null  ){
			user.googleProfile = goauth.id_token;
		}
		//var profile = document.register.profile.options[document.register.profile.selectedIndex].value;
		
		var params = JSON.stringify(user);
		xml = getHTTPObject();
		xml.onreadystatechange = procesarResultadoUser;
		xml.open('POST',url,false)	;
		xml.setRequestHeader("Content-type", "application/json");
		xml.send(params);	
		return true;
	}
}
/**
 * Validates the register fields
 * @returns {Boolean}
 */
function validarRegister(){
	var result = document.getElementById("errorregister");
	/*if (document.frmcity == null){
		result.innerHTML="No available cities for register!";
		return true;
	}	
	else*/
	if (!document.register.terms.checked){
		result.innerHTML=languaje.frm_user_error_tos;
		Alert.render(languaje.frm_user_error_tos);
		return false;
		
	}
	else if (document.register.name.value == ""){
		result.innerHTML=languaje.frm_user_error_name;
		Alert.render(languaje.frm_user_error_name);
		return false;
	}
	else if (document.register.password.value == "" && document.register.password.length < 5){
		result.innerHTML=languaje.frm_user_error_password;
		Alert.render(languaje.frm_user_error_password);
		return false;
	}
	else if (document.register.surname.value == ""){
		result.innerHTML=languaje.frm_user_error_surname;
		Alert.render(languaje.frm_user_error_surname);
		return false;
	}
	else if (document.register.username.value == ""){
		result.innerHTML=languaje.frm_user_error_username;
		Alert.render(languaje.frm_user_error_username);
		return false;
	}
	else if (document.register.email.value == ""){
		result.innerHTML=languaje.frm_user_error_email;
		Alert.render(languaje.frm_user_error_email);
		return false;
	}
	return true;

}
/**
 * Validates the update fields
 * @returns {Boolean}
 */
function validarUpdate(){
	var result = document.getElementById("errorregister");
	/*if (!document.register.terms.checked){
		result.innerHTML=languaje.frm_user_error_tos;
		Alert.render(languaje.frm_user_error_tos);
		return false;
		
	}
	else */
if (document.register.name.value == ""){
		result.innerHTML=languaje.frm_user_error_name;
		Alert.render(languaje.frm_user_error_name);
		return false;
	}
	else if (document.register.surname.value == ""){
		result.innerHTML=languaje.frm_user_error_surname;
		Alert.render(languaje.frm_user_error_surname);
		return false;
	}
	else if (document.register.username.value == ""){
		result.innerHTML=languaje.frm_user_error_username;
		Alert.render(languaje.frm_user_error_username);
		return false;
	}
	else if (document.register.email.value == ""){
		result.innerHTML=languaje.frm_user_error_email;
		Alert.render(languaje.frm_user_error_email);
		return false;
	}
	else if (document.register.password.value != "" && document.register.password.length < 5){
		result.innerHTML=languaje.frm_user_error_password_2;
		Alert.render(languaje.frm_user_error_password_2);
		return false;
	}
	else if (document.register.password.value != "" && document.register.currentpassword.value != sessionStorage.getItem("password")){
		result.innerHTML=languaje.frm_user_error_password_3;
		Alert.render(languaje.frm_user_error_password_3);
		return false;
	}
		
	return true;
	
}
/**
 * If the register goes well return to index page.
 */
function procesarResultadoUpdate(){
	if (xml.readyState == 4){		
		//var sessionid = user.sessionId;
		if (xml.status == "200" && xml.responseText != null && xml.responseText != ""){
		//if (xml.status != "400" && xml.status != "403" && xml.responseText != null && xml.responseText != ""){
			sessionStorage.setItem("user",xml.responseText);
			sessionStorage.removeItem("errorregister");
			if (document.register.password.value != "" && document.register.currentpassword.value == sessionStorage.getItem("password")){
				sessionStorage.setItem("password",document.register.password.value);
			}
			
			document.location='index.html';
			
		}
		else {
			var respuesta = eval('(' + xml.responseText + ')');
			if (respuesta.hasOwnProperty("message"))
				sessionStorage.setItem("errorregister","The Update failed. "+respuesta.message);
			else
				sessionStorage.setItem("errorregister","The Update failed.");
			paintFrmRegister();			
			
			
		}
		
	}

}
/**
 * If the register goes well return to index page.
 */
function procesarResultadoUser(){
	if (xml.readyState == 4){		
		//var sessionid = user.sessionId;
		if (xml.status == "200" && xml.responseText != null && xml.responseText != ""){
		//if (xml.status != "400" && xml.status != "403" && xml.responseText != null && xml.responseText != ""){
			sessionStorage.setItem("user",xml.responseText);
			sessionStorage.removeItem("errorregister");
			sessionStorage.setItem("password",document.register.password.value);
			document.location='index.html';
			
		}
		else {
			var respuesta = eval('(' + xml.responseText + ')');
			if (respuesta.hasOwnProperty("message")){
				
				sessionStorage.setItem("errorregister","The Operation with the user failed. "+respuesta.message);
				Alert.render(respuesta.message);
			}
			else{
				sessionStorage.setItem("errorregister","The Operation with the user failed.");
				Alert.render("The Operation with the user failed.");
			}
			paintFrmRegister();
			
		}
		
	}

}
/**
 * Paint registry form.
 */
function paintFrmLogin(){
	var result = document.getElementById("formulario");
	//var responsetext=sessionStorage.getItem("user");
	//user = eval('(' + responsetext + ')');

	result.innerText = "<h1>"+languaje.forms_button_login+"</h1>";
	var errorlogin = sessionStorage.getItem("errorlogin");
	if (errorlogin!=null && errorlogin!=""){
		result.innerText = result.innerText+"<div id='error'>"+errorlogin+"</div>";
		sessionStorage.removeItem("errorlogin");
	}
	else{
		result.innerText = result.innerText+"<div id='error'></div>";
	}
	result.innerText = result.innerText+"<form name='login' accept-charset='utf-8' >";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label for='username'>"+languaje.frm_admin_user_name+"</label>";
	result.innerText = result.innerText+"<input type='text' name='username' placeholder='yourname' required>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label for='password'>"+languaje.forms_password+"</label>";
	result.innerText = result.innerText+"<input type='password' name='password' placeholder='password' pattern='.{5,}' required title='5 characters minimum'></li>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_login+"' onclick='javascript:ieslogin()'>";
	
	result.innerText = result.innerText+"<nav>or</nav>";
		
	result.innerText = result.innerText+"<input type='button' value='Access with Google' onclick='auth(true);'/>";
	
	result.innerText = result.innerText+"<nav>or</nav>";
	
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_register+"' onclick='javascript:paintFrmRegister()'>";
	
	result.innerText = result.innerText+"<nav>or</nav>";
		
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_reset_password+"' onclick='javascript:paintPasswordResetForm()'>";
		
	result.innerText = result.innerText+"</form>";
	result.innerHTML =result.innerText ;	
}
/**
 * callback of the logout function.
 */
function procesarLogout(){
	var result = document.getElementById("formulario");
	if (xml.readyState == 4){
		sessionStorage.removeItem("user");
		document.location='index.html';//paintFrmLogin();
	}
	else
		result.innerHTML = "Loading...";
}
/**
 * View logs of an user (the user try to see its own logs.
 * @param app
 */
function viewLogsUser(paramuser){
	var result = document.getElementById("content");
	
	var user ="";
	if (paramuser == undefined || paramuser == null || paramuser ==''){
		responseText = sessionStorage.getItem("user");
		user = eval('(' + responseText + ')');
	}
	else{
		responseText =sessionStorage.getItem("lstusers");
		var users= eval('(' + responseText + ')');
		 user = users[paramuser];
		
	}
	result.innerText = "<h1>Logs of "+user.username+"</h1>";
	result.innerText = result.innerText + "<div id='log1'></div>";
	result.innerText = result.innerText + "<div id='log2'></div>";
	result.innerText = result.innerText + "<div id='log3'></div>";
	result.innerText = result.innerText + "<div id='log4'></div>";
	result.innerText = result.innerText + "<div id='log5'></div>";
	result.innerHTML =result.innerText;
	//url = protocol+ip+'/IESCities/api/datasets/deletedataset';
	url = '/IESCities/api/entities/users/me/ratings';
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	
	
	xml = getHTTPObject();
	xml.onreadystatechange = viewLogUser;
	xml.open('GET',url,false );
	xml.setRequestHeader('Authorization', auth);

	xml.send();	


	
	
}
function viewLogUser(){
	var result = document.getElementById("log1");
	if (xml.readyState == 4){
		if (xml.status == "200"){

		//if (xml.status!="500" && xml.status!="403"){
			result.innerText = "<p>Ratings Of the User .</p>";
			var logs= eval('(' + xml.responseText + ')');
			if (logs == null ) result.innerText+"";
			else if (logs.length != null){
				for (data=0;data<logs.length;data++){
					result.innerText = result.innerText+log[data].ratingId+" "+log[data].date+" "+log[data].rating+" "+log[data].title+" "+log[data].comment+" "+log[data].lang+" "+log[data].userId+" "+log[data].userName+" "+log[data].appId;
				}
				
			}
		}
		result.innerHTML =result.innerText;

	}
}