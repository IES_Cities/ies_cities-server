//http://www.the-art-of-web.com/html/html5-form-validation/
/**
 * This function gets all the apps of the council, and take as parameter the selected city
 * @returns {Boolean}
 */
function loadCityApps(){
	var councilid = sessionStorage.getItem("councilid");//document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
	sessionStorage.setItem("councilid",councilid);
	//url = protocol+urlservicio+'api/entities/councils/'+councilid+'/apps';
	url = '/IESCities/api/entities/councils/'+councilid+'/apps?lang='+lang;
	xml = getHTTPObject();
	xml.onreadystatechange = procesarApps;
	xml.open('GET',url,false )	;
	xml.send();	
	return true;
}
/**
 * Callbak function of loadCityAppps
 * Gets the list of apps and paint it in the lef side of web page
 */
function procesarApps(){
	var result = document.getElementById("apps");
	if (xml.readyState == 4){
		//if (xml.status!="403" && xml.responseText!= null && xml.responseText!=""){
		if (xml.status == "200" && xml.responseText!= null && xml.responseText!=""){


			var applications= eval('(' + xml.responseText + ')');
			
			result.innerText = "<h1>"+languaje.applications_city+"</h1>";
			result.innerText = result.innerText+" <ul>";
			if (applications == null ) result.innerText+"";
			else if (applications.length != null){
				for (data=0;data<applications.length;data++){
					if (applications[data].name != null && applications[data].name != "") {
						result.innerText = result.innerText+" <li><a href='#' onclick=getApps('"+applications[data].appId+"')>"+applications[data].name+"</a></li>";
					}
				}
			}	
			/*
			else if (applications.Application.length == null){
				result.innerText = result.innerText+" <li><a href='#' onclick=getApps('"+applications.Application.appId+"')>"+applications.Application.name+"</a></li>";
			}
			else{
				for (data=0;data<applications.Application.length;data++){
					
					result.innerText = result.innerText+" <li><a href='#' onclick=getApps('"+applications.Application[data].appId+"')>"+applications.Application[data].name+"</a></li>";
				}
			}
			*/
			result.innerText = result.innerText+" </ul>";
			var responsetext = sessionStorage.getItem("user");
			if (responsetext != null){
				user = eval('(' + responsetext + ')');
			}
			if (responsetext !=null && user != null ){
	
				result.innerText = result.innerText+"<form name='frmapps' accept-charset='utf-8'>";
				result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"' onclick=paintFrmApplication('')>";	
				result.innerText = result.innerText+"</form>";
				
			}
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_search+"' onclick=searchApps()>";	
			result.innerText = result.innerText+"<p>&nbsp;</p>";
			result.innerHTML =result.innerText ;
		}
		//if (document.location.pathname =='/IESCities/index.html' || document.location.pathname =='/IESCities/') loadUser();
		
	}
	else{
		result.innerText = "<h1>"+languaje.applications_app+"</h1";
		result.innerText = result.innerText+"Loading...";
		var responsetext = sessionStorage.getItem("user");
		if (responsetext != null){
			user = eval('(' + responsetext + ')');
		}
		if (responsetext !=null && user != null ){

			result.innerText = result.innerText+"<form name='frmdataset' accept-charset='utf-8'>";
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"' onclick=paintFrmApplication('')>";	
			result.innerText = result.innerText+"</form>";
			
		}
		
		result.innerHTML =result.innerText ;
	}

}
/**
 * function of search, defaultl value for search =a because an empty field produces a crash.
 * @returns {Boolean}
 */
function searchApps(){
	
	var search = '';
	if (document.frmsearch == undefined) search = DEFAULT_SEARCH_VALUE;
	else search = document.frmsearch.name.value;
	if (search == undefined || search == null || search == '') search = DEFAULT_SEARCH_VALUE;
	//url = protocol+urlservicio+'api/entities/apps/search/';
	url = '/IESCities/api/entities/apps/search/';
	var params = "keywords="+search+'&lang='+lang;
	xml = getHTTPObject();
	xml.onreadystatechange = paintListApp;
	xml.open('GET',url+'?'+params,false)	;
	xml.send();	
	return true;		
}
/**
 * Callback function of searchApps
 */
function paintListApp(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			//result.innerText = "<p>Result .</p>";
			var applicatins= eval('(' + xml.responseText + ')');
			paintListApps(applicatins);
		}
		//result.innerHTML =result.innerText;
	
	}
}
/**
 * Function that receive a list of applications in json format and paint it as a table.
 * @param applicationlist
 */
function paintListApps(applicationlist){
	var result = document.getElementById("content");
	result.innerText = "<h1>"+languaje.applications_app+"</h1>";
	result.innerText = result.innerText+"<form name='frmsearch' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<input type='text' name='name' placeholder='"+languaje.forms_button_search_string+"' required />";
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_search+"' onclick=searchApps()>";	
	result.innerText = result.innerText+"</form>";
	
	result.innerText = result.innerText+"<table>";
	result.innerText = result.innerText+"<thead>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<th>"+languaje.frm_applications_app_name+"</th>";
	result.innerText = result.innerText+"<th>"+languaje.forms_description+"</th>";
	result.innerText = result.innerText+"<th>"+languaje.forms_url+"</th>";
	result.innerText = result.innerText+"<th>"+languaje.forms_tags+"</th>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"</thead>";
	result.innerText = result.innerText+"<tbody>";
	if (applicationlist == null ) result.innerText = result.innerText+"";
	else if (applicationlist.length != null){
		for (data=0;data<applicationlist.length;data++){
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td>"+applicationlist[data].name+"</td>";			
			result.innerText = result.innerText+"<td >"+applicationlist[data].description+"</td>";
			result.innerText = result.innerText+"<td ><a href='"+applicationlist[data].url+"' >"+applicationlist[data].name+" </a></td>";
			result.innerText = result.innerText+"<td >"+paintTags(applicationlist[data].tags)+"</td>";			
			result.innerText = result.innerText+"</tr>";
			
		}
	}
	result.innerText = result.innerText+"</tbody>";
	result.innerText = result.innerText+"</table>";
	result.innerHTML =result.innerText ;			
		
}
/**
 * Load the defined scopes, needed in app creation.
 * The service getallscopes has disappear! So use search :?
 * @returns {Boolean}
 */
function loadGeoScopes(){
	//url = protocol+ip+'/IESCities/api/council/getallscopes';entities/geoscopes
	//url = protocol+urlservicio+'api/entities/geoscopes/search/';
	url = '/IESCities/api/entities/geoscopes/search/';
	xml = getHTTPObject();
	xml.onreadystatechange = procesarGeoScopes;
	var params = 'keywords=o';
	xml.open('GET',url+'?'+params,false);
	xml.send();	
	return true;
}
/**
 * Callback function of loadGeoScopes
 */
function procesarGeoScopes(){
	if (xml.readyState == 4){
		if (xml.status=="200"){
			sessionStorage.setItem("scopes",xml.responseText);
		}
	}

}
/**
 * This function paint a form with the data of an application in order to make the update.
 * If its call from the table of My Applications receve the index of the App to load.
 * 
 * If call from the load app in the left side of the web, the application to load is in session.
 * @param indiceapp
 */
function paintFrmUpdateApp(indiceapp){
	if (indiceapp == undefined || indiceapp == null || indiceapp ==''){

		var responseText = sessionStorage.getItem("app");
		var selectedapp = eval('(' + responseText + ')');
		
		paintFrmApplication(selectedapp);
	}
	else{
		var responseText = sessionStorage.getItem("userapps");
		var applications= eval('(' + responseText + ')');
		var selectedapp = applications[indiceapp];
		sessionStorage.setItem("app",JSON.stringify(selectedapp));
		paintFrmApplication(selectedapp);
		
	}
}
function paintFrmApplication(selectedapp){
	var result = document.getElementById("content");
	if (selectedapp ==undefined || selectedapp == ''){
		result.innerText = "<h1>"+languaje.frm_applications_new_app+"</h1>";
		
	}
	else{
		result.innerText = "<h1>"+languaje.frm_applications_update_app+"</h1>";
		
	}
	var errorapp =sessionStorage.getItem("errorapp");
	
	if (errorapp!=null && errorapp!=""){
		result.innerText = result.innerText+"<div id='errorapp'>"+errorapp+"</div>";
		sessionStorage.removeItem("errorapp");
	}	
	else{
		result.innerText = result.innerText+"<div id='errorapp'></div>";
	}
	
	result.innerText = result.innerText+"<form name='frmnewapplication' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='name'>"+languaje.frm_applications_new_app+"</label>";
	result.innerText = result.innerText+"<input type='text' name='name' placeholder='"+languaje.frm_applications_app_name_ph+"' required />";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='languaje'>"+languaje.languaje+"</label>";
	if (selectedapp == undefined || selectedapp == ''){
		result.innerText = result.innerText+"<select name='lang'>";
		result.innerText = result.innerText+" <option value ='EN'>EN</option>";
		result.innerText = result.innerText+" </select>";
	}
	else{
		result.innerText = result.innerText+"<select name='lang'>";
		result.innerText = result.innerText+" <option value ='EN'>EN</option>";
		result.innerText = result.innerText+" <option value ='ES'>ES</option>";
		result.innerText = result.innerText+" <option value ='IT'>IT</option>";
		result.innerText = result.innerText+" </select>";		
	}
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='description'>"+languaje.forms_description+"</label>";
	result.innerText = result.innerText+"<textarea rows=10 name='description' placeholder='"+languaje.frm_applications_app_description_ph+"' required ></textarea>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='permissions'>"+languaje.forms_permission+"</label>";
	result.innerText = result.innerText+"<input type='text' name='permissions' placeholder='"+languaje.frm_applications_app_permission_ph+"'/>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='url'>"+languaje.forms_url+" </label>("+languaje.frm_applications_url_patter+")";
	result.innerText = result.innerText+"<input type='text' name='url' placeholder='"+languaje.frm_applications_app_url_ph+"' required  pattern=\"https?://.+\"/>";
	//result.innerText = result.innerText+"<small>Starting with http</small>";	
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='image'>"+languaje.forms_image+"</label>";
	result.innerText = result.innerText+"<input type='text' name='image' placeholder='"+languaje.frm_applications_app_image_ph+"'/>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='packageName'>"+languaje.forms_package_name+"</label>";
	result.innerText = result.innerText+"<input type='text' name='packageName' placeholder='"+languaje.forms_package_name+"' required/>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='version'>"+languaje.forms_version+"</label>";
	result.innerText = result.innerText+"<input type='text' name='version' placeholder='"+languaje.forms_version+"' required/>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='ToS'>"+languaje.forms_tos+"</label>";
	result.innerText = result.innerText+"<textarea rows=10 name='tos' placeholder='"+languaje.forms_tos+"' required></textarea>";;
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='tags'>"+languaje.forms_tags+"</label>";
	result.innerText = result.innerText+"<input type='text' name='tags' placeholder='Tags'/>";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='geographicalScope'>"+languaje.forms_geographical_scope+"</label>";
	
	
	result.innerText = result.innerText+" <select name='geographicalScope'>";
	var scopes= eval('(' + sessionStorage.getItem("scopes") + ')');
	if (scopes == null) result.innerText+"";
	else if (scopes.length != null){
		for (data=0;data<scopes.length;data++){
			result.innerText = result.innerText+" <option value ='"+scopes[data].id+"'>"+scopes[data].name+"</option>";
		}
	}
	
	else if (scopes.GeoScope.length == null){
		selected ="selected";
		result.innerText = result.innerText+" <option value ='"+scopes.GeoScope.id+"'>"+scopes.GeoScope.name+"</option>";
	}
	else{
		for (data=0;data<scopes.GeoScope.length;data++){
			result.innerText = result.innerText+" <option value ='"+scopes.GeoScope[data].id+"'>"+scopes.GeoScope[data].name+"</option>";
		}
	}
	result.innerText = result.innerText+" </select>";
	
	
	result.innerText = result.innerText+"</p>";
	if (selectedapp ==undefined || selectedapp == ''){
		result.innerText = result.innerText+"<p>";
		result.innerText = result.innerText+"<label class='fieldnew' for='dataset'>"+languaje.forms_used_dataset+"</label>";
		
		
		result.innerText = result.innerText+" <select name='datasets' id = 'selDataset' multiple = 'multiple'>";
		
		var datasets= eval('(' + sessionStorage.getItem("datasets") + ')');
		if (datasets == null) result.innerText+"";
		else if (datasets.length != null){
			for (data=0;data<datasets.length;data++){
				result.innerText = result.innerText+" <option value ='"+datasets[data].datasetId+"'>"+datasets[data].name+"</option>";
			}
		}
		
		result.innerText = result.innerText+" </select>";
		
		
		result.innerText = result.innerText+"</p>";
	}
	if (selectedapp == undefined || selectedapp == ''){
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_create+"' onclick='javascript:createApplication()'/>";
		
	}
	else{
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_update+"' onclick='javascript:updateApplication()'/>";
	}
	
	
	result.innerText = result.innerText+"</form>";
	result.innerHTML =result.innerText ;
	
	if (selectedapp != undefined && selectedapp != ''){
		document.frmnewapplication.name.value = selectedapp.name;
		document.frmnewapplication.description.value = selectedapp.description;
		document.frmnewapplication.permissions.value = selectedapp.permissions;	
		
		document.frmnewapplication.url.value = selectedapp.url;
		document.frmnewapplication.image.value = selectedapp.image;
		document.frmnewapplication.packageName.value = selectedapp.packageName;	
		  for(var i=0; i < document.frmnewapplication.lang.options.length; i++)
		    {
		      if(document.frmnewapplication.lang.options[i].text == selectedapp.lang)
		    	  document.frmnewapplication.lang.selectedIndex = i;
				
		    }		//var applications= eval('(' + xml.responseText + ')');
		
		document.frmnewapplication.packageName.value = selectedapp.lang;	
		
		document.frmnewapplication.version.value = selectedapp.version;
		document.frmnewapplication.tos.value = selectedapp.termsOfService;
		document.frmnewapplication.tags.value = selectedapp.tags;	
		var tags = selectedapp.tags.split(",");
		var strtags ="";
		for(var i=0; i < tags.length; i++)
		    {
				strtags = strtags +tags[i]+" ,";
			}
			strtags = strtags.substring(0, strtags.length-1);
			document.frmnewapplication.tags.value = strtags;
		  for(var i=0; i < document.frmnewapplication.geographicalScope.options.length; i++)
		    {
		      if(document.frmnewapplication.geographicalScope.options[i].text == selectedapp.geographicalScope.name)
		    	  document.frmnewapplication.geographicalScope.selectedIndex = i;
				
		    }		//var applications= eval('(' + xml.responseText + ')');
		  /*
		 var appdatasets =  eval('(' +sessionStorage.getItem("appdatasets")+ ')');
		 if (appdatasets!=null){//datasets
			for (data=0;data<appdatasets.length;data++){
				  for(var i=0; i < document.frmnewapplication.datasets.options.length; i++)
				    {
				      if(document.frmnewapplication.datasets.options[i].text == appdatasets[data].name)
				    	  document.frmnewapplication.datasets.options[i].selected = true;
						
				    }		
								
			}
		
			 
		 }
		 */
	}
	
}
/**
 * function that call the update service
 * @returns {Boolean}
 */
function updateApplication(){
	if (validateApplication()){
		var responseText = "";
		var selectedapp = "";
		responseText = sessionStorage.getItem("app");
		selectedapp = eval('(' + responseText + ')');
		
		var councilid = sessionStorage.getItem("councilid");
		//var city = new Object();
		//city.councilId =  councilid;
		//city.name = document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
		selectedapp.belongsTo = getCouncil(councilid);
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		selectedapp.name = document.frmnewapplication.name.value;
		selectedapp.description = document.frmnewapplication.description.value;	
		selectedapp.url = document.frmnewapplication.url.value;	
		var permissions = document.frmnewapplication.permissions.value;
		if (permissions == ""){
			permissions = "See in google play.";
		}

		selectedapp.permissions = permissions;
		selectedapp.image = document.frmnewapplication.image.value;
		selectedapp.version = document.frmnewapplication.version.value;
		selectedapp.termsOfService = document.frmnewapplication.tos.value;
		var tags = document.frmnewapplication.tags.value.split(",");
		selectedapp.tags = [];
		for (var i = 0; i < tags.length; i++) {
			selectedapp.tags.push(tags[i]);
		}
		//document.frmnewapplication.tags.value;
		
		selectedapp.lang = document.frmnewapplication.lang.options[document.frmnewapplication.lang.selectedIndex].value;
		selectedapp.geographicalScope =  getGeoscope(document.frmnewapplication.geographicalScope.options[document.frmnewapplication.geographicalScope.selectedIndex].value);
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		var params = JSON.stringify(selectedapp);	
		//var params  = "appName="+name+"&description="+description+"&url="+urla+"&permissions="+permissions+
			//		"&image="+image+"&version="+version+"&tos="+tos+"&tags="+tags+"&scopeid="+scopeid+"&councilid="+councilid;
		url = '/IESCities/api/entities/apps/'+selectedapp.appId;
		//url = protocol+urlservicio+'api/entities/apps/'+selectedapp.appId;
		xml = getHTTPObject();
		xml.onreadystatechange = processUpdateApp;
		xml.open('PUT',url,false)	;
		//Send the proper header information along with the request
		xml.setRequestHeader("Content-type", "application/json");
		xml.setRequestHeader('Authorization', auth);
		
		xml.send(params);	
		return true;
	}
}
/**
 * Funtion tha call the createApplication srvice
 */
function createApplication(){
	if (validateApplication()){
		var councilid = sessionStorage.getItem("councilid");	
		var application = new Object();
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		var name = document.frmnewapplication.name.value;
		var description = document.frmnewapplication.description.value;	
		var urla = document.frmnewapplication.url.value;	
		var permissions = document.frmnewapplication.permissions.value;
		if (permissions == ""){
			permissions = "See in google play.";
		}
		var image = document.frmnewapplication.image.value;
		var version = document.frmnewapplication.version.value;
		var tos = document.frmnewapplication.tos.value;
		//var tags = document.frmnewapplication.tags.value;
		var tags = document.frmnewapplication.tags.value.split(",");
		application.tags = [];
		for (var i = 0; i < tags.length; i++) {
			application.tags.push(tags[i]);
		}
		
		var packagename = document.frmnewapplication.packageName.value;
		var applang = document.frmnewapplication.lang.options[document.frmnewapplication.lang.selectedIndex].value;
		
		var scopeid =  document.frmnewapplication.geographicalScope.options[document.frmnewapplication.geographicalScope.selectedIndex].value;
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		
		sessionStorage.setItem("councilcreated",name);
		application.name = name;
		application.description = description;
		application.url= urla;
		application.image = image;
		application.version = version;
		application.tos = tos;
		application.tags = tags;
		application.permissions = permissions;
		application.packageName = packagename;
		application.councilid = councilid;
		application.scopeid = scopeid;
		application.lang = applang;
		var params = JSON.stringify(application);	

		//var params  = "appName='"+name+"'&description='"+description+"'&url='"+urla+"'&permissions='"+permissions+
			//		"'&image='"+image+"'&version='"+version+"'&tos='"+tos+"'&tags='"+tags+"'&scopeid="+scopeid+
				//	"&packagename='"+packagename+"'&councilid="+councilid;
		url = '/IESCities/api/entities/apps';
		//url = protocol+urlservicio+'api/entities/apps';
		xml = getHTTPObject();
		xml.onreadystatechange = processResultUploadApplication;
		xml.open('POST',url,false)	;
		xml.setRequestHeader("Content-type", "application/json");		
		xml.setRequestHeader('Authorization', auth);
		
		xml.send(params);	
		
		//Add the datasets to the uploaded application.

	}
	return true;		   	   
}
/**
 * For app validation.
 */
function validateApplication(){
	var result = document.getElementById("errorapp");
	var description = document.frmnewapplication.description.value;	
	var urla = document.frmnewapplication.url.value;	
	var permissions = document.frmnewapplication.permissions.value;
	var image = document.frmnewapplication.image.value;
	var version = document.frmnewapplication.version.value;
	var tos = document.frmnewapplication.tos.value;
	var tags = document.frmnewapplication.tags.value;
	var packagename = document.frmnewapplication.packageName.value;
	
	if (document.frmnewapplication.name.value == ""){
		result.innerHTML = languaje.frm_applications_name_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	} 
	else if (description== ""){
		result.innerHTML=languaje.frm_applications_description_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (urla == ""){
		result.innerHTML=languaje.frm_applications_url_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (permissions == ""){
		//result.innerHTML=languaje.frm_applications_permissions_error;
		//Alert.render(languaje.frm_applications_name_error);
		//return false;
	}
	else if (image == ""){
		result.innerHTML=languaje.frm_applications_image_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (version == ""){
		result.innerHTML=languaje.frm_applications_version_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (tos == ""){
		result.innerHTML=languaje.frm_applications_tos_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (tags == ""){
		result.innerHTML=languaje.frm_applications_tags_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	else if (packagename == ""){
		result.innerHTML=languaje.frm_applications_package_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	}
	return true;
	
}
function getGeoscope(gid){
	var scopes= eval('(' + sessionStorage.getItem("scopes") + ')');	
	if (scopes == null) return new Object();
	else if (scopes.length != null){
		for (data=0;data<scopes.length;data++){
			if (scopes[data].id == gid) 
				return scopes[data];
		}
	}
	return new Object();
	

}
function processResultUploadApplication2(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			
			
			paintFrmApplication();
		}
		else {
			sessionStorage.setItem("errorapp",languaje.frm_applications_upload_error+": "+xml.responseText);
			paintFrmApplication();
		}
	}
}
/**
 * Refresh the left side of the web when app is created.
 */
function processResultUploadApplication(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			
			
			sessionStorage.removeItem("councilcreated");
			application= eval('(' + xml.responseText + ')');
			var appid = application.appId;
			var seldatasets = document.getElementById("selDataset");
			var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
			log("AppCustom","AppUpload:"+application.appId);
			if (appid != null && appid != "") {
				for (var i = 0; i < seldatasets.options.length; i++){
					//examine current option
					if (seldatasets.options[i].selected)
					{
						var dataset = new Object();
						
						
						dataset.datasetId =seldatasets.options[i].value;
						params = JSON.stringify(dataset);	
						url = '/IESCities/api/entities/apps/'+appid+'/datasets/';
						//url = protocol+urlservicio+'api/entities/apps/'+appid+'/datasets/';
						xml.onreadystatechange = processResultUploadApplication2;
						xml.open('PUT',url,false);
						xml.setRequestHeader("Content-type", "application/json");		
						xml.setRequestHeader('Authorization', auth);				
						xml.send(params);	
					}
				}	
			}
			loadCityApps();
						
			paintFrmApplication();
		}
		else {
			sessionStorage.setItem("errorapp",languaje.frm_applications_upload_error+": "+xml.responseText);
			paintFrmApplication();
		}
	}
}
/**
 * Call the rest service that retrieve an app.
 * @param appid
 * @returns {Boolean}
 */
function getApps(appid){
	//url = protocol+urlservicio+'api/entities/apps/'+appid;
	url = '/IESCities/api/entities/apps/'+appid+'?lang='+lang;
	xml = getHTTPObject();
	xml.onreadystatechange = paintApps;
	xml.open('GET',url)	;
	xml.send();	
	return true;	
}
/**
 * Callbak of the getApps function.
 */

function processUpdateApp(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			paintApps	();
		}
		else {
			sessionStorage.setItem("errorapp",languaje.frm_applications_upload_error+": "+xml.responseText);
			paintFrmApplication();
		}
	}
}


function paintApps(){
	if (xml.readyState == 4){
		if (xml.status=="200"){
	
			var result = document.getElementById("content");
			var application= "";
			result.innerText =  "<h1></h1>";
			
			application= eval('(' + xml.responseText + ')');
			var errorapp =sessionStorage.getItem("errorapp");
			
			if (errorapp!=null && errorapp!=""){
				result.innerText = result.innerText+"<div id='errorapp'>"+errorapp+"</div>";
				sessionStorage.removeItem("errorapp");
			}	
			else{
				result.innerText = result.innerText+"<div id='errorapp'></div>";
			}
			
			result.innerText =  "<h1>"+application.name+"</h1>";
			sessionStorage.setItem("app",xml.responseText);
			
			var responsetext =sessionStorage.getItem("user");
			
			if (responsetext != null){
				user = eval('(' + responsetext + ')');
			}
			result.innerText = result.innerText+"<section id='contentApp'>";	
			result.innerText = result.innerText+"<article id='subContentApp'>";
			if (application == null ) result.innerText = result.innerText+"";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppImage' ><img  src='"+application.image+"' /></div>";
			result.innerText = result.innerText+"<div class='dvAppImageTitle' ><p></p></div>";
			result.innerText = result.innerText+"</div>";
			//result.innerText = result.innerText+"<div>";
			//result.innerText = result.innerText+"<td>ID</td>";
			//result.innerText = result.innerText+"<td>"+application.appId+"</td>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppName'><p>"+languaje.forms_name+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppNameContent'><p>"+application.name+"</p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppDescription'><p>"+languaje.forms_description+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppContent'><p>"+application.description+"</p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppPermissions'><p>"+languaje.forms_permission+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppPermissionsContent'><p>"+application.permissions+"</p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppURL'><p>"+languaje.forms_url+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppURLContent'><p><a href='#' onclick=logDownload('AppCustom','AppDownload:"+application.appId+"','"+application.url+"')>"+application.name+" </a></p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppPackage'><p>"+languaje.forms_package_name+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppPackageContent'><p>"+application.packageName+"</p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppVersion'><p>"+languaje.forms_version+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppVersionContent'><p>"+application.version+"</p></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppGS'><p>"+languaje.forms_geographical_scope+"</p></div>";
			if (application.geographicalScope != undefined && application.geographicalScope != null ){
				result.innerText = result.innerText+"<div class='dvAppGSContent'><p>"+application.geographicalScope.name+"</p></div>";
			}
			else{
				result.innerText = result.innerText+"<div class='dvAppGSContent'></div>";
			}
			result.innerText = result.innerText+"</div>";
			
			
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppTags'><p>"+languaje.forms_tags+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppTagsContent'><p>"+paintTags(application.tags)+"</p></div>";
			result.innerText = result.innerText+"</div>";
			if (responsetext !=null && user != null ){
				
				result.innerText = result.innerText+"<div>";
				result.innerText = result.innerText+"<div class='dvAppToS'><p>"+languaje.forms_actions+"</p></div>";
				result.innerText = result.innerText+"<div class='dvAppToSContent'>";
		
		
				result.innerText = result.innerText+"<form name='frmapp' accept-charset='utf-8'>";
				//result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"' onclick=paintFrmApplication('')>";
				if (user.admin){
					result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_update+"' onclick='paintFrmUpdateApp()'/>";
					result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_delete+"' onclick='deleteApplication()'/>";
					result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_logs+"' onclick='viewLogsApp();'/>";
					result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_graph+"' onclick='showAppGrafica("+application.appId+");'/>";
				}
				result.innerText = result.innerText+"</form>";
				result.innerText = result.innerText+"</div></div>";
			}			
			
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div  class='dvAppDataset'><p>"+languaje.dataset_datasets+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppDatasetContent' id='appdatasets'></div>";
			result.innerText = result.innerText+"</div>";
			result.innerText = result.innerText+"<div>";
			result.innerText = result.innerText+"<div class='dvAppToS'><p>"+languaje.forms_tos+"</p></div>";
			result.innerText = result.innerText+"<div class='dvAppToSContent'><p>"+application.termsOfService+"</p></div>";
			result.innerText = result.innerText+"</div>";
			
			

			
			
			
			result.innerText = result.innerText+"</article>";
			result.innerText = result.innerText+"</section>";//</div>";
			
			result.innerHTML =result.innerText ;
			
			if (application!=null && application.appId != ""){
				paintAplicationDataset(application.appId);
			}
		}
	}
}
function showAppGrafica(appid){		
	$("#content").load("graficas"+appid+".html?_="+(new Date()).getTime());
}
function paintAplicationDataset(appid){
	//GET /entities/apps/{appid}/datasets
	url = '/IESCities/api/entities/apps/'+appid+'/datasets?lang='+lang;
	//url = protocol+urlservicio+'api/entities/apps/'+appid+'/datasets';
	xml = getHTTPObject();
	xml.onreadystatechange = paintDatasetsApps;
	xml.open('GET',url)	;
	xml.send();	
	return true;	
	
}
function paintDatasetsApps(){
	
	if (xml.readyState == 4){
		if (xml.status=="200"){
			var result = document.getElementById("appdatasets");
			sessionStorage.setItem("appdatasets",xml.responseText);
			var datasets= eval('(' + xml.responseText + ')');
			
			result.innerText ="<p>"; 
			
			//if (datasets == null ) result.innerText+"<p>";
			
			if (datasets.length != null){
				for (data=0;data<datasets.length;data++){
					result.innerText = result.innerText+" "+datasets[data].name ;
				}
			}	
			result.innerText = result.innerText+"</p>";
			result.innerHTML =result.innerText ;
		}

	}	
	
}

function logDownload(typo,mensaje,url){
	log(typo,mensaje);
	//document.location=url;
	window.open(url,'_blank');
}
/**
 * This function delete an App.
 * If its call from the table of My Applications receve the index of the App to delete.
 * 
 * If call from the load app in the left side of the web, the application to delete is in session.
 */
function deleteApplication(app){
	var responseText ="";
	var appName ="";
	var appId ="";
	if (app == undefined || app == null || app ==''){
		responseText = sessionStorage.getItem("app");
		selectedapp = eval('(' + responseText + ')');
		appName = selectedapp.name;
		appId = selectedapp.appId;
	}
	else{
		responseText =sessionStorage.getItem("userapps");
		var applications= eval('(' + responseText + ')');
		var application = applications[app];
		
		appName =application.name;
		appId = application.appId;
	}
	
	if (confirm(languaje.forms_areyousure_delete_message+" "+appName+'?')) {
		//url = protocol+ip+'/IESCities/api/datasets/deletedataset';
		url = '/IESCities/api/entities/apps/'+appId;
		//url = protocol+urlservicio+'api/entities/apps/'+appId;
		//http://localhost:8080/IESCities/api/entities/apps/92
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		
		xml = getHTTPObject();
		xml.onreadystatechange = procesarDeleteApplications;
		xml.open('DELETE',url,false );
		xml.setRequestHeader('Authorization', auth);
		
		xml.send();	
		return true;	    
	}
}
/**
 * Callbak of delete application.
 */
function procesarDeleteApplications(){
	var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200"){
			Alert.render(languaje.forms_app_remove_message);
			document.location='index.html';
		}
		else {
			Alert.render(languaje.forms_app_not_remove_message+" "+xml.responseText);
			document.location='index.html';
		}
	}
}
function viewLogsApp(app){
	var result = document.getElementById("content");
	var type = "all";
	var from = "none";
	var to = "none";
	if (document.frmsearch!= null){
		type = document.frmsearch.type.options[document.frmsearch.type.selectedIndex].value;
		if (document.frmsearch.from.value != ""){
			var aux= document.frmsearch.from.value;
			aux= aux.replace("PM"," PM");
			aux= aux.replace("AM"," AM");
			aux =  aux+", GMT";
			document.frmsearch.from.value = aux;
			from = document.frmsearch.from.value;
		}
		if (document.frmsearch.to.value != ""){
			var aux= document.frmsearch.to.value;
			aux= aux.replace("PM"," PM");
			aux= aux.replace("AM"," AM");
			aux =  aux+", GMT";
			document.frmsearch.to.value = aux;
			to = document.frmsearch.to.value;
		}
	}
	var responseText ="";
	var appName ="";
	//var appId ="";
	if (app == undefined || app == null || app ==''){
		responseText = sessionStorage.getItem("app");
		selectedapp = eval('(' + responseText + ')');
		appName = selectedapp.name;
		appId = selectedapp.appId;
	}
	else{
		responseText =sessionStorage.getItem("userapps");
		var applications= eval('(' + responseText + ')');
		var application = applications[app];
		sessionStorage.setItem("app",JSON.stringify(application));
		appName =application.name;
		appId = application.appId;
	}
	result.innerText = "<h1>Logs of "+appName+"</h1>";
	
	result.innerText = result.innerText+"<form name='frmsearch' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<select name=type>";
	//result.innerText = result.innerText+" <option value ='all'>All</option>";
	result.innerText = result.innerText+" <option value ='AppLaunch'>AppLaunch</option>";
	result.innerText = result.innerText+" <option value ='AppStart'>AppStart</option>";
	result.innerText = result.innerText+" <option value ='AppStop'>AppStop</option>";
	result.innerText = result.innerText+" <option value ='AppConsume'>AppConsume</option>";
	result.innerText = result.innerText+" <option value ='AppProsume'>AppProsume</option>";
	result.innerText = result.innerText+" <option value ='AppCustom'>AppCustom</option>";

	result.innerText = result.innerText+"</select></br>";   
	result.innerText = result.innerText+"<p id='idfrom'>From: <input type='Text' id='from' maxlength='25' size='25'/>";
	result.innerText = result.innerText+"<img src='img/cal.gif' onclick=javascript:NewCssCal('from','ddMMyyyy','dropdown',true,12) style='cursor:pointer'/></br></p>";	
	result.innerText = result.innerText+"<p id='idto'>To: <input type='Text' id='to' maxlength='25' size='25'/>";
	result.innerText = result.innerText+"<img src='img/cal.gif' onclick=javascript:NewCssCal('to','ddMMyyyy','dropdown',true,12) style='cursor:pointer'/></br></p>";	
	//result.innerText = result.innerText+"From: <input type='datetime' name='from'></br>";
	//result.innerText = result.innerText+"To: <input type='datetime' name='to'></br>";
	result.innerText = result.innerText+"<input type='button' value='Search' onclick='javascript:viewLogsApp();'/>";	
	result.innerText = result.innerText+"</form>";
	
	result.innerText = result.innerText + "<div id='log3'></div>";
	result.innerHTML =result.innerText;
	//url = protocol+urlservicio+'api/datasets/deletedataset';
	if (from == "none" || to == "none") {
		return false;
	}
	else{
		url = '/IESCities/api/log/app/getquery/?app='+appName+'&type='+type+'&from='+from+'&to='+to;
		
		xml = getHTTPObject();
		xml.onreadystatechange = viewLog3;
		xml.open('GET',url,false );
		xml.send();	
	}

	
}
//20/06/2014 12:03
//04/06/2014 1:30 PM, GMT
function formatDate(date){
	var yf=date.split("-")[0];           
	var mf=date.split("-")[1];
	var df=date.split("-")[2];
	while (mf.lengh <2)mf='0'+mf;
	while (df.lengh <2)df='0'+df;
	return df+'/'+mf+'/'+yf;
}
/**
 * {
    "appid": "Democratree",
    "date": "04-jun-2014 16:31:30",
    "duration": 0,
    "message": "Data consumed",
    "sessionid": 22791236,
    "timestamp": 1401892290.624,
    "type": "AppCustom"
  },
 */
function viewLog3(){
	var result = document.getElementById("log3");
	if (xml.readyState == 4){
		var log= eval('(' + xml.responseText + ')');
		if (log == null )  result.innerText = "<p>Logs</p>";
		else result.innerText = "<p>Logs: "+log.length+" </p>";
		result.innerText = result.innerText+"<table>";
		result.innerText = result.innerText+"<thead>";
		result.innerText = result.innerText+"<tr>";
		result.innerText = result.innerText+"<th>"+languaje.forms_date+"</th>";
		result.innerText = result.innerText+"<th>"+languaje.frm_applications_app_name+"</th>";
		result.innerText = result.innerText+"<th>"+languaje.forms_session_id+"</th>";
		result.innerText = result.innerText+"<th>"+languaje.forms_type+"</th>";
		result.innerText = result.innerText+"<th>"+languaje.forms_duration+"</th>";
		result.innerText = result.innerText+"<th>"+languaje.forms_message+"</th>";
		result.innerText = result.innerText+"</tr>";
		result.innerText = result.innerText+"</thead>";
		result.innerText = result.innerText+"<tbody>";
		if (log == null )  result.innerText = result.innerText+"";
		else if (log.length != null){
			for (data=0;(data<log.length && data < 50) ;data++){
				result.innerText = result.innerText+"<tr>";
				result.innerText = result.innerText+"<td>"+log[data].date+"</td>";			
				result.innerText = result.innerText+"<td >"+log[data].appid+"</td>";
				result.innerText = result.innerText+"<td>"+log[data].sessionid+"</td>";			
				result.innerText = result.innerText+"<td >"+log[data].type+"</td>";
				result.innerText = result.innerText+"<td>"+log[data].duration+"</td>";			
				result.innerText = result.innerText+"<td >"+log[data].message+"</td>";
				result.innerText = result.innerText+"</tr>";
				
			}
		}
		result.innerText = result.innerText+"</tbody>";
		result.innerText = result.innerText+"</table>";
		
	
		result.innerHTML =result.innerText;

	}
}

