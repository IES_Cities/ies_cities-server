//•	GRP.SUP.1  Gráfico mostrando el número de aplicaciones o servicios desplegados en cada ciudad de IES Cities (KPI.WI.1). 
function loadGRPSUP1Data(){ 
	url = '/IESCities/api/entities/apps/all';
	xml = getHTTPObject();
	xml.onreadystatechange = procesarGRPSUP1s;
	xml.open('GET',url,false)	;
	xml.send();	
	return true;
}
/**
 * Callback of procesarGRPSUP1s function.
 * @returns {Boolean}
 */
function procesarGRPSUP1s(){

	if (xml.readyState == 4){
		if (xml.status == "200"){
			var applications = eval('(' + xml.responseText + ')');
			var jsoncities = sessionStorage.getItem("allcities");
			var allcities= eval('(' + jsoncities + ')');
			var cities = [];
			for (var i = 0; i <  allcities.length; i++) {
				var value = new Object();
				value.name =allcities[i].name;
				value.number = 0;				
				cities[i] = value;
			}
			for (var app = 0; app < applications.length;app++){
				var apcity = applications[app].belongsTo.name;
				for (var city = 0; city< cities.length;city++){
					if (apcity == cities[city].name ) {
						cities[city].number = cities[city].number +1;
						break;
					}
				}
			}
			paintGraphicBarras (cities,applications.length);
		}

	}

}
// GRP.SUP.4 à Interés en cada ciudad (KPI.WI.5) à De los usuarios totales que acceden a 
//IES Cities conocer cuántos van a Zaragoza, cuántos a Majadahonda….
function loadGRPSUP4Data(){ 
	url = '/IESCities/api/log/app/getquery/webinterface?type=WebCitySelect&from=none&to=none';
	//http://150.241.239.65:8080/IESCities/api/log/app/getquery/Democratree?type=all&from=none&to=none
	
	xml = getHTTPObject();
	xml.onreadystatechange = procesarGRPSUP4s;
	xml.open('GET',url,false );
	xml.send();	
	return true;
}
function procesarGRPSUP4s(){

	if (xml.readyState == 4){
		if (xml.status == "200"){
			var jsoncities = sessionStorage.getItem("allcities");
			var allcities= eval('(' + jsoncities + ')');
			var cities = [];
			for (var i = 0; i <  allcities.length; i++) {
				var value = new Object();
				value.councilId = allcities[i].councilId;
				value.name =allcities[i].name;
				value.number = 0;				
				cities[i] = value;
			}
			var logs= eval('(' + xml.responseText + ')');

			for (var log = 0; log < logs.length;log++){
				var citylogid = logs[log].message;
				for (var city = 0; city< cities.length;city++){
					if (citylogid == cities[city].councilId ) {
						cities[city].number = cities[city].number +1;
						break;
					}
				}

			}
			/*var log="";
			for (var city=0 ; city <  cities.length; city++){
				log = log +"City: "+cities[city].name+"Accesos: "+cities[city].number+"\n";
			}
			alert (log);*/
			paintGraphicBarras (cities,logs.length);
		}

	}

}
function loadGRPSUP5Data(){ 
	url = '/IESCities/api/entities/datasets/all';
	xml = getHTTPObject();
	xml.onreadystatechange = procesarGRPSUP5s;
	xml.open('GET',url,false)	;
	xml.send();	
	return true;
}
function procesarGRPSUP5s(){

	if (xml.readyState == 4){
		if (xml.status == "200"){
			var datasets = eval('(' + xml.responseText + ')');
			var jsoncities = sessionStorage.getItem("allcities");
			var allcities= eval('(' + jsoncities + ')');
			var cities = [];
			for (var i = 0; i <  allcities.length; i++) {
				var value = new Object();
				value.councilId = allcities[i].councilId;
				value.name =allcities[i].name;
				value.number = 0;				
				cities[i] = value;
			}
			for (var dta = 0; dta < datasets.length;dta++){
				var datasetcity = datasets[dta].councilId;
				for (var city = 0; city< cities.length;city++){
					if (datasetcity == cities[city].councilId ) {
						cities[city].number = cities[city].number +1;
						break;
					}
				}
			}
			paintGraphicBarras (cities,datasets.length);
		}

	}

}
function paintGraphicBarras(cities, total){
	//alert(total);
	var result = document.getElementById("grafica");
	var colors = ["bar teal","bar salmon","bar peach","bar lime","bar grape"];
	var maxvalue =0;
	result.innerText = "<li  class='axis'>";
	for (var city=0 ; city <  cities.length; city++){
		if (cities[city].number > maxvalue) maxvalue = 	cities[city].number;
	}
	//alert (maxvalue);
	for (var city=0 ; city <  cities.length; city++){
		var color="bar teal";
		if (city >= colors.length){
			color  = colors[city % colors.length];
		}
		else color  = colors[city];
		
		var relativo = "0";
		var percent ="0";
		//Avoid divide by 0
		if (maxvalue > 0 ) {
			relativo = (cities[city].number*100)/maxvalue ;
			relativo = relativo.toFixed(1);
		}
		if (total > 0){
			percent =(cities[city].number*100)/total;
			percent =percent.toFixed(1);
			
		}
		
		
		result.innerText = result.innerText + "<li title='"+cities[city].number+"' style='height: "+relativo+"%;' class='"+color+"'>";
		result.innerText = result.innerText + "<div class='percent'>"+percent+"<span>%</span></div>";
		result.innerText = result.innerText + "<div class='skill'>"+cities[city].name+"</div>";
		result.innerText = result.innerText + "</li>";
	}

	result.innerHTML =result.innerText ;
	
	
}