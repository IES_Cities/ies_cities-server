//http://www.csstablegenerator.com/?table_id=7
var pageactual = 0;
var recorlimit = 20;

function getCityUsers(){
	
	
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	councild = user.managedCouncilId;
	url = '/IESCities/api/entities/councils/'+councild+'/users';
	if (user == null || user == "") return true;
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	xml = getHTTPObject();
	xml.onreadystatechange = paintUsers;
	xml.open('GET',url)	;
	xml.setRequestHeader('Authorization', auth);
	xml.send();	
	return true;	
}

function getIESUsers(){
	url = '/IESCities/api/entities/users/all?offset='+(pageactual*recorlimit)+'&limit='+recorlimit;
	var user= eval('(' + sessionStorage.getItem("user") + ')');
	sessionStorage.setItem("retorno", "IES");
	if (user == null || user == "") return true;
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	xml = getHTTPObject();
	xml.onreadystatechange = paintUsers;
	xml.open('GET',url)	;
	xml.setRequestHeader('Authorization', auth);
	xml.send();	
	return true;	
}
function paintUsers(){
	var result = document.getElementById("content");
	if (xml.readyState == 4){		
		//var sessionid = user.sessionID;
		if (xml.status!="403" && xml.responseText!= null && xml.responseText!=""){
			var user= eval('(' + sessionStorage.getItem("user") + ')');

			var users= eval('(' + xml.responseText + ')');
			
			//sessionStorage.setItem("users",users);
			var selectcities ="";
			result.innerText = "<h1>"+languaje.form_users+" </h1>";
			result.innerText = result.innerText+"<table>";
			result.innerText = result.innerText+"<thead>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<th>"+languaje.frm_admin_user_name+"</th>";
			if (user.admin){
				result.innerText = result.innerText+"<th>"+languaje.frm_admin_pref_council+"</th>";
				result.innerText = result.innerText+"<th>"+languaje.frm_admin_man_council+"</th>";
			}
			result.innerText = result.innerText+"<th>"+languaje.form_admin+"</th>";
			result.innerText = result.innerText+"<th>"+languaje.form_developer+"</th>";
			if (user.admin){
				result.innerText = result.innerText+"<th>"+languaje.form_actions+"</th>";
			}
			if (user.admin){
				result.innerText = result.innerText+"<th>"+languaje.forms_admin_set_admin_message+"</th>";
			}
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"</thead>";
			result.innerText = result.innerText+"<tbody>";
			if (users == null ) result.innerText = result.innerText+"";
			else if (users.length != null){
				
				for (data=0;data<users.length;data++){
					if (user.admin){
						selectcities =paintSelectCities(users[data].userId);
						sessionStorage.setItem(users[data].userId,JSON.stringify(users[data]));
					}
					
					result.innerText = result.innerText+"<tr>";
					result.innerText = result.innerText+"<td>"+users[data].name+ " ("+users[data].email+")</td>";
					if (user.admin){
						if (users[data].preferredCouncilId !=null && users[data].preferredCouncilId != undefined && users[data].preferredCouncilId !=-1){
							result.innerText = result.innerText+"<td>"+getSelectedCouncil(users[data].preferredCouncilId)+"</td>";
						}
						else{
							result.innerText = result.innerText+"<td></td>";
						}
								
						if (users[data].managedCouncilId !=null && users[data].managedCouncilId != undefined && users[data].managedCouncilId !=-1){
							result.innerText = result.innerText+"<td>"+getSelectedCouncil(users[data].managedCouncilId)+"</td>";
						}
						else{
							result.innerText = result.innerText+"<td></td>";
						}
					}
					if (user.admin){
						result.innerText = result.innerText+"<td ><a href='#' onclick=changeIESAdmin('"+users[data].userId+"','"+users[data].admin+"')>"+users[data].admin+" </a></td>";
					}
					else{
						result.innerText = result.innerText+"<td >"+users[data].admin+"</td>";
					}
					
					result.innerText = result.innerText+"<td >"+users[data].developer+"</td>";//<a href='#' onclick=viewLogsUserByAdmin('"+users[data].userId+"')>View User Logs </a>
					if (user.admin){
						result.innerText = result.innerText+"<td>";
						result.innerText = result.innerText+"<a href='#' onclick=getUser('"+users[data].userId+"')>"+languaje.forms_button_view+" </a> " +
															"<a href='#' onclick=removeUser('"+users[data].userId+"')>"+languaje.forms_button_delete+" </a>" ;
						result.innerText = result.innerText+"</td>";
					}
					if (user.admin){
						result.innerText = result.innerText+"<td>";
						result.innerText = result.innerText+selectcities;
						result.innerText = result.innerText+"</td>";
					}
				
					result.innerText = result.innerText+"</tr>";
					
				}

				
			}
			result.innerText = result.innerText+"</tbody>";
			result.innerText = result.innerText+"</table>";
				if (pageactual > 0){
						result.innerText = result.innerText+"<input type='button' value='<' onclick=last20() /> ";
																			
				}
				if (users.length >= recorlimit ){
					result.innerText = result.innerText+"<input type='button' value='>' onclick=next20() /> " ;
				}
			
			result.innerHTML =result.innerText ;
			
		}
		else {
			result.innerText = "<h1>"+languaje.form_users+" </h1>";
			result.innerHTML  = result.innerText+"No Users found.";
			
		}
	}	
}
function next20(){
	pageactual=pageactual+1;
	getIESUsers();
}
function last20(){
	if (pageactual > 0)
		pageactual=pageactual-1;
	else
		pageactual = 0;
	getIESUsers();
}
function changeIESAdmin(usertochange,currentstatus){
	var status='';
	if (currentstatus == 'true') status='false';
	else status='true';
	if (confirm(languaje.forms_admin_change_admin_message)) {

		url = '/IESCities/api/entities/users/'+usertochange+'/admin/'+status;
		responseText = sessionStorage.getItem("user");
		admin = eval('(' + responseText + ')');
		var auth = make_basic_auth(admin.username,sessionStorage.getItem("password"));
		//var params ="user_id="+user_id;
		
		xml = getHTTPObject();
		xml.onreadystatechange = resultChangeIESAdmin;
		xml.open('POST',url,false );
		xml.setRequestHeader('Content-type', 'application/json');
	
		xml.setRequestHeader('Authorization', auth);
	}
	xml.send();	
	
	
}
function resultChangeIESAdmin(){
	if (xml.readyState == 4){
		if (xml.status=="200"){
			retorno = sessionStorage.getItem("retorno");
			if (retorno=="IES"){
				sessionStorage.removeItem("retorno");
				getIESUsers();
				
			}
			else{
				sessionStorage.removeItem("retorno");
				getCityUsers();
				
			}
		}
	}	
}
function setAsCityAdmin(userid){// document.frmcity.city.options[document.frmcity.city.selectedIndex].value
	var councilid = eval("document.frmcity"+userid+".city.options[document.frmcity"+userid+".city.selectedIndex].value");
	var user = sessionStorage.getItem(userid);
	/*var users = sessionStorage.getItem(users);
	for (data = 0; data <users.length; data++){
		if (users[data].userId == userid){
			user = users[data];
			break;
		}
	}*/
	if (councilid!='-1'){
		url = '/IESCities/api/entities/councils/'+councilid+'/admins';
		
	}
	else {
		var usersend = eval('(' + user + ')');
		url = '/IESCities/api/entities/councils/'+usersend.managedCouncilId+'/admins/'+userid;
	}
	
	var sessioncouncil = sessionStorage.getItem("councilid");
	
	
	var long =sessionStorage.length;
	for (var key=0; key < long; key++){
		var keystorage = sessionStorage.key(key);
		if (!isNaN(keystorage)) {
			sessionStorage.removeItem(keystorage); key--;long --;
		}
	}
	sessionStorage.setItem("councilid",sessioncouncil);
	//sessionStorage.keys;
	//PUT /entities/councils/{councilid}/admins
	responseText = sessionStorage.getItem("user");
	admin = eval('(' + responseText + ')');
	var auth = make_basic_auth(admin.username,sessionStorage.getItem("password"));
	xml = getHTTPObject();
	xml.onreadystatechange = resultChangeIESAdmin;
	sessionStorage.setItem("retorno","IES");
	if (councilid!='-1'){
		xml.open('PUT',url,false );
	}
	else{
		xml.open('DELETE',url,false );
	}
	xml.setRequestHeader("Content-type", "application/json");
	xml.setRequestHeader('Authorization', auth);
	if (councilid!='-1'){
		xml.send(user);		
	}
	else{
		xml.send();
	}
}

function getUser(user_id){
	var result = document.getElementById("content");
	
	
	url = '/IESCities/api/entities/users/'+user_id;
	responseText = sessionStorage.getItem("user");
	admin = eval('(' + responseText + ')');
	var auth = make_basic_auth(admin.username,sessionStorage.getItem("password"));
	//var params ="user_id="+user_id;
	
	xml = getHTTPObject();
	xml.onreadystatechange = viewUser;
	xml.open('GET',url,false );
	xml.setRequestHeader('Authorization', auth);

	xml.send();	
	
}

function viewUser(){
	if (xml.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		if (xml.status == "200"){
		
			var user= eval('(' + xml.responseText + ')');
			paintUser(user);
		}
	}	
}
function paintUser(user){
	var result = document.getElementById("content");
	result.innerText =  "<h1>"+user.username+"</h1>";
	//sessionStorage.setItem("app",xml.responseText);
	
	//var responsetext =sessionStorage.getItem("user");
	
	//result.innerText = result.innerText+"<div id='izquierda'></div>";
	//result.innerText = result.innerText+"<div id='derecha'>";
	result.innerText = result.innerText+"<table>";	
	result.innerText = result.innerText+"<thead>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<th>"+languaje.forms_field+"</th>";
	result.innerText = result.innerText+"<th>"+languaje.forms_value+"</th>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"</thead>";
	result.innerText = result.innerText+"<tbody>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>ID</td>";
	result.innerText = result.innerText+"<td>"+user.userId+"</td>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>"+languaje.forms_name+"</td>";
	result.innerText = result.innerText+"<td>"+user.name+"</td>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>"+languaje.forms_username+"</td>";
	result.innerText = result.innerText+"<td>"+user.username+"</td>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>"+languaje.forms_surname+"</td>";
	result.innerText = result.innerText+"<td>"+user.surname+"</td>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>"+languaje.forms_email+"</td>";
	result.innerText = result.innerText+"<td>"+user.email+"</td>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<td>"+languaje.forms_profile+"</td>";
	result.innerText = result.innerText+"<td>"+user.profile+"</td>";
	result.innerText = result.innerText+"</tr>";
	
	if (user.preferredCouncilId != -1 ){
		prefereedcouncil= getSelectedCouncil(user.preferredCouncilId);
		result.innerText = result.innerText+"<tr>";
		result.innerText = result.innerText+"<td>"+languaje.frm_admin_pref_council+"</td>";
		result.innerText = result.innerText+"<td>"+prefereedcouncil+"</td>";
		result.innerText = result.innerText+"</tr>";
		
		
	}
	if (user.managedCouncilId != -1 ){
		managedcouncil = getSelectedCouncil(user.managedCouncilId);
		result.innerText = result.innerText+"<tr>";
		result.innerText = result.innerText+"<td>"+languaje.frm_admin_man_council+"</td>";
		result.innerText = result.innerText+"<td>"+managedcouncil+"</td>";
		result.innerText = result.innerText+"</tr>";
		
	}
	
	
	result.innerText = result.innerText+"</tbody>";
	result.innerText = result.innerText+"</table>";//</div>";
	result.innerHTML =result.innerText ;

}
function removeUser(user){
	
	if (confirm(languaje.forms_delete_user_message)) {
		//url = protocol+ip+'/IESCities/api/datasets/deletedataset';
		//var params = "userid="+user;
		url = '/IESCities/api/entities/users/'+user;
		
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		
		xml = getHTTPObject();
		xml.onreadystatechange = procesarRemoveUser;
		xml.open('DELETE',url,false );
		xml.setRequestHeader('Authorization', auth);
		
		xml.send();	
		return true;	    
	}
}
function procesarRemoveUser(){
	if (xml.readyState == 4){
		if (xml.status!="500" && xml.status!="403"){
			retorno = sessionStorage.getItem("retorno");
			if (retorno=="IES"){
				sessionStorage.removeItem("retorno");
				getIESUsers();
				
			}
			else{
				sessionStorage.removeItem("retorno");
				getCityUsers();
				
			}
		}
	}

}
function paintSelectCities(userid){
	var response =  sessionStorage.getItem("allcities");
	var cities= eval('(' + response + ')');
				
	var	result ="";
	result = "<form name='frmcity"+userid+"'>";
	//result = "<input type='hidden' name='userid' value ='"+userid+"'/>";
	result = result+" <select name='city'>";
		result = result+" <option value ='-1'>None</option>";
		if (cities == null) result+"";
		else if (cities.length != null){
			for (var data=0;data<cities.length;data++){
				//<option value="82">Zaragoza</option>
				result = result+" <option value ='"+cities[data].councilId+"'>"+cities[data].name+"</option>";
			}
		}
		
		result = result+" </select>" ;
		result= result+"<input type='button' value='"+languaje.forms_button_set+"' onclick=setAsCityAdmin('"+userid+"')>";	
		result= result+	"</form>";
		return result;
	

}

function viewLogsUserByAdmin(user_id){
	var result = document.getElementById("content");
	
	
	result.innerText = "<h1>Logs of "+user.username+"</h1>";
	result.innerText = result.innerText + "<div id='log1'></div>";
	result.innerText = result.innerText + "<div id='log2'></div>";
	result.innerText = result.innerText + "<div id='log3'></div>";
	result.innerText = result.innerText + "<div id='log4'></div>";
	result.innerText = result.innerText + "<div id='log5'></div>";
	result.innerHTML =result.innerText;
	//url = protocol+ip+'/IESCities/api/datasets/deletedataset';
	url = '/IESCities/api/entities/users/'+user_id+'/ratings';
	responseText = sessionStorage.getItem("user");
	admin = eval('(' + responseText + ')');
	var auth = make_basic_auth(admin.username,sessionStorage.getItem("password"));
	//var params ="userid="+user_id;
	
	xml = getHTTPObject();
	xml.onreadystatechange = viewLogUser;
	xml.open('GET',url,false );
	xml.setRequestHeader('Authorization', auth);

	xml.send();	


	
	
}

