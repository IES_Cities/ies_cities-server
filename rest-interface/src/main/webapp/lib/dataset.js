/**
 * This function gets all the datasets f the council, and take as parameter the selected city
 * @returns {Boolean}
 */

function loadCityDatasets(){
	
	var councilid = sessionStorage.getItem("councilid");//document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
	
	sessionStorage.setItem("councilid",councilid);
	//url = protocol+urlservicio+'api/entities/councils/'+councilid+'/datasets';
	url = '/IESCities/api/entities/councils/'+councilid+'/datasets';
	var params = "councilid="+councilid+"&offset=0&limit=100";
	xml = getHTTPObject();
	xml.onreadystatechange = procesarDatasets;
	xml.open('GET',url+'?'+params,false )	;
	xml.send();	
	return true;
}
/*
"[
    {
        "councilId": 217,
        "datasetId": 208,
        "description": "Dataset containing complaints and suggestions, which according to different administrative categories, citizens send to the Zaragoza City Council authorizing their publication. http://www.zaragoza.es/ciudad/risp/open311.htm (http://wiki.open311.org/GeoReport_v2)",
        "jsonMapping": "",
        "name": "Complaints and suggestions dataset",
        "numberAppsUsingIt": 1,
        "publishingTimestamp": "2014-09-18T11:07:23.464Z",
        "quality": 0,
        "qualityVerifiedBy": -1,
        "readAccessNumber": 0,
        "userId": 2533,
        "writeAccessNumber": 0
    },
  ...
*/
function procesarDatasets(){
	var result = document.getElementById("datasets");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			sessionStorage.setItem("datasets",xml.responseText);
			var datasets= eval('(' + xml.responseText + ')');
			var selectedDataset  = sessionStorage.getItem("datasetid");
			result.innerText = "<h1>"+languaje.datasets_city+"</h1>";
			result.innerText = result.innerText+" <ul>";
			//result.innerText = result.innerText+" <select id ='lstDatasets' onchange='getDataset();'>";
					
			if (datasets == null ) result.innerText+"";
			else if (datasets.length != null){
				for (data=0;data<datasets.length;data++){
					var selected ="";
					if (selectedDataset == datasets[data].datasetId) selected = "selected";
					//result.innerText = result.innerText+" <option value ="+datasets[data].datasetId+" "+selected+">"+datasets[data].name+"</option>";
					result.innerText = result.innerText+" <li><a href='#'onclick=getDataset('"+datasets[data].datasetId+"')>"+datasets[data].name+"</a></li>";
				}
			}
			/*
			else if (datasets.Dataset.length == null){
				result.innerText = result.innerText+" <li><a href='#' onclick=getDataset('"+datasets.Dataset.datasetId+"')>"+datasets.Dataset.name+"</a></li>";
			}
			else{
				for (data=0;data<datasets.Dataset.length;data++){
					
					result.innerText = result.innerText+" <li><a href='#'onclick=getDataset('"+datasets.Dataset[data].datasetId+"')>"+datasets.Dataset[data].name+"</a></li>";
				}
			}
			*/
			//result.innerText = result.innerText+" </select>";
			result.innerText = result.innerText+" </ul>";
	
			var responsetext = sessionStorage.getItem("user");
			if (responsetext != null){
				user = eval('(' + responsetext + ')');
			}
			var selectedcity = sessionStorage.getItem("councilid");
			
			
			if (responsetext !=null && user != null && user.managedCouncilId == selectedcity){ 
	
				result.innerText = result.innerText+"<form name='frmdataset' accept-charset='utf-8'>";
				result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"' onclick=paintFrmDataset('')>";	
				result.innerText = result.innerText+"</form>";
				
			}
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_search+"' onclick=searchDatasets()>";	
			
			/*
			else if (datasets.length != null) {
				for (data=0;data<datasets.length;data++){
					
					result.innerText = result.innerText+" </br></br>";
				}
				
				
			}
			
			*/
			result.innerHTML =result.innerText ;
		}
		loadCityAdmins();	
	}
	else{
		result.innerText = "<h1>"+languaje.dataset_datasets+"</h1>";
		result.innerText = result.innerText+"Loading...";
		var responsetext = sessionStorage.getItem("user");
		if (responsetext != null){
			user = eval('(' + responsetext + ')');
		}
		var selectedcity = sessionStorage.getItem("councilid");
		if (responsetext !=null && user != null ){ //&& user.managedCouncilId == selectedcity

			result.innerText = result.innerText+"<form name='frmdataset' accept-charset='utf-8'>";
			result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"' onclick=paintFrmDataset('')>";	
			result.innerText = result.innerText+"</form>";
			
		}
		
		result.innerHTML =result.innerText ;
		//loadCityApps();
	}
}

function loadCityAdmins(){
	
	var councilid = sessionStorage.getItem("councilid");//document.frmcity.city.options[document.frmcity.city.selectedIndex].value;
	url = '/IESCities/api/entities/councils/'+councilid+'/admins';
	var params = "councilid="+councilid+"&offset=0&limit=100";
	xml = getHTTPObject();
	xml.onreadystatechange = procesarAdmins;
	xml.open('GET',url+'?'+params,false )	;
	xml.send();	
	return true;
}
function procesarAdmins(){
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			sessionStorage.setItem("councils",xml.responseText);
		}
	}
}

	
/**
 * function of search, defaultl value for search =a because an empty field produces a crash.
 * @returns {Boolean}
 */
function searchDatasets(){
	
	var search = '';
	if (document.frmsearchdata == undefined) search = DEFAULT_SEARCH_VALUE;
	else search = document.frmsearchdata.name.value;	
	if (search == undefined || search == null || search == '') search = DEFAULT_SEARCH_VALUE;
	//url = protocol+urlservicio+'api/entities/datasets/search';
	url = '/IESCities/api/entities/datasets/search';
	var params = "keywords="+search+"&offset=0&limit=100";
	xml = getHTTPObject();
	xml.onreadystatechange = paintListDataset;
	xml.open('GET',url+'?'+params,false);
	xml.send();	
	return true;	
	
}

/**
 * Callback function of searchDatasets
 */
function paintListDataset(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status=="200" ){
			//result.innerText = "<p>Result .</p>";
			var dataset= eval('(' + xml.responseText + ')');
			paintListDatasets(dataset);
		}
		//result.innerHTML =result.innerText;	
	}
}
/**
 * Function that receive a list of datasets in json format and paint it as a table.
 * @param datasetlist
 */
function paintListDatasets(datasetlist){
	var result = document.getElementById("content");
	result.innerText = "<h1>"+languaje.dataset_datasets+" </h1>";
	result.innerText = result.innerText+"<form name='frmsearchdata' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<input type='text' name='name' placeholder='"+languaje.forms_button_search_string+"' required />";
	result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_search+"' onclick=searchDatasets()>";	
	result.innerText = result.innerText+"</form>";
	
	result.innerText = result.innerText+"<table>";
	result.innerText = result.innerText+"<thead>";
	result.innerText = result.innerText+"<tr>";
	result.innerText = result.innerText+"<th>"+languaje.frm_dataset_dat_name+"</th>";
	result.innerText = result.innerText+"<th>"+languaje.forms_description+"</th>";
	result.innerText = result.innerText+"</tr>";
	result.innerText = result.innerText+"</thead>";
	result.innerText = result.innerText+"<tbody>";
	if (datasetlist == null ) result.innerText = result.innerText+"";
	else if (datasetlist.length != null){
		for (data=0;data<datasetlist.length;data++){
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td>"+datasetlist[data].name+"</td>";			
			result.innerText = result.innerText+"<td >"+datasetlist[data].description+"</td>";
			result.innerText = result.innerText+"</tr>";
			
		}
	}
	result.innerText = result.innerText+"</tbody>";
	result.innerText = result.innerText+"</table>";
	result.innerHTML =result.innerText ;			
		
}

function getDatasetMapping(){
	var responseText = sessionStorage.getItem("dataset");
	var dataset = eval('(' + responseText + ')');
	jsouser = sessionStorage.getItem("user");
	var user = eval('(' + jsouser + ')');
	var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
	//url = protocol+urlservicio+'api/entities/datasets/'+dataset.datasetId+'/mapping';
	url = '/IESCities/api/entities/datasets/'+dataset.datasetId+'/mapping';
	xml = getHTTPObject();
	xml.onreadystatechange = setDataset;
	xml.open('GET',url,false);
	//Send the proper header information along with the request
	xml.setRequestHeader("Content-type", "application/json");
	xml.setRequestHeader('Authorization', auth);
	
	xml.send();	
	return true;
	
}
function setDataset(){
	
	if (xml.readyState == 4){
		if (xml.status == "200"){
			var result= eval('(' + xml.responseText + ')');
			
			document.frmnewdataset.jsonmapping.value = xml.responseText;
		}
	}
	
}
/**
 * Function call by update button, get the selected dataset, and send this data to form.
 */
function paintFrmUpdateDataset(){
	var responseText = sessionStorage.getItem("dataset");
	var selecteddataset = eval('(' + responseText + ')');
	paintFrmDataset(selecteddataset);
}
/**
 * Paint an empty dataset form or a form filled with the data received.
 */
function paintFrmDataset(selecteddataset){
	var result = document.getElementById("content");
	var jsonMapping ="";
	if (selecteddataset ==undefined || selecteddataset == ''){
		result.innerText = "<h1>"+languaje.frm_dataset_new_dat+"</h1>";		
	}
	else{
		result.innerText = "<h1>"+languaje.frm_dataset_update_dat+"</h1>";
	}
	var errordata =sessionStorage.getItem("errordata");
	
	if (errordata!=null && errordata!=""){
		result.innerText = result.innerText+"<div id='errordata'>"+errordata+"</div>";
		sessionStorage.removeItem("errordata");
	}	
	else{
		result.innerText = result.innerText+"<div id='errordata'></div>";
	}
	
	result.innerText = result.innerText+"<form name='frmnewdataset' accept-charset='utf-8'>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='datasetname'>"+languaje.frm_dataset_dat_name+"</label>";
	result.innerText = result.innerText+"<input type='text' name='datasetname' placeholder='"+languaje.frm_dataset_dat_name_ph+"' required />";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='datasetdescription'>"+languaje.forms_description+"</label>";
	result.innerText = result.innerText+"<input type='text' name='datasetdescription' placeholder='"+languaje.frm_dataset_dat_description_ph+"' required />";
	result.innerText = result.innerText+"</p>";
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='jsonmapping'>"+languaje.frm_dataset_dat_json_mapping+"</label>";
	result.innerText = result.innerText+"<textarea rows=10 name='jsonmapping' placeholder='"+JSON.stringify(languaje.frm_dataset_dat_json_mapping_ph)+"' required ></textarea>";
	result.innerText = result.innerText+"</p>";
	
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='datasetrating'>"+languaje.forms_rating+"</label>";
	
	
	result.innerText = result.innerText+" <select name='datasetrating'>";
	result.innerText = result.innerText+" <option value =1>"+languaje.forms_rating_1+"</option>";
	result.innerText = result.innerText+" <option value =2>"+languaje.forms_rating_2+"</option>";
	result.innerText = result.innerText+" <option value =3>"+languaje.forms_rating_3+"</option>";
	result.innerText = result.innerText+" <option value =4>"+languaje.forms_rating_4+"</option>";
	result.innerText = result.innerText+" <option value =5>"+languaje.forms_rating_5+"</option>";
	result.innerText = result.innerText+" </select>";
	
	
	result.innerText = result.innerText+"</p>";
	
	result.innerText = result.innerText+"<p>";
	result.innerText = result.innerText+"<label class='fieldnew' for='validatedby'>"+languaje.forms_validatedby+"</label>";
	//En update y create si el usuario que crea o updatea es counciladmin tambien verfica.
	var responsetext = sessionStorage.getItem("user");
	if (responsetext != null){
		user = eval('(' + responsetext + ')');
	}
	var selectedcity = sessionStorage.getItem("councilid");
	var councils = eval('(' + sessionStorage.getItem("councils") + ')');
	var namec='';	
	if (responsetext !=null && user != null && user.managedCouncilId == selectedcity){
		namec = user.name;
	}

	result.innerText = result.innerText+" <input type='text' name='validatedby' value='"+namec+"' disabled  />";		
	
		
	result.innerText = result.innerText+"</p>";
		
	
	
	if (selecteddataset ==undefined || selecteddataset == ''){
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_create+"' onclick='javascript:createDataset()'/>";
		
	}
	else{
		result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_update+"' onclick='javascript:updateDataset()'/>";
	}
	
	
	result.innerText = result.innerText+"</form>";
	result.innerHTML =result.innerText ;
	
	if (selecteddataset !=undefined && selecteddataset != ''){
		document.frmnewdataset.datasetname.value = selecteddataset.name;
		document.frmnewdataset.datasetdescription.value = selecteddataset.description;//quality
		getDatasetMapping();
		for(var i=0; i < document.frmnewdataset.datasetrating.options.length; i++) {
			if(document.frmnewdataset.datasetrating.options[i].value == selecteddataset.quality){
				document.frmnewdataset.datasetrating.selectedIndex = i;
				
		    }
		}

	}
}

/**
 * function that call update dataset rest service callback function  = paintDataset
 * @returns {Boolean}
 */
function updateDataset(){
	if (validateDataset()){
		var responseText = sessionStorage.getItem("dataset");
		var dataset = eval('(' + responseText + ')');
		var selectedcity = sessionStorage.getItem("councilid");	
		dataset.council_id  = selectedcity;
		dataset.readAccessNumber =0;
		dataset.writeAccessNumber =0;
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		dataset.userId  = user.userId;
		dataset.name = document.frmnewdataset.datasetname.value;
		dataset.description = document.frmnewdataset.datasetdescription.value;	
		dataset.jsonMapping = JSON.parse(document.frmnewdataset.jsonmapping.value);	
		dataset.quality = document.frmnewdataset.datasetrating.options[document.frmnewdataset.datasetrating.selectedIndex].value;
		if (document.frmnewdataset.validatedby.value != ''){
			dataset.qualityVerifiedBy = user.userId;
		}
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		var params = JSON.stringify(dataset);	
		//var params  = JSON.stringify(datasetid:"+dataset.datasetId+dataset);
		url = '/IESCities/api/entities/datasets/'+dataset.datasetId;
		//url = protocol+urlservicio+'api/entities/datasets/'+dataset.datasetId;
		xml = getHTTPObject();
		xml.onreadystatechange = paintDataset;
		xml.open('PUT',url,true)	;
		//Send the proper header information along with the request
		xml.setRequestHeader("Content-type", "application/json");
		//xml.setRequestHeader("Content-length", params.length);
		//xml.setRequestHeader("Connection", "close");	
		xml.setRequestHeader('Authorization', auth);
		
		xml.send(params);	
		return true;
	}
}

/**
 * function that call create dataset rest service callback function  = processResultUploadDataset
 */
function createDataset(){
	

	if (validateDataset()){
		var dataset = new Object();
		var selectedcity = sessionStorage.getItem("councilid");	
		dataset.council_id  = selectedcity;
		dataset.readAccessNumber =0;
		dataset.writeAccessNumber =0;
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		dataset.userId  = user.userId;
		dataset.name = document.frmnewdataset.datasetname.value;
		dataset.description = document.frmnewdataset.datasetdescription.value;	
		dataset.jsonMapping = JSON.parse(document.frmnewdataset.jsonmapping.value);	
		dataset.quality = document.frmnewdataset.datasetrating.options[document.frmnewdataset.datasetrating.selectedIndex].value;
		if (document.frmnewdataset.validatedby.value != ''){
			dataset.qualityVerifiedBy = user.userId;
		}

		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		var params = JSON.stringify(dataset);	
		url = '/IESCities/api/entities/datasets/';
		//url = protocol+urlservicio+'api/entities/datasets/';
		xml = getHTTPObject();
		xml.onreadystatechange = processResultUploadDataset;
		xml.open('POST',url,true)	;
		//Send the proper header information along with the request
		xml.setRequestHeader("Content-type", "application/json");
		//xml.setRequestHeader("Content-length", params.length);
		//xml.setRequestHeader("Connection", "close");	
		xml.setRequestHeader('Authorization', auth);
		
		xml.send(params);	
		return true;
	}
}
/**
 * function that validates frm dataset fields.
 */
function validateDataset(){
	var result = document.getElementById("errordata");
	var name = document.frmnewdataset.datasetname.value;
	var description = document.frmnewdataset.datasetdescription.value;	
	var jsonMapping = document.frmnewdataset.jsonmapping.value;	
	if (name == ""){
		result.innerHTML = languaje.frm_applications_name_error;
		Alert.render(languaje.frm_applications_name_error);
		return false;
	} 
	else if (description== ""){
		result.innerHTML = languaje.frm_applications_description_error;
		Alert.render(languaje.frm_applications_description_error);
		return false;
	}
	else if (jsonMapping == ""){
		result.innerHTML = languaje.frm_dataset_json_mapping_error;
		Alert.render(languaje.frm_dataset_json_mapping_error);
		return false;
	}
	try{
        JSON.parse(jsonMapping);
    }
	catch(e){
		result.innerHTML = languaje.frm_dataset_json_mapping_invalid;
		Alert.render(languaje.frm_dataset_json_mapping_invalid);
		return false;
    }
	return true;
	
}
/**
 * wow,.. i can´t rememenber for what it is! :P
 * @param i
 * @returns
 */
function padStr(i) {
    return (i < 10) ? "0" + i : "" + i;
}
/**
 * Calback funtion of createDataset
 */
function processResultUploadDataset(){
	//var result = document.getElementById("content");
	if (xml.readyState == 4){
		if (xml.status == "200"){
			loadCityDatasets();
			paintFrmDataset();
		}
		else {
			Alert.render(languaje.frm_applications_upload_error+": "+xml.responseText);
			sessionStorage.setItem("errordata",languaje.frm_applications_upload_error+": "+xml.responseText);
			paintFrmDataset();
				
		}
	}
}

/**
 * function that gets the data of one dataset. Callback Function paintDataset
 * @param datasetid
 * @returns {Boolean}
 */
 function getDataset(datasetid){
	//var params = "datasetid="+datasetid;
	log("AppCustom","Access to dataset info:"+datasetid);
	url = '/IESCities/api/entities/datasets/'+datasetid;
	//url = protocol+urlservicio+'api/entities/datasets/'+datasetid;
	xml = getHTTPObject();
	xml.onreadystatechange = paintDataset;
	xml.open('GET',url,false)	;
	xml.send();	
	sessionStorage.setItem("datasetid",datasetid);
	return true;	
}
/*
function getDataset(){
	var selectBox = document.getElementById("lstDatasets");
	var datasetid = selectBox.options[selectBox.selectedIndex].value;
	//var params = "datasetid="+datasetid;
	log("AppCustom","Access to dataset info:"+datasetid);
	url = '/IESCities/api/entities/datasets/'+datasetid;
	//url = protocol+urlservicio+'api/entities/datasets/'+datasetid;
	xml = getHTTPObject();
	xml.onreadystatechange = paintDataset;
	xml.open('GET',url,false)	;
	xml.send();	
	sessionStorage.setItem("datasetid",datasetid);
	return true;	
}
*/
function setDatasetDetails(){
	if (xml2.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		var result = document.getElementById("description");

		if (xml2.status == "200"){
	
			var info = eval('(' + xml2.responseText + ')');
			var datasetid = sessionStorage.getItem("datasetid");
			sessionStorage.removeItem("datasetid");
			
		
			result.innerText = "<h1>"+languaje.frm_datasets_how_to_1+"</h1>";
			result.innerText =result.innerText+"<p>"+languaje.frm_datasets_how_to_2+":";
			result.innerText =result.innerText+" "+languaje.frm_datasets_how_to_3+"</p>";
			
			result.innerText = result.innerText+"<p> "+languaje.frm_datasets_how_to_4+" <strong>"+info.type+"</strong></p>";
			
			if (info.type == "database"){
				result.innerText = result.innerText+"<p> "+languaje.frm_datasets_how_to_5+":</p>";
				//result.innerText = result.innerText+'<a href="http://'+urlservicio+'swagger/index.html#!/data/executeSQLQuery_post_1" target="_blank"/>SQL Query</a>';
				result.innerText = result.innerText+"<a href='#' onclick=\"logApiAccess('DevDoc','SQL Query','/IESCities/swagger/index.html#!/data/executeSQLQuery_post_1')\"/>SQL Query</a>";
				//result.innerText = result.innerText+'</br> http://'+urlservicio+'api/data/query/'+datasetid+'/sql';
				result.innerText = result.innerText+"<p></br> "+languaje.frm_datasets_how_to_6+":</br></p>";
				//result.innerText = result.innerText+'<a href="http://'+urlservicio+'swagger/index.html#!/data/executeSQLUpdate_post_0" target="_blank"/>SQL Update</a>';		
				result.innerText = result.innerText+"<a href='#' onclick=\"logApiAccess('DevDoc','SQL Update','/IESCities/swagger/index.html#!/data/executeSQLUpdate_post_0')\"/>SQL Update</a>";
				
				//result.innerText = result.innerText+'</br> http://'+urlservicio+'api/data/update/'+datasetid+'/sql';
		
			}
			else if (info.type == "sparql"){
				result.innerText = result.innerText+"<p> "+languaje.frm_datasets_how_to_5+":</p>";
				//result.innerText = result.innerText+'<a href="http://'+urlservicio+'swagger/index.html#!/data/executeSPARLQuery_post_3" target="_blank"/>SPARQL Query</a>';
				result.innerText = result.innerText+"<a href='#' onclick=\"logApiAccess('DevDoc','SPARQL Query','/IESCities/swagger/index.html#!/data/executeSPARLQuery_post_3')\"/>SPARQL Query</a>";
		
				//result.innerText = result.innerText+protocol+urlservicio+'api/data/query/'+datasetid+'/sparql';
			}
			result.innerText = result.innerText+"<p>"+languaje.frm_datasets_how_to_7+":";
			result.innerText = result.innerText+languaje.frm_datasets_how_to_8;
			result.innerText = result.innerText+languaje.frm_datasets_how_to_9;
			result.innerText = result.innerText+"</br>   <a href='files/How to use Datasets.docx' target='_blank'/>"+languaje.forms_more_info+" </a> "+languaje.frm_datasets_how_to_10+".</p>";
			result.innerText = result.innerText+"<p></br> "+languaje.forms_more_info_api+": <a href='#' onclick=\"logApiAccess('DevDoc','API','/IESCities/swagger/index.html')\" /> API</a></p>";
			result.innerHTML =result.innerText ;
			
			result = document.getElementById("tablas");
			//if (info.type == "database" || info.type == "sparql"){
			if (info.type != null){
				//result.innerText = result.innerText+"</br> "+languaje.frm_datasets_how_to_11+":";
				result.innerText = result.innerText+ "<h1>"+languaje.frm_datasets_how_to_11+"</h1>";
				result.innerText = result.innerText+"<table>";	
				result.innerText = result.innerText+"<tbody>";

				if (info.tables.length != null){
					for (data=0;data<info.tables.length;data++){
						if (info.tables[data].name =="USUARIOS") continue;
						if (info.tables[data].name =="EMPRESAS") continue;
						result.innerText = result.innerText+"<table>";	
						result.innerText = result.innerText+"<tbody>";

						result.innerText = result.innerText+"<tr>";
						result.innerText = result.innerText+"<td COLSPAN=2 class='tdDatasetContent'><h1>"+info.tables[data].name+"</h1></td>";
						result.innerText = result.innerText+"</tr>";
						
						for (lineas=0;lineas<info.tables[data].columns.length;lineas++){
							result.innerText = result.innerText+"<tr>";
							result.innerText = result.innerText+"<td class='tdDatasetContent'>"+info.tables[data].columns[lineas].name+"</td>";
							result.innerText = result.innerText+"<td class='tdDatasetContent'>"+info.tables[data].columns[lineas].type+"</td>";
							result.innerText = result.innerText+"</tr>";
						}
						
						
						
						
						result.innerText = result.innerText+"</tbody>";
						result.innerText = result.innerText+"</table>";

						//result.innerText = result.innerText+" <li>"+info.tables[data]+"</li>";
					}
				}
				result.innerText = result.innerText+"</tbody>";
				result.innerText = result.innerText+"</table>";
				
			}
			result.innerHTML =result.innerText ;
		}
		else {
			var datasetid = sessionStorage.getItem("datasetid");
			sessionStorage.removeItem("datasetid");
			
		
			result.innerText = "<h1>"+languaje.frm_datasets_how_to_1+"</h1>";
			result.innerText =result.innerText+"<p>"+languaje.frm_datasets_how_to_2+":";
			result.innerText =result.innerText+" "+languaje.frm_datasets_how_to_3+"</p>";
			result.innerText = result.innerText+"<p>"+languaje.frm_datasets_how_to_7+":";
			result.innerText = result.innerText+languaje.frm_datasets_how_to_8;
			result.innerText = result.innerText+languaje.frm_datasets_how_to_9;
			result.innerText = result.innerText+"</br>   <a href='files/How to use Datasets.docx' target='_blank'/>"+languaje.forms_more_info+" </a> "+languaje.frm_datasets_how_to_10+".</p>";
			result.innerText = result.innerText+"<p></br> "+languaje.forms_more_info_api+": <a href='#' onclick=\"logApiAccess('DevDoc','API','/IESCities/swagger/index.html')\" /> API</a></p>";			result.innerHTML =result.innerText ;
			
		}

	}
	
}
function logApiAccess(typo,mensaje,url){
	log(typo,mensaje);//
	//document.location=url;
	window.open(url,'_blank');
}
/**
 * callback of getDataset
 */
function paintDataset(){
	if (xml.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		if (xml.status == "200"){

			var result = document.getElementById("content");
			var dataset= eval('(' + xml.responseText + ')');
			var qualityVerifiedBy = dataset.qualityVerifiedBy;
			var responsetext = sessionStorage.getItem("user");
			sessionStorage.setItem("dataset",xml.responseText);
			if (responsetext != null){
				user = eval('(' + responsetext + ')');
			}
			result.innerText = "<div id='description'></div>";
			result.innerText = result.innerText+ "<h1>"+languaje.dataset_datasets+"</h1>";
		
			result.innerText = result.innerText+"<table>";	
			//result.innerText = result.innerText+"<thead>";
			//result.innerText = result.innerText+"<tr>";
			//result.innerText = result.innerText+"<th>Field</th>";
			//result.innerText = result.innerText+"<th>Value</th>";
			//result.innerText = result.innerText+"</tr>";
			//result.innerText = result.innerText+"</thead>";
			result.innerText = result.innerText+"<tbody>";
			if (dataset == null ) result.innerText = result.innerText+"";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td class='tdDatasetId'>ID</td>";
			result.innerText = result.innerText+"<td class='tdDatasetContent'>"+dataset.datasetId+"</td>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td class='tdDatasetName'>"+languaje.forms_name+"</td>";
			result.innerText = result.innerText+"<td class='tdDatasetContent'>"+dataset.name+"</td>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td class='tdDatasetDescription'>"+languaje.forms_description+"</td>";
			result.innerText = result.innerText+"<td class='tdDatasetContent'>"+dataset.description+"</td>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td class='tdDatasetRating'>"+languaje.forms_rating+"</td>";
			result.innerText = result.innerText+"<td class='tdDatasetContent'>"+getQuality(dataset.quality)+"</td>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<td class='tdDatasetValidatedBy'>"+languaje.forms_validated_by+"</td>";
			
			var councils = eval('(' + sessionStorage.getItem("councils") + ')');
			for (data=0;data<councils.length;data++){
				if (councils[data].userId == qualityVerifiedBy){
					result.innerText = result.innerText+"<td class='tdDatasetContent'>"+councils[data].name+"</td>";
					break;
				}
					
			}
			result.innerText = result.innerText+"<td class='tdDatasetContent'></td>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"</tbody>";
			result.innerText = result.innerText+"</table></div>";	
			
			result.innerText = result.innerText+"<div id='tablas'></div>";
			
			
			var selectedcity = sessionStorage.getItem("councilid");
			if (responsetext !=null && user != null &&  (user.managedCouncilId == selectedcity )){//|| user.admin
				result.innerText = result.innerText+"<form name='frmdataset' accept-charset='utf-8'>";
				//result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_new+"'  onclick=paintFrmDataset('')>";	
				result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_update+"' onclick=paintFrmUpdateDataset()>";
				result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_delete+"' onclick=deleteDataset()>";
				result.innerText = result.innerText+"<input type='button' value='"+languaje.forms_button_logs+"' onclick=viewLogsDataset()>";
				
				result.innerText = result.innerText+"</form>";
			}
			
			result.innerHTML =result.innerText ;
			
			url = '/IESCities/api/entities/datasets/'+dataset.datasetId+'/schema';
			//url = protocol+urlservicio+'api/entities/datasets/'+dataset.datasetId+'/info';
			xml2 = getHTTPObject();
			xml2.onreadystatechange = setDatasetDetails;
			xml2.open('GET',url, false)	;
			xml2.send();			
		}
	}
}
function getQuality(quality){
	switch(quality) {
    case 1:
        return languaje.forms_rating_1;
        break;
    case 2:
    	return languaje.forms_rating_2;
    	break;
    case 3:
    	return languaje.forms_rating_3;
        break;
    case 4:
    	return languaje.forms_rating_4;
        break;
    case 5:
    	return languaje.forms_rating_5;
        break;
    default:
    	return languaje.forms_rating_1;
	}
}

/**
 * Service that removes a dataset,  callback = procesarDeleteDatasets
 * @returns {Boolean}
 */
function deleteDataset(){
	var responseText = sessionStorage.getItem("dataset");
	var selecteddataset = eval('(' + responseText + ')');
	if (confirm('Are you sure you want to delete '+selecteddataset.name+'?')) {
		//url = protocol+ip+'/IESCities/api/datasets/deletedataset';
		//var params = "datasetid="+selecteddataset.datasetId;
		url = '/IESCities/api/entities/datasets/'+selecteddataset.datasetId;
		//url = protocol+urlservicio+'api/entities/datasets/'+selecteddataset.datasetId;
		jsouser = sessionStorage.getItem("user");
		var user = eval('(' + jsouser + ')');
		var auth = make_basic_auth(user.username,sessionStorage.getItem("password"));
		
		xml = getHTTPObject();
		xml.onreadystatechange = procesarDeleteDatasets;
		xml.open('DELETE',url,false );
		xml.setRequestHeader('Authorization', auth);
		
		xml.send();	
		return true;	    
	}
}
/**
 * Callback of deleteDataset
 */
function procesarDeleteDatasets(){
	var result = document.getElementById("content");
	if (xml.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		if (xml.status == "200"){

			
			document.location='index.html';
		}
		else {
			Alert.render(languaje.frm_applications_name_error);
			result.innerText =xml.responseText;
			result.innerHTML =result.innerText;
			document.location='index.html';
		}
	}
}
function viewLogsDataset(app){
	var result = document.getElementById("content");
	
	var responseText ="";
	var name ="";
	var dataset_id ="";
	if (app == undefined || app == null || app ==''){
		responseText = sessionStorage.getItem("dataset");
		selecteddataset = eval('(' + responseText + ')');
		name = selecteddataset.name;
		dataset_id = selecteddataset.datasetId;
	}
	else{
		responseText =sessionStorage.getItem("dataset");
		var datasets= eval('(' + responseText + ')');
		var dataset = datasets[app];
		
		name =application.name;
		dataset_id = dataset.datasetId;
	}
	result.innerText = "<h1>"+languaje.forms_button_logs+" "+name+"</h1>";
	result.innerText = result.innerText + "<div id='log1'></div>";
	result.innerHTML =result.innerText;

	url = '/IESCities/api/log/dataset/getquery';
	//url = protocol+urlservicio+'api/log/dataset/getquery';
	//http://150.241.239.65:8080/IESCities/api/log/dataset/getquery?dsid=376&city=326
	councilid = sessionStorage.getItem("councilid");
    params = 'dsid='+dataset_id+'&city='+councilid;
	
	xml = getHTTPObject();
	xml.onreadystatechange = viewLogData1;
	xml.open('GET',url+'?'+params,false );
	xml.send();	

	
	
}
/**
 * "city": 326,
    "datasetid": 376,
    "message": "none",
    "sessionid": 1223345,
    "timestamp": 1401878141.596,
    "type": "DatasetWrite"
  },
 */
function viewLogData1(){
	var result = document.getElementById("log1");
	if (xml.readyState == 4){
		//if (xml.status!="500" && xml.status!="403"){
		if (xml.status == "200"){

			//result.innerText = "<p>"+languaje.forms_logs_dataset_message+"</p> ";
			var log= eval('(' + xml.responseText + ')');
			if (log == null )  result.innerText = "<p>Logs</p>";
			else result.innerText = "<p>Logs: "+log.length+" </p>";
				
			result.innerText = result.innerText+"<table>";
			result.innerText = result.innerText+"<thead>";
			result.innerText = result.innerText+"<tr>";
			result.innerText = result.innerText+"<th>Timestamp</th>";
			result.innerText = result.innerText+"<th>Dataset ID</th>";
			result.innerText = result.innerText+"<th>"+languaje.forms_session_id+"</th>";
			result.innerText = result.innerText+"<th>"+languaje.forms_type+"</th>";
			result.innerText = result.innerText+"<th>"+languaje.forms_city+"</th>";
			result.innerText = result.innerText+"<th>"+languaje.forms_message+"</th>";
			result.innerText = result.innerText+"</tr>";
			result.innerText = result.innerText+"</thead>";
			result.innerText = result.innerText+"<tbody>";
			if (log == null )  result.innerText = result.innerText+"";
			else if (log.length != null){
				for (data=0;(data < log.length && data < 50);data++){
					result.innerText = result.innerText+"<tr>";
					result.innerText = result.innerText+"<td>"+log[data].timestamp+"</td>";			
					result.innerText = result.innerText+"<td >"+log[data].datasetid+"</td>";
					result.innerText = result.innerText+"<td>"+log[data].sessionid+"</td>";			
					result.innerText = result.innerText+"<td >"+log[data].type+"</td>";
					result.innerText = result.innerText+"<td>"+log[data].city+"</td>";			
					result.innerText = result.innerText+"<td >"+log[data].message+"</td>";
					result.innerText = result.innerText+"</tr>";
					
				}
			}
			result.innerText = result.innerText+"</tbody>";
			result.innerText = result.innerText+"</table>";
		}
		result.innerHTML =result.innerText;

	}
}


