lang_es = {
//Admin functions
	admin_management_features : "Funciones administrativas",
	manage_ies_cities_users : "Gestionar usuarios de IES Cities",
	admin_dashboard : "Consola de Administrador",
	council_management : "Gestión Ayuntamiento",
	manage_councils_users : "Gestión de usuarios de la ciudad",
	council_dashboard : "Consola de Ayuntamiento",	
	engagement_campaigns: "Engagement Campaigns",
//Index	
	returns : "Volver",
	contact_form : "Formulario de Contacto",
	select_new_city : "Elige nueva ciudad",
	forum :"Foro",
	learn_more_about : "Aprende más acerca de IES Cities",	
	form_message_read:"He leido las ", 
	term_of_use : "Condiciones de uso",
	read_terms_of_use : "Lee las Condiciones de Uso de este sitio",
	ies_cities_support : "Soporte de IES Cities",
	get_technical_suport : "Consigue soporte tecnico de IESCities",
	getting_started : "Empezando en IES Cities",
	access_manual : "Accede al manual de IES Cities",
	welcome_logged : "Bienvenido! Estas logueado como ",
	user_info_1 : "¿Te gustaría participar en el proceso de construcción de la identidad digital de tu ciudad?",
	user_info_2 : "Si es así, entra en tu ciudad para acceder a las aplicaciones y fuentes de datos.",
	menu_ies_twitter : "Twitter de IES Cities",
	menu_ies_fb : "Facebook de IES Cities",
	menu_ies_eu : "IES Cities Eu",
	footer_1 : "Este proyecto ha recibido fondos del Programa de innovación ",
	footer_2 : " y competitividad de la unión europea",
	footer_3 : " bajo acuerdo de subvención.",
//Applications
	applications_city : "Aplicaciones de la Ciudad",
	applications_app : "Aplicaciones",
	languaje : "Lenguaje",
//Datasets
	datasets_city: "Datases de la Ciudad",
	dataset_datasets : "Datasets",
//Dataset form tags
	frm_dataset_dat_name : "Nombre del Dataset",
	frm_dataset_new_dat : "Nuevo Dataset",
	frm_dataset_update_dat : "Modificar Dataset",
	frm_dataset_dat_name_ph: "Nombre del Dataset",
	frm_dataset_dat_description_ph: "Descripción del Dataset",
	frm_dataset_dat_json_mapping: "Mapeo Json",
	frm_dataset_dat_json_mapping_ph: "Información de Conexion en formato Json",
	frm_dataset_json_mapping_error : "La Información de Conexión en formato Json es obligatoria!",
	frm_dataset_json_mapping_invalid : "El formato de la información de conexión es invalido!",
//Datasets HOW TO
	frm_datasets_how_to_1 : "Cómo usar este Dataset.",
	frm_datasets_how_to_2 : "Si queremos desarrollar una aplicación usando los datasets de IES Cities, necesitamos seguir los siguientes pasos",
	frm_datasets_how_to_3 : "Lo primero será seleccionar el Dataset que nos interesa. El campo ‘Descripción’ explica qué datos tiene un Dataset específico",
	frm_datasets_how_to_4 : "El tipo de dataset es ",
	frm_datasets_how_to_5 : "Por tanto, si quieres consultar los datos deberás usar el siguiente servicio",
	frm_datasets_how_to_6 : "Para consultas 'update' e 'insert' deberás usar el siguiente servicio",		
	frm_datasets_how_to_7 : "Cuando ya hemos decidido que este dataset puede sernos de utilidad, ",
	frm_datasets_how_to_8 : "es importante anotar el ID y las tablas que podemos usar del Dataset elegido. El ID nos permitirá hacer queries y recuperar datos del dataset.",
	frm_datasets_how_to_9 : "Para consultar la API REST deberemos acceder a través de los links de la sección ‘How to use’ de cada Dataset",
	frm_datasets_how_to_10 : " acerca de como usar los Datasets",
	frm_datasets_how_to_11 : "Lista de tablas de este Dataset",
//Applications forms tags
	frm_applications_app_name : "Nombre de la aplicación",
	frm_applications_app_name_ph : "Nombre de aplicación",
	frm_applications_app_description_ph : "Descripción de la aplicación",
	frm_applications_app_permission_ph : "Permisos de la aplicación",
	frm_applications_app_url_ph : "URL de la applicación",
	frm_applications_app_image_ph : "URL de la imagen",
	frm_applications_new_app : "Nueva Aplicación",
	frm_applications_update_app : "Modificar Aplicación",
	frm_applications_url_patter : "Empezar con http",
	frm_applications_name_error : "¡El nombre es obligatorio!",
	frm_applications_description_error : "¡La descripción es obligatoria!",
	frm_applications_url_error : "¡La URL es obligatoria!",
	frm_applications_permissions_error : "¡Los permisos son obligatorios!",
	frm_applications_image_error : "¡La imagen es obligatoria!",
	frm_applications_version_error : "¡La versión es obligatoria!",
	frm_applications_tos_error : "¡Los términos de servicio son obligatorios!",
	frm_applications_tags_error : "¡Los tags son obligatorios!",
	frm_applications_package_error : "¡El paquete es obligatorio!",
	frm_applications_upload_error : "La creación ha fallado",
//Admin form tags
	frm_admin_user_name : "Nombre de Usuario",	
	frm_admin_pref_council: "Ciudad preferida",
	frm_admin_man_council:"Ciudad gestionada",
//User form	
	frm_user_apps: "Aplicaciones del usuario",
	frm_user_update_user: "Modificar Usuario",	
	frm_user_error_name:"¡Falta el Nombre!",
	frm_user_error_password:"¡Falta la contraseña o es menor de 5 caracteres!",
	frm_user_error_password_2:"¡Nueva contraseña menor de 5 caracteres!",
	frm_user_error_password_3:"¡La contraseña actual es inválida!",	
	frm_user_error_surname:"¡Falta el apellido!",
	frm_user_error_username:"¡Falta el nombre de usuario!",
	frm_user_error_email:"¡Falta el correo electrónico!",	
	frm_user_error_tos:"¡Acepte las condiciones de uso para continuar!",	
//Forms
	forms_button_new : "Nueva",
	forms_button_graph : "Graficos",
	forms_button_create : "Crear",
	forms_button_search : "Buscar",
	forms_button_update : "Actualizar",
	forms_button_delete : "Borrar",
	forms_button_logs : "Logs",	
	forms_button_view : "Ver",
	forms_button_set : "Asignar",
	forms_button_back: "Atras",
	forms_button_search_string : "Cadena de busqueda",
	forms_button_login : "Login",
	forms_button_logout : "Logout",
	forms_button_myapps : "Mis Aplicaciones",
	forms_button_change_profile : "Cambiar Perfil",
	forms_button_reset_password : "Cambiar Contraseña",
	forms_register: "Registrar",
	forms_name: "Nombre",
	forms_username: "Nombre de usuario",
	forms_password: "Contraseña",
	forms_current_password: "Contraseña actual",
	forms_new_password: "Nueva Contraseña",
	forms_surname: "Apellidos",
	forms_email: "Correo electrónico",
	forms_profile: "Perfil",
	forms_description : "Descripción",
	forms_url : "Url",
	forms_tags : "Tags",
	forms_actions : "Acciones",
	forms_permission : "Permisos",
	forms_image : "Imagen",
	forms_package_name : "Nombre del paquete",
	forms_version :"Versión",
	forms_tos : "Condiciones de Servicio",
	forms_geographical_scope :"Zona geográfica",
	forms_date:"Fecha",
	

	forms_session_id :"ID de sesión",
	forms_type :"Tipo",
	forms_duration : "Duración",
	forms_message : "Mensaje",	
	forms_city : "Ciudad",
	forms_used_dataset : "Dataset usado",
	forms_more_info : "Más información",  
	forms_more_info_api : "Más información acerca de la API",	
	forms_rating : "Rating",
	forms_rating_1 : "En WEB",
	forms_rating_2 : "Machine-readable data",
	forms_rating_3 : "Formatos no propietarios",
	forms_rating_4 : "Estándars RDF",
	forms_rating_5 : "Linked RDF",	
	forms_validatedby: "Validado por",
	forms_field :"Campo",
	forms_value :"Valor",	
	forms_areyousure_delete_message :"¿Estás seguro que quieres borrarlo?",
	forms_app_remove_message :"¡Aplicación borrada!",
	forms_app_not_remove_message :"¡Aplicación NO borrada!",
	forms_logs_app_message :"Logs de la aplicación",
	forms_logs_dataset_message :"Ratings del Dataset en la ciudad ",
	forms_admin_set_admin_message :"Asignar como administrador de la ciudad",
	forms_admin_change_admin_message: "¿Estás seguro de cambiar el estado de administrador del usuario?",	
	forms_delete_user_message: "¿Estás seguro de borrar al usuario?",
	forms_user_required: "¡El usuario es obligatorio!",
	forms_user_passshort_message:"¡Contraseña falta o demasiado corta!",
	forms_invalid_user_passshort_message:"Usuario o contraseña inválido",
	forms_mail_sent_message:"Se ha enviado un mensaje a tu correo, por favor sigue las instrucciones.",
	forms_applications_not_found_message:"No se han encontrado aplicaciones para este usuario",
	forms_user_update_message:"El campo `Current Password´ es obligatorio solo si desea cambiar la password.",
	forms_validated_by: "Validado por",
	form_users : "Usuarios",
	form_user : "Usuario",
	form_admin : "Administrador",
	form_developer : "Desarrollador",
	form_actions : "Acciones",

//Contact form
	form_contact_title:"Formulario de contacto de IES Cities",
	form_contact_glad:"Nos alegramos de que quieras contactar con ",
	form_contact_name:"Tu nombre y apellidos...",
	form_contact_email:"Tu dirección de Email...",
	form_contact_select_user_type:"Tipo de usuario",
	form_contact_select_citizen:"Ciudadano",
	form_contact_select_developer:"Desarrollador",
	form_contact_select_pc:"Compañía Privada",
	form_contact_select_pa:"Administración pública",	
	form_contact_select_our_lang:"Selecciona el lenguaje",
	form_contact_select_problem:"Selecciona el tipo de problema",
	form_contact_select_tech:"Problema técnico",
	form_contact_select_general:"Consulta general",
	form_contact_select_more:"Mas información acerca de IES Cities",	
	form_contact_type_name:"Escribe tu mensaje aquí...",
	form_contact_select_city:"Selecciona tu Ciudad"
	
};