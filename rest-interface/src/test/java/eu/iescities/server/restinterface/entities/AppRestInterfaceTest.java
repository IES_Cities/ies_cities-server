/*  
*   Author: Unai Aguilera <unai.aguilera@deusto.es>
*/
package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.AppRating;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;
import eu.iescities.server.restinterface.serialization.ApplicationInfo;

public class AppRestInterfaceTest extends JerseyTest {

	private static final String ZGZ_ADMIN_USER = "zgz_user";
    private static final String ZGZ_ADMIN_PASS = "zgz_pass";

    private static final String BRISTOL_ADMIN_USER = "bristol_user";
    private static final String BRISTOL_ADMIN_PASS = "bristol_pass";

    private static final String ROVERETO_ADMIN_USER = "rovereto_user";
    private static final String ROVERETO_ADMIN_PASS = "rovereto_pass";

    private static final String MAJADAHONDA_ADMIN_USER = "majadahonda_user";
    private static final String MAJADAHONDA_ADMIN_PASS = "majadahonda_pass";
    
    private static final String NORMAL_USER = "some_user";
    private static final String NORMAL_USER_PASS = "some_pass";
    
    private static List<Integer> registeredApps = new ArrayList<Integer>();

    private static Council zgzCouncil = new Council();
    private static GeoScope zgzGeoScope;
    
    private static User normalUser;
    
    private static Dataset dataset;
    
    @Rule 
    public ContiPerfRule rule = new ContiPerfRule();

    @Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	private static int registerApp(String userSession, int id, Council council, String[] tags, String packageName) {
        Application tempApp = new Application();
        tempApp.setName("Test app " + id);
        tempApp.setDescription("Description of the test app " + id);
        tempApp.setUrl("http://iescities.eu/" + id);
        tempApp.setImage("http://server.iescities.eu/testApp" + id + ".jpg");
        tempApp.setRelatedCouncilId(council.getCouncilId());
        tempApp.setVersion("0.1");
        tempApp.setTermsOfService("Terms of Service for app " + id);
        tempApp.setPermissions("Permisions of test app " + id);
        tempApp.setPackageName(packageName + "." + id);
        tempApp.setTags(new HashSet<String>(Arrays.asList(tags)));
        tempApp.setGeographicalScope(council.getGeographicalScope());
        tempApp.setLang(Language.EN);
        
        return ApplicationManagement.registerApp(userSession, tempApp).getAppId();
    }

    /**
     * Executed before the test suite of this class.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    	TestUtils.cleanUpDatastore();
        
    	TestUtils.addAdminUser().getUserId();
        String adminSession = TestUtils.loginAdminUser();
        
        zgzGeoScope = GeoScopeManagement.createScope(
                adminSession, "Zaragoza Region",
                new GeoPoint(41.6894079, -0.8427317999999999),
                new GeoPoint(41.6139746, -0.9472301000000001));
        
        zgzCouncil.setName("Zaragoza");
        zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
        zgzCouncil.setGeographicalScope(zgzGeoScope);
        zgzCouncil.setLang(Language.EN);
        zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

        final User zgzAdminUser = new User();
        zgzAdminUser.setUsername(ZGZ_ADMIN_USER);
        zgzAdminUser.setPassword(ZGZ_ADMIN_PASS);
        zgzAdminUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
        int zgzAdminUserId = UserManagement.registerUser(zgzAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, zgzCouncil.getCouncilId(), zgzAdminUserId);

        final GeoScope bristolGeoScope = GeoScopeManagement.createScope(
                adminSession, "Bristol Region",
                new GeoPoint(51.54443269999999, -2.4509025),
                new GeoPoint(51.3925452, -2.7305165));
        
        Council bristolCouncil = new Council();
        bristolCouncil.setName("Bristol");
        bristolCouncil.setDescription("Bristol Council");
        bristolCouncil.setGeographicalScope(bristolGeoScope);
        bristolCouncil.setLang(Language.EN);
        bristolCouncil = CouncilManagement.createCouncil(adminSession, bristolCouncil);

        final User bristolAdminUser = new User();
        bristolAdminUser.setUsername(BRISTOL_ADMIN_USER);
        bristolAdminUser.setPassword(BRISTOL_ADMIN_PASS);
        bristolAdminUser.setPreferredCouncilId(bristolCouncil.getCouncilId());
        int bristolAdminId = UserManagement.registerUser(bristolAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, bristolCouncil.getCouncilId(), bristolAdminId);

        final GeoScope roveretoGeoScope = GeoScopeManagement.createScope(
                adminSession, "Rovereto Region",
                new GeoPoint(45.91260949999999, 11.0738002),
                new GeoPoint(45.85202, 10.9991401));
        Council roveretoCouncil = new Council();
        
        roveretoCouncil.setName("Rovereto");
        roveretoCouncil.setDescription("Rovereto Council");
        roveretoCouncil.setGeographicalScope(roveretoGeoScope);
        roveretoCouncil.setLang(Language.EN);
        roveretoCouncil = CouncilManagement.createCouncil(adminSession, roveretoCouncil);

        final User roveretoAdminUser = new User();
        roveretoAdminUser.setUsername(ROVERETO_ADMIN_USER);
        roveretoAdminUser.setPassword(ROVERETO_ADMIN_PASS);
        roveretoAdminUser.setPreferredCouncilId(roveretoCouncil.getCouncilId());
        int roveretoAdminId = UserManagement.registerUser(roveretoAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, roveretoCouncil.getCouncilId(), roveretoAdminId);

        final GeoScope majadahondaGeoScope = GeoScopeManagement.createScope(
                adminSession, "Majadahonda Region",
                new GeoPoint(40.4837203, -3.8448064),
                new GeoPoint(40.4450864, -3.8880567));
        
        Council majadahondaCouncil = new Council();
        majadahondaCouncil.setName("Majadahonda");
        majadahondaCouncil.setDescription("Majadahonda Council");
        majadahondaCouncil.setGeographicalScope(majadahondaGeoScope);
        majadahondaCouncil.setLang(Language.EN);
        majadahondaCouncil = CouncilManagement.createCouncil(adminSession, majadahondaCouncil);

        final User majadahondaAdminUser = new User();
        majadahondaAdminUser.setUsername(MAJADAHONDA_ADMIN_USER);
        majadahondaAdminUser.setPassword(MAJADAHONDA_ADMIN_PASS);
        majadahondaAdminUser.setPreferredCouncilId(majadahondaCouncil.getCouncilId());
        int majadahondaAdminId = UserManagement.registerUser(majadahondaAdminUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, majadahondaCouncil.getCouncilId(), majadahondaAdminId);
        
        normalUser = new User();
        normalUser.setUsername(NORMAL_USER);
        normalUser.setPassword(NORMAL_USER_PASS);
        normalUser = UserManagement.registerUser(normalUser);
        
        UserManagement.logout(adminSession);
		
        /////////// Apps //////////////        
		
        String userSession = UserManagement.login(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS);
        registeredApps.add(registerApp(userSession, 1, zgzCouncil, new String[]{"Zaragoza-EN", "311"}, "com.tuenti.messenger"));
        
        UserManagement.logout(userSession);
		
        userSession = UserManagement.login(BRISTOL_ADMIN_USER, BRISTOL_ADMIN_PASS);		
        registeredApps.add(registerApp(userSession, 2, bristolCouncil, new String[]{"Bristol", "311"}, "com.tuenti.messenger"));
        UserManagement.logout(userSession);
								
        userSession = UserManagement.login(ROVERETO_ADMIN_USER, ROVERETO_ADMIN_PASS);
        registeredApps.add(registerApp(userSession, 3, roveretoCouncil, new String[]{"Rovereto", "Cycling"}, "com.facebook.katana"));
        UserManagement.logout(userSession);

        userSession = UserManagement.login(MAJADAHONDA_ADMIN_USER, MAJADAHONDA_ADMIN_PASS);
        String description = "The service allows users to obtain information of the " +
                "leisure and cultural offer of the city, both private and public, " +
                "filtered by their preferences. The user gets all the information of " +
                "the selected event (date, time, price, geolocation, availability, " + 
                "type of target user, etc.). In addition, the service sends alerts to " + 
                "registered users which fit their profile.";
		
        Application tempApp = new Application();
        tempApp.setName("Majadahonda Leisure & Events");
        tempApp.setDescription(description);
        tempApp.setUrl("http://iescities.eu");
        tempApp.setImage("http://server.iescities.eu/testApp.jpg");
        tempApp.setRelatedCouncilId(majadahondaCouncil.getCouncilId());
        tempApp.setVersion("0.1");
        tempApp.setTermsOfService("Terms of Service for app 1");
        tempApp.setPermissions("Permisions of service 1");
        tempApp.setPackageName("com.example.viveMajadahonda");
        tempApp.setTags(new HashSet<String>(Arrays.asList(new String[]{"Majadahonda", "leisure", "events", "citizens", "shops", "shopping"})));
        tempApp.setGeographicalScope(majadahondaCouncil.getGeographicalScope());
        tempApp.setLang(Language.EN);
        
        final Application app = ApplicationManagement.registerApp(userSession, tempApp);
        
        registeredApps.add(app.getAppId());
        
        dataset = new Dataset();
		dataset.setName("test_dataset");
		dataset.setJsonMapping("");
		dataset.setLang(Language.EN);
		dataset = DatasetManagement.registerDataset(userSession, dataset);
		ApplicationManagement.addDataset(userSession, app.getAppId(), dataset.getDatasetId());
       
        UserManagement.logout(userSession);
    }

    /**
     * Executed after the test suite of this class.
     * 
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        TestUtils.cleanUpDatastore();
    }
 
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetApp() {
        int appId = registeredApps.get(0).intValue();

        Application app = target("/entities/apps/" + appId)
                            .request(MediaType.APPLICATION_JSON)
                            .get(Application.class);

        assertEquals(appId, app.getAppId());
    }

    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetInvalidAppId() {
        Response response = target("/entities/apps/" + Integer.MAX_VALUE)
                            .request(MediaType.APPLICATION_JSON)
                            .get(Response.class);
        
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetNonExistingAppId() {
        Response response = target("/entities/apps/" + Integer.MAX_VALUE)
                            .request(MediaType.APPLICATION_JSON)
                            .get(Response.class);
        
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplications() {    	
        List<Application> applications = target("/entities/apps/all")
                        .request(MediaType.APPLICATION_JSON)
                        .get(new GenericType<List<Application>>(){});
        
        assertEquals(4, applications.size());
        
        for (int i = 1; i < 4; i++) {
        	Application app = applications.get(i - 1);
        	assertEquals("Test app " + i, app.getName());
        	assertEquals("Description of the test app " + i, app.getDescription());
        	assertEquals("http://iescities.eu/" + i, app.getUrl());
        	assertEquals("http://server.iescities.eu/testApp" + i + ".jpg", app.getImage());
        	assertEquals("0.1", app.getVersion());
        	assertEquals("Terms of Service for app " + i, app.getTermsOfService());
        	assertEquals("Permisions of test app " + i, app.getPermissions());
        }
        
        assertEquals("com.tuenti.messenger.1", applications.get(0).getPackageName());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsInvalidLimit() {    	
        Response response = target("/entities/apps/all")
                        .queryParam("limit", 2000)
        				.request(MediaType.APPLICATION_JSON)
                        .get(Response.class);
        
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsNearZaragoza() {
        List<Application> response = target("/entities/apps/near/41.6333333,-0.8833333/5")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});
        
        assertEquals(1, response.size());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsNearBilbao() {
    	List<Application> response = target("/entities/apps/near/43.25,-2.9666667")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});

       assertTrue(response.isEmpty());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsNearMajadahondaWithSmallRadius() {
        List<Application> response = target("/entities/apps/near/40.4666667,-3.8666667/5")
                            .request(MediaType.APPLICATION_JSON)
                            .get(new GenericType<List<Application>>(){});
        
        assertEquals(1, response.size());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsInvalidRadius() {
        Response response = target("/entities/apps/near/40.4666667,-3.8666667/something")
                            .request(MediaType.APPLICATION_JSON)
                            .get(Response.class);
        
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testGetAllApplicationsNearInvalidCoordinates() {
        Response response = target("/entities/apps/near/aaa,-3.8666667/5")
                            .request(MediaType.APPLICATION_JSON)
                            .get(Response.class);
        
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testSearchApplicationsMatching311() {
        List<Application> response = target("/entities/apps/search/")
        					.queryParam("keywords", "311")
                            .request(MediaType.APPLICATION_JSON)
                            .get(new GenericType<List<Application>>(){});
        
        assertEquals(2, response.size());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testSearchApplicationsMatchingBristol() {
        List<Application> response = target("/entities/apps/search/")
        					.queryParam("keywords", "Bristol")
                            .request(MediaType.APPLICATION_JSON)
                            .get(new GenericType<List<Application>>(){});
        
        assertEquals(1, response.size());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testSearchApplicationsMatchingZaragozaAndRovereto() {
        List<Application> response = target("/entities/apps/search/")
        					.queryParam("keywords", "Zaragoza Rovereto")
                            .request(MediaType.APPLICATION_JSON)
                            .get(new GenericType<List<Application>>(){});
        
        assertEquals(1, response.size());
    }   
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testSearchApplicationsInvalidLimit() {
        Response response = target("/entities/apps/search/")
        					.queryParam("keywords", "Zaragoza Rovereto")
        					.queryParam("limit", 2000)
                            .request(MediaType.APPLICATION_JSON)
                            .get(Response.class);
        
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }
 
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
    public void testSearchApplicationsMatchingBond() {
        List<Application> response = target("/entities/apps/search/")
        		.queryParam("keywords", "Bond")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});

        assertTrue(response.isEmpty());
    }

    @Test
    public void testCreateApplication() {
        client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));
        
        ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setName("test_app_name");
        appInfo.setDescription("Some long description");
        appInfo.setUrl("http://iescities.eu/");
        appInfo.setImage("http://someimage.com/image");
        appInfo.setVersion("1.0");
        appInfo.setTos("thetos");
        appInfo.setPermissions("some_permissions");
        appInfo.setPackageName("thepackagename");
        Set<String> tags = new HashSet<String>();
        tags.add("tag1");
        tags.add("tag2");
        appInfo.setTags(tags);
        appInfo.setCouncilid(zgzCouncil.getCouncilId());
        appInfo.setScopeid(zgzGeoScope.getId());
        appInfo.setLang(Language.EN);
        
        Entity<ApplicationInfo> entity = Entity.entity(appInfo, MediaType.APPLICATION_JSON);
        Application application = target("/entities/apps/")
            .request(MediaType.APPLICATION_JSON)
            .post(entity, Application.class);

        assertEquals("test_app_name", application.getName());
        assertEquals("Some long description", application.getDescription());
        assertEquals("http://iescities.eu/", application.getUrl());
        assertEquals("http://someimage.com/image", application.getImage());
        assertEquals("1.0", application.getVersion());
        assertEquals("thetos", application.getTermsOfService());
        assertEquals("some_permissions", application.getPermissions());
        assertEquals("thepackagename", application.getPackageName());

        Set<String> expectedTags = new HashSet<String>();
        expectedTags.add("tag1");
        expectedTags.add("tag2");
        assertEquals(expectedTags, application.getTags());
        
        // Remove app
        final String userSession = UserManagement.login(NORMAL_USER, NORMAL_USER_PASS);
        ApplicationManagement.removeApp(userSession, application.getAppId());
        UserManagement.logout(userSession);
    }
    
    @Test
    public void testCreateApplicationNameAlreadyUsed() {
        client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));
        
        ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setName("Majadahonda Leisure & Events");
        appInfo.setDescription("Some long description");
        appInfo.setUrl("http://iescities.eu/");
        appInfo.setImage("http://someimage.com/image");
        appInfo.setVersion("1.0");
        appInfo.setTos("thetos");
        appInfo.setPermissions("some_permissions");
        appInfo.setPackageName("thepackagename");
        Set<String> tags = new HashSet<String>();
        tags.add("tag1");
        tags.add("tag2");
        appInfo.setTags(tags);
        appInfo.setCouncilid(zgzCouncil.getCouncilId());
        appInfo.setScopeid(zgzGeoScope.getId());
        appInfo.setLang(Language.EN);
        
        Entity<ApplicationInfo> entity = Entity.entity(appInfo, MediaType.APPLICATION_JSON);
        Response response = target("/entities/apps/")
            .request(MediaType.APPLICATION_JSON)
            .post(entity, Response.class);
        
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testUpdateApp() {
        client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));

        ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setName("new_app_name");
        appInfo.setDescription("Some long description");
        appInfo.setUrl("http://iescities.eu/");
        appInfo.setImage("http://someimage.com/image");
        appInfo.setVersion("1.0");
        appInfo.setTos("thetos");
        appInfo.setPermissions("some_permissions");
        appInfo.setPackageName("the.package.name");
        Set<String> tags = new HashSet<String>();
        tags.add("tag1");
        tags.add("tag2");
        appInfo.setTags(tags);
        appInfo.setCouncilid(zgzCouncil.getCouncilId());
        appInfo.setScopeid(zgzGeoScope.getId());
        appInfo.setLang(Language.EN);
        
        Entity<ApplicationInfo> entity = Entity.entity(appInfo, MediaType.APPLICATION_JSON);
        Application application = target("/entities/apps/")
            .request(MediaType.APPLICATION_JSON)
            .post(entity, Application.class);
        
        application.setName("updated_name");
        
        Entity<Application> updateEntity = Entity.entity(application, MediaType.APPLICATION_JSON);
        
        application = target("/entities/apps/" + application.getAppId())
        		.request(MediaType.APPLICATION_JSON)
        		.put(updateEntity, Application.class);
        
        assertEquals("updated_name", application.getName());
        
        // Remove app
        final String userSession = UserManagement.login(NORMAL_USER, NORMAL_USER_PASS);
        ApplicationManagement.removeApp(userSession, application.getAppId());
        UserManagement.logout(userSession);
    }
    
    @Test
    public void testUpdateAppInvalidID() {
        client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));

        Application application = new Application();
        application.setName("app_name");
        
        Entity<Application> updateEntity = Entity.entity(application, MediaType.APPLICATION_JSON);
        
        Response response = target("/entities/apps/" + Integer.MAX_VALUE)
        		.request(MediaType.APPLICATION_JSON)
        		.put(updateEntity, Response.class);
        
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteApplication() {
    	client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));

    	ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setName("temp_app");
        appInfo.setDescription("Some long description");
        appInfo.setUrl("http://iescities.eu/");
        appInfo.setImage("http://someimage.com/image");
        appInfo.setVersion("1.0");
        appInfo.setTos("thetos");
        appInfo.setPermissions("some_permissions");
        appInfo.setPackageName("thepackagename");
        Set<String> tags = new HashSet<String>();
        tags.add("tag1");
        tags.add("tag2");
        appInfo.setTags(tags);
        appInfo.setCouncilid(zgzCouncil.getCouncilId());
        appInfo.setScopeid(zgzGeoScope.getId());
        appInfo.setLang(Language.EN);
        
        Entity<ApplicationInfo> entity = Entity.entity(appInfo, MediaType.APPLICATION_JSON);

        Application application = target("/entities/apps/")
            .request(MediaType.APPLICATION_JSON)
            .post(entity, Application.class);

    	Response response = target("/entities/apps/" + application.getAppId())
    		.request(MediaType.APPLICATION_JSON)
    		.delete(Response.class);

    	assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteApplicationInvalidAppId() {
    	client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

        Response response = target("/entities/apps/" + Integer.MAX_VALUE)
            .request(MediaType.APPLICATION_JSON)
            .delete(Response.class);

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testDeleteApplicationInvalidUser() {
    	client().register(HttpAuthenticationFeature.basic(NORMAL_USER, NORMAL_USER_PASS));
    	
        Response response = target("/entities/apps/" + registeredApps.get(3))
            .request(MediaType.APPLICATION_JSON)
            .delete(Response.class);
        
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDatasets() {
		List<Dataset> datasets = target("/entities/apps/" + registeredApps.get(3) + "/datasets")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Dataset>>(){});
		
		assertEquals(1, datasets.size());
	}
    
    @Test
    @PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDatasetsInvalidLimit() {
		Response response = target("/entities/apps/" + registeredApps.get(3) + "/datasets")
				.queryParam("limit", 2000)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDatasetsInvalidID() {
		Response response = target("/entities/apps/" + Integer.MAX_VALUE + "/datasets")
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDevelopers() {
		List<User> developers = target("/entities/apps/" + registeredApps.get(3) + "/developers")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<User>>(){});
		
		assertEquals(1, developers.size());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDevelopersInvalidLimit() {
		Response response = target("/entities/apps/" + registeredApps.get(3) + "/developers")
				.queryParam("limit", 2000)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppDevelopersInvalidID() {
		Response response = target("/entities/apps/" + Integer.MAX_VALUE + "/developers")
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetRatings() {		
		List<AppRating> ratings = target("/entities/apps/" + registeredApps.get(3) + "/ratings")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<AppRating>>(){});
		
		assertEquals(0, ratings.size());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetRatingsInvalidLimit() {		
		Response response = target("/entities/apps/" + registeredApps.get(3) + "/ratings")
				.queryParam("limit", 2000)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAppRatingsInvalidID() {
		Response response = target("/entities/apps/" + Integer.MAX_VALUE + "/ratings")
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testAddDeveloper() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));
		
		User newDeveloper = new User();
		newDeveloper.setUserId(normalUser.getUserId());
		
		Entity<User> entity = Entity.entity(newDeveloper, MediaType.APPLICATION_JSON);
		
		int appId = registeredApps.get(0);
		assertEquals(1, ApplicationManagement.getAllDevelopersOfApp(appId, 0, 1000).size());
		
		Response response = target("/entities/apps/" + appId + "/developers")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		assertEquals(2, ApplicationManagement.getAllDevelopersOfApp(appId, 0, 1000).size());
	}
	
	@Test
	public void testAddDeveloperAppNotFound() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));
		
		User newDeveloper = new User();
		newDeveloper.setUserId(normalUser.getUserId());
		
		Entity<User> entity = Entity.entity(newDeveloper, MediaType.APPLICATION_JSON);
		
		Response response = target("/entities/apps/" + Integer.MAX_VALUE + "/developers")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testAddDataset() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));
		
		Dataset newDataset = new Dataset();
		newDataset.setDatasetId(dataset.getDatasetId());
		
		Entity<Dataset> entity = Entity.entity(newDataset, MediaType.APPLICATION_JSON);
		
		int appId = registeredApps.get(0);
		assertEquals(0, ApplicationManagement.getAllDatasetsOfApp(appId, 0, 1000, Language.EN, null).size());
		
		Response response = target("/entities/apps/" + appId + "/datasets")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		assertEquals(1, ApplicationManagement.getAllDatasetsOfApp(appId, 0, 1000, Language.EN, null).size());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAddDatasetInvalidApp() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));
		
		Dataset newDataset = new Dataset();
		newDataset.setDatasetId(dataset.getDatasetId());
		
		Entity<Dataset> entity = Entity.entity(newDataset, MediaType.APPLICATION_JSON);
		
		Response response = target("/entities/apps/" + Integer.MAX_VALUE + "/datasets")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	private int countLangApps(List<Application> apps, Language lang) {
		int total = 0;
		for (Application app : apps) {
			if (app.getLang() == lang) {
				total++;
			}
		}
		
		return total;
	}
	
	@Test
	public void testAddLanguage() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));
		
		final Application appSpanish = addLanguage(Language.ES);
        assertTrue(registeredApps.get(0) == appSpanish.getAppId());
        
        final Application appItalian = addLanguage(Language.IT);
        assertTrue(registeredApps.get(0) == appItalian.getAppId());
            	
        final List<Application> defaultLanguageApps = target("/entities/apps/all")
        		.request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});
            
        assertEquals(4, defaultLanguageApps.size());
        
        final List<Application> englishApp = target("/entities/apps/all")
        		.queryParam("lang", "EN")
        		.request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});
            
        assertEquals(4, englishApp.size());
        
        final List<Application> spanishApps = target("/entities/apps/all")
        		.queryParam("lang", "ES")
        		.request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});
            
        assertEquals(4, spanishApps.size());
        assertEquals(1, countLangApps(spanishApps, Language.ES));
        assertEquals(3, countLangApps(spanishApps, Language.EN));
        
        final List<Application> italianApps = target("/entities/apps/all")
        		.queryParam("lang", "IT")
        		.request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Application>>(){});
            
        assertEquals(4, italianApps.size());
        assertEquals(1, countLangApps(italianApps, Language.IT));
        assertEquals(3, countLangApps(italianApps, Language.EN));
	}

	private Application addLanguage(Language lang) {
		ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setName("Alternative test_app_name");
        appInfo.setDescription("Alternative Some long description");
        appInfo.setLang(lang);
        
        final Entity<ApplicationInfo> updateEntity = Entity.entity(appInfo, MediaType.APPLICATION_JSON);
        
        final Application application = target("/entities/apps/" + registeredApps.get(0))
        		.request(MediaType.APPLICATION_JSON)
        		.put(updateEntity, Application.class);
		return application;
	}
}