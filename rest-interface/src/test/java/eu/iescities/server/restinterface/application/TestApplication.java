package eu.iescities.server.restinterface.application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import eu.iescities.server.restinterface.auth.SecurityRequestFilter;

public class TestApplication extends ResourceConfig {

	public TestApplication() {
		packages("eu.iescities.server.restinterface");
        
        register(RolesAllowedDynamicFeature.class);
        register(SecurityRequestFilter.class);
    }
}