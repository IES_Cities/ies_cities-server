/*  
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.loggingrest.DatasetLog;
import eu.iescities.server.loggingrest.DatasetLogging;
import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.database.DBConnector;
import eu.iescities.server.querymapper.datasource.database.DBDataSource;
import eu.iescities.server.querymapper.datasource.database.pool.PoolManager;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaDataSource;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.querymapper.util.scheduler.DataDownloadJob;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.query.QueryRestInterface;
import eu.iescities.server.restinterface.serialization.query.UpdateResult;
import util.DBUtility;

public class UpdateRestInterfaceTest extends JerseyTest {

	private static final String TEST_USER = "user_1";
	private static final String TEST_PASS = "secret";
	
	private static final String INVALID_USER = "user_2";

	private static Dataset invalidDataset;
	private static Dataset updateableDataset, updateableDataset2;
	private static Dataset sparqlDataset, jsonDataset, userDataset;
	
	private static Council zgzCouncil;

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@Before
	public void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();
		DBUtility.createDatabase();
		
		PoolManager.getInstance().clear();
		Config.getInstance().deleteDBDir();
		Config.getInstance().createDataDir();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		final GeoScope zgzGeoScope = GeoScopeManagement.createScope(
				adminSession, "Zaragoza Region", new GeoPoint(41.6894079,
						-0.8427317999999999), new GeoPoint(41.6139746,
						-0.9472301000000001));

		zgzCouncil = new Council();
		zgzCouncil.setName("Zaragoza");
		zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
		zgzCouncil.setGeographicalScope(zgzGeoScope);
		zgzCouncil.setLang(Language.EN);
		zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

		final User testUser = new User();
		testUser.setUsername(TEST_USER);
		testUser.setPassword(TEST_PASS);
		testUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		int defaultUser = UserManagement.registerUser(testUser).getUserId();
		
		final User invalidUser = new User();
		invalidUser.setUsername(INVALID_USER);
		invalidUser.setPassword(TEST_PASS);
		invalidUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		UserManagement.registerUser(invalidUser).getUserId();

		CouncilManagement.addCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), defaultUser);

		UserManagement.logout(adminSession);

		String userSession = UserManagement.login(TEST_USER, TEST_PASS);

		// Register updateable SQL dataset		
		String jsonSchema = FileLoader.loadResource("/security/dbconfig/sqlite_update_conf.json");

		Dataset dataset = new Dataset();
		dataset.setName("updateable_dataset");
		dataset.setDescription("This dataset provides an example of an sql dataset");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		updateableDataset = DatasetManagement.registerDataset(userSession,
				dataset);

		dataset = new Dataset();
		dataset.setName("updateable_dataset2");
		dataset.setDescription("This dataset provides an example of an sql dataset");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		updateableDataset2 = DatasetManagement.registerDataset(userSession,
				dataset);

		// Register invalid description		
		jsonSchema = FileLoader.loadResource("/dbconfig/invalid.json");

		dataset = new Dataset();
		dataset.setName("invalid");
		dataset.setDescription("This dataset provides an example of an invalid dataset");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		invalidDataset = DatasetManagement
				.registerDataset(userSession, dataset);
		
		// Register sparql dataset
		jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");
		dataset = new Dataset();
		dataset.setName("sparql_dataset");
		dataset.setDescription("This dataset provides an example of an updateable sparql dataset");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		sparqlDataset = DatasetManagement.registerDataset(userSession, dataset);
		
		// Register json backed dataset		
		jsonSchema = FileLoader.loadResource("/json/mapping/jsonmapping1.json");

		dataset = new Dataset();
		dataset.setName("json_dataset");
		dataset.setDescription("This dataset provides an example of dataset that is backed by a JSON resource file");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		jsonDataset = DatasetManagement.registerDataset(userSession, dataset);
		final DataSourceConnector connector = ConnectorFactory.load(jsonDataset.getJsonMapping());
		DataDownloadJob dataDownloadJob = new DataDownloadJob();
		dataDownloadJob.execute(connector, new Integer(jsonDataset.getDatasetId()).toString());
		
		// Register json backed dataset		
		jsonSchema = FileLoader.loadResource("/json_schema/mapping/jsonmapping1.json");

		dataset = new Dataset();
		dataset.setName("user_dataset");
		dataset.setDescription("Example of user created dataset");
		dataset.setJsonMapping(jsonSchema);
		dataset.setLang(Language.EN);
		userDataset = DatasetManagement.registerDataset(userSession, dataset);
		final DataSourceConnector userConnector = ConnectorFactory.load(userDataset.getJsonMapping());
		new JSONSchemaDataSource((JSONSchemaConnector) userConnector, new Integer(userDataset.getDatasetId()).toString());
	}

	@Test
	public void testExecuteSQLUpdate() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(
				String.format("data/update/%s/sql",
						updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(sqlInsert,
				UpdateResult.class);

		assertEquals(1, result.getRows());

		Entity<String> updateEntity = Entity.entity("DELETE FROM people where name = 'test_name';",
				MediaType.TEXT_PLAIN);

		result = target(
				String.format("data/update/%s/sql",
						updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);

		assertTrue(result.getRows() > 0);
	}
	
	@Test(expected=BadRequestException.class)
	public void testExecuteSQLUpdateInvalidTransactionNotEnabled() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		target(String.format("data/update/%s/sql",
				updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(sqlInsert,
				UpdateResult.class);
	}
	
	@Test
	public void testExecuteSQLUpdateTransaction() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(String.format("data/update/%s/sql",
				updateableDataset.getDatasetId()))
				.queryParam("transaction", Boolean.TRUE)
				.request(MediaType.APPLICATION_JSON)
				.post(sqlInsert, UpdateResult.class);
		
		assertEquals(2, result.getRows());
	}
	
	@Test
	public void testExecuteSQLUpdateInvalidDatasetID() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/update/%s/sql",
			Integer.MAX_VALUE)).request(
			MediaType.APPLICATION_JSON).post(sqlInsert,
			Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
	
	@Test(expected=ForbiddenException.class)
	public void testExecuteSQLUpdateJSONInvalidUser() throws IOException {
		client().register(HttpAuthenticationFeature.basic(INVALID_USER, TEST_PASS));
		
		String jsonld = FileLoader.loadResource("/json/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);
		
		target(String.format("data/update/%s/json",
			updateableDataset.getDatasetId())).request(
			MediaType.APPLICATION_JSON).post(updateEntity,
			UpdateResult.class);
	}
	
	@Test(expected=ForbiddenException.class)
	public void testExecuteSQLUpdateInvalidUser() {
		client().register(HttpAuthenticationFeature.basic(INVALID_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		target(String.format("data/update/%s/sql",
			updateableDataset.getDatasetId())).request(
			MediaType.APPLICATION_JSON).post(sqlInsert,
			UpdateResult.class);
	}

	@Test
	public void testLoggingSQLUpdate() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		DatasetLogging datasetLogging = new DatasetLogging();
		List<DatasetLog> datasetLogs = datasetLogging.getQuery(
				updateableDataset2.getDatasetId(), QueryRestInterface.DEFAULT_COUNCIL_ID, "DatasetWrite", "none", "none", "none");

		assertEquals(0, datasetLogs.size());

		try {
			Entity<String> dropEntity = Entity.entity("drop table test",
					MediaType.TEXT_PLAIN);

			UpdateResult result = target(
					String.format("data/update/%s/sql",
							updateableDataset2.getDatasetId())).request(
					MediaType.APPLICATION_JSON).post(dropEntity,
					UpdateResult.class);

			assertEquals(0, result.getRows());

		} catch (BadRequestException e) {
			// Table did not exist
		}

		datasetLogging = new DatasetLogging();
		datasetLogs = datasetLogging.getQuery(
				updateableDataset2.getDatasetId(), zgzCouncil.getCouncilId(), "DatasetWrite", "none", "none", "none");

		assertEquals(1, datasetLogs.size());

		Entity<String> sqlInsert = Entity.entity("insert into people (id, name) values (null, 'test_name');",
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(
				String.format("data/update/%s/sql",
						updateableDataset2.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(sqlInsert,
				UpdateResult.class);

		assertEquals(1, result.getRows());

		datasetLogging = new DatasetLogging();
		datasetLogs = datasetLogging.getQuery(
				updateableDataset2.getDatasetId(), zgzCouncil.getCouncilId(), "DatasetWrite", "none", "none", "none");

		assertEquals(2, datasetLogs.size());
		
		datasetLogs = datasetLogging.getQuery(
				updateableDataset2.getDatasetId(), QueryRestInterface.DEFAULT_COUNCIL_ID, "DatasetWrite", "none", "none", "none");

		assertEquals(0, datasetLogs.size());
	}

	@Test
	public void testInvalidDBConfiguration() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		String sqlInsert = "insert into country (id, name) values (null, 'test_country');";

		Entity<String> updateEntity = Entity.entity(sqlInsert,
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/update/%s/sql",
						invalidDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity, Response.class);

		assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	@Ignore
	public void testExecuteJSONLDUpdate() throws IOException {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/jsonld/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(
				String.format("data/update/%s/jsonld",
						sparqlDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);

		assertEquals(0, result.getRows());
		
		String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");
		SPARQLConnector schema = SPARQLConnector.load(jsonSchema);
		
		UpdateRequest request = new UpdateRequest();
		request.add("DELETE WHERE { <http://www.morelab.deusto.es/resource/unai-aguilera#newentry> ?p ?o }");
		
		UpdateProcessor p = UpdateExecutionFactory.createRemote(request, schema.getUpdateEndpoint());
		p.execute();
	}

	@Test
	public void testExecuteJSONLDUpdateInvalidJSONLD() throws IOException {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/jsonld/update/error_update.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/update/%s/jsonld",
				sparqlDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteJSONLDUpdateIncompatibleDataset() throws IOException {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/jsonld/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/update/%s/jsonld",
						updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test(expected=BadRequestException.class)
	public void testExecuteJSONLDUpdateError() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException {		
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/json/update/error_update.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		target(String.format("data/update/%s/jsonld",
				updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);
	}
	
	@Test
	public void testExecuteJSONLDUpdateInvalidDatasetID() throws IOException {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/jsonld/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/update/%s/jsonld",
				Integer.MAX_VALUE)).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteJSONUpdate() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException, DataSourceConnectorException {		
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/json/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(
				String.format("data/update/%s/json",
						updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);

		assertEquals(2, result.getRows());
		
		String jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_update_conf.json");
		DBConnector dbConfig = (DBConnector) ConnectorFactory.load(jsonMapping);
		
		SQLUpdateable updateDatabase = new DBDataSource(dbConfig, false);
		updateDatabase.executeSQLUpdate("DELETE FROM people WHERE occupation = 'physicist';", "user_1");
	}
	
	@Test(expected=BadRequestException.class)
	public void testExecuteJSONUpdateInvalid() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException {		
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/json/update/error_update.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		target(String.format("data/update/%s/json",
				updateableDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);
	}
	
	@Test
	public void testExecuteJSONUpdateInvalidDatasetID() throws IOException, DataSourceManagementException, DataSourceSecurityManagerException {		
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		String jsonld = FileLoader.loadResource("/json/update/update1.json");

		Entity<String> updateEntity = Entity.entity(jsonld,
				MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/update/%s/json",
			Integer.MAX_VALUE)).request(
			MediaType.APPLICATION_JSON).post(updateEntity,
			Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSQLUpdateJSONDataset() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));
		
		String sqlInsert = "insert into results (key1, key2, key3) values ('A', 'B', 'C');";

		Entity<String> updateEntity = Entity.entity(sqlInsert, MediaType.TEXT_PLAIN);

		UpdateResult insertResult = target(String.format("data/update/%s/sql",
				jsonDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity, UpdateResult.class);
		
		assertEquals(1, insertResult.getRows());
		
		client().register(HttpAuthenticationFeature.basic(INVALID_USER, TEST_PASS));
		
		Entity<String> queryEntity = Entity.entity(
				"select * from results;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", jsonDataset.getDatasetId()))
				.queryParam("origin", "user")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(1, result.getRows().size());
		
		result = target(
				String.format("data/query/%s/sql", jsonDataset.getDatasetId()))
				.queryParam("origin", "original")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(2, result.getRows().size());
		
		result = target(
				String.format("data/query/%s/sql", jsonDataset.getDatasetId()))
				.queryParam("origin", "any")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(3, result.getRows().size());
	}
	
	@Test
	public void testExecuteSQLUpdateJSONDatasetDelete() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		prepareData();
		
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));
		
		String sqlDelete = "DELETE FROM results WHERE key1 = 'A'";

		Entity<String> deleteEntity = Entity.entity(sqlDelete, MediaType.TEXT_PLAIN);

		UpdateResult deleteResult = target(String.format("data/update/%s/sql",
				jsonDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(deleteEntity, UpdateResult.class);
		
		assertEquals(1, deleteResult.getRows());
		
		Entity<String> queryEntity = Entity.entity(
				"select * from results;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", jsonDataset.getDatasetId()))
				.queryParam("origin", "user")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(0, result.getRows().size());
	}
	
	@Test
	public void testExecuteSQLUpdateJSONDatasetDeleteInvalidUser() throws IOException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		prepareData();
		
		client().register(HttpAuthenticationFeature.basic(INVALID_USER, TEST_PASS));
		
		String sqlDelete = "DELETE FROM results WHERE key1 = 'A'";

		Entity<String> deleteEntity = Entity.entity(sqlDelete, MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/update/%s/sql",
				jsonDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(deleteEntity, Response.class);
		
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());
	}

	private void prepareData() throws DatabaseCreationException, DataSourceSecurityManagerException,
			DataSourceManagementException, UpdateableFactoryException, DataSourceConnectorException {
		final DataSourceConnector connector = ConnectorFactory.load(jsonDataset.getJsonMapping());						
		final SQLUpdateable sqlUpdateable = UpdateableFactory.createUpdateable(connector, new Integer(jsonDataset.getDatasetId()).toString(), false);
		
		final String sqlInsert = "insert into results (key1, key2, key3) values ('A', 'B', 'C');";
		sqlUpdateable.executeSQLUpdate(sqlInsert, TEST_USER);
	}
	
	@Test
	public void testExecuteSQLUpdateUserDataset() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into table1 (key1, key2) values ('A', 'B');",
				MediaType.TEXT_PLAIN);

		UpdateResult result = target(
				String.format("data/update/%s/sql",
						userDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(sqlInsert,
				UpdateResult.class);

		assertEquals(1, result.getRows());

		Entity<String> updateEntity = Entity.entity("DELETE FROM table1 where key1 = 'A';",
				MediaType.TEXT_PLAIN);

		result = target(
				String.format("data/update/%s/sql",
						userDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(updateEntity,
				UpdateResult.class);

		assertEquals(1, result.getRows());
	}
	
	@Test
	public void testExecuteSQLUpdateUserInvalidUser() {
		client().register(HttpAuthenticationFeature.basic(INVALID_USER, TEST_PASS));

		Entity<String> sqlInsert = Entity.entity("insert into table1 (key1, key2) values ('A', 'B');",
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/update/%s/sql",
						userDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(sqlInsert,
				Response.class);
		
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}
}