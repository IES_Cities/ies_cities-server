/*  
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.loggingrest.DatasetLog;
import eu.iescities.server.loggingrest.DatasetLogging;
import eu.iescities.server.querymapper.config.Config;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.querymapper.util.scheduler.DataDownloadJob;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;

public class QueryRestInterfaceTest extends JerseyTest {
	
	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();

	private static final String TEST_PASS = "secret";
	private static final String TEST_USER = "bob";

	private static Dataset sparqlDataset, sparqlDataset2;
	private static Dataset sqlDataset, sqlDataset2;
	private static Dataset jsonDataset;
	private static Dataset userListDataset;

	private static Council zgzCouncil;

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();
		
		Config.getInstance().deleteDBDir();
		Config.getInstance().createDataDir();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		final GeoScope zgzGeoScope = GeoScopeManagement.createScope(
				adminSession, "Zaragoza Region", new GeoPoint(41.6894079,
						-0.8427317999999999), new GeoPoint(41.6139746,
						-0.9472301000000001));

		zgzCouncil = new Council();
		zgzCouncil.setName("Zaragoza");
		zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
		zgzCouncil.setGeographicalScope(zgzGeoScope);
		zgzCouncil.setLang(Language.EN);
		zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

		final User testUser = new User();
		testUser.setUsername(TEST_USER);
		testUser.setPassword(TEST_PASS);
		testUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		int defaultUser = UserManagement.registerUser(testUser).getUserId();

		CouncilManagement.addCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), defaultUser);

		UserManagement.logout(adminSession);

		String userSession = UserManagement.login(TEST_USER, TEST_PASS);

		// Register SPARQL endpoint dataset		
		String jsonMapping = FileLoader.loadResource("/sparql/foaf_schema.json");

		Dataset dataset = new Dataset();
		dataset.setName("sparql_dataset");
		dataset.setDescription("This dataset provides an example of an sparql dataset");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		sparqlDataset = DatasetManagement.registerDataset(userSession, dataset);

		dataset = new Dataset();
		dataset.setName("sparql_dataset2");
		dataset.setDescription("This dataset provides an example of an sparql dataset");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		sparqlDataset2 = DatasetManagement
				.registerDataset(userSession, dataset);

		// Register SQL dataset		
		jsonMapping = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		dataset = new Dataset();
		dataset.setName("sql_dataset");
		dataset.setDescription("This dataset provides an example of an sql dataset");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		sqlDataset = DatasetManagement.registerDataset(userSession, dataset);

		dataset = new Dataset();
		dataset.setName("sql_dataset2");
		dataset.setDescription("This dataset provides an example of an sql dataset");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		sqlDataset2 = DatasetManagement.registerDataset(userSession, dataset);
		
		// Register json backed dataset		
		jsonMapping = FileLoader.loadResource("/json/mapping/jsonmapping1.json");

		dataset = new Dataset();
		dataset.setName("json_dataset");
		dataset.setDescription("This dataset provides an example of dataset that is backed by a JSON resource file");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		jsonDataset = DatasetManagement.registerDataset(userSession, dataset);
		final DataSourceConnector connector = ConnectorFactory.load(jsonDataset.getJsonMapping());
		DataDownloadJob downloadJob = new DataDownloadJob();
		downloadJob.execute(connector, new Integer(jsonDataset.getDatasetId()).toString());
		
		// Register user list backed dataset		
		jsonMapping = FileLoader.loadResource("/security/dbconfig/user_list.json");

		dataset = new Dataset();
		dataset.setName("user_list_dataset");
		dataset.setDescription("This dataset provides an example of dataset with a user access list");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);

		userListDataset = DatasetManagement.registerDataset(userSession, dataset);
	}
	
	@Test
	public void testExecuteSQLQuery() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", sparqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(2, result.getRows().size());

		/********* First row *******************/

		assertEquals(
				"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare",
				result.getRows().get(0).getValue("id"));

		assertEquals("http://www.slideshare.net/uaguiler/", result.getRows()
				.get(0).getValue("foaf_page"));

		assertEquals("http://www.slideshare.net/", result.getRows().get(0)
				.getValue("foaf_accountServiceHomepage"));

		assertEquals("uaguiler", result.getRows().get(0).getValue("sioc_name"));

		assertTrue(result.getRows().get(0).getValue("sioc_id").toString().isEmpty());

		/*********** Second row ***********/

		assertEquals(
				"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin",
				result.getRows().get(1).getValue("id"));

		assertEquals("http://www.linkedin.com/pub/unai-aguilera/", result
				.getRows().get(1).getValue("foaf_page"));

		assertEquals("http://www.linkedin.com/", result.getRows().get(1)
				.getValue("foaf_accountServiceHomepage"));

		assertTrue(result.getRows().get(1).getValue("sioc_name").toString().isEmpty());

		assertEquals("26263133", result.getRows().get(1).getValue("sioc_id"));
	}
	
	@Test
	public void testSQLQueryResultFormat() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		String result = target(
				String.format("data/query/%s/sql", sparqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						String.class);

		String expected = "{\"count\":2,"
				+ "\"rows\":[{\"id\":\"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare\","
				+ "\"foaf_accountServiceHomepage\":\"http://www.slideshare.net/\","
				+ "\"foaf_page\":\"http://www.slideshare.net/uaguiler/\","
				+ "\"sioc_id\":\"\","
				+ "\"sioc_name\":\"uaguiler\"},"
				+ "{\"id\":\"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin\","
				+ "\"foaf_accountServiceHomepage\":\"http://www.linkedin.com/\","
				+ "\"foaf_page\":\"http://www.linkedin.com/pub/unai-aguilera/\","
				+ "\"sioc_id\":\"26263133\","
				+ "\"sioc_name\":\"\"}]}";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testExecuteSQLQueryJSONLD() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		String result = target(
				String.format("data/query/%s/sql/jsonld", sparqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						String.class);
		
		String expected = "{\n" + 
				"  \"count\": 2,\n" + 
				"  \"rows\": [\n" + 
				"    {\n" + 
				"      \"@context\": {\n" + 
				"        \"foaf_accountServiceHomepage\": \"http://xmlns.com/foaf/0.1/accountServiceHomepage\",\n" +
				"        \"foaf_page\": \"http://xmlns.com/foaf/0.1/page\",\n" +
				"        \"sioc_id\": \"http://rdfs.org/sioc/ns#id\",\n" +
				"        \"sioc_name\": \"http://rdfs.org/sioc/ns#name\"\n" +  
				"      },\n" + 
				"      \"@id\": \"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare\",\n" + 
				"      \"foaf_accountServiceHomepage\": \"http://www.slideshare.net/\",\n" + 
				"      \"foaf_page\": \"http://www.slideshare.net/uaguiler/\",\n" + 
				"      \"sioc_id\": \"\",\n" + 
				"      \"sioc_name\": \"uaguiler\"\n" + 
				"    },\n" + 
				"    {\n" + 
				"      \"@context\": {\n" + 
				"        \"foaf_accountServiceHomepage\": \"http://xmlns.com/foaf/0.1/accountServiceHomepage\",\n" +
				"        \"foaf_page\": \"http://xmlns.com/foaf/0.1/page\",\n" +  
				"        \"sioc_id\": \"http://rdfs.org/sioc/ns#id\",\n" +
				"        \"sioc_name\": \"http://rdfs.org/sioc/ns#name\"\n" +  
				"      },\n" + 
				"      \"@id\": \"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin\",\n" + 
				"      \"foaf_accountServiceHomepage\": \"http://www.linkedin.com/\",\n" + 
				"      \"foaf_page\": \"http://www.linkedin.com/pub/unai-aguilera/\",\n" + 
				"      \"sioc_id\": \"26263133\",\n" + 
				"      \"sioc_name\": \"\"\n" + 
				"    }\n" + 
				"  ]\n" + 
				"}";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testExecuteSQLQueryJSONLDInvalidQuery() {
		Entity<String> queryEntity = Entity.entity(
				"select * from not_valid_table;", MediaType.TEXT_PLAIN);

		Response response = target(String.format("data/query/%s/sql/jsonld", sparqlDataset.getDatasetId()))
			.request(MediaType.APPLICATION_JSON).post(queryEntity, Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSQLQueryJSONLDNotSPARLDataset() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sql/jsonld", sqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSQLQueryJSONLDInvalidDatasetID() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sql/jsonld", Integer.MAX_VALUE))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
		
	@Test
	public void testLoggingSQLQuery() {	
		DatasetLogging datasetLogging = new DatasetLogging();
		List<DatasetLog> datasetLogs = datasetLogging.getQuery(
				sparqlDataset2.getDatasetId(),
				QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none", "none",
				"none");

		assertEquals(0, datasetLogs.size());

		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql",
						sparqlDataset2.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(queryEntity, ResultSet.class);

		assertEquals(2, result.getRows().size());

		datasetLogging = new DatasetLogging();
		datasetLogs = datasetLogging.getQuery(sparqlDataset2.getDatasetId(),
				QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none", "none",
				"none");

		assertEquals(1, datasetLogs.size());

		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		result = target(
				String.format("data/query/%s/sql",
						sparqlDataset2.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(queryEntity, ResultSet.class);

		datasetLogs = datasetLogging.getQuery(sparqlDataset2.getDatasetId(),
				zgzCouncil.getCouncilId(), "DatasetQuery", "none", "none",
				"none");

		assertEquals(1, datasetLogs.size());

		try {
			queryEntity = Entity.entity("select **** from soc_UserAccount;",
					MediaType.TEXT_PLAIN);

			result = target(
					String.format("data/query/%s/sql",
							sparqlDataset2.getDatasetId())).request(
					MediaType.APPLICATION_JSON).post(queryEntity,
					ResultSet.class);
		} catch (BadRequestException e) {
			datasetLogging = new DatasetLogging();
			datasetLogs = datasetLogging.getQuery(
					sparqlDataset2.getDatasetId(),
					QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none",
					"none", "none");

			assertEquals(1, datasetLogs.size());
		}
	}

	@Test
	public void testTransformSQLQuery() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		String result = target(
				String.format("data/query/%s/transform/sql",
						sparqlDataset.getDatasetId())).request(
				MediaType.TEXT_PLAIN).post(queryEntity, String.class);
		
		String expected = 	"PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>\n" + 
							"select (str(?a) as ?id) (str(?b) as ?foaf_accountServiceHomepage) (str(?c) as ?foaf_page) (str(?d) as ?sioc_id) (str(?e) as ?sioc_name) where {\n" + 
							"\t?a	a	<http://rdfs.org/sioc/ns#UserAccount> .\n" +
							"\toptional {?a	<http://xmlns.com/foaf/0.1/accountServiceHomepage>	?b .}\n" + 
							"\toptional {?a	<http://xmlns.com/foaf/0.1/page>	?c .}\n" + 
							"\toptional {?a	<http://rdfs.org/sioc/ns#id>	?d .}\n" +
							"\toptional {?a	<http://rdfs.org/sioc/ns#name>	?e .}\n" +
							"}";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testTransformSQLQueryInvalidDataset() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/transform/sql",
						sqlDataset.getDatasetId())).request(
				MediaType.TEXT_PLAIN).post(queryEntity, Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testTransformSQLQueryInvalidDatasetID() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount;", MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/transform/sql",
						Integer.MAX_VALUE)).request(
				MediaType.TEXT_PLAIN).post(queryEntity, Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testExecuteSQLQueryEmptyResult() {
		Entity<String> queryEntity = Entity.entity(
				"select * from sioc_UserAccount where id = 'aaaa';",
				MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", sparqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(0, result.getRows().size());
	}

	@Test
	public void testExecuteSQLInvalidDatasetID() {
		Entity<String> queryEntity = Entity.entity("", MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sql",
						QueryRestInterface.DEFAULT_COUNCIL_ID)).request(
				MediaType.APPLICATION_JSON).post(queryEntity, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testExecuteSQLInvalidQuery() {
		Entity<String> queryEntity = Entity.entity("select * from ;",
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sql", sparqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSPARQLQueryInvalid() {
		String sparqlQuery = "INVALID Query";

		Entity<String> queryEntity = Entity.entity(sparqlQuery,
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sparql",
						sparqlDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(queryEntity, Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testExecuteSPARQLQuery() {
		String sparqlQuery = "PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>"
				+ "select (str(?a) as ?id) (str(?b) as ?foaf_accountServiceHomepage) (str(?c) as ?foaf_page) (str(?d) as ?sioc_id) (str(?e) as ?sioc_name) where {"
				+ "?a  a   <http://rdfs.org/sioc/ns#UserAccount> ."
				+ "optional {?a    <http://xmlns.com/foaf/0.1/accountServiceHomepage>  ?b .}"
				+ "optional {?a    <http://xmlns.com/foaf/0.1/page>    ?c .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#id>    ?d .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#name>  ?e .}" + "}";

		Entity<String> queryEntity = Entity.entity(sparqlQuery,
				MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sparql",
						sparqlDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(queryEntity, ResultSet.class);

		assertEquals(2, result.getRows().size());

		/********* First row *******************/

		assertEquals(
				"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare",
				result.getRows().get(0).getValue("id"));

		assertEquals("http://www.slideshare.net/uaguiler/", result.getRows()
				.get(0).getValue("foaf_page"));

		assertEquals("http://www.slideshare.net/", result.getRows().get(0)
				.getValue("foaf_accountServiceHomepage"));

		assertEquals("uaguiler", result.getRows().get(0).getValue("sioc_name"));

		assertTrue(result.getRows().get(0).getValue("sioc_id").toString().isEmpty());

		/*********** Second row ***********/

		assertEquals(
				"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin",
				result.getRows().get(1).getValue("id"));

		assertEquals("http://www.linkedin.com/pub/unai-aguilera/", result
				.getRows().get(1).getValue("foaf_page"));

		assertEquals("http://www.linkedin.com/", result.getRows().get(1)
				.getValue("foaf_accountServiceHomepage"));

		assertTrue(result.getRows().get(1).getValue("sioc_name").toString().isEmpty());

		assertEquals("26263133", result.getRows().get(1).getValue("sioc_id"));
	}
	
	@Test
	public void testExecuteSPARQLQueryRelationalDataset() {
		String sparqlQuery = "PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>"
				+ "select (str(?a) as ?id) (str(?b) as ?foaf_accountServiceHomepage) (str(?c) as ?foaf_page) (str(?d) as ?sioc_id) (str(?e) as ?sioc_name) where {"
				+ "?a  a   <http://rdfs.org/sioc/ns#UserAccount> ."
				+ "optional {?a    <http://xmlns.com/foaf/0.1/accountServiceHomepage>  ?b .}"
				+ "optional {?a    <http://xmlns.com/foaf/0.1/page>    ?c .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#id>    ?d .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#name>  ?e .}" + "}";

		Entity<String> queryEntity = Entity.entity(sparqlQuery,
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sparql",
						sqlDataset.getDatasetId())).request(
				MediaType.APPLICATION_JSON).post(queryEntity, Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSPARQLQueryInvalidDatasetID() {
		String sparqlQuery = "PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>"
				+ "select (str(?a) as ?id) (str(?b) as ?foaf_accountServiceHomepage) (str(?c) as ?foaf_page) (str(?d) as ?sioc_id) (str(?e) as ?sioc_name) where {"
				+ "?a  a   <http://rdfs.org/sioc/ns#UserAccount> ."
				+ "optional {?a    <http://xmlns.com/foaf/0.1/accountServiceHomepage>  ?b .}"
				+ "optional {?a    <http://xmlns.com/foaf/0.1/page>    ?c .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#id>    ?d .}"
				+ "optional {?a    <http://rdfs.org/sioc/ns#name>  ?e .}" + "}";

		Entity<String> queryEntity = Entity.entity(sparqlQuery,
				MediaType.TEXT_PLAIN);

		Response response = target(
				String.format("data/query/%s/sparql",
						Integer.MAX_VALUE)).request(
				MediaType.APPLICATION_JSON).post(queryEntity, Response.class);
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	public void testExecuteSQLQueryRelationalDataset() {
		Entity<String> queryEntity = Entity.entity("select * from people;",
				MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", sqlDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(4, result.getRows().size());
	}

	@Test
	public void testLoggingSQLQueryRelationalDataset() {
		DatasetLogging datasetLogging = new DatasetLogging();
		List<DatasetLog> datasetLogs = datasetLogging.getQuery(
				sqlDataset2.getDatasetId(),
				QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none", "none",
				"none");

		assertEquals(0, datasetLogs.size());

		Entity<String> queryEntity = Entity.entity("select * from people;",
				MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", sqlDataset2.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(4, result.getRows().size());

		datasetLogging = new DatasetLogging();
		datasetLogs = datasetLogging.getQuery(sqlDataset2.getDatasetId(),
				QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none", "none",
				"none");

		assertEquals(1, datasetLogs.size());

		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));

		result = target(
				String.format("data/query/%s/sql", sqlDataset2.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		datasetLogging = new DatasetLogging();
		datasetLogs = datasetLogging.getQuery(sqlDataset2.getDatasetId(),
				zgzCouncil.getCouncilId(), "all", "none", "none", "none");

		assertEquals(1, datasetLogs.size());

		queryEntity = Entity.entity("select * from ppp;", MediaType.TEXT_PLAIN);

		try {
			result = target(
					String.format("data/query/%s/sql",
							sqlDataset2.getDatasetId())).request(
					MediaType.APPLICATION_JSON).post(queryEntity,
					ResultSet.class);
		} catch (BadRequestException e) {
			datasetLogging = new DatasetLogging();
			datasetLogs = datasetLogging.getQuery(sqlDataset2.getDatasetId(),
					QueryRestInterface.DEFAULT_COUNCIL_ID, "all", "none",
					"none", "none");
			assertEquals(1, datasetLogs.size());
		}
	}
	
	@Test
	public void testExecuteSQLQueryUserList() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));
		
		Entity<String> queryEntity = Entity.entity(
				"select * from people;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", userListDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(4, result.getRows().size());
	}
	
	@Test
	public void testExecuteSQLQueryUserListAccesibleTable() {		
		Entity<String> queryEntity = Entity.entity(
				"select * from country;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", userListDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(3, result.getRows().size());
	}
	
	@Test(expected=ForbiddenException.class)
	public void testExecuteSQLQueryUserListInvalidUser() {		
		Entity<String> queryEntity = Entity.entity(
				"select * from people;", MediaType.TEXT_PLAIN);

		target(String.format("data/query/%s/sql", userListDataset.getDatasetId()))
			.request(MediaType.APPLICATION_JSON).post(queryEntity, ResultSet.class);
	}
	
	@Test
	@Ignore
	public void testExecuteSQLQueryJSONDataset() {
		Entity<String> queryEntity = Entity.entity(
				"select * from results;", MediaType.TEXT_PLAIN);

		ResultSet result = target(
				String.format("data/query/%s/sql", jsonDataset.getDatasetId()))
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						ResultSet.class);

		assertEquals(2, result.getRows().size());
		
		assertEquals("value1", result.getRows().get(0).getValue("key1"));
		assertEquals("value2", result.getRows().get(0).getValue("key2"));
		assertEquals("value3", result.getRows().get(0).getValue("key3"));
		
		assertEquals("value4", result.getRows().get(1).getValue("key1"));
		assertEquals("value5", result.getRows().get(1).getValue("key2"));
		assertEquals("value6", result.getRows().get(1).getValue("key3"));
	}
	
	@Test
	public void testExecuteKPIQuery() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));
		
		Entity<String> queryEntity = Entity.entity(
				"SELECT COUNT(table_name) FROM information_schema.tables WHERE table_schema='public'", MediaType.TEXT_PLAIN);
		
		String result = target("data/query/kpi")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						String.class);
		
		assertTrue(!result.isEmpty());
	}
	
	@Test
	public void testExecuteKPIQueryInvalidSQL() {
		client().register(HttpAuthenticationFeature.basic(TEST_USER, TEST_PASS));
		
		Entity<String> queryEntity = Entity.entity(
				"SELECT * from invalid_table", MediaType.TEXT_PLAIN);
		
		Response response = target("data/query/kpi")
				.request(MediaType.APPLICATION_JSON).post(queryEntity,
						Response.class);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
}
