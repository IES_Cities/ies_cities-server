package eu.iescities.server.restinterface.sample;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CommentExtractor;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.restinterface.util.config.Config;

public class SampleData {
	
	/// Council admin users
	
	private static final String BRISTOL_ADMIN = "bristol";
	private static final String BRISTOL_ADMIN_PASS = "secret"; 
	
	private static final String ZGZ_ADMIN = "zaragoza";
	private static final String ZGZ_ADMIN_PASS = "secret";
	
	private static final String ROVERETO_ADMIN = "rovereto";
	private static final String ROVERETO_ADMIN_PASS = "secret";
	
	private static final String MAJADAHONDA_ADMIN = "majadahonda";
	private static final String MAJADAHONDA_ADMIN_PASS = "secret";
	
	/// Registered users
	
	private static final String MYBRISTOL_USER = "bris_mybristol";
	private static final String MYBRISTOL_USER_PASS = "secret";
	
	private static final String DEMOCRATREE_USER = "bris_democratree";
	private static final String DEMOCRATREE_USER_PASS = "secret";
	
	private static final String MAJADAHONDA_LE_USER = "maj_leisure";
	private static final String MAJADAHONDA_LE_USER_PASS = "secret";
	
	private static final String MAJADAHONDA_HEALTHY_USER = "maj_healthy";
	private static final String MAJADAHONDA_HEALTHY_USER_PASS = "secret";
	
	private static final String ROVERETO_EXPLORER_USER = "rov_explorer";
	private static final String ROVERETO_EXPLORER_USER_PASS = "secret";
	
	private static final String ROVERETO_VIAGGIA_USER = "rov_viaggia";
	private static final String ROVERETO_VIAGGIA_USER_PASS = "secret";
	
	private static final String ZGZ_COMPLAINTS_USER = "zgz_complaints";
	private static final String ZGZ_COMPLAINTS_USER_PASS = "secret";
	
	private static final String ZGZ_MAPS_USER = "zgz_maps";
	private static final String ZGZ_MAPS_USER_PASS = "secret";
	
	private static boolean update = true;
	
	private static void registerUsers() {
    	TestUtils.addAdminUser().getUserId();
		
		User user1 = new User();
    	user1.setUsername(MYBRISTOL_USER);
    	user1.setPassword(MYBRISTOL_USER_PASS);
    	UserManagement.registerUser(user1);
    	
    	User user2 = new User();
    	user2.setUsername(DEMOCRATREE_USER);
    	user2.setPassword(DEMOCRATREE_USER_PASS);
    	UserManagement.registerUser(user2);
    	
    	User user3 = new User();
    	user3.setUsername(MAJADAHONDA_LE_USER);
    	user3.setPassword(MAJADAHONDA_LE_USER_PASS);
    	UserManagement.registerUser(user3);
    	
    	User user4 = new User();
    	user4.setUsername(MAJADAHONDA_HEALTHY_USER);
    	user4.setPassword(MAJADAHONDA_HEALTHY_USER_PASS);
    	UserManagement.registerUser(user4);
    	
    	User user5 = new User();
    	user5.setUsername(ROVERETO_EXPLORER_USER);
    	user5.setPassword(ROVERETO_EXPLORER_USER_PASS);
    	UserManagement.registerUser(user5);
    	
    	User user6 = new User();
    	user6.setUsername(ROVERETO_VIAGGIA_USER);
    	user6.setPassword(ROVERETO_VIAGGIA_USER_PASS);
    	UserManagement.registerUser(user6);
    	
    	User user7 = new User();
    	user7.setUsername(ZGZ_COMPLAINTS_USER);
    	user7.setPassword(ZGZ_COMPLAINTS_USER_PASS);
    	UserManagement.registerUser(user7);
    	
    	User user8 = new User();
    	user8.setUsername(ZGZ_MAPS_USER);
    	user8.setPassword(ZGZ_MAPS_USER_PASS);
    	UserManagement.registerUser(user8);
	}
	
	public static Council getCouncil(String councilName) {
		for (Council c : CouncilManagement.getAllCouncils(0, 1000, Language.EN, null)) {
			if (c.getName().equals(councilName))
				return c;
		}
		return null;
	}

	public static void main(String[] args) throws Exception {
		final String file = "iescities.properties";
        try {
        	final Config config = Config.getInstance();
        	config.load(file);
        	eu.iescities.server.querymapper.config.Config.getInstance().setDataDir(config.getDataDir());
        } catch (IOException e) {
        	System.out.println("Could not load " + file + ". Using default values");
        }
		
		System.out.print("RE-create database. CURRENT DATA WILL BE DELETED. THIS ACTION CANNOT BE UNDONE. (N=UPDATE ONLY) [y/N]? ");
		char input = (char) System.in.read();
		
		if (input == 'y' || input == 'Y')
			update = false;
		
		Council zgzCouncil;
		Council bristolCouncil;
		Council roveretoCouncil;
		Council majadahondaCouncil;
		
		if (!update) {
	    	TestUtils.cleanUpDatastore();
	  
	    	registerUsers();
        
	        String adminSession = TestUtils.loginAdminUser();    	
	        
	        zgzCouncil = createZgzCouncil(adminSession);
	        bristolCouncil = createBristolCouncil(adminSession);
	        roveretoCouncil = createRoveretoCouncil(adminSession);
	        majadahondaCouncil = createMajadahondaCouncil(adminSession);
        
	        UserManagement.logout(adminSession);
		} else {			
			zgzCouncil = getCouncil("Zaragoza");
	        bristolCouncil = getCouncil("Bristol");
			roveretoCouncil = getCouncil("Rovereto");
	        majadahondaCouncil = getCouncil("Majadahonda");
		}
		
        createZaragozaData(zgzCouncil);
		createBristolData(bristolCouncil);        
        createRoveretoData(roveretoCouncil);
        createMajadahondaData(majadahondaCouncil);
        
        createPlayerData();
	}

	private static void createMajadahondaData(Council majadahondaCouncil) throws IOException {
		String userSession;
		String jsonSchema;
		userSession = UserManagement.login(MAJADAHONDA_ADMIN, MAJADAHONDA_ADMIN_PASS);

        Application majadahondaLEApp = new Application();
        majadahondaLEApp.setLang(Language.EN);
        majadahondaLEApp.setName("Majadahonda Leisure & Events");
        majadahondaLEApp.setDescription("This app lets you know about all the important and fun events created by the local " +
        		"commerces that are happening or will take place in Majadahonda. You can search for the events ordered by your " +
        		"preferences. You can get information about the event, the dates, prices, exact place, etc. " +
        		"You are even able to check it in a map. You can share this information through the social networks like Facebook, " +
        		"Twitter, Google Plus, WhatsApp, Hangouts, etc.. If you have any doubt about a special event or want to say something " +
        		"about it, you can contribute to improve the system through comments. ");
        majadahondaLEApp.setUrl("http://iescities.eu");
        majadahondaLEApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-majadahonda-leisure-logo-3357157233-1_avatar.png");
        majadahondaLEApp.setRelatedCouncilId(majadahondaCouncil.getCouncilId());
        majadahondaLEApp.setVersion("1.0");
        majadahondaLEApp.setTermsOfService("Tecnalia shall be responsible for treatment of the personal data it has access to through " +
        		"the registration process (if it exists), and shall comply with that laid down in the Directive 95/46EC of the European " +
        		"Parliament and of the Council of 24 October 1995 on the protection of individuals with regard to the processing of " +
        		"personal data and on the free movement of such data, as well as any other applicable national regulations currently in " +
        		"force or introduced in the future to modify and/or replace it." +
        		"The gathering and processing of that personal data is designed to manage THE REGISTRATION FOR USE THE APPLICATION, " +
        		"THE ELABORATION OF LIMITED USE STATISTICS OF IT, IN ADDITION WE CAN USE THAT DATA TO KEEP YOU INFORMED ABOUT NEW VERSIONS, " +
        		"NEW Features, ETC. OF THE APPLICATION. In no case, TECNALIA will use your data for other purposes than specified, without " +
        		"asking for your authorization. When using social networks credentials for the registration process, TECNALIA won’t read or " +
        		"gather any information of your profile, except that you provided expressly." +
        		"You’re entitled to exercise your right to access, rectify, erasure and object the treatment of your data, as provided by law, " +
        		"by sending an e-mail to the address jorge.perez@tecnalia.com");
        majadahondaLEApp.setPermissions("INTERNET, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION, READ_EXTERNAL_STORAGE");
        majadahondaLEApp.setGooglePlayRating(0.0);
        majadahondaLEApp.setPackageName("com.Majadahonda.LeisureAndEvents");
        majadahondaLEApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
            "events", "fun", "leisure", "activities", "spare time", "majadahonda", "social"
        })));
        majadahondaLEApp.setGeographicalScope(majadahondaCouncil.getGeographicalScope());
        
        majadahondaLEApp = modifyApp(userSession, majadahondaLEApp);
        
        jsonSchema = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
        
        Dataset majadahondaLEDataset = new Dataset();
        majadahondaLEDataset.setLang(Language.EN);
        majadahondaLEDataset.setName("ComercialEvents");
        majadahondaLEDataset.setDescription("This dataset will store information related to different leisure events created " +
        		"and managed by Majadahonda’s retailers.");
        majadahondaLEDataset.setJsonMapping(jsonSchema);
		
        majadahondaLEDataset = modifyDataset(userSession, majadahondaLEDataset);
        
        ApplicationManagement.addDataset(userSession, majadahondaLEApp.getAppId(), majadahondaLEDataset.getDatasetId());
        
        Application majadahondaHCApp = new Application();
        majadahondaHCApp.setLang(Language.EN);
        majadahondaHCApp.setName("Majadahonda Healthy City");
        majadahondaHCApp.setDescription("This app lets you know everything about the weather and how to get a healthy life in " +
        		"Majadahonda through different sensors that measure different parameters of the environment. You are able to " +
        		"know the weather forecast for Majadahonda. You can also share your feelings about the weather to improve the " +
        		"whole system and help other people.");
        majadahondaHCApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-majadahonda-healthy-logo-3100838811-2_avatar.png");
        majadahondaHCApp.setRelatedCouncilId(majadahondaCouncil.getCouncilId());
        majadahondaHCApp.setVersion("1.0");
        majadahondaHCApp.setTermsOfService("Tecnalia shall be responsible for treatment of the personal data it has access to through " +
        		"the registration process (if it exists), and shall comply with that laid down in the Directive 95/46EC of the European " +
        		"Parliament and of the Council of 24 October 1995 on the protection of individuals with regard to the processing of " +
        		"personal data and on the free movement of such data, as well as any other applicable national regulations currently in " +
        		"force or introduced in the future to modify and/or replace it." +
        		"The gathering and processing of that personal data is designed to manage THE REGISTRATION FOR USE THE APPLICATION, " +
        		"THE ELABORATION OF LIMITED USE STATISTICS OF IT, IN ADDITION WE CAN USE THAT DATA TO KEEP YOU INFORMED ABOUT NEW VERSIONS, " +
        		"NEW Features, ETC. OF THE APPLICATION. In no case, TECNALIA will use your data for other purposes than specified, without " +
        		"asking for your authorization. When using social networks credentials for the registration process, TECNALIA won’t read or " +
        		"gather any information of your profile, except that you provided expressly." +
        		"You’re entitled to exercise your right to access, rectify, erasure and object the treatment of your data, as provided by law, " +
        		"by sending an e-mail to the address jorge.perez@tecnalia.com");
        majadahondaHCApp.setPermissions("INTERNET, READ_CALENDAR, WRITE_CALENDAR");
        majadahondaHCApp.setGooglePlayRating(0.0);
        majadahondaHCApp.setPackageName("com.Majadahonda.HealthyCity");
        majadahondaHCApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
            "weather", "social", "forecast", "majadahonda", "health"
        })));
        majadahondaHCApp.setGeographicalScope(majadahondaCouncil.getGeographicalScope());
        
        majadahondaHCApp = modifyApp(userSession, majadahondaHCApp);
        
        Dataset majadahondaLEDataset2 = new Dataset();
        majadahondaLEDataset2.setLang(Language.EN);
        majadahondaLEDataset2.setName("SensorsData");
        majadahondaLEDataset2.setDescription("This dataset will store information related to different Sensors deployed in Majadahonda city.");
        majadahondaLEDataset2.setJsonMapping("");
		
        majadahondaLEDataset2 = modifyDataset(userSession, majadahondaLEDataset2);
        ApplicationManagement.addDataset(userSession, majadahondaHCApp.getAppId(), majadahondaLEDataset2.getDatasetId());
        
        jsonSchema = FileLoader.loadResource("/security/dbconfig/user_list3.json");
        
        Dataset example_dataset = new Dataset();
        example_dataset.setLang(Language.EN);
        example_dataset.setName("user_list_example");
        example_dataset.setDescription("This dataset shows an example of ");
        example_dataset.setJsonMapping(jsonSchema);
		
        example_dataset = modifyDataset(userSession, example_dataset);
        ApplicationManagement.addDataset(userSession, majadahondaHCApp.getAppId(), example_dataset.getDatasetId());
        
        UserManagement.logout(userSession);
	}
	
	private static Dataset getDataset(String datasetName) {
		for (Dataset d : DatasetManagement.getAllDataSets(0, 1000, Language.EN, null)) {
			if (d.getName().equals(datasetName))
				return d;
		}
		return null; 
	}

	private static Dataset modifyDataset(String session, Dataset dataset) {
		if (update) {
			Dataset oldDataset = getDataset(dataset.getName());
			if (oldDataset != null) {
				dataset.setDatasetId(oldDataset.getDatasetId());
				dataset = DatasetManagement.updateDataset(session, dataset);
				return dataset;
			}
		}
        dataset = DatasetManagement.registerDataset(session, dataset);
		return dataset;
	}
	
	private static Application getApplication(String appName) {
		for (Application a : ApplicationManagement.getAllApps(0, 100, Language.EN, null)) {
			if (a.getName().equals(appName))
				return a;
		}
		return null;
	}

	private static Application modifyApp(String session, Application app) {
		if (update) {
    		Application oldApp = getApplication(app.getName());
    		if (oldApp != null) {
    			app.setAppId(oldApp.getAppId());
    			app = ApplicationManagement.updateApp(session, app);
    			return app;
    		}	
        }
		
        app = ApplicationManagement.registerApp(session, app);
		return app;
	}

	private static void createRoveretoData(Council roveretoCouncil) {
		String userSession;
		userSession = UserManagement.login(ROVERETO_ADMIN, ROVERETO_ADMIN_PASS);

        Application roveretoVRApp = new Application();
        roveretoVRApp.setLang(Language.EN);
        roveretoVRApp.setName("ViaggiaRovereto");
        roveretoVRApp.setDescription("Would you like to use more public transport but you never know their timetables?" +
        		" Would you like to plan your trips by foot, by public transport, by car or bike sharing and choose the fastest way?" +
        		" Would you like to be informed in real time about delays of train and bus lines that you regularly use, and about the urban viability of Rovereto?" +
        		" Parking places at your favourite parking lot are full and you don't know where the closest parking lot is?" +
        		" Would you like to report inconveniences and problems related to your urban mobility experience?" +
        		" ViaggiaRovereto is the application that satisfies your needs of sustainable mobility in Rovereto and surroundings!\n"
        		+ "With VaggiaRovereto you can:\n\n- plan multimodal trips (by foot, car, public transport or with shared mobility services"
        		+ " such as car or bike sharing)\n- monitor your usual routes and receive notifications in real time of any delays or problems;"
        		+ "\n- consult up-to-date information on public transport timetables, urban viability and parking availability  \n"
        		+ "- report inconveniences and problems encountered during your trips by foot, car, public transport or with shared mobility services.");
        
        roveretoVRApp.setUrl("https://play.google.com/store/apps/details?id=eu.trentorise.smartcampus.viaggiarovereto");
        roveretoVRApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-rovereto-viaggia-logo-42432892-0_avatar.png");
        roveretoVRApp.setRelatedCouncilId(roveretoCouncil.getCouncilId());
        roveretoVRApp.setVersion("0.9.12");
        roveretoVRApp.setTermsOfService("Terms of Service for ViaggiaRovereto");
        roveretoVRApp.setPermissions("android.permission.ACCESS_COARSE_LOCATION, " +
        		"android.permission.ACCESS_FINE_LOCATION, " +
        		"android.permission.ACCESS_NETWORK_STATE, " +
        		"android.permission.AUTHENTICATE_ACCOUNTS, " +
        		"android.permission.GET_ACCOUNTS, " +
        		"android.permission.INTERNET, " +
        		"android.permission.MANAGE_ACCOUNTS, " +
        		"android.permission.READ_EXTERNAL_STORAGE, " +
        		"android.permission.READ_PHONE_STATE, " +
        		"android.permission.READ_SYNC_SETTINGS, " +
        		"android.permission.READ_SYNC_STATS, " +
        		"android.permission.USE_CREDENTIALS, " +
        		"android.permission.VIBRATE, " +
        		"android.permission.WRITE_EXTERNAL_STORAGE, " +
        		"android.permission.WRITE_SETTINGS, " +
        		"android.permission.WRITE_SYNC_SETTINGS, " +
        		"com.google.android.providers.gsf.permission.READ_GSERVICES, " +
        		"eu.trentorise.smartcampus.permission.MAPS_RECEIVE");
        roveretoVRApp.setPackageName("eu.trentorise.smartcampus.viaggiarovereto");
        roveretoVRApp.setGooglePlayRating(0.0);
        roveretoVRApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
        	"mobility", "transportation", "journey planning", "buses", "trains", "parking"
        })));
        roveretoVRApp.setGeographicalScope(roveretoCouncil.getGeographicalScope());

        roveretoVRApp = modifyApp(userSession, roveretoVRApp);
        
        Dataset roveretoVRxplorerDataset = new Dataset();
        roveretoVRxplorerDataset.setLang(Language.EN);
        roveretoVRxplorerDataset.setName("Trentino’s Public Transportation");
        roveretoVRxplorerDataset.setDescription("Open dataset stores all the information about public transportation in Trentino’s region in GTFS format (http://www2.comune.rovereto.tn.it/iride/extra/ordinanze_gps/json/).");
        
        roveretoVRxplorerDataset = modifyDataset(userSession, roveretoVRxplorerDataset);
		ApplicationManagement.addDataset(userSession, roveretoVRApp.getAppId(), roveretoVRxplorerDataset.getDatasetId());
        
        CommentExtractor.extractCommentsFromGooglePlay(roveretoVRApp.getPackageName());
        
        Application roveretoREApp = new Application();
        roveretoREApp.setLang(Language.EN);
        roveretoREApp.setName("Rovereto Explorer");
        roveretoREApp.setDescription("Looking to a something interesting to do this afternoon or during the week-end? " +
        		"Looking to an excuse to come back to Rovereto? " +
        		"With Rovereto Explorer you will be able to discover all about the events, shows and festivals in Rovereto! " +
        		"And if you want, you can help improving the descriptions of these events with helpful information and suggestions " +
        		"for the other participants.");
        roveretoREApp.setUrl("http://www.smartcampuslab.it/download/VAS/apk/iescities/RoveretoExplorer.apk");
        roveretoREApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-rovereto-esplora-logo-1180765805-1_avatar.png");
        roveretoREApp.setRelatedCouncilId(roveretoCouncil.getCouncilId());
        roveretoREApp.setVersion("1.2.4");
        roveretoREApp.setTermsOfService("Terms of Service for ViaggiaRovereto");
        roveretoREApp.setPermissions("android.permission.ACCESS_COARSE_LOCATION, " +
        		"android.permission.ACCESS_FINE_LOCATION, " +
        		"android.permission.ACCESS_NETWORK_STATE, " +
        		"android.permission.AUTHENTICATE_ACCOUNTS, " +
        		"android.permission.GET_ACCOUNTS, " +
        		"android.permission.INTERNET, " +
        		"android.permission.MANAGE_ACCOUNTS, " +
        		"android.permission.READ_EXTERNAL_STORAGE, " +
        		"android.permission.READ_PHONE_STATE, " +
        		"android.permission.READ_SYNC_SETTINGS, " +
        		"android.permission.READ_SYNC_STATS, " +
        		"android.permission.USE_CREDENTIALS, " +
        		"android.permission.VIBRATE, " +
        		"android.permission.WRITE_EXTERNAL_STORAGE, " +
        		"android.permission.WRITE_SETTINGS, " +
        		"android.permission.WRITE_SYNC_SETTINGS, " +
        		"com.google.android.providers.gsf.permission.READ_GSERVICES");
        roveretoREApp.setPackageName("eu.iescities.pilot.rovereto.roveretoexplorer");
        roveretoREApp.setGooglePlayRating(0.0);
        roveretoREApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
        	"events", "participate", "culture", "sport", "today"
        })));
        roveretoREApp.setGeographicalScope(roveretoCouncil.getGeographicalScope());

        roveretoREApp = modifyApp(userSession, roveretoREApp);
        
        Dataset roveretoRExplorerDataset = new Dataset();
        roveretoRExplorerDataset.setLang(Language.EN);
        roveretoRExplorerDataset.setName("Rovereto Events");
        roveretoRExplorerDataset.setDescription("This dataset contains all the events showed to the users and created by the Rovereto’s council (http://www2.comune.rovereto.tn.it/servizionline/extra/json_sito/event/).");
        
        roveretoRExplorerDataset = modifyDataset(userSession, roveretoRExplorerDataset);
		ApplicationManagement.addDataset(userSession, roveretoREApp.getAppId(), roveretoRExplorerDataset.getDatasetId());
        
        UserManagement.logout(userSession);
	}

	private static void createBristolData(Council bristolCouncil) throws IOException {
		String userSession;
		userSession = UserManagement.login(BRISTOL_ADMIN, BRISTOL_ADMIN_PASS);

        Application bristolMBApp = new Application();
        bristolMBApp.setLang(Language.EN);
        bristolMBApp.setName("myBristol");
        bristolMBApp.setDescription("A place where you can easily share what you're up to with an online network of local people. " +
        		"Whatever your job, activity or hobby, you can share tips, photos and inspiration with others. You can also join " +
        		"groups and keep up to date with what like-minded people are creating too. However you use myBristol, we hope it'll " +
        		"help you get the most out of where you live and the things you do.");
        bristolMBApp.setUrl("https://play.google.com/store/apps/details?id=eu.iescities.MyBristol");
        bristolMBApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-bristol-mybristol-logo-1644611729-3_avatar.png");
        bristolMBApp.setRelatedCouncilId(bristolCouncil.getCouncilId());
        bristolMBApp.setVersion("0.9.2");
        bristolMBApp.setTermsOfService("This Privacy Policy governs the manner in which MyBristol collects, uses, maintains and discloses information "
        		+ "collected from users (each, a \"User\") of the mybristol.org.uk website (\"Site\"). This privacy policy applies to the Site and all "
        		+ "products and services offered by MyBristol.\n\nPersonal identification information\n===================================\n\nWe may "
        		+ "collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, "
        		+ "register on the site, and in connection with other activities, services, features or resources we make available on our Site. Users may "
        		+ "be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will "
        		+ "collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse "
        		+ "to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.\n\n"
        		+ "Non-personal identification information\n=======================================\n\nWe may collect non-personal identification information"
        		+ "about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer"
        		+ "and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers"
        		+ "utilised and other similar information.\n\nWeb browser cookies\n===================\nOur Site may use \"cookies\" to enhance User experience."
        		+ "User\'s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose"
        		+ "to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not"
        		+ "function properly.\n\nHow we use collected information\n================================\nMyBristol may collect and use Users personal information"
        		+ "for the following purposes:\n\n* To improve customer service\n* Information you provide helps us respond to your customer service requests and support"
        		+ "needs more efficiently.\n* To personalize user experience\n* We may use information in the aggregate to understand how our Users as a group use"
        		+ "the services and resources provided on our Site.\n* To improve our Site\n* We may use feedback you provide to improve our products and services.\n"
        		+ "* To send periodic emails\n\nIf User decides to opt-in to our mailing list, they will receive emails that may include company news, updates,"
        		+ "related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed"
        		+ "unsubscribe instructions at the bottom of each email.\n\nHow we protect your information\n===============================\n\n"
        		+ "We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration,"
        		+ "disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.\n\n"
        		+ "Sharing your personal information\n=================================\n\nWe do not sell, trade, or rent Users personal identification information"
        		+ "to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and "
        		+ "users with our business partners, trusted affiliates and advertisers for the purposes outlined above.\n\nThird party websites\n====================\n\n"
        		+ "Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors"
        		+ " and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites"
        		+ " linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services"
        		+ " may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link"
        		+ " to our Site, is subject to that website\'s own terms and policies.\n\nCompliance with children\'s online privacy protection act"
        		+ "\n========================================================\n\nProtecting the privacy of the very young is especially important. "
        		+ "For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website"
        		+ " is structured to attract anyone under 13.\n\nChanges to this privacy policy\n==============================\n\nMyBristol has the discretion"
        		+ " to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to"
        		+ " frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect."
        		+ " You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications."
        		+ "\n\nYour acceptance of these terms\n==============================\n\nBy using this Site, you signify your acceptance of this policy."
        		+ " If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this"
        		+ " policy will be deemed your acceptance of those changes.");
        
        bristolMBApp.setPermissions("camera, photo library, INTERNET");
        bristolMBApp.setGooglePlayRating(0.0);
        bristolMBApp.setPackageName("eu.iescities.MyBristol");
        bristolMBApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
        	"social media", "photo", "blog", "local", "group", "share"
        })));
        bristolMBApp.setGeographicalScope(bristolCouncil.getGeographicalScope());

        bristolMBApp = modifyApp(userSession, bristolMBApp);   
        
        Dataset bristolMBDataset = new Dataset();
        bristolMBDataset.setLang(Language.EN);
        bristolMBDataset.setName("MyKW Dataset");
        bristolMBDataset.setDescription("A dataset containing the publications and related data of the MyKW application");
        
        bristolMBDataset = modifyDataset(userSession, bristolMBDataset);
        ApplicationManagement.addDataset(userSession, bristolMBApp.getAppId(), bristolMBDataset.getDatasetId());
        
        Application bristolDTApp = new Application();
        bristolDTApp.setLang(Language.EN);
        bristolDTApp.setName("Democratree");
        bristolDTApp.setDescription("The Bristol Tree app is designed to let individuals and communities propose new locations " +
        		"for tree planting across the City. The app uses a Local Authority data set to display the current position of " +
        		"City Council managed trees, and allows registered users to propose a place to plant a new tree, and a purpose, " +
        		"person or particular story that it might represent. Registered users can 'vote up' or support these suggestions, " +
        		"with the most popular proposals rising to the top of the leaderboard.");
        bristolDTApp.setUrl("https://play.google.com/store/apps/details?id=uk.co.democratree");
        bristolDTApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-bristol-democratree-logo-2851630200-2_avatar.png");
        bristolDTApp.setRelatedCouncilId(bristolCouncil.getCouncilId());
        bristolDTApp.setVersion("0.8");
        
        bristolDTApp.setTermsOfService("Personal identification information\n===================================\n\n"
        		+ "We may collect personal identification information from Users in a variety of ways, including, but not limited to, "
        		+ "when Users visit our site, register on the site, and in connection with other activities, services, features or resources "
        		+ "we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. "
        		+ "Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they "
        		+ "voluntarily submit such information to us. Users can always refuse to supply personally identification information, except "
        		+ "that it may prevent them from engaging in certain Site related activities.\n\nNon-personal identification information"
        		+ "\n=======================================\n\nWe may collect non-personal identification information about Users whenever "
        		+ "they interact with our Site. Non-personal identification information may include the browser name, the type of computer and "
        		+ "technical information about Users means of connection to our Site, such as the operating system and the Internet service "
        		+ "providers utilised and other similar information.\n\nWeb browser cookies\n===================\n\nOur Site may use \"cookies\" "
        		+ "to enhance User experience. User\'s web browser places cookies on their hard drive for record-keeping purposes and sometimes to"
        		+ " track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are"
        		+ " being sent. If they do so, note that some parts of the Site may not function properly.\n\nHow we use collected information"
        		+ "\n================================\n\nDemocratree may collect and use Users personal information for the following purposes:\n*"
        		+ " To improve customer service\n* Information you provide helps us respond to your customer service requests and support needs more"
        		+ " efficiently.\n* To personalize user experience\n* We may use information in the aggregate to understand how our Users as a group"
        		+ " use the services and resources provided on our Site.\n* To improve our Site\n* We may use feedback you provide to improve our"
        		+ " products and services.\n* To send periodic emails\n\nIf User decides to opt-in to our mailing list, they will receive emails"
        		+ " that may include company news, updates, related product or service information, etc. If at any time the User would like to"
        		+ " unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.\n\n"
        		+ "How we protect your information\n===============================\n\nWe adopt appropriate data collection, storage and"
        		+ " processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction"
        		+ " of your personal information, username, password, transaction information and data stored on our Site.\n\nSharing your"
        		+ " personal information\n=================================\n\nWe do not sell, trade, or rent Users personal identification"
        		+ " information to others. We may share generic aggregated demographic information not linked to any personal identification"
        		+ " information regarding \nvisitors and users with our business partners, trusted affiliates and advertisers for the purposes"
        		+ " outlined above.\n\nThird party websites\n====================\n\nUsers may find advertising or other content on our Site"
        		+ " that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties."
        		+ " We do not control the content or links that appear on these sites and are not responsible for the practices employed by"
        		+ " websites linked to or from our Site. In addition, these sites or services, including their content and links, may be"
        		+ " constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing"
        		+ " and interaction on any other website, including websites which have a link to our Site, is subject to that website\'s own"
        		+ " terms and policies.\n\nCompliance with children\'s online privacy protection act"
        		+ "\n========================================================\n\nProtecting the privacy of the very young is especially"
        		+ " important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13,"
        		+ " and no part of our website is structured to attract anyone under 13.\n\nChanges to this privacy policy"
        		+ "\n==============================\n\nDemocratree has the discretion to update this privacy policy at any time. When we do,"
        		+ " we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any"
        		+ " changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and"
        		+ " agree that it is your responsibility to review this privacy policy periodically and become aware of modifications."
        		+ "\n\nYour acceptance of these terms\n==============================\n\nBy using this Site, you signify your acceptance"
        		+ " of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following"
        		+ " the posting of changes to this policy will be deemed your acceptance of those changes.\n");
        
        bristolDTApp.setPermissions("INTERNET");
        bristolDTApp.setGooglePlayRating(0.0);
        bristolDTApp.setPackageName("uk.co.democratree");
        bristolDTApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
        	"trees", "maps", "bristol", "crowdsourcing"
        })));
        bristolDTApp.setGeographicalScope(bristolCouncil.getGeographicalScope());

        bristolDTApp = modifyApp(userSession, bristolDTApp);
        
        //Register Bristol dataset
        final String jsonSchema = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
        
        Dataset bristolDTDataset = new Dataset();
        bristolDTDataset.setLang(Language.EN);
        bristolDTDataset.setName("bristol_dataset");
        bristolDTDataset.setDescription("A dataset that contains the votes, tree locations and relevant information that users of the app have contributed.");
        bristolDTDataset.setJsonMapping(jsonSchema);
		
        bristolDTDataset = modifyDataset(userSession, bristolDTDataset);
        ApplicationManagement.addDataset(userSession, bristolDTApp.getAppId(), bristolDTDataset.getDatasetId());
        
        Dataset bristolDT2Dataset = new Dataset();
        bristolDT2Dataset.setLang(Language.EN);
        bristolDT2Dataset.setName("Bristol Council Trees Dataset");
        bristolDT2Dataset.setDescription("An open dataset that contains locations of planted trees around the city.");
        
        bristolDT2Dataset = modifyDataset(userSession, bristolDT2Dataset);
        ApplicationManagement.addDataset(userSession, bristolDTApp.getAppId(), bristolDT2Dataset.getDatasetId());
        
        Dataset bristolDT3Dataset = new Dataset();
        bristolDT3Dataset.setLang(Language.EN);
        bristolDT3Dataset.setName("Bristol Tree Pips Dataset");
        bristolDT3Dataset.setDescription("A dataset that contains priority zones for tree planting around the city.");
        
        bristolDT3Dataset = modifyDataset(userSession, bristolDT3Dataset);
        ApplicationManagement.addDataset(userSession, bristolDTApp.getAppId(), bristolDT3Dataset.getDatasetId());
                
        UserManagement.logout(userSession);
	}

	private static void createZaragozaData(Council zgzCouncil) {
		String userSession = UserManagement.login(ZGZ_ADMIN, ZGZ_ADMIN_PASS);
        
        Application zaragozaCSApp = new Application();
        zaragozaCSApp.setLang(Language.EN);
        zaragozaCSApp.setName("Your opinion matters (Complaints & Suggestions)");
        zaragozaCSApp.setDescription("The application will allow citizens to send complaints and suggestions about public " +
        		"services, as well as report problems detected in the public space. This will be done via web, or mobile, " +
        		"and it will be possible to include the geographical coordinates of the incident and a photograph. " +
        		"The city council, in return, will inform the citizens about the status of the incident (fixed, on going or pending).");
        zaragozaCSApp.setUrl("https://play.google.com/store/apps/details?id=es.zgz.apps.complaints");
        zaragozaCSApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-zaragoza-complaints-logo-1172407584-4_avatar.png");
        zaragozaCSApp.setRelatedCouncilId(zgzCouncil.getCouncilId());
        zaragozaCSApp.setVersion("1.0.6");
        zaragozaCSApp.setTermsOfService("Terms of Service for Complaints & Suggestions");
        zaragozaCSApp.setPermissions("INTERNET, WRITE_EXTERNAL_STORAGE, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, CAMERA, VIBRATE");
        zaragozaCSApp.setGooglePlayRating(0.0);
        zaragozaCSApp.setPackageName("es.zgz.apps.complaints");
        zaragozaCSApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
            "zaragoza", "complaints", "suggestions", "photo", "smart city", "opinion", "311"
        })));
        zaragozaCSApp.setGeographicalScope(zgzCouncil.getGeographicalScope());
        
        zaragozaCSApp = modifyApp(userSession, zaragozaCSApp);
        
        Dataset zaragozaCSDataset = new Dataset();
        zaragozaCSDataset.setLang(Language.EN);
        zaragozaCSDataset.setName("Complaints and suggestions dataset");
        zaragozaCSDataset.setDescription("Dataset containing complaints and suggestions, which according to different administrative categories,"
        		+ " citizens send to the Zaragoza City Council authorizing their publication. "
        		+ "http://www.zaragoza.es/ciudad/risp/open311.htm "
        		+ "(http://wiki.open311.org/GeoReport_v2)");
        
        zaragozaCSDataset = modifyDataset(userSession, zaragozaCSDataset);
		ApplicationManagement.addDataset(userSession, zaragozaCSApp.getAppId(), zaragozaCSDataset.getDatasetId());
        
        Application zaragozaCMApp = new Application();
        zaragozaCMApp.setLang(Language.EN);
        zaragozaCMApp.setName("Zaragoza Maps (Collaborative maps)");
        zaragozaCMApp.setDescription("This application will permit the citizens to generate their own collaborative maps, " +
        		"adding new information on top of the city hall map repository.  Citizens create maps by defining Points of interest (POI) " +
        		"about a wide range of subjects. These maps can be consulted or edited by other citizens so their information is based on the" +
        		" collaboration of different citizens.");
        zaragozaCMApp.setUrl("https://play.google.com/store/apps/details?id=es.zgz.apps.maps");
        zaragozaCMApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-zaragoza-diymaps-logo-3097295127-4_avatar.png");
        zaragozaCMApp.setRelatedCouncilId(zgzCouncil.getCouncilId());
        zaragozaCMApp.setVersion("1.0.6");
        zaragozaCMApp.setTermsOfService("Terms of Service for Complaints & Suggestions");
        zaragozaCMApp.setPermissions("INTERNET, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, VIBRATE");
        zaragozaCMApp.setGooglePlayRating(0.0);
        zaragozaCMApp.setPackageName("es.zgz.apps.maps");
        zaragozaCMApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
            "zaragoza", "maps", "collaborative", "smart city", "mobility"
        })));
        zaragozaCMApp.setGeographicalScope(zgzCouncil.getGeographicalScope());
        
        zaragozaCMApp = modifyApp(userSession, zaragozaCMApp);
        
        
        Dataset zaragozaCMDataset = new Dataset();
        zaragozaCMDataset.setName("Collaborative Maps dataset");
        zaragozaCMDataset.setDescription("Dataset containing the POIs registered for the collaborative maps. http://www.zaragoza.es/ciudad/risp/mapas-colaborativos.htm");
        zaragozaCMDataset.setLang(Language.EN);
        
        zaragozaCMDataset = modifyDataset(userSession, zaragozaCMDataset);
		ApplicationManagement.addDataset(userSession, zaragozaCMApp.getAppId(), zaragozaCMDataset.getDatasetId());
        
		UserManagement.logout(userSession);
	}

	private static Council createMajadahondaCouncil(String adminSession) {
		final GeoScope majadahondaGeoScope = GeoScopeManagement.createScope(
                adminSession, "Majadahonda Region",
                new GeoPoint(40.4837203, -3.8448064),
                new GeoPoint(40.4450864, -3.8880567));
        
        Council majadahondaCouncil = new Council();
        majadahondaCouncil.setName("Majadahonda");
        majadahondaCouncil.setDescription("Majadahonda Council");
        majadahondaCouncil.setGeographicalScope(majadahondaGeoScope);
        majadahondaCouncil.setLang(Language.EN);
        majadahondaCouncil = CouncilManagement.createCouncil(adminSession, majadahondaCouncil);
        
        User majadahondaUser = new User();
        majadahondaUser.setName("Majadahonda Council Administrator");
        majadahondaUser.setUsername(MAJADAHONDA_ADMIN);
        majadahondaUser.setPassword(MAJADAHONDA_ADMIN_PASS);
        int majadahondaUserId = UserManagement.registerUser(majadahondaUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, majadahondaCouncil.getCouncilId(), majadahondaUserId);
		return majadahondaCouncil;
	}

	private static Council createRoveretoCouncil(String adminSession) {
		final GeoScope roveretoGeoScope = GeoScopeManagement.createScope(
                adminSession, "Rovereto Region",
                new GeoPoint(45.91260949999999, 11.0738002),
                new GeoPoint(45.85202, 10.9991401));
        Council roveretoCouncil = new Council();
        
        roveretoCouncil.setName("Rovereto");
        roveretoCouncil.setDescription("Rovereto Council");
        roveretoCouncil.setGeographicalScope(roveretoGeoScope);
        roveretoCouncil.setLang(Language.EN);
        roveretoCouncil = CouncilManagement.createCouncil(adminSession, roveretoCouncil);
        
        User roveretoUser = new User();
        roveretoUser.setName("Rovereto Council Administrator");
        roveretoUser.setUsername(ROVERETO_ADMIN);
        roveretoUser.setPassword(ROVERETO_ADMIN_PASS);
        int roveretoUserId = UserManagement.registerUser(roveretoUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, roveretoCouncil.getCouncilId(), roveretoUserId);
		return roveretoCouncil;
	}

	private static Council createBristolCouncil(String adminSession) {
		User bristolUser = new User();
        bristolUser.setName("Bristol Council Administrator");
        bristolUser.setUsername(BRISTOL_ADMIN);
        bristolUser.setPassword(BRISTOL_ADMIN_PASS);
        int bristolUserId = UserManagement.registerUser(bristolUser).getUserId();
        
        final GeoScope bristolGeoScope = GeoScopeManagement.createScope(
                adminSession, "Bristol Region",
                new GeoPoint(51.54443269999999, -2.4509025),
                new GeoPoint(51.3925452, -2.7305165));
        
        Council bristolCouncil = new Council();
        bristolCouncil.setName("Bristol");
        bristolCouncil.setDescription("Bristol Council");
        bristolCouncil.setGeographicalScope(bristolGeoScope);
        bristolCouncil.setLang(Language.EN);
        bristolCouncil = CouncilManagement.createCouncil(adminSession, bristolCouncil);
        
        CouncilManagement.addCouncilAdmin(adminSession, bristolCouncil.getCouncilId(), bristolUserId);
		return bristolCouncil;
	}

	private static Council createZgzCouncil(String adminSession) {
		final GeoScope zgzGeoScope = GeoScopeManagement.createScope(
                adminSession, "Zaragoza Region",
                new GeoPoint(41.6894079, -0.8427317999999999),
                new GeoPoint(41.6139746, -0.9472301000000001));
        
        Council zgzCouncil = new Council();
        zgzCouncil.setName("Zaragoza");
        zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
        zgzCouncil.setGeographicalScope(zgzGeoScope);
        zgzCouncil.setLang(Language.EN);
        zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

        User zgzUser = new User();
        zgzUser.setName("Zaragoza Council Administrator");
        zgzUser.setUsername(ZGZ_ADMIN);
        zgzUser.setPassword(ZGZ_ADMIN_PASS);
        int zgzUserId = UserManagement.registerUser(zgzUser).getUserId();
        
        CouncilManagement.addCouncilAdmin(adminSession, zgzCouncil.getCouncilId(), zgzUserId);
		return zgzCouncil;
	}
	
	private static void createPlayerData() {
		String userSession = TestUtils.loginAdminUser();
        
        Application playerApp = new Application();
        playerApp.setLang(Language.EN);
        playerApp.setName("IES Cities Player");
        playerApp.setDescription("The IES Cities Player acts as a launcher of the applications developed within the IES Cities framework, enabling internet-based services for the cities accross Europe. \n" + 
        		"Open Data made available by public administrations can thus reach to citizens through urban mobile apps. \n" + 
        		"A distinctive feature of those apps is that citizens can also contribute with new data, enriching the knowledge about the city. \n" + 
        		"Let's use IES Cities Player to know which applications are available in your nearby area!");
        playerApp.setUrl("https://play.google.com/store/apps/details?id=es.zgz.apps.complaints");
        playerApp.setImage("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/ies_cities-player-logo-2224361995-5_avatar.png");
        playerApp.setVersion("1.0.6");
        playerApp.setTermsOfService("Terms for IES Cities Player");
        playerApp.setPermissions("INTERNET, ACCESS_COARSE_LOCATION");
        playerApp.setGooglePlayRating(0.0);
        playerApp.setPackageName("eu.iescities.player");
        playerApp.setTags(new HashSet<String>(Arrays.asList(new String[]{
            "player", "install", "ies cities"
        })));
        
        playerApp = modifyApp(userSession, playerApp);
        
        UserManagement.logout(userSession);
	}
}
