package eu.iescities.server.restinterface.conf;

public class ContiPerConf {

	public static final int INVOCATIONS = 100;
	public static final int THREADS = 10;
}
