/*  
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;

public class UserRestInterfaceTest extends JerseyTest {

	private static final String USERID1_NAME = "other_user";
	private static final String USERID1_PASS = "other_pass";

	private static final String USERID2_NAME = "oldname";
	private static final String USERID2_PASS = "somepassword";

	private static int userId1, userId2;

	private static Council council = new Council();
	private static GeoScope geoscope;
	private static Application app;
	
	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		geoscope = GeoScopeManagement.createScope(adminSession,
				"Zaragoza Region",
				new GeoPoint(41.6894079, -0.8427317999999999), new GeoPoint(
						41.6139746, -0.9472301000000001));

		council.setName("Some council");
		council.setDescription("Council description");
		council.setGeographicalScope(geoscope);
		council.setLang(Language.EN);
		council = CouncilManagement.createCouncil(adminSession, council);

		User otherUser = new User();
		otherUser.setUsername(USERID1_NAME);
		otherUser.setPassword(USERID1_PASS);
		userId1 = UserManagement.registerUser(otherUser).getUserId();

		User anotherUser = new User();
		anotherUser.setUsername(USERID2_NAME);
		anotherUser.setPassword(USERID2_PASS);
		userId2 = UserManagement.registerUser(anotherUser).getUserId();

		CouncilManagement.addCouncilAdmin(adminSession,
				council.getCouncilId(), userId2);

		UserManagement.logout(adminSession);

		String userSession = UserManagement.login(USERID2_NAME, USERID2_PASS);

		Dataset dataset = new Dataset();
		dataset.setName("test_dataset");
		dataset.setJsonMapping("");
		dataset.setLang(Language.EN);
		dataset = DatasetManagement.registerDataset(userSession, dataset);

		app = new Application();
		app.setName("Test app");
		app.setDescription("Description of the test app ");
		app.setUrl("http://iescities.eu/");
		app.setImage("http://server.iescities.eu/testApp.jpg");
		app.setRelatedCouncilId(council.getCouncilId());
		app.setVersion("0.1");
		app.setTermsOfService("Terms of Service for app ");
		app.setPermissions("Permisions of test app ");
		app.setPackageName("some.package.name");
		app.setTags(new HashSet<String>(Arrays.asList(new String[] { "tag1",
				"tag2" })));
		app.setGeographicalScope(geoscope);
		app.setLang(Language.EN);

		app = ApplicationManagement.registerApp(userSession, app);
		ApplicationManagement.addDataset(userSession, app.getAppId(),
				dataset.getDatasetId());

		UserManagement.addInstalledApp(userSession, app.getAppId());

		UserManagement.logout(userSession);
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetCurrentUser() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User user = target("entities/users/me").request(
				MediaType.APPLICATION_JSON).get(User.class);

		assertEquals(TestUtils.ADMIN_NAME, user.getUsername());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUser() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User user = target("entities/users/" + userId1).request(
				MediaType.APPLICATION_JSON).get(User.class);

		assertEquals(USERID1_NAME, user.getUsername());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUserInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/users/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUserInstalledApps() {
		client().register(HttpAuthenticationFeature.basic(USERID2_NAME, USERID2_PASS));

		List<Application> applications = target("entities/users/me/installed")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<Application>>() {
						});

		assertEquals(1, applications.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUserDevelopedApps() {
		client().register(HttpAuthenticationFeature.basic(USERID2_NAME, USERID2_PASS));

		List<Application> applications = target("entities/users/me/developer")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<Application>>() {
						});

		assertEquals(1, applications.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUserDatasets() {
		client().register(HttpAuthenticationFeature.basic(USERID2_NAME, USERID2_PASS));

		List<Dataset> datasets = target("entities/users/me/datasets").request(
				MediaType.APPLICATION_JSON).get(
				new GenericType<List<Dataset>>() {
				});

		assertEquals(1, datasets.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetUserDatasetsInvalidLimit() {
		client().register(HttpAuthenticationFeature.basic(USERID2_NAME, USERID2_PASS));

		Response response = target("entities/users/me/datasets")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAddInstalledApp() {
		client().register(HttpAuthenticationFeature.basic(USERID1_NAME, USERID1_PASS));

		Application installedApp = new Application();
		installedApp.setAppId(app.getAppId());

		Entity<Application> entity = Entity.entity(installedApp,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/users/me/installed").request(
				MediaType.APPLICATION_JSON).put(entity, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		String userSession = UserManagement.login(USERID1_NAME, USERID1_PASS);

		assertEquals(1, UserManagement.getInstalledApps(userSession, Language.EN, null).size());

		UserManagement.logout(userSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAddInstalledAppInvalidID() {
		client().register(HttpAuthenticationFeature.basic(USERID1_NAME, USERID1_PASS));

		Application installedApp = new Application();
		installedApp.setAppId(Integer.MAX_VALUE);

		Entity<Application> entity = Entity.entity(installedApp,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/users/me/installed").request(
				MediaType.APPLICATION_JSON).put(entity, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testRemoveInstalledApp() {
		client().register(HttpAuthenticationFeature.basic(USERID1_NAME, USERID1_PASS));

		Application installedApp = new Application();
		installedApp.setAppId(app.getAppId());

		Entity<Application> entity = Entity.entity(installedApp,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/users/me/installed").request(
				MediaType.APPLICATION_JSON).put(entity, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("entities/users/me/installed/" + app.getAppId())
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		String userSession = UserManagement.login(USERID1_NAME, USERID1_PASS);

		assertEquals(0, UserManagement.getInstalledApps(userSession, Language.EN, null).size());

		UserManagement.logout(userSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testRemoveInstalledAppInvalidID() {
		client().register(HttpAuthenticationFeature.basic(USERID1_NAME, USERID1_PASS));

		Response response = target(
				"entities/users/me/installed/" + Integer.MAX_VALUE).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testSetIESAdminTrue() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/users/" + userId2 + "/admin/true")
				.request(MediaType.APPLICATION_JSON).post(null, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		String adminSession = UserManagement.login(TestUtils.ADMIN_NAME,
				TestUtils.ADMIN_PASSWORD);

		assertTrue(UserManagement.getUserById(adminSession, userId2).isAdmin());

		UserManagement.removeIesAdmin(adminSession, userId2);

		UserManagement.logout(adminSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testSetIESAdminTrueInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"entities/users/" + Integer.MAX_VALUE + "/admin/true").request(
				MediaType.APPLICATION_JSON).post(null, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testSetIESAdminFalse() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/users/" + userId2 + "/admin/true")
				.request(MediaType.APPLICATION_JSON).post(null, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		String adminSession = UserManagement.login(TestUtils.ADMIN_NAME,
				TestUtils.ADMIN_PASSWORD);

		assertTrue(UserManagement.getUserById(adminSession, userId2).isAdmin());

		UserManagement.removeIesAdmin(adminSession, userId2);

		UserManagement.logout(adminSession);

		response = target("entities/users/" + userId2 + "/admin/false")
				.request(MediaType.APPLICATION_JSON).post(null, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		adminSession = UserManagement.login(TestUtils.ADMIN_NAME,
				TestUtils.ADMIN_PASSWORD);

		assertFalse(UserManagement.getUserById(adminSession, userId2).isAdmin());

		UserManagement.logout(adminSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testSetIESAdminFalseInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"entities/users/" + Integer.MAX_VALUE + "/admin/false")
				.request(MediaType.APPLICATION_JSON).post(null, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetAllUsers() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		List<User> users = target("/entities/users/all").request(
				MediaType.APPLICATION_JSON).get(new GenericType<List<User>>() {
		});

		assertEquals(3, users.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetAllUsersInvalidLimit() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("/entities/users/all")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testLogin() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User user = target("entities/users/login").request(
				MediaType.APPLICATION_JSON).get(User.class);

		assertEquals(TestUtils.ADMIN_NAME, user.getUsername());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDeleteUser() {
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername("temp_user");
		user.setPassword("temp_pass");
		int userId = UserManagement.registerUser(user).getUserId();

		UserManagement.logout(adminSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/users/" + userId1).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		adminSession = TestUtils.loginAdminUser();

		UserManagement.getUserById(adminSession, userId);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testDeleteUserInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/users/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDeleteCurrentUser() {
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername("temp_user");
		user.setPassword("temp_pass");
		int userId = UserManagement.registerUser(user).getUserId();

		UserManagement.logout(adminSession);

		client().register(HttpAuthenticationFeature.basic("temp_user", "temp_pass"));

		Response response = target("/entities/users/me").request().delete(
				Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		adminSession = TestUtils.loginAdminUser();

		UserManagement.getUserById(adminSession, userId);
	}

	@Test
	public void testCreateUser() {
		User userInfo = new User();
		userInfo.setUsername("someusername");
		userInfo.setPassword("somepassword");

		Entity<User> entity = Entity.entity(userInfo,
				MediaType.APPLICATION_JSON);

		User user = target("entities/users/").request(
				MediaType.APPLICATION_JSON).post(entity, User.class);

		assertEquals(userInfo.getUsername(), user.getUsername());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testCreateUserAlreadyExists() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User userInfo = new User();
		userInfo.setUsername(USERID2_NAME);
		userInfo.setPassword(USERID2_PASS);

		Entity<User> entity = Entity.entity(userInfo,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/users/").request(
				MediaType.APPLICATION_JSON).post(entity, Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testUpdateUser() {
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername("temp_user");
		user.setPassword("temp_pass");
		UserManagement.registerUser(user).getUserId();

		UserManagement.logout(adminSession);

		client().register(HttpAuthenticationFeature.basic("temp_user", "temp_pass"));

		User updateUserInfo = new User();
		updateUserInfo.setPassword("newpassword");
		updateUserInfo.setName("The name");
		updateUserInfo.setSurname("The surname");

		Entity<User> updateEntity = Entity.entity(updateUserInfo,
				MediaType.APPLICATION_JSON);

		user = target("entities/users/" + userId2).request(
				MediaType.APPLICATION_JSON).put(updateEntity, User.class);

		assertEquals(updateUserInfo.getName(), user.getName());
		assertEquals(updateUserInfo.getSurname(), user.getSurname());
	}

//	@Test
//	public void testUpdateUserInvalidID() {
//		client().register(
//				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
//						TestUtils.ADMIN_PASSWORD));
//
//		User updateUserInfo = new User();
//		updateUserInfo.setPassword("newpassword");
//		updateUserInfo.setName("The name");
//		updateUserInfo.setSurname("The surname");
//
//		Entity<User> updateEntity = Entity.entity(updateUserInfo,
//				MediaType.APPLICATION_JSON);
//
//		Response response = target("entities/users/" + Integer.MAX_VALUE)
//				.request(MediaType.APPLICATION_JSON).put(updateEntity,
//						Response.class);
//
//		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
//				response.getStatus());
//	}
}