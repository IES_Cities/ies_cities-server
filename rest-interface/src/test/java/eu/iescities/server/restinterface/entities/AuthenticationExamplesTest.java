/*  
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;

public class AuthenticationExamplesTest extends JerseyTest {

	private static final String USER_NAME = "user";
	private static final String USER_PASS = "some_pass";

	private static final String COUNCIL_ADMIN_NAME = "council_admin";
	private static final String COUNCIL_ADMIN_PASS = "some_pass";

	private static final String APP_DEVELOPER_NAME = "app_developer";
	private static final String APP_DEVELOPER_PASS = "some_pass";
	
	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername(USER_NAME);
		user.setPassword(USER_PASS);
		UserManagement.registerUser(user).getUserId();

		User councilAdmin = new User();
		councilAdmin.setUsername(COUNCIL_ADMIN_NAME);
		councilAdmin.setPassword(COUNCIL_ADMIN_PASS);

		int councilAdminId = UserManagement.registerUser(councilAdmin)
				.getUserId();

		GeoScope geoScope = GeoScopeManagement.createScope(adminSession,
				"Zaragoza Region",
				new GeoPoint(41.6894079, -0.8427317999999999), new GeoPoint(
						41.6139746, -0.9472301000000001));

		Council council = new Council();
		council.setName("Zaragoza");
		council.setDescription("Ayuntamiento de Zaragoza");
		council.setGeographicalScope(geoScope);
		council.setLang(Language.EN);
		council = CouncilManagement.createCouncil(adminSession, council);

		CouncilManagement.addCouncilAdmin(adminSession,
				council.getCouncilId(), councilAdminId);

		User appDeveloper = new User();
		appDeveloper.setUsername(APP_DEVELOPER_NAME);
		appDeveloper.setPassword(APP_DEVELOPER_PASS);

		UserManagement.registerUser(appDeveloper).getUserId();

		UserManagement.logout(adminSession);

		Application app = new Application();
		app.setName("Application name");
		app.setDescription("Some description");
		app.setUrl("http://iescities.eu");
		app.setImage("http://server.iescities.eu/testApp.jpg");
		app.setRelatedCouncilId(council.getCouncilId());
		app.setVersion("0.1");
		app.setTermsOfService("Terms of Service for app 1");
		app.setPermissions("Permisions of service 1");
		app.setPackageName("com.example.package");
		app.setTags(new HashSet<String>(Arrays.asList(new String[] { "tag1",
				"tag2", "tag3" })));
		app.setGeographicalScope(council.getGeographicalScope());
		app.setLang(Language.EN);

		String developerSession = UserManagement.login(APP_DEVELOPER_NAME,
				APP_DEVELOPER_PASS);

		app = ApplicationManagement.registerApp(developerSession, app);

		UserManagement.logout(developerSession);
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAnonymous() {
		Response response = target("auth/test/any/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/user/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/admin/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/council/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/developer/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testRegistered() {
		client().register(HttpAuthenticationFeature.basic(USER_NAME, USER_PASS));

		Response response = target("auth/test/any/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/user/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/admin/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/council/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/developer/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAdmin() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("auth/test/any/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/user/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/admin/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/council/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/developer/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testCouncilAdmin() {
		client().register(
				HttpAuthenticationFeature.basic(COUNCIL_ADMIN_NAME, COUNCIL_ADMIN_PASS));

		Response response = target("auth/test/any/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/user/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/admin/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/council/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/developer/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAppDeveloper() {
		client().register(
				HttpAuthenticationFeature.basic(APP_DEVELOPER_NAME, APP_DEVELOPER_PASS));

		Response response = target("auth/test/any/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/user/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		response = target("auth/test/admin/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/council/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(),
				response.getStatus());

		response = target("auth/test/developer/hello").request(
				MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
}