package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.Dataset;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;

public class CouncilRestInterfaceTest extends JerseyTest {

	private static final String ZGZ_ADMIN_USER = "zgz_admin";
	private static final String ZGZ_ADMIN_PASS = "some_pass";

	private static Council zgzCouncil;
	
	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	public static User addAdminUser() {
		return TestUtils.addAdminUser();
	}

	public static String loginAdminUser() {
		return TestUtils.loginAdminUser();
	}

	/**
	 * Executed before the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		GeoScope zgzGeoScope = GeoScopeManagement.createScope(adminSession,
				"Zaragoza Region",
				new GeoPoint(41.6894079, -0.8427317999999999), new GeoPoint(
						41.6139746, -0.9472301000000001));

		zgzCouncil = new Council();
		zgzCouncil.setName("Zaragoza");
		zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
		zgzCouncil.setGeographicalScope(zgzGeoScope);
		zgzCouncil.setLang(Language.EN);
		zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

		final User zgzAdminUser = new User();
		zgzAdminUser.setUsername(ZGZ_ADMIN_USER);
		zgzAdminUser.setPassword(ZGZ_ADMIN_PASS);
		zgzAdminUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		int zgzAdminUserId = UserManagement.registerUser(zgzAdminUser)
				.getUserId();

		CouncilManagement.addCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), zgzAdminUserId);

		final GeoScope bristolGeoScope = GeoScopeManagement.createScope(
				adminSession, "Bristol Region", new GeoPoint(51.54443269999999,
						-2.4509025), new GeoPoint(51.3925452, -2.7305165));

		Council bristolCouncil = new Council();
		bristolCouncil.setName("Bristol");
		bristolCouncil.setDescription("Bristol Council");
		bristolCouncil.setGeographicalScope(bristolGeoScope);
		bristolCouncil.setLang(Language.EN);
		bristolCouncil = CouncilManagement.createCouncil(adminSession,
				bristolCouncil);

		final GeoScope roveretoGeoScope = GeoScopeManagement.createScope(
				adminSession, "Rovereto Region", new GeoPoint(
						45.91260949999999, 11.0738002), new GeoPoint(45.85202,
						10.9991401));
		Council roveretoCouncil = new Council();

		roveretoCouncil.setName("Rovereto");
		roveretoCouncil.setDescription("Rovereto Council");
		roveretoCouncil.setGeographicalScope(roveretoGeoScope);
		roveretoCouncil.setLang(Language.EN);
		roveretoCouncil = CouncilManagement.createCouncil(adminSession,
				roveretoCouncil);

		final GeoScope majadahondaGeoScope = GeoScopeManagement.createScope(
				adminSession, "Majadahonda Region", new GeoPoint(40.4837203,
						-3.8448064), new GeoPoint(40.4450864, -3.8880567));

		Council majadahondaCouncil = new Council();
		majadahondaCouncil.setName("Majadahonda");
		majadahondaCouncil.setDescription("Majadahonda Council");
		majadahondaCouncil.setGeographicalScope(majadahondaGeoScope);
		majadahondaCouncil.setLang(Language.EN);
		majadahondaCouncil = CouncilManagement.createCouncil(adminSession,
				majadahondaCouncil);

		UserManagement.logout(adminSession);

		String zgzAdminSession = UserManagement.login(ZGZ_ADMIN_USER,
				ZGZ_ADMIN_PASS);

		// Register SPARQL endpoint dataset		
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");

		Dataset sparqlDataset = new Dataset();
		sparqlDataset.setName("sparql_dataset");
		sparqlDataset.setJsonMapping(jsonSchema);
		sparqlDataset.setLang(Language.EN);
		DatasetManagement.registerDataset(zgzAdminSession, sparqlDataset);
		
		final String dbConfig = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		Dataset sqliteDataset = new Dataset();
		sqliteDataset.setName("sqlite_dataset");
		sqliteDataset.setJsonMapping(dbConfig);
		sqliteDataset.setLang(Language.EN);
		DatasetManagement.registerDataset(zgzAdminSession, sqliteDataset);

		String description = "The service allows users to obtain information of the "
				+ "leisure and cultural offer of the city, both private and public, "
				+ "filtered by their preferences. The user gets all the information of "
				+ "the selected event (date, time, price, geolocation, availability, "
				+ "type of target user, etc.). In addition, the service sends alerts to "
				+ "registered users which fit their profile.";

		Application tempApp = new Application();
		tempApp.setName("Majadahonda Leisure & Events");
		tempApp.setDescription(description);
		tempApp.setUrl("http://iescities.eu");
		tempApp.setImage("http://server.iescities.eu/testApp.jpg");
		tempApp.setRelatedCouncilId(zgzCouncil.getCouncilId());
		tempApp.setVersion("0.1");
		tempApp.setTermsOfService("Terms of Service for app 1");
		tempApp.setPermissions("Permisions of service 1");
		tempApp.setPackageName("com.example.viveMajadahonda");
		tempApp.setTags(new HashSet<String>(Arrays.asList(new String[] {
				"Majadahonda", "leisure", "events", "citizens", "shops",
				"shopping" })));
		tempApp.setGeographicalScope(zgzCouncil.getGeographicalScope());
		tempApp.setLang(Language.EN);

		ApplicationManagement.registerApp(zgzAdminSession, tempApp);

		UserManagement.logout(zgzAdminSession);
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllCouncils() {
		List<Council> councils = target("entities/councils/all").request(
				MediaType.APPLICATION_JSON).get(
				new GenericType<List<Council>>() {
				});

		assertEquals(4, councils.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllCouncilsInvalidLimit() {
		Response response = target("entities/councils/all")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testCreateCouncil() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Council newCouncil = new Council();
		newCouncil.setName("Test City");
		newCouncil.setDescription("Test Council");
		newCouncil.setLang(Language.EN);

		GeoScope geoScope = new GeoScope();
		geoScope.setName("Region Test");
		geoScope.setNorthEastLatitude(40.4837203);
		geoScope.setNorthEastLongitude(-3.8448064);
		geoScope.setSouthWestLatitude(40.4450864);
		geoScope.setSouthWestLongitude(-3.8880567);

		newCouncil.setGeographicalScope(geoScope);

		Entity<Council> entity = Entity.entity(newCouncil,
				MediaType.APPLICATION_JSON);

		Council council = target("entities/councils/").request(
				MediaType.APPLICATION_JSON).post(entity, Council.class);

		assertEquals(newCouncil.getName(), council.getName());

		assertTrue(newCouncil.getGeographicalScope().getNorthEastLatitude() == council
				.getGeographicalScope().getNorthEastLatitude());
		assertTrue(newCouncil.getGeographicalScope().getNorthEastLongitude() == council
				.getGeographicalScope().getNorthEastLongitude());
		assertTrue(newCouncil.getGeographicalScope().getSouthWestLatitude() == council
				.getGeographicalScope().getSouthWestLatitude());
		assertTrue(newCouncil.getGeographicalScope().getSouthWestLongitude() == council
				.getGeographicalScope().getSouthWestLongitude());
	}

	@Test
	public void testCreateCouncilAlreadyExists() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Council newCouncil = new Council();
		newCouncil.setName("Majadahonda");
		newCouncil.setDescription("Test Council");

		GeoScope geoScope = new GeoScope();
		geoScope.setName("New geoscope test");
		geoScope.setNorthEastLatitude(40.4837203);
		geoScope.setNorthEastLongitude(-3.8448064);
		geoScope.setSouthWestLatitude(40.4450864);
		geoScope.setSouthWestLongitude(-3.8880567);

		newCouncil.setGeographicalScope(geoScope);

		Entity<Council> entity = Entity.entity(newCouncil,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/councils/").request(
				MediaType.APPLICATION_JSON).post(entity, Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testDeleteCouncil() {
		String adminSession = TestUtils.loginAdminUser();

		GeoScope newGeoScope = GeoScopeManagement.createScope(adminSession,
				"Some geoscope", new GeoPoint(40.4837203, -3.8448064),
				new GeoPoint(40.4450864, -3.8880567));

		Council newCouncil = new Council();
		newCouncil.setName("Council name");
		newCouncil.setDescription("Council description");
		newCouncil.setGeographicalScope(newGeoScope);
		newCouncil.setLang(Language.EN);
		newCouncil = CouncilManagement.createCouncil(adminSession, newCouncil);

		UserManagement.logout(adminSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"entities/councils/" + newCouncil.getCouncilId()).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testDeleteCouncilInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/councils/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasets() {
		List<Dataset> datasets = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/datasets")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<Dataset>>() {
						});

		assertEquals(2, datasets.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetsInvalidLimit() {
		Response response = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/datasets")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetsInvalidID() {
		Response response = target(
				"entities/councils/" + Integer.MAX_VALUE + "/datasets")
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetApplications() {
		List<Application> applications = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/apps")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<Application>>() {
						});

		assertEquals(1, applications.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetApplicationsInvalidLimit() {
		Response response = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/apps")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetApplicationsInvalidID() {
		Response response = target(
				"entities/councils/" + Integer.MAX_VALUE + "/apps").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testUpdateCouncil() {
		String adminSession = TestUtils.loginAdminUser();

		Council council = new Council();

		council.setName("Updateable City");
		council.setDescription("Some description");
		council.setGeographicalScope(zgzCouncil.getGeographicalScope());
		council.setLang(Language.EN);
		council = CouncilManagement.createCouncil(adminSession, council);

		UserManagement.logout(adminSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Council updateCouncil = new Council();
		updateCouncil.setName("Updated City");
		updateCouncil.setGeographicalScope(zgzCouncil.getGeographicalScope());
		updateCouncil.setDescription("Updated description");
		updateCouncil.setLang(Language.EN);

		Entity<Council> entity = Entity.entity(updateCouncil,
				MediaType.APPLICATION_JSON)	;

		updateCouncil = target("entities/councils/" + council.getCouncilId())
				.request(MediaType.APPLICATION_JSON).put(entity, Council.class);

		assertEquals("Updated City", updateCouncil.getName());
		assertEquals("Updated description", updateCouncil.getDescription());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testUpdateCouncilInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Council updateCouncil = new Council();
		updateCouncil.setName("Updated City");
		updateCouncil.setGeographicalScope(zgzCouncil.getGeographicalScope());
		updateCouncil.setDescription("Updated description");

		Entity<Council> entity = Entity.entity(updateCouncil,
				MediaType.APPLICATION_JSON);

		Response response = target("entities/councils/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllCouncilAdmins() {
		List<User> admins = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/admins")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<User>>() {
						});

		assertEquals(1, admins.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllCouncilAdminsInvalidID() {
		Response response = target(
				"entities/councils/" + Integer.MAX_VALUE + "/admins").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
	
	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetAllCouncilUsers() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		List<User> users = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/users")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<User>>() {
						});

		assertEquals(1, users.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetAllCouncilUsersInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"entities/councils/" + Integer.MAX_VALUE + "/users").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetAllCouncilUsersInvalidLimit() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"entities/councils/" + zgzCouncil.getCouncilId() + "/users")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testAddAdmin() {
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername("temp_user");
		user.setPassword("temp_pass");
		int userId = UserManagement.registerUser(user).getUserId();

		UserManagement.logout(adminSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User newAdmin = new User();
		newAdmin.setUserId(userId);

		Entity<User> entity = Entity.entity(newAdmin,
				MediaType.APPLICATION_JSON);

		Response response = target(
				"/entities/councils/" + zgzCouncil.getCouncilId() + "/admins")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		adminSession = TestUtils.loginAdminUser();

		assertEquals(
				2,
				CouncilManagement.getCouncilAdmins(zgzCouncil.getCouncilId(),
						0, 1000).size());

		CouncilManagement.removeCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), userId);

		UserManagement.removeUser(adminSession, userId);

		UserManagement.logout(adminSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testAddAdminInvalidUserId() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User newAdmin = new User();
		newAdmin.setUserId(Integer.MAX_VALUE);

		Entity<User> entity = Entity.entity(newAdmin,
				MediaType.APPLICATION_JSON);

		Response response = target(
				"/entities/councils/" + zgzCouncil.getCouncilId() + "/admins")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testRemoveAdmin() {
		String adminSession = TestUtils.loginAdminUser();

		User user = new User();
		user.setUsername("temp_user");
		user.setPassword("temp_pass");
		int userId = UserManagement.registerUser(user).getUserId();

		UserManagement.logout(adminSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		User newAdmin = new User();
		newAdmin.setUserId(userId);

		Entity<User> entity = Entity.entity(newAdmin,
				MediaType.APPLICATION_JSON);

		Response response = target(
				"/entities/councils/" + zgzCouncil.getCouncilId() + "/admins")
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		adminSession = TestUtils.loginAdminUser();

		assertEquals(
				2,
				CouncilManagement.getCouncilAdmins(zgzCouncil.getCouncilId(),
						0, 1000).size());

		CouncilManagement.removeCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), userId);

		UserManagement.logout(adminSession);

		response = target(
				"/entities/councils/" + zgzCouncil.getCouncilId() + "/admins/"
						+ userId).request(MediaType.APPLICATION_JSON).delete(
				Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

		adminSession = TestUtils.loginAdminUser();

		assertEquals(
				1,
				CouncilManagement.getCouncilAdmins(zgzCouncil.getCouncilId(),
						0, 1000).size());

		UserManagement.removeUser(adminSession, userId);

		UserManagement.logout(adminSession);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testRemoveAdminInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target(
				"/entities/councils/" + Integer.MAX_VALUE + "/admins/"
						+ Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetCouncil() {
		Council council = target(
				"/entities/councils/" + zgzCouncil.getCouncilId()).request(
				MediaType.APPLICATION_JSON).get(Council.class);

		assertEquals(zgzCouncil.getName(), council.getName());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetCouncilInvalidID() {
		Response response = target("/entities/councils/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
}