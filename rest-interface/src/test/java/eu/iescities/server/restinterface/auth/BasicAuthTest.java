package eu.iescities.server.restinterface.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class BasicAuthTest {

	@Test
	public void testDecode() {
		final String[] data = BasicAuth.decode("dXNlcjpwYXNz");
		assertEquals("user", data[0]);
		assertEquals("pass", data[1]);
	}
	
	@Test
	public void testDecodeInvalid() {
		assertNull(BasicAuth.decode(""));
	}
}
