/*  
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;

public class GeoscopeRestInterfaceTest extends JerseyTest {

	private static final String SOME_NAME = "some_user";
	private static final String SOME_PASS = "some_pass";

	private static User user;

	private static GeoScope geoscope1;
	
	@Rule 
    public ContiPerfRule rule = new ContiPerfRule();

	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		user = new User();
		user.setUsername(SOME_NAME);
		user.setPassword(SOME_PASS);
		user = UserManagement.registerUser(user);

		UserManagement.logout(adminSession);

		String userSession = UserManagement.login(SOME_NAME, SOME_PASS);

		geoscope1 = GeoScopeManagement.createScope(userSession, "Geoscope 1",
				new GeoPoint(41.6894079, -0.8427317999999999), new GeoPoint(
						41.6139746, -0.9472301000000001));

		GeoScopeManagement.createScope(userSession, "Geoscope 2", new GeoPoint(
				45.91260949999999, 11.0738002), new GeoPoint(45.85202,
				10.9991401));

		UserManagement.logout(userSession);
	}

	/**
	 * Executed after the test suite of this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test
	public void testCreateGeoscope() {
		client().register(HttpAuthenticationFeature.basic(SOME_NAME, SOME_PASS));

		GeoScope geoscope = new GeoScope();
		geoscope.setName("New geoscope");
		geoscope.setNorthEastGeoPoint(new GeoPoint(45.91260949999999,
				11.0738002));
		geoscope.setSouthWestGeoPoint(new GeoPoint(45.85202, 10.9991401));

		Entity<GeoScope> entity = Entity.entity(geoscope,
				MediaType.APPLICATION_JSON);

		geoscope = target("entities/geoscopes/").request(
				MediaType.APPLICATION_JSON).post(entity, GeoScope.class);

		assertEquals("New geoscope", geoscope.getName());
	}

	@Test
	public void testDeleteGeoscope() {
		String userSession = UserManagement.login(SOME_NAME, SOME_PASS);

		GeoScope geoscope = GeoScopeManagement.createScope(userSession,
				"temp_geoscope", new GeoPoint(41.6894079, -0.8427317999999999),
				new GeoPoint(41.6139746, -0.9472301000000001));

		UserManagement.logout(userSession);

		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/geoscopes/" + geoscope.getId())
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testDeleteGeoscopeInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(TestUtils.ADMIN_NAME,
						TestUtils.ADMIN_PASSWORD));

		Response response = target("entities/geoscopes/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetGeoscope() {
		GeoScope geoscope = target("entities/geoscopes/" + geoscope1.getId())
				.request(MediaType.APPLICATION_JSON).get(GeoScope.class);

		assertEquals(geoscope1.getName(), geoscope.getName());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetGeoscopeInvalidID() {
		Response response = target("entities/geoscopes/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testSearchGeoscopes() {
		List<GeoScope> geoscopes = target("entities/geoscopes/search/")
				.queryParam("keywords", "Geoscope")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<GeoScope>>() {
				});

		assertEquals(2, geoscopes.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testSearchGeoscopesInvalidLimit() {
		Response response = target("entities/geoscopes/search/")
				.queryParam("keywords", "Geoscope").queryParam("limit", 2000)
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testCoveringPoint() {
		List<GeoScope> geoscopes = target(
				"entities/geoscopes/covering/41.6894079, -0.8427317999999999")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<GeoScope>>() {
						});

		assertEquals(1, geoscopes.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testCoveringPointBadNumber() {
		Response response = target("entities/geoscopes/covering/41.6894079,w")
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}
}