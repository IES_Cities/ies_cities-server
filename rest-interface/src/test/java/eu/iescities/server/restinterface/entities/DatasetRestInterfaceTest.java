/*
 *   Author: Unai Aguilera <unai.aguilera@deusto.es>
 */
package eu.iescities.server.restinterface.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import eu.iescities.server.accountinterface.ApplicationManagement;
import eu.iescities.server.accountinterface.CouncilManagement;
import eu.iescities.server.accountinterface.DatasetManagement;
import eu.iescities.server.accountinterface.GeoScopeManagement;
import eu.iescities.server.accountinterface.TestUtils;
import eu.iescities.server.accountinterface.UserManagement;
import eu.iescities.server.accountinterface.datainterface.Application;
import eu.iescities.server.accountinterface.datainterface.Council;
import eu.iescities.server.accountinterface.datainterface.GeoPoint;
import eu.iescities.server.accountinterface.datainterface.GeoScope;
import eu.iescities.server.accountinterface.datainterface.Language;
import eu.iescities.server.accountinterface.datainterface.User;
import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.util.FileLoader;
import eu.iescities.server.restinterface.application.TestApplication;
import eu.iescities.server.restinterface.conf.ContiPerConf;
import eu.iescities.server.restinterface.serialization.Dataset;
import eu.iescities.server.restinterface.serialization.DatasetBodyReader;
import eu.iescities.server.restinterface.serialization.DatasetBodyWriter;
import eu.iescities.server.restinterface.serialization.DatasetListBodyReader;
import eu.iescities.server.restinterface.serialization.DatasetListBodyWriter;
import eu.iescities.server.restinterface.serialization.DatasetSourceConnectorBodyReader;
import eu.iescities.server.restinterface.serialization.SchemaInfo;

public class DatasetRestInterfaceTest extends JerseyTest {

	private static final String ZGZ_ADMIN_USER = "zgz_admin";
	private static final String ZGZ_ADMIN_PASS = "some_pass";

	private static final String BASIC_USER = "basic_user";
	private static final String BASIC_PASS = "basic_pass";

	private static final String OTHER_USER = "other_user";
	private static final String OTHER_PASS = "other_pass";

	private static User basicUser;
	private static User otherUser;

	private static final List<eu.iescities.server.accountinterface.datainterface.Dataset> registeredDatasets = new ArrayList<eu.iescities.server.accountinterface.datainterface.Dataset>();

	private static eu.iescities.server.accountinterface.datainterface.Dataset jsonDataset;
	private static eu.iescities.server.accountinterface.datainterface.Dataset emptyMappingDataset;

	private static Council zgzCouncil;
	private static User zgzAdminUser;
	
	@Rule
	public ContiPerfRule rule = new ContiPerfRule();
	
	@Override
	protected javax.ws.rs.core.Application configure() {
		return new TestApplication();
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(DatasetSourceConnectorBodyReader.class);
		config.register(DatasetListBodyReader.class);
		config.register(DatasetListBodyWriter.class);
		config.register(DatasetBodyReader.class);
		config.register(DatasetBodyWriter.class);
		super.configureClient(config);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.cleanUpDatastore();

		TestUtils.addAdminUser().getUserId();
		String adminSession = TestUtils.loginAdminUser();

		final GeoScope zgzGeoScope = GeoScopeManagement.createScope(
				adminSession, "Zaragoza Region", new GeoPoint(41.6894079,
						-0.8427317999999999), new GeoPoint(41.6139746,
						-0.9472301000000001));

		zgzCouncil = new Council();
		zgzCouncil.setName("Zaragoza");
		zgzCouncil.setDescription("Ayuntamiento de Zaragoza");
		zgzCouncil.setGeographicalScope(zgzGeoScope);
		zgzCouncil.setLang(Language.EN);
		zgzCouncil = CouncilManagement.createCouncil(adminSession, zgzCouncil);

		zgzAdminUser = new User();
		zgzAdminUser.setUsername(ZGZ_ADMIN_USER);
		zgzAdminUser.setPassword(ZGZ_ADMIN_PASS);
		zgzAdminUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		zgzAdminUser = UserManagement.registerUser(zgzAdminUser);

		CouncilManagement.addCouncilAdmin(adminSession,
				zgzCouncil.getCouncilId(), zgzAdminUser.getUserId());

		basicUser = new User();
		basicUser.setUsername(BASIC_USER);
		basicUser.setPassword(BASIC_PASS);
		basicUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		basicUser = UserManagement.registerUser(basicUser);

		otherUser = new User();
		otherUser.setUsername(OTHER_USER);
		otherUser.setPassword(OTHER_PASS);
		otherUser.setPreferredCouncilId(zgzCouncil.getCouncilId());
		otherUser = UserManagement.registerUser(otherUser);

		UserManagement.logout(adminSession);

		// //////////////// Test dataset information registration
		// /////////////////////////

		String zgzAdminSession = UserManagement.login(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS);

		// Register SPARQL endpoint dataset
		final String jsonSchema = FileLoader.loadResource("/sparql/foaf_schema.json");

		eu.iescities.server.accountinterface.datainterface.Dataset sparqlDataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		sparqlDataset.setName("sparql_dataset");
		sparqlDataset.setJsonMapping(jsonSchema);
		sparqlDataset.setQuality(4);
		sparqlDataset.setQualityVerifiedBy(zgzAdminUser.getUserId());
		sparqlDataset.setLang(Language.EN);
		registeredDatasets.add(DatasetManagement.registerDataset(zgzAdminSession, sparqlDataset));

		// Register SQL dataset
		String dbConfig = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		eu.iescities.server.accountinterface.datainterface.Dataset sqliteDataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		sqliteDataset.setName("sqlite_dataset");
		sqliteDataset.setJsonMapping(dbConfig);
		sqliteDataset.setLang(Language.EN);
		registeredDatasets.add(DatasetManagement.registerDataset(
				zgzAdminSession, sqliteDataset));

		// Register REST dataset
		eu.iescities.server.accountinterface.datainterface.Dataset restDataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		restDataset.setName("rest_dataset");
		restDataset.setJsonMapping(dbConfig);
		restDataset.setLang(Language.EN);
		registeredDatasets.add(DatasetManagement.registerDataset(
				zgzAdminSession, restDataset));

		//Register database
		dbConfig = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		eu.iescities.server.accountinterface.datainterface.Dataset majadahondaDataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		majadahondaDataset.setName("majadahonda_dataset");
		majadahondaDataset.setJsonMapping(dbConfig);
		majadahondaDataset.setLang(Language.EN);
		registeredDatasets.add(DatasetManagement.registerDataset(
				zgzAdminSession, majadahondaDataset));
		
		//Register empty mapping dataset
		emptyMappingDataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		emptyMappingDataset.setName("empty_mapping");
		emptyMappingDataset.setLang(Language.EN);
		emptyMappingDataset = DatasetManagement.registerDataset(zgzAdminSession, emptyMappingDataset);

		Application app = new Application();
		app.setName("Test app");
		app.setDescription("Description of the test app ");
		app.setUrl("http://iescities.eu/");
		app.setImage("http://server.iescities.eu/testApp.jpg");
		app.setRelatedCouncilId(zgzCouncil.getCouncilId());
		app.setVersion("0.1");
		app.setTermsOfService("Terms of Service for app ");
		app.setPermissions("Permisions of test app ");
		app.setPackageName("some.package.name");
		app.setTags(new HashSet<String>(Arrays.asList(new String[] { "tag1",
				"tag2" })));
		app.setGeographicalScope(zgzGeoScope);
		app.setLang(Language.EN);

		app = ApplicationManagement.registerApp(zgzAdminSession, app);
		ApplicationManagement.addDataset(zgzAdminSession, app.getAppId(),
				registeredDatasets.get(0).getDatasetId());

		// Register json backed dataset
		String jsonMapping = FileLoader.loadResource("/json/mapping/jsonmapping1.json");

		eu.iescities.server.accountinterface.datainterface.Dataset dataset = new eu.iescities.server.accountinterface.datainterface.Dataset();
		dataset.setName("json_dataset");
		dataset.setDescription("This method provides an example of dataset that is backed by a JSON resource file");
		dataset.setJsonMapping(jsonMapping);
		dataset.setLang(Language.EN);
		jsonDataset = DatasetManagement.registerDataset(zgzAdminSession, dataset);

		UserManagement.logout(zgzAdminSession);

		String userSession = UserManagement.login(BASIC_USER, BASIC_PASS);

		dbConfig = FileLoader.loadResource("/dbconfig/sqlite_conf.json");
		Dataset userDataset = new Dataset();
		userDataset.setName("user_dataset");
		userDataset.setMapping(dbConfig);
		userDataset.setLang(Language.EN);
		registeredDatasets.add(DatasetManagement.registerDataset(userSession, userDataset.getDBDataset()));

		UserManagement.logout(userSession);
	}

	/**
	 * Executed after the test suite of this class.
	 *
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestUtils.cleanUpDatastore();
	}

	@Test
	public void testCreateDatasetCouncilAdmin() throws IOException, DataSourceConnectorException {
		final String json = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		Dataset datasetInfo = new Dataset();
		datasetInfo.setName("new_dataset");
		datasetInfo.setDescription("Some long description.");
		datasetInfo.setMapping(json);
		datasetInfo.setLang(Language.EN);

		Entity<Dataset> entity = Entity.entity(datasetInfo,
				MediaType.APPLICATION_JSON);

		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Dataset dataset = target("entities/datasets/").request(
				MediaType.APPLICATION_JSON).post(entity, Dataset.class);

		assertEquals("new_dataset", dataset.getName());
		assertEquals("Some long description.", dataset.getDescription());

		DataSourceConnector expected = ConnectorFactory.load(json);
		assertEquals(expected, dataset.getJsonMapping());
		assertEquals(zgzCouncil.getCouncilId(), dataset.getCouncilId());
		assertEquals(zgzAdminUser.getUserId(), dataset.getUserId());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testCreateDatasetAlreadyExists() throws IOException, DataSourceConnectorException {
		final String jsonSchema = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		Dataset datasetInfo = new Dataset();
		datasetInfo.setName("rest_dataset");
		datasetInfo.setDescription("Some long description.");
		datasetInfo.setMapping(jsonSchema);
		datasetInfo.setLang(Language.EN);

		Entity<Dataset> entity = Entity.entity(datasetInfo,
				MediaType.APPLICATION_JSON);

		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Response response = target("entities/datasets/").request(
				MediaType.APPLICATION_JSON).post(entity, Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testUpdateDataset() throws IOException, DataSourceConnectorException {
		eu.iescities.server.accountinterface.datainterface.Dataset ds = registeredDatasets.get(1);
		Dataset dataset = new Dataset(ds);
		dataset.setName("changed_dataset_name");
		dataset.setDescription("Some new description.");
		dataset.setLang(Language.EN);

		final String json = FileLoader.loadResource("/sparql/foaf_schema.json");
		dataset.setMapping(json);

		Entity<Dataset> entity = Entity.entity(dataset,
				MediaType.APPLICATION_JSON);

		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		target("entities/datasets/" + dataset.getDatasetId())
				.request(MediaType.APPLICATION_JSON).put(entity, Dataset.class);

		ds = DatasetManagement.getDatasetById(dataset.getDatasetId(), Language.EN, null);
		Dataset updatedDataset = new Dataset(ds);

		assertEquals("changed_dataset_name", updatedDataset.getName());
		assertEquals("Some new description.", updatedDataset.getDescription());

		DataSourceConnector expected = ConnectorFactory.load(json);
		assertEquals(expected, dataset.getJsonMapping());
		assertEquals(expected, updatedDataset.getJsonMapping());
	}

	@Test
	public void testUpdateDatasetQuality() throws IOException {
		eu.iescities.server.accountinterface.datainterface.Dataset ds = registeredDatasets.get(1);

		Dataset dataset = new Dataset(ds);
		dataset.setQuality(4);
		dataset.setQualityVerifiedBy(zgzAdminUser.getUserId());
		dataset.setLang(Language.EN);

		Entity<Dataset> entity = Entity.entity(dataset,
				MediaType.APPLICATION_JSON);

		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Dataset updatedDataset = target(
				"entities/datasets/" + dataset.getDatasetId()).request(
				MediaType.APPLICATION_JSON).put(entity, Dataset.class);

		assertEquals(4, updatedDataset.getQuality());
		assertEquals(zgzAdminUser.getUserId(), updatedDataset.getQualityVerifiedBy());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testUpdateDatasetInvalidId() throws IOException, DataSourceConnectorException {
		final String jsonSchema = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		Dataset datasetid = new Dataset();
		datasetid.setName("changed_dataset_name");
		datasetid.setDescription("Some new description.");
		datasetid.setMapping(jsonSchema);

		Entity<Dataset> entity = Entity.entity(datasetid,
				MediaType.APPLICATION_JSON);

		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Response response = target("entities/datasets/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON)
				.put(entity, Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testCreateDatasetRegisteredUser() throws IOException, DataSourceConnectorException {
		final String dbConfig = FileLoader.loadResource("/dbconfig/sqlite_conf.json");

		Dataset datasetInfo = new Dataset();
		datasetInfo.setName("other_dataset");
		datasetInfo.setDescription("Some long description.");
		datasetInfo.setMapping(dbConfig);
		datasetInfo.setLang(Language.EN);

		Entity<Dataset> entity = Entity.entity(datasetInfo,
				MediaType.APPLICATION_JSON);

		client().register(HttpAuthenticationFeature.basic(BASIC_USER, BASIC_PASS));

		Dataset dataset = target("entities/datasets/").request(
				MediaType.APPLICATION_JSON).post(entity, Dataset.class);

		assertEquals(-1, dataset.getCouncilId());
		assertEquals(basicUser.getUserId(), dataset.getUserId());
	}

	@Test
//	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetEmptyMapping() throws IOException {
		Dataset dataset = target(
				"entities/datasets/"
						+ emptyMappingDataset.getDatasetId()).request(
				MediaType.APPLICATION_JSON).get(Dataset.class);

		Dataset expected = new Dataset(emptyMappingDataset);

		assertEquals(expected.getName(), dataset.getName());
		assertEquals(new DataSourceConnector(), dataset.getJsonMapping());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetWithMapping() throws IOException {
		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Dataset dataset = target(
				"entities/datasets/"
						+ registeredDatasets.get(0).getDatasetId()).request(
				MediaType.APPLICATION_JSON).get(Dataset.class);

		eu.iescities.server.accountinterface.datainterface.Dataset ds = registeredDatasets.get(0);
		Dataset expected = new Dataset(ds);

		assertEquals(expected.getName(), dataset.getName());
		assertNotNull(dataset.getJsonMapping());
	}
	
	@Test
	public void testGetDatasetWithDatabaseMapping() throws IOException {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Dataset dataset = target("entities/datasets/" 
				+ registeredDatasets.get(1).getDatasetId())
				.request(MediaType.APPLICATION_JSON)
				.get(Dataset.class);
		
		assertEquals(ConnectorType.database, dataset.getJsonMapping().getType());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetUnauthorizedMapping() throws IOException {
		client().register(
				HttpAuthenticationFeature.basic(BASIC_USER, BASIC_PASS));

		Dataset dataset = target(
				"entities/datasets/"
						+ registeredDatasets.get(0).getDatasetId()).request(
				MediaType.APPLICATION_JSON).get(Dataset.class);

		eu.iescities.server.accountinterface.datainterface.Dataset ds = registeredDatasets.get(0);
		Dataset expected = new Dataset(ds);

		assertEquals(expected.getName(), dataset.getName());
		assertEquals(new DataSourceConnector(), dataset.getJsonMapping());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetInvalidID() {
		Response response = target("entities/datasets/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void testDeleteDatasetAdmin() {
		client().register(HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Response response = target(
				"entities/datasets/"
						+ registeredDatasets.get(3).getDatasetId()).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteDatasetUser() {
		client().register(HttpAuthenticationFeature.basic(BASIC_USER, BASIC_PASS));

		Response response = target(
				"entities/datasets/"
						+ registeredDatasets.get(4).getDatasetId()).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteDatasetInvalidUser() {
		client().register(HttpAuthenticationFeature.basic(OTHER_USER, OTHER_PASS));

		Response response = target(
				"entities/datasets/"
						+ registeredDatasets.get(4).getDatasetId()).request(
				MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testDeleteDatasetInvalidID() {
		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Response response = target("entities/datasets/" + Integer.MAX_VALUE)
				.request(MediaType.APPLICATION_JSON).delete(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testSearchDataset() {
		List<Dataset> datasets = target("entities/datasets/search")
				.queryParam("keywords", "sqlite sparql")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Dataset>>() {
				});

		assertEquals(2, datasets.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testSearchDatasetInvalidLimit() {
		Response response = target("entities/datasets/search")
				.queryParam("keywords", "sqlite sparql")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllDatasets() {
		List<Dataset> datasets = target("entities/datasets/all").request(
				MediaType.APPLICATION_JSON).get(
				new GenericType<List<Dataset>>() {
				});

		assertEquals(7, datasets.size());

		DataSourceConnector expected = new DataSourceConnector();
		for (Dataset dataset : datasets) {
			assertEquals(expected, dataset.getJsonMapping());
		}
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetAllDatasetsInvalidLimit() {
		Response response = target("entities/datasets/all")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetApps() {
		List<Application> applications = target(
				"entities/datasets/"
						+ registeredDatasets.get(0).getDatasetId() + "/apps")
				.request(MediaType.APPLICATION_JSON).get(
						new GenericType<List<Application>>() {
						});

		assertEquals(1, applications.size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetAppsInvalidID() {
		Response response = target(
				"entities/datasets/" + Integer.MAX_VALUE + "/apps").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetDatasetAppsInvalidLimit() {
		Response response = target(
				"entities/datasets/"
						+ registeredDatasets.get(0).getDatasetId() + "/apps")
				.queryParam("limit", 2000).request(MediaType.APPLICATION_JSON)
				.get(Response.class);

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetDatasetMapping() throws IOException, DataSourceConnectorException {
		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		DataSourceConnector connector = target(
				"entities/datasets/"
						+ registeredDatasets.get(0).getDatasetId()
						+ "/mapping").request(MediaType.APPLICATION_JSON).get(
								DataSourceConnector.class);

		final String expectedJSON = FileLoader.loadResource("/sparql/foaf_schema.json");
		final DataSourceConnector expected = ConnectorFactory.load(expectedJSON);

		assertEquals(expected, connector);
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = 1)
	public void testGetDatasetMappingNotFound() {
		client().register(
				HttpAuthenticationFeature.basic(ZGZ_ADMIN_USER, ZGZ_ADMIN_PASS));

		Response response = target(
				"entities/datasets/" + Integer.MAX_VALUE + "/mapping").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaInfoSPARQL() {
		final SchemaInfo schemaInfo = target(
				"entities/datasets/" + registeredDatasets.get(0).getDatasetId() + "/info").request(
				MediaType.APPLICATION_JSON).get(SchemaInfo.class);

		assertEquals(ConnectorType.sparql, schemaInfo.getType());

		assertEquals(11, schemaInfo.getTables().size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaSPARQL() {
		final DataSourceInfo datasetInfo = target(
				"entities/datasets/" + registeredDatasets.get(0).getDatasetId() + "/schema").request(
				MediaType.APPLICATION_JSON).get(DataSourceInfo.class);

		assertEquals(ConnectorType.sparql, datasetInfo.getType());

		assertEquals(11, datasetInfo.getTables().size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaInfoSQL() {
		final SchemaInfo schemaInfo = target(
				"entities/datasets/" + registeredDatasets.get(1).getDatasetId() + "/info").request(
				MediaType.APPLICATION_JSON).get(SchemaInfo.class);

		assertEquals(ConnectorType.database, schemaInfo.getType());

		assertEquals(2, schemaInfo.getTables().size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaSQL() {
		final DataSourceInfo datasetInfo = target(
				"entities/datasets/" + registeredDatasets.get(1).getDatasetId() + "/schema").request(
				MediaType.APPLICATION_JSON).get(DataSourceInfo.class);

		assertEquals(ConnectorType.database, datasetInfo.getType());

		assertEquals(2, datasetInfo.getTables().size());
	}

	@Test
	@Ignore
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaInfoJSON() throws InterruptedException {
		final SchemaInfo schemaInfo = target(
				"entities/datasets/" + jsonDataset.getDatasetId() + "/info").request(
				MediaType.APPLICATION_JSON).get(SchemaInfo.class);

		assertEquals(ConnectorType.json, schemaInfo.getType());

		assertEquals(1, schemaInfo.getTables().size());
	}

	@Test
	@Ignore
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaJSON() throws InterruptedException {
		final DataSourceInfo schemaInfo = target(
				"entities/datasets/" + jsonDataset.getDatasetId() + "/schema").request(
				MediaType.APPLICATION_JSON).get(DataSourceInfo.class);

		assertEquals(ConnectorType.json, schemaInfo.getType());

		assertEquals(1, schemaInfo.getTables().size());
	}

	@Test
	@PerfTest(invocations = ContiPerConf.INVOCATIONS, threads = ContiPerConf.THREADS)
	public void testGetSchemaInfoNotFound() {
		Response response = target(
				"entities/datasets/" + Integer.MAX_VALUE + "/info").request(
				MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
				response.getStatus());
	}
}
