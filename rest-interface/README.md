 Rest Interface library
======================
This library is responsible for giving access to server features that need to 
be accessed remotely via the rest services.

The rest services are decribed using Swagger ([https://helloreverb.com/developers/swagger]https://helloreverb.com/developers/swagger)

After launching the embedded server, the web methods are described and can be tested by connecting to the following URL

	http://127.0.0.1:8002/IESCities/swagger/index.html

Authentication
-------------------
The authentication is done using Basic Auth. More info:

http://en.wikipedia.org/wiki/Basic_access_authentication
https://tools.ietf.org/html/rfc2617

All methods requiring authentication must be called with a header containing the user:pass encoded in Base64. 

Example of Javascript code to generate the user/pass encoding and API method request

    function getHTTPObject()
    {
      if (window.XMLHttpRequest) {
  	    return new XMLHttpRequest();
      }
      else 
  	    return new ActiveXObject("Microsoft.XMLHTTP");
    }

    function make_basic_auth(user, password) {
      var tok = user + ':' + password;
      var hash = window.btoa(tok);
      return "Basic " + hash;
    }
 
    var auth = make_basic_auth('bob', 'secret');
    url = 'http://localhost:8080/IESCities/api/datasets/createdataset';
    xmlHttp = getHTTPObject();
    
    xmlHttp.onreadystatechange = function() {
    	if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
    		alert(xmlHttp.responseText);
    };
    
    xmlHttp.open('POST',url);
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.setRequestHeader('Authorization', auth);    

    var dataset = 	{ 	name: "new_dataset",
						description: "test_dataset",
						jsonMapping: document.getElementById("jsonMapping").value
				  	};
                    
    xmlHttp.send(JSON.stringify(dataset));   
 
AppRestInterface
--------------------
Here are defined the operations that can be performed remotely 
conceptually deal with the mobile applications.

--> createApplication --> Register a new application, receives as parameters all the fields needed
 for an application and the seesionid of the register user that performs the create.  
 URL: /apps/createapplication
 Method: GET
 Params:      
 	 * @param sessionID of the owner of the application 
     * @param appName
     * @param description
     * @param url
     * @param image
     * @param version
     * @param termsOfService
     * @param permissions
     * @param packageName
     * @param setoftags
     * @param councilID
     * @param scopeID
 Return:
      * @return An application objetc or null in case of an error or an exception. 
  
 
--> deleteDataset --> Removes a given application from the IES cities platform. 
Only allowed by application developers or IES Cities admin users.
URL: /apps/deleteapplication
Method: GET
Params:
	 * @param sessionID the user's session ID
	 * @param appID the ID of the application
Return:
	 * @return number of deleted applications


--> getAllApplications --> 	Gets a subset of the list of all applications. (0, 1000)
URL: /apps/all
Method: GET
	 * @return a list of applications sorted by the application IDs (ascending)

--> getAllApplicationsMatching --> Gets a subset of the list of all applications containing 
the given search string.
The search string is split into words and each is matched against application names, descriptions, 
tags and package names (OR-connected).
Note: The search is case-insensitive in all fields but the tags.

URL: /apps/search/{keywords}
Method: GET
Params:
	 * @param keywords the string to search for (space-separated words).
Return:

     * @return a list of applications including at least one of the search words
  and sorted by the application IDs (ascending)

--> getAllApplicationsNearGeopoint --> Gets a subset of the list of all applications,
whose location satisfies the constraint of being within the defined radius.

URL:/apps/near/{geopoint}{radius : (/\\S+)?}")
Params: 
     * @param geopoint, where i am.
     * @param radius, search radius.

Return:

	 * @return a list of applications sorted by the application IDs (ascending)

--> getApplicationInfo --> Retrieves an application object from the datastore based on its ID.

URL:/apps/id/{appID}
Method: GET
Params: 
	 * @param appID the ID of the application to retrieve


Return:
	 * @return a  Application object
	 
CityRestInterface
--------------------	 
Here are defined the operations that can be performed remotely 
conceptually deal with council management.	 

--> createCity --> 	Creates a new council. Only allowed by IES Cities admin users.

URL: /city/createcity
Method: GET
Params:
     * @param sessionid the admin's session ID
     * @param name
     * @param description
     * @param region
     * @param northwestlatitude
     * @param northwestlongitude
     * @param southeastlatitude
     * @param southeastlongitude
     
Return:
	 * @return the new council object

--> getAllCities --> Gets a subset of the list of all councils. ( 0 , 100)

URL: /city/getallcities 
Method: GET
	 * @return a list of councils.
	 
 --> getCouncilApps --> Gets a subset of the list of all applications related to a given council.
 
 URL: /city/getcouncilapps
 Method: GET
 Params:
 	 * @param councilid the council's ID
	 * @param offset  offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit maximum number of applications to retrieve
 Return:
	 * @return a list of applications sorted by their IDs (ascending) Response.Status.BAD_REQUEST
	 *             is thrown if the limit is too high
 
 
 --> getCouncilDataset --> Gets a subset of the list of all datasets related to a given council.
 
 URL: /city/getcouncildatasets
 Method: GET
 Params:
 	 * @param councilid the council's ID
	 * @param offset  offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit maximum number of datasets to retrieve
 Return:
	 * @return a list of datasets sorted by their IDs (ascending) Response.Status.BAD_REQUEST
	 *             is thrown if the limit is too high

--> removeCity -->  Removes an existing council. Only allowed by IES Cities admin users.

 URL: /city/removecity
 Method: GET
 Params:
     * @param sessionid the admin's session ID
     * @param councilid the ID of the council to remove
 Return:
	 * @return the number of removed councils


DatasetRestInterface
--------------------	 
Here are defined the operations that can be performed remotely 
conceptually deal with datasets management.	 


--> createDataset --> 	Registers a new dataset. Only allowed by council admin users.

URL: /datasets/createdataset
Method: GET
Params:
	 * @param sessionID the user's session ID
	 * @param name  The name of the dataset
	 * @param description A description of the dataset
	 
Return:
	 * @return the new dataset object or WResponse.Status.BAD_REQUEST
	 *			 if the session ID is invalid or the user is not a council admin.


--> deleteDataset --> Removes a dataset from the IES cities platorm.
	 * A dataset can only be removed by an admin of the council which
	 * published the dataset or an IES cities platform admin.

URL: /datasets/deletedataset 

Method: GET
Params:
	 * @param sessionid the user's session ID
	 * @param datasetid the data set's ID
Return:
	 * @return the number of instances that were deleted

--> getDataset --> Gets a dataset by its ID.

URL: /datasets/getdataset
Method: GET
Params:
	 * @param datasetID the ID of the dataset to retrieve

Return: 
	 * @return the dataset

--> searchDataset --> Gets a subset of the list of all datasets containing the given search string.
	 * The search string is split into words and each is matched against dataset
	 * names and descriptions (OR-connected), case-insensitively.

URL: /datasets/searchdataset  

Method: GET
Params:

	 * @param searchString the string to search for (space-separated words)
	 * @param offset to start at (<tt>0</tt> starts with the first element)
	 * @param limit maximum number of datasets to retrieve
	  
Return:
	 * @return a list of datasets including at least one of the search words and
	 *         sorted by the dataset IDs (ascending)

UserRestInterface
--------------------	 
Here are defined the operations that can be performed remotely 
conceptually deal with user management.	 


--> createUser --> 	Registers a user with username and password. This method allows any non-
     * empty username and password. It is upon the caller to check for bad 
     * names and password security. 


URL: /users/createuser
Method: GET
Params:
     * @param name
     * @param password
     * @param surname
     * @param username
     * @param email
     * @param council_id
     * @param profile
	 
Return:
	 * @return the new user object or WebApplicationException
				 


--> deleteUser --> Delete the user, password validation is required.

URL: /users/deleteuser 

Method: GET
Params:
     * @param sessionid
     * @param password

Return:
	 * @return true if the user is deleted

--> getUser --> Retrieves the  User object from the datastore and validates the
	 * passed sessionID.


URL: /users/getuser 
Method: GET
Params:
	 * @param sessionid the user's session ID
	  

Return: 
	 * @return a  User object or null if sessiond is invalid.
	 

--> logout --> Logs a user out identified by a session ID.

URL: /users/logout  

Method: GET
Params:

	 * @param sessionID the user's session ID
	  
Return:
	 * @return true if the user is loged out correctly false otherwise

--> updateUser --> Update a user, by the moment only the password and user profile.

URL: /users/updateuser  

Method: GET
Params:

      * @param sessionid
     * @param name
     * @param currentpassword
     * @param newpassword
     * @param profile
     * @param surname
     * @param username
     * @param email
     * @param council_id
    
	  
Return:
	 * @return a  User object or null if sessiond is invalid.

Example
-------

Majadahonda application get data from IESCIties.

Majadahonda app, get from the server the id of the used dataset:

http://150.241.239.65:8080/IESCities/webresources/datasets/searchdataset?searchString=majadahonda_dataset&offset=0&limit=1

Then make a query througth the API.
http://150.241.239.65:8080/IESCities/webresources/query/<dataset_id>/sql

And put in RAW Boy, as POST request the query: "select * from events"

The result in JSON;

	{
	    "rows": [{
	        "columns": {
	            "FECHA_F": "2014-03-15",
	            "IMAGEN": "mercado.jpg",
	            "FECHA_I": "2014-03-14",
	            "TITULO": "Mercado medieval",
	            "COORDENADAS": "40.47132,-3.87385",
	            "ZONA": "2",
	            "HORA_I": "00:00:00",
	            "DESCRIPCION_C": "Mercado medieval",
	            "PRECIO": "0",
	            "DIRECCION": "Calle Granadilla ,23 - 28220 Majadahonda",
                "CIF": "A30079842",
	            "HORA_F": "00:00:00",
	            "ID": "1",
	            "DESCRIPCION_L": "Mercado con espectáculos y animación, animales, zona para comer y ambietación medieval.",
	            "CATEGORIA": "1"
	        }
	    }, {
	        "columns": {
	            "FECHA_F": "2014-03-31",
	            "IMAGEN": "default.jpg",
	            "FECHA_I": "2014-03-24",
	            "TITULO": "dafasd",
	            "COORDENADAS": "(42.8420476, -2.672694399999955)",
	            "ZONA": "2",
	            "HORA_I": "00:00:00",
	            "DESCRIPCION_C": " fsdafsda fsda fsda",
	            "PRECIO": "3",
	            "DIRECCION": "dato 40, vitoria",
	            "CIF": "E58230517",
	            "HORA_F": "00:00:00",
	            "ID": "11",
	            "DESCRIPCION_L": " f sda fsdaf sda",
	            "CATEGORIA": "3"
	        }
	    }, {
	        "columns": {
	            "FECHA_F": "2014-03-17",
	            "IMAGEN": "E58230517.jpg",
	            "FECHA_I": "2014-03-03",
	            "TITULO": "dfasdfa",
	            "COORDENADAS": "(42.8420476, -2.672694399999955)",
	            "ZONA": "5",
	            "HORA_I": "00:00:00",
	            "DESCRIPCION_C": " fdsafsda",
	            "PRECIO": "3",
	            "DIRECCION": "dato 40, vitoria",
	            "CIF": "E58230517",
	            "HORA_F": "00:00:00",
	            "ID": "12",
	            "DESCRIPCION_L": " fsda fsdaf sda",
	            "CATEGORIA": "2"
	        }
	    }],
	    "count": 3
	}




		
Commands
--------

